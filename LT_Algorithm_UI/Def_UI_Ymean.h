﻿#ifndef Def_UI_Ymean_h__
#define Def_UI_Ymean_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

#define YMEAN_COUNT_MAX 500

typedef enum enYmean_Spec
{
	Spec_Ymean = 0,
	Spec_Ymean_MAX,
};

typedef struct _tag_UI_Ymean
{
	// Inspect
	ST_Inspect		stInspect;

	// Spec
	int				iDefectBlockSize;					// Mean filter 블록 사이즈
	int				iEdgeSize;							// 이미지의 Edge(or Boundary) 영역 정의 픽셀 사이즈
	float			fCenterThreshold;					// 이미지 Center 영역에서, 블록의 중심 평균 밝기와 주변 블록의 평균 밝기 비율
	float			fEdgeThreshold;						// 이미지 Edge 영역에서, 블록의 중심 평균 밝기와 주변 블록의 평균 밝기 비율
	float			fCornerThreshold;					// 이미지 Corner 영역에서, 블록의 중심 평균 밝기와 주변 블록의 평균 밝기 비율
	int				iLscBlockSize;						// 지역적인 렌즈 Shading 보상 블록의 크기
	bool			bEnableCircle;						// 4개의 corner 영역을 검사 영역에서 제외하기 위한 parameter 0 : Main, Narrow Camera 1 : Wide Camera
	int				iPosOffsetX;						// Wide 영상 내 타원 검출 시 사용하는 이미지 영역의 X offset 좌표(영상의 외곽 영역을 타원 검출 시 사용하지 않을 경우 Offset 적용)
	int				iPosOffsetY;						// 이미지 내 타원을 찾을 때 사용하고자 하는 좌, 우 픽셀 좌표(영상의 외곽 영역을 타원 검출 시 사용하지 않을 경우 Offset 적용)
	double			dbRadiusRatioX;						// Wide 영상에서 검출된 타원의 가로 반지름 비율
	double			dbRadiusRatioY;						// Wide 영상에서 검출된 타원의 세로 반지름 비율

	ST_Spec			stSpecMin[Spec_Ymean_MAX];
	ST_Spec			stSpecMax[Spec_Ymean_MAX];
	bool					b8BitUse;
	_tag_UI_Ymean()
	{
		stInspect.Reset();

		for (int nidx = 0; nidx < Spec_Ymean_MAX; nidx++)
		{
			stSpecMin[nidx].Reset();
			stSpecMax[nidx].Reset();
		}
		

		iDefectBlockSize	= 32;
		iEdgeSize			= 100;
		fCenterThreshold	= 50.0;
		fEdgeThreshold		= 50.0;
		fCornerThreshold	= 50.0;
		iLscBlockSize		= 128;
		bEnableCircle		= false;
		iPosOffsetX			= 5;
		iPosOffsetY			= 5;
		dbRadiusRatioX		= 0.5;
		dbRadiusRatioY		= 0.5;
		b8BitUse = false;
	};

	_tag_UI_Ymean& operator= (_tag_UI_Ymean& ref)
	{
		stInspect.Reset();

		iDefectBlockSize	= ref.iDefectBlockSize;	
		iEdgeSize			= ref.iEdgeSize;			
		fCenterThreshold	= ref.fCenterThreshold;	
		fEdgeThreshold		= ref.fEdgeThreshold;		
		fCornerThreshold	= ref.fCornerThreshold;	
		iLscBlockSize		= ref.iLscBlockSize;		
		bEnableCircle		= ref.bEnableCircle;		
		iPosOffsetX			= ref.iPosOffsetX;			
		iPosOffsetY			= ref.iPosOffsetY;			
		dbRadiusRatioX		= ref.dbRadiusRatioX;	
		dbRadiusRatioY		= ref.dbRadiusRatioY;
		b8BitUse			= ref.b8BitUse;

		for (int nidx = 0; nidx < Spec_Ymean_MAX; nidx++)
		{
			stSpecMin[nidx] = ref.stSpecMin[nidx];
			stSpecMax[nidx] = ref.stSpecMax[nidx];
		}
		
				
		return *this;
	};

}ST_UI_Ymean, *PST_UI_Ymean;

#pragma pack(pop)

#endif // Def_Ymean_h__
