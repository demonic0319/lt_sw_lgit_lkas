﻿// List_Chart.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_Chart.h"

// CList_Chart

typedef enum enListNum_Chart
{
	ChOp_Object = 0,
	ChOp_PosX,
	ChOp_PosY,
	ChOp_Width,
	ChOp_Height,
	ChOp_Color,
	ChOp_Bright,
	ChOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_Chart[] =
{
	_T(""),						// ChOp_Object = 0,
	_T("Center X"),				// ChOp_PosX,
	_T("Center Y"),				// ChOp_PosY,
	_T("Width"),				// ChOp_Width,
	_T("Height"),				// ChOp_Height,
	_T("Color"),				// ChOp_Color,
	_T("Bright"),				// ChOp_Bright,
	NULL
};

const int	iListAglin_Chart[] =
{
	LVCFMT_LEFT,				// ChOp_Object = 0,
	LVCFMT_CENTER,				// ChOp_PosX,
	LVCFMT_CENTER,				// ChOp_PosY,
	LVCFMT_CENTER,				// ChOp_Width,
	LVCFMT_CENTER,				// ChOp_Height,
	LVCFMT_CENTER,				// ChOp_Color,
	LVCFMT_CENTER,				// ChOp_Bright,
};

const int	iHeaderWidth_Chart[] =
{
	 60,						// ChOp_Object = 0,
	150,						// ChOp_PosX,
	150,						// ChOp_PosY,
	150,						// ChOp_Width,
	150,						// ChOp_Height,
	150,						//  ChOp_Color,
	150,						//  ChOp_Bright,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_OVERLAY	2001

IMPLEMENT_DYNAMIC(CList_Chart, CList_Cfg_VIBase)

CList_Chart::CList_Chart()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Chart::~CList_Chart()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_Chart, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_Chart::OnNMClick						)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_Chart::OnNMDblclk					)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_Chart::OnEnKillFocusEdit				)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_TYPE,	&CList_Chart::OnEnKillFocusComboType		)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_TYPE,	&CList_Chart::OnEnSelectComboType			)	
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_Chart 메시지 처리기입니다.
int CList_Chart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader(); 
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_ed_CellEdit.Create(WS_CHILD | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_EDT_CELLEDIT);
	m_ed_CellEdit.SetValidChars(_T("0123456789"));

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkColor[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szMarkColor[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_Chart::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_Chart::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_Chart::InitHeader()
{
	for (int nCol = 0; nCol < ChOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_Chart[nCol], iListAglin_Chart[nCol], iHeaderWidth_Chart[nCol]);
	}

	for (int nCol = 0; nCol < ChOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_Chart[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_Chart::InsertFullData()
{
	if ( NULL == m_pstConfig)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < ChOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_Chart::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	szValue.Format(_T("ROI"));
	SetItemText(nRow, ChOp_Object, szValue);

	szValue.Format(_T("%d"), m_pstConfig->rtROI.CenterPoint().x);
	SetItemText(nRow, ChOp_PosX, szValue);

	szValue.Format(_T("%d"), m_pstConfig->rtROI.CenterPoint().y);
	SetItemText(nRow, ChOp_PosY, szValue);

	szValue.Format(_T("%d"), m_pstConfig->rtROI.Width());
	SetItemText(nRow, ChOp_Width, szValue);

	szValue.Format(_T("%d"), m_pstConfig->rtROI.Height());
	SetItemText(nRow, ChOp_Height, szValue);

	szValue.Format(_T("%s"), g_szMarkColor[m_pstConfig->nMarkColor]);
	SetItemText(nRow, ChOp_Color, szValue);

	szValue.Format(_T("%d"), m_pstConfig->nBrightness);
	SetItemText(nRow, ChOp_Bright, szValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_Chart::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_Chart::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < ChOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == ChOp_Color)
			{
				m_cb_Type.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Type.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Type.SetFocus();
				m_cb_Type.SetCurSel(m_pstConfig->nMarkColor);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	}

	GetOwner()->SendNotifyMessage(m_wn_Change, 0, 0);

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_Chart::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_Chart::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if ( NULL == m_pstConfig)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfig->rtROI;

	switch (nCol)
	{
	case ChOp_PosX:
		m_pstConfig->RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case ChOp_PosY:
		m_pstConfig->RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case ChOp_Width:
		m_pstConfig->RectPosWH(iValue, rtTemp.Height());
		break;
	case ChOp_Height:
		m_pstConfig->RectPosWH(rtTemp.Width(), iValue);
		break;
	case ChOp_Bright:
		m_pstConfig->nBrightness = iValue;
		break;
	default:
		break;
	}
	
	if (m_pstConfig->rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfig->rtROI;

		m_pstConfig->rtROI.left	= 0;
		m_pstConfig->rtROI.right	= m_pstConfig->rtROI.left + rtTemp.Width();
	}

	if (m_pstConfig->rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfig->rtROI;

		m_pstConfig->rtROI.right	= (LONG)m_dwImage_Width;
		m_pstConfig->rtROI.left	= m_pstConfig->rtROI.right - rtTemp.Width();
	}

	if (m_pstConfig->rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfig->rtROI;

		m_pstConfig->rtROI.top	= 0;
		m_pstConfig->rtROI.bottom = rtTemp.Height() + m_pstConfig->rtROI.top;
	}

	if (m_pstConfig->rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfig->rtROI;

		m_pstConfig->rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfig->rtROI.top	= m_pstConfig->rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfig->rtROI.Height() <= 0)
		m_pstConfig->RectPosWH(m_pstConfig->rtROI.Width(), 1);

	if (m_pstConfig->rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfig->RectPosWH(m_pstConfig->rtROI.Width(), m_dwImage_Height);

	if (m_pstConfig->rtROI.Width() <= 0)
		m_pstConfig->RectPosWH(1, m_pstConfig->rtROI.Height());

	if (m_pstConfig->rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfig->RectPosWH(m_dwImage_Width, m_pstConfig->rtROI.Height());

	CString szValue;

	switch (nCol)
	{
	case ChOp_PosX:
		szValue.Format(_T("%d"), m_pstConfig->rtROI.CenterPoint().x);
		break;
	case ChOp_PosY:
		szValue.Format(_T("%d"), m_pstConfig->rtROI.CenterPoint().y);
		break;
	case ChOp_Width:
		szValue.Format(_T("%d"), m_pstConfig->rtROI.Width());
		break;
	case ChOp_Height:
		szValue.Format(_T("%d"), m_pstConfig->rtROI.Height());
		break;
	case ChOp_Bright:
		szValue.Format(_T("%d"), m_pstConfig->nBrightness);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(szValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_Chart::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_Chart::GetCellData()
{
	if ( NULL == m_pstConfig)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_Chart::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);

		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/3 - 16:11
// Desc.		:
//=============================================================================
void CList_Chart::OnEnKillFocusComboType()
{
	SetItemText(m_nEditRow, ChOp_Color, g_szMarkColor[m_pstConfig->nMarkColor]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/3 - 16:11
// Desc.		:
//=============================================================================
void CList_Chart::OnEnSelectComboType()
{
	m_pstConfig->nMarkColor = m_cb_Type.GetCurSel();

	SetItemText(m_nEditRow, ChOp_Color, g_szMarkColor[m_pstConfig->nMarkColor]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}