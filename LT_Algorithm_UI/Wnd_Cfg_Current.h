﻿#ifndef Wnd_Cfg_Current_h__
#define Wnd_Cfg_Current_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Current.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Current
//-----------------------------------------------------------------------------
class CWnd_Cfg_Current : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Current)

public:
	CWnd_Cfg_Current();
	virtual ~CWnd_Cfg_Current();

	enum enCurrentStatic
	{
		STI_CUR_PARAM = 0,
		STI_CUR_SPEC,
		STI_CUR_OFFSET_CH1,
		STI_CUR_OFFSET_CH2,
		STI_CUR_OFFSET_CH3,
		STI_CUR_OFFSET_CH4,
		STI_CUR_OFFSET_CH5,
		STI_CUR_MAX,
	};

	enum enCurrentButton
	{
		BTN_CUR_MAX = 1,
	};

	enum enCurrentComobox
	{
		CMB_CUR_MAX = 1,
	};

	enum enCurrentEdit
	{
		EDT_CUR_OFFSET_CH1 = 0,
		EDT_CUR_OFFSET_CH2,
		EDT_CUR_OFFSET_CH3,
		EDT_CUR_OFFSET_CH4,
		EDT_CUR_OFFSET_CH5,
		EDT_CUR_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CVGStatic			m_st_Item[STI_CUR_MAX];
	CButton				m_bn_Item[BTN_CUR_MAX];
	CComboBox			m_cb_Item[CMB_CUR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_CUR_MAX];

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Spec[Current_Max];
	CMFCMaskedEdit		m_ed_SpecMin[Current_Max];
	CMFCMaskedEdit		m_ed_SpecMax[Current_Max];
	CMFCButton			m_chk_SpecMin[Current_Max];
	CMFCButton			m_chk_SpecMax[Current_Max];

	ST_UI_Current*		m_pstConfig		= NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Current* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Current_h__
