﻿#ifndef Def_UI_BlackSpot_h__
#define Def_UI_BlackSpot_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

#define BlackSpot_COUNT_MAX 500

typedef enum enBlackSpot_Spec
{
	Spec_BlackSpot = 0,
	Spec_BlackSpot_MAX,
};

typedef struct _tag_UI_BlackSpot
{
	// Inspect
	ST_Inspect		stInspect;

	// Spec
	int		nBlockWidth;
	int		nBlockHeight;
	int		nClusterSize;
	int		nDefectInCluster;
	double	dDefectRatio;
	int		nMaxSingleDefectNum;

	bool			bEnableCircle;						// 4개의 corner 영역을 검사 영역에서 제외하기 위한 parameter 0 : Main, Narrow Camera 1 : Wide Camera
	int				iPosOffsetX;						// Wide 영상 내 타원 검출 시 사용하는 이미지 영역의 X offset 좌표(영상의 외곽 영역을 타원 검출 시 사용하지 않을 경우 Offset 적용)
	int				iPosOffsetY;						// 이미지 내 타원을 찾을 때 사용하고자 하는 좌, 우 픽셀 좌표(영상의 외곽 영역을 타원 검출 시 사용하지 않을 경우 Offset 적용)
	double			dbRadiusRatioX;						// Wide 영상에서 검출된 타원의 가로 반지름 비율
	double			dbRadiusRatioY;						// Wide 영상에서 검출된 타원의 세로 반지름 비율

	ST_Spec			stSpecMin[Spec_BlackSpot_MAX];
	ST_Spec			stSpecMax[Spec_BlackSpot_MAX];

	bool					b8BitUse;
	_tag_UI_BlackSpot()
	{
		stInspect.Reset();

		for (int nidx = 0; nidx < Spec_BlackSpot_MAX; nidx++)
		{
			stSpecMin[nidx].Reset();
			stSpecMax[nidx].Reset();
		}
		
		b8BitUse			= false;
		nBlockWidth			= 0;
		nBlockHeight		= 0;
		nClusterSize		= 0;
		nDefectInCluster	= 0;
		dDefectRatio		= 1.0;
		nMaxSingleDefectNum = 0;

		bEnableCircle		= false;
		iPosOffsetX			= 5;
		iPosOffsetY			= 5;
		dbRadiusRatioX		= 0.5;
		dbRadiusRatioY		= 0.5;
	};

	_tag_UI_BlackSpot& operator= (_tag_UI_BlackSpot& ref)
	{
		stInspect.Reset();

		nBlockWidth = ref.nBlockWidth;
		nBlockHeight = ref.nBlockHeight;
		nClusterSize = ref.nClusterSize;
		nDefectInCluster = ref.nDefectInCluster;
		dDefectRatio = ref.dDefectRatio;
		nMaxSingleDefectNum = ref.nMaxSingleDefectNum;

		bEnableCircle		= ref.bEnableCircle;		
		iPosOffsetX			= ref.iPosOffsetX;			
		iPosOffsetY			= ref.iPosOffsetY;			
		dbRadiusRatioX		= ref.dbRadiusRatioX;	
		dbRadiusRatioY		= ref.dbRadiusRatioY;
		b8BitUse			= ref.b8BitUse;

		for (int nidx = 0; nidx < Spec_BlackSpot_MAX; nidx++)
		{
			stSpecMin[nidx] = ref.stSpecMin[nidx];
			stSpecMax[nidx] = ref.stSpecMax[nidx];
		}
				
		return *this;
	};

}ST_UI_BlackSpot, *PST_UI_BlackSpot;

#pragma pack(pop)

#endif // Def_BlackSpot_h__
