﻿#ifndef Wnd_Cfg_BlackSpot_h__
#define Wnd_Cfg_BlackSpot_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_BlackSpot.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Ymean
//-----------------------------------------------------------------------------
class CWnd_Cfg_BlackSpot : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_BlackSpot)

public:
	CWnd_Cfg_BlackSpot();
	virtual ~CWnd_Cfg_BlackSpot();

	enum enBlackSpotStatic
	{
		STI_BS_INSPECT = 0,
		STI_BS_SPEC,
		STI_BS_PARAM,
		STI_BS_DataForamt,
		STI_BS_OutMode,
		STI_BS_SensorType,
		STI_BS_BlackLevel, //공통
		STI_BS_BlockWidth,
		STI_BS_BlockHeight,
		STI_BS_ClusterSize,
		STI_BS_DefectInCluster,
		STI_BS_DefectRatio,
		STI_BS_MaxSingleDefectNum,
		STI_BS_EnableCircle,
		STI_BS_PosOffsetX,
		STI_BS_PosOffsetY,
		STI_BS_RadiusRationX,
		STI_BS_RadiusRationY,
		STI_BS_Count,
		STI_BS_8bitUse,
		STI_BS_MAX,
	};

	enum enBlackSpotButton
	{
		BTN_BS_MAX = 1,
	};

	enum enBlackSpotComobox
	{
		CMB_BS_DataForamt = 0,
		CMB_BS_OutMode,
		CMB_BS_SensorType,
		CMB_BS_EnableCircle,
		CMB_BS_8bitUse,
		CMB_BS_MAX,
	};

	enum enBlackSpotEdit
	{
		EDT_BS_BlackLevel = 0,
		EDT_BS_BlockWidth,
		EDT_BS_BlockHeight,
		EDT_BS_ClusterSize,
		EDT_BS_DefectInCluster,
		EDT_BS_DefectRatio,
		EDT_BS_MaxSingleDefectNum,
		EDT_BS_PosOffsetX,
		EDT_BS_PosOffsetY,
		EDT_BS_RadiusRationX,
		EDT_BS_RadiusRationY,
		EDT_BS_Count,
		EDT_BS_MAX,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_BS_MAX];
	CButton						m_bn_Item[BTN_BS_MAX];
	CComboBox					m_cb_Item[CMB_BS_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_BS_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_BlackSpot_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_BlackSpot_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_BlackSpot_MAX];
	CMFCButton					m_chk_SpecMin[Spec_BlackSpot_MAX];
	CMFCButton					m_chk_SpecMax[Spec_BlackSpot_MAX];

	ST_UI_BlackSpot*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_BlackSpot* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
	void	Enable_Setting		(__in BOOL bEnable);
};

#endif // Wnd_Cfg_BlackSpot_h__
