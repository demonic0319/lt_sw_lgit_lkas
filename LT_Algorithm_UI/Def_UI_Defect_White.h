﻿#ifndef Def_UI_Defect_White_h__
#define Def_UI_Defect_White_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#define Defect_White_COUNT_MAX 500

#pragma pack(push,1)

typedef enum enDefect_White_Spec
{
	Spec_Defect_White_Count = 0,
	Spec_Defect_White_MAX,
};

typedef struct _tag_UI_Defect_White
{
	// Inspect
	ST_Inspect				stInspect;

	// PARAMETER

	int		nBlockSize;
	double	dbDefectRatio;
	int		nMaxSingleDefectNum;

	ST_Spec			stSpecMin[Spec_Defect_White_MAX];
	ST_Spec			stSpecMax[Spec_Defect_White_MAX];

	bool					b8BitUse;


	_tag_UI_Defect_White()
	{
		stInspect.Reset();

		nBlockSize = 0;
		dbDefectRatio = 0;
		nMaxSingleDefectNum = 0;
		b8BitUse = false;

		for (UINT nIdx = 0; nIdx < Spec_Defect_White_MAX; nIdx++)
		{
			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}

		
	}

	_tag_UI_Defect_White& operator= (_tag_UI_Defect_White& ref)
	{
		nBlockSize = ref.nBlockSize;
		dbDefectRatio = ref.dbDefectRatio;
		nMaxSingleDefectNum = ref.nMaxSingleDefectNum;
		b8BitUse = ref.b8BitUse;

		for (UINT nIdx = 0; nIdx < Spec_Defect_White_MAX; nIdx++)
		{

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_Defect_White, *PST_UI_Defect_White;

#pragma pack(pop)

#endif // Def_Defect_Black_h__

