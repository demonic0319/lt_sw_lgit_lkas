﻿#ifndef List_ShadingOp_h__
#define List_ShadingOp_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_UI_Shading.h"

typedef enum enListItemNum_ShadingOp
{
	ShaOp_ItemNum = ROI_Shading_Max,
};

//-----------------------------------------------------------------------------
// List_ShadingInfo
//-----------------------------------------------------------------------------
class CList_ShadingOp : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_ShadingOp)
	
public:
	CList_ShadingOp();
	virtual ~CList_ShadingOp();

protected:

	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow				(CREATESTRUCT& cs);

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel				(UINT nFlags, short zDelta, CPoint pt);


	afx_msg void	OnEnKillFocusEdit			();



	BOOL			UpdateCellData				(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData			(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;

	UINT				m_nEditCol;
	UINT				m_nEditRow;

	CMFCMaskedEdit		m_ed_CellEdit;
	CComboBox			m_cb_Overlay;

	UINT				m_nMaxROI	= 0;
	ST_Input_Shading*	m_pstConfig = NULL;

public:

	void SetMaxROI	(__in UINT nMaxROI)
	{
		m_nMaxROI = nMaxROI;
	};

	void SetPtr_ConfigInfo(__in ST_Input_Shading* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfig = pstConfigInfo;
	};

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();

};
#endif // List_ShadingInfo_h__
