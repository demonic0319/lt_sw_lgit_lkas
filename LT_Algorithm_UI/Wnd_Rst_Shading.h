#ifndef Wnd_Rst_Shading_h__
#define Wnd_Rst_Shading_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"

// CWnd_Rst_Shading
typedef enum
{
	enShading_Result_Field_1,
	enShading_Result_Field_2,
	enShading_Result_Field_1_Dev,
	enShading_Result_Field_2_Dev,
	enShading_Result_Max

}enShading_Result_Num;

class CWnd_Rst_Shading : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Shading)

public:
	CWnd_Rst_Shading();
	virtual ~CWnd_Rst_Shading();

	UINT m_nHeaderCnt = enShading_Result_Max;

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd(CDC* pDC);

	CVGStatic		m_st_Header[enShading_Result_Max];
	CVGStatic		m_st_Result[enShading_Result_Max];

public:

	void Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in  BOOL bReulst, __in  double dbValue);
	void Result_Reset();
};

#endif // Wnd_Rst_SFR_h__
