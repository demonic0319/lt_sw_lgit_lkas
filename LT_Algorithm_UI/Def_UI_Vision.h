﻿#ifndef Def_UI_Vision_h__
#define Def_UI_Vision_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_ROI_Vision
{
	Vision_Match,
	Vision_Max,
};

typedef struct _tag_UI_Vision
{
	UINT		nCycle;

	CString		szPatternName;
	double		dbOffset[Vision_Max]; 

	ST_Spec		stSpecMin[Vision_Max];
	ST_Spec		stSpecMax[Vision_Max];
	
	_tag_UI_Vision()
	{
		
		nCycle = 1;

		for (UINT nIdx = 0; nIdx < Vision_Max; nIdx++)
		{
			dbOffset[nIdx] = 1.0;

			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}
	};

	_tag_UI_Vision& operator= (_tag_UI_Vision& ref)
	{
		nCycle = ref.nCycle;
		szPatternName = ref.szPatternName;
		
		for (UINT nIdx = 0; nIdx < Vision_Max; nIdx++)
		{
			dbOffset[nIdx]	= ref.dbOffset[nIdx];

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_Vision, *PST_UI_Vision;

#pragma pack(pop)

#endif // Def_Current_h__
