﻿#ifndef Def_UI_IIC_h__
#define Def_UI_IIC_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_ROI_IIC
{
	IIC_Max = 1,
};

typedef struct _tag_UI_IIC
{
	CString		szPatternName;
	ST_Spec		stSpecMin[IIC_Max];
	ST_Spec		stSpecMax[IIC_Max];
	
	_tag_UI_IIC()
	{
		//nCycle = 1;

		for (UINT nIdx = 0; nIdx < IIC_Max; nIdx++)
		{
			//dbOffset[nIdx] = 1.0;

			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}
	};

	_tag_UI_IIC& operator= (_tag_UI_IIC& ref)
	{
		//nCycle = ref.nCycle;
		szPatternName = ref.szPatternName;
		
		for (UINT nIdx = 0; nIdx < IIC_Max; nIdx++)
		{
			//dbOffset[nIdx]	= ref.dbOffset[nIdx];

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_IIC, *PST_UI_IIC;

#pragma pack(pop)

#endif // Def_Current_h__
