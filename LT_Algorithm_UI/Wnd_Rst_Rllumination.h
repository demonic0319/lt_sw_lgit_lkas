#ifndef Wnd_Rst_Rllumination_h__
#define Wnd_Rst_Rllumination_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_Rllumination.h"

// CWnd_Rst_Rllumination
typedef enum
{
	enRI_Result_Corner = 0,
	enRI_Result_Min,
	enRI_Result_Max,

}enRI_Result;

static	LPCTSTR	g_szRI_Result[] =
{
	_T("Corner"),
	_T("Min"),
	NULL
};

class CWnd_Rst_Rllumination : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Rllumination)

public:
	CWnd_Rst_Rllumination();
	virtual ~CWnd_Rst_Rllumination();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enRI_Result_Max];
	CVGStatic	m_st_Result[enRI_Result_Max];

public:

	void Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_Ymean_h__
