#ifndef Wnd_Rst_Displace_h__
#define Wnd_Rst_Displace_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"

// CWnd_Rst_Displace
typedef enum
{
// 	enDisplace_Result_A,
// 	enDisplace_Result_B,
//  	enDisplace_Result_C,
// 	enDisplace_Result_D,
	enDisplace_Result_ModifyTx,
	enDisplace_Result_ModifyTy,
	enDisplace_Result_AATx,
	enDisplace_Result_AATy,
	enDisplace_Result_Tx,
	enDisplace_Result_Ty,
	enDisplace_Result_Max,

}enDisplace_Result_Num;

class CWnd_Rst_Displace : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Displace)

public:
	CWnd_Rst_Displace();
	virtual ~CWnd_Rst_Displace();

	UINT m_nHeaderCnt = enDisplace_Result_Max;
	
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic		m_st_Header[enDisplace_Result_Max];
	CVGStatic		m_st_Result[enDisplace_Result_Max];

public:

	void Result_Display			(__in UINT nResultIdx, __in UINT nIdx, __in  BOOL bReulst, __in  double dbValue);
	void Result_Reset			();
};

#endif // Wnd_Rst_SFR_h__
