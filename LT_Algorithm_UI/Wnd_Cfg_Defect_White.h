﻿#ifndef Wnd_Cfg_Defect_White_h__
#define Wnd_Cfg_Defect_White_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Defect_White.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Defect_White
//-----------------------------------------------------------------------------
class CWnd_Cfg_Defect_White : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Defect_White)

public:
	CWnd_Cfg_Defect_White();
	virtual ~CWnd_Cfg_Defect_White();

	enum enDefect_WhiteStatic
	{
		STI_DW_INSPECT = 0,
		STI_DW_SPEC,
		STI_DW_PARAMETER,
		STI_DW_DataForamt,
		STI_DW_OutMode,
		STI_DW_SensorType,
		STI_DW_BlackLevel, //공통
		STI_DW_BlockSize,
		STI_DW_DefectRatio,
		STI_DW_MaxSingleDefectNum,
		STI_DW_DefectCount,
		STI_DW_8bitUse,
		STI_DW_MAX,
	};

	enum enDefect_WhiteButton
	{
		BTN_DW_MAX = 1,
	};

	enum enDefect_WhiteComobox
	{
		CMB_DW_DataForamt = 0,
		CMB_DW_OutMode,
		CMB_DW_SensorType,
		CMB_DW_EnableCircle,
		CMB_DW_8bitUse,
		CMB_DW_MAX,
	};

	enum enDefect_WhiteEdit
	{
		EDT_DW_BlackLevel = 0,
		EDT_DW_BlockSize,
		EDT_DW_DefectRatio,
		EDT_DW_MaxSingleDefectNum,
		EDT_DW_MAX,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_DW_MAX];
	CButton						m_bn_Item[BTN_DW_MAX];
	CComboBox					m_cb_Item[CMB_DW_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_DW_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_Defect_White_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_Defect_White_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_Defect_White_MAX];
	CMFCButton					m_chk_SpecMin[Spec_Defect_White_MAX];
	CMFCButton					m_chk_SpecMax[Spec_Defect_White_MAX];

	ST_UI_Defect_White*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Defect_White* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Defect_White_h__
