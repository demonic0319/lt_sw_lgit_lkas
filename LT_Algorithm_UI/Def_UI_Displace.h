﻿#ifndef Def_UI_Displace_h__
#define Def_UI_Displace_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_ROI_Displace
{
	Displace_A = 0,
	Displace_B,
// 	Displace_C,
// 	Displace_D,
	Displace_Max,
};

typedef struct _tag_UI_Displace
{
	UINT		nCycle;

	double		dbOffset[Displace_Max];

	ST_Spec		stSpecMin[Displace_Max];
	ST_Spec		stSpecMax[Displace_Max];
	
	_tag_UI_Displace()
	{
		nCycle = 1;

		for (UINT nIdx = 0; nIdx < Displace_Max; nIdx++)
		{
			dbOffset[nIdx] = 1.0;

			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}
	};

	_tag_UI_Displace& operator= (_tag_UI_Displace& ref)
	{
		nCycle = ref.nCycle;
		
		for (UINT nIdx = 0; nIdx < Displace_Max; nIdx++)
		{
			dbOffset[nIdx]	= ref.dbOffset[nIdx];

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_Displace, *PST_UI_Displace;

#pragma pack(pop)

#endif // Def_Current_h__
