﻿#ifndef Wnd_Cfg_LCB_h__
#define Wnd_Cfg_LCB_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_LCB.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_LCB
//-----------------------------------------------------------------------------
class CWnd_Cfg_LCB : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_LCB)

public:
	CWnd_Cfg_LCB();
	virtual ~CWnd_Cfg_LCB();

	enum enLCBStatic
	{
		STI_LCB_INSPECT = 0,
		STI_LCB_SPEC,
		STI_LCB_PARAM,
		STI_LCB_DataForamt,
		STI_LCB_OutMode,
		STI_LCB_SensorType,
		STI_LCB_BlackLevel, //공통
		STI_LCB_CenterThreshold,
		STI_LCB_EdgeThreshold,
		STI_LCB_CornerThreshold,
		STI_LCB_MaxSingleDefectNum,
		STI_LCB_MinDefectWidthHeight,
		STI_LCB_EnableCircle,
		STI_LCB_PosOffsetX,
		STI_LCB_PosOffsetY,
		STI_LCB_RadiusRationX,
		STI_LCB_RadiusRationY,
		STI_LCB_Count,
		STI_LCB_8bitUse,
		STI_LCB_MAX,
	};

	enum enLCBButton
	{
		BTN_LCB_MAX = 1,
	};

	enum enLCBComobox
	{
		CMB_LCB_DataForamt = 0,
		CMB_LCB_OutMode,
		CMB_LCB_SensorType,
		CMB_LCB_EnableCircle,
		CMB_LCB_8bitUse,
		CMB_LCB_MAX,
	};

	enum enLCBEdit
	{
		EDT_LCB_BlackLevel = 0,
		EDT_LCB_CenterThreshold,
		EDT_LCB_EdgeThreshold,
		EDT_LCB_CornerThreshold,
		EDT_LCB_MaxSingleDefectNum,
		EDT_LCB_MinDefectWidthHeight,
		EDT_LCB_PosOffsetX,
		EDT_LCB_PosOffsetY,
		EDT_LCB_RadiusRationX,
		EDT_LCB_RadiusRationY,
		EDT_LCB_Count,
		EDT_LCB_MAX,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_LCB_MAX];
	CButton						m_bn_Item[BTN_LCB_MAX];
	CComboBox					m_cb_Item[CMB_LCB_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_LCB_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_LCB_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_LCB_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_LCB_MAX];
	CMFCButton					m_chk_SpecMin[Spec_LCB_MAX];
	CMFCButton					m_chk_SpecMax[Spec_LCB_MAX];

	ST_UI_LCB*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_LCB* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
	void	Enable_Setting		(__in BOOL bEnable);
};

#endif // Wnd_Cfg_LCB_h__
