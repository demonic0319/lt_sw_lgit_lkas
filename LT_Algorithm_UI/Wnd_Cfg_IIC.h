﻿#ifndef Wnd_Cfg_IIC_h__
#define Wnd_Cfg_IIC_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_IIC.h"
#include "File_WatchList.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_IIC
//-----------------------------------------------------------------------------
class CWnd_Cfg_IIC : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_IIC)

public:
	CWnd_Cfg_IIC();
	virtual ~CWnd_Cfg_IIC();

	enum enIICStatic
	{
		STI_IIC_PARAM = 0,
		STI_IIC_PATTERN,
		STI_IIC_MAX,
	};

	enum enIICButton
	{
		BTN_IIC_MAX = 1,
	};

	enum enIICComobox
	{
		CMB_IIC_PATTERN,
		CMB_IIC_MAX
	};

	enum enIICEdit
	{
		EDT_IIC_MATCH_RATE = 0,
		EDT_IIC_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CVGStatic			m_st_Item[STI_IIC_MAX];
	CButton				m_bn_Item[BTN_IIC_MAX];
	CComboBox			m_cb_Item[CMB_IIC_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_IIC_MAX];

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Spec[IIC_Max];
	CMFCMaskedEdit		m_ed_SpecMin[IIC_Max];
	CMFCMaskedEdit		m_ed_SpecMax[IIC_Max];
	CMFCButton			m_chk_SpecMin[IIC_Max];
	CMFCButton			m_chk_SpecMax[IIC_Max];

	ST_UI_IIC*		m_pstConfig		= NULL;

	CString			m_szI2CImgFolderPath;

	CFile_WatchList	m_IniWatch;
	void		RefreshFileList(__in const CStringList* pFileList);

public:

	void	SetPtr_RecipeInfo(ST_UI_IIC* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	// iic ini file path
	void SetIICImageFolderPath(__in LPCTSTR szPath)
	{
		m_szI2CImgFolderPath = szPath;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_IIC_h__
