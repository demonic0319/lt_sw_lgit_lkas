﻿// List_DynamicBW.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_DynamicBW.h"

typedef enum enListNum_DynamicOp
{
	DyOp_Object = 0,
	DyOp_PosX,
	DyOp_PosY,
	DyOp_Color,
	DyOp_Overlay,
	DyOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_DynamicOp[] =
{
	_T(""),						// DyOp_Object = 0,
	_T("Center X"),				// DyOp_PosX,
	_T("Center Y"),				// DyOp_PosY,
	_T("Image Color"),			// DyOp_Color,
	_T("OverlayDir"),			// DyOp_Overlay,
	NULL
};

const int	iListAglin_DynamicOp[] =
{
	LVCFMT_LEFT,				// DyOp_Object = 0,	
	LVCFMT_CENTER,				// DyOp_PosX,
	LVCFMT_CENTER,				// DyOp_PosY,
	LVCFMT_CENTER,				// DyOp_Color,
	LVCFMT_CENTER,				// DyOp_Overlay,
};

const int	iHeaderWidth_DynamicOp[] =
{
	60,							// DyOp_Object = 0,	
	100,							// DyOp_PosX,
	100,							// DyOp_PosY,
	150,						// DyOp_Color,
	100,							// DyOp_Overlay,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_COLOR		2000
#define IDC_COM_CELLCOMBO_OVERLAY	2001

// CList_DynamicBW

IMPLEMENT_DYNAMIC(CList_DynamicBW, CList_Cfg_VIBase)

CList_DynamicBW::CList_DynamicBW()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_DynamicBW::~CList_DynamicBW()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_DynamicBW, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_DynamicBW::OnNMClick					)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_DynamicBW::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_DynamicBW::OnEnKillFocusEdit			)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_COLOR,	&CList_DynamicBW::OnEnKillFocusComboColor	)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_COLOR,	&CList_DynamicBW::OnEnSelectComboColor		)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_OVERLAY,	&CList_DynamicBW::OnEnKillFocusComboOverlay	)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_OVERLAY,	&CList_DynamicBW::OnEnSelectComboOverlay	)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_DynamicBW 메시지 처리기입니다.
int CList_DynamicBW::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_ed_CellEdit.Create(WS_CHILD | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_EDT_CELLEDIT);
	m_ed_CellEdit.SetValidChars(_T("-.0123456789"));

	m_cb_Color.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_COLOR);
	m_cb_Color.ResetContent();

	for (UINT nIdx = 0; NULL != g_szSNRBWType_Static[nIdx]; nIdx++)
	{
		m_cb_Color.InsertString(nIdx, g_szSNRBWType_Static[nIdx]);
	}

	m_cb_Overlay.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_OVERLAY);
	m_cb_Overlay.ResetContent();

	for (UINT nIdex = 0; NULL != g_szOverayDir[nIdex]; nIdex++)
	{
		m_cb_Overlay.InsertString(nIdex, g_szOverayDir[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_DynamicBW::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_DynamicBW::InitHeader()
{
	for (int nCol = 0; nCol < DyOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_DynamicOp[nCol], iListAglin_DynamicOp[nCol], iHeaderWidth_DynamicOp[nCol]);
	}

	for (int nCol = 0; nCol < DyOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_DynamicOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_DynamicBW::InsertFullData()
{
 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < DyOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_DynamicBW::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	szValue.Format(_T("%d"), nRow);
	SetItemText(nRow, DyOp_Object, szValue);

	szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].ptROI.x);
	SetItemText(nRow, DyOp_PosX, szValue);

	szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].ptROI.y);
	SetItemText(nRow, DyOp_PosY, szValue);

	szValue.Format(_T("%s"), g_szSNRBWType_Static[m_pstConfig->stInput[nRow].eSNRType]);
	SetItemText(nRow, DyOp_Color, szValue);

	szValue.Format(_T("%s"), g_szOverayDir[m_pstConfig->stInput[nRow].nOverlayDir]);
	SetItemText(nRow, DyOp_Overlay, szValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = TRUE;
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem == DyOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = FALSE;

				if (nBuffer == 0x1000)
					m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < DyOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == DyOp_Color)
			{
				m_cb_Color.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Color.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Color.SetFocus();
				m_cb_Color.SetCurSel(m_pstConfig->stInput[m_nEditRow].eSNRType);
			}
			else if (pNMItemActivate->iSubItem == DyOp_Overlay)
			{
				m_cb_Overlay.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Overlay.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Overlay.SetFocus();
				m_cb_Overlay.SetCurSel(m_pstConfig->stInput[m_nEditRow].nOverlayDir);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();

				GetOwner()->SendNotifyMessage(m_wn_Change, 0, 0);
			}
		}
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);



	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));


	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_DynamicBW::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if ( NULL == m_pstConfig)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	switch (nCol)
	{
	case DyOp_PosX:
		m_pstConfig->stInput[nRow].ptROI.x = iValue;
		break;
	case DyOp_PosY:
		m_pstConfig->stInput[nRow].ptROI.y = iValue;
		break;
	default:
		break;
	}

	CString szValue;

	switch (nCol)
	{
	case DyOp_PosX:
		szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].ptROI.x);
		break;
	case DyOp_PosY:
		szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].ptROI.y);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(szValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_DynamicBW::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_DynamicBW::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_DynamicBW::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);

		
		
		if (0 < zDelta)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < iValue)
				iValue = iValue + ((zDelta / 120));

			if (0 < dbValue)
				dbValue = dbValue + ((zDelta / 120) * 0.01);
		}

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;
		


		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnEnKillFocusComboColor()
{
	SetItemText(m_nEditRow, DyOp_Color, g_szSNRBWType_Static[m_pstConfig->stInput[m_nEditRow].eSNRType]);
	m_cb_Color.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnEnSelectComboColor()
{
	m_pstConfig->stInput[m_nEditRow].eSNRType = (enSNRBWType)m_cb_Color.GetCurSel();

	SetItemText(m_nEditRow, DyOp_Color, g_szSNRBWType_Static[m_pstConfig->stInput[m_nEditRow].eSNRType]);
	m_cb_Color.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusComboOverlay
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/8 - 16:05
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnEnKillFocusComboOverlay()
{
	SetItemText(m_nEditRow, DyOp_Overlay, g_szOverayDir[m_pstConfig->stInput[m_nEditRow].nOverlayDir]);
	m_cb_Overlay.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboOverlay
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/8 - 16:05
// Desc.		:
//=============================================================================
void CList_DynamicBW::OnEnSelectComboOverlay()
{
	m_pstConfig->stInput[m_nEditRow].nOverlayDir = m_cb_Overlay.GetCurSel();

	SetItemText(m_nEditRow, DyOp_Overlay, g_szOverayDir[m_pstConfig->stInput[m_nEditRow].nOverlayDir]);
	m_cb_Overlay.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}
