﻿#ifndef Wnd_Cfg_Displace_h__
#define Wnd_Cfg_Displace_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Displace.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Displace
//-----------------------------------------------------------------------------
class CWnd_Cfg_Displace : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Displace)

public:
	CWnd_Cfg_Displace();
	virtual ~CWnd_Cfg_Displace();

	enum enDisplaceStatic
	{
		STI_DIP_PARAM = 0,
		STI_DIP_SPEC,
		STI_DIP_OFFSET_POS_A,
		STI_DIP_OFFSET_POS_B,
// 		STI_DIP_OFFSET_POS_C,
// 		STI_DIP_OFFSET_POS_D,
		STI_DIP_MAX,
	};

	enum enDisplaceButton
	{
		BTN_DIP_MAX = 1,
	};

	enum enDisplaceComobox
	{
		CMB_DIP_MAX = 1,
	};

	enum enDisplaceEdit
	{
		EDT_DIP_OFFSET_POS_A = 0,
		EDT_DIP_OFFSET_POS_B,
// 		EDT_DIP_OFFSET_POS_C,
// 		EDT_DIP_OFFSET_POS_D,
		EDT_DIP_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CVGStatic			m_st_Item[STI_DIP_MAX];
	CButton				m_bn_Item[BTN_DIP_MAX];
	CComboBox			m_cb_Item[CMB_DIP_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_DIP_MAX];

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Spec[Displace_Max];
	CMFCMaskedEdit		m_ed_SpecMin[Displace_Max];
	CMFCMaskedEdit		m_ed_SpecMax[Displace_Max];
	CMFCButton			m_chk_SpecMin[Displace_Max];
	CMFCButton			m_chk_SpecMax[Displace_Max];

	ST_UI_Displace*		m_pstConfig		= NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Displace* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Displace_h__
