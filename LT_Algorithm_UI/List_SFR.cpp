﻿// List_SFR.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_SFR.h"

// CList_SFR

typedef enum enListNum_SFR
{
	SfOp_Object = 0,
	SfOp_PosX,
	SfOp_PosY,
	SfOp_Width,
	SfOp_Height,
	SfOp_EdgeDir,
	SfOp_FLists,
	SfOp_SFR,
	SfOp_Offset,
	SfOp_MinSpecUse,
	SfOp_MinSpec,
	SfOp_MaxSpecUse,
	SfOp_MaxSpec,
	SfOp_Overlay,
	SfOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_SFR[] =
{
	_T(""),						// SfOp_Object = 0,
	_T("Center X"),				// SfOp_PosX,
	_T("Center Y"),				// SfOp_PosY,
	_T("Width"),				// SfOp_Width,
	_T("Height"),				// SfOp_Height,
	_T("EdgeDir"),				// SfOp_EdgeDir,
	_T("LinePair"),				// Linepare to Frequency 
	_T("SFR"),					// SfOp_SFR,
	_T("Offset"),				// SfOp_Offset,
	_T(""),						// SfOp_MinSpecUse,
	_T("MinSpec"),				// SfOp_MinSpec,
	_T(""),						// SfOp_MaxSpecUse,
	_T("MaxSpec"),				// SfOp_MaxSpec,
	_T("OverlayDir"),			// SfOp_Overlay,
	NULL
};

const int	iListAglin_SFR[] =
{
	LVCFMT_LEFT,				// SfOp_Object = 0,
	LVCFMT_CENTER,				// SfOp_PosX,
	LVCFMT_CENTER,				// SfOp_PosY,
	LVCFMT_CENTER,				// SfOp_Width,
	LVCFMT_CENTER,				// SfOp_Height,
	LVCFMT_CENTER,				// SfOp_EdgeDir,
	LVCFMT_CENTER,				// SfOp_FLists,
	LVCFMT_CENTER,				// SfOp_SFR,
	LVCFMT_CENTER,				// SfOp_Offset,
	LVCFMT_CENTER,				// SfOp_MinSpecUse,
	LVCFMT_CENTER,				// SfOp_MinSpec,
	LVCFMT_CENTER,				// SfOp_MaxSpecUse,
	LVCFMT_CENTER,				// SfOp_MaxSpec,
	LVCFMT_CENTER,				// SfOp_Overlay,
};

const int	iHeaderWidth_SFR[] =
{
	 60,						// SfOp_Object = 0,
	 80,						// SfOp_PosX,
	 80,						// SfOp_PosY,
	 80,						// SfOp_Width,
	 80,						// SfOp_Height,
	 80,						// SfOp_EdgeDir,
	 90,						// SfOp_FLists,
	 90,						// SfOp_SFR,
	 85,						// SfOp_Offset,
	 25,						// SfOp_MinSpecUse,
	 85,						// SfOp_MinSpec,
	 25,						// SfOp_MaxSpecUse,
	 80,						// SfOp_MaxSpec,
	 80,						// SfOp_Overlay,
};

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_OVERLAY	2001

IMPLEMENT_DYNAMIC(CList_SFR, CList_Cfg_VIBase)

CList_SFR::CList_SFR()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_SFR::~CList_SFR()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_SFR, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_SFR::OnNMClick					)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_SFR::OnNMDblclk					)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_SFR::OnEnKillFocusEdit			)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_TYPE,	&CList_SFR::OnEnKillFocusComboType		)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_TYPE,	&CList_SFR::OnEnSelectComboType			)	
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_OVERLAY,	&CList_SFR::OnEnKillFocusComboOverlay	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_OVERLAY,	&CList_SFR::OnEnSelectComboOverlay		)	
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_SFR 메시지 처리기입니다.
int CList_SFR::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER | LVS_EX_CHECKBOXES);

	InitHeader(); 
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_ed_CellEdit.Create(WS_CHILD | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_EDT_CELLEDIT);
	m_ed_CellEdit.SetValidChars(_T("-.0123456789"));

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szEdgeDir[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szEdgeDir[nIdex]);
	}

	m_cb_Overlay.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_OVERLAY);
	m_cb_Overlay.ResetContent();

	for (UINT nIdex = 0; NULL != g_szOverayDir[nIdex]; nIdex++)
	{
		m_cb_Overlay.InsertString(nIdex, g_szOverayDir[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFR::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFR::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFR::InitHeader()
{
	for (int nCol = 0; nCol < SfOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_SFR[nCol], iListAglin_SFR[nCol], iHeaderWidth_SFR[nCol]);
	}

	for (int nCol = 0; nCol < SfOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_SFR[nCol]);
	}
}

//=============================================================================
// Method		: DeleteHeader
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2018/8/23 - 19:40
// Desc.		:
//=============================================================================
void CList_SFR::DeleteHeader(UINT nRow)
{
	for (int nCol = 0; nCol < SfOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_SFR[nCol]);
	}

	SetColumnWidth(nRow, 0);
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_SFR::InsertFullData()
{
	if ( NULL == m_pstConfig)
		return;

 	DeleteAllItems();
 
	for (UINT nIdx = 0; nIdx < SfOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_SFR::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	szValue.Format(_T("%d"), nRow);
	SetItemText(nRow, SfOp_Object, szValue);

	if (m_pstConfig->stInput[nRow].bEnable == TRUE)
		SetItemState(nRow, 0x2000, LVIS_STATEIMAGEMASK);
	else
		SetItemState(nRow, 0x1000, LVIS_STATEIMAGEMASK);

	szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.CenterPoint().x);
	SetItemText(nRow, SfOp_PosX, szValue);

	szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.CenterPoint().y);
	SetItemText(nRow, SfOp_PosY, szValue);

	szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.Width());
	SetItemText(nRow, SfOp_Width, szValue);

	szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.Height());
	SetItemText(nRow, SfOp_Height, szValue);
	
	szValue.Format(_T("%s"), g_szEdgeDir[m_pstConfig->stInput[nRow].iEdgeDir]);
	SetItemText(nRow, SfOp_EdgeDir, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->stInput[nRow].dbFrequencyLists);
	SetItemText(nRow, SfOp_FLists, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->stInput[nRow].dbSFR);
	SetItemText(nRow, SfOp_SFR, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->stInput[nRow].dbOffset);
	SetItemText(nRow, SfOp_Offset, szValue);

	szValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfig->stInput[nRow].stSpecMin.bEnable]);
	SetItemText(nRow, SfOp_MinSpecUse, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->stInput[nRow].stSpecMin.dbValue);
	SetItemText(nRow, SfOp_MinSpec, szValue);

	szValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfig->stInput[nRow].stSpecMax.bEnable]);
	SetItemText(nRow, SfOp_MaxSpecUse, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig->stInput[nRow].stSpecMax.dbValue);
	SetItemText(nRow, SfOp_MaxSpec, szValue);

	szValue.Format(_T("%s"), g_szOverayDir[m_pstConfig->stInput[nRow].nOverlayDir]);
	SetItemText(nRow, SfOp_Overlay, szValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_SFR::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LVHITTESTINFO HitInfo;
	HitInfo.pt = pNMItemActivate->ptAction;

	HitTest(&HitInfo);

	// Check Ctrl Event
	if (HitInfo.flags == LVHT_ONITEMSTATEICON)
	{
		UINT nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

		if (nBuffer == 0x2000)
			m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = FALSE;

		if (nBuffer == 0x1000)
			m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = TRUE;

		GetOwner()->SendNotifyMessage(m_wn_Change, 0, 0);
	}

	if (SfOp_MinSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfig->stInput[pNMItemActivate->iItem].stSpecMin.bEnable = !m_pstConfig->stInput[pNMItemActivate->iItem].stSpecMin.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfig->stInput[pNMItemActivate->iItem].stSpecMin.bEnable]);
	}

	if (SfOp_MaxSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfig->stInput[pNMItemActivate->iItem].stSpecMax.bEnable = !m_pstConfig->stInput[pNMItemActivate->iItem].stSpecMax.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfig->stInput[pNMItemActivate->iItem].stSpecMax.bEnable]);
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_SFR::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (SfOp_MinSpecUse == pNMItemActivate->iSubItem || SfOp_MaxSpecUse == pNMItemActivate->iSubItem)
		{
			*pResult = 1;
			return;
		}

		if (pNMItemActivate->iSubItem == SfOp_Object)
		{
			LVHITTESTINFO HitInfo;
			HitInfo.pt = pNMItemActivate->ptAction;

			HitTest(&HitInfo);

			// Check Ctrl Event
			if (HitInfo.flags == LVHT_ONITEMSTATEICON)
			{
				UINT nBuffer;

				nBuffer = GetItemState(pNMItemActivate->iItem, LVIS_STATEIMAGEMASK);

				if (nBuffer == 0x2000)
					m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = FALSE;

				if (nBuffer == 0x1000)
					m_pstConfig->stInput[pNMItemActivate->iItem].bEnable = TRUE;
			}
		}

		if (pNMItemActivate->iSubItem < SfOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == SfOp_EdgeDir)
			{
				m_cb_Type.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Type.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Type.SetFocus();
				m_cb_Type.SetCurSel(m_pstConfig->stInput[m_nEditRow].iEdgeDir);
			}
			else if (pNMItemActivate->iSubItem == SfOp_Overlay)
			{
				m_cb_Overlay.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Overlay.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Overlay.SetFocus();
				m_cb_Overlay.SetCurSel(m_pstConfig->stInput[m_nEditRow].nOverlayDir);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
	}
	GetOwner()->SendNotifyMessage(m_wn_Change, 0, 0);

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_SFR::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == SfOp_FLists || m_nEditCol == SfOp_SFR || m_nEditCol == SfOp_Offset || m_nEditCol == SfOp_MinSpec || m_nEditCol == SfOp_MaxSpec)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_SFR::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if ( NULL == m_pstConfig)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfig->stInput[nRow].rtROI;

	switch (nCol)
	{
	case SfOp_PosX:
		m_pstConfig->stInput[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case SfOp_PosY:
		m_pstConfig->stInput[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case SfOp_Width:
		m_pstConfig->stInput[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case SfOp_Height:
		m_pstConfig->stInput[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	default:
		break;
	}
	
	if (m_pstConfig->stInput[nRow].rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfig->stInput[nRow].rtROI;

		m_pstConfig->stInput[nRow].rtROI.left	= 0;
		m_pstConfig->stInput[nRow].rtROI.right	= m_pstConfig->stInput[nRow].rtROI.left + rtTemp.Width();
	}

	if (m_pstConfig->stInput[nRow].rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfig->stInput[nRow].rtROI;

		m_pstConfig->stInput[nRow].rtROI.right	= (LONG)m_dwImage_Width;
		m_pstConfig->stInput[nRow].rtROI.left	= m_pstConfig->stInput[nRow].rtROI.right - rtTemp.Width();
	}

	if (m_pstConfig->stInput[nRow].rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfig->stInput[nRow].rtROI;

		m_pstConfig->stInput[nRow].rtROI.top	= 0;
		m_pstConfig->stInput[nRow].rtROI.bottom = rtTemp.Height() + m_pstConfig->stInput[nRow].rtROI.top;
	}

	if (m_pstConfig->stInput[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfig->stInput[nRow].rtROI;

		m_pstConfig->stInput[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfig->stInput[nRow].rtROI.top	= m_pstConfig->stInput[nRow].rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfig->stInput[nRow].rtROI.Height() <= 0)
		m_pstConfig->stInput[nRow].RectPosWH(m_pstConfig->stInput[nRow].rtROI.Width(), 1);

	if (m_pstConfig->stInput[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfig->stInput[nRow].RectPosWH(m_pstConfig->stInput[nRow].rtROI.Width(), m_dwImage_Height);

	if (m_pstConfig->stInput[nRow].rtROI.Width() <= 0)
		m_pstConfig->stInput[nRow].RectPosWH(1, m_pstConfig->stInput[nRow].rtROI.Height());

	if (m_pstConfig->stInput[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfig->stInput[nRow].RectPosWH(m_dwImage_Width, m_pstConfig->stInput[nRow].rtROI.Height());

	CString szValue;

	switch (nCol)
	{
	case SfOp_PosX:
		szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.CenterPoint().x);
		break;
	case SfOp_PosY:
		szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.CenterPoint().y);
		break;
	case SfOp_Width:
		szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.Width());
		break;
	case SfOp_Height:
		szValue.Format(_T("%d"), m_pstConfig->stInput[nRow].rtROI.Height());
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(szValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCelldbData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFR::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case SfOp_FLists:
		m_pstConfig->stInput[nRow].dbFrequencyLists = dbValue;
		break;
	case SfOp_SFR:
		m_pstConfig->stInput[nRow].dbSFR = dbValue;
		break;
	case SfOp_Offset:
		m_pstConfig->stInput[nRow].dbOffset = dbValue;
		break;
	case SfOp_MinSpec:
		m_pstConfig->stInput[nRow].stSpecMin.dbValue = dbValue;
		break;
	case SfOp_MaxSpec:
		m_pstConfig->stInput[nRow].stSpecMax.dbValue = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_SFR::GetCellData()
{
	if ( NULL == m_pstConfig)
		return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_SFR::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue	= _ttof(strText);

		if (m_nEditCol == SfOp_FLists || m_nEditCol == SfOp_SFR || m_nEditCol == SfOp_Offset || m_nEditCol == SfOp_MinSpec || m_nEditCol == SfOp_MaxSpec)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < zDelta)
			{
				iValue = iValue + ((zDelta / 120));
				dbValue = dbValue + ((zDelta / 120) * 0.01);
			}
			else
			{
				if (0 < iValue)
					iValue = iValue + ((zDelta / 120));

				if (0 < dbValue)
					dbValue = dbValue + ((zDelta / 120) * 0.01);
			}

			if (iValue < 0)
				iValue = 0;

			if (iValue > 2000)
				iValue = 2000;

			if (dbValue < 0.0)
				dbValue = 0.0;

		}

		if (m_nEditCol == SfOp_FLists || m_nEditCol == SfOp_SFR || m_nEditCol == SfOp_Offset || m_nEditCol == SfOp_MinSpec || m_nEditCol == SfOp_MaxSpec)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/3 - 16:11
// Desc.		:
//=============================================================================
void CList_SFR::OnEnKillFocusComboType()
{
	SetItemText(m_nEditRow, SfOp_EdgeDir, g_szEdgeDir[m_pstConfig->stInput[m_nEditRow].iEdgeDir]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/6/3 - 16:11
// Desc.		:
//=============================================================================
void CList_SFR::OnEnSelectComboType()
{
	m_pstConfig->stInput[m_nEditRow].iEdgeDir = m_cb_Type.GetCurSel();

	SetItemText(m_nEditRow, SfOp_EdgeDir, g_szEdgeDir[m_pstConfig->stInput[m_nEditRow].iEdgeDir]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusComboOverlay
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/8 - 14:14
// Desc.		:
//=============================================================================
void CList_SFR::OnEnKillFocusComboOverlay()
{
	SetItemText(m_nEditRow, SfOp_Overlay, g_szOverayDir[m_pstConfig->stInput[m_nEditRow].nOverlayDir]);
	m_cb_Overlay.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboOverlay
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/8 - 14:14
// Desc.		:
//=============================================================================
void CList_SFR::OnEnSelectComboOverlay()
{
	m_pstConfig->stInput[m_nEditRow].nOverlayDir = m_cb_Overlay.GetCurSel();

	SetItemText(m_nEditRow, SfOp_Overlay, g_szOverayDir[m_pstConfig->stInput[m_nEditRow].nOverlayDir]);
	m_cb_Overlay.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}
