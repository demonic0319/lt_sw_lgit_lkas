﻿#ifndef Wnd_Cfg_Rllumination_h__
#define Wnd_Cfg_Rllumination_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Rllumination.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Rllumination
//-----------------------------------------------------------------------------
class CWnd_Cfg_Rllumination : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Rllumination)

public:
	CWnd_Cfg_Rllumination();
	virtual ~CWnd_Cfg_Rllumination();

	enum enRlluminationStatic
	{
		STI_RI_INSPECT = 0,
		STI_RI_PARAM,
		STI_RI_SPEC,
		STI_RI_DataForamt,
		STI_RI_OutMode,
		STI_RI_SensorType,
		STI_RI_BlackLevel,
		STI_RI_8bitUse,
		STI_RI_Corner,
		STI_RI_Min,
		STI_RI_BoxSize,
		STI_RI_BoxField,
		STI_RI_Corneroffset,
		STI_RI_Minoffset,
		STI_RI_MAX,
	};

	enum enRlluminationButton
	{
		BTN_RI_MAX = 1,
	};

	enum enRlluminationComobox
	{
		CMB_RI_DataForamt = 0,
		CMB_RI_OutMode,
		CMB_RI_SensorType,
		CMB_RI_8bitUse,
		CMB_RI_MAX,
	};

	enum enRlluminationEdit
	{
		EDT_RI_BlackLevel = 0,
		EDT_RI_BoxSize,
		EDT_RI_BoxField,
		EDT_RI_Corneroffset,
		EDT_RI_Minoffset,
		EDT_RI_MAX,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CVGStatic					m_st_Item[STI_RI_MAX];
	CButton						m_bn_Item[BTN_RI_MAX];
	CComboBox					m_cb_Item[CMB_RI_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_RI_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_RI_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_RI_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_RI_MAX];
	CMFCButton					m_chk_SpecMin[Spec_RI_MAX];
	CMFCButton					m_chk_SpecMax[Spec_RI_MAX];

	ST_UI_Rllumination*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Rllumination* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Rllumination_h__
