﻿#ifndef Def_UI_Chart_h__
#define Def_UI_Chart_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef struct _tag_UI_Chart
{
	CRect	rtROI;
	
	UINT	nMarkColor;
	UINT	nBrightness;

	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtROI;

		rtROI.left	 = iPosX - (int)(rtTemp.Width() / 2);
		rtROI.right  = rtROI.left + rtTemp.Width();
		rtROI.top	 = iPosY - (int)(rtTemp.Height() / 2);
		rtROI.bottom = rtROI.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtROI;

		rtROI.left   = rtTemp.CenterPoint().x - (iWidth / 2);
		rtROI.right  = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtROI.top	 = rtTemp.CenterPoint().y - (iHeight / 2);
		rtROI.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};

	_tag_UI_Chart()
	{
		Reset();
	};

	void Reset()
	{
		rtROI.SetRectEmpty();

		nMarkColor  = MCol_Black;
		nBrightness = 30;
	};

	_tag_UI_Chart& operator= (_tag_UI_Chart& ref)
	{
		rtROI			= ref.rtROI;
		nMarkColor		= ref.nMarkColor;
		nBrightness		= ref.nBrightness;

		return *this;
	};

}ST_UI_Chart, *PST_UI_Chart;

#pragma pack(pop)

#endif // Def_Chart_h__
