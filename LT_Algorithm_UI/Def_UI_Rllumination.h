﻿#ifndef Def_UI_Rllumination_h__
#define Def_UI_Rllumination_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_ROI_Rllumination
{
	ROI_Rllumination_FST  = 0,
	ROI_Rllumination_LST  = 4,
	ROI_Rllumination_Max,
};

typedef enum enRI_Spec
{
	Spec_RI_Coner = 0,
	Spec_RI_Min,
	Spec_RI_MAX,
};


typedef struct _tag_Input_Rllumination
{
	CPoint		ptROI;

	UINT		nOverlayDir;
	double		dbOffset;
	double		dbThreshold;
	
	ST_Spec		stSpecMin;
	ST_Spec		stSpecMax;

	_tag_Input_Rllumination()
	{
		Reset();
	};

	void Reset()
	{
		ptROI.x = 0;
		ptROI.y = 0;

		nOverlayDir			= OverlayDir_Top;
		dbOffset			= 0.0;
		dbThreshold			= 0.0;
		stSpecMin.Reset();
		stSpecMax.Reset();
	};

	_tag_Input_Rllumination& operator= (_tag_Input_Rllumination& ref)
	{
		ptROI			= ref.ptROI;
		
		nOverlayDir		= ref.nOverlayDir;
		dbOffset		= ref.dbOffset;
		dbThreshold		= ref.dbThreshold;

		stSpecMin		= ref.stSpecMin;
		stSpecMax		= ref.stSpecMax;
		
		return *this; 
	};

}ST_Input_Rllumination, *PST_Input_Rllumination;

typedef struct _tag_UI_Rllumination
{
	// Inspect
	ST_Inspect			stInspect;


	
	double dBoxField;
	UINT nBoxSize;
	double dSpecRIcornerMin;
	double dSpecRIcornerMax;
	double dSpecRIminMin;
	double dSpecRIminMax;
	bool	b8BitUse;

	double dbCorneroffset;
	double dbMinoffset;

	ST_Spec			stSpecMin[Spec_RI_MAX];
	ST_Spec			stSpecMax[Spec_RI_MAX];


	_tag_UI_Rllumination()
	{
		stInspect.Reset();


		dBoxField = 0.8;
		nBoxSize = 150;
		dSpecRIcornerMax = 200;
		dSpecRIcornerMin = -200;
		dSpecRIminMax = 200;
		dSpecRIminMin = -200;
		b8BitUse = FALSE;

		dbCorneroffset = 0.1;
		dbMinoffset = 0.1;

		for (UINT nIdx = 0; nIdx < Spec_RI_MAX; nIdx++)
		{
			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}


	};

	_tag_UI_Rllumination& operator= (_tag_UI_Rllumination& ref)
	{
		stInspect.Reset();


		dBoxField = ref.dBoxField;
		nBoxSize = ref.nBoxSize;
		dSpecRIcornerMax = ref.dSpecRIcornerMax;
		dSpecRIcornerMin = ref.dSpecRIcornerMin;
		dSpecRIminMax = ref.dSpecRIminMin;
		dSpecRIminMin = ref.dSpecRIminMin;
		b8BitUse = ref.b8BitUse;

		dbCorneroffset = ref.dbCorneroffset;
		dbMinoffset = ref.dbMinoffset;

		for (UINT nIdx = 0; nIdx < Spec_RI_MAX; nIdx++)
		{

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}
		return *this;
	};

}ST_UI_Rllumination, *PST_UI_Rllumination;

#pragma pack(pop)

#endif // Def_Rllumination_h__
