#ifndef Wnd_Rst_BlackSpot_h__
#define Wnd_Rst_BlackSpot_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_BlackSpot.h"

// CWnd_Rst_Dynamic
typedef enum
{
	enBlackSpot_Result_Count = 0,
	enBlackSpot_Result_Max,

}enBlackSpot_Result;

static	LPCTSTR	g_szBlackSpot_Result[] =
{
	_T("Count"),
	NULL
};

class CWnd_Rst_BlackSpot : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_BlackSpot)

public:
	CWnd_Rst_BlackSpot();
	virtual ~CWnd_Rst_BlackSpot();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enBlackSpot_Result_Max];
	CVGStatic	m_st_Result[enBlackSpot_Result_Max];

public:

	void Result_Display		(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_BlackSpot_h__
