#ifndef Wnd_Rst_SFR_h__
#define Wnd_Rst_SFR_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"
#include "Def_UI_SFR.h"

// CWnd_Rst_SFR
typedef enum
{
	enSFR_Result_Max = 30,

}enDynaminc_Header;

class CWnd_Rst_SFR : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_SFR)

public:
	CWnd_Rst_SFR();
	virtual ~CWnd_Rst_SFR();

	
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic		m_st_Header[enSFR_Result_Max];
	CVGStatic		m_st_Result[enSFR_Result_Max];

public:

	void Result_Display			(__in UINT nResultIdx, __in UINT nIdx, __in  BOOL bReulst, __in  double dbValue);
	void Result_Reset			();
};

#endif // Wnd_Rst_SFR_h__
