// Wnd_Rst_SFR.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Rst_Shading.h"

// CWnd_Rst_Shading

IMPLEMENT_DYNAMIC(CWnd_Rst_Shading, CWnd)

CWnd_Rst_Shading::CWnd_Rst_Shading()
{
}

CWnd_Rst_Shading::~CWnd_Rst_Shading()
{
}

BEGIN_MESSAGE_MAP(CWnd_Rst_Shading, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_Rst_SFR 메시지 처리기입니다.
int CWnd_Rst_Shading::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	CString szText;

	m_st_Header[enShading_Result_Field_1].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enShading_Result_Field_1].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enShading_Result_Field_1].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enShading_Result_Field_1].Create(_T("Field 1"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enShading_Result_Field_1].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enShading_Result_Field_1].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enShading_Result_Field_1].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enShading_Result_Field_1].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Header[enShading_Result_Field_2].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enShading_Result_Field_2].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enShading_Result_Field_2].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enShading_Result_Field_2].Create(_T("Field 2"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enShading_Result_Field_2].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enShading_Result_Field_2].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enShading_Result_Field_2].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enShading_Result_Field_2].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Header[enShading_Result_Field_1_Dev].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enShading_Result_Field_1_Dev].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enShading_Result_Field_1_Dev].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enShading_Result_Field_1_Dev].Create(_T("Field 1 Dev"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enShading_Result_Field_1_Dev].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enShading_Result_Field_1_Dev].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enShading_Result_Field_1_Dev].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enShading_Result_Field_1_Dev].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Header[enShading_Result_Field_2_Dev].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enShading_Result_Field_2_Dev].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enShading_Result_Field_2_Dev].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enShading_Result_Field_2_Dev].Create(_T("Field 2 Dev"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enShading_Result_Field_2_Dev].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enShading_Result_Field_2_Dev].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enShading_Result_Field_2_Dev].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enShading_Result_Field_2_Dev].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/7/29 - 12:59
// Desc.		:
//=============================================================================
void CWnd_Rst_Shading::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin = 0;
	int iSpacing = 0;
	int iCateSpacing = 0;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int HeaderW = (iWidth) / 2;
	int HeaderH = 18;

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		iLeft = iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iLeft += HeaderW - 1;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iTop += HeaderH;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/7/29 - 13:00
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Shading::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: protected  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/7/29 - 13:00
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Shading::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: Result_Display
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nResultIdx
// Parameter	: __in UINT nIdx
// Parameter	: __in BOOL bReulst
// Parameter	: __in double dbValue
// Qualifier	:
// Last Update	: 2018/7/29 - 12:11
// Desc.		:
//=============================================================================
void CWnd_Rst_Shading::Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue)
{
	CString szValue;

	if (TRUE == bReulst)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	szValue.Format(_T("%.2f"), dbValue);

	m_st_Result[nIdx].SetWindowText(szValue);
}

//=============================================================================
// Method		: Result_Reset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/7/29 - 12:10
// Desc.		:
//=============================================================================
void CWnd_Rst_Shading::Result_Reset()
{
	for (UINT nIdx = 0; nIdx < enShading_Result_Max; nIdx++)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T(""));

		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T(""));
	}
}
