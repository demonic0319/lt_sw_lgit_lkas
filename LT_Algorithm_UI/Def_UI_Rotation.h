﻿#ifndef Def_UI_Rotation_h__
#define Def_UI_Rotation_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_Rotation_ROI
{
	ROI_Rotation_FST = 0,
	ROI_Rotation_LST = 8,
	ROI_Rotation_Max,
};


typedef enum enRotation_Spec
{
	Spec_Rotation = 0,
	Spec_Rotation_MAX,
};

typedef enum enROI_Rotate
{
	ROI_Rot_LT = 0,
	ROI_Rot_MT,
	ROI_Rot_RT,

	ROI_Rot_LM,
	ROI_Rot_MM,
	ROI_Rot_RM,

	ROI_Rot_LB,
	ROI_Rot_MB,
	ROI_Rot_RB,

	ROI_Rot_Max,
};

static LPCTSTR g_szROI_Rotate[] =
{
	_T("LT"),
	_T("MT"),
	_T("RT"),

	_T("LM"),
	_T("MM"),
	_T("RM"),

	_T("LB"),
	_T("MB"),
	_T("RB"),
	NULL
};


typedef struct _tag_Input_Rotation
{
	CRect		rtROI;
	UINT		nMarkType;
	UINT		nMarkColor;
	UINT		nBrightness;
	UINT		nSharpness;


	void RectPosXY(int iPosX, int iPosY)
	{
		CRect rtTemp = rtROI;

		rtROI.left = iPosX - (int)(rtTemp.Width() / 2);
		rtROI.right = rtROI.left + rtTemp.Width();
		rtROI.top = iPosY - (int)(rtTemp.Height() / 2);
		rtROI.bottom = rtROI.top + rtTemp.Height();
	};

	void RectPosWH(int iWidth, int iHeight)
	{
		CRect rtTemp = rtROI;

		rtROI.left = rtTemp.CenterPoint().x - (iWidth / 2);
		rtROI.right = rtTemp.CenterPoint().x + (iWidth / 2) + iWidth % 2;
		rtROI.top = rtTemp.CenterPoint().y - (iHeight / 2);
		rtROI.bottom = rtTemp.CenterPoint().y + (iHeight / 2) + iHeight % 2;
	};


	_tag_Input_Rotation()
	{
		Reset();
	};

	void Reset()
	{
		rtROI.left = 0;
		rtROI.right = 0;
		rtROI.top = 0;
		rtROI.bottom = 0;

		nMarkType = MTyp_Circle;
		nMarkColor = MCol_Black;
		nBrightness = 0;
		nSharpness = 100;
	};

	_tag_Input_Rotation& operator= (_tag_Input_Rotation& ref)
	{
		rtROI = ref.rtROI;
		nMarkType = ref.nMarkType;
		nMarkColor = ref.nMarkColor;
		nBrightness = ref.nBrightness;
		nSharpness = ref.nSharpness;
			

		return *this; 
	};

}ST_Input_Rotation, *PST_Input_Rotation;

typedef struct _tag_UI_Rotation
{
	// Inspect
	ST_Inspect				stInspect;
	ST_Input_Rotation		stInput[ROI_Rotation_Max];

	// Spec
	int			nROIBoxSize;
	int			nMaxROIBoxSize;
	double		dRadius;
	double		dRealGapX;
	double		dRealGapY;
	int			nFiducialMarkNum;
	int			nFiducialMarkType;
	double		dModuleChartDistance;
	int			nDistortionAlrotithmType;

	double		dbOffsetRot;


	ST_Spec			stSpecMin[Spec_Rotation_MAX];
	ST_Spec			stSpecMax[Spec_Rotation_MAX];

	_tag_UI_Rotation()
	{
		stInspect.Reset();

		nROIBoxSize					 = 40;
		nMaxROIBoxSize				= 40;
		dRadius						= 1.0;
		dRealGapX					= 1.0;
		dRealGapY					= 1.0;
		nFiducialMarkNum			= 0;
		nFiducialMarkType			= 0;
		dModuleChartDistance		= 1.0;
		nDistortionAlrotithmType	= 2;
		dbOffsetRot					= 1.0;

		for (UINT nIdx = 0; nIdx < Spec_Rotation_MAX; nIdx++)
		{
			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}

		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			stInput[nROI].Reset();
		}
	}

	_tag_UI_Rotation& operator= (_tag_UI_Rotation& ref)
	{
		nROIBoxSize				= ref.nROIBoxSize;
		nMaxROIBoxSize			 = ref.nMaxROIBoxSize;
		dRadius					 = ref.dRadius;
		dRealGapX = ref.dRealGapX;
		dRealGapY = ref.dRealGapY;
		nFiducialMarkNum		 = ref.nFiducialMarkNum;
		nFiducialMarkType		 = ref.nFiducialMarkType;
		dModuleChartDistance	 = ref.dModuleChartDistance;
		nDistortionAlrotithmType = ref.nDistortionAlrotithmType;
		dbOffsetRot				 = ref.dbOffsetRot;


		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			stInput[nROI] = ref.stInput[nROI];
		}

		for (UINT nIdx = 0; nIdx < Spec_Rotation_MAX; nIdx++)
		{

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_Rotation, *PST_UI_Rotation;

#pragma pack(pop)

#endif // Def_Rotation_h__

