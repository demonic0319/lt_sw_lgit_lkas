﻿#ifndef Def_UI_Shading_h__
#define Def_UI_Shading_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_ROI_Shading
{
	ROI_Shading_FST  = 0,
	ROI_Shading_LST  = 18,
	ROI_Shading_Max,
};

typedef enum enUI_ShadingField
{
	enUI_ShadingField_1,
	enUI_ShadingField_2,
	enUI_ShadingField_1_Dev,
	enUI_ShadingField_2_Dev,
	enUI_ShadingField_Max
};

typedef struct _tag_Input_Shading
{
	CPoint		ptROI;

	UINT		nOverlayDir;
	double		dbOffset;
	double		dbThreshold;
	
	ST_Spec		stSpecMin;
	ST_Spec		stSpecMax;

	_tag_Input_Shading()
	{
		Reset();
	};

	void Reset()
	{
		ptROI.x = 0;
		ptROI.y = 0;

		nOverlayDir			= OverlayDir_Top;
		dbOffset			= 0.0;
		dbThreshold			= 0.0;
		stSpecMin.Reset();
		stSpecMax.Reset();
	};

	_tag_Input_Shading& operator= (_tag_Input_Shading& ref)
	{
		ptROI			= ref.ptROI;
		
		nOverlayDir		= ref.nOverlayDir;
		dbOffset		= ref.dbOffset;
		dbThreshold		= ref.dbThreshold;

		stSpecMin		= ref.stSpecMin;
		stSpecMax		= ref.stSpecMax;
		
		return *this; 
	};

}ST_Input_Shading, *PST_Input_Shading;

typedef struct _tag_UI_Shading
{
	// Inspect
	ST_Inspect			stInspect;

	// Parameter
	int	iMaxROIWidth;						
	int	iMaxROIHeight;						
	int	iMaxROICount;						// 이미지 내 ROI 의 최대 개수
	int	iNormalizeIndex;					// ROI 를 Normalize 할 블록 Index

	double dTestField_1;
	double dTestField_2;

	// Spec
	ST_Spec		stSpecMin[enUI_ShadingField_Max];
	ST_Spec		stSpecMax[enUI_ShadingField_Max];

	_tag_UI_Shading()
	{
		stInspect.Reset();

		iMaxROIWidth	= 40;
		iMaxROIHeight	= 40;
		iMaxROICount	= ROI_Shading_Max;
		iNormalizeIndex = 7;

		dTestField_1 = 0.65;
		dTestField_2 = 0.85;

		for (int i = 0; i < enUI_ShadingField_Max; i++)
		{
			stSpecMin[i].Reset();
			stSpecMax[i].Reset();
		}
	

	};

	_tag_UI_Shading& operator= (_tag_UI_Shading& ref)
	{
		stInspect.Reset();

		iMaxROIWidth		= ref.iMaxROIWidth;
		iMaxROIHeight		= ref.iMaxROIHeight;
		iMaxROICount		= ref.iMaxROICount;
		iNormalizeIndex = ref.iNormalizeIndex;
		dTestField_1 = ref.dTestField_1;
		dTestField_2 = ref.dTestField_2;

		for (int i = 0; i < enUI_ShadingField_Max; i++)
		{
			stSpecMin[i] = ref.stSpecMin[i];
			stSpecMax[i] = ref.stSpecMax[i];
		}

		return *this;
	};

}ST_UI_Shading, *PST_UI_Shading;

#pragma pack(pop)

#endif // Def_Shading_h__
