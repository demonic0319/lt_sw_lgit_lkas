#ifndef Wnd_Rst_Rotate_h__
#define Wnd_Rst_Rotate_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_Rotation.h"

// CWnd_Rst_DRotate
typedef enum
{
	enRotate_Result_Degree = 0,
	enRotate_Result_Max,

}enRotate_Result;

static	LPCTSTR	g_szRotate_Result[] =
{
	_T("Degree"),
	NULL
};

class CWnd_Rst_Rotate : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Rotate)

public:
	CWnd_Rst_Rotate();
	virtual ~CWnd_Rst_Rotate();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enRotate_Result_Max];
	CVGStatic	m_st_Result[enRotate_Result_Max];

public:

	void Result_Display		(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_Rotate_h__
