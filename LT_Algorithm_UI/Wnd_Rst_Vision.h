#ifndef Wnd_Rst_Vision_h__
#define Wnd_Rst_Vision_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"

// CWnd_Rst_Vision
typedef enum
{
	enVision_Result_MatchRate,
	enVision_Result_Max,

}enVision_Result_Num;

class CWnd_Rst_Vision : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Vision)

public:
	CWnd_Rst_Vision();
	virtual ~CWnd_Rst_Vision();

	UINT m_nHeaderCnt = enVision_Result_Max;
	
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic		m_st_Header[enVision_Result_Max];
	CVGStatic		m_st_Result[enVision_Result_Max];

public:

	void Result_Display			(__in UINT nResultIdx, __in UINT nIdx, __in  BOOL bReulst, __in  double dbValue);
	void Result_Reset			();
};

#endif // Wnd_Rst_SFR_h__
