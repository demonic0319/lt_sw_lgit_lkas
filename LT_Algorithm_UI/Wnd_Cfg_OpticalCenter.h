﻿#ifndef Wnd_Cfg_OpticalCenter_h__
#define Wnd_Cfg_OpticalCenter_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_OpticalCenter.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_OpticalCenter
//-----------------------------------------------------------------------------
class CWnd_Cfg_OpticalCenter : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_OpticalCenter)

public:
	CWnd_Cfg_OpticalCenter();
	virtual ~CWnd_Cfg_OpticalCenter();

	enum enOpticalCenterStatic
	{
		STI_OC_PARAM = 0,
		STI_OC_SPEC,
		STI_OC_STATE,
		STI_OC_CENTER_X,
		STI_OC_CENTER_Y,
		STI_OC_OFFSET_X,
		STI_OC_OFFSET_Y,
		STI_OC_MAX,
	};

	enum enOpticalCenterButton
	{
		BTN_OC_MAX = 1,
	};

	enum enOpticalCenterComobox
	{
		CMB_OC_STATE,
		CMB_OC_MAX,
	};

	enum enOpticalCenterEdit
	{
		EDT_OC_CENTER_X,
		EDT_OC_CENTER_Y,
		EDT_OC_OFFSET_X,
		EDT_OC_OFFSET_Y,
		EDT_OC_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CVGStatic			m_st_Item[STI_OC_MAX];
	CButton				m_bn_Item[BTN_OC_MAX];
	CComboBox			m_cb_Item[CMB_OC_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_OC_MAX];

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Spec[OpticalCenter_Max];
	CMFCMaskedEdit		m_ed_SpecMin[OpticalCenter_Max];
	CMFCMaskedEdit		m_ed_SpecMax[OpticalCenter_Max];
	CMFCButton			m_chk_SpecMin[OpticalCenter_Max];
	CMFCButton			m_chk_SpecMax[OpticalCenter_Max];

	ST_UI_OpticalCenter*		m_pstConfig		= NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_OpticalCenter* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_OpticalCenter_h__
