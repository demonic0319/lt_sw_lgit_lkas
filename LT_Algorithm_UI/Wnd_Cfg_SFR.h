﻿#ifndef Wnd_Cfg_SFR_h__
#define Wnd_Cfg_SFR_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_SFR.h"
#include "List_SFR.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_SFR
//-----------------------------------------------------------------------------
class CWnd_Cfg_SFR : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_SFR)

public:
	CWnd_Cfg_SFR();
	virtual ~CWnd_Cfg_SFR();

	enum enSFRStatic
	{
		STI_SFR_INSPECT = 0,
		STI_SFR_SPEC,
		STI_SFR_ROI,
		STI_SFR_DataForamt,
		STI_SFR_OutMode,
		STI_SFR_SensorType,
		STI_SFR_BlackLevel,
		STI_SFR_EdgeAngle,
		STI_SFR_PixelSize,
	//	STI_SFR_Type,
		STI_SFR_Method,
		//STI_SFR_EnableLog,
		STI_SFR_AverageCount,
		STI_SFR_MAX,
	};

	enum enSFRButton
	{
		BTN_SFR_MAX = 1,
	};

	enum enSFRComobox
	{
		CMB_SFR_DataForamt = 0,
		CMB_SFR_OutMode,
		CMB_SFR_SensorType,
	//	CMB_SFR_Type,
		CMB_SFR_Method,
		//CMB_SFR_EnableLog,
		CMB_SFR_MAX,
	};

	enum enSFREdit
	{
		EDT_SFR_BlackLevel = 0,
		EDT_SFR_EdgeAngle,
		EDT_SFR_PixelSize,
		EDT_SFR_AverageCount,
		EDT_SFR_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CList_SFR			m_List;

	CVGStatic			m_st_Item[STI_SFR_MAX];
	CButton				m_bn_Item[BTN_SFR_MAX];
	CComboBox			m_cb_Item[CMB_SFR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_SFR_MAX];

	ST_UI_SFR*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_SFR* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfig = pstConfigInfo;
	};

	void	SetUpdateData();
	void	GetUpdateData();
};
#endif // Wnd_Cfg_SFR_h__
