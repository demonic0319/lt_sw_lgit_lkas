﻿// List_ShadingOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_ShadingOp.h"

typedef enum enListNumShadingOp
{
	ShaOp_Object = 0,
	ShaOp_PosX,
	ShaOp_PosY,
	ShaOp_Threshold,
	ShaOp_Offset,
	ShaOp_MinSpecUse,
	ShaOp_MinSpec,
	ShaOp_MaxSpecUse,
	ShaOp_MaxSpec,
	ShaOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_ShadingOp[] =
{
	_T(""),						// ShaOp_Object = 0,
	_T("Center X"),				// ShaOp_PosX,
	_T("Center Y"),				// ShaOp_PosY,
	_T("Threshold"),			// ShaOp_Threshold,
	_T("Offset"),				// ShaOp_Offset,
	_T(""),						// ShaOp_MinSpecUse,
	_T("MinSpec"),				// ShaOp_MinSpec,
	_T(""),						// ShaOp_MaxSpecUse,
	_T("MaxSpec"),				// ShaOp_MaxSpec,
	NULL
};

const int	iListAglin_ShadingOp[] =
{
	LVCFMT_LEFT,				// ShaOp_Object = 0,
	LVCFMT_CENTER,				// ShaOp_PosX,
	LVCFMT_CENTER,				// ShaOp_PosY,
	LVCFMT_CENTER,				// ShaOp_Offset,
	LVCFMT_CENTER,				// ShaOp_Threshold,
	LVCFMT_CENTER,				// ShaOp_MinSpecUse,
	LVCFMT_CENTER,				// ShaOp_MinSpec,
	LVCFMT_CENTER,				// ShaOp_MaxSpecUse,
	LVCFMT_CENTER,				// ShaOp_MaxSpec,
};

const int	iHeaderWidth_ShadingOp[] =
{
	60,							// ShaOp_Object = 0,
	70,							// ShaOp_PosX,
	70,							// ShaOp_PosY,
	80,							// ShaOp_Offset,
	80,							// ShaOp_Threshold,
	25,							// ShaOp_MinSpecUse,
	80,							// ShaOp_MinSpec,
	25,							// ShaOp_MaxSpecUse,
	80,							// ShaOp_MaxSpec,
};						

#define IDC_EDT_CELLEDIT			1000
#define IDC_COM_CELLCOMBO_TYPE		2000
#define IDC_COM_CELLCOMBO_OVERLAY	2001

IMPLEMENT_DYNAMIC(CList_ShadingOp, CList_Cfg_VIBase)

CList_ShadingOp::CList_ShadingOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_pstConfig = NULL;
}

CList_ShadingOp::~CList_ShadingOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_ShadingOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_ShadingOp::OnNMClick					)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_ShadingOp::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_ShadingOp::OnEnKillFocusEdit			)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_ShadingOp 메시지 처리기입니다.
int CList_ShadingOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;
		
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	SetFont(&m_Font);
	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader(); 
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_ed_CellEdit.Create(WS_CHILD | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_EDT_CELLEDIT);
	m_ed_CellEdit.SetValidChars(_T("-.0123456789"));

	m_cb_Overlay.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_OVERLAY);
	m_cb_Overlay.ResetContent();

	for (UINT nIdex = 0; NULL != g_szOverayDir[nIdex]; nIdex++)
	{
		m_cb_Overlay.InsertString(nIdex, g_szOverayDir[nIdex]);
	}
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ShadingOp::InitHeader()
{
	for (int nCol = 0; nCol < ShaOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_ShadingOp[nCol], iListAglin_ShadingOp[nCol], iHeaderWidth_ShadingOp[nCol]);
	}

	for (int nCol = 0; nCol < ShaOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_ShadingOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_ShadingOp::InsertFullData()
{
	if (m_pstConfig == NULL)
		return;

 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < m_nMaxROI; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_ShadingOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	szValue.Format(_T("%d"), nRow);
	SetItemText(nRow, ShaOp_Object, szValue);
	
	szValue.Format(_T("%d"), m_pstConfig[nRow].ptROI.x);
	SetItemText(nRow, ShaOp_PosX, szValue);

	szValue.Format(_T("%d"), m_pstConfig[nRow].ptROI.y);
	SetItemText(nRow, ShaOp_PosY, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig[nRow].dbThreshold);
	SetItemText(nRow, ShaOp_Threshold, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig[nRow].dbOffset);
	SetItemText(nRow, ShaOp_Offset, szValue);
	
	szValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfig[nRow].stSpecMin.bEnable]);
	SetItemText(nRow, ShaOp_MinSpecUse, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig[nRow].stSpecMin.dbValue);
	SetItemText(nRow, ShaOp_MinSpec, szValue);

	szValue.Format(_T("%s"), g_szSpecUse_Static[m_pstConfig[nRow].stSpecMax.bEnable]);
	SetItemText(nRow, ShaOp_MaxSpecUse, szValue);

	szValue.Format(_T("%.2f"), m_pstConfig[nRow].stSpecMax.dbValue);
	SetItemText(nRow, ShaOp_MaxSpec, szValue);

}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (ShaOp_MinSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfig[pNMItemActivate->iItem].stSpecMin.bEnable = !m_pstConfig[pNMItemActivate->iItem].stSpecMin.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfig[pNMItemActivate->iItem].stSpecMin.bEnable]);
	}

	if (ShaOp_MaxSpecUse == pNMItemActivate->iSubItem)
	{
		m_pstConfig[pNMItemActivate->iItem].stSpecMax.bEnable = !m_pstConfig[pNMItemActivate->iItem].stSpecMax.bEnable;
		SetItemText(pNMItemActivate->iItem, pNMItemActivate->iSubItem, g_szSpecUse_Static[m_pstConfig[pNMItemActivate->iItem].stSpecMax.bEnable]);
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (ShaOp_MinSpecUse == pNMItemActivate->iSubItem || ShaOp_MaxSpecUse == pNMItemActivate->iSubItem)
		{
			*pResult = 1;
			return;
		}

		if (pNMItemActivate->iSubItem < ShaOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);

			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);
		
			
			m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
			m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
			m_ed_CellEdit.SetFocus();

			GetOwner()->SendNotifyMessage(m_wn_Change, 0, 0);
			
		}
	}

	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_ShadingOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	if (m_nEditCol == ShaOp_Threshold || m_nEditCol == ShaOp_Offset || m_nEditCol == ShaOp_MinSpec || m_nEditCol == ShaOp_MaxSpec)
	{
		UpdateCelldbData(m_nEditRow, m_nEditCol, _ttof(strText));
	}
	else
	{
		UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));
	}

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (NULL == m_pstConfig)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	switch (nCol)
	{
	case ShaOp_PosX:
		m_pstConfig[nRow].ptROI.x = iValue;
		break;
	case ShaOp_PosY:
		m_pstConfig[nRow].ptROI.y = iValue;
		break;
	default:
		break;
	}

	CString szValue;

	switch (nCol)
	{
	case ShaOp_PosX:
		szValue.Format(_T("%d"), m_pstConfig[nRow].ptROI.x);
		break;
	case ShaOp_PosY:
		szValue.Format(_T("%d"), m_pstConfig[nRow].ptROI.y);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(szValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	switch (nCol)
	{
	case ShaOp_Threshold:
		m_pstConfig[nRow].dbThreshold = dbValue;
		break;
	case ShaOp_Offset:
		m_pstConfig[nRow].dbOffset = dbValue;
		break;
	case ShaOp_MinSpec:
		m_pstConfig[nRow].stSpecMin.dbValue = dbValue;
		break;
	case ShaOp_MaxSpec:
		m_pstConfig[nRow].stSpecMax.dbValue = dbValue;
		break;
	default:
		break;
	}

	CString str;
	str.Format(_T("%.2f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_ShadingOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_ShadingOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue = _ttoi(strText);
		double dbValue = _ttof(strText);
		
		if (m_nEditCol == ShaOp_Threshold || m_nEditCol == ShaOp_Offset || m_nEditCol == ShaOp_MinSpec || m_nEditCol == ShaOp_MaxSpec)
		{
			iValue = iValue + ((zDelta / 120));
			dbValue = dbValue + ((zDelta / 120) * 0.01);
		}
		else
		{
			if (0 < zDelta)
			{
				iValue = iValue + ((zDelta / 120));
				dbValue = dbValue + ((zDelta / 120) * 0.01);
			}
			else
			{
				if (0 < iValue)
					iValue = iValue + ((zDelta / 120));

				if (0 < dbValue)
					dbValue = dbValue + ((zDelta / 120) * 0.01);
			}

			if (iValue < 0)
				iValue = 0;

			if (iValue > 2000)
				iValue = 2000;

			if (dbValue < 0.0)
				dbValue = 0.0;

		}

		if (m_nEditCol == ShaOp_Threshold || m_nEditCol == ShaOp_Offset || m_nEditCol == ShaOp_MinSpec || m_nEditCol == ShaOp_MaxSpec)
		{
			UpdateCelldbData(m_nEditRow, m_nEditCol, dbValue);
		}
		else
		{
			UpdateCellData(m_nEditRow, m_nEditCol, iValue);
		}
	}

	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

