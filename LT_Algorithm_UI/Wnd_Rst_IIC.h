#ifndef Wnd_Rst_IIC_h__
#define Wnd_Rst_IIC_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"

// CWnd_Rst_IIC
typedef enum
{
	enIIC_Result_MatchRate,
	enIIC_Result_Max,

}enIIC_Result_Num;

class CWnd_Rst_IIC : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_IIC)

public:
	CWnd_Rst_IIC();
	virtual ~CWnd_Rst_IIC();

	UINT m_nHeaderCnt = enIIC_Result_Max;
	
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic		m_st_Header[enIIC_Result_Max];
	CVGStatic		m_st_Result[enIIC_Result_Max];

public:

	void Result_Display			(__in UINT nResultIdx, __in UINT nIdx, __in  BOOL bReulst, __in  double dbValue);
	void Result_Reset			();
};

#endif // Wnd_Rst_SFR_h__
