﻿#ifndef Wnd_Cfg_DynamicBW_h__
#define Wnd_Cfg_DynamicBW_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_DynamicBW.h"
#include "List_DynamicBW.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_DynamicBW
//-----------------------------------------------------------------------------
class CWnd_Cfg_DynamicBW : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_DynamicBW)

public:
	CWnd_Cfg_DynamicBW();
	virtual ~CWnd_Cfg_DynamicBW();

	enum enDynamicStatic
	{
		STI_DYN_INSPECT = 0,
		STI_DYN_SPEC,
		STI_DYN_ROI,
		STI_DYN_DataForamt,
		STI_DYN_OutMode,
		STI_DYN_SensorType,
		STI_DYN_BlackLevel,
		STI_DYN_ROI_Max,
		STI_DYN_Offset_DY,
		STI_DYN_Offset_BW,
		STI_DYN_ROI_Width,
		STI_DYN_ROI_Height,
		STI_DYN_DR,
		STI_DYN_SNR,
		STI_DYN_PARAM,
		//STI_DYN_DRThreshold,
		//STI_DYN_SNRThreshold,
		STI_DYN_8bitUse,
		STI_DYN_MAX,
	};

	enum enDynamicButton
	{
		BTN_DYN_MAX = 1,
	};

	enum enDynamicComobox
	{
		CMB_DYN_DataForamt = 0,
		CMB_DYN_OutMode,
		CMB_DYN_SensorType,
		CMB_DYN_8bitUse,
		CMB_DYN_MAX,
	};

	enum enDynamicEdit
	{
		EDT_DYN_BlackLevel = 0,
		EDT_DYN_ROI_Max,
		EDT_DYN_Offset_DY,
		EDT_DYN_Offset_BW,
		EDT_DYN_ROI_Width,
		EDT_DYN_ROI_Height,
		EDT_DYN_DR,
		EDT_DYN_SNR,
		//EDT_DYN_DRThreshold, 
		//EDT_DYN_SNRThreshold, 
		EDT_DYN_MAX,
	};

protected:
	
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont						m_font;

	CList_DynamicBW				m_List;

	CVGStatic					m_st_Item[STI_DYN_MAX];
	CButton						m_bn_Item[BTN_DYN_MAX];
	CComboBox					m_cb_Item[CMB_DYN_MAX];
	CMFCMaskedEdit				m_ed_Item[EDT_DYN_MAX];

	CVGStatic					m_st_CapItem;
	CVGStatic					m_st_CapSpecMin;
	CVGStatic					m_st_CapSpecMax;

	CVGStatic					m_st_Spec[Spec_DynamicBW_MAX];
	CMFCMaskedEdit				m_ed_SpecMin[Spec_DynamicBW_MAX];
	CMFCMaskedEdit				m_ed_SpecMax[Spec_DynamicBW_MAX];
	CMFCButton					m_chk_SpecMin[Spec_DynamicBW_MAX];
	CMFCButton					m_chk_SpecMax[Spec_DynamicBW_MAX];

	ST_UI_DynamicBW*			m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_DynamicBW* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_DynamicBW_h__
