﻿// Wnd_Cfg_IIC.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_IIC.h"
#include "resource.h"

static LPCTSTR	g_szIIC_Static[] =
{
	_T("PARAMETER"),
	_T("PATTERN"),
// 	_T("SPEC"),
// 	_T("MATCH RATE"),
	NULL
};

static LPCTSTR	g_szIIC_Button[] =
{
	NULL
};

// CWnd_Cfg_IIC
typedef enum EIIC_OptID
{
	IDC_BTN_ITEM	= 1001,
	IDC_CMB_ITEM	= 2001,
	IDC_EDT_ITEM	= 3001,
	IDC_LIST_ITEM	= 4001,

	IDC_ED_SPEC_MIN  = 5001,
	IDC_ED_SPEC_MAX  = 6001,
	IDC_CHK_SPEC_MIN = 7001,
	IDC_CHK_SPEC_MAX = 8001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_IIC, CWnd_Cfg_VIBase)

CWnd_Cfg_IIC::CWnd_Cfg_IIC()
{
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_IIC::~CWnd_Cfg_IIC()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_IIC, CWnd_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_IIC 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_Cfg_IIC::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_IIC_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szIIC_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_Item[STI_IIC_PARAM].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	//m_st_Item[STI_IIC_SPEC].SetColorStyle(CVGStatic::ColorStyle_DarkGray);

	for (UINT nIdex = 0; nIdex < BTN_IIC_MAX; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szIIC_Button[nIdex], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BTN_ITEM + nIdex);
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	for (UINT nIdex = 0; nIdex < EDT_IIC_MAX; nIdex++)
	{
		m_ed_Item[nIdex].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDT_ITEM + nIdex);
		m_ed_Item[nIdex].SetWindowText(_T("0"));
		m_ed_Item[nIdex].SetValidChars(_T("0123456789.-"));
	}

	for (UINT nIdex = 0; nIdex < CMB_IIC_MAX; nIdex++)
	{
		m_cb_Item[nIdex].Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CMB_ITEM + nIdex);
	}

	if (!m_szI2CImgFolderPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szI2CImgFolderPath, _T("bmp"));
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
		m_cb_Item[CMB_IIC_PATTERN].SetCurSel(0);
	}


// 	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
// 	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
// 	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
// 	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

// 	for (UINT nIdx = 0; nIdx < IIC_Max; nIdx++)
// 	{
// 		m_st_Spec[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
// 		m_st_Spec[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
// 		m_st_Spec[nIdx].SetFont_Gdip(L"Arial", 9.0F);
// 		m_st_Spec[nIdx].Create(g_szIIC_Static[nIdx + STI_IIC_MATCH_RATE], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
// 		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
// 		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));
// 
// 		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
// 		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
// 		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));
// 
// 		m_ed_SpecMin[nIdx].SetFont(&m_font);
// 		m_ed_SpecMax[nIdx].SetFont(&m_font);
// 
// 		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
// 		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);
// 
// 		m_chk_SpecMin[nIdx].SetMouseCursorHand();
// 		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
// 		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
// 		m_chk_SpecMin[nIdx].SizeToContent();
// 
// 		m_chk_SpecMax[nIdx].SetMouseCursorHand();
// 		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
// 		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
// 		m_chk_SpecMax[nIdx].SizeToContent();
// 	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_IIC::OnSize(UINT nType, int cx, int cy)
{
	CWnd_Cfg_VIBase::OnSize(nType, cx, cy);

	int iMargin		= 10;
	int iSpacing	= 5;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int iStWidth	= (iWidth - iSpacing - iSpacing) / 6;
	int iEdWidth	= iStWidth * 2;
	int iStHeight	= 25;

	m_st_Item[STI_IIC_PARAM].MoveWindow(iLeft, iTop, iWidth, iStHeight);

	iLeft = iMargin;
	iTop += iStHeight + iSpacing;
	m_st_Item[STI_IIC_PATTERN].MoveWindow(iLeft, iTop, iStWidth, iStHeight);

	iLeft += iStWidth + 2;
	m_cb_Item[CMB_IIC_PATTERN].MoveWindow(iLeft, iTop, iEdWidth, 100);

// 	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 	m_st_Item[STI_IIC_MATCH_RATE].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
// 
// 	iLeft += iStWidth + 2;
// 	m_ed_Item[EDT_IIC_MATCH_RATE].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

//	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 	m_st_Item[STI_IIC_OFFSET_POS_B].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
// 
// 	iLeft += iStWidth + 2;
// 	m_ed_Item[EDT_IIC_OFFSET_POS_B].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);
// 
// 	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 	m_st_Item[STI_IIC_OFFSET_POS_C].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
// 
// 	iLeft += iStWidth + 2;
// 	m_ed_Item[EDT_IIC_OFFSET_POS_C].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);
// 
// 	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 	m_st_Item[STI_IIC_OFFSET_POS_D].MoveWindow(iLeft, iTop, iStWidth, iStHeight);
// 
// 	iLeft += iStWidth + 2;
// 	m_ed_Item[EDT_IIC_OFFSET_POS_D].MoveWindow(iLeft, iTop, iEdWidth, iStHeight);

// 	// 이름
// 	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 	m_st_Item[STI_IIC_SPEC].MoveWindow(iLeft, iTop, iWidth, iStHeight);
// 
// 	// 스펙
// 	iLeft = iMargin;
// 	iTop += iStHeight + iSpacing;
// 
// 	int iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;	//170;
// 	int iLeft_2nd = iLeft + iCtrlWidth + iSpacing;
// 	int iLeft_3rd = iLeft_2nd + iCtrlWidth + iSpacing;
// 	
// 	iEdWidth = iCtrlWidth - iSpacing - iStHeight;
// 
// 	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iStHeight);
// 	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iStHeight);
// 	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iStHeight);
// 
// 	for (UINT nIdx = 0; nIdx < IIC_Max; nIdx++)
// 	{
// 		iTop += iStHeight + iSpacing;
// 
// 		m_st_Spec[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iStHeight);
// 
// 		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iStHeight, iStHeight);
// 		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd + iStHeight + iSpacing, iTop, iEdWidth, iStHeight);
// 
// 		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iStHeight, iStHeight);
// 		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd + iStHeight + iSpacing, iTop, iEdWidth, iStHeight);
// 	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_IIC::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_IIC::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_Cfg_VIBase::OnShowWindow(bShow, nStatus);

	if (NULL != m_pstConfig)
	{
		if (TRUE == bShow)
		{
			GetOwner()->SendMessage(m_wm_Overlay, 0, m_It_Overlay);
		}
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_IIC::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/12 - 17:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_IIC::SetUpdateData()
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	//szValue = m_szI2CImgFolderPath + _T("\\") + m_pstConfig->szPatternName;

	if (!m_szI2CImgFolderPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szI2CImgFolderPath, _T("bmp"));
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
		
		int iSel = m_cb_Item[CMB_IIC_PATTERN].FindString(-1, m_pstConfig->szPatternName);
		m_cb_Item[CMB_IIC_PATTERN].SetCurSel(iSel);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/14 - 18:07
// Desc.		:
//=============================================================================
void CWnd_Cfg_IIC::GetUpdateData()
{
	if (NULL == m_pstConfig)
		return;

	CString szValue;

	m_cb_Item[CMB_IIC_PATTERN].GetWindowText(szValue);
	m_pstConfig->szPatternName = szValue;

// 	m_ed_Item[EDT_IIC_MATCH_RATE].GetWindowText(szValue);
// 	m_pstConfig->dbOffset[IIC_Match] = _ttof(szValue);

// 	m_ed_Item[EDT_IIC_OFFSET_POS_B].GetWindowText(szValue);
// 	m_pstConfig->dbOffset[IIC_B] = _ttof(szValue);
// 
// 	m_ed_Item[EDT_IIC_OFFSET_POS_C].GetWindowText(szValue);
// 	m_pstConfig->dbOffset[IIC_C] = _ttof(szValue);
// 
// 	m_ed_Item[EDT_IIC_OFFSET_POS_D].GetWindowText(szValue);
// 	m_pstConfig->dbOffset[IIC_D] = _ttof(szValue);

// 	for (UINT nSpec = 0; nSpec < IIC_Max; nSpec++)
// 	{
// 		m_ed_SpecMin[nSpec].GetWindowText(szValue);
// 		m_pstConfig->stSpecMin[nSpec].dbValue = _ttof(szValue);
// 
// 		m_ed_SpecMax[nSpec].GetWindowText(szValue);
// 		m_pstConfig->stSpecMax[nSpec].dbValue = _ttof(szValue);
// 
// 		m_pstConfig->stSpecMin[nSpec].bEnable = m_chk_SpecMin[nSpec].GetCheck();
// 		m_pstConfig->stSpecMax[nSpec].bEnable = m_chk_SpecMax[nSpec].GetCheck();
// 	}
}



void CWnd_Cfg_IIC::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_Item[CMB_IIC_PATTERN].ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_Item[CMB_IIC_PATTERN].AddString(pFileList->GetNext(pos));
	}

	// 초기화
	if (0 < pFileList->GetCount())
	{
		m_cb_Item[CMB_IIC_PATTERN].SetCurSel(0);
	}
}