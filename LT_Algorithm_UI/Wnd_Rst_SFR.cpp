// Wnd_Rst_SFR.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Rst_SFR.h"

// CWnd_Rst_SFR

IMPLEMENT_DYNAMIC(CWnd_Rst_SFR, CWnd)

CWnd_Rst_SFR::CWnd_Rst_SFR()
{
}

CWnd_Rst_SFR::~CWnd_Rst_SFR()
{
}

BEGIN_MESSAGE_MAP(CWnd_Rst_SFR, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_Rst_SFR 메시지 처리기입니다.
int CWnd_Rst_SFR::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	CString szText;

	for (UINT nIdx = 0; nIdx < enSFR_Result_Max; nIdx++)
	{
		szText.Format(_T("%d"), nIdx);
		m_st_Header[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Header[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Header[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Header[nIdx].Create(szText, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_st_Result[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Result[nIdx].Create(_T("X"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/7/29 - 12:59
// Desc.		:
//=============================================================================
void CWnd_Rst_SFR::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin		= 0;
	int iSpacing	= 0;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx;
	int iHeight		= cy;

	int HeaderW		= iWidth  / 6;
	int HeaderH		= iHeight / 10;

	for (UINT nIdx = 0; nIdx < 10; nIdx++)
	{
		iLeft = iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iLeft += HeaderW;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW + 1, HeaderH);

		iTop += HeaderH;
	}

	iTop = iMargin;

	for (UINT nIdx = 10; nIdx < 20; nIdx++)
	{
		iLeft = HeaderW * 2 + iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iLeft += HeaderW;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW + 1, HeaderH);

		iTop += HeaderH;
	}

	iTop = iMargin;

	for (UINT nIdx = 20; nIdx < 30; nIdx++)
	{
		iLeft = HeaderW * 4 + iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iLeft += HeaderW;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, iWidth - iLeft, HeaderH);

		iTop += HeaderH;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/7/29 - 13:00
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_SFR::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: protected  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/7/29 - 13:00
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_SFR::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: Result_Display
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nResultIdx
// Parameter	: __in UINT nIdx
// Parameter	: __in BOOL bReulst
// Parameter	: __in double dbValue
// Qualifier	:
// Last Update	: 2018/7/29 - 12:11
// Desc.		:
//=============================================================================
void CWnd_Rst_SFR::Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue)
{
	CString szValue;

	if (TRUE == bReulst)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	szValue.Format(_T("%.2f"), dbValue);

	m_st_Result[nIdx].SetWindowText(szValue);
}

//=============================================================================
// Method		: Result_Reset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/7/29 - 12:10
// Desc.		:
//=============================================================================
void CWnd_Rst_SFR::Result_Reset()
{
	for (UINT nIdx = 0; nIdx < enSFR_Result_Max; nIdx++)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T("X"));

		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T("X"));
	}
}
