// Wnd_Rst_SFR.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Rst_Displace.h"

// CWnd_Rst_Displace

IMPLEMENT_DYNAMIC(CWnd_Rst_Displace, CWnd)

CWnd_Rst_Displace::CWnd_Rst_Displace()
{
}

CWnd_Rst_Displace::~CWnd_Rst_Displace()
{
}

BEGIN_MESSAGE_MAP(CWnd_Rst_Displace, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CWnd_Rst_SFR 메시지 처리기입니다.
int CWnd_Rst_Displace::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	CString szText;

// 	m_st_Header[enDisplace_Result_A].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Header[enDisplace_Result_A].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Header[enDisplace_Result_A].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Header[enDisplace_Result_A].Create(_T("Point A"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_Result[enDisplace_Result_A].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Result[enDisplace_Result_A].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Result[enDisplace_Result_A].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Result[enDisplace_Result_A].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_Header[enDisplace_Result_B].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Header[enDisplace_Result_B].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Header[enDisplace_Result_B].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Header[enDisplace_Result_B].Create(_T("Point B"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_Result[enDisplace_Result_B].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Result[enDisplace_Result_B].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Result[enDisplace_Result_B].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Result[enDisplace_Result_B].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_Header[enDisplace_Result_C].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Header[enDisplace_Result_C].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Header[enDisplace_Result_C].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Header[enDisplace_Result_C].Create(_T("Point C"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_Result[enDisplace_Result_C].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Result[enDisplace_Result_C].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Result[enDisplace_Result_C].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Result[enDisplace_Result_C].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_Header[enDisplace_Result_D].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Header[enDisplace_Result_D].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Header[enDisplace_Result_D].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Header[enDisplace_Result_D].Create(_T("Point D"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
// 
// 	m_st_Result[enDisplace_Result_D].SetStaticStyle(CVGStatic::StaticStyle_Data);
// 	m_st_Result[enDisplace_Result_D].SetColorStyle(CVGStatic::ColorStyle_Default);
// 	m_st_Result[enDisplace_Result_D].SetFont_Gdip(L"Arial", 9.0F);
// 	m_st_Result[enDisplace_Result_D].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Header[enDisplace_Result_ModifyTx].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enDisplace_Result_ModifyTx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enDisplace_Result_ModifyTx].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enDisplace_Result_ModifyTx].Create(_T("Modify Tilt x"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enDisplace_Result_ModifyTx].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enDisplace_Result_ModifyTx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enDisplace_Result_ModifyTx].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enDisplace_Result_ModifyTx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Header[enDisplace_Result_ModifyTy].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enDisplace_Result_ModifyTy].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enDisplace_Result_ModifyTy].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enDisplace_Result_ModifyTy].Create(_T("Modify Tilt y"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enDisplace_Result_ModifyTy].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enDisplace_Result_ModifyTy].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enDisplace_Result_ModifyTy].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enDisplace_Result_ModifyTy].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	m_st_Header[enDisplace_Result_AATx].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enDisplace_Result_AATx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enDisplace_Result_AATx].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enDisplace_Result_AATx].Create(_T("AA Tilt x"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enDisplace_Result_AATx].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enDisplace_Result_AATx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enDisplace_Result_AATx].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enDisplace_Result_AATx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	
	m_st_Header[enDisplace_Result_Tx].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enDisplace_Result_Tx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enDisplace_Result_Tx].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enDisplace_Result_Tx].Create(_T("Tilt x"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enDisplace_Result_Tx].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enDisplace_Result_Tx].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enDisplace_Result_Tx].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enDisplace_Result_Tx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Header[enDisplace_Result_AATy].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enDisplace_Result_AATy].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enDisplace_Result_AATy].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enDisplace_Result_AATy].Create(_T("AA Tilt y"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Result[enDisplace_Result_AATy].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enDisplace_Result_AATy].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enDisplace_Result_AATy].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enDisplace_Result_AATy].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_Header[enDisplace_Result_Ty].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Header[enDisplace_Result_Ty].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Header[enDisplace_Result_Ty].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Header[enDisplace_Result_Ty].Create(_T("Tilt y"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	
	m_st_Result[enDisplace_Result_Ty].SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_st_Result[enDisplace_Result_Ty].SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Result[enDisplace_Result_Ty].SetFont_Gdip(L"Arial", 9.0F);
	m_st_Result[enDisplace_Result_Ty].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
				
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/7/29 - 12:59
// Desc.		:
//=============================================================================
void CWnd_Rst_Displace::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin		= 0;
	int iSpacing	= 0;
	int iCateSpacing = 0;

	int iLeft		= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;

	int HeaderW		= (iWidth) / 2;
	int HeaderH		= 18;

	for (UINT nIdx = 0; nIdx < m_nHeaderCnt; nIdx++)
	{
		iLeft = iMargin;
		m_st_Header[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iLeft += HeaderW - 1;
		m_st_Result[nIdx].MoveWindow(iLeft, iTop, HeaderW, HeaderH);

		iTop += HeaderH;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2018/7/29 - 13:00
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Displace::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: protected  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2018/7/29 - 13:00
// Desc.		:
//=============================================================================
BOOL CWnd_Rst_Displace::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: Result_Display
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nResultIdx
// Parameter	: __in UINT nIdx
// Parameter	: __in BOOL bReulst
// Parameter	: __in double dbValue
// Qualifier	:
// Last Update	: 2018/7/29 - 12:11
// Desc.		:
//=============================================================================
void CWnd_Rst_Displace::Result_Display(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in double dbValue)
{
	CString szValue;

	if (TRUE == bReulst)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
	}
	else
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
	}

	szValue.Format(_T("%.2f"), dbValue);

	m_st_Result[nIdx].SetWindowText(szValue);
}

//=============================================================================
// Method		: Result_Reset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/7/29 - 12:10
// Desc.		:
//=============================================================================
void CWnd_Rst_Displace::Result_Reset()
{
	for (UINT nIdx = 0; nIdx < enDisplace_Result_Max; nIdx++)
	{
		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T(""));

		m_st_Result[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Result[nIdx].SetWindowText(_T(""));
	}
}
