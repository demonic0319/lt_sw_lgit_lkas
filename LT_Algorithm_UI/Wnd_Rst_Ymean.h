#ifndef Wnd_Rst_Ymean_h__
#define Wnd_Rst_Ymean_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_Ymean.h"

// CWnd_Rst_Dynamic
typedef enum
{
	enYmean_Result_Count = 0,
	enYmean_Result_Max,

}enYmean_Result;

static	LPCTSTR	g_szYmean_Result[] =
{
	_T("Count"),
	NULL
};

class CWnd_Rst_Ymean : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Ymean)

public:
	CWnd_Rst_Ymean();
	virtual ~CWnd_Rst_Ymean();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enYmean_Result_Max];
	CVGStatic	m_st_Result[enYmean_Result_Max];

public:

	void Result_Display		(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_Ymean_h__
