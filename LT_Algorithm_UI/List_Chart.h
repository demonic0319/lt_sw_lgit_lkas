﻿#ifndef List_Chart_h__
#define List_Chart_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_UI_Chart.h"

typedef enum enListItemNum_Chart
{
	ChOp_ItemNum = 1,
};

//-----------------------------------------------------------------------------
// List_ChartInfo
//-----------------------------------------------------------------------------
class CList_Chart : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_Chart)

public:
	CList_Chart();
	virtual ~CList_Chart();

protected:
	
	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow				(CREATESTRUCT& cs);

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel				(UINT nFlags, short zDelta, CPoint pt);
	
	afx_msg void	OnEnKillFocusComboType		();
	afx_msg void	OnEnSelectComboType			();

	afx_msg void	OnEnKillFocusEdit			();

	BOOL			UpdateCellData				(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData			(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;

	UINT				m_nEditCol		= 0;
	UINT				m_nEditRow		= 0;

	CMFCMaskedEdit		m_ed_CellEdit;
	CComboBox			m_cb_Type;

	ST_UI_Chart*		m_pstConfig		= NULL;

public:

	void SetPtr_ConfigInfo(ST_UI_Chart* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfig = pstConfigInfo;
	};

	void InitHeader		();
	void InsertFullData	();
	void SetRectRow		(UINT nRow);
	void GetCellData	();
	
};

#endif // List_ChartInfo_h__
