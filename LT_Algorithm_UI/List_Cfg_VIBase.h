//*****************************************************************************
// Filename	: 	List_Cfg_VIBase.h
// Created	:	2018/1/26 - 17:33
// Modified	:	2018/1/26 - 17:33
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef List_Cfg_VIBase_h__
#define List_Cfg_VIBase_h__

#pragma once

#include <afxwin.h>

//-----------------------------------------------------------------------------
// CList_Cfg_VIBase
//-----------------------------------------------------------------------------
class CList_Cfg_VIBase : public CListCtrl
{
	DECLARE_DYNAMIC(CList_Cfg_VIBase)

public:
	CList_Cfg_VIBase();
	virtual ~CList_Cfg_VIBase();

	void SetChang_Message	(__in UINT nMeg)
	{
		m_wn_Change = nMeg;
	};



protected:
	DECLARE_MESSAGE_MAP()

	UINT	m_wn_Change			= NULL;



	DWORD	m_dwImage_Width		= 1280;
	DWORD	m_dwImage_Height	= 1080;


};


#endif // List_Cfg_VIBase_h__
