﻿#ifndef Def_UI_OpticalCenter_h__
#define Def_UI_OpticalCenter_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_ROI_OpticalCenter
{
	OpticalCenter_Offset_X,
	OpticalCenter_Offset_Y,
	OpticalCenter_Max,
};

typedef enum enUI_CamState
{
	OC_CamState_Original,	// 오리지날
	OC_CamState_UpsideDown, // 상하반전
	OC_CamState_Reverse,	// 좌우반전
	OC_CamState_Rotate,		// 로테이트
	OC_CamState_Max,
};

typedef struct _tag_UI_OpticalCenter
{
	UINT		nCycle;

	double		dCenterX;
	double		dCenterY;
	double		dOffset[OpticalCenter_Max];

	UINT nCamState;

	ST_Spec		stSpecMin[OpticalCenter_Max];
	ST_Spec		stSpecMax[OpticalCenter_Max];
	
	_tag_UI_OpticalCenter()
	{
		nCycle = 1;
		dCenterX = 0;
		dCenterY = 0;

		for (UINT nIdx = 0; nIdx < OpticalCenter_Max; nIdx++)
		{
			dOffset[nIdx] = 0;

			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}
	};

	_tag_UI_OpticalCenter& operator= (_tag_UI_OpticalCenter& ref)
	{
		nCycle = ref.nCycle;
		dCenterX = ref.dCenterX;
		dCenterY = ref.dCenterY;
		
		for (UINT nIdx = 0; nIdx < OpticalCenter_Max; nIdx++)
		{
			dOffset[nIdx] = ref.dOffset[nIdx];

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_OpticalCenter, *PST_UI_OpticalCenter;

#pragma pack(pop)

#endif // Def_OpticalCenter_h__
