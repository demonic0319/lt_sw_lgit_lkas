#ifndef Wnd_Rst_Current_h__
#define Wnd_Rst_Current_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"

// CWnd_Rst_Current
typedef enum
{
	enCurrent_Result_CH1,
	enCurrent_Result_CH2,
	enCurrent_Result_CH3,
	enCurrent_Result_CH4,
	enCurrent_Result_CH5,
	enCurrent_Result_Max,

}enCurrent_Result_Num;

class CWnd_Rst_Current : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Current)

public:
	CWnd_Rst_Current();
	virtual ~CWnd_Rst_Current();

	UINT m_nHeaderCnt = enCurrent_Result_Max;
	
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic		m_st_Header[enCurrent_Result_Max];
	CVGStatic		m_st_Result[enCurrent_Result_Max];

public:

	void Result_Display			(__in UINT nResultIdx, __in UINT nIdx, __in  BOOL bReulst, __in  double dbValue);
	void Result_Reset			();
};

#endif // Wnd_Rst_SFR_h__
