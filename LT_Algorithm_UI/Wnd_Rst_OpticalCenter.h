#ifndef Wnd_Rst_OpticalCenter_h__
#define Wnd_Rst_OpticalCenter_h__

#pragma once

#include "afxwin.h"

#include "VGStatic.h"

// CWnd_Rst_OpticalCenter
typedef enum
{
	enOpticalCenter_Result_X,
	enOpticalCenter_Result_Y,
	enOpticalCenter_Result_Max,

}enOpticalCenter_Result_Num;

class CWnd_Rst_OpticalCenter : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_OpticalCenter)

public:
	CWnd_Rst_OpticalCenter();
	virtual ~CWnd_Rst_OpticalCenter();

	UINT m_nHeaderCnt = enOpticalCenter_Result_Max;
	
protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic		m_st_Header[enOpticalCenter_Result_Max];
	CVGStatic		m_st_Result[enOpticalCenter_Result_Max];

public:

	void Result_Display			(__in UINT nResultIdx, __in UINT nIdx, __in  BOOL bReulst, __in  double iValue);
	void Result_Reset			();
};

#endif // Wnd_Rst_SFR_h__
