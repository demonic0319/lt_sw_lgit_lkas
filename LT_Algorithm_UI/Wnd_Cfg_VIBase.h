//*****************************************************************************
// Filename	: 	Wnd_Cfg_VIBase.h
// Created	:	2018/1/26 - 17:57
// Modified	:	2018/1/26 - 17:57
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Wnd_Cfg_VIBase_h__
#define Wnd_Cfg_VIBase_h__

#pragma once

#include <afxwin.h>

//=============================================================================
// CWnd_Cfg_VIBase
//=============================================================================
class CWnd_Cfg_VIBase : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_VIBase)

public:
	CWnd_Cfg_VIBase();
	virtual ~CWnd_Cfg_VIBase();

	void SetOverlayID (__in UINT nWM_ID, __in UINT nItem)
	{
		m_wm_Overlay = nWM_ID;
		m_It_Overlay = nItem;
	}

	void SeChangeOption(__in UINT nWM_ID)
	{
		m_wm_ChangeOption = nWM_ID;
	}

protected:

	DECLARE_MESSAGE_MAP()

	// Tab Ctrl에서 활성화 될때 메세지
	UINT	m_It_Overlay		= 0;
	UINT	m_wm_Overlay		= NULL;
	UINT	m_wm_ChangeOption	= NULL;
};


#endif // Wnd_Cfg_VIBase_h__
