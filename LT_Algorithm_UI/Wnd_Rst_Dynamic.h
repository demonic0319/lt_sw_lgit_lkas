#ifndef Wnd_Rst_Dynamic_h__
#define Wnd_Rst_Dynamic_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_DynamicBW.h"

// CWnd_Rst_Dynamic
typedef enum
{
	enDyn_Result_Dynamic = 0,
	enDyn_Result_SNR,
	enDyn_Result_Max,

}enDynaminc_Result;

static	LPCTSTR	g_szDynamic_Result[] =
{
	_T("Dynamic Range"),
	_T("SNR BW"),
	NULL
};

class CWnd_Rst_Dynamic : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Dynamic)

public:
	CWnd_Rst_Dynamic();
	virtual ~CWnd_Rst_Dynamic();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enDyn_Result_Max];
	CVGStatic	m_st_Result[enDyn_Result_Max];

public:

	void Result_Display		(__in UINT nResultIdx, __in UINT nIdx,__in  BOOL bReulst, __in  double dbValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_Dynamic_h__
