//*****************************************************************************
// Filename	: 	Def_UI_Common.h
// Created	:	2018/1/27 - 16:47
// Modified	:	2018/1/27 - 16:47
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Def_UI_Common_h__
#define Def_UI_Common_h__

#include <afxwin.h>
#pragma pack(push,1)

typedef enum enOverlayDir
{
	OverlayDir_Left = 0,
	OverlayDir_Right,	
	OverlayDir_Top,		
	OverlayDir_Bottom	
};

static LPCTSTR	g_szOverayDir[] =
{
	_T("◀"),
	_T("▶"),
	_T("▲"),
	_T("▼"),
	NULL
};

typedef enum enDataForamt
{
	_DataFormat_YUV = 0,					// YUV
	_DataFormat_8BIT,					// 8bit Raw
	_DataFormat_10BIT,					// 10bit Packed Raw
	_DataFormat_PARALLEL_10BIT,			// 10bit Parallel Raw
	_DataFormat_12BIT,					// 12bit Packed Raw
	_DataFormat_PARALLEL_12BIT,			// 12bit Parallel Raw
};

static LPCTSTR	g_szDataForamt_Static[] =
{
	_T("YUV"),
	_T("8BIT"),
	_T("10BIT"),
	_T("PARALLEL 10BIT"),
	_T("12BIT"),
	_T("PARALLEL 12BIT"),
	NULL
};

typedef enum enOutMode
{
	_OUTMODE_BAYER_BGGR_YUV422_YCbYCr = 0,			// OutMode 가 YUV422_YcbYCr 인 영상
	_OUTMODE_BAYER_RGGB_YUV422_YCrYCb,				// OutMode 가 YUV422_ YCrYCb 인 영상
	_OUTMODE_BAYER_GBRG_YUV422_CbYCrY,				// OutMode 가 YUV422_ CbYCrY 인 영상
	_OUTMODE_BAYER_GRBG_YUV422_CrYCbY,				// OutMode 가 YUV422_ CrYCbY 인 영상
	_OUTMODE_BAYER_BLACKWHITE,						// OutMode 가 BGGR 인 영상
};

static LPCTSTR	g_szOutMode_Static[] =
{
	_T("YUV422 YCbYCr & BAYER BGGR"),
	_T("YUV422 YCrYCb & BAYER RGGB"),
	_T("YUV422 CbYCrY & BAYER GBRG"),
	_T("YUV422 CrYCbY & BAYER GRBG"),
	_T("BAYER BLACKWHITE"),
	NULL
};

typedef enum enSensorType
{
	_SENSORTYPE_RGGB = 0,				// RGGB 용으로 Interpolation 이 필요한 영상
	_SENSORTYPE_RCCC,					// RCCC 용으로 Interpolation 이 필요한 영상
	_SENSORTYPE_RCCB,					// RCCB 용으로 Interpolation 이 필요한 영상
	_SENSORTYPE_CCCC,					// CCCC 용으로 Interpolation 이 필요한 영상
	_SENSORTYPE_OTER,					// Bayer 이외의 출력 영상(YUV 등)
};

static LPCTSTR	g_szSensorType_Static[] =
{
	_T("RGGB"),
	_T("RCCC"),
	_T("RCCB"),
	_T("CCCC"),
	_T("OTER"),
	NULL
};

typedef enum enSFRType
{
	_ESFR_ISO12233,						///< ISO12233 algorithm type
	_ESFR_RHOMBUS, 						///< RHOMBUS algorithm type
	_ESFR_LGIT_ISO, 					///< Modified ISO12233 algorithm type
};

static LPCTSTR	g_szSFRType_Static[] =
{
	_T("ISO12233"),
	_T("RHOMBUS"),
	_T("LGIT ISO"),
	NULL
};

typedef enum enSFRMethod
{
	_ESFRMethod_Freq2SFR,				///< Calculate SFR value using Spatial Frequency
	_ESFRMethod_SFR2Freq,				///< Calculate Spatial Frequency using SFR value
};

typedef enum enSFRFrequencyUnit
{
	_ESFRFreq_CyclePerPixel,
	_ESFRFreq_LinePairPerMilliMeter,
	_ESFRFreq_LineWidthPerPictureHeight
};


static LPCTSTR	g_szSFRMethod_Static[] =
{
	_T("Freq2SFR"),
	_T("SFR2Freq"),
	NULL
};

typedef enum enSNRBWType
{
	_SNR_BW_IMAGE_WHITE,
	_SNR_BW_IMAGE_BLACK,
	_SNR_BW_IMAGE_GRAY,
};

static LPCTSTR	g_szSNRBWType_Static[] =
{
	_T("Image White"),
	_T("Image Black"),
	_T("Image Gray"),
	NULL
};

typedef enum enEdgeDir
{
	_EdgeDir_Vertical = 0,
	_EdgeDir_Horizontal,
	_EdgeDir_Max,
};

static LPCTSTR	g_szEdgeDir[] =
{
	_T("↕"),
	_T("↔"),
	NULL
};

static LPCTSTR	g_szEnableLog_Static[] =
{
	_T("NOT USE"),
	_T("USE"),
	NULL
};

static LPCTSTR	g_szEnableCircle_Static[] =
{
	_T("Disable"), //Main, Narrow Camera
	_T("Enable"), //Wide Camera
	NULL
};

static LPCTSTR	g_szEnable_Static[] =
{
	_T("Disable"), //Main, Narrow Camera
	_T("Enable"), //Wide Camera
	NULL
};

static LPCTSTR	g_szSpecUse_Static[] =
{
	_T("○"),
	_T("ⓥ"),
	NULL
};

typedef enum enMarkType
{
	MTyp_Circle = 0,
	MTyp_FiducialMark,
	MTyp_Max,
};

static LPCTSTR	g_szMarkType[] =
{
	_T("●"),
	_T("■"),
	NULL

};

typedef enum enMarkColor
{
	MCol_White = 0,
	MCol_Black,
	MCol_Max,
};

static LPCTSTR	g_szMarkColor[] =
{
	_T("○"),
	_T("●"),
	NULL
};

typedef struct _tag_Spec
{
	BOOL	bEnable;

	int		iValue;
	double	dbValue;

	void Reset()
	{
		bEnable = TRUE;
		iValue  = 0;
		dbValue = 0.0;
	};

	_tag_Spec()
	{
		Reset();
	};

	_tag_Spec& operator= (_tag_Spec& ref)
	{
		bEnable = ref.bEnable;

		iValue = ref.iValue;
		dbValue = ref.dbValue;

		return *this;
	};

}ST_Spec, *PST_Spec;

typedef struct _tag_Inspect
{
	enDataForamt	eDataForamt;		//	센서 Data Format
	enOutMode		eOutMode;			//	eOutMode 이미지 Interpolation을 위한 센서 Data Pattern
	enSensorType	eSensorType;		//	eSensorType 센서 종류
	int				iBlackLevel;		//	nBlackLevel DN 영상 신호의 Black level을 올바르게 보이기 위한 Calibration Value(default: 64)

	void Reset()
	{
		eDataForamt		= _DataFormat_YUV;
		eOutMode		= _OUTMODE_BAYER_BLACKWHITE;
		eSensorType		= _SENSORTYPE_RGGB;
		iBlackLevel		= 64;
	};

	_tag_Inspect()
	{
		Reset();
	};

	_tag_Inspect& operator= (_tag_Inspect& ref)
	{
		eDataForamt		= ref.eDataForamt;
		eOutMode		= ref.eOutMode;
		eSensorType		= ref.eSensorType;
		iBlackLevel		= ref.iBlackLevel;

		return *this;
	};

}ST_Inspect, *PST_Inspect_Com;
#pragma pack(pop)

#endif // Def_UI_Common_h__
