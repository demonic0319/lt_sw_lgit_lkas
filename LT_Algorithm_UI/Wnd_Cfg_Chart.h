﻿#ifndef Wnd_Cfg_Chart_h__
#define Wnd_Cfg_Chart_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Chart.h"
#include "List_Chart.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Chart
//-----------------------------------------------------------------------------
class CWnd_Cfg_Chart : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Chart)

public:
	CWnd_Cfg_Chart();
	virtual ~CWnd_Cfg_Chart();

	enum enChartStatic
	{
		STI_CUR_PARAM = 0,
		STI_CUR_MAX,
	};

	enum enChartButton
	{
		BTN_CUR_MAX = 1,
	};

	enum enChartComobox
	{
		CMB_CUR_MAX = 1,
	};

	enum enChartEdit
	{
		EDT_CUR_MAX = 1,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CList_Chart			m_List;

	CVGStatic			m_st_Item[STI_CUR_MAX];
	CButton				m_bn_Item[BTN_CUR_MAX];
	CComboBox			m_cb_Item[CMB_CUR_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_CUR_MAX];

	ST_UI_Chart*		m_pstConfig		= NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Chart* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Chart_h__
