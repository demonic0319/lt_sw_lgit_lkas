//*****************************************************************************
// Filename	: 	File_Config_VI.cpp
// Created	:	2018/1/26 - 14:58
// Modified	:	2018/1/26 - 14:58
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#include "File_Config_VI.h"

#define		AppName_Ymean			_T("Ymean")
#define		AppName_Dynamic			_T("DynamicBW")
#define		AppName_SFR				_T("SFR")
#define		AppName_Shading			_T("Shading")
#define		AppName_Current			_T("Current")
#define		AppName_Rllumination	_T("Rllumination")
#define		AppName_Chart			_T("Chart")
#define		AppName_DefectBlack		_T("DefectBlack")
#define		AppName_DefectWhite		_T("DefectWhite")
#define		AppName_OpticalCenter	_T("OpticalCenter")
#define		AppName_Rotation		_T("Rotation")
#define		AppName_BlackSpot		_T("BlackSpot")
#define		AppName_LCB				_T("LCB")
#define		AppName_Displace		_T("Displace")
#define		AppName_Vision			_T("Vision")
#define		AppName_IIC				_T("IIC")

CFile_VI_Config::CFile_VI_Config()
{
}


CFile_VI_Config::~CFile_VI_Config()
{
}

//=============================================================================
// Method		: Load_Common
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szAppName
// Parameter	: __in _tag_Inspect & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:15
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_Common(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in _tag_Inspect& stConfig)
{
	TCHAR   inBuff[255] = { 0, };

	GetPrivateProfileString(szAppName, _T("DataForamt"), _T("0"), inBuff, 80, szPath);
	stConfig.eDataForamt = (enDataForamt)_ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("OutMode"), _T("0"), inBuff, 80, szPath);
	stConfig.eOutMode = (enOutMode)_ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("SensorType"), _T("0"), inBuff, 80, szPath);
	stConfig.eSensorType = (enSensorType)_ttoi(inBuff);

	GetPrivateProfileString(szAppName, _T("BlackLevel"), _T("0"), inBuff, 80, szPath);
	stConfig.iBlackLevel = _ttoi(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Save_Common
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szAppName
// Parameter	: __in const _tag_Inspect * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:16
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_Common(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in const _tag_Inspect* pstConfig)
{
	CString szValue;

	szValue.Format(_T("%d"), pstConfig->eDataForamt);
	WritePrivateProfileString(szAppName, _T("DataForamt"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->eOutMode);
	WritePrivateProfileString(szAppName, _T("OutMode"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->eSensorType);
	WritePrivateProfileString(szAppName, _T("SensorType"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iBlackLevel);
	WritePrivateProfileString(szAppName, _T("BlackLevel"), szValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_YmeanFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Ymean & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:18
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_YmeanFile(__in LPCTSTR szPath, __out ST_UI_Ymean& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Ymean;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("DefectBlockSize"), _T("32"), inBuff, 255, szPath);
	stConfig.iDefectBlockSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("EdgeSize"), _T("100"), inBuff, 255, szPath);
	stConfig.iEdgeSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("CenterThreshold"), _T("50.0"), inBuff, 255, szPath);
	stConfig.fCenterThreshold = (float)_ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("EdgeThreshold"), _T("50.0"), inBuff, 255, szPath);
	stConfig.fEdgeThreshold = (float)_ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("CornerThreshold"), _T("50.0"), inBuff, 255, szPath);
	stConfig.fCornerThreshold = (float)_ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("LscBlockSize"), _T("128"), inBuff, 255, szPath);
	stConfig.iLscBlockSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("EnableCircle"), _T("0"), inBuff, 255, szPath);
	stConfig.bEnableCircle = (1 == _ttoi(inBuff)) ? true : false;

	GetPrivateProfileString(strAppName, _T("PosOffsetX"), _T("5"), inBuff, 255, szPath);
	stConfig.iPosOffsetX = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("PosOffsetY"), _T("5"), inBuff, 255, szPath);
	stConfig.iPosOffsetY = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("RadiusRatioY"), _T("5.0"), inBuff, 255, szPath);
	stConfig.dbRadiusRatioY = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("RadiusRatioX"), _T("5.0"), inBuff, 255, szPath);
	stConfig.dbRadiusRatioX = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Enable8bit"), _T("0"), inBuff, 255, szPath);
	stConfig.b8BitUse = (1 == _ttoi(inBuff)) ? true : false;

	for (UINT nSpec = 0; nSpec < Spec_Ymean_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Ymean, nSpec);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].iValue = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_YmeanFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_Ymean * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_YmeanFile(__in LPCTSTR szPath, __in const ST_UI_Ymean* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Ymean;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%d"), pstConfig->iDefectBlockSize);
	WritePrivateProfileString(strAppName, _T("DefectBlockSize"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iEdgeSize);
	WritePrivateProfileString(strAppName, _T("EdgeSize"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->fCenterThreshold);
	WritePrivateProfileString(strAppName, _T("CenterThreshold"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->fEdgeThreshold);
	WritePrivateProfileString(strAppName, _T("EdgeThreshold"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->fCornerThreshold);
	WritePrivateProfileString(strAppName, _T("CornerThreshold"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iLscBlockSize);
	WritePrivateProfileString(strAppName, _T("LscBlockSize"), szValue, szPath); 

	szValue.Format(_T("%d"), pstConfig->bEnableCircle);
	WritePrivateProfileString(strAppName, _T("EnableCircle"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iPosOffsetX);
	WritePrivateProfileString(strAppName, _T("PosOffsetX"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iPosOffsetY);
	WritePrivateProfileString(strAppName, _T("PosOffsetY"), szValue, szPath);

	szValue.Format(_T("%.1f"), pstConfig->dbRadiusRatioX);
	WritePrivateProfileString(strAppName, _T("RadiusRatioX"), szValue, szPath);

	szValue.Format(_T("%.1f"), pstConfig->dbRadiusRatioY);
	WritePrivateProfileString(strAppName, _T("RadiusRatioY"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->b8BitUse);
	WritePrivateProfileString(strAppName, _T("Enable8bit"), szValue, szPath);

	for (UINT nSpec = 0; nSpec < Spec_Ymean_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Ymean, nSpec);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_DynamicBWFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_DynamicBW & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_DynamicBWFile(__in LPCTSTR szPath, __out ST_UI_DynamicBW& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Dynamic;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("MaxROICount"), _T("0"), inBuff, 255, szPath);
	stConfig.iMaxROICount = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("OffsetDy"), _T("0"), inBuff, 255, szPath);
	stConfig.dbOffsetDR = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("OffsetBW"), _T("0"), inBuff, 255, szPath);
	stConfig.dbOffsetSNR = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxROIWidth"), _T("40"), inBuff, 255, szPath);
	stConfig.iMaxROIWidth = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxROIHeight "), _T("40"), inBuff, 255, szPath);
	stConfig.iMaxROIHeight = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("LscBlockSize "), _T("0"), inBuff, 255, szPath);
	stConfig.iLscBlockSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Enable8bit"), _T("0"), inBuff, 255, szPath);
	stConfig.b8BitUse = (1 == _ttoi(inBuff)) ? true : false;

	GetPrivateProfileString(strAppName, _T("DRThreshold"), _T("100"), inBuff, 255, szPath);
	stConfig.dbDRThreshold = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("SNRThreshold"), _T("100"), inBuff, 255, szPath);
	stConfig.dbSNRThreshold = _ttof(inBuff);


	for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_Dynamic, nROI);

		GetPrivateProfileString(strAppName, _T("CenterX"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].ptROI.x = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("CenterY"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].ptROI.y = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("OverlayDir"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].nOverlayDir = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("SNRType"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].eSNRType = (enSNRBWType)_ttoi(inBuff);
	}
	
	for (UINT nSpec = 0; nSpec < Spec_DynamicBW_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Dynamic, nSpec);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_DynamicBWFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_DynamicBW * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_DynamicBWFile(__in LPCTSTR szPath, __in const ST_UI_DynamicBW* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Dynamic;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%d"), pstConfig->iMaxROICount);
	WritePrivateProfileString(strAppName, _T("MaxROICount"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dbOffsetSNR);
	WritePrivateProfileString(strAppName, _T("OffsetBW"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dbOffsetDR);
	WritePrivateProfileString(strAppName, _T("OffsetDy"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iMaxROIWidth);
	WritePrivateProfileString(strAppName, _T("MaxROIWidth"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iMaxROIHeight);
	WritePrivateProfileString(strAppName, _T("MaxROIHeight"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->b8BitUse);
	WritePrivateProfileString(strAppName, _T("Enable8bit"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iLscBlockSize);
	WritePrivateProfileString(strAppName, _T("LscBlockSize"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dbDRThreshold);
	WritePrivateProfileString(strAppName, _T("DRThreshold"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dbSNRThreshold);
	WritePrivateProfileString(strAppName, _T("SNRThreshold"), szValue, szPath);

	for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_Dynamic, nROI);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].ptROI.x);
		WritePrivateProfileString(strAppName, _T("CenterX"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].ptROI.y);
		WritePrivateProfileString(strAppName, _T("CenterY"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].bEnable);
		WritePrivateProfileString(strAppName, _T("Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].nOverlayDir);
		WritePrivateProfileString(strAppName, _T("OverlayDir"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].eSNRType);
		WritePrivateProfileString(strAppName, _T("SNRType"), szValue, szPath);
	}

	for (UINT nSpec = 0; nSpec < Spec_DynamicBW_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Dynamic, nSpec);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMin[nSpec].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMax[nSpec].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}
	
	return TRUE;
}

//=============================================================================
// Method		: Load_SFRFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_SFR & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_SFRFile(__in LPCTSTR szPath, __out ST_UI_SFR& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_SFR;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("MaxEdgeAngle"), _T("0"), inBuff, 255, szPath);
	stConfig.dbMaxEdgeAngle = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("PixelSize"), _T("0"), inBuff, 255, szPath);
	stConfig.dbPixelSize = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("AverageCount"), _T("0"), inBuff, 255, szPath);
	stConfig.nAverageCount = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Type"), _T("0"), inBuff, 255, szPath);
	stConfig.eType = (enSFRType)_ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Method"), _T("0"), inBuff, 255, szPath);
	stConfig.eMethod = (enSFRMethod)_ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("EnableLog"), _T("0"), inBuff, 255, szPath);
	stConfig.bEnableLog = (1 == _ttoi(inBuff)) ? true : false;

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_SFR, nROI);

		GetPrivateProfileString(strAppName, _T("left"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].rtROI.left = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("right"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].rtROI.right = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("top"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].rtROI.top = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("bottom"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].rtROI.bottom = _ttoi(inBuff);
		
		GetPrivateProfileString(strAppName, _T("EdgeDir"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].iEdgeDir = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("FrequencyLists"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].dbFrequencyLists = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Offset"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].dbOffset = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("SFR"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].dbSFR = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("FrequencyNum"), _T("1"), inBuff, 255, szPath);
		stConfig.stInput[nROI].iFrequencyNum = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("OverlayDir"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].nOverlayDir = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].stSpecMin.bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].stSpecMax.bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].stSpecMin.dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nROI].stSpecMax.dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_SFRFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_SFR * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_SFRFile(__in LPCTSTR szPath, __in const ST_UI_SFR* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_SFR;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%.2f"), pstConfig->dbMaxEdgeAngle);
	WritePrivateProfileString(strAppName, _T("MaxEdgeAngle"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dbPixelSize);
	WritePrivateProfileString(strAppName, _T("PixelSize"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nAverageCount);
	WritePrivateProfileString(strAppName, _T("AverageCount"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->eType);
	WritePrivateProfileString(strAppName, _T("Type"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->eMethod);
	WritePrivateProfileString(strAppName, _T("Method"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->bEnableLog);
	WritePrivateProfileString(strAppName, _T("EnableLog"), szValue, szPath);

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_SFR, nROI);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].rtROI.left);
		WritePrivateProfileString(strAppName, _T("left"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].rtROI.right);
		WritePrivateProfileString(strAppName, _T("right"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].rtROI.top);
		WritePrivateProfileString(strAppName, _T("top"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].rtROI.bottom);
		WritePrivateProfileString(strAppName, _T("bottom"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].bEnable);
		WritePrivateProfileString(strAppName, _T("Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].iEdgeDir);
		WritePrivateProfileString(strAppName, _T("EdgeDir"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stInput[nROI].dbFrequencyLists);
		WritePrivateProfileString(strAppName, _T("FrequencyLists"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stInput[nROI].dbSFR);
		WritePrivateProfileString(strAppName, _T("SFR"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].iFrequencyNum);
		WritePrivateProfileString(strAppName, _T("FrequencyNum"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].nOverlayDir);
		WritePrivateProfileString(strAppName, _T("OverlayDir"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stInput[nROI].dbOffset);
		WritePrivateProfileString(strAppName, _T("Offset"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].stSpecMin.bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stInput[nROI].stSpecMax.bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stInput[nROI].stSpecMin.dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stInput[nROI].stSpecMax.dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_ShadingFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Shading & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_ShadingFile(__in LPCTSTR szPath, __out ST_UI_Shading& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Shading;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("MaxROIWidth"), _T("40"), inBuff, 255, szPath);
	stConfig.iMaxROIWidth = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxROIHeight "), _T("40"), inBuff, 255, szPath);
	stConfig.iMaxROIHeight = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxROICount"), _T("3"), inBuff, 255, szPath);
	stConfig.iMaxROICount = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("NormalizeIndex"), _T("0"), inBuff, 255, szPath);
	stConfig.iNormalizeIndex = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("dTestField_1"), _T("0.65"), inBuff, 255, szPath);
	stConfig.dTestField_1 = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("dTestField_2"), _T("0.85"), inBuff, 255, szPath);
	stConfig.dTestField_2 = _ttof(inBuff);

	for (UINT nCh = 0; nCh < enUI_ShadingField_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Shading, nCh);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_ShadingFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_Shading * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_ShadingFile(__in LPCTSTR szPath, __in const ST_UI_Shading* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Shading;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%d"), pstConfig->iMaxROIWidth);
	WritePrivateProfileString(strAppName, _T("MaxROIWidth"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iMaxROIHeight);
	WritePrivateProfileString(strAppName, _T("MaxROIHeight"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iMaxROICount);
	WritePrivateProfileString(strAppName, _T("MaxROICount"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iNormalizeIndex);
	WritePrivateProfileString(strAppName, _T("NormalizeIndex"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dTestField_1);
	WritePrivateProfileString(strAppName, _T("dTestField_1"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dTestField_2);
	WritePrivateProfileString(strAppName, _T("dTestField_2"), szValue, szPath);

	for (UINT nCh = 0; nCh < enUI_ShadingField_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Shading, nCh);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMin[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMax[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_CurrentFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Current & stConfig
// Qualifier	:
// Last Update	: 2018/8/8 - 20:21
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_CurrentFile(__in LPCTSTR szPath, __out ST_UI_Current& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Current;

	for (UINT nCh = 0; nCh < Current_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Current, nCh);

		GetPrivateProfileString(strAppName, _T("Offset"), _T("1.0"), inBuff, 255, szPath);
		stConfig.dbOffset[nCh] = _ttof(inBuff);
	
		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_CurrentFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_Current * pstConfig
// Qualifier	:
// Last Update	: 2018/8/8 - 20:21
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_CurrentFile(__in LPCTSTR szPath, __in const ST_UI_Current* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Current;

	for (UINT nCh = 0; nCh < Current_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Current, nCh);

		szValue.Format(_T("%.2f"), pstConfig->dbOffset[nCh]);
		WritePrivateProfileString(strAppName, _T("Offset"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMin[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMax[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_RlluminationFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Ymean & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:18
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_RlluminationFile(__in LPCTSTR szPath, __out ST_UI_Rllumination& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Rllumination;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("Enable8bit"), _T("0"), inBuff, 255, szPath);
	stConfig.b8BitUse = (1 == _ttoi(inBuff)) ? true : false;

	GetPrivateProfileString(strAppName, _T("BoxSize"), _T("150"), inBuff, 255, szPath);
	stConfig.nBoxSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("BoxField"), _T("0.8"), inBuff, 255, szPath);
	stConfig.dBoxField = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Corneroffset"), _T("0.1"), inBuff, 255, szPath);
	stConfig.dbCorneroffset = _ttof(inBuff);
	
	GetPrivateProfileString(strAppName, _T("Minoffset"), _T("0.1"), inBuff, 255, szPath);
	stConfig.dbMinoffset = _ttof(inBuff);

	for (UINT nSpec = 0; nSpec < Spec_RI_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Rllumination, nSpec);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_RlluminationFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_DynamicBW * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:27
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_RlluminationFile(__in LPCTSTR szPath, __in const ST_UI_Rllumination* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Rllumination;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%d"), pstConfig->b8BitUse);
	WritePrivateProfileString(strAppName, _T("Enable8bit"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nBoxSize);
	WritePrivateProfileString(strAppName, _T("BoxSize"), szValue, szPath);

	szValue.Format(_T("%.3f"), pstConfig->dBoxField);
	WritePrivateProfileString(strAppName, _T("BoxField"), szValue, szPath);

	szValue.Format(_T("%.3f"), pstConfig->dbCorneroffset);
	WritePrivateProfileString(strAppName, _T("Corneroffset"), szValue, szPath);

	szValue.Format(_T("%.3f"), pstConfig->dbMinoffset);
	WritePrivateProfileString(strAppName, _T("Minoffset"), szValue, szPath);


	for (UINT nSpec = 0; nSpec < Spec_RI_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Rllumination, nSpec);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMin[nSpec].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMax[nSpec].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_ChartFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Chart & stConfig
// Qualifier	:
// Last Update	: 2018/8/23 - 17:06
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_ChartFile(__in LPCTSTR szPath, __out ST_UI_Chart& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Chart;

	GetPrivateProfileString(strAppName, _T("MarkColor"), _T("0"), inBuff, 255, szPath);
	stConfig.nMarkColor = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("Brightness"), _T("30"), inBuff, 255, szPath);
	stConfig.nBrightness = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("left"), _T("0"), inBuff, 255, szPath);
	stConfig.rtROI.left = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("right"), _T("0"), inBuff, 255, szPath);
	stConfig.rtROI.right = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("top"), _T("0"), inBuff, 255, szPath);
	stConfig.rtROI.top = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("bottom"), _T("0"), inBuff, 255, szPath);
	stConfig.rtROI.bottom = _ttoi(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: Save_ChartFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_Chart * pstConfig
// Qualifier	:
// Last Update	: 2018/8/23 - 17:08
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_ChartFile(__in LPCTSTR szPath, __in const ST_UI_Chart* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Chart;

	szValue.Format(_T("%d"), pstConfig->nMarkColor);
	WritePrivateProfileString(strAppName, _T("MarkColor"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nBrightness);
	WritePrivateProfileString(strAppName, _T("Brightness"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->rtROI.left);
	WritePrivateProfileString(strAppName, _T("left"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->rtROI.right);
	WritePrivateProfileString(strAppName, _T("right"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->rtROI.top);
	WritePrivateProfileString(strAppName, _T("top"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->rtROI.bottom);
	WritePrivateProfileString(strAppName, _T("bottom"), szValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_Defect_BlackFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Ymean & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:18
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_Defect_BlackFile(__in LPCTSTR szPath, __out ST_UI_Defect_Black& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_DefectBlack;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("BlockSize"), _T("0"), inBuff, 255, szPath);
	stConfig.nBlockSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxSingleDefectNum"), _T("0"), inBuff, 255, szPath);
	stConfig.nMaxSingleDefectNum = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("DefectRatio"), _T("1.0"), inBuff, 255, szPath);
	stConfig.dbDefectRatio = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Enable8bit"), _T("0"), inBuff, 255, szPath);
	stConfig.b8BitUse = (1 == _ttoi(inBuff)) ? true : false;

	for (UINT nSpec = 0; nSpec < Spec_Defect_Black_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_DefectBlack, nSpec);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].iValue = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_DefectBlackFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_Ymean * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_DefectBlackFile(__in LPCTSTR szPath, __in const ST_UI_Defect_Black* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_DefectBlack;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%d"), pstConfig->nBlockSize);
	WritePrivateProfileString(strAppName, _T("BlockSize"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nMaxSingleDefectNum);
	WritePrivateProfileString(strAppName, _T("MaxSingleDefectNum"), szValue, szPath);

	szValue.Format(_T("%.3f"), pstConfig->dbDefectRatio);
	WritePrivateProfileString(strAppName, _T("DefectRatio"), szValue, szPath);


	szValue.Format(_T("%d"), pstConfig->b8BitUse);
	WritePrivateProfileString(strAppName, _T("Enable8bit"), szValue, szPath);

	for (UINT nSpec = 0; nSpec < Spec_Defect_Black_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_DefectBlack, nSpec);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_Defect_WhiteFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Ymean & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:18
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_Defect_WhiteFile(__in LPCTSTR szPath, __out ST_UI_Defect_White& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_DefectWhite;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("BlockSize"), _T("0"), inBuff, 255, szPath);
	stConfig.nBlockSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxSingleDefectNum"), _T("0"), inBuff, 255, szPath);
	stConfig.nMaxSingleDefectNum = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("DefectRatio"), _T("1.0"), inBuff, 255, szPath);
	stConfig.dbDefectRatio = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Enable8bit"), _T("0"), inBuff, 255, szPath);
	stConfig.b8BitUse = (1 == _ttoi(inBuff)) ? true : false;

	for (UINT nSpec = 0; nSpec < Spec_Defect_White_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_DefectWhite, nSpec);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].iValue = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_DefectWhiteFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_Ymean * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_DefectWhiteFile(__in LPCTSTR szPath, __in const ST_UI_Defect_White* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_DefectWhite;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%d"), pstConfig->nBlockSize);
	WritePrivateProfileString(strAppName, _T("BlockSize"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nMaxSingleDefectNum);
	WritePrivateProfileString(strAppName, _T("MaxSingleDefectNum"), szValue, szPath);

	szValue.Format(_T("%.3f"), pstConfig->dbDefectRatio);
	WritePrivateProfileString(strAppName, _T("DefectRatio"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->b8BitUse);
	WritePrivateProfileString(strAppName, _T("Enable8bit"), szValue, szPath);

	for (UINT nSpec = 0; nSpec < Spec_Defect_White_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_DefectWhite, nSpec);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}


//=============================================================================
// Method		: Load_OpticalCenterFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_OpticalCenter_Opt & stConfig
// Qualifier	:
// Last Update	: 2018/2/18 - 12:33
// Desc.		:
//=============================================================================

//BOOL CFile_VI_Config::Load_OpticalCenterFile(__in LPCTSTR szPath, __out ST_OpticalCenter_Opt& stConfig)
//{
//	if (NULL == szPath)
//		return FALSE;
//
//	TCHAR   inBuff[255] = { 0, };
//
//	CString	strAppName = AppName_OpticalCenter;
//
//	GetPrivateProfileString(strAppName, _T("StandarX"), _T("0"), inBuff, 255, szPath);
//	stConfig.nStandarX = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("StandarY"), _T("0"), inBuff, 255, szPath);
//	stConfig.nStandarY = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("OffsetX"), _T("0"), inBuff, 255, szPath);
//	stConfig.iOffsetX = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("OffsetY"), _T("0"), inBuff, 255, szPath);
//	stConfig.iOffsetY = _ttoi(inBuff);
//
//	for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
//	{
//		strAppName.Format(_T("%s_SPEC_%d"), AppName_OpticalCenter, nIdx);
//
//		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
//		stConfig.stSpec_Min[nIdx].bEnable = _ttoi(inBuff);
//
//		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
//		stConfig.stSpec_Min[nIdx].iValue = _ttoi(inBuff);
//
//		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
//		stConfig.stSpec_Min[nIdx].dbValue = _ttof(inBuff);
//
//		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
//		stConfig.stSpec_Max[nIdx].bEnable = _ttoi(inBuff);
//
//		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
//		stConfig.stSpec_Max[nIdx].iValue = _ttoi(inBuff);
//
//		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
//		stConfig.stSpec_Max[nIdx].dbValue = _ttof(inBuff);
//	}
//
//	// ROI AuAa
//	strAppName.Format(_T("%s_ROI_0"), AppName_OpticalCenter);
//	GetPrivateProfileString(strAppName, _T("left"), _T("0"), inBuff, 255, szPath);
//	stConfig.rtROI.left = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("right"), _T("0"), inBuff, 255, szPath);
//	stConfig.rtROI.right = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("top"), _T("0"), inBuff, 255, szPath);
//	stConfig.rtROI.top = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("bottom"), _T("0"), inBuff, 255, szPath);
//	stConfig.rtROI.bottom = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("MarkType"), _T("0"), inBuff, 255, szPath);
//	stConfig.nMarkType = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("MarkColor"), _T("0"), inBuff, 255, szPath);
//	stConfig.nMarkColor = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("Brightness"), _T("0"), inBuff, 255, szPath);
//	stConfig.nBrightness = _ttoi(inBuff);
//
//	GetPrivateProfileString(strAppName, _T("Sharpness"), _T("0"), inBuff, 255, szPath);
//	stConfig.nSharpness = _ttoi(inBuff);
//
//	return TRUE;
//}

//=============================================================================
// Method		: Save_OpticalCenterFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_OpticalCenter_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2018/2/18 - 12:44
// Desc.		:
//=============================================================================
//BOOL CFile_VI_Config::Save_OpticalCenterFile(__in LPCTSTR szPath, __in const ST_OpticalCenter_Opt* pstConfig)
//{
//	if (NULL == szPath)
//		return FALSE;
//
//	if (NULL == pstConfig)
//		return FALSE;
//
//	CString strValue;
//	CString	strAppName = AppName_OpticalCenter;
//
//	strValue.Format(_T("%d"), pstConfig->nStandarX);
//	WritePrivateProfileString(strAppName, _T("StandarX"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->nStandarY);
//	WritePrivateProfileString(strAppName, _T("StandarY"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->iOffsetX);
//	WritePrivateProfileString(strAppName, _T("OffsetX"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->iOffsetY);
//	WritePrivateProfileString(strAppName, _T("OffsetY"), strValue, szPath);
//
//	for (UINT nIdx = 0; nIdx < Spec_OC_Max; nIdx++)
//	{
//		strAppName.Format(_T("%s_SPEC_%d"), AppName_OpticalCenter, nIdx);
//
//		strValue.Format(_T("%d"), pstConfig->stSpec_Min[nIdx].bEnable);
//		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);
//
//		strValue.Format(_T("%d"), pstConfig->stSpec_Min[nIdx].iValue);
//		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);
//
//		strValue.Format(_T("%.2f"), pstConfig->stSpec_Min[nIdx].dbValue);
//		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);
//
//		strValue.Format(_T("%d"), pstConfig->stSpec_Max[nIdx].bEnable);
//		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);
//
//		strValue.Format(_T("%d"), pstConfig->stSpec_Max[nIdx].iValue);
//		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);
//
//		strValue.Format(_T("%.2f"), pstConfig->stSpec_Max[nIdx].dbValue);
//		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
//	}
//
//	// ROI AuAa
//	strAppName.Format(_T("%s_ROI_0"), AppName_OpticalCenter);
//
//	strValue.Format(_T("%d"), pstConfig->rtROI.left);
//	WritePrivateProfileString(strAppName, _T("left"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->rtROI.right);
//	WritePrivateProfileString(strAppName, _T("right"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->rtROI.top);
//	WritePrivateProfileString(strAppName, _T("top"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->rtROI.bottom);
//	WritePrivateProfileString(strAppName, _T("bottom"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->nMarkType);
//	WritePrivateProfileString(strAppName, _T("MarkType"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->nMarkColor);
//	WritePrivateProfileString(strAppName, _T("MarkColor"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->nBrightness);
//	WritePrivateProfileString(strAppName, _T("Brightness"), strValue, szPath);
//
//	strValue.Format(_T("%d"), pstConfig->nSharpness);
//	WritePrivateProfileString(strAppName, _T("Sharpness"), strValue, szPath);
//
//	return TRUE;
//}

//=============================================================================
// Method		: Load_RotateFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_Rotate_Opt & stConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:45
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_RotateFile(__in LPCTSTR szPath, __out ST_UI_Rotation& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppTotal = AppName_Rotation;
	CString	strAppName;

	Load_Common(szPath, strAppTotal, stConfig.stInspect);

	GetPrivateProfileString(strAppTotal, _T("ROIBoxSize"), _T("0"), inBuff, 80, szPath);
	stConfig.nROIBoxSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppTotal, _T("MaxROIBoxSize"), _T("0"), inBuff, 80, szPath);
	stConfig.nMaxROIBoxSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppTotal, _T("Radius"), _T("0"), inBuff, 80, szPath);
	stConfig.dRadius = _ttof(inBuff);

	GetPrivateProfileString(strAppTotal, _T("RealGapX"), _T("0"), inBuff, 80, szPath);
	stConfig.dRealGapX = _ttof(inBuff);

	GetPrivateProfileString(strAppTotal, _T("RealGapY"), _T("0"), inBuff, 80, szPath);
	stConfig.dRealGapY = _ttof(inBuff);

	GetPrivateProfileString(strAppTotal, _T("ModuleChartDistance"), _T("0"), inBuff, 80, szPath);
	stConfig.dModuleChartDistance = _ttof(inBuff);

	GetPrivateProfileString(strAppTotal, _T("OffsetRot"), _T("1"), inBuff, 80, szPath);
	stConfig.dbOffsetRot = _ttof(inBuff);

	for (UINT nIdx = 0; nIdx < Spec_Rotation_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_Rotation, nIdx);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_nValue"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nIdx].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nIdx].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_nValue"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nIdx].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_dbValue"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nIdx].dbValue = _ttof(inBuff);
	}


	// ROI AuAa
	for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_Rotation, nIdx);

		GetPrivateProfileString(strAppName, _T("left"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].rtROI.left = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("right"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].rtROI.right = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("top"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].rtROI.top = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("bottom"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].rtROI.bottom = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("MarkType"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].nMarkType = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("MarkColor"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].nMarkColor = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Brightness"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].nBrightness = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Sharpness"), _T("0"), inBuff, 255, szPath);
		stConfig.stInput[nIdx].nSharpness = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_RotateFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_Rotate_Opt * pstConfigInfo
// Qualifier	:
// Last Update	: 2017/12/3 - 13:56
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_RotateFile(__in LPCTSTR szPath, __in const ST_UI_Rotation* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString strValue;
	CString	strAppName = AppName_Rotation;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	strValue.Format(_T("%d"), pstConfig->nROIBoxSize);
	WritePrivateProfileString(strAppName, _T("ROIBoxSize"), strValue, szPath);

	strValue.Format(_T("%d"), pstConfig->nMaxROIBoxSize);
	WritePrivateProfileString(strAppName, _T("MaxROIBoxSize"), strValue, szPath);

	strValue.Format(_T("%.3f"), pstConfig->dRadius);
	WritePrivateProfileString(strAppName, _T("Radius"), strValue, szPath);

	strValue.Format(_T("%.3f"), pstConfig->dRealGapX);
	WritePrivateProfileString(strAppName, _T("RealGapX"), strValue, szPath);

	strValue.Format(_T("%.3f"), pstConfig->dRealGapY);
	WritePrivateProfileString(strAppName, _T("RealGapY"), strValue, szPath);

	strValue.Format(_T("%.3f"), pstConfig->dModuleChartDistance);
	WritePrivateProfileString(strAppName, _T("ModuleChartDistance"), strValue, szPath);

	strValue.Format(_T("%.3f"), pstConfig->dbOffsetRot);
	WritePrivateProfileString(strAppName, _T("OffsetRot"), strValue, szPath);

	for (UINT nIdx = 0; nIdx < Spec_Rotation_MAX; nIdx++)
	{
		strAppName.Format(_T("%s_SPEC_%d"), AppName_Rotation, nIdx);

		strValue.Format(_T("%d"), pstConfig->stSpecMin[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stSpecMin[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Min_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfig->stSpecMin[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_dbValue"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stSpecMax[nIdx].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stSpecMax[nIdx].iValue);
		WritePrivateProfileString(strAppName, _T("Max_nValue"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstConfig->stSpecMax[nIdx].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_dbValue"), strValue, szPath);
	}
	

	// ROI AuAa
	for (UINT nIdx = 0; nIdx < ROI_Rot_Max; nIdx++)
	{
		strAppName.Format(_T("%s_ROI_%d"), AppName_Rotation, nIdx);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].rtROI.left);
		WritePrivateProfileString(strAppName, _T("left"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].rtROI.right);
		WritePrivateProfileString(strAppName, _T("right"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].rtROI.top);
		WritePrivateProfileString(strAppName, _T("top"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].rtROI.bottom);
		WritePrivateProfileString(strAppName, _T("bottom"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].nMarkType);
		WritePrivateProfileString(strAppName, _T("MarkType"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].nMarkColor);
		WritePrivateProfileString(strAppName, _T("MarkColor"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].nBrightness);
		WritePrivateProfileString(strAppName, _T("Brightness"), strValue, szPath);

		strValue.Format(_T("%d"), pstConfig->stInput[nIdx].nSharpness);
		WritePrivateProfileString(strAppName, _T("Sharpness"), strValue, szPath);
	}

	return TRUE;
}


//=============================================================================
// Method		: Load_BlackSpotFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_Ymean & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:18
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_BlackSpotFile(__in LPCTSTR szPath, __out ST_UI_BlackSpot& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_BlackSpot;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("BlockWidth"), _T("32"), inBuff, 255, szPath);
	stConfig.nBlockWidth = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("BlockHeight"), _T("100"), inBuff, 255, szPath);
	stConfig.nBlockHeight = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("ClusterSize"), _T("50.0"), inBuff, 255, szPath);
	stConfig.nClusterSize = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("DefectInCluster"), _T("50.0"), inBuff, 255, szPath);
	stConfig.nDefectInCluster = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("DefectRatio"), _T("50.0"), inBuff, 255, szPath);
	stConfig.dDefectRatio = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxSingleDefectNum"), _T("128"), inBuff, 255, szPath);
	stConfig.nMaxSingleDefectNum = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("EnableCircle"), _T("0"), inBuff, 255, szPath);
	stConfig.bEnableCircle = (1 == _ttoi(inBuff)) ? true : false;

	GetPrivateProfileString(strAppName, _T("PosOffsetX"), _T("5"), inBuff, 255, szPath);
	stConfig.iPosOffsetX = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("PosOffsetY"), _T("5"), inBuff, 255, szPath);
	stConfig.iPosOffsetY = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("RadiusRatioY"), _T("5.0"), inBuff, 255, szPath);
	stConfig.dbRadiusRatioY = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("RadiusRatioX"), _T("5.0"), inBuff, 255, szPath);
	stConfig.dbRadiusRatioX = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Enable8bit"), _T("0"), inBuff, 255, szPath);
	stConfig.b8BitUse = (1 == _ttoi(inBuff)) ? true : false;

	for (UINT nSpec = 0; nSpec < Spec_BlackSpot_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_BlackSpot, nSpec);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].iValue = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_BlackSpotFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_Ymean * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_BlackSpotFile(__in LPCTSTR szPath, __in const ST_UI_BlackSpot* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_BlackSpot;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%d"), pstConfig->nBlockWidth);
	WritePrivateProfileString(strAppName, _T("BlockWidth"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nBlockHeight);
	WritePrivateProfileString(strAppName, _T("BlockHeight"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nClusterSize);
	WritePrivateProfileString(strAppName, _T("ClusterSize"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nDefectInCluster);
	WritePrivateProfileString(strAppName, _T("DefectInCluster"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dDefectRatio);
	WritePrivateProfileString(strAppName, _T("DefectRatio"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nMaxSingleDefectNum);
	WritePrivateProfileString(strAppName, _T("MaxSingleDefectNum"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->bEnableCircle);
	WritePrivateProfileString(strAppName, _T("EnableCircle"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iPosOffsetX);
	WritePrivateProfileString(strAppName, _T("PosOffsetX"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iPosOffsetY);
	WritePrivateProfileString(strAppName, _T("PosOffsetY"), szValue, szPath);

	szValue.Format(_T("%.1f"), pstConfig->dbRadiusRatioX);
	WritePrivateProfileString(strAppName, _T("RadiusRatioX"), szValue, szPath);

	szValue.Format(_T("%.1f"), pstConfig->dbRadiusRatioY);
	WritePrivateProfileString(strAppName, _T("RadiusRatioY"), szValue, szPath);


	szValue.Format(_T("%d"), pstConfig->b8BitUse);
	WritePrivateProfileString(strAppName, _T("Enable8bit"), szValue, szPath);

	for (UINT nSpec = 0; nSpec < Spec_BlackSpot_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_BlackSpot, nSpec);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_LCBFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_UI_LCB & stConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:18
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Load_LCBFile(__in LPCTSTR szPath, __out ST_UI_LCB& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_LCB;

	Load_Common(szPath, strAppName, stConfig.stInspect);

	GetPrivateProfileString(strAppName, _T("CenterThreshold"), _T("32"), inBuff, 255, szPath);
	stConfig.dCenterThreshold = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("EdgeThreshold"), _T("100"), inBuff, 255, szPath);
	stConfig.dEdgeThreshold = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("CornerThreshold"), _T("50.0"), inBuff, 255, szPath);
	stConfig.dCornerThreshold = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("MaxSingleDefectNum"), _T("50.0"), inBuff, 255, szPath);
	stConfig.nMaxSingleDefectNum = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("MinDefectWidthHeight"), _T("50.0"), inBuff, 255, szPath);
	stConfig.nMinDefectWidthHeight = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("EnableCircle"), _T("0"), inBuff, 255, szPath);
	stConfig.bEnableCircle = (1 == _ttoi(inBuff)) ? true : false;

	GetPrivateProfileString(strAppName, _T("PosOffsetX"), _T("5"), inBuff, 255, szPath);
	stConfig.iPosOffsetX = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("PosOffsetY"), _T("5"), inBuff, 255, szPath);
	stConfig.iPosOffsetY = _ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("RadiusRatioY"), _T("5.0"), inBuff, 255, szPath);
	stConfig.dbRadiusRatioY = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("RadiusRatioX"), _T("5.0"), inBuff, 255, szPath);
	stConfig.dbRadiusRatioX = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("Enable8bit"), _T("0"), inBuff, 255, szPath);
	stConfig.b8BitUse = (1 == _ttoi(inBuff)) ? true : false;

	for (UINT nSpec = 0; nSpec < Spec_LCB_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_LCB, nSpec);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nSpec].iValue = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nSpec].iValue = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: Save_LCBFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_UI_LCB * pstConfig
// Qualifier	:
// Last Update	: 2018/8/1 - 13:22
// Desc.		:
//=============================================================================
BOOL CFile_VI_Config::Save_LCBFile(__in LPCTSTR szPath, __in const ST_UI_LCB* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_LCB;

	Save_Common(szPath, strAppName, &pstConfig->stInspect);

	szValue.Format(_T("%.3f"), pstConfig->dCenterThreshold);
	WritePrivateProfileString(strAppName, _T("CenterThreshold"), szValue, szPath);

	szValue.Format(_T("%.3f"), pstConfig->dEdgeThreshold);
	WritePrivateProfileString(strAppName, _T("EdgeThreshold"), szValue, szPath);

	szValue.Format(_T("%.3f"), pstConfig->dCornerThreshold);
	WritePrivateProfileString(strAppName, _T("CornerThreshold"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nMaxSingleDefectNum);
	WritePrivateProfileString(strAppName, _T("MaxSingleDefectNum"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nMinDefectWidthHeight);
	WritePrivateProfileString(strAppName, _T("MinDefectWidthHeight"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->bEnableCircle);
	WritePrivateProfileString(strAppName, _T("EnableCircle"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iPosOffsetX);
	WritePrivateProfileString(strAppName, _T("PosOffsetX"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->iPosOffsetY);
	WritePrivateProfileString(strAppName, _T("PosOffsetY"), szValue, szPath);

	szValue.Format(_T("%.1f"), pstConfig->dbRadiusRatioX);
	WritePrivateProfileString(strAppName, _T("RadiusRatioX"), szValue, szPath);

	szValue.Format(_T("%.1f"), pstConfig->dbRadiusRatioY);
	WritePrivateProfileString(strAppName, _T("RadiusRatioY"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->b8BitUse);
	WritePrivateProfileString(strAppName, _T("Enable8bit"), szValue, szPath);

	for (UINT nSpec = 0; nSpec < Spec_LCB_MAX; nSpec++)
	{
		strAppName.Format(_T("%s_%d"), AppName_LCB, nSpec);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nSpec].iValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}


BOOL CFile_VI_Config::Load_DisplaceFile(__in LPCTSTR szPath, __out ST_UI_Displace& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Displace;

	for (UINT nCh = 0; nCh < Displace_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Displace, nCh);

		GetPrivateProfileString(strAppName, _T("Offset"), _T("1.0"), inBuff, 255, szPath);
		stConfig.dbOffset[nCh] = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

BOOL CFile_VI_Config::Save_DisplaceFile(__in LPCTSTR szPath, __in const ST_UI_Displace* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Displace;

	for (UINT nCh = 0; nCh < Displace_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Displace, nCh);

		szValue.Format(_T("%.2f"), pstConfig->dbOffset[nCh]);
		WritePrivateProfileString(strAppName, _T("Offset"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMin[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMax[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

BOOL CFile_VI_Config::Load_VisionFile(__in LPCTSTR szPath, __out ST_UI_Vision& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_Vision;

	GetPrivateProfileString(strAppName, _T("szPatternName"), _T(""), inBuff, 255, szPath);
	stConfig.szPatternName = inBuff;

	for (UINT nCh = 0; nCh < Vision_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Vision, nCh);

		GetPrivateProfileString(strAppName, _T("Offset"), _T("1.0"), inBuff, 255, szPath);
		stConfig.dbOffset[nCh] = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

BOOL CFile_VI_Config::Save_VisionFile(__in LPCTSTR szPath, __in const ST_UI_Vision* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_Vision;

	szValue.Format(_T("%s"), pstConfig->szPatternName);
	WritePrivateProfileString(strAppName, _T("szPatternName"), szValue, szPath);

	for (UINT nCh = 0; nCh < Vision_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_Vision, nCh);

		szValue.Format(_T("%.2f"), pstConfig->dbOffset[nCh]);
		WritePrivateProfileString(strAppName, _T("Offset"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMin[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMax[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}

BOOL CFile_VI_Config::Load_IICFile(__in LPCTSTR szPath, __out ST_UI_IIC& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_IIC;

	GetPrivateProfileString(strAppName, _T("szPatternName"), _T(""), inBuff, 255, szPath);
	stConfig.szPatternName = inBuff;

	return TRUE;
}

BOOL CFile_VI_Config::Save_IICFile(__in LPCTSTR szPath, __in const ST_UI_IIC* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_IIC;

	szValue.Format(_T("%s"), pstConfig->szPatternName);
	WritePrivateProfileString(strAppName, _T("szPatternName"), szValue, szPath);

	return TRUE;
}

BOOL CFile_VI_Config::Load_OpticalCenterFile(__in LPCTSTR szPath, __out ST_UI_OpticalCenter& stConfig)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };

	CString	strAppName = AppName_OpticalCenter;

	GetPrivateProfileString(strAppName, _T("iCenterX"), _T("0"), inBuff, 255, szPath);
	stConfig.dCenterX = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("iCenterY"), _T("0"), inBuff, 255, szPath);
	stConfig.dCenterY = _ttof(inBuff);

	GetPrivateProfileString(strAppName, _T("nCamState"), _T("0"), inBuff, 255, szPath);
	stConfig.nCamState = _ttoi(inBuff);

	for (UINT nCh = 0; nCh < OpticalCenter_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_OpticalCenter, nCh);

		GetPrivateProfileString(strAppName, _T("Offset"), _T("1.0"), inBuff, 255, szPath);
		stConfig.dOffset[nCh] = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Min_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMin[nCh].dbValue = _ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Enable"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].bEnable = _ttoi(inBuff);

		GetPrivateProfileString(strAppName, _T("Max_Value"), _T("0"), inBuff, 255, szPath);
		stConfig.stSpecMax[nCh].dbValue = _ttof(inBuff);
	}

	return TRUE;
}

BOOL CFile_VI_Config::Save_OpticalCenterFile(__in LPCTSTR szPath, __in const ST_UI_OpticalCenter* pstConfig)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstConfig)
		return FALSE;

	CString szValue;
	CString	strAppName = AppName_OpticalCenter;

	szValue.Format(_T("%.2f"), pstConfig->dCenterX);
	WritePrivateProfileString(strAppName, _T("iCenterX"), szValue, szPath);

	szValue.Format(_T("%.2f"), pstConfig->dCenterY);
	WritePrivateProfileString(strAppName, _T("iCenterY"), szValue, szPath);

	szValue.Format(_T("%d"), pstConfig->nCamState);
	WritePrivateProfileString(strAppName, _T("nCamState"), szValue, szPath);

	for (UINT nCh = 0; nCh < OpticalCenter_Max; nCh++)
	{
		strAppName.Format(_T("%s_%d"), AppName_OpticalCenter, nCh);

		szValue.Format(_T("%.2f"), pstConfig->dOffset[nCh]);
		WritePrivateProfileString(strAppName, _T("Offset"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMin[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Min_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMin[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Min_Value"), szValue, szPath);

		szValue.Format(_T("%d"), pstConfig->stSpecMax[nCh].bEnable);
		WritePrivateProfileString(strAppName, _T("Max_Enable"), szValue, szPath);

		szValue.Format(_T("%.2f"), pstConfig->stSpecMax[nCh].dbValue);
		WritePrivateProfileString(strAppName, _T("Max_Value"), szValue, szPath);
	}

	return TRUE;
}
