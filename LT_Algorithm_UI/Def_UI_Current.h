﻿#ifndef Def_UI_Current_h__
#define Def_UI_Current_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_ROI_Current
{
	Current_CH1 = 0,
	Current_CH2,
	Current_CH3,
	Current_CH4,
	Current_CH5,
	Current_Max,
};

typedef struct _tag_UI_Current
{
	UINT		nCycle;

	double		dbOffset[Current_Max];

	ST_Spec		stSpecMin[Current_Max];
	ST_Spec		stSpecMax[Current_Max];
	
	_tag_UI_Current()
	{
		nCycle = 1;

		for (UINT nIdx = 0; nIdx < Current_Max; nIdx++)
		{
			dbOffset[nIdx] = 1.0;

			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}
	};

	_tag_UI_Current& operator= (_tag_UI_Current& ref)
	{
		nCycle = ref.nCycle;
		
		for (UINT nIdx = 0; nIdx < Current_Max; nIdx++)
		{
			dbOffset[nIdx]	= ref.dbOffset[nIdx];

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_Current, *PST_UI_Current;

#pragma pack(pop)

#endif // Def_Current_h__
