#ifndef Wnd_Rst_Defect_Black_h__
#define Wnd_Rst_Defect_Black_h__

#pragma once

#include "afxwin.h"
#include "VGStatic.h"
#include "Def_UI_Defect_Black.h"

// CWnd_Rst_Defect_Black
typedef enum
{
	enDefect_Black_Result_Count = 0,
	enDefect_Black_Result_Max,

}enDefect_Black_Result;

static	LPCTSTR	g_szDefect_Black_Result[] =
{
	_T("Count"),
	NULL
};

class CWnd_Rst_Defect_Black : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Rst_Defect_Black)

public:
	CWnd_Rst_Defect_Black();
	virtual ~CWnd_Rst_Defect_Black();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg BOOL	OnEraseBkgnd		(CDC* pDC);

	CVGStatic	m_st_Header[enDefect_Black_Result_Max];
	CVGStatic	m_st_Result[enDefect_Black_Result_Max];

public:

	void Result_Display		(__in UINT nResultIdx, __in UINT nIdx, __in BOOL bReulst, __in int nValue);
	void Result_Reset		();
};


#endif // Wnd_Rst_Defect_Black_h__
