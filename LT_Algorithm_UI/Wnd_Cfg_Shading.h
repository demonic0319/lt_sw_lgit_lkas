﻿#ifndef Wnd_Cfg_Shading_h__
#define Wnd_Cfg_Shading_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Shading.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Shading
//-----------------------------------------------------------------------------
class CWnd_Cfg_Shading : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Shading)

public:
	CWnd_Cfg_Shading();
	virtual ~CWnd_Cfg_Shading();

	enum enShadingStatic
	{
		STI_SHD_PARAM = 0,
		STI_SHD_SPEC,
		STI_SHD_PATCH_WIDTH,
		STI_SHD_PATCH_HEIGHT,
		STI_SHD_TEST_AREA1,
		STI_SHD_TEST_AREA2,
		STI_SHD_MAX,
	};

	enum enShadingButton
	{
		BTN_SHD_MAX = 1,
	};

	enum enShadingComobox
	{
		CMB_SHD_MAX = 1,
	};

	enum enShadingEdit
	{
		EDT_SHD_PATCH_WIDTH,
		EDT_SHD_PATCH_HEIGHT,
		EDT_SHD_TEST_AREA1,
		EDT_SHD_TEST_AREA2,
		EDT_SHD_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl(UINT nID);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	CFont				m_font;

	CVGStatic			m_st_Item[STI_SHD_MAX];
	CButton				m_bn_Item[BTN_SHD_MAX];
	CComboBox			m_cb_Item[CMB_SHD_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_SHD_MAX];

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Spec[enUI_ShadingField_Max];
	CMFCMaskedEdit		m_ed_SpecMin[enUI_ShadingField_Max];
	CMFCMaskedEdit		m_ed_SpecMax[enUI_ShadingField_Max];
	CMFCButton			m_chk_SpecMin[enUI_ShadingField_Max];
	CMFCButton			m_chk_SpecMax[enUI_ShadingField_Max];

	ST_UI_Shading*		m_pstConfig = NULL;

public:

	void	SetPtr_RecipeInfo(ST_UI_Shading* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	void	SetUpdateData();
	void	GetUpdateData();
};

#endif // Wnd_Cfg_Shading_h__
