﻿// List_RotateOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "List_RotateOp.h"

typedef enum enListNum_RotateOp
{
	RoOp_Object = 0,
	RoOp_PosX,
	RoOp_PosY,
	RoOp_Width,
	RoOp_Height,
	RoOp_Type,
	RoOp_Color,
	RoOp_Brightness,
	RoOp_Sharpness,
	RoOp_MaxCol,
};

static LPCTSTR	g_lpszHeader_RotateOp[] =
{
	_T(""),
	_T("CenterX"),
	_T("CenterY"),
	_T("Width"),
	_T("Height"),
	_T("Type"),
	_T("Color"),
	_T("Brightness"),
	_T("Sharpness"),					 //SfOp_ItemPos,
	NULL
};

static LPCTSTR	g_lpszItem_RotateOp[] =
{
	NULL
};

const int	iListAglin_RotateOp[] =
{
	LVCFMT_LEFT,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
	LVCFMT_CENTER,
};

const int	iHeaderWidth_RotateOp[] =
{
	105,
	105,
	105,
	105,
	105,
	105,
	105,
	105,
	105,
};

#define IDC_EDT_CELLEDIT		1000
#define IDC_COM_CELLCOMBO_TYPE	2000
#define IDC_COM_CELLCOMBO_COLOR	3000

// CList_RotateOp

IMPLEMENT_DYNAMIC(CList_RotateOp, CList_Cfg_VIBase)

CList_RotateOp::CList_RotateOp()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
	m_nEditCol = 0;
	m_nEditRow = 0;

	m_pstConfigInfo = NULL;
}

CList_RotateOp::~CList_RotateOp()
{
	m_Font.DeleteObject();
}
BEGIN_MESSAGE_MAP(CList_RotateOp, CList_Cfg_VIBase)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT	(NM_CLICK,					&CList_RotateOp::OnNMClick				)
	ON_NOTIFY_REFLECT	(NM_DBLCLK,					&CList_RotateOp::OnNMDblclk				)
	ON_EN_KILLFOCUS		(IDC_EDT_CELLEDIT,			&CList_RotateOp::OnEnKillFocusEdit		)
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_TYPE,	&CList_RotateOp::OnEnKillFocusComboType	)
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_TYPE,	&CList_RotateOp::OnEnSelectComboType	)	
	ON_CBN_KILLFOCUS	(IDC_COM_CELLCOMBO_COLOR,	&CList_RotateOp::OnEnKillFocusComboColor)	
	ON_CBN_SELCHANGE	(IDC_COM_CELLCOMBO_COLOR,	&CList_RotateOp::OnEnSelectComboColor	)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// CList_RotateOp 메시지 처리기입니다.
int CList_RotateOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CList_Cfg_VIBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();
	m_ed_CellEdit.Create(WS_CHILD | ES_CENTER | ES_NUMBER, CRect(0, 0, 0, 0), this, IDC_EDT_CELLEDIT);
	this->GetHeaderCtrl()->EnableWindow(FALSE);

	m_cb_Type.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_TYPE);
	m_cb_Type.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkType[nIdex]; nIdex++)
	{
		m_cb_Type.InsertString(nIdex, g_szMarkType[nIdex]);
	}

	m_cb_Color.Create(WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | CBS_DROPDOWNLIST, CRect(0, 0, 0, 0), this, IDC_COM_CELLCOMBO_COLOR);
	m_cb_Color.ResetContent();

	for (UINT nIdex = 0; NULL != g_szMarkColor[nIdex]; nIdex++)
	{
		m_cb_Color.InsertString(nIdex, g_szMarkColor[nIdex]);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_RotateOp::OnSize(UINT nType, int cx, int cy)
{
	CList_Cfg_VIBase::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES |  LVS_EX_FULLROWSELECT;

	return CList_Cfg_VIBase::PreCreateWindow(cs);
}

//=============================================================================
// Method		: InitHeader
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_RotateOp::InitHeader()
{
	for (int nCol = 0; nCol < RoOp_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader_RotateOp[nCol], iListAglin_RotateOp[nCol], iHeaderWidth_RotateOp[nCol]);
	}

	for (int nCol = 0; nCol < RoOp_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, iHeaderWidth_RotateOp[nCol]);
	}
}

//=============================================================================
// Method		: InsertFullData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/27 - 17:25
// Desc.		:
//=============================================================================
void CList_RotateOp::InsertFullData()
{
 	DeleteAllItems();

	for (UINT nIdx = 0; nIdx < RoOp_ItemNum; nIdx++)
	{
		InsertItem(nIdx, _T(""));
		SetRectRow(nIdx);
 	}
}

//=============================================================================
// Method		: SetRectRow
// Access		: public  
// Returns		: void
// Parameter	: UINT nRow
// Qualifier	:
// Last Update	: 2017/6/26 - 14:26
// Desc.		:
//=============================================================================
void CList_RotateOp::SetRectRow(UINT nRow)
{
	if (NULL == m_pstConfigInfo)
		return;

	CString strValue;

	strValue.Format(_T("%s"), g_szROI_Rotate[nRow]);
	SetItemText(nRow, RoOp_Object, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().x);
	SetItemText(nRow, RoOp_PosX, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().y);
	SetItemText(nRow, RoOp_PosY, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Width());
	SetItemText(nRow, RoOp_Width, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Height());
	SetItemText(nRow, RoOp_Height, strValue);

	strValue.Format(_T("%s"), g_szMarkType[m_pstConfigInfo->stInput[nRow].nMarkType]);
	SetItemText(nRow, RoOp_Type, strValue);

	strValue.Format(_T("%s"), g_szMarkColor[m_pstConfigInfo->stInput[nRow].nMarkColor]);
	SetItemText(nRow, RoOp_Color, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].nBrightness);
	SetItemText(nRow, RoOp_Brightness, strValue);

	strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].nSharpness);
	SetItemText(nRow, RoOp_Sharpness, strValue);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_RotateOp::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	*pResult = 0;
}

//=============================================================================
// Method		: OnNMDblclk
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/6/26 - 14:27
// Desc.		:
//=============================================================================
void CList_RotateOp::OnNMDblclk(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (0 <= pNMItemActivate->iItem)
	{
		if (pNMItemActivate->iSubItem < RoOp_MaxCol && pNMItemActivate->iSubItem > 0)
		{
			CRect rectCell;

			m_nEditCol = pNMItemActivate->iSubItem;
			m_nEditRow = pNMItemActivate->iItem;

			ModifyStyle(WS_VSCROLL, 0);
			
			GetSubItemRect(m_nEditRow, m_nEditCol, LVIR_BOUNDS, rectCell);
			ClientToScreen(rectCell);
			ScreenToClient(rectCell);

			if (pNMItemActivate->iSubItem == RoOp_Type)
			{
				m_cb_Type.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Type.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Type.SetFocus();
				m_cb_Type.SetCurSel(m_pstConfigInfo->stInput[m_nEditRow].nMarkType);
			}
			else if (pNMItemActivate->iSubItem == RoOp_Color)
			{
				m_cb_Color.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_cb_Color.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_cb_Color.SetFocus();
				m_cb_Color.SetCurSel(m_pstConfigInfo->stInput[m_nEditRow].nMarkColor);
			}
			else
			{
				m_ed_CellEdit.SetWindowText(GetItemText(m_nEditRow, m_nEditCol));
				m_ed_CellEdit.SetWindowPos(NULL, rectCell.left, rectCell.top, rectCell.Width(), rectCell.Height(), SWP_SHOWWINDOW);
				m_ed_CellEdit.SetFocus();
			}
		}
		GetOwner()->SendNotifyMessage(m_nMessage, 0, 0);
	}
	*pResult = 0;
}

//=============================================================================
// Method		: OnEnKillFocusEdit
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
void CList_RotateOp::OnEnKillFocusEdit()
{
	CString strText;
	m_ed_CellEdit.GetWindowText(strText);

	UpdateCellData(m_nEditRow, m_nEditCol, _ttoi(strText));

	CRect rc;
	GetClientRect(rc);
	OnSize(SIZE_RESTORED, rc.Width(), rc.Height());

	m_ed_CellEdit.SetWindowText(_T(""));
	m_ed_CellEdit.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

}

//=============================================================================
// Method		: UpdateCellData
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: int iValue
// Qualifier	:
// Last Update	: 2017/6/26 - 14:28
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::UpdateCellData(UINT nRow, UINT nCol, int iValue)
{
	if (NULL == m_pstConfigInfo)
		return FALSE;

	if (iValue < 0)
		iValue = 0;

	CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

	switch (nCol)
	{
	case RoOp_PosX:
		m_pstConfigInfo->stInput[nRow].RectPosXY(iValue, rtTemp.CenterPoint().y);
		break;
	case RoOp_PosY:
		m_pstConfigInfo->stInput[nRow].RectPosXY(rtTemp.CenterPoint().x, iValue);
		break;
	case RoOp_Width:
		m_pstConfigInfo->stInput[nRow].RectPosWH(iValue, rtTemp.Height());
		break;
	case RoOp_Height:
		m_pstConfigInfo->stInput[nRow].RectPosWH(rtTemp.Width(), iValue);
		break;
	case RoOp_Brightness:
		if (iValue >= 255)
			iValue = 255;

		m_pstConfigInfo->stInput[nRow].nBrightness = iValue;
		break;
	case RoOp_Sharpness:
		if (iValue >= 127)
			iValue = 127;

		m_pstConfigInfo->stInput[nRow].nSharpness = iValue;
		break;
	default:
		break;
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.left < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.left = 0;
		m_pstConfigInfo->stInput[nRow].rtROI.right = m_pstConfigInfo->stInput[nRow].rtROI.left + rtTemp.Width();
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.right >(LONG)m_dwImage_Width)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.right = (LONG)m_dwImage_Width;
		m_pstConfigInfo->stInput[nRow].rtROI.left = m_pstConfigInfo->stInput[nRow].rtROI.right - rtTemp.Width();
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.top < 0)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.top = 0;
		m_pstConfigInfo->stInput[nRow].rtROI.bottom = rtTemp.Height() + m_pstConfigInfo->stInput[nRow].rtROI.top;
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.bottom >(LONG)m_dwImage_Height)
	{
		CRect rtTemp = m_pstConfigInfo->stInput[nRow].rtROI;

		m_pstConfigInfo->stInput[nRow].rtROI.bottom = (LONG)m_dwImage_Height;
		m_pstConfigInfo->stInput[nRow].rtROI.top = m_pstConfigInfo->stInput[nRow].rtROI.bottom - rtTemp.Height();
	}

	if (m_pstConfigInfo->stInput[nRow].rtROI.Height() <= 0)
		m_pstConfigInfo->stInput[nRow].RectPosWH(m_pstConfigInfo->stInput[nRow].rtROI.Width(), 1);

	if (m_pstConfigInfo->stInput[nRow].rtROI.Height() >= (LONG)m_dwImage_Height)
		m_pstConfigInfo->stInput[nRow].RectPosWH(m_pstConfigInfo->stInput[nRow].rtROI.Width(), m_dwImage_Height);

	if (m_pstConfigInfo->stInput[nRow].rtROI.Width() <= 0)
		m_pstConfigInfo->stInput[nRow].RectPosWH(1, m_pstConfigInfo->stInput[nRow].rtROI.Height());

	if (m_pstConfigInfo->stInput[nRow].rtROI.Width() >= (LONG)m_dwImage_Width)
		m_pstConfigInfo->stInput[nRow].RectPosWH(m_dwImage_Width, m_pstConfigInfo->stInput[nRow].rtROI.Height());

	CString strValue;

	switch (nCol)
	{
	case RoOp_PosX:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().x);
		break;
	case RoOp_PosY:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.CenterPoint().y);
		break;
	case RoOp_Width:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Width());
		break;
	case RoOp_Height:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].rtROI.Height());
		break;
	case RoOp_Brightness:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].nBrightness);
		break;
	case RoOp_Sharpness:
		strValue.Format(_T("%d"), m_pstConfigInfo->stInput[nRow].nSharpness);
		break;
	default:
		break;
	}

	m_ed_CellEdit.SetWindowText(strValue);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: UpdateCellData_double
// Access		: protected  
// Returns		: BOOL
// Parameter	: UINT nRow
// Parameter	: UINT nCol
// Parameter	: double dbValue
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::UpdateCelldbData(UINT nRow, UINT nCol, double dbValue)
{
	CString str;
	str.Format(_T("%.1f"), dbValue);

	m_ed_CellEdit.SetWindowText(str);
	SetRectRow(nRow);

	return TRUE;
}

//=============================================================================
// Method		: GetCellData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
void CList_RotateOp::GetCellData()
{
	return;
}

//=============================================================================
// Method		: OnMouseWheel
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nFlags
// Parameter	: short zDelta
// Parameter	: CPoint pt
// Qualifier	:
// Last Update	: 2017/8/12 - 20:45
// Desc.		:
//=============================================================================
BOOL CList_RotateOp::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	CWnd* pWndFocus = GetFocus();

	if (m_ed_CellEdit.GetSafeHwnd() == pWndFocus->GetSafeHwnd())
	{
		CString strText;
		m_ed_CellEdit.GetWindowText(strText);

		int iValue		= _ttoi(strText);
		double dbValue  = _ttof(strText);

		iValue = iValue + ((zDelta / 120));
		dbValue = dbValue + ((zDelta / 120)*0.1);

		if (iValue < 0)
			iValue = 0;

		if (iValue > 2000)
			iValue = 2000;

		if (dbValue < 0.0)
			dbValue = 0.0;

		if (dbValue > 2.0)
			dbValue = 2.0;
		
		UpdateCellData(m_nEditRow, m_nEditCol, iValue);

	}
	return CList_Cfg_VIBase::OnMouseWheel(nFlags, zDelta, pt);
}

//=============================================================================
// Method		: OnEnKillFocusComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_RotateOp::OnEnKillFocusComboType()
{
	SetItemText(m_nEditRow, RoOp_Type, g_szMarkType[m_pstConfigInfo->stInput[m_nEditRow].nMarkType]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_RotateOp::OnEnSelectComboType()
{
	m_pstConfigInfo->stInput[m_nEditRow].nMarkType = m_cb_Type.GetCurSel();

	SetItemText(m_nEditRow, RoOp_Type, g_szMarkType[m_pstConfigInfo->stInput[m_nEditRow].nMarkType]);
	m_cb_Type.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnKillFocusComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_RotateOp::OnEnKillFocusComboColor()
{
	SetItemText(m_nEditRow, RoOp_Color, g_szMarkColor[m_pstConfigInfo->stInput[m_nEditRow].nMarkColor]);
	m_cb_Color.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}

//=============================================================================
// Method		: OnEnSelectComboColor
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/18 - 14:11
// Desc.		:
//=============================================================================
void CList_RotateOp::OnEnSelectComboColor()
{
	m_pstConfigInfo->stInput[m_nEditRow].nMarkColor = m_cb_Color.GetCurSel();

	SetItemText(m_nEditRow, RoOp_Color, g_szMarkColor[m_pstConfigInfo->stInput[m_nEditRow].nMarkColor]);
	m_cb_Color.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);
}