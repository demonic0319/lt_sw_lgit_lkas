﻿#ifndef Wnd_Cfg_Vision_h__
#define Wnd_Cfg_Vision_h__

#pragma once

#include "Wnd_Cfg_VIBase.h"
#include "VGStatic.h"
#include "Def_UI_Vision.h"
#include "File_WatchList.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_Vision
//-----------------------------------------------------------------------------
class CWnd_Cfg_Vision : public CWnd_Cfg_VIBase
{
	DECLARE_DYNAMIC(CWnd_Cfg_Vision)

public:
	CWnd_Cfg_Vision();
	virtual ~CWnd_Cfg_Vision();

	enum enVisionStatic
	{
		STI_VIS_PARAM = 0,
		STI_VIS_SPEC,
		STI_VIS_MATCH_RATE,
		STI_VIS_MASTER,
		STI_VIS_MAX,
	};

	enum enVisionButton
	{
		BTN_VIS_MAX = 1,
	};

	enum enVisionComobox
	{
		CMB_VIS_PATTERN,
		CMB_VIS_MAX
	};

	enum enVisionEdit
	{
		EDT_VIS_MATCH_RATE = 0,
		EDT_VIS_MAX,
	};

protected:
	DECLARE_MESSAGE_MAP()
	
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	CFont				m_font;

	CVGStatic			m_st_Item[STI_VIS_MAX];
	CButton				m_bn_Item[BTN_VIS_MAX];
	CComboBox			m_cb_Item[CMB_VIS_MAX];
	CMFCMaskedEdit		m_ed_Item[EDT_VIS_MAX];

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Spec[Vision_Max];
	CMFCMaskedEdit		m_ed_SpecMin[Vision_Max];
	CMFCMaskedEdit		m_ed_SpecMax[Vision_Max];
	CMFCButton			m_chk_SpecMin[Vision_Max];
	CMFCButton			m_chk_SpecMax[Vision_Max];

	ST_UI_Vision*		m_pstConfig		= NULL;

	CString			m_szVisionImgFolderPath;

	CFile_WatchList	m_IniWatch;
	void		RefreshFileList(__in const CStringList* pFileList);
public:

	void	SetPtr_RecipeInfo(ST_UI_Vision* pstRecipeInfo)
	{
		if (pstRecipeInfo == NULL)
			return;

		m_pstConfig = pstRecipeInfo;
	};

	// iic ini file path
	void SetVisionImageFolderPath(__in LPCTSTR szPath)
	{
		m_szVisionImgFolderPath = szPath;
	};

	void	SetUpdateData		();
	void	GetUpdateData		();
};

#endif // Wnd_Cfg_Vision_h__
