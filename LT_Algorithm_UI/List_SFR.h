﻿#ifndef List_SFR_h__
#define List_SFR_h__

#pragma once

#include "List_Cfg_VIBase.h"
#include "Def_UI_SFR.h"

typedef enum enListItemNum_SFR
{
	SfOp_ItemNum = ROI_SFR_Max,
};

//-----------------------------------------------------------------------------
// List_SFRInfo
//-----------------------------------------------------------------------------
class CList_SFR : public CList_Cfg_VIBase
{
	DECLARE_DYNAMIC(CList_SFR)

public:
	CList_SFR();
	virtual ~CList_SFR();

protected:
	
	DECLARE_MESSAGE_MAP()

	virtual BOOL	PreCreateWindow				(CREATESTRUCT& cs);

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk					(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel				(UINT nFlags, short zDelta, CPoint pt);
	
	afx_msg void	OnRangeCheckBtnMinCtrl		(UINT nID);
	afx_msg void	OnRangeCheckBtnMaxCtrl		(UINT nID);

	afx_msg void	OnEnKillFocusComboType		();
	afx_msg void	OnEnSelectComboType			();

	afx_msg void	OnEnKillFocusComboOverlay	();
	afx_msg void	OnEnSelectComboOverlay		();

	afx_msg void	OnEnKillFocusEdit			();

	BOOL			UpdateCellData				(UINT nRow, UINT nCol, int iValue);
	BOOL			UpdateCelldbData			(UINT nRow, UINT nCol, double dValue);

	CFont				m_Font;

	UINT				m_nEditCol		= 0;
	UINT				m_nEditRow		= 0;

	CMFCMaskedEdit		m_ed_CellEdit;
	CComboBox			m_cb_Type;
	CComboBox			m_cb_Overlay;

	ST_UI_SFR*			m_pstConfig		= NULL;

public:

	void SetPtr_ConfigInfo(ST_UI_SFR* pstConfigInfo)
	{
		if (pstConfigInfo == NULL)
			return;

		m_pstConfig = pstConfigInfo;
	};

	void InitHeader			();
	void DeleteHeader		(UINT nRow);
	void InsertFullData		();
	void SetRectRow			(UINT nRow);
	void GetCellData		();
	
};

#endif // List_SFRInfo_h__
