﻿#ifndef Def_UI_DynamicBWBW_h__
#define Def_UI_DynamicBWBW_h__

#include <afxwin.h>

#include "Def_UI_Common.h"

#pragma pack(push,1)

typedef enum enUI_DynamicBW_ROI
{
	ROI_DnyBW_FST = 0,
	ROI_DnyBW_LST = 2,
	ROI_DnyBW_Max,
};


typedef enum enDynamicBW_Spec
{
	Spec_Dynamic = 0,
	Spec_SNR_BW,
	Spec_DynamicBW_MAX,
};


typedef struct _tag_Input_DynamicBW
{
	CPoint			ptROI;

	BOOL			bEnable;				
	UINT			nOverlayDir;
	
// 	ST_Spec			stSpecMin;
// 	ST_Spec			stSpecMax;
	enSNRBWType		eSNRType;

	_tag_Input_DynamicBW()
	{
		Reset();
	};

	void Reset()
	{
		ptROI.x = 0;
		ptROI.y = 0;
		bEnable		= FALSE;
		nOverlayDir	= OverlayDir_Top;

// 		stSpecMin.Reset();
// 		stSpecMax.Reset();

		eSNRType	= _SNR_BW_IMAGE_WHITE;
	};

	_tag_Input_DynamicBW& operator= (_tag_Input_DynamicBW& ref)
	{
		ptROI= ref.ptROI;
			
		bEnable		= ref.bEnable;
		nOverlayDir	= ref.nOverlayDir;

// 		stSpecMin	= ref.stSpecMin;
// 		stSpecMax	= ref.stSpecMax;

		eSNRType	= ref.eSNRType;

		return *this; 
	};

}ST_Input_DynamicBW, *PST_Input_DynamicBW;

typedef struct _tag_UI_DynamicBW
{
	// Inspect
	ST_Inspect				stInspect;
	ST_Input_DynamicBW		stInput[ROI_DnyBW_Max];

	// Spec
	int						iLscBlockSize;					// Pixel 지역적인 렌즈 Shading 보상 블록의 크기 (사용안함)
	int						iMaxROICount;					// 이미지 내 ROI 의 최대 개수
	double					dbOffsetDR;
	double					dbOffsetSNR;

	int						iMaxROIWidth;
	int						iMaxROIHeight;

	double					dbDRThreshold;
	double					dbSNRThreshold;
	bool					b8BitUse;

	ST_Spec			stSpecMin[Spec_DynamicBW_MAX];
	ST_Spec			stSpecMax[Spec_DynamicBW_MAX];

	_tag_UI_DynamicBW()
	{
		stInspect.Reset();

		iMaxROIWidth = 40;
		iMaxROIHeight = 40;

		iLscBlockSize	= 0;
		iMaxROICount	= 3;

		dbOffsetDR		= 1.0;
		dbOffsetSNR		= 1.0;

		dbDRThreshold	= 100.0;
		dbSNRThreshold  = 100.0;

		b8BitUse = false;

		for (UINT nIdx = 0; nIdx < Spec_DynamicBW_MAX; nIdx++)
		{
			stSpecMin[nIdx].Reset();
			stSpecMax[nIdx].Reset();
		}

		for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
		{
			stInput[nROI].Reset();
		}
	}

	_tag_UI_DynamicBW& operator= (_tag_UI_DynamicBW& ref)
	{
		stInspect		= ref.stInspect;
		iLscBlockSize	= ref.iLscBlockSize;
		iMaxROICount	= ref.iMaxROICount;

		dbOffsetDR		= ref.dbOffsetDR;
		dbOffsetSNR		= ref.dbOffsetSNR;

		iMaxROIWidth	= ref.iMaxROIWidth;
		iMaxROIHeight	= ref.iMaxROIHeight;

		dbDRThreshold	= ref.dbDRThreshold;
		dbSNRThreshold	= ref.dbSNRThreshold;

		b8BitUse		= ref.b8BitUse;

		for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
		{
			stInput[nROI] = ref.stInput[nROI];
		}

		for (UINT nIdx = 0; nIdx < Spec_DynamicBW_MAX; nIdx++)
		{

			stSpecMin[nIdx] = ref.stSpecMin[nIdx];
			stSpecMax[nIdx] = ref.stSpecMax[nIdx];
		}

		return *this;
	};

}ST_UI_DynamicBW, *PST_UI_DynamicBW;

#pragma pack(pop)

#endif // Def_DynamicBW_h__

