//*****************************************************************************
// Filename	: 	File_Config_VI.h
// Created	:	2018/1/26 - 14:53
// Modified	:	2018/1/26 - 14:53
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef File_VI_Config_h__
#define File_VI_Config_h__

#pragma once

#include <afxwin.h>
#include "CommonFunction.h"

#include "Def_UI_Common.h"
#include "Def_UI_Ymean.h"
#include "Def_UI_DynamicBW.h"
#include "Def_UI_SFR.h"
#include "Def_UI_Shading.h"
#include "Def_UI_Current.h"
#include "Def_UI_Rllumination.h"
#include "Def_UI_Chart.h"
#include "Def_UI_Defect_Black.h"
#include "Def_UI_Defect_White.h"
#include "Def_UI_OpticalCenter.h"
#include "Def_UI_Rotation.h"
#include "Def_UI_BlackSpot.h"
#include "Def_UI_LCB.h"
#include "Def_UI_Displace.h"
#include "Def_UI_Vision.h"
#include "Def_UI_IIC.h"

//-----------------------------------------------------------------------------
// CFile_VI_Config
//-----------------------------------------------------------------------------
class CFile_VI_Config
{
public:
	CFile_VI_Config();
	~CFile_VI_Config();
	
	BOOL	Load_Common					(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in _tag_Inspect& stConfig);
	BOOL	Save_Common					(__in LPCTSTR szPath, __in LPCTSTR szAppName, __in const _tag_Inspect* pstConfig);

	// Ymean
	BOOL	Load_YmeanFile			(__in LPCTSTR szPath, __out ST_UI_Ymean& stConfig);
	BOOL	Save_YmeanFile			(__in LPCTSTR szPath, __in const ST_UI_Ymean* pstConfig);

	// Dynamic
	BOOL	Load_DynamicBWFile			(__in LPCTSTR szPath, __out ST_UI_DynamicBW& stConfig);
	BOOL	Save_DynamicBWFile			(__in LPCTSTR szPath, __in const ST_UI_DynamicBW* pstConfig);
	
	// SFR
	BOOL	Load_SFRFile				(__in LPCTSTR szPath, __out ST_UI_SFR& stConfig);
	BOOL	Save_SFRFile				(__in LPCTSTR szPath, __in const ST_UI_SFR* pstConfig);

	// Shading
	BOOL	Load_ShadingFile			(__in LPCTSTR szPath, __out ST_UI_Shading& stConfig);
	BOOL	Save_ShadingFile			(__in LPCTSTR szPath, __in const ST_UI_Shading* pstConfig);

	// Current
	BOOL	Load_CurrentFile			(__in LPCTSTR szPath, __out ST_UI_Current& stConfig);
	BOOL	Save_CurrentFile			(__in LPCTSTR szPath, __in const ST_UI_Current* pstConfig);

	// Rllumination
	BOOL	Load_RlluminationFile		(__in LPCTSTR szPath, __out ST_UI_Rllumination& stConfig);
	BOOL	Save_RlluminationFile		(__in LPCTSTR szPath, __in const ST_UI_Rllumination* pstConfig);

	// Chart
	BOOL	Load_ChartFile				(__in LPCTSTR szPath, __out ST_UI_Chart& stConfig);
	BOOL	Save_ChartFile				(__in LPCTSTR szPath, __in const ST_UI_Chart* pstConfig);
	
	// Defect_Black
	BOOL Load_Defect_BlackFile			(__in LPCTSTR szPath, __out ST_UI_Defect_Black& stConfig);
	BOOL Save_DefectBlackFile			(__in LPCTSTR szPath, __in const ST_UI_Defect_Black* pstConfig);
	
	// Defect_White
	BOOL Load_Defect_WhiteFile			(__in LPCTSTR szPath, __out ST_UI_Defect_White& stConfig);
	BOOL Save_DefectWhiteFile			(__in LPCTSTR szPath, __in const ST_UI_Defect_White* pstConfig);

	// ����Aa
	//BOOL	Load_OpticalCenterFile		(__in LPCTSTR szPath, __out ST_OpticalCenter_Opt& stConfig);
	//BOOL	Save_OpticalCenterFile		(__in LPCTSTR szPath, __in const ST_OpticalCenter_Opt* pstConfig);

	// Rotate
	BOOL	Load_RotateFile				(__in LPCTSTR szPath, __out ST_UI_Rotation& stConfig);
	BOOL	Save_RotateFile				(__in LPCTSTR szPath, __in const ST_UI_Rotation* pstConfig);

	// BlackSpot
	BOOL	Load_BlackSpotFile			(__in LPCTSTR szPath, __out ST_UI_BlackSpot& stConfig);
	BOOL	Save_BlackSpotFile			(__in LPCTSTR szPath, __in const ST_UI_BlackSpot* pstConfig);

	// LCB
	BOOL	Load_LCBFile				(__in LPCTSTR szPath, __out ST_UI_LCB& stConfig);
	BOOL	Save_LCBFile				(__in LPCTSTR szPath, __in const ST_UI_LCB* pstConfig);

	// Displace
	BOOL	Load_DisplaceFile			(__in LPCTSTR szPath, __out ST_UI_Displace& stConfig);
	BOOL	Save_DisplaceFile			(__in LPCTSTR szPath, __in const ST_UI_Displace* pstConfig);

	// Vision
	BOOL	Load_VisionFile				(__in LPCTSTR szPath, __out ST_UI_Vision& stConfig);
	BOOL	Save_VisionFile				(__in LPCTSTR szPath, __in const ST_UI_Vision* pstConfig);

	// IIC
	BOOL	Load_IICFile				(__in LPCTSTR szPath, __out ST_UI_IIC& stConfig);
	BOOL	Save_IICFile				(__in LPCTSTR szPath, __in const ST_UI_IIC* pstConfig);

	// Optical Center
	BOOL	Load_OpticalCenterFile		(__in LPCTSTR szPath, __out ST_UI_OpticalCenter& stConfig);
	BOOL	Save_OpticalCenterFile		(__in LPCTSTR szPath, __in const ST_UI_OpticalCenter* pstConfig);
};											

#endif // File_VI_Config_h__
