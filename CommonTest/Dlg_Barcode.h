﻿//*****************************************************************************
// Filename	: 	Dlg_Barcode.h
// Created	:	2016/11/1 - 16:59
// Modified	:	2016/11/1 - 16:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Dlg_Barcode_h__
#define Dlg_Barcode_h__

#pragma once

#include "resource.h"
#include "Def_Enum_Cm.h"
#include "Def_DataStruct_Cm.h"
#include "VGStatic.h"

typedef enum enBarcodeType
{
	Barcode_SN,
	Barcode_WIP_ID,
	Barcode_Pallet_ID,
	Barcode_PartNo,
};

//-----------------------------------------------------------------------------
// CDlg_Barcode dialog
//-----------------------------------------------------------------------------
class CDlg_Barcode : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Barcode)

public:
	CDlg_Barcode(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlg_Barcode();

// Dialog Data
	enum { IDD = IDD_DLG_BARCODE };

protected:
	virtual void	DoDataExchange		(CDataExchange* pDX);    // DDX/DDV support
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void	OnGetMinMaxInfo		(MINMAXINFO* lpMMI);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage	(MSG* pMsg);
	virtual BOOL	OnInitDialog		();
	virtual void	OnOK				();
	
	DECLARE_MESSAGE_MAP()

	CFont			m_font_Large;
	CFont			m_font_Default;

	UINT			m_nBarcodeLength;
	CString			m_szBarcode;

	CVGStatic		m_st_Site;
	CEdit			m_ed_Barcode;
	CVGStatic		m_st_Status;

	CButton			m_bn_OK;
	CButton			m_bn_Cancel;

	BOOL			m_bIsModal				= FALSE;

	BOOL			m_bChk_BarcodeLength	= FALSE;

	enBarcodeType	m_nBarcodeType			= Barcode_SN;

	enBarcodeChk	CheckBarcode		(__in LPCTSTR szBarcode);
	void			SetStatus			(__in enBarcodeChk nStatus);

public:

	virtual INT_PTR DoModal();

	BOOL			SetBarcodeLength	(__in UINT nLength, __in BOOL bUse = TRUE);

	CString			GetBarcode			();

	BOOL			InsertBarcode		(__in LPCTSTR szBarcode);
	
	void			ResetBarcode		();
	
	void			SetBarcodeType		(__in enBarcodeType nBarcodeType);
};

#endif // Dlg_Barcode_h__
