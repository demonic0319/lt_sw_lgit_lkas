// Wnd_Cfg_3DCal.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Cfg_3DCal.h"

// CWnd_Cfg_3D_Depth
#define IDC_EDIT_PARAM	2000
#define IDC_EDIT_SPEC	2001
#define IDC_EDIT_ROI	2002

#define IDC_LST_ROI		4000


IMPLEMENT_DYNAMIC(CWnd_Cfg_3D_Depth, CWnd)

CWnd_Cfg_3D_Depth::CWnd_Cfg_3D_Depth()
{
	VERIFY(m_font.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_3D_Depth::~CWnd_Cfg_3D_Depth()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_3D_Depth, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//ON_COMMAND_RANGE(IDC_BTN_ADD, IDC_BTN_ADD + PROI_BT_MaxNum, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_Cfg_3D_Depth 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
int CWnd_Cfg_3D_Depth::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_3D_Depth_MaxNum; nIdx++)
	{
		m_st_Param_Depth[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_Param_Depth[nIdx].SetFont_Gdip(L"Arial", 9.0F, FontStyleRegular);
		m_st_Param_Depth[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Param_Depth[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Param_Depth[nIdx].Create(g_Param_3D_Depth[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_Param_Depth[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDIT_PARAM + nIdx);
		m_ed_Param_Depth[nIdx].SetFont(&m_font);
		m_ed_Param_Depth[nIdx].SetValidChars(_T("0123456789.-"));
	}

	// ROI
	m_lst_3DCalROI.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LST_ROI);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/11/10 - 17:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Depth::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 10;
	int iSpacing = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iCtrlHeight = 36;
	int iTempWidth = (iWidth - (iSpacing * 3)) / 4;
	int iNameWidth = iTempWidth;

	int iLeftEdit = iLeft + iNameWidth + iSpacing;
	int iLeftSub = iLeftEdit + iTempWidth + iSpacing;
	int iLeftEditSub = iLeftSub + iNameWidth + iSpacing;

	int iListWidth = iWidth;
	int iListHeight = 240;

	// 세팅 파라미터
	for (UINT nIdx = 0; nIdx < Param_3D_Depth_MaxNum; nIdx++)
	{
		if (0 == (nIdx % 2))
		{
			m_st_Param_Depth[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);
			m_ed_Param_Depth[nIdx].MoveWindow(iLeftEdit, iTop, iTempWidth, iCtrlHeight);
		}
		else
		{
			m_st_Param_Depth[nIdx].MoveWindow(iLeftSub, iTop, iNameWidth, iCtrlHeight);
			m_ed_Param_Depth[nIdx].MoveWindow(iLeftEditSub, iTop, iTempWidth, iCtrlHeight);
			iTop += iCtrlHeight + iSpacing;
		}
	}

	// ROI
	m_lst_3DCalROI.MoveWindow(iLeft, iTop, iListWidth, iListHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_3D_Depth::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: Set_3DCalInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_3DCal_Op * st3DCalOp
// Qualifier	:
// Last Update	: 2017/11/11 - 16:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Depth::Set_3DCalInfo(__in const ST_3DCal_Op* st3DCalOp)
{
		
}

//=============================================================================
// Method		: Get_3DCalInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_3DCal_Op & pst3DCalOp
// Qualifier	:
// Last Update	: 2017/11/11 - 16:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Depth::Get_3DCalInfo(__out ST_3DCal_Op& pst3DCalOp)
{
	
}

//=============================================================================
// Method		: Set_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:34
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Depth::Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo)
{

}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/11 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Depth::Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo)
{

}

//=============================================================================
// Method		: Set_3DCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __in ST_3DCal_Depth_Para * pst3DCal_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 17:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Depth::Set_3DCal_Para(__in ST_3DCal_Depth_Para* pst3DCal_Para)
{
	CString szText;

	// 3D Cal Depth -----------------------------------------------------------
	szText.Format(_T("%d"), pst3DCal_Para->nImgWidth);
	m_ed_Param_Depth[Param_3D_Depth_ImgWidth].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nImgHeight);
	m_ed_Param_Depth[Param_3D_Depth_ImgHeight].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nSamplingStep);
	m_ed_Param_Depth[Param_3D_Depth_SamplingStep].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nNumofCornerX);
	m_ed_Param_Depth[Param_3D_Depth_NumofCornerX].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nNumofCornerY);
	m_ed_Param_Depth[Param_3D_Depth_NumofCornerY].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fChessMarginX);
	m_ed_Param_Depth[Param_3D_Depth_ChessMarginX].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fChessMarginY);
	m_ed_Param_Depth[Param_3D_Depth_ChessMarginY].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fPatternSizeX);
	m_ed_Param_Depth[Param_3D_Depth_PatternSizeX].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fPatternSizeY);
	m_ed_Param_Depth[Param_3D_Depth_PatternSizeY].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nIrThres);
	m_ed_Param_Depth[Param_3D_Depth_IrThres].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_Para->fStdThres);
	m_ed_Param_Depth[Param_3D_Depth_StdThres].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_Para->nUseDepthFilter);
	m_ed_Param_Depth[Param_3D_Depth_UseDepthFilter].SetWindowText(szText);

	//szText.Format(_T("%d"), pst3DCal_Para->ROI[MAX_3DCAL_ROI]);
	//m_ed_Param_Depth[Param_3D_Depth_ROI].SetWindowText(szText);

	m_lst_3DCalROI.InsertFullData(pst3DCal_Para);
}

//=============================================================================
// Method		: Get_3DCal_Para
// Access		: public  
// Returns		: void
// Parameter	: __out ST_3DCal_Depth_Para & stOut3DCal_Para
// Qualifier	:
// Last Update	: 2017/11/12 - 17:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_3D_Depth::Get_3DCal_Para(__out ST_3DCal_Depth_Para& stOut3DCal_Para)
{
	CString szText;

	// 3D Cal Depth -----------------------------------------------------------
	
	m_ed_Param_Depth[Param_3D_Depth_ImgWidth].GetWindowText(szText);
	stOut3DCal_Para.nImgWidth				= _ttoi(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_ImgHeight].GetWindowText(szText);
	stOut3DCal_Para.nImgHeight			= _ttoi(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_SamplingStep].GetWindowText(szText);
	stOut3DCal_Para.nSamplingStep			= _ttoi(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_NumofCornerX].GetWindowText(szText);
	stOut3DCal_Para.nNumofCornerX			= _ttoi(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_NumofCornerY].GetWindowText(szText);
	stOut3DCal_Para.nNumofCornerY			= _ttoi(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_ChessMarginX].GetWindowText(szText);
	stOut3DCal_Para.fChessMarginX			= (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_ChessMarginY].GetWindowText(szText);
	stOut3DCal_Para.fChessMarginY			= (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_PatternSizeX].GetWindowText(szText);
	stOut3DCal_Para.fPatternSizeX			= (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_PatternSizeY].GetWindowText(szText);
	stOut3DCal_Para.fPatternSizeY			= (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_IrThres].GetWindowText(szText);
	stOut3DCal_Para.nIrThres				= _ttoi(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_StdThres].GetWindowText(szText);
	stOut3DCal_Para.fStdThres				= (float)_ttof(szText.GetBuffer(0));

	m_ed_Param_Depth[Param_3D_Depth_UseDepthFilter].GetWindowText(szText);
	stOut3DCal_Para.nUseDepthFilter		= _ttoi(szText.GetBuffer(0));

	m_lst_3DCalROI.GetCellData(stOut3DCal_Para);
}

