﻿//*****************************************************************************
// Filename	: 	Dlg_OperMode.cpp
// Created	:	2016/11/6 - 19:09
// Modified	:	2016/11/6 - 19:09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Dlg_OperMode.cpp : implementation file
//

#include "stdafx.h"
#include "Dlg_OperMode.h"
#include "afxdialogex.h"
#include "Def_WindowMessage_Cm.h"


#define		IDC_ST_OPERMODE_MODE_T		1001
#define		IDC_ED_OPERMODE_PASSWORD	1003

#define		IDC_RB_OPER_MODE_FST		1200
#define		IDC_RB_OPER_MODE_LST		IDC_RB_OPER_MODE_FST + OpMode_MaxEnum - 1

//=============================================================================
// CDlg_OperMode dialog
//=============================================================================
IMPLEMENT_DYNAMIC(CDlg_OperMode, CDialogEx)

CDlg_OperMode::CDlg_OperMode(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_OperMode::IDD, pParent)
{
	VERIFY(m_font_Large.CreateFont(
		20,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	VERIFY(m_font_Default.CreateFont(
		36,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	Reset_OperateMode();
}

CDlg_OperMode::~CDlg_OperMode()
{
	m_font_Large.DeleteObject();
	m_font_Default.DeleteObject();
}

void CDlg_OperMode::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_OperMode, CDialogEx)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_EN_CHANGE	(IDC_ED_OPERMODE_PASSWORD, OnEnChangeEdPassword)
	ON_COMMAND_RANGE(IDC_RB_OPER_MODE_FST, IDC_RB_OPER_MODE_LST, OnCommandRangeRbOperMode)
END_MESSAGE_MAP()


// CDlg_OperMode message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/11/4 - 11:45
// Desc.		:
//=============================================================================
int CDlg_OperMode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Title.SetBackColor(Gdiplus::Color::Black);
	m_st_Title.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Title.SetFont_Gdip(L"Arial", 24.0F);

	for (UINT nIdx = 0; nIdx < OpMode_MaxEnum; nIdx++)
	{
		m_rb_OperMode[nIdx].Create(g_szOperateMode[nIdx], WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON, rectDummy, this, IDC_RB_OPER_MODE_FST + nIdx);

		m_rb_OperMode[nIdx].SetFont(&m_font_Large);

		m_rb_OperMode[nIdx].m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
		m_rb_OperMode[nIdx].SetImage(IDB_SELECTNO_16);
		m_rb_OperMode[nIdx].SetCheckedImage(IDB_SELECT_16);
		m_rb_OperMode[nIdx].SizeToContent();
	}

	m_rb_OperMode[OpMode_Production].ModifyStyle(0, WS_GROUP);
	m_rb_OperMode[OpMode_Production].SetCheck(TRUE);


	m_st_Password.SetBackColor(Gdiplus::Color::Black);
	m_st_Password.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Password.SetFont_Gdip(L"Arial", 20.0F);

	m_st_Title.Create(_T("Operate Mode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_st_Password.Create(_T("PW"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_ed_Password.Create(dwStyle | WS_BORDER | WS_TABSTOP | ES_CENTER | ES_PASSWORD, rectDummy, this, IDC_ED_OPERMODE_PASSWORD);

	m_bn_OK.Create(_T("OK"), dwStyle | WS_TABSTOP, rectDummy, this, IDOK);
	m_bn_Cancel.Create(_T("Cancel"), dwStyle | WS_TABSTOP, rectDummy, this, IDCANCEL);

	m_ed_Password.SetFont(&m_font_Default);
	m_ed_Password.SetFocus();
	m_ed_Password.EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/11/4 - 11:45
// Desc.		:
//=============================================================================
void CDlg_OperMode::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin - iMagrin;
	int iCtrlWidth = iWidth;
	int iCtrlHeight = (iHeight - (iSpacing * (OpMode_MaxEnum)) - iCateSpacing) / (3 + OpMode_MaxEnum);
	int iStaticWidth = 80;
	int iTempWidth	= iWidth - iStaticWidth;
	int iSubLeft	= iLeft + iStaticWidth;

	m_st_Title.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	iTop += iCtrlHeight + iSpacing;
	
	UINT nNotUseCount = 0;
	for (UINT nIdx = 0; nIdx < enOperateMode::OpMode_MaxEnum; nIdx++)
	{
		if (m_bUseMode[nIdx])
		{
			m_rb_OperMode[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

			iTop += iCtrlHeight + iSpacing;
		}
		else
		{
			m_rb_OperMode[nIdx].MoveWindow(0, 0, 0, 0);
			++nNotUseCount;
		}
	}

	for (UINT nIdx = 0; nIdx < nNotUseCount; nIdx++)
	{
		iTop += iCtrlHeight + iSpacing;
	}
	
	iCtrlWidth = (iWidth - iSpacing) / 2;

	iLeft = iMagrin;
	//iTop += iCtrlHeight + iSpacing;
	m_st_Password.MoveWindow(iLeft, iTop, iStaticWidth, iCtrlHeight);
	m_ed_Password.MoveWindow(iSubLeft, iTop, iTempWidth, iCtrlHeight);

	iTop += iCtrlHeight + iCateSpacing;
	iCtrlWidth = (iWidth - (iSpacing * 2)) / 3;
	m_bn_OK.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iCtrlWidth + iSpacing;
	iLeft += iCtrlWidth + iSpacing;
	m_bn_Cancel.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_OperMode::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		// 여기에 Enter키 기능 작성
		return TRUE;
	}
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
	{
		// 여기에 ESC키 기능 작성       
		return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_OperMode::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	Load_OperMode();

	m_ed_Password.SetFocus();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// Method		: OnEnChangeEdPassword
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/2 - 11:33
// Desc.		:
//=============================================================================
void CDlg_OperMode::OnEnChangeEdPassword()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}

//=============================================================================
// Method		: OnCommandRangeRbOperMode
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/3/12 - 19:12
// Desc.		:
//=============================================================================
void CDlg_OperMode::OnCommandRangeRbOperMode(UINT nID)
{
	UINT nIndex = nID - IDC_RB_OPER_MODE_FST;

	if (OpMode_Production == nIndex)
	{
		m_ed_Password.EnableWindow(FALSE);
	}
	else
	{
		m_ed_Password.EnableWindow(TRUE);
		m_ed_Password.SetFocus();
	}
}

//=============================================================================
// Method		: OnOK
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/7 - 14:19
// Desc.		:
//=============================================================================
void CDlg_OperMode::OnOK()
{
 	if (BST_CHECKED == m_rb_OperMode[OpMode_Production].GetCheck())
	{
		Save_OperMode();

		AfxGetApp()->GetMainWnd()->SendMessage(WM_EQP_OPER_MODE, (LPARAM)m_nOperMode, 0);
	}
	else
 	{
 		if (CheckPassword())
 		{
 			Save_OperMode();
 
 			AfxGetApp()->GetMainWnd()->SendMessage(WM_EQP_OPER_MODE, (LPARAM)m_nOperMode, 0);
 
 			// Delay?
 		}
 		else
 		{
 			// 암호가 틀림
 			AfxMessageBox(_T("Password incorrect!!"));
 			return;
 		}
 	}

	CDialogEx::OnOK();
}

//=============================================================================
// Method		: OnCancel
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/7 - 19:10
// Desc.		:
//=============================================================================
void CDlg_OperMode::OnCancel()
{
	CDialogEx::OnCancel();
}

//=============================================================================
// Method		: LoadAcessMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/7 - 14:31
// Desc.		:
//=============================================================================
void CDlg_OperMode::Load_OperMode()
{
	CString szText;
	for (UINT nIdx = 0; nIdx < enOperateMode::OpMode_MaxEnum; nIdx++)
	{
		m_rb_OperMode[nIdx].SetCheck(BST_UNCHECKED);
	}

	m_rb_OperMode[m_nOperMode].SetCheck(BST_CHECKED);
}

//=============================================================================
// Method		: SaveAcessMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/8 - 9:52
// Desc.		:
//=============================================================================
void CDlg_OperMode::Save_OperMode()
{
	enOperateMode nOperMode;
	for (UINT nIdx = 0; nIdx < m_arOperModez.GetCount(); nIdx++)
	{
		nOperMode = (enOperateMode)m_arOperModez.GetAt(nIdx);
		
		if (BST_CHECKED == m_rb_OperMode[nOperMode].GetCheck())
		{
			m_nOperMode = nOperMode;
			break;
		}
	}
}

//=============================================================================
// Method		: CheckPassword
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/11/7 - 18:00
// Desc.		:
//=============================================================================
BOOL CDlg_OperMode::CheckPassword()
{
	CString szPass;
	CString szLoadPass;
	m_ed_Password.GetWindowText(szPass);

	szLoadPass = m_regManagement.LoadPassword_Admin();
	if (0 == szPass.Compare(szLoadPass))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}

	if (0 == szPass.GetLength())
	{
		m_nOperMode = OpMode_Production;
		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: Set_OperateMode
// Access		: public  
// Returns		: void
// Parameter	: __in enOperateMode nOnlineMode
// Qualifier	:
// Last Update	: 2016/11/7 - 17:53
// Desc.		:
//=============================================================================
void CDlg_OperMode::Set_OperateMode(__in enOperateMode nOnlineMode)
{
	BOOL bFind = FALSE;
	for (UINT nIdx = 0; nIdx < m_arOperModez.GetCount(); nIdx++)
	{
		if (nOnlineMode == m_arOperModez.GetAt(nIdx))
		{
			bFind = TRUE;
			break;
		}
	}

	if (bFind)
	{
		m_nOperMode = nOnlineMode;
	}
}

//=============================================================================
// Method		: Add_OperateMode
// Access		: public  
// Returns		: void
// Parameter	: __in enOperateMode nOnlineMode
// Qualifier	:
// Last Update	: 2018/3/12 - 17:32
// Desc.		:
//=============================================================================
void CDlg_OperMode::Add_OperateMode(__in enOperateMode nOnlineMode)
{
	BOOL bFind = FALSE;
	for (UINT nIdx = 0; nIdx < m_arOperModez.GetCount(); nIdx++)
	{
		if (nOnlineMode == m_arOperModez.GetAt(nIdx))
		{
			bFind = TRUE;
			break;
		}
	}

	if (FALSE == bFind)
	{
		m_arOperModez.Add(nOnlineMode);
		m_bUseMode[nOnlineMode] = TRUE;
	}
}

//=============================================================================
// Method		: Reset_OperateMode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/12 - 18:56
// Desc.		:
//=============================================================================
void CDlg_OperMode::Reset_OperateMode()
{
	m_arOperModez.RemoveAll();

	m_arOperModez.Add(enOperateMode::OpMode_Production);

	m_nOperMode = OpMode_Production;

	memset(m_bUseMode, 0, sizeof(BOOL) * OpMode_MaxEnum);
	m_bUseMode[OpMode_Production] = TRUE;;
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2018/3/12 - 20:26
// Desc.		:
//=============================================================================
void CDlg_OperMode::SetSystemType(__in enInsptrSysType nSysType)
{
	switch (nSysType)
	{
	case Sys_2D_Cal:
	case Sys_3D_Cal:
	{
		Add_OperateMode(enOperateMode::OpMode_Master);
#ifdef _DEBUG
		Add_OperateMode(enOperateMode::OpMode_DryRun);
#endif
	}
		break;

	case Sys_Focusing:
	{
		Add_OperateMode(enOperateMode::OpMode_Master);
		Add_OperateMode(enOperateMode::OpMode_StartUp_Check);
	}
		break;

	case Sys_Image_Test:
	case Sys_IR_Image_Test:
	{
		Add_OperateMode(enOperateMode::OpMode_Master);
		Add_OperateMode(enOperateMode::OpMode_StartUp_Check);
#ifdef _DEBUG
		Add_OperateMode(enOperateMode::OpMode_DryRun);
#endif
	}
		break;

	default:
		break;
	}
}
