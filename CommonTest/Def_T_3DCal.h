#ifndef Def_T_3DCal_h__
#define Def_T_3DCal_h__

#include <afxwin.h>
#include "Def_T_LGE_Common.h"
#include "Def_Test_Cm.h"

#pragma pack(push,1)

#define		MAX_3DCAL_ROI				12
#define		MAX_TI_3DCAL_Eval_ROI			6
#define		MAX_3DCAL_Re_DistCoef		12
#define		MAX_3DCAL_Re_AccracyMap		30
#define		MAX_EVAL_ExternalGT			3
#define		MAX_EVAL_Re_Chess			3
#define		MAX_EVAL_Re_TofDist			3
#define		MAX_EVAL_Re_Error			18
#define		MAX_EVAL_Shading			2
#define		MAX_EVAL_ShadingV			2
#define		MAX_EVAL_ARFERROR			6

#define		MAX_3DCAL_Position			MAX_3DCAL_ROI

typedef enum enParam_3D_Depth
{
	Param_3D_Depth_ImgWidth,
	Param_3D_Depth_ImgHeight,
	Param_3D_Depth_NumofCornerX,
	Param_3D_Depth_NumofCornerY,
	Param_3D_Depth_ChessMarginX,
	Param_3D_Depth_ChessMarginY,
	Param_3D_Depth_PatternSizeX,
	Param_3D_Depth_PatternSizeY,
	Param_3D_Depth_IrThres,
	Param_3D_Depth_StdThres,
	Param_3D_Depth_SamplingStep,
	Param_3D_Depth_UseDepthFilter,
	Param_3D_Depth_MaxNum,
};

static LPCTSTR	g_Param_3D_Depth[] =
{
	_T("Img Width"),
	_T("Img Height"),
	_T("Num of Corner X"),
	_T("Num of Corner Y"),
	_T("Chess Margin X"),
	_T("Chess Margin Y"),
	_T("Pattern Size X"),
	_T("Pattern Size Y"),
	_T("Ir Thres"),
	_T("Std Thres"),
	_T("Sampling Step"),
	_T("Use Depth Filter"),
	NULL
};

typedef enum enParam_3D_Eval
{
	Param_3D_Eval_ImgWidth,
	Param_3D_Eval_ImgHeight,
	Param_3D_Eval_NumofCornerX,
	Param_3D_Eval_NumofCornerY,
	Param_3D_Eval_PatternSizeX,
	Param_3D_Eval_PatternSizeY,
	Param_3D_Eval_ChessMarginX,
	Param_3D_Eval_ChessMarginY,
	Param_3D_Eval_UseDepthFileter,
	Param_3D_Eval_IrThres,
	Param_3D_Eval_ExternalGT_0,
	Param_3D_Eval_ExternalGT_1,
	Param_3D_Eval_ExternalGT_2,
	Param_3D_Eval_ShadeROIH,
	Param_3D_Eval_ShadeROIV,
	Param_3D_Eval_ShadeROID,
	Param_3D_Eval_PatchSizeU,
	Param_3D_Eval_PatchSizeV,
	Param_3D_Eval_PatchStepU,
	Param_3D_Eval_PatchStepV,
	Param_3D_Eval_MaxNum,
};

static LPCTSTR	g_Param_3D_Eval[] =
{
	_T("Img Width"),
	_T("Img Height"),
	_T("Num of Corner X"),
	_T("Num of Corner Y"),
	_T("Pattern Size X"),
	_T("Pattern Size Y"),
	_T("Chess Margin X"),
	_T("Chess Margin Y"),
	_T("Use Depth Fileter"),
	_T("Ir Thres"),
	_T("External GT_0"),
	_T("External GT_1"),
	_T("External GT_2"),
	_T("Shade ROI H"),
	_T("Shade ROI V"),
	_T("Shade ROI D"),
	_T("Patch Size U"),
	_T("Patch Size V"),
	_T("Patch Step U"),
	_T("Patch Step V"),
	NULL
};

typedef enum enSpec_3DCal
{
	Spec_3D_Depth_DistCoef_05,			// 6
	Spec_3D_Depth_TotalFittingError,
	Spec_3D_Depth_AccracyMap0_17,		// 30
	Spec_3D_Depth_AccracyMap17_29,		// 30
	Spec_3D_Depth_InvalidRatio,
	Spec_3D_Depth_FailureCnt,
	Spec_3DCal_MaxNum,
};

static LPCTSTR	g_szSpec3DCal[] =
{
	_T("Depth: Dist Coef [5]"),
	_T("Depth: Total Fitting Error"),
	_T("Depth: Accracy Map0_17"),
	_T("Depth: Accracy Map17_29"),
	_T("Depth: Invalid Ratio"),
	_T("Depth: Failure Cnt"),
	NULL
};

typedef enum enSpec_3DCal_Eval
{
	Spec_3D_Eval_Chess_00,		// 3
	Spec_3D_Eval_Chess_01,		// 3
	Spec_3D_Eval_Chess_02,		// 3
	Spec_3D_Eval_TofDist_00,	// 3
	Spec_3D_Eval_TofDist_01,	// 3
	Spec_3D_Eval_TofDist_02,	// 3
	Spec_3D_Eval_Error,			// 18
	Spec_3D_Eval_ShadingH,		// 10
	Spec_3D_Eval_ShadingV,		// 6
	Spec_3D_Eval_ShadingD0,		// 10
	Spec_3D_Eval_ShadingD1,		// 10
	Spec_3D_Eval_FailureCnt,
	Spec_3D_Eval_IntensityC,
	Spec_3D_Eval_MaxNum,
};

static LPCTSTR	g_szSpec3DCal_Eval[] =
{
	_T("Eval: Chess [0]"),
	_T("Eval: Chess [1]"),
	_T("Eval: Chess [2]"),
	_T("Eval: T of Dist [0]"),
	_T("Eval: T of Dist [1]"),
	_T("Eval: T of Dist [2]"),
	_T("Eval: Error"),
	_T("Eval: Shading H"),
	_T("Eval: Shading V"),
	_T("Eval: Shading D0"),
	_T("Eval: Shading D1"),
	_T("Eval: Failure Cnt"),
	_T("Eval: Intensity C"),
	NULL
};

typedef struct _tag_DTC_Description
{
	CString szID;
	CString szDesc;

	_tag_DTC_Description()
	{
		Reset();
	};

	void Reset()
	{
		szID.IsEmpty();
		szDesc.IsEmpty();		
	};	

}ST_DTC_Desc, *PST_DTC_Desc;

typedef	CArray<ST_DTC_Desc, ST_DTC_Desc>	ARR_DTC_DESC;

//-------------------------------------------------------------------
// 3D CAL ROI 정보
//-------------------------------------------------------------------
#define ST_3DCal_ROI	ST_LGE_ROI
#define PST_3DCal_ROI	ST_LGE_ROI*

typedef CArray<ST_3DCal_ROI, ST_3DCal_ROI>	ARR_3D_ROI;

//-------------------------------------------------------------------
// 3D CAL Distance Parameter
//-------------------------------------------------------------------
typedef struct _tag_3DCal_Depth_Para
{
	int				nImgWidth;
	int				nImgHeight;
	int				nSamplingStep;
	int				nNumofCornerX;			 // default: 4
	int				nNumofCornerY;			 // default: 6
	float			fChessMarginX;			 // default: 700
	float			fChessMarginY;			 // default: 350
	float			fPatternSizeX;			 // defauil: 60
	float			fPatternSizeY;			 // defauil: 60
	int				nIrThres;				 // default: 16
	float			fStdThres;				 // default: 17
	int				nUseDepthFilter;
	float			arfCoefTemp[MAX_3DCAL_Re_DistCoef];
	ST_3DCal_ROI	ROI[MAX_3DCAL_ROI];


	_tag_3DCal_Depth_Para()
	{
		Reset();
	};

	_tag_3DCal_Depth_Para& operator= (const _tag_3DCal_Depth_Para& ref)
	{
		nImgWidth			= ref.nImgWidth;
		nImgHeight			= ref.nImgHeight;
		nSamplingStep		= ref.nSamplingStep;
		nNumofCornerX		= ref.nNumofCornerX;
		nNumofCornerY		= ref.nNumofCornerY;
		fChessMarginX		= ref.fChessMarginX;
		fChessMarginY		= ref.fChessMarginY;
		fPatternSizeX		= ref.fPatternSizeX;
		fPatternSizeY		= ref.fPatternSizeY;
		nIrThres			= ref.nIrThres;
		fStdThres			= ref.fStdThres;
		nUseDepthFilter		= ref.nUseDepthFilter;
		for (UINT nIdx = 0; nIdx < MAX_3DCAL_ROI; nIdx++)
		{
			ROI[nIdx] = ref.ROI[nIdx];
		}

		for (UINT nIdx = 0; nIdx < MAX_3DCAL_Re_DistCoef; nIdx++)
		{
			arfCoefTemp[nIdx] = ref.arfCoefTemp[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		nImgWidth			= 0;
		nImgHeight			= 0;
		nSamplingStep		= 0;
		nNumofCornerX		= 0;
		nNumofCornerY		= 0;
		fChessMarginX		= 0.0f;
		fChessMarginY		= 0.0f;
		fPatternSizeX		= 0.0f;
		fPatternSizeY		= 0.0f;
		nIrThres			= 0;
		fStdThres			= 0.0f;
		nUseDepthFilter		= 0;
		for (UINT nIdx = 0; nIdx < MAX_3DCAL_ROI; nIdx++)
		{
			ROI[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < MAX_3DCAL_Re_DistCoef; nIdx++)
		{
			arfCoefTemp[nIdx] = 0.0;
		}
	};

}ST_3DCal_Depth_Para, *PST_3DCal_Depth_Para;

//-------------------------------------------------------------------
// 3D CAL Distance Results
//-------------------------------------------------------------------
typedef struct _tag_3DCal_Depth_Result
{
	float	fArfDistCoef[MAX_3DCAL_Re_DistCoef];
	float	fTotalFittingError;						// Pass: <20.0
	float	fArtAccracyMap[MAX_3DCAL_Re_AccracyMap];// Pass: = 0
	float	fInvalidRatio;							// Pass: < 0.1
	int		nFailureCnt;							// Pass: = 0

	_tag_3DCal_Depth_Result()
	{
		Reset();
	};

	_tag_3DCal_Depth_Result& operator= (const _tag_3DCal_Depth_Result& ref)
	{
		for (UINT nIdx = 0; nIdx < MAX_3DCAL_Re_DistCoef; nIdx++)
		{
			fArfDistCoef[nIdx]	= ref.fArfDistCoef[nIdx];
		}		
		fTotalFittingError	= ref.fTotalFittingError;
		for (UINT nIdx = 0; nIdx < MAX_3DCAL_Re_AccracyMap; nIdx++)
		{
			fArtAccracyMap[nIdx]= ref.fArtAccracyMap[nIdx];
		}
		fInvalidRatio		= ref.fInvalidRatio;
		nFailureCnt			= ref.nFailureCnt;

		return *this;
	};

	void Reset()
	{
		memset(fArfDistCoef, 0, sizeof(float) * MAX_3DCAL_Re_DistCoef);
		fTotalFittingError	= 0.0;
		memset(fArtAccracyMap, 0, sizeof(float) * MAX_3DCAL_Re_AccracyMap);
		fInvalidRatio		= 0.0;
		nFailureCnt			= 0;
	};

}ST_3DCal_Depth_Result, *PST_3DCal_Depth_Result;

typedef struct _tag_3DCal_Depth_Spec
{
	float	fArfDistCoef[MAX_3DCAL_Re_DistCoef];	// MAX_3DCAL_Re_DistCoef
	float	fTotalFittingError;						// Pass: <20.0
	float	fArtAccracyMap0_17;						// MAX_3DCAL_Re_AccracyMap Pass: = 0
	float	fArtAccracyMap17_29;					// MAX_3DCAL_Re_AccracyMap Pass: = 0
	float	fInvalidRatio;							// Pass: < 0.1
	int		nFailureCnt;							// Pass: = 0

	_tag_3DCal_Depth_Spec()
	{
		Reset();
	};

	_tag_3DCal_Depth_Spec& operator= (const _tag_3DCal_Depth_Spec& ref)
	{
		for (UINT nIdx = 0; nIdx < MAX_3DCAL_Re_DistCoef; nIdx++)
		{
			fArfDistCoef[nIdx]	= ref.fArfDistCoef[nIdx];
		}		
		fTotalFittingError	= ref.fTotalFittingError;
		fArtAccracyMap0_17	= ref.fArtAccracyMap0_17;
		fArtAccracyMap17_29 = ref.fArtAccracyMap17_29;
		fInvalidRatio		= ref.fInvalidRatio;
		nFailureCnt			= ref.nFailureCnt;

		return *this;
	};

	void Reset()
	{
		memset(fArfDistCoef, 0, sizeof(float) * MAX_3DCAL_Re_DistCoef);
		fTotalFittingError	= 0.0;
		fArtAccracyMap0_17	= 0.0;
		fArtAccracyMap17_29 = 0.0;
		fInvalidRatio		= 0.0;
		nFailureCnt			= 0;
	};

}ST_3DCal_Depth_Spec, *PST_3DCal_Depth_Spec;

//-------------------------------------------------------------------
// 3D CAL Distance 전체 데이터
//-------------------------------------------------------------------
typedef struct _tag_3DCal_Depth_Op
{
	ST_3DCal_Depth_Para		Parameter;
	ST_3DCal_Depth_Result	Result;		// Results
	ST_3DCal_Depth_Spec		Spec_Min;	// Results Spec Min
	ST_3DCal_Depth_Spec		Spec_Max;	// Results Spec Max
	
	_tag_3DCal_Depth_Op()
	{
		Reset();
	};

	_tag_3DCal_Depth_Op& operator= (const _tag_3DCal_Depth_Op& ref)
	{
		Parameter	= ref.Parameter;
		Result		= ref.Result;
		Spec_Min	= ref.Spec_Min;
		Spec_Max	= ref.Spec_Max;

		return *this;
	};

	void Reset()
	{
		Parameter.Reset();
		Result.Reset();
		Spec_Min.Reset();
		Spec_Max.Reset();
	};

}ST_3DCal_Depth_Op, *PST_3DCal_Depth_Op;

//-------------------------------------------------------------------
// 3D CAL Evaluation Parameters
//-------------------------------------------------------------------
typedef struct _tag_3DCal_Eval_Para
{
	int				nImgWidth;
	int				nImgHeight;
	int				nNumofCornerX;
	int				nNumofCornerY;
	float			fPatternSizeX;
	float			fPatternSizeY;
	float			fChessMarginX;
	float			fChessMarginY;
	int				nUseDepthFileter;
	
	int				nIrThres;
	float			fExternalGT[MAX_EVAL_ExternalGT];
	ST_3DCal_ROI	ROI[MAX_TI_3DCAL_Eval_ROI];

	int				nPatchSizeU;
	int				nPatchSizeV;
	int				nPatchStepU;
	int				nPatchStepV;
	int				nShadeROIH;
	int				nShadeROIV;
	int				nShadeROID;
	int				nShadeIndex;

	_tag_3DCal_Eval_Para()
	{
		Reset();
	};

	_tag_3DCal_Eval_Para& operator= (const _tag_3DCal_Eval_Para& ref)
	{
		nImgWidth			= ref.nImgWidth;
		nImgHeight			= ref.nImgHeight;
		nNumofCornerX		= ref.nNumofCornerX;
		nNumofCornerY		= ref.nNumofCornerY;
		fPatternSizeX		= ref.fPatternSizeX;
		fPatternSizeY		= ref.fPatternSizeY;
		fChessMarginX		= ref.fChessMarginX;
		fChessMarginY		= ref.fChessMarginY;
		nUseDepthFileter	= ref.nUseDepthFileter;
		nIrThres			= ref.nIrThres;
		for (UINT nIdx = 0; nIdx < MAX_EVAL_ExternalGT; nIdx++)
		{
			fExternalGT[nIdx] = ref.fExternalGT[nIdx];
		}	

		for (UINT nIdx = 0; nIdx < MAX_TI_3DCAL_Eval_ROI; nIdx++)
		{
			ROI[nIdx] = ref.ROI[nIdx];
		}		

		nPatchSizeU			= ref.nPatchSizeU;
		nPatchSizeV			= ref.nPatchSizeV;
		nPatchStepU			= ref.nPatchStepU;
		nPatchStepV			= ref.nPatchStepV;
		nShadeROIH			= ref.nShadeROIH;
		nShadeROIV			= ref.nShadeROIV;
		nShadeROID			= ref.nShadeROID;
		nShadeIndex			= ref.nShadeIndex;

		return *this;
	};

	void Reset()
	{
		nImgWidth			= 0;
		nImgHeight			= 0;
		nNumofCornerX		= 0;
		nNumofCornerY		= 0;
		fPatternSizeX		= 0.0f;
		fPatternSizeY		= 0.0f;
		fChessMarginX		= 0.0f;
		fChessMarginY		= 0.0f;
		nUseDepthFileter	= 0;
		nIrThres			= 0;
		for (UINT nIdx = 0; nIdx < MAX_EVAL_ExternalGT; nIdx++)
		{
			fExternalGT[nIdx] = 0;
		}

		for (UINT nIdx = 0; nIdx < MAX_TI_3DCAL_Eval_ROI; nIdx++)
		{
			ROI[nIdx].Reset();
		}		

		nPatchSizeU			= 0;
		nPatchSizeV			= 0;
		nPatchStepU			= 0;
		nPatchStepV			= 0;
		nShadeROIH			= 0;
		nShadeROIV			= 0;
		nShadeROID			= 0;
		nShadeIndex			= 1;
	};

}ST_3DCal_Eval_Para, *PST_3DCal_Eval_Para;

//-------------------------------------------------------------------
// 3D CAL Evaluation Result
//-------------------------------------------------------------------

/* evaluation */
typedef struct
{
	float fChess;
	float fTofDist;
	float arfError[MAX_EVAL_ARFERROR];
}ST_LT_3DEvalCalCoreEvalInfo;
typedef struct
{
	float fDistance; /* pixel */
	float fValue; /* Target Intensity / Center Intensity  * 100(%) */
}ST_LT_3DEvalhadingInfo;

typedef struct _tag_3DCal_Eval_Result
{
	ST_LT_3DEvalCalCoreEvalInfo arstEval[MAX_EVAL_Re_Chess];
	ST_LT_3DEvalhadingInfo arstShadingH[MAX_EVAL_Shading];
	ST_LT_3DEvalhadingInfo arstShadingV[MAX_EVAL_ShadingV];
//	ST_LT_3DEvalhadingInfo arstShadingD0[MAX_EVAL_Shading]; /* Increase direction */
//	ST_LT_3DEvalhadingInfo arstShadingD1[MAX_EVAL_Shading]; /* Decrease direction */
	int nFailureCnt;
	float fIntensityC;									/* intensity of center */


	_tag_3DCal_Eval_Result()
	{
		Reset();
	};

	_tag_3DCal_Eval_Result& operator= (const _tag_3DCal_Eval_Result& ref)
	{
		memcpy(&arstEval, ref.arstEval, sizeof(ST_LT_3DEvalCalCoreEvalInfo)*MAX_EVAL_Re_Chess);
		memcpy(&arstShadingH, ref.arstShadingH, sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_Shading);
//		memcpy(&arstShadingD0, ref.arstShadingD0, sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_Shading);
//		memcpy(&arstShadingD1, ref.arstShadingD1, sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_Shading);

		memcpy(&arstShadingV, ref.arstShadingV, sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_ShadingV);
		
		nFailureCnt		= ref.nFailureCnt;
		fIntensityC		= ref.fIntensityC;

		return *this;
	};

	void Reset()
	{
		memset(&arstEval,			0x00,		sizeof(ST_LT_3DEvalCalCoreEvalInfo)*MAX_EVAL_Re_Chess);
		memset(&arstShadingH,		0x00,		sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_Shading);
//		memset(&arstShadingD0,		0x00,		sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_Shading);
//		memset(&arstShadingD1,		0x00,		sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_Shading);

		memset(&arstShadingV,		0x00,		sizeof(ST_LT_3DEvalhadingInfo)*MAX_EVAL_ShadingV);

		nFailureCnt		= 0;
		fIntensityC		= 0.0f;
	};

}ST_3DCal_Eval_Result, *PST_3DCal_Eval_Result;

typedef struct _tag_3DCal_Eval_Spec
{
	float		fChess[MAX_EVAL_Re_Chess];
	float		fTofDist[MAX_EVAL_Re_TofDist];
	float		arfError;		//[MAX_EVAL_Re_Error];
	double		arstShadingH;	//[MAX_EVAL_Shading];
	double		arstShadingV;	//[MAX_EVAL_ShadingV];
	double		arstShadingD0;	//[MAX_EVAL_Shading];
	double		arstShadingD1;	//[MAX_EVAL_Shading];
	int			nFailureCnt;
	float		fIntensityC;

	_tag_3DCal_Eval_Spec()
	{
		Reset();
	};

	_tag_3DCal_Eval_Spec& operator= (const _tag_3DCal_Eval_Spec& ref)
	{
		for (UINT nIdx = 0; nIdx < MAX_EVAL_Re_Chess; nIdx++)
		{
			fChess[nIdx] = ref.fChess[nIdx];
		}

		for (UINT nIdx = 0; nIdx < MAX_EVAL_Re_TofDist; nIdx++)
		{
			fTofDist[nIdx] = ref.fTofDist[nIdx];
		}
		arfError		= ref.arfError;
		arstShadingH	= ref.arstShadingH;
		arstShadingD0	= ref.arstShadingD0;
		arstShadingD1	= ref.arstShadingD1;
		arstShadingV	= ref.arstShadingV;
		nFailureCnt		= ref.nFailureCnt;
		fIntensityC		= ref.fIntensityC;

		return *this;
	};

	void Reset()
	{
		memset(fChess, 0, sizeof(float) * MAX_EVAL_Re_Chess);
		memset(fTofDist, 0, sizeof(float) * MAX_EVAL_Re_TofDist);
		arfError		= 0.0f;
		arstShadingH	= 0.0f;
		arstShadingV	= 0.0f;
		arstShadingD0	= 0.0f;
		arstShadingD1	= 0.0f;
		nFailureCnt		= 0;
		fIntensityC		= 0.0f;
	};

}ST_3DCal_Eval_Spec, *PST_3DCal_Eval_Spec;

//-------------------------------------------------------------------
// 3D CAL Evaluation 전체 데이터
//-------------------------------------------------------------------
typedef struct _tag_3DCal_Eval_Op
{
	ST_3DCal_Eval_Para		Parameter;
	ST_3DCal_Eval_Result	Result;		// Results
	ST_3DCal_Eval_Spec		Spec_Min;	// Results Spec Min
	ST_3DCal_Eval_Spec		Spec_Max;	// Results Spec Max

	_tag_3DCal_Eval_Op()
	{
		Reset();
	};

	_tag_3DCal_Eval_Op& operator= (const _tag_3DCal_Eval_Op& ref)
	{
		Parameter	= ref.Parameter;
		Result		= ref.Result;
		Spec_Min	= ref.Spec_Min;
		Spec_Max	= ref.Spec_Max;

		return *this;
	};

	void Reset()
	{
		Parameter.Reset();
		Result.Reset();
		Spec_Min.Reset();
		Spec_Max.Reset();
	};

}ST_3DCal_Eval_Op, *PST_3DCal_Eval_Op;

typedef struct _tagST_3DCal_TeachMotorPos
{
	double dbDistanceX;
	double dbDistanceY;

	_tagST_3DCal_TeachMotorPos()
	{
		dbDistanceX = 0.0;
		dbDistanceY = 0.0;
	};

	_tagST_3DCal_TeachMotorPos& operator= (const _tagST_3DCal_TeachMotorPos& ref)
	{
		dbDistanceX = ref.dbDistanceX;
		dbDistanceY = ref.dbDistanceY;

		return *this;
	};

	void Reset()
	{
		dbDistanceX = 0.0;
		dbDistanceY = 0.0;
	};

}ST_3DCal_TeachMotor, *PST_3DCal_TeachMotor;

//-------------------------------------------------------------------
// 3D CAL 전체 데이터
//-------------------------------------------------------------------
typedef struct _tag_3DCal_Op
{
	ST_3DCal_Depth_Op	Depth;
	ST_3DCal_Eval_Op	Evaluation;

	_tag_3DCal_Op()
	{
		Reset();
	};

	_tag_3DCal_Op& operator= (const _tag_3DCal_Op& ref)
	{
		Depth		= ref.Depth;
		Evaluation	= ref.Evaluation;

		return *this;
	};

	void Reset()
	{
		Depth.Reset();
		Evaluation.Reset();
	};

}ST_3DCal_Op, *PST_3DCal_Op;

//-----------------------------------------------------------------------------
// 3D Calibration 설비의 전체 데이터
//-----------------------------------------------------------------------------
// typedef struct _tag_3DCal_Para
// {
// 	ST_3DCal_Depth_Para		Depth;
// 	ST_3DCal_Eval_Para		Evaluation;
// 	// Dummy Hand
// 
// 	_tag_3DCal_Para()
// 	{
// 		Reset();
// 	};
// 
// 	_tag_3DCal_Para& operator= (const _tag_3DCal_Para& ref)
// 	{
// 		Depth		= ref.Depth;
// 		Evaluation	= ref.Evaluation;
// 
// 		return *this;
// 	};
// 
// 	void Reset()
// 	{
// 		Depth.Reset();
// 		Evaluation.Reset();
// 	};
// }ST_3DCal_Para, *PST_3DCal_Para;

typedef struct _tag_3DCal_Info
{
	ST_3DCal_Depth_Para		Depth_Param;
	ST_3DCal_Depth_Spec		Depth_Min;
	ST_3DCal_Depth_Spec		Depth_Max;
	ST_3DCal_Eval_Para		Eval_Param;
	ST_3DCal_Eval_Spec		Eval_Min;
	ST_3DCal_Eval_Spec		Eval_Max;
	ST_3DCal_TeachMotor		MotorPos[MAX_3DCAL_Position];
	// Dummy Hand

	_tag_3DCal_Info()
	{
		Reset();
	};

	_tag_3DCal_Info& operator= (const _tag_3DCal_Info& ref)
	{
		Depth_Param = ref.Depth_Param;
		Depth_Min	= ref.Depth_Min;
		Depth_Max	= ref.Depth_Max;
		Eval_Param	= ref.Eval_Param;
		Eval_Min	= ref.Eval_Min;
		Eval_Max	= ref.Eval_Max;

		for (UINT nIdx = 0; nIdx < MAX_3DCAL_Position; nIdx++)
		{
			MotorPos[nIdx] = ref.MotorPos[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		Depth_Param.Reset();
		Depth_Min.Reset();
		Depth_Max.Reset();
		Eval_Param.Reset();
		Eval_Min.Reset();
		Eval_Max.Reset();

		for (UINT nIdx = 0; nIdx < MAX_3DCAL_Position; nIdx++)
		{
			MotorPos[nIdx].Reset();
		}
	};
}ST_3DCal_Info, *PST_3DCal_Info;
#pragma pack(pop)

#endif // Def_T_3DCal_h__
