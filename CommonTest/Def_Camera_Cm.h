//*****************************************************************************
// Filename	: 	Def_Camera_Cm.h
// Created	:	2017/10/25 - 20:09
// Modified	:	2017/10/25 - 20:09
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Camera_Cm_h__
#define Def_Camera_Cm_h__

#include "Def_ResultCode_Cm.h"
#include "Def_DataStruct_Cm.h"

#pragma pack(push, 1)

//-----------------------------------------------------------------------------
// 카메라 검사 정보 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_CamInfo_Base
{
	CString				szLotID;			// Lot ID	
	CString				szRecipeName;		// 모델 이름
	CString				szOperatorName;		// 작업자 이름
	CString				szBarcode;			// 제품 바코드
	UINT				nMES_TryCnt;		// MES에서 수신된 차수

	ST_TestTime			TestTime;			// 검사에 관련된 측정된 시간
	DWORD				dwCycleTime;		// 검사 시작에서 배출까지 시간
	DWORD				dwTactTime;			// Tact Time (전 제품 배출시간부터 현 제품 배출시간)

	UINT				nSocketIndex;		// 카메라 위치 (Left, Right, Center)
	enTestProcess		nProgressStatus;	// 각 카메라의 검사 진행 상태
	enTestResult		nJudgment;			// 최종 결과, 제품 유무
	LRESULT				ResultCode;			// 결과 코드 (오류 코드)	

	//ST_StepMeasInfo		TestInfo;			// 검사 스텝 정보

	enTestResult		nJudgeLoading;		// 로딩 루틴 결과
	BOOL				bMES_Validation;	// MES 바코드 사용가능 여부??

	BOOL				bInitialize;
	BOOL				bFinalize;

	_tag_CamInfo_Base()
	{
		nMES_TryCnt			= 1;

		dwCycleTime			= 0;
		dwTactTime			= 0;
		nSocketIndex		= Para_Left;
		nProgressStatus		= enTestProcess::TP_Ready;
		nJudgment			= enTestResult::TR_Empty;
		ResultCode			= RC_OK;

		nJudgeLoading		= TR_Pass;
		bMES_Validation		= FALSE;

		bInitialize			= FALSE;
		bFinalize			= FALSE;
	};

	virtual void Reset(__in BOOL bWithoutBarcode = FALSE)
	{
		if (FALSE == bWithoutBarcode)
		{
			szBarcode.Empty();
			nMES_TryCnt = 1;
		}

		szLotID.Empty();
		szRecipeName.Empty();
		szOperatorName.Empty();

		TestTime.Reset();
		dwCycleTime			= 0;
		dwTactTime			= 0;
		nSocketIndex		= Para_Left;
		nProgressStatus		= enTestProcess::TP_Ready;
		nJudgment			= enTestResult::TR_Ready;
		ResultCode			= RC_OK;

		//TestInfo.ResetData();

		nJudgeLoading		= TR_Pass;
// 		bMES_Validation		= FALSE;

		bInitialize			= FALSE;
		bFinalize			= FALSE;
	};

	_tag_CamInfo_Base& operator= (_tag_CamInfo_Base& ref)
	{
		szLotID				= ref.szLotID;
		szRecipeName		= ref.szRecipeName;
		szOperatorName		= ref.szOperatorName;
		szBarcode			= ref.szBarcode;
		nMES_TryCnt			= ref.nMES_TryCnt;

		TestTime			= ref.TestTime;
		dwCycleTime			= ref.dwCycleTime;
		dwTactTime			= ref.dwTactTime;
		nSocketIndex		= ref.nSocketIndex;
		nProgressStatus		= ref.nProgressStatus;
		nJudgment			= ref.nJudgment;
		ResultCode			= ref.ResultCode;

		//TestInfo			= ref.TestInfo;
		nJudgeLoading		= ref.nJudgeLoading;
		bMES_Validation		= ref.bMES_Validation;

		bInitialize			= ref.bInitialize;
		bFinalize			= ref.bFinalize;

		return *this;
	};

	virtual void Reset_Measurment()
	{
		//TestTime.Reset();
		ResultCode			= RC_OK;
	};

	// 검사 진행 상태 설정
	void SetTestProgress(__in enTestProcess nProcess)
	{
		nProgressStatus = nProcess;
	};

	// 검사 기본 정보 설정
	void SetInformation(__in LPCTSTR szIn_LotName, __in LPCTSTR szIn_Barcode, __in LPCTSTR szIn_ModelName, __in LPCTSTR szIn_OperatorName)
	{
		szLotID			= szIn_LotName;
		szBarcode		= szIn_Barcode;
		szRecipeName	= szIn_ModelName;		// 모델 이름
		szOperatorName	= szIn_OperatorName;
	};

	// 팔레트 로딩 시작 시간 설정
	void Set_StartLoading()
	{
		dwCycleTime			= 0;
		TestTime.Set_StartLoading();
	};

	// 팔레트 로딩 종료 시간 설정
	void Set_EndUnloading()
	{
		TestTime.Set_EndUnloading();
		dwCycleTime = TestTime.Cycle.dwDuration;
	};

	// 검사 시작 시간 설정
	void Set_StartTest()
	{
		TestTime.Set_StartTest();
	};

	// 검사 종료 시간 설정
	void Set_EndTest()
	{
		TestTime.Set_EndTest();
	};

	// 검사 항목의 검사 시작 시간 설정
	void Set_StartTestItem(__in UINT nItemIndex)
	{
		TestTime.Set_StartTestItem(nItemIndex);
	};

	// 검사 항목의 검사 종료 시간 설정
	void Set_EndTestItem(__in UINT nItemIndex)
	{
		TestTime.Set_EndTestItem(nItemIndex);
	};

	// 장착된 소켓(Para) 위치
	void SetSocketIndex(__in UINT nIdx)
	{
		nSocketIndex = nIdx;
	};

}ST_CamInfo_Base, *PST_CamInfo_Base;

#pragma pack (pop)

#endif // Def_Camera_Cm_h__
