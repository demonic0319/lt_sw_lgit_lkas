//*****************************************************************************
// Filename	: 	Wnd_Cfg_WipID.h
// Created	:	2017/11/16 - 16:29
// Modified	:	2017/11/16 - 16:29
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// CWnd_Cfg_WipID
#ifndef Wnd_Cfg_WipID_h__
#define Wnd_Cfg_WipID_h__

#pragma once

#include "VGStatic.h"
#include "Def_DataStruct_Cm.h"
#include "Def_T_Foc.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_WipID
//-----------------------------------------------------------------------------
class CWnd_Cfg_WipID : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_WipID)

public:
	CWnd_Cfg_WipID();
	virtual ~CWnd_Cfg_WipID();

protected:
	afx_msg int			OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void		OnSize					(UINT nType, int cx, int cy);
	afx_msg void		OnShowWindow			(BOOL bShow, UINT nStatus);
	virtual BOOL		PreCreateWindow			(CREATESTRUCT& cs);
	afx_msg void		OnBnClickedChkSpecMin	(UINT nID);
	afx_msg void		OnBnClickedChkSpecMax	(UINT nID);

	DECLARE_MESSAGE_MAP()

	CFont				m_font;

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Item[Spec_Foc_MaxNum];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_Foc_MaxNum];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_Foc_MaxNum];
	CMFCButton			m_chk_SpecMin[Spec_Foc_MaxNum];
	CMFCButton			m_chk_SpecMax[Spec_Foc_MaxNum];

public:

	void			Set_TestItemInfo		(__in ST_TestItemInfo* pstTestItemInfo);
	void			Get_TestItemInfo		(__out ST_TestItemInfo& stOutTestItemInfo);

	void			Set_WipID_Spec			(__in ST_Foc_Spec* pstWipID_SpecMin, __in ST_Foc_Spec* pstWipID_SpecMax);
	void			Get_WipID_Spec			(__out ST_Foc_Spec& stOut2DCal_SpecMin, __out ST_Foc_Spec& stOut2DCal_SpecMax);

};
#endif // Wnd_Cfg_WipID_h__