//*****************************************************************************
// Filename	: 	Def_T_IR.h
// Created	:	2017/11/13 - 11:51
// Modified	:	2017/11/13 - 11:51
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_T_IR_h__
#define Def_T_IR_h__

#include <afxwin.h>

#include "Def_UI_Current.h"
#include "Def_UI_OpticalCenter.h"
#include "Def_UI_SFR.h"
#include "Def_UI_DynamicBW.h"
#include "Def_UI_Shading.h"
#include "Def_UI_Ymean.h"
#include "Def_UI_LCB.h"
#include "Def_UI_BlackSpot.h"
#include "Def_UI_Rllumination.h"
#include "Def_UI_Rotation.h"
#include "Def_UI_Chart.h"
#include "Def_UI_Displace.h"
#include "Def_UI_Vision.h"
#include "Def_UI_IIC.h"
#include "Def_UI_Defect_White.h"
#include "Def_UI_Defect_Black.h"

#pragma pack(push,1)

typedef struct _tag_Result_SFR
{
	CRect		rtROI[ROI_SFR_Max];		// 보정된 ROI

	BOOL		bResult[ROI_SFR_Max];	// 개별 결과
	double		dbValue[ROI_SFR_Max];	// 개별 결과 값

	CPoint	cCenterPoint;

	_tag_Result_SFR()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			rtROI[nROI].SetRectEmpty();

			bResult[nROI]	= TRUE;
			dbValue[nROI]	= 0.0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			if (FALSE == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_SFR& operator= (_tag_Result_SFR& ref)
	{
		for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
		{
			rtROI[nROI]		= ref.rtROI[nROI];		

			bResult[nROI]	= ref.bResult[nROI];
			dbValue[nROI]	= ref.dbValue[nROI];
		}
		cCenterPoint = ref.cCenterPoint;

		return *this;
	};

}ST_Result_SFR, *PST_Result_SFR;



typedef struct _tag_Result_Rotation
{
	CRect		rtROI[ROI_Rotation_Max];		// 보정된 ROI

	double		dbRotation;
	BOOL		bRotation;

	_tag_Result_Rotation()
	{
		Reset();
	};

	void Reset()
	{
		dbRotation = 0.0;

		bRotation = TRUE;

		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			rtROI[nROI].left = 0;
			rtROI[nROI].right = 0;
			rtROI[nROI].top = 0;
			rtROI[nROI].bottom = 0;
		}
	};

	_tag_Result_Rotation& operator= (_tag_Result_Rotation& ref)
	{
		dbRotation = ref.dbRotation;

		bRotation = ref.bRotation;


		for (UINT nROI = 0; nROI < ROI_Rotation_Max; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_Rotation, *PST_Result_Rotation;

typedef struct _tag_Result_DynamicBW
{
	CPoint		ptROI[ROI_DnyBW_Max];		// 보정된 ROI

	double		dbDynamic;
	double		dbSNR_BW;

	BOOL		bDynamic;
	BOOL		bSNR_BW;

	double dbVarianceValue[ROI_DnyBW_Max]; //분산
	double dbAverageValue[ROI_DnyBW_Max];  //평균

	_tag_Result_DynamicBW()
	{
		Reset();
	};

	void Reset()
	{
		dbDynamic	= 0.0;
		dbSNR_BW	= 0.0;

		bDynamic	= TRUE;
		bSNR_BW		= TRUE;

		for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;

			dbVarianceValue[nROI] = 0.0;
			dbAverageValue[nROI] = 0.0;
		}
	};

	_tag_Result_DynamicBW& operator= (_tag_Result_DynamicBW& ref)
	{
		dbDynamic	= ref.dbDynamic;
		dbSNR_BW	= ref.dbSNR_BW;
		
		bDynamic	= ref.bDynamic;
		bSNR_BW		= ref.bSNR_BW;

		for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
	
			dbVarianceValue[nROI] = ref.dbVarianceValue[nROI];
			dbAverageValue[nROI] = ref.dbAverageValue[nROI];
		}

		return *this;
	};

}ST_Result_DynamicBW, *PST_Result_DynamicBW;

typedef struct _tag_Result_Ymean
{
	bool bYmeanResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[YMEAN_COUNT_MAX];		//찾은 이물의 위치
	
	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_Ymean()
	{
		Reset();
	};

	void Reset()
	{
		bYmeanResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < YMEAN_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_Ymean& operator= (_tag_Result_Ymean& ref)
	{
		bYmeanResult = ref.bYmeanResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < YMEAN_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_Ymean, *PST_Result_Ymean;


typedef struct _tag_Result_LCB
{
	bool bLCBResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[LCB_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_LCB()
	{
		Reset();
	};

	void Reset()
	{
		bLCBResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < LCB_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_LCB& operator= (_tag_Result_LCB& ref)
	{
		bLCBResult = ref.bLCBResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < LCB_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_LCB, *PST_Result_LCB;


typedef struct _tag_Result_BlackSpot
{
	bool bBlackSpotResult; // 판정 결과

	int nDefectCount; //
	CRect	rtROI[BlackSpot_COUNT_MAX];		//찾은 이물의 위치

	int nCenterCount;
	CPoint pCenterMaxPoint;
	int nEdgeCount;
	CPoint pEdgeMaxPoint;
	int nCornerCount;
	CPoint pCornerMaxPoint;

	int nCircleCount;
	CPoint pCircleMaxPoint;
	int ocx;
	int ocy;
	int radx;
	int rady;

	_tag_Result_BlackSpot()
	{
		Reset();
	};

	void Reset()
	{
		bBlackSpotResult = true;

		nDefectCount = 0;
		nCenterCount = 0;
		nEdgeCount = 0;
		nCornerCount = 0;
		nCircleCount = 0;

		ocx = 0;
		ocy = 0;
		radx = 0;
		rady = 0;

		pCenterMaxPoint.SetPoint(0, 0);
		pEdgeMaxPoint.SetPoint(0, 0);
		pCornerMaxPoint.SetPoint(0, 0);
		pCircleMaxPoint.SetPoint(0, 0);

		for (UINT nROI = 0; nROI < BlackSpot_COUNT_MAX; nROI++)
		{
			rtROI[nROI].SetRect(0, 0, 0, 0);
		}
	};

	_tag_Result_BlackSpot& operator= (_tag_Result_BlackSpot& ref)
	{
		bBlackSpotResult = ref.bBlackSpotResult;

		nDefectCount = ref.nDefectCount;
		nCenterCount = ref.nCenterCount;
		nEdgeCount = ref.nEdgeCount;
		nCornerCount = ref.nCornerCount;
		nCircleCount = ref.nCircleCount;

		ocx = ref.ocx;
		ocy = ref.ocy;
		radx = ref.radx;
		rady = ref.rady;

		for (UINT nROI = 0; nROI < BlackSpot_COUNT_MAX; nROI++)
		{
			rtROI[nROI] = ref.rtROI[nROI];
		}

		return *this;
	};

}ST_Result_BlackSpot, *PST_Result_BlackSpot;


typedef struct _tag_Result_Defect_Black
{
	bool bDefect_BlackResult; // 판정 결과

	int nDefect_BlackCount; //
	CPoint	ptROI[Defect_Black_COUNT_MAX];		//찾은 이물의 위치

	_tag_Result_Defect_Black()
	{
		Reset();
	};

	void Reset()
	{
		bDefect_BlackResult = true;
		nDefect_BlackCount = 0; //


		for (UINT nROI = 0; nROI < Defect_Black_COUNT_MAX; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;
		}
	};

	_tag_Result_Defect_Black& operator= (_tag_Result_Defect_Black& ref)
	{
		bDefect_BlackResult = ref.bDefect_BlackResult;
		nDefect_BlackCount = ref.nDefect_BlackCount;


		for (UINT nROI = 0; nROI < Defect_Black_COUNT_MAX; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_Defect_Black, *PST_Result_Defect_Black;


typedef struct _tag_Result_Defect_White
{
	bool bDefect_WhiteResult; // 판정 결과

	int nDefect_WhiteCount; //
	CPoint	ptROI[Defect_White_COUNT_MAX];		//찾은 이물의 위치

	_tag_Result_Defect_White()
	{
		Reset();
	};

	void Reset()
	{
		bDefect_WhiteResult = true;
		nDefect_WhiteCount = 0; //


		for (UINT nROI = 0; nROI < Defect_White_COUNT_MAX; nROI++)
		{
			ptROI[nROI].x = 0;
			ptROI[nROI].y = 0;
		}
	};

	_tag_Result_Defect_White& operator= (_tag_Result_Defect_White& ref)
	{
		bDefect_WhiteResult = ref.bDefect_WhiteResult;
		nDefect_WhiteCount = ref.nDefect_WhiteCount;


		for (UINT nROI = 0; nROI < Defect_White_COUNT_MAX; nROI++)
		{
			ptROI[nROI] = ref.ptROI[nROI];
		}

		return *this;
	};

}ST_Result_Defect_White, *PST_Result_Defect_White;

typedef struct _tag_Result_Shading
{
	double		dValue[enUI_ShadingField_Max];
	BOOL		bResult[enUI_ShadingField_Max];

	_tag_Result_Shading()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nROI = 0; nROI < enUI_ShadingField_Max; nROI++)
		{
			dValue[nROI] = 0;
			bResult[nROI] = 0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < enUI_ShadingField_Max; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Shading& operator= (_tag_Result_Shading& ref)
	{
		for (UINT nROI = 0; nROI < enUI_ShadingField_Max; nROI++)
		{
			dValue[nROI] = ref.dValue[nROI];
			bResult[nROI] = ref.bResult[nROI];
		}

		return *this;
	};

}ST_Result_Shading, *PST_Result_Shading;

typedef struct _tag_Result_Rllumination
{
	CRect		ptRIROI[ROI_Rllumination_Max];			// 보정된 ROI

	double		dRICorner;	// 중심 값
	double		dRIMin;		// 최소 밝기 값
	double		dRIValue[ROI_Rllumination_Max];		// 개별 결과 값

	bool		dRIResult[Spec_RI_MAX];

	_tag_Result_Rllumination()
	{
		Reset();
	};

	void Reset()
	{
		dRICorner = 0.0;
		dRIMin = 0.0;


		for (UINT nidx = 0; nidx < Spec_RI_MAX; nidx++)
		{
			dRIResult[nidx] = false;
		}

		for (UINT nROI = 0; nROI < ROI_Rllumination_Max; nROI++)
		{
			ptRIROI[nROI].left = 0;
			ptRIROI[nROI].right = 0;

			ptRIROI[nROI].top = 0;
			ptRIROI[nROI].bottom = 0;

			//dRIResult[nROI] = true;
			dRIValue[nROI] = 0.0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < Spec_RI_MAX; nROI++)
		{
			if (false == dRIResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Rllumination operator= (_tag_Result_Rllumination& ref)
	{
		for (UINT nidx = 0; nidx < Spec_RI_MAX; nidx++)
		{
			dRIResult[nidx] = ref.dRIResult[nidx];
		}

		for (UINT nROI = 0; nROI < ROI_Rllumination_Max; nROI++)
		{
			ptRIROI[nROI] = ref.ptRIROI[nROI];
			//dRIResult[nROI] = ref.dRIResult[nROI];
			dRIValue[nROI] = ref.dRIValue[nROI];
		}

		return *this;
	};

}ST_Result_Rllumination, *PST_Result_Rllumination;


typedef struct _tag_Result_Current
{
	BOOL		bResult[5];	// 개별 결과
	double		dbValue[5];	// 개별 결과 값

	_tag_Result_Current()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nCH = 0; nCH < 5; nCH++)
		{
			bResult[nCH] = TRUE;
			dbValue[nCH] = 0.0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < 5; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Current& operator= (_tag_Result_Current& ref)
	{
		for (UINT nROI = 0; nROI < 5; nROI++)
		{
			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
		}

		return *this;
	};

}ST_Result_Current, *PST_Result_Current;


typedef struct _tag_Result_OpticalCenter
{
	BOOL		bResult[OpticalCenter_Max];	// 개별 결과
	double		dValue[OpticalCenter_Max];	// 개별 결과 값

	_tag_Result_OpticalCenter()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nCH = 0; nCH < OpticalCenter_Max; nCH++)
		{
			bResult[nCH] = TRUE;
			dValue[nCH] = 0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < OpticalCenter_Max; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_OpticalCenter& operator= (_tag_Result_OpticalCenter& ref)
	{
		for (UINT nROI = 0; nROI < OpticalCenter_Max; nROI++)
		{
			bResult[nROI] = ref.bResult[nROI];
			dValue[nROI] = ref.dValue[nROI];
		}

		return *this;
	};

}ST_Result_OpticalCenter, *PST_Result_OpticalCenter;


typedef struct _tag_Result_Displace
{
	BOOL		bResult[10];	// 개별 결과
	double		dbValue[10];	// 개별 결과 값

	_tag_Result_Displace()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nCH = 0; nCH < 10; nCH++)
		{
			bResult[nCH] = TRUE;
			dbValue[nCH] = 0.0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < 10; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Displace& operator= (_tag_Result_Displace& ref)
	{
		for (UINT nROI = 0; nROI < 10; nROI++)
		{
			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
		}

		return *this;
	};

}ST_Result_Displace, *PST_Result_Displace;


typedef struct _tag_Result_Vision
{
	BOOL		bResult[1];	// 개별 결과
	double		dbValue[1];	// 개별 결과 값
	int			iCenterX[1];	// 개별 결과 값
	int			iCenterY[1];	// 개별 결과 값

	_tag_Result_Vision()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nCH = 0; nCH < 1; nCH++)
		{
			bResult[nCH] = TRUE;
			dbValue[nCH] = 0.0;
			iCenterX[nCH] = 0;
			iCenterY[nCH] = 0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < 1; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_Vision& operator= (_tag_Result_Vision& ref)
	{
		for (UINT nROI = 0; nROI < 1; nROI++)
		{
			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
			iCenterX[nROI] = ref.iCenterX[nROI];
			iCenterY[nROI] = ref.iCenterY[nROI];
		}

		return *this;
	};

}ST_Result_Vision, *PST_Result_Vision;


typedef struct _tag_Result_IIC
{
	BOOL		bResult[1];	// 개별 결과
	double		dbValue[1];	// 개별 결과 값

	_tag_Result_IIC()
	{
		Reset();
	};

	void Reset()
	{
		for (UINT nCH = 0; nCH < 1; nCH++)
		{
			bResult[nCH] = TRUE;
			dbValue[nCH] = 0.0;
		}
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		for (UINT nROI = 0; nROI < 1; nROI++)
		{
			if (false == bResult[nROI])
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	_tag_Result_IIC& operator= (_tag_Result_IIC& ref)
	{
		for (UINT nROI = 0; nROI < 1; nROI++)
		{
			bResult[nROI] = ref.bResult[nROI];
			dbValue[nROI] = ref.dbValue[nROI];
		}

		return *this;
	};

}ST_Result_IIC, *PST_Result_IIC;


typedef struct _tag_Result_Chart
{
	BOOL	bResult;
	BOOL	bDetect;
	CRect	rtROI;

	_tag_Result_Chart()
	{
		Reset();
	};

	void Reset()
	{
		bResult = FALSE;
		bDetect = FALSE;

		rtROI.SetRectEmpty();
	};

	// 최종 결과 가져오기
	BOOL GetFinalResult()
	{
		return 	bResult;
	}

	_tag_Result_Chart& operator= (_tag_Result_Chart& ref)
	{
		bResult = ref.bResult;
		bDetect = ref.bDetect;
		
		rtROI	= ref.rtROI;

		return *this;
	};

}ST_Result_Chart, *PST_Result_Chart;

// 옵션
typedef struct _tag_IR_Opt
{
	ST_UI_SFR				stSFR;
	ST_UI_DynamicBW			stDynamicBW;
	ST_UI_Shading			stShading;
	ST_UI_Ymean				stYmean;
	ST_UI_LCB				stLCB;
	ST_UI_BlackSpot			stBlackSpot;
	ST_UI_Current			stCurrent;
	ST_UI_Rllumination		stRllumination;
	ST_UI_Chart				stChart;
	ST_UI_Displace			stDisplace;
	ST_UI_Vision			stVision;
	ST_UI_IIC				stIIC;
	ST_UI_Rotation			stDistortion;
	ST_UI_Defect_Black		stDefect_Black;
	ST_UI_Defect_White		stDefect_White;
	ST_UI_OpticalCenter		stOpticalCenter;

	_tag_IR_Opt()
	{
		Reset();
	};

	_tag_IR_Opt& operator= (const _tag_IR_Opt& ref)
	{
		return *this;
	};

	void Reset()
	{
		
	};

}ST_IR_Opt, *PST_IR_Opt;

// 결과
typedef struct _tag_IR_Result
{
	CString szFileNmae;
	
	ST_Result_SFR				stSFR;
	ST_Result_Current			stCurrent;
	ST_Result_DynamicBW			stDynamicBW;
	ST_Result_Shading			stShading;
	ST_Result_Ymean				stYmean;
	ST_Result_LCB				stLCB;
	ST_Result_BlackSpot			stBlackSpot;
	ST_Result_Rllumination		stRllumination;
	ST_Result_Chart				stChart;
	ST_Result_Displace			stDisplace;
	ST_Result_Vision			stVision;
	ST_Result_IIC				stIIC;
    ST_Result_Rotation			stDistortion;
	ST_Result_Defect_Black		stDefect_Black;
	ST_Result_Defect_White		stDefect_White;
	ST_Result_OpticalCenter		stOpticalCenter;

	_tag_IR_Result()
	{
		szFileNmae.Empty();

		Reset();
	};

	_tag_IR_Result& operator= (_tag_IR_Result& ref)
	{
		szFileNmae = ref.szFileNmae;

		return *this;
	};

	void Reset()
	{
		stSFR.Reset();
		stCurrent.Reset();
		stDynamicBW.Reset();
		stShading.Reset();
		stYmean.Reset();
		stLCB.Reset();
		stBlackSpot.Reset();
		stRllumination.Reset();
		stChart.Reset();
		stDisplace.Reset();
		stVision.Reset();
		stIIC.Reset();
	    stDistortion.Reset();
		stDefect_Black.Reset();
		stDefect_White.Reset();
		stOpticalCenter.Reset();
	};

}ST_IR_Result, *PST_IR_Result;

#pragma pack(pop)

#endif // Def_T_IR_h__
