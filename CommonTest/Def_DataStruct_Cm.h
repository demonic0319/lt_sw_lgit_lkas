﻿//*****************************************************************************
// Filename	: Def_DataStruct_Cm.h
// Created	: 2012/11/1
// Modified	: 2012/11/1 - 16:43
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_DataStruct_Cm_h__
#define Def_DataStruct_Cm_h__

#include <afxwin.h>
#include <Mmsystem.h>
#include "Def_Enum_Cm.h"
#include "Def_Test_Cm.h"
#include "Def_TestItem_Cm.h"
#include "Def_MES_Cm.h"

#pragma pack(push, 1)

#define		MAX_CONSUMABLES_ITEM_CNT	10	// 소모품 카운트 처리 품목 개수

//---------------------------------------------------------
// Image Buffer
//---------------------------------------------------------
#define MAX_IMAGE_HISTORY_BUFFER		15
typedef struct _tag_ImageBuffer
{
	LPWORD		lpwImage_16bit;		// 2Byte 1채널 데이터
	LPBYTE		lpbyImage_8bit;		// RGB 3채널 데이터
	LPBYTE		lpbyImage_8bit_Q;	// RGB 3채널 데이터

	DWORD		dwWidth;		// 영상 가로 크기
	DWORD		dwHeight;		// 영상 세로 크기
	DWORD		dwPixelCount;
	DWORD		dwSize;			// 버퍼 크기 (Byte)

	_tag_ImageBuffer()
	{
		lpwImage_16bit = nullptr;
		lpbyImage_8bit = nullptr;
		lpbyImage_8bit_Q = nullptr;

		dwWidth		= 0;
		dwHeight	= 0;
		dwPixelCount= 0;
		dwSize		= 0;
	};

	~_tag_ImageBuffer()
	{
		ReleaseMem();
	};

	void AssignMem(__in DWORD dwInSizeX, __in DWORD dwInSizeY)
	{
		dwWidth = dwInSizeX;
		dwHeight = dwInSizeY;
		dwPixelCount = dwWidth * dwHeight;

		if (dwSize != dwPixelCount)
		{
			ReleaseMem();

			dwSize = dwPixelCount;
		}

		if ((nullptr == lpwImage_16bit) && (0 != dwSize))
		{
			lpwImage_16bit = new WORD[dwSize];
		}

		if ((nullptr == lpbyImage_8bit) && (0 != dwSize))
		{
			lpbyImage_8bit = new BYTE[dwSize * 3]; // RGB 3채널
		}

		if ((nullptr == lpbyImage_8bit_Q) && (0 != dwSize))
		{
			lpbyImage_8bit_Q = new BYTE[dwSize * 3]; // RGB 3채널
		}
	};

	void ReleaseMem()
	{
		if (nullptr != lpwImage_16bit)
		{
			delete[](lpwImage_16bit);

			lpwImage_16bit = nullptr;
		}

		if (nullptr != lpbyImage_8bit)
		{
			delete[](lpbyImage_8bit);

			lpbyImage_8bit = nullptr;
		}

		if (nullptr != lpbyImage_8bit_Q)
		{
			delete[](lpbyImage_8bit_Q);

			lpbyImage_8bit_Q = nullptr;
		}
	};

	void ResetData()
	{
		if (nullptr != lpwImage_16bit)
		{
			memset(lpwImage_16bit, 0, sizeof(WORD) * dwSize);
		}

		if (nullptr != lpbyImage_8bit)
		{
			memset(lpbyImage_8bit, 0, dwSize * 3);
		}

		if (nullptr != lpbyImage_8bit_Q)
		{
			memset(lpbyImage_8bit_Q, 0, dwSize * 3);
		}
	};

}ST_ImageBuffer, *PST_ImageBuffer;

typedef struct _tag_RawBuffer
{
	LPBYTE		lpbyImage_Raw;	//

	DWORD		dwSize;			// 버퍼 크기 (Byte)

	_tag_RawBuffer()
	{
		lpbyImage_Raw	= nullptr;

		dwSize			= 0;
	};

	~_tag_RawBuffer()
	{
		ReleaseMem();
	};

	void AssignMem(__in DWORD dwInSize)
	{
		if (dwSize != dwInSize)
		{
			ReleaseMem();

			dwSize = dwInSize;
		}

		if ((nullptr == lpbyImage_Raw) && (0 != dwSize))
		{
			lpbyImage_Raw = new BYTE[dwSize];
		}
	};

	void ReleaseMem()
	{
		if (nullptr != lpbyImage_Raw)
		{
			delete[](lpbyImage_Raw);

			lpbyImage_Raw = nullptr;
		}
	};

	void ResetData()
	{
		if (nullptr != lpbyImage_Raw)
		{
			memset(lpbyImage_Raw, 0, dwSize);
		}
	};

}ST_RawBuffer, *PST_RawBuffer;

//---------------------------------------------------------
// 자동 테스트 구동용 구조체
//---------------------------------------------------------
typedef struct _tag_SelfRun
{
	UINT	nModuleCnt;		// 현재 제품 카운트
	UINT	nCycleCnt;		// 현재 주기 카운트
	DWORD	dwRunTime;		// 현재 진행 시간

	UINT	nModuleCnt_Set;	// 설정: 제품 개수
	UINT	nCycleCnt_Set;	// 설정: 주기 회수
	DWORD	dwRunTime_Set;	// 설정: 진행 시간

	BOOL	bAutoBarcode;	// 자동으로 바코드 입력
	BOOL	bOnlyHandler;	// or With Tester;
	BOOL	bTypeCycleCnt;	// or Time

	_tag_SelfRun()
	{
		nModuleCnt		= 0;
		nCycleCnt		= 0;
		dwRunTime		= 0;

		nModuleCnt_Set	= 0;
		nCycleCnt_Set	= 0;
		dwRunTime_Set	= 0;

		bAutoBarcode	= FALSE;
		bOnlyHandler	= TRUE;
		bTypeCycleCnt	= TRUE;
	};

	void Reset()
	{
		nModuleCnt		= 0;
		nCycleCnt		= 0;
		dwRunTime		= 0;
	};

}ST_SelfRun;

//---------------------------------------------------------
// Elapsed Time 설정용 구조체
//---------------------------------------------------------
typedef struct _tag_ElapTime
{
	DWORD	dwMinTime;			// 최단 Elapsed Time
	DWORD	dwMaxTime;			// 최장 Elapsed Time
	DOUBLE	dAverageTime;		// 장비 Elapsed Time
	DWORD64	dwCumulativeTime;	// 장비 누적 Elapsed Time
	DWORD	dwCount;			// 측정 갯수

	_tag_ElapTime()
	{
		Reset();
	};

	_tag_ElapTime& operator= (_tag_ElapTime& ref)
	{
		dwMinTime			= ref.dwMinTime;
		dwMaxTime			= ref.dwMaxTime;
		dAverageTime		= ref.dAverageTime;
		dwCumulativeTime	= ref.dwCumulativeTime;
		dwCount				= ref.dwCount;
	};

	void Reset()
	{
		dwMinTime			= 0xFFFFFFFF;
		dwMaxTime			= 0;
		dAverageTime		= 0.0f;
		dwCumulativeTime	= 0;
		dwCount				= 0;
	};

	void AddElapTime(__in DWORD dwInputElapTime)
	{
		// 카운트 증가
		++dwCount;

		// 누적 타임 
		dwCumulativeTime += dwInputElapTime;

		// 평균
		if (0 != dwCount)
			dAverageTime = (DOUBLE)dwCumulativeTime / (DOUBLE)dwCount;
		else
			dAverageTime = 0.0f;

		// Min
		dwMinTime = __min(dwInputElapTime, dwMinTime);

		// Max
		dwMaxTime = __max(dwInputElapTime, dwMaxTime);
	};


	void LoadPreviousElapTime(__in DWORD dwPrevCumulativeTime, __in DWORD dwPrevOutputCnt)
	{
		dwCumulativeTime	= dwPrevCumulativeTime;
		dwCount				= dwPrevOutputCnt;

		if (0 != dwPrevOutputCnt)
		{
			dAverageTime = (DOUBLE)dwCumulativeTime / (DOUBLE)dwCount;
		}
		else
		{
			dwCumulativeTime = 0;
			dAverageTime = 0.0f;
		}
	};

}ST_ElapTime, *PST_ElapTime;

//---------------------------------------------------------
// 검사 진행 시간 구조체
//---------------------------------------------------------
typedef struct _tag_CycleTime
{
	ST_ElapTime		CycleTime;						// 검사 시작 -> 검사 종료까지 시간
	ST_ElapTime		TactTime;						// 이전 제품 배출 -> 현재 제품 배출까지 시간

	ST_ElapTime		ItemCycleTime[MAX_TESTITEM];	// 검사 항목 별 검사 시작 -> 검사 종료까지 시간
	ST_ElapTime		ParaTestTime[MAX_SITE_CNT];		// 검사 시작 -> 검사 종료까지 시간

	UINT			nTestItemCnt;					// 검사 항목 개수
	UINT			nSocketCnt;						// 검사 소켓(파라) 개수

	_tag_CycleTime()
	{
		nTestItemCnt	= MAX_TESTITEM;
		nSocketCnt		= MAX_SITE_CNT;
	};

	_tag_CycleTime& operator= (_tag_CycleTime& ref)
	{
		CycleTime	= ref.CycleTime;
		TactTime	= ref.TactTime;

		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			ItemCycleTime[nIdx] = ref.ItemCycleTime[nIdx];
		}

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			ParaTestTime[nIdx] = ref.ParaTestTime[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		CycleTime.Reset();
		TactTime.Reset();

		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			ItemCycleTime[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			ParaTestTime[nIdx].Reset();
		}
	};

	void AddCycleTime(__in DWORD dwCycleTime)
	{
		CycleTime.AddElapTime(dwCycleTime);
	};

	void AddTestTime(__in DWORD dwTestTime, __in UINT nParaIdx = 0)
	{
		if (nParaIdx < MAX_SITE_CNT)
		{
			ParaTestTime[nParaIdx].AddElapTime(dwTestTime);
		}
	};

	void AddItemCycleTime(__in DWORD dwCycleTime, __in UINT nTestItemIndex)
	{
		if (nTestItemIndex < MAX_TESTITEM)
		{
			ItemCycleTime[nTestItemIndex].AddElapTime(dwCycleTime);
		}
	};

	void AddTactTime(__in DWORD dwTactTime)
	{
		TactTime.AddElapTime(dwTactTime);
	};
	
}ST_CycleTime, *PST_CycleTime;

//---------------------------------------------------------
// 검사 진행 시간 체크용 구조체
//---------------------------------------------------------
typedef struct _tag_TestTime_Unit
{
	DWORD	dwStart;		// 시작 시간
	DWORD	dwEnd;			// 종료 시간
	DWORD	dwDuration;		// 진행 시간

	_tag_TestTime_Unit()
	{
		Reset();
	};

	_tag_TestTime_Unit& operator= (_tag_TestTime_Unit& ref)
	{
		dwStart		= ref.dwStart;
		dwEnd		= ref.dwEnd;
		dwDuration	= ref.dwDuration;

		return *this;
	};

	void Reset()
	{
		dwStart		= 0;
		dwEnd		= 0;
		dwDuration	= 0;
	};

	void Set_Start()
	{
		dwStart = timeGetTime();
	};

	// 종료 시간 설정
	void Set_End()
	{
		dwEnd = timeGetTime();

		if (dwEnd < dwStart)
		{
			dwDuration = 0xFFFFFFFF - dwStart + dwEnd;
		}
		else
		{
			dwDuration = dwEnd - dwStart;
		}
	};

	// 시작부터 현재 체크하는 시간까지 걸린 시간 계산
	DWORD Get_Duration(DWORD dwTimeCheck)
	{
		if (dwTimeCheck < dwStart)
		{
			dwDuration = 0xFFFFFFFF - dwStart + dwTimeCheck;
		}
		else
		{
			dwDuration = dwTimeCheck - dwStart;
		}

		return dwDuration;
	};
}ST_TestTime_Unit, *PST_TestTime_Unit;

typedef struct _tag_TestTime
{
	SYSTEMTIME			tmStart_Test;			// 전체 검사 시작 시각
	SYSTEMTIME			tmEnd_Test;				// 전체 검사 종료 시각
	SYSTEMTIME			tmStart_Loading;		// 팔레트 로딩 시작 시각
	SYSTEMTIME			tmEnd_Unloading;		// 팔레트 로딩 종료 시각

	ST_TestTime_Unit	Test;					// 검사 구간 진행 시간 (총 진행 시간에서 로딩, 언로딩 시간 제외)
	ST_TestTime_Unit	Cycle;					// 검사 총진행 시간 (제품 투입부터 배출까지)
	ST_TestTime_Unit	TestItem[MAX_TESTITEM];	// 검사 항목 별 검사 시간 측정 용도 변수

	_tag_TestTime()
	{
		Reset();
	};

	_tag_TestTime& operator= (_tag_TestTime& ref)
	{
		memcpy(&tmStart_Test,		&ref.tmStart_Test,		sizeof(SYSTEMTIME));
		memcpy(&tmEnd_Test,			&ref.tmEnd_Test,		sizeof(SYSTEMTIME));
		memcpy(&tmStart_Loading,	&ref.tmStart_Loading,	sizeof(SYSTEMTIME));
		memcpy(&tmEnd_Unloading,	&ref.tmEnd_Unloading,	sizeof(SYSTEMTIME));

		Test	= ref.Test;
		Cycle	= ref.Cycle;

		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			TestItem[nIdx]	= ref.TestItem[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		Test.Reset();
		Cycle.Reset();
		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			TestItem[nIdx].Reset();
		}

		ZeroMemory(&tmStart_Test,		sizeof(SYSTEMTIME));
		ZeroMemory(&tmEnd_Test,			sizeof(SYSTEMTIME));
		ZeroMemory(&tmStart_Loading,	sizeof(SYSTEMTIME));
		ZeroMemory(&tmEnd_Unloading,	sizeof(SYSTEMTIME));
	};

	// 검사 시간 초기화
	void Reset_Test()
	{
		Test.Reset();
		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			TestItem[nIdx].Reset();
		}

		ZeroMemory(&tmStart_Test,	sizeof(SYSTEMTIME));
		ZeroMemory(&tmEnd_Test,		sizeof(SYSTEMTIME));
	};

	// 검사 항목 검사 시간 초기화
	void Reset_TestItem()
	{
		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			TestItem[nIdx].Reset();
		}
	};

	// 팔레트 로딩 시간 초기화
	void Reset_Loading()
	{
		Cycle.Reset();

		ZeroMemory(&tmStart_Loading,	sizeof(SYSTEMTIME));
		ZeroMemory(&tmEnd_Unloading,	sizeof(SYSTEMTIME));
	};

	// 팔레트 로딩 시작 시간 설정
	void Set_StartLoading()
	{
		GetLocalTime(&tmStart_Loading);
		
		Cycle.Set_Start();
	};

	// 팔레트 로딩 종료 시간 설정
	void Set_EndUnloading()
	{
		GetLocalTime(&tmEnd_Unloading);

		Cycle.Set_End();
	};

	// 시작부터 현재 체크하는 시간까지 걸린 시간 계산
	DWORD Get_Duration_Cycle(DWORD dwTimeCheck)
	{
		return Cycle.Get_Duration(dwTimeCheck);
	};

	// 전체 검사 시작 시간 설정
	void Set_StartTest()
	{
		GetLocalTime(&tmStart_Test);

		Test.Set_Start();
	};

	// 전체 검사 종료 시간 설정
	void Set_EndTest()
	{
		GetLocalTime(&tmEnd_Test);
		Test.Set_End();
	};

	// 시작부터 현재 체크하는 시간까지 걸린 시간 계산
	DWORD Get_Duration_Test(DWORD dwTimeCheck)
	{
		return Test.Get_Duration(dwTimeCheck);
	};

	// 개별 검사 항목별 검사 시간 설정
	void Set_StartTestItem(__in UINT nItemIndex)
	{
		if (nItemIndex < MAX_TESTITEM)
		{
			TestItem[nItemIndex].Set_Start();
		}
	};

	// 개별 검사 항목별 검사 종료 시간 설정
	void Set_EndTestItem(__in UINT nItemIndex)
	{
		if (nItemIndex < MAX_TESTITEM)
		{
			TestItem[nItemIndex].Set_End();
		}
	};

	// 검사 항목의 검사 진행 시간 얻기
	DWORD Get_Duration_TestItem(DWORD dwTimeCheck, __in UINT nItemIndex)
	{
		return TestItem[nItemIndex].Get_Duration(dwTimeCheck);
	};

}ST_TestTime, *PST_TestTime;

//---------------------------------------------------------
// 에러 코드 처리용 구조체
//---------------------------------------------------------
typedef struct _tag_ErrorInfo
{
	SYSTEMTIME	tmTime;		// 에러 발생 시간
	long		lCode;		// 에러 고유 코드
	UINT		nType;		// 에러 형태
	CString		szDesc;		// 에러 설명

	_tag_ErrorInfo()
	{
		Reset();
	};

	void Reset()
	{
		ZeroMemory(&tmTime, sizeof(SYSTEMTIME));
		lCode = 0;
		nType = 0;
		szDesc.Empty();
	};

	_tag_ErrorInfo& operator= (_tag_ErrorInfo& ref)
	{		
		memcpy(&tmTime, &ref.tmTime, sizeof(SYSTEMTIME));
		lCode	= ref.lCode;
		nType	= ref.nType;
		szDesc	= ref.szDesc;

		return *this;
	};
}ST_ErrorInfo, *PST_ErrorInfo;

//---------------------------------------------------------
// 수율 구조체
//---------------------------------------------------------
typedef struct _tag_Yield
{
	DWORD	dwTotal;	// 총계
	DWORD	dwPass;		// 양품 수
	DWORD	dwFail;		// 불량 수
	FLOAT	fYield;		// 수율
	DWORD	dwNoTest;	// 미검사 수
	DWORD	dwRework;	// 재검 수

	_tag_Yield()
	{
		Reset();
	};

	_tag_Yield& operator= (_tag_Yield& ref)
	{
		dwTotal		= ref.dwTotal;
		dwPass		= ref.dwPass;
		dwFail		= ref.dwFail;
		fYield		= ref.fYield;
		dwNoTest	= ref.dwNoTest;
		dwRework	= ref.dwRework;		

		return *this;
	};

	inline void	Reset()
	{
		dwTotal		= 0;
		dwPass		= 0;
		dwFail		= 0;
		fYield		= 0.0f;
		dwNoTest	= 0;
		dwRework	= 0;		
	};

	// 수율 계산
	inline void	ComputeYield()
	{
		dwTotal = dwPass + dwFail;

		if (0 == dwTotal)
			fYield = 0.0;
		else
			fYield = (FLOAT)dwPass / (FLOAT)dwTotal * 100.0f;
	};	

	// 양품 수 증가
	void	IncreasePass()
	{
		++dwPass;
		ComputeYield();
	};

	// 불량 수 증가
	void	IncreaseFail()
	{
		++dwFail;
		ComputeYield();
	};

	void	IncreaseFail(__in UINT nTestItemIdx)
	{
		if (nTestItemIdx < MAX_TESTITEM)
		{
			++dwFail;

			ComputeYield();
		}
	};

	void	IncreaseFail(__in UINT nTestItemIdx, __in UINT nSocketIdx)
	{
		if ((nTestItemIdx < MAX_TESTITEM) && (nSocketIdx < MAX_SITE_CNT))
		{
			++dwFail;

			ComputeYield();
		}
	};

	// 미검사 수 증가
	void	IncreaseNoTest()
	{
		++dwNoTest;
	};

	// 재검사 수 증가
	void	IncreaseRework()
	{
		++dwRework;
	};

}ST_Yield, *PST_Yield;

//---------------------------------------------------------
// 수율 정보 구조체
//---------------------------------------------------------
typedef struct _tag_YieldInfo
{
	ST_Yield	TestYield;					// 전체 검사 수율
	ST_Yield	TestItem[MAX_TESTITEM];		// 검사 항목 별 수율
	ST_Yield	TestSocket[MAX_SITE_CNT];	// 검사 소켓(파라) 별 수율

	UINT		nTestItemCnt;				// 검사 항목 개수
	UINT		nSocketCnt;					// 검사 소켓(파라) 개수

	_tag_YieldInfo()
	{
		Reset();
	};

	_tag_YieldInfo& operator= (_tag_YieldInfo& ref)
	{
		TestYield		= ref.TestYield;
		
		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			TestItem[nIdx]	= ref.TestItem[nIdx];
		}

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			TestSocket[nIdx] = ref.TestSocket[nIdx];
		}

		return *this;
	};

	inline void	Reset()
	{
		TestYield.Reset();

		for (UINT nIdx = 0; nIdx < MAX_TESTITEM; nIdx++)
		{
			TestItem[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			TestSocket[nIdx].Reset();
		}

		nTestItemCnt = MAX_TESTITEM;
		nSocketCnt	 = MAX_SITE_CNT;
	};

	// Pass 증가
	void	IncreasePass()
	{
		TestYield.IncreasePass();
	};

	void	IncreasePass(__in UINT nSocketIdx)
	{
		if (nSocketIdx < MAX_SITE_CNT)
		{
			TestYield.IncreasePass();
			TestSocket[nSocketIdx].IncreasePass();
		}
	};

	// Fail 증가
	void	IncreaseFail()
	{
		TestYield.IncreaseFail();
	};

	void	IncreaseFail(__in UINT nSocketIdx)
	{
		if (nSocketIdx < MAX_SITE_CNT)
		{
			TestYield.IncreaseFail();
			TestSocket[nSocketIdx].IncreaseFail();
		}
	};

	// 검사 항목별 Pass 증가
	void	IncreasePass_TestItem(__in UINT nItemIndex)
	{
		if (nItemIndex < MAX_TESTITEM)
		{
			TestItem[nItemIndex].IncreasePass();
		}
	};

	// 검사 항목별 Fail 증가
	void	IncreaseFail_TestItem(__in UINT nItemIndex)
	{
		if (nItemIndex < MAX_TESTITEM)
		{
			TestItem[nItemIndex].IncreaseFail();
		}
	};

	// 미검사 카운트 증가
	void	IncreaseNoTest()
	{
		TestYield.IncreaseNoTest();
	};

	void	IncreaseNoTest(__in UINT nSocketIdx)
	{
		if (nSocketIdx < MAX_SITE_CNT)
		{
			TestYield.IncreaseNoTest();
			TestSocket[nSocketIdx].IncreaseNoTest();
		}
	};

	// 재검사 카운트 증가
	void	IncreaseRework()
	{
		TestYield.IncreaseRework();
	};

	void	IncreaseRework(__in UINT nSocketIdx)
	{
		if (nSocketIdx < MAX_SITE_CNT)
		{
			TestYield.IncreaseRework();
			TestSocket[nSocketIdx].IncreaseRework();
		}
	};

}ST_YieldInfo, *PST_YieldInfo;

//---------------------------------------------------------
// 소모품 사용 카운트
//---------------------------------------------------------
typedef struct _tag_ConsumablesItem
{
	CString		szItemName;		// 소모품 항목 명칭
	DWORD		dwCount_Max;	// 소모품 사용 횟수 최대 한계치
	DWORD		dwCount;		// 소모품 사용 횟수

	_tag_ConsumablesItem()
	{
		dwCount_Max	= 0;
		dwCount		= 0;
	};

	_tag_ConsumablesItem& operator= (_tag_ConsumablesItem& ref)
	{
		szItemName	= ref.szItemName;
		dwCount_Max	= ref.dwCount_Max;
		dwCount		= ref.dwCount;

	return *this;
	};

}ST_ConsumablesItem, *PST_ConsumablesItem;

//---------------------------------------------------------
// 소모품 사용 정보 구조체
//---------------------------------------------------------
typedef struct _tag_ConsumablesInfo
{
	CString					szFilename;						// 소모품 정보가 저장되는 파일명
	UINT					ItemCount;						// 소모품 항목 갯수

	ST_ConsumablesItem		Item[MAX_CONSUMABLES_ITEM_CNT];	// 소모품 항목 별 사용 횟수

	_tag_ConsumablesInfo()
	{
		ItemCount = MAX_CONSUMABLES_ITEM_CNT;

		for (UINT nIdx = 0; nIdx < MAX_CONSUMABLES_ITEM_CNT; nIdx++)
		{
			Item[nIdx].dwCount_Max	= 50000;
			Item[nIdx].dwCount		= 0;
		}
	};

	_tag_ConsumablesInfo& operator= (_tag_ConsumablesInfo& ref)
	{
		szFilename	= ref.szFilename;
		ItemCount	= ref.ItemCount;

		for (UINT nIdx = 0; nIdx < MAX_CONSUMABLES_ITEM_CNT; nIdx++)
		{
			Item[nIdx].dwCount_Max	= ref.Item[nIdx].dwCount_Max;
			Item[nIdx].dwCount		= ref.Item[nIdx].dwCount;
		}

		return *this;
	};

	void ResetCount(__in UINT nIdx)
	{
		if (nIdx < MAX_CONSUMABLES_ITEM_CNT)
		{
			Item[nIdx].dwCount = 0;
		}
	};

	BOOL IsMaxCount()
	{
		BOOL bReturn = FALSE;

		for (UINT nIdx = 0; nIdx < MAX_CONSUMABLES_ITEM_CNT; nIdx++)
		{
			bReturn |= Item[nIdx].dwCount_Max <= Item[nIdx].dwCount ? TRUE : FALSE;
		}

		return bReturn;
	};

	BOOL IsMaxCount(__in UINT nIdx)
	{
		if (nIdx < MAX_CONSUMABLES_ITEM_CNT)
		{
			BOOL bReturn = Item[nIdx].dwCount_Max <= Item[nIdx].dwCount ? TRUE : FALSE;

			return bReturn;
		}
		else
		{
			return FALSE;
		}
	};

	void Increase_Count(__in UINT nIdx)
	{
		if (nIdx < MAX_CONSUMABLES_ITEM_CNT)
		{
			++Item[nIdx].dwCount;
		}
	};

}ST_ConsumablesInfo, *PST_ConsumablesInfo;

//---------------------------------------------------------
// 사이트별 검사 진행 상태 구조체
//---------------------------------------------------------
typedef struct _tag_SiteInfo_Base
{
	BOOL				bConnection;	// 장치 연결 상태
	BOOL				bExistCam;		// 카메라 유무

	enTestResult		Result;			// 대기, 검사	
	enTestProcess		TestStatus;		// 검사 프로세스 진행 여부
	BYTE				byProcPercent;	// 진행율 ( 0 ~ 100%)

	_tag_SiteInfo_Base()
	{
		bConnection		= FALSE;
		bExistCam		= FALSE;

		Result			= TR_Empty;
		TestStatus		= TP_Ready;
		byProcPercent	= 0;
	};

	_tag_SiteInfo_Base& operator= (_tag_SiteInfo_Base& ref)
	{
		bConnection		= ref.bConnection;
		bExistCam		= ref.bExistCam;

		Result			= ref.Result;
		TestStatus		= ref.TestStatus;
		byProcPercent	= ref.byProcPercent;

		return *this;
	};

	void Reset()
	{
		Result			= TR_Empty;
		TestStatus		= TP_Ready;
		byProcPercent	= 0;
	};

	void SetResult(enTestResult ResultPara)
	{
		Result = ResultPara;
	};
}ST_SiteInfo_Base, *PST_SiteInfo_Base;

//---------------------------------------------------------
// 파워 서플라이 상태 구조체
//---------------------------------------------------------
typedef struct _tag_PowerSupply
{
	float		fVoltage;		// 설정 전압
	float		fCurrent;		// 설정 전류
	float		fMeasVoltage;	// 측정 전압
	float		fMeasCurrent;	// 측정 전류
	float		fVoltageStep;	// 스텝 전압
	float		fCurrentStep;	// 스텝 전류
	BOOL		bOut_On;		// Output On/Off
	BOOL		bKeyLock_On;	// KeyLock On/Off
	CString		Version;		// Version
	int			iErrorCode;		// Error Code
	CString		ErrorDesc;		// Error Desc
	BOOL		bCVStatus;		// CV/CC 상태

	_tag_PowerSupply()
	{
		fVoltage		= 0.0f;
		fCurrent		= 0.0f;
		fMeasVoltage	= 0.0f;
		fMeasCurrent	= 0.0f;
		fVoltageStep	= 0.0f;
		fCurrentStep	= 0.0f;
		bOut_On			= FALSE;
		bKeyLock_On		= FALSE;
		iErrorCode		= 0;
		bCVStatus		= FALSE;
	};

	_tag_PowerSupply& operator= (const _tag_PowerSupply& ref)
	{
		fVoltage		= ref.fVoltage;
		fCurrent		= ref.fCurrent;
		fMeasVoltage	= ref.fMeasVoltage;
		fMeasCurrent	= ref.fMeasCurrent;
		fVoltageStep	= ref.fVoltageStep;
		fCurrentStep	= ref.fCurrentStep;
		bOut_On			= ref.bOut_On;
		bKeyLock_On		= ref.bKeyLock_On;
		Version			= ref.Version;
		iErrorCode		= ref.iErrorCode;
		ErrorDesc		= ref.ErrorDesc;
		bCVStatus		= ref.bCVStatus;

		return *this;
	};

}ST_PowerSupply, *PST_PowerSupply;

//---------------------------------------------------------
// 레시피 설정값 구조체
//---------------------------------------------------------
typedef struct _tag_RecipeInfo_Base
{
	enInsptrSysType nInspectionType;	// 검사기 종류

	CString			szModelCode;		// 제품 Model 명칭
	enModelType		ModelType;			// 고정 모델
	CString			szRecipeFile;		// 레시피 설정 파일
	CString			sziicFile;			// iic 세팅 파일
	CString			szRecipeFullPath;	// 레시피 파일 Full Path
	CString			szConsumablesFile;	// 소모품 사용현황 설정 파일

	ST_StepInfo		StepInfo;			// 검사 스텝 정보
	ST_TestItemInfo	TestItemInfo;		// 검사 항목 정보 (결과 판정 스펙)

	UINT			nTestRetryCount;	// 전체 검사 불량 시 재시도 횟수
	UINT			nTestMode;	// 전체 검사 불량 시 재시도 횟수

	double			dExposure;			// 그래버에서 수신된 RGB영상 밝기 조절 수치
	float			fVoltage;			// Camera 전원

	float			fPixelSize;			
	float			fFocalLength;	

	double			dLEDRMSCycle;

	UINT			nCamImageType;
	UINT			nLightDelay;
	UINT			nChangeDelay;

	UINT			nViselLED;

	BOOL			bUseStereoCal_Flag;	// Sterero CAL 불량시 Flag값 변경하여 재측정하는 기능 사용 여부

	_tag_RecipeInfo_Base()
	{
		nInspectionType		= enInsptrSysType::Sys_Focusing;
		ModelType			= Model_OMS_Entry;
		dLEDRMSCycle		=0.0;
		nTestRetryCount		= 0;
		nTestMode = 0;
		dExposure			= 0.0f;
		fVoltage			= 12.0f;

		fPixelSize			= 1.74f;
		fFocalLength		= 5.6f;
		nCamImageType		= CAM_STATE_ORIGINAL;
		nLightDelay			= 1000;
		nViselLED			= 0x7F; //최댓값
		nChangeDelay		= 5000;
		bUseStereoCal_Flag	= FALSE;
	};

	_tag_RecipeInfo_Base& operator= (_tag_RecipeInfo_Base& ref)
	{
		nInspectionType		= ref.nInspectionType;
		szModelCode			= ref.szModelCode;
		ModelType			= ref.ModelType;
		szRecipeFile		= ref.szRecipeFile;
		sziicFile			= ref.sziicFile;
		szRecipeFullPath	= ref.szRecipeFullPath;
		szConsumablesFile	= ref.szConsumablesFile;
		StepInfo			= ref.StepInfo;
		TestItemInfo		= ref.TestItemInfo;

		nTestRetryCount = ref.nTestRetryCount;
		nTestMode = ref.nTestMode;

		dExposure			= ref.dExposure;
		fVoltage			= ref.fVoltage;

		fPixelSize			= ref.fPixelSize;
		fFocalLength		= ref.fFocalLength;
		nCamImageType		= ref.nCamImageType;
		nLightDelay			= ref.nLightDelay;
		nViselLED			= ref.nViselLED;
		nChangeDelay		= ref.nChangeDelay;
		bUseStereoCal_Flag	= ref.bUseStereoCal_Flag;
		dLEDRMSCycle		= ref.dLEDRMSCycle;

		return *this;
	};

	virtual void Reset()
	{
		szModelCode.Empty();
		szRecipeFile.Empty();
		sziicFile.Empty();
		szRecipeFullPath.Empty();
		szConsumablesFile.Empty();

		StepInfo.RemoveAll();
		TestItemInfo.RemoveAll();

		nTestRetryCount		= 0;
		dExposure			= 0.0f;
		fVoltage			= 12.0f;
		nLightDelay			= 1000;
		nViselLED			= 0x7F; // 최댓값
		nChangeDelay		= 5000;

		fPixelSize			= 1.74f;
		fFocalLength		= 5.6f;
		nCamImageType		= CAM_STATE_ORIGINAL;

		bUseStereoCal_Flag	= FALSE;
		dLEDRMSCycle		= 0.0;
	};

	// 검사기 종류 설정
	virtual void SetSystemType(__in enInsptrSysType nSysType)
	{
		nInspectionType = nSysType;
		TestItemInfo.SetSystemType(nSysType);
	};

}ST_RecipeInfo_Base, *PST_RecipeInfo_Base;

//---------------------------------------------------------
// 프로그램 경로 구조체
//---------------------------------------------------------
typedef struct _tag_ProgramPath_Base
{
	CString		szProgram;		// 프로그램 시작 경로
	CString		szLog;			// LOG 경로
	CString		szReport;		// 검사 결과 Report 경로
	CString		szRecipe;		// 모델 설정 파일 경로
	CString		szImage;		// 이미지 저장 경로
	CString		szConsumables;	// 소모품 설정 파일 저장 경로

}ST_ProgramPath_Base, *PST_ProgramPath_Base;

//---------------------------------------------------------
// 전체 검사에 관련된 데이터 기록용 구조체
//---------------------------------------------------------
typedef struct _tag_InspectionInfo_Base
{	
	ST_YieldInfo		YieldInfo;			// 수율
	ST_ConsumablesInfo	ConsumablesInfo;	// 소모품 (포고 핀) 카운트
	ST_CycleTime		CycleTime;			// 검사 진행 시간

	enPermissionMode	PermissionMode;		// 검사 모드 (작업자, 관리자, )
	enMES_Online		MESOnlineMode;		// MES Online/Offline
	enOperateMode		OperateMode;		// 설비 구동 모드
	
	CString				szLotName;			// LOT 명
	CString				szRecipeName;		// Model 명
	CString				szOperatorName;		// 작업자 명
	CString				szEquipmentID;		// 검사기 고유 ID (MES)

	enTestProcess		TestStatus;			// 검사 진행 상태
	enTestResult		Judgment_All;		// 전체 검사 결과
	
	BOOL				bForcedStop;		// 강제 정지 플래그
	BOOL				bEMO;				// 비상 정지 플래그

	BOOL				bTestEnable[MAX_SITE_CNT];	// 각 Para/Site 별 검사 사용 여부 

	DWORD				dwPrevOutputTime;	// 이전 제품 배출 시간
	DWORD				dwTactTime;			// 현재 Tact Time

	_tag_InspectionInfo_Base()
	{
		PermissionMode		= Permission_Operator;
		MESOnlineMode		= MES_Offline;
		OperateMode			= OpMode_Production;
		szRecipeName		= _T("Default");

		TestStatus			= TP_Ready;
		Judgment_All		= TR_Empty;

		bForcedStop			= FALSE;
		bEMO				= FALSE;
		
		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
		{
			bTestEnable[nIdx] = TRUE;
		}

		dwPrevOutputTime	= 0;
		dwTactTime			= 0;
	};

	void Initialize()
	{

	};
	
	// 검사 진행 상태 설정
	void SetTestStatus(enTestProcess nStatus)
	{
		TestStatus = nStatus;
	};

	// 검사 진행 상태 구하기
	enTestProcess GetTestStatus()
	{
		return TestStatus;
	};

	// LOT 정보 설정
	virtual void SetLotInfo(__in LPCTSTR szInLOTNum, __in LPCTSTR szInOperator)
	{
		szLotName		= szInLOTNum;
		szOperatorName	= szInOperator;
	};
	
	// LOT 명칭 설정
	virtual void SetLotName(__in LPCTSTR szInLOTNum)
	{
		szLotName = szInLOTNum;
	};

	// LOT 명칭 초기화
	virtual void ResetLotName()
	{
		szLotName.Empty();
	};

	// Tact 타임 설정
	DWORD SetTactTime(__in DWORD dwOutputTime)
	{
		if (0 != dwPrevOutputTime)
		{
			if (dwPrevOutputTime < dwOutputTime)
			{
				dwTactTime = dwOutputTime - dwPrevOutputTime;
			}
			else
			{
				dwTactTime = 0xFFFFFFFF - dwPrevOutputTime + dwOutputTime;
			}

			dwPrevOutputTime = dwOutputTime;
		}
		else
		{
			dwTactTime = 0;
			dwPrevOutputTime = dwOutputTime;
		}

		return dwTactTime;
	};

	// 검사 결과 판정 처리
	void Set_Judgment_All(__in enTestResult nIN_Judgment)
	{
		Judgment_All = nIN_Judgment;
	};

}ST_InspectionInfo_Base, *PST_InspectionInfo_Base;




#pragma pack (pop)

#endif // Def_DataStruct_Cm_h__
