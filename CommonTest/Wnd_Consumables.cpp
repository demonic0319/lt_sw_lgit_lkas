//*****************************************************************************
// Filename	: 	Wnd_Consumables.cpp
// Created	:	2017/9/18 - 9:49
// Modified	:	2017/9/18 - 9:49
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Consumables.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Consumables.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_Consumables

#define IDC_BN_RESET_FST	1000
#define IDC_BN_RESET_LST	IDC_BN_RESET_FST + CI_Y_MaxEnum - 1

static LPCTSTR g_szConsumItem[] =
{
	_T("Pogo Pin"),	// Pogo Count Left
	_T("Pogo Pin (R)"),	// Pogo Count Right
	NULL
};

static LPCTSTR g_szConsumHeader[] =
{
	_T("Name"),
	_T("Spec"),
	_T("Usage"),
	_T("Remain"),
	_T("Reset"),
	NULL
};

static int g_iCell_Width[] =
{
	200,	// CH_X_ItemName,
	150,	// CH_X_Spec,		
	150,	// CH_X_Usage,		
	150,	// CH_X_Remain,	
	120,	// CH_X_Reset,	
};

IMPLEMENT_DYNAMIC(CWnd_Consumables, CWnd)

CWnd_Consumables::CWnd_Consumables()
{
	m_iTotalWidthRate = 0;
	for (UINT nIdx = 0; nIdx < CH_X_MaxEnum; nIdx++)
	{
		m_iTotalWidthRate += g_iCell_Width[nIdx];
		m_iCtrlWidth[nIdx] = 0;
	}
}

CWnd_Consumables::~CWnd_Consumables()
{
}

BEGIN_MESSAGE_MAP(CWnd_Consumables, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_BN_RESET_FST, IDC_BN_RESET_LST, OnCmdRangeBnReset)
END_MESSAGE_MAP()

// CWnd_Consumables message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/18 - 9:50
// Desc.		:
//=============================================================================
int CWnd_Consumables::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nColIdx = 0; nColIdx < CH_X_MaxEnum; nColIdx++)
	{
		//m_st_Caption[nColIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Caption[nColIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Caption[nColIdx].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Caption[nColIdx].Create(g_szConsumHeader[nColIdx], WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
	}

	for (UINT nRowIdx = 0; nRowIdx < CI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < CH_X_MaxEnum; nColIdx++)
		{
			//m_st_Value[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
			m_st_Cell[nRowIdx][nColIdx].SetColorStyle(CVGStatic::ColorStyle_White);
			m_st_Cell[nRowIdx][nColIdx].SetFont_Gdip(L"Arial", 8.0F);
			m_st_Cell[nRowIdx][nColIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
		}

		m_bn_Reset[nRowIdx].Create(_T("Reset"), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | BS_PUSHLIKE, rectDummy, this, IDC_BN_RESET_FST + nRowIdx);
	}

	for (UINT nRowIdx = 0; nRowIdx < CI_Y_MaxEnum; nRowIdx++)
	{
		m_st_Cell[nRowIdx][CH_X_ItemName].SetText(g_szConsumItem[nRowIdx]);
	}

	// 아이템이 1개일 경우 Pogo 핀 구분을 없앰
	if (1 == m_nItemCount)
	{
		m_st_Cell[0][CH_X_ItemName].SetText(L"Pogo Pin");
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/18 - 9:50
// Desc.		:
//=============================================================================
void CWnd_Consumables::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx + (CH_X_MaxEnum - 1);
	int iHeight		= cy;
	int iRemind		= 0;

	// 너비
	for (UINT nIdx = 0; nIdx < CH_X_MaxEnum; nIdx++)
	{
		m_iCtrlWidth[nIdx] = (iWidth * g_iCell_Width[nIdx]) / m_iTotalWidthRate;
		iRemind += m_iCtrlWidth[nIdx];
	}
	iRemind = iWidth - iRemind;

	for (INT iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		m_iCtrlWidth[iIdx - 1] += 1;
	}

	// 높이
	int iCellHeight = iHeight + (m_nItemCount);
	int iCtrlHeight[CI_Y_MaxEnum + 1] = { 0, };
	iRemind = 0;

	for (UINT nIdx = 0; nIdx < (m_nItemCount + 1); nIdx++)
	{
		iCtrlHeight[nIdx] = iCellHeight / (m_nItemCount + 1);
		iRemind += iCtrlHeight[nIdx];
	}
	iRemind = iCellHeight - iRemind;

	for (int iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		iCtrlHeight[iIdx - 1] += 1;
	}

	// 컨트롤 이동
	for (UINT nColIdx = 0; nColIdx < CH_X_MaxEnum; nColIdx++)
	{
		m_st_Caption[nColIdx].MoveWindow(iLeft, iTop, m_iCtrlWidth[nColIdx], iCtrlHeight[m_nItemCount]);
		iLeft += (m_iCtrlWidth[nColIdx] - 1);
	}

	iLeft = 0;
	iTop += (iCtrlHeight[m_nItemCount] - 1);

	for (UINT nRowIdx = 0; nRowIdx < m_nItemCount; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < CH_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].MoveWindow(iLeft, iTop, m_iCtrlWidth[nColIdx], iCtrlHeight[nRowIdx]);

			if ((CH_X_Reset == nColIdx) && (m_bUseResetButton))
			{
				m_bn_Reset[nRowIdx].MoveWindow(iLeft + 1, iTop + 1, m_iCtrlWidth[CH_X_Reset] - 2, iCtrlHeight[nRowIdx] - 2);
			}

			iLeft += (m_iCtrlWidth[nColIdx] - 1);
		}
		
		iLeft = 0;
		iTop += (iCtrlHeight[nRowIdx] - 1);
	}

	// 안쓰는 컨트롤 감추기
	for (UINT nRowIdx = m_nItemCount; nRowIdx < CI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < CH_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].MoveWindow(0, 0, 0, 0);
		}

		m_bn_Reset[nRowIdx].MoveWindow(0, 0, 0, 0);
	}
}

//=============================================================================
// Method		: OnCmdRangeBnReset
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/3/4 - 17:50
// Desc.		:
//=============================================================================
void CWnd_Consumables::OnCmdRangeBnReset(UINT nID)
{
	UINT nIndex = nID - IDC_BN_RESET_FST;

	if (GetOwner())
	{
		GetOwner()->SendNotifyMessage(WM_CONSUMABLES_RESET, (WPARAM)nIndex, 0);
	}
}

//=============================================================================
// Method		: Set_UseResetButton
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2018/3/4 - 17:45
// Desc.		:
//=============================================================================
void CWnd_Consumables::Set_UseResetButton(__in BOOL bUse)
{
	m_bUseResetButton = bUse;
}

//=============================================================================
// Method		: ResetAllItemData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/18 - 17:08
// Desc.		:
//=============================================================================
void CWnd_Consumables::ResetAllItemData()
{
	for (UINT nRowIdx = 0; nRowIdx < CI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < CH_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].SetText(L"");
		}
	}
}

//=============================================================================
// Method		: SetItemData
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nItemIndex
// Parameter	: __in UINT nSpec
// Parameter	: __in UINT nUsage
// Qualifier	:
// Last Update	: 2017/9/18 - 17:04
// Desc.		:
//=============================================================================
void CWnd_Consumables::SetItemData(__in UINT nItemIndex, __in UINT nSpec, __in UINT nUsage)
{
	UINT nRemain = nSpec - nUsage;

	//m_st_Cell[CI_Y_MaxEnum][CH_X_MaxEnum];

	if (nItemIndex < m_nItemCount)
	{
		CStringW szText;
		
		szText.Format(L"%d", nSpec);
		m_st_Cell[nItemIndex][CH_X_Spec].SetText(szText.GetBuffer(0));
		
		szText.Format(L"%d", nUsage);
		m_st_Cell[nItemIndex][CH_X_Usage].SetText(szText.GetBuffer(0));
		
		szText.Format(L"%d", nRemain);
		m_st_Cell[nItemIndex][CH_X_Remain].SetText(szText.GetBuffer(0));
	}
}

//=============================================================================
// Method		: SetItemCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/9/21 - 16:28
// Desc.		:
//=============================================================================
void CWnd_Consumables::SetItemCount(__in UINT nCount)
{
	if (nCount <= CI_Y_MaxEnum)
	{
		if (m_nItemCount != nCount)
		{
			m_nItemCount = nCount;			

			if (GetSafeHwnd())
			{
				CRect rc;
				GetClientRect(rc);
				OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
			}
		}
	}
}
