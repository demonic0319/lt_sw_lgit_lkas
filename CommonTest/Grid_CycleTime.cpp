﻿//*****************************************************************************
// Filename	: 	Grid_CycleTime.cpp
// Created	:	2016/11/14 - 18:00
// Modified	:	2016/11/14 - 18:00
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Grid_CycleTime.h"

typedef enum
{
	IDX_Y_Header = 0,
	IDX_Y_Count,
	IDX_Y_Ave,
	IDX_Y_Min,
	IDX_Y_Max,
	IDX_ROW_MAX,
}enumRowHeaderCT;

static LPCTSTR lpszRowHeader[] =
{
	_T(""),
	_T("Cnt"),
	_T("Ave"),
	_T("Min"),
	_T("Max"),
	NULL
};

static const COLORREF clrRowHeader[] =
{
	RGB(0, 0, 0),
	RGB(50, 50, 200),
	RGB(50, 50, 200),
	RGB(50, 50, 200),
	RGB(50, 50, 200),
	RGB(50, 50, 200),
};

typedef enum
{
	IDX_X_Header	= 0,
	IDX_X_TT,
	IDX_X_CT,
	IDX_X_Para_L,
	IDX_X_Para_R,
	IDX_COL_MAX,
}enumColHeaderCT;

static LPCTSTR lpszColHeader[] =
{
	_T(""),
	_T("T/T"),
	_T("C/T"),
	_T("C/T L"),
	_T("C/T R"),
	NULL
};

static const COLORREF clrColHeader[] =
{
	RGB(0, 0, 0),	
	RGB(63, 101, 169),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
};

#define		RGB_BLACK		RGB(0x00, 0x00, 0x00)
#define		RGB_WHITE		RGB(0xFF, 0xFF, 0xFF)
#define		RGB_YELLOW		RGB(0xFF, 0xFF, 0x00)
#define		RGB_ROW_HEADER	RGB(0x77, 0x77, 0x77)//RGB(63, 101,  169)
#define		RGB_COL_HEADER	RGB(0xFF, 200, 100)
#define		RGB_TITLE		RGB(150, 200, 0xFF)
#define		RGB_BK_ID		RGB(135, 169, 213)

#define		RGB_BIT_SET		RGB(123, 255,  75) //RGB(112, 173, 71)
#define		RGB_BIT_CLEAR	RGB(100,  10,  10) //RGB(237, 125, 49)
//#define		RGB_SELECT		RGB(0xFF, 200, 100) //RGB(150, 200, 0xFF)

//=============================================================================
//
//=============================================================================
CGrid_CycleTime::CGrid_CycleTime()
{
	SetRowColCount(IDX_ROW_MAX, IDX_COL_MAX);

	//setup the fonts
	m_font_Header.CreateFont(16, 0, 0, 0, 900, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
	m_font_Data.CreateFont(14, 0, 0, 0, 600, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
}

//=============================================================================
//
//=============================================================================
CGrid_CycleTime::~CGrid_CycleTime()
{
	m_font_Header.DeleteObject();
	m_font_Data.DeleteObject();
}

//=============================================================================
// Method		: OnSetup
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/14 - 14:30
// Desc.		:
//=============================================================================
void CGrid_CycleTime::OnSetup()
{
	__super::OnSetup();
}

//=============================================================================
// Method		: DrawGridOutline
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::DrawGridOutline()
{
	CGrid_Base::DrawGridOutline();

	SetDefFont(&m_font_Data);

	// 헤더를 설정한다.
	InitHeader();
}

//=============================================================================
// Method		: CGrid_CycleTime::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::CalGridOutline()
{
	//CGrid_Base::CalGridOutline();

	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();

	if ((nWidth <= 0) || (nHeight <= 0))
		return;

	// 기본 열 추가 ---------------------------------------
	// 열 너비용 비율을 만듦 ()
	int		nColWidthRate[IDX_COL_MAX] = { 20, 30, 30, 30, 30 };
	int		iTotalRate = 0;
	for (UINT nIdx = 0; nIdx < m_nMaxCols; nIdx++)
	{
		iTotalRate += nColWidthRate[nIdx];
	}

	int		iMisc		= 0;
	int		iUnitWidth	= 0;
	// 기본 열 추가 ---------------------------------------
	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		iUnitWidth = (nWidth * nColWidthRate[iCol]) / iTotalRate;
		iMisc += iUnitWidth;
		SetColWidth(iCol, iUnitWidth);
	}
	// Width 계산하고 남거나 모자르는 공간 계산하여 Name 영역에 추가	
	SetColWidth(IDX_X_Header, ((nWidth * nColWidthRate[IDX_X_Header]) / iTotalRate) + nWidth - iMisc);

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight = nHeight / m_nMaxRows;
	UINT nRemindHeight = nHeight - (nUnitHeight * m_nMaxRows);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight + 1);
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight);
	}
}

//=============================================================================
// Method		: CGrid_CycleTime::InitRowHeader
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::InitHeader()
{
	for (UINT iRow = 0; iRow < m_nMaxRows; iRow++)
	{
		QuickSetFont		(IDX_X_Header, iRow, &m_font_Header);
		QuickSetBackColor	(IDX_X_Header, iRow, RGB_ROW_HEADER);
		QuickSetTextColor	(IDX_X_Header, iRow, RGB_WHITE);
		QuickSetText		(IDX_X_Header, iRow, lpszRowHeader[iRow]);
	}

	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		QuickSetFont		(iCol, IDX_Y_Header, &m_font_Header);
		QuickSetBackColor	(iCol, IDX_Y_Header, RGB_BLACK);
		QuickSetTextColor	(iCol, IDX_Y_Header, RGB_WHITE);
		QuickSetText		(iCol, IDX_Y_Header, lpszColHeader[iCol]);
	}

	for (UINT iRow = 1; iRow < m_nMaxRows; iRow++)
	{
		for (UINT iCol = 1; iCol < m_nMaxCols; iCol++)
		{
			QuickSetFont		(iCol, iRow, &m_font_Data);
			QuickSetBackColor	(iCol, iRow, RGB_WHITE);
			QuickSetTextColor	(iCol, iRow, RGB_BLACK);			
		}
	}

	//QuickSetText(IDX_X_Data, IDX_Y_OutputCnt, _T("0"));
}

//=============================================================================
// Method		: OnHint
// Access		: virtual protected  
// Returns		: int
// Parameter	: int col
// Parameter	: long row
// Parameter	: int section
// Parameter	: CString * string
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
int CGrid_CycleTime::OnHint(int col, long row, int section, CString *string)
{
	return FALSE;
}

//=============================================================================
// Method		: CGrid_CycleTime::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2015/12/10 - 23:28
// Desc.		:
//=============================================================================
void CGrid_CycleTime::OnGetCell(int col, long row, CUGCell *cell)
{
	CGrid_Base::OnGetCell(col, row, cell);

	//   	switch (row)
	//   	{
	//   	case IDX_Y_Header:
	//   		if (IDX_X_Total == col)
	//   			cell->SetBorder(cell->GetBorder() | UG_BDR_RMEDIUM | UG_BDR_BMEDIUM);
	//   		break;
	//   	}
}

//=============================================================================
// Method		: OnDrawFocusRect
// Access		: virtual protected  
// Returns		: void
// Parameter	: CDC * dc
// Parameter	: RECT * rect
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_CycleTime::OnDrawFocusRect(CDC *dc, RECT *rect)
{

}

//=============================================================================
// Method		: SetUseSocketCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/10/10 - 18:45
// Desc.		:
//=============================================================================
void CGrid_CycleTime::SetUseSocketCount(__in UINT nCount)
{
	m_nSocketCount = nCount;
	 
	if (1 < m_nSocketCount)
	{
		SetRowColCount(IDX_ROW_MAX, IDX_X_Para_L + m_nSocketCount);
	}
	else
	{
		SetRowColCount(IDX_ROW_MAX, IDX_X_Para_L);
	}
	 
	if (GetSafeHwnd())
	{
	 	CalGridOutline();
	}
}

//=============================================================================
// Method		: SetCycleTime
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_CycleTime * pstCycleTime
// Qualifier	:
// Last Update	: 2016/11/14 - 19:10
// Desc.		:
//=============================================================================
void CGrid_CycleTime::SetCycleTime(__in const ST_CycleTime* pstCycleTime)
{
	CString szText;

	// Tact Time
	szText.Format(_T("%d"), pstCycleTime->TactTime.dwCount);
	QuickSetText(IDX_X_TT, IDX_Y_Count, szText);

	szText.Format(_T("%.1f"), pstCycleTime->TactTime.dAverageTime / 1000.0f);
	QuickSetText(IDX_X_TT, IDX_Y_Ave, szText);

	if (0xFFFFFFFF != pstCycleTime->TactTime.dwMinTime)
	{
		szText.Format(_T("%.1f"), pstCycleTime->TactTime.dwMinTime / 1000.0f);
	}
	else
	{
		szText = _T("0");
	}
	QuickSetText(IDX_X_TT, IDX_Y_Min, szText);

	szText.Format(_T("%.1f"), pstCycleTime->TactTime.dwMaxTime / 1000.0f);
	QuickSetText(IDX_X_TT, IDX_Y_Max, szText);

	// Cycle Time
	szText.Format(_T("%d"), pstCycleTime->CycleTime.dwCount);
	QuickSetText(IDX_X_CT, IDX_Y_Count, szText);

	szText.Format(_T("%.1f"), pstCycleTime->CycleTime.dAverageTime / 1000.0f);
	QuickSetText(IDX_X_CT, IDX_Y_Ave, szText);

	if (0xFFFFFFFF != pstCycleTime->CycleTime.dwMinTime)
	{
		szText.Format(_T("%.1f"), pstCycleTime->CycleTime.dwMinTime / 1000.0f);
	}
	else
	{
		szText = _T("0");
	}
	QuickSetText(IDX_X_CT, IDX_Y_Min, szText);

	szText.Format(_T("%.1f"), pstCycleTime->CycleTime.dwMaxTime / 1000.0f);
	QuickSetText(IDX_X_CT, IDX_Y_Max, szText);

	// Para별 Cycle Time
	if (1 < m_nSocketCount)
	{
		UINT nIdx = 0;
		for (UINT nCol = IDX_X_Para_L; nCol < m_nMaxCols; nCol++, nIdx++)
		{
			szText.Format(_T("%d"), pstCycleTime->ParaTestTime[nIdx].dwCount);
			QuickSetText(nCol, IDX_Y_Count, szText);

			szText.Format(_T("%.1f"), pstCycleTime->ParaTestTime[nIdx].dAverageTime / 1000.0f);
			QuickSetText(nCol, IDX_Y_Ave, szText);

			if (0xFFFFFFFF != pstCycleTime->ParaTestTime[nIdx].dwMinTime)
			{
				szText.Format(_T("%.1f"), pstCycleTime->ParaTestTime[nIdx].dwMinTime / 1000.0f);
			}
			else
			{
				szText = _T("0");
			}
			QuickSetText(nCol, IDX_Y_Min, szText);

			szText.Format(_T("%.1f"), pstCycleTime->ParaTestTime[nIdx].dwMaxTime / 1000.0f);
			QuickSetText(nCol, IDX_Y_Max, szText);
		}
	}

	RedrawRange(1, 1, m_nMaxCols, m_nMaxRows);
}

