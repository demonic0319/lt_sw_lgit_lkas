//*****************************************************************************
// Filename	: 	Wnd_ImgProc.h
// Created	:	2016/3/22 - 10:09
// Modified	:	2016/3/22 - 10:09
//
// Author	:	PiRing
//	
// Purpose	:	영상 출력용 윈도우
//*****************************************************************************
#ifndef Wnd_ImgProc_h__
#define Wnd_ImgProc_h__

#pragma once

//#define		MAX_IMAGE_BUFFER	(5120 * 2880 * 4)2592 1944 4
#define		MAX_IMAGE_BUFFER	(2592 * 1944 * 4)

//-----------------------------------------------------------------------------
// CWnd_ImgProc
//-----------------------------------------------------------------------------

class CWnd_ImgProc : public CWnd
{
	DECLARE_DYNAMIC(CWnd_ImgProc)

public:
	CWnd_ImgProc();
	virtual ~CWnd_ImgProc();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnPaint			();
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	BITMAPINFO		m_BmpInfo;
	LPBYTE			m_lpbyRGB			= NULL;
	DWORD			m_dwRGBSize			= 0;

	BOOL			m_bRender			= FALSE;

	// 오버레이
	BOOL			m_bUseOverlay		= FALSE;
	BOOL			m_bUseCrosshair		= FALSE;
	CArray <RECT, RECT&> m_arrRect;
	CArray <BOOL, BOOL&> m_arrUseRect;

	BOOL			m_bUseFPS			= FALSE;
	DWORD			m_dwFrameRate		= 0;
	BOOL			m_bSignal			= FALSE;

public:
	
	void		SetUseFPS			(__in BOOL bUse);
	void		SetUseOverlay		(__in BOOL bUse = TRUE);
	void		SetUseCrosshair		(__in BOOL bUse = TRUE);

	void		AddOverlayRect		(__in RECT rect);
	void		ResetOverlayRect	();

	void		DrawEmptyRect		(__in CDC* pDC);
	void		DrawOverlayRect		(__in CDC* pDC, __in LONG lDestWidth, __in LONG lDestHeight);	
	void		DrawCrosshair		(__in CDC* pDC);

	void		DrawVideo			(__in CDC *pDC);
	void		DrawRGB				(__in CDC *pDC);

	void		Render				(__in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void		Render_Immediately	(__in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void		Render_Empty		();

	void		SetUseOverlayRect	(__in INT_PTR nIndex, __in BOOL bUse);

	void		SetFPS				(__in DWORD dwFrameRate);
	
};
#endif // Wnd_ImgProc_h__


