//*****************************************************************************
// Filename	: 	Wnd_TabImgProc.cpp
// Created	:	2018/2/8 - 11:08
// Modified	:	2018/2/8 - 11:08
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// Wnd_TabImgProc.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_TabImgProc.h"


// CWnd_TabImgProc

#define IDC_RB_TAB_BEGIN	1000
#define IDC_RB_TAB_END		IDC_RB_TAB_BEGIN + MAX_TAB_COUNT - 1

IMPLEMENT_DYNAMIC(CWnd_TabImgProc, CWnd_BaseView)

CWnd_TabImgProc::CWnd_TabImgProc()
{

}

CWnd_TabImgProc::~CWnd_TabImgProc()
{
}


BEGIN_MESSAGE_MAP(CWnd_TabImgProc, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_RB_TAB_BEGIN, IDC_RB_TAB_END, OnRangeRbTab)
END_MESSAGE_MAP()

// CWnd_TabImgProc message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2018/2/8 - 11:08
// Desc.		:
//=============================================================================
int CWnd_TabImgProc::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = /*WS_VISIBLE | */WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UINT	nWndID = 10;
	
	for (UINT nIdx = 0; nIdx < MAX_TAB_COUNT; nIdx++)
	{		
		m_rb_Tab[nIdx].Create(_T(""), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON, rectDummy, this, IDC_RB_TAB_BEGIN + nIdx);

//		m_rb_Tab[nIdx].SetFont(&m_Font);
//		m_rb_Tab[nIdx].m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
// 		m_rb_Tab[nIdx].SetImage(IDB_SELECTNO_16);
// 		m_rb_Tab[nIdx].SetCheckedImage(IDB_SELECT_16);
// 		m_rb_Tab[nIdx].SizeToContent();
// 		m_rb_Tab[nIdx].SetCheck(TRUE);
	}

	m_rb_Tab[0].ModifyStyle(0, WS_GROUP);

	m_wnd_LiveImage.Create(NULL, _T(""), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);;

	for (UINT nIdx = 0; nIdx < MAX_IMAGE_WINDOW; nIdx++)
	{
		m_arwnd_ImageProc[nIdx].Create(NULL, _T(""), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);;
	}

	if (m_bUseLive)
	{
		AddTab(&m_wnd_LiveImage, _T("Live"));
		SetActiveTab(0);
	}

	m_wnd_Dummy.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_wnd_Dummy.Create(_T(""), dwStyle | WS_VISIBLE | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);
	m_wnd_Dummy.SetText(_T("Empty"));

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2018/2/18 - 17:26
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 0;
	int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft			= iMagrin;
	int iTop			= iMagrin;
	int iWidth			= cx - iMagrin - iMagrin;
	int iHeight			= cy - iMagrin - iMagrin;

	int iTabWidth	= 100;
	int iTabHeight	= 20;
	
	iHeight = iHeight - iTabHeight;
	
	int iImgWidth = iWidth;
	int iImgHeight = ((0 < m_dwHeight) && (0 < m_dwHeight)) ? (iImgWidth * m_dwHeight / m_dwWidth) : 0;
	if (iHeight < iImgHeight)
	{
		iImgHeight = iHeight;
		iImgWidth =  ((0 < m_dwHeight) && (0 < m_dwHeight)) ? (iImgHeight * m_dwWidth / m_dwHeight) : 0;
	}

	if (iImgWidth < iWidth)
	{
		iLeft = (iWidth - iImgWidth) / 2;
	}
	else if (iImgHeight < iHeight)
	{
		iTop = (iHeight - iImgHeight) / 2;
	}

	// 영상 출력 윈도우
	m_wnd_LiveImage.MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
	for (UINT nIdx = 0; nIdx < MAX_IMAGE_WINDOW; nIdx++)
	{
		m_arwnd_ImageProc[nIdx].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
	}

	// 탭 버튼
	UINT iTabCnt = (UINT)m_arpWnd_Tab.GetCount();
	int iTempWidth = (0 < iTabCnt) ? (cx / iTabCnt) : 0;
	iTabWidth = min(iTabWidth, iTempWidth);
	int iRemind = iWidth - (iTabCnt * iTabWidth);

	iLeft = 0;
	iTop = iHeight;
	for (UINT nIdx = 0; nIdx < iTabCnt; nIdx++)
	{
		if (0 < iRemind)
		{
			m_rb_Tab[nIdx].MoveWindow(iLeft, iTop, iTabWidth + 1, iTabHeight);

			iLeft += (iTabWidth + 1);

			--iRemind;
		}
		else
		{
			m_rb_Tab[nIdx].MoveWindow(iLeft, iTop, iTabWidth, iTabHeight);

			iLeft += iTabWidth;
		}
	}
	for (UINT nIdx = iTabCnt; nIdx < MAX_TAB_COUNT; nIdx++)
	{
		m_rb_Tab[nIdx].MoveWindow(0, 0, 0, 0);
	}

	if (0 < iTabCnt)
	{
		m_wnd_Dummy.MoveWindow(0, 0, iWidth, iHeight);
		//m_wnd_Dummy.MoveWindow(0, 0, 0, 0);
	}
	else
	{
		m_wnd_Dummy.MoveWindow(0, 0, cx, cy);
	}
}

//=============================================================================
// Method		: OnRangeRbTab
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2018/2/19 - 13:58
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::OnRangeRbTab(UINT nID)
{
	UINT nIndex = nID - IDC_RB_TAB_BEGIN;

	SetActiveTab(nIndex);
}

//=============================================================================
// Method		: RedrawWindow
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/19 - 14:12
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::RedrawWindow()
{
	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
		//SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
	}
}

//=============================================================================
// Method		: AddTab
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in CWnd * pTabWnd
// Parameter	: __in LPCTSTR lpszTabLabel
// Qualifier	:
// Last Update	: 2018/2/18 - 18:14
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::AddTab(__in CWnd* pTabWnd, __in LPCTSTR lpszTabLabel)
{
	if (NULL != pTabWnd)
	{
		if (0 <= m_arpWnd_Tab.Add(pTabWnd))
		{
			INT_PTR iTabIdx = m_arpWnd_Tab.GetCount() - 1;
			m_rb_Tab[iTabIdx].SetWindowText(lpszTabLabel);	

			// 다시 그리기
			RedrawWindow();
		}
	}
}

//=============================================================================
// Method		: RemoveTab
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT iTab
// Parameter	: __in BOOL bRedraw
// Qualifier	:
// Last Update	: 2018/2/19 - 11:15
// Desc.		:
//=============================================================================
BOOL CWnd_TabImgProc::RemoveTab(__in UINT iTab, __in BOOL bRedraw /*= TRUE*/)
{
	if (iTab < m_arpWnd_Tab.GetCount())
	{
		// Hide Tab
		m_arpWnd_Tab.GetAt(iTab)->ShowWindow(SW_HIDE);

		// 배열에서 제거
		m_arpWnd_Tab.RemoveAt(iTab);

		if (0 < m_arpWnd_Tab.GetCount())
		{
			SetActiveTab((UINT)m_arpWnd_Tab.GetCount() - 1);
		}

		// 다시 그리기
		if (bRedraw)
		{
			RedrawWindow();
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: SetActiveTab
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT iTab
// Qualifier	:
// Last Update	: 2018/2/18 - 18:14
// Desc.		:
//=============================================================================
BOOL CWnd_TabImgProc::SetActiveTab(__in UINT iTab)
{
	if (iTab < m_arpWnd_Tab.GetCount())
	{
		// 이전 탭 비활성화
		if (m_nActiveTabIndex != iTab)
		{
			if ((0 <= m_nActiveTabIndex) && (m_nActiveTabIndex < m_arpWnd_Tab.GetCount()))
			{
				m_rb_Tab[m_nActiveTabIndex].SetCheck(BST_UNCHECKED);

				if (m_arpWnd_Tab.GetAt(m_nActiveTabIndex)->GetSafeHwnd())
				{
					m_arpWnd_Tab.GetAt(m_nActiveTabIndex)->ShowWindow(SW_HIDE);
				}
			}

			m_nActiveTabIndex = iTab;
		}

		// 선택 탭 활성화
		if (m_arpWnd_Tab.GetAt(m_nActiveTabIndex)->GetSafeHwnd())
		{
			m_arpWnd_Tab.GetAt(m_nActiveTabIndex)->ShowWindow(SW_SHOW);
		}

		m_rb_Tab[m_nActiveTabIndex].SetCheck(BST_CHECKED);
	}

	return TRUE;
}

//=============================================================================
// Method		: Set_UseLiveImage
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bIN_UseLive
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::Set_UseLiveImage(__in BOOL bIN_UseLive)
{
	if (m_bUseLive != bIN_UseLive)
	{
		if (GetSafeHwnd()) // 윈도우가 생성된 이후 사용여부 변경
		{
			CStringArray szarLabel;
			CString szLabel;

			int iStartOffset = m_bUseLive ? 1 : 0;
			int iCount = (int)m_arpWnd_Tab.GetCount();

			if (0 < iCount)
			{
				// 탭의 이름 구하기
				for (INT nIdx = iStartOffset; nIdx < iCount; nIdx++)
				{
					m_rb_Tab[nIdx].GetWindowText(szLabel);
					szarLabel.Add(szLabel);
				}

				// Tab 삭제
				for (INT nIdx = (iCount - 1); 0 <= nIdx; nIdx--)
				{
					RemoveTab(nIdx, FALSE);
				}
				//m_tc_History.RemoveAllTabs();
			}
			
			// 미사용 -> 사용
			if (bIN_UseLive)
			{
				AddTab(&m_wnd_LiveImage, _T("Live"));
			}

			// 탭에 다시 추가
			iStartOffset = 0;//m_tc_History.GetTabsNum();
			for (UINT nIdx = 0; nIdx < szarLabel.GetCount(); nIdx++)
			{
				AddTab(&m_arwnd_ImageProc[nIdx], szarLabel.GetAt(nIdx));
			}

			if (0 < m_arpWnd_Tab.GetCount())
			{
				SetActiveTab(0);
			}
		}

		m_bUseLive = bIN_UseLive;
	}
}

//=============================================================================
// Method		: Add_ImageTab
// Access		: virtual public  
// Returns		: int
// Parameter	: __in LPCTSTR lpszTabLabel
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
int CWnd_TabImgProc::Add_ImageTab(__in LPCTSTR lpszTabLabel)
{
	UINT iWndIndex = 0;
	if (m_bUseLive)
	{
		iWndIndex = (UINT)(m_arpWnd_Tab.GetCount() - 1);
	}
	else
	{
		iWndIndex = (UINT)m_arpWnd_Tab.GetCount();
	}

	if (iWndIndex < MAX_IMAGE_WINDOW)
	{
		AddTab(&m_arwnd_ImageProc[iWndIndex], lpszTabLabel);

		if (FALSE == m_bUseLive) // Live Image 고정
		{
			iWndIndex = (UINT)(m_arpWnd_Tab.GetCount() - 1);
			SetActiveTab(iWndIndex);
		}
	}
	else
	{
		TRACE(_T("CWnd_TabImgProc::Add_ImageTab() Error : MAX_IMAGE_WINDOW <= Tab Count\n"));
	}

	return iWndIndex;
}

//=============================================================================
// Method		: Remove_ImageTab
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::Remove_ImageTab()
{
	UINT iWindowCount = 0;
	if (m_bUseLive)
	{
		iWindowCount = (UINT)m_arpWnd_Tab.GetCount() - 1;
	}
	else
	{
		iWindowCount = (UINT)m_arpWnd_Tab.GetCount();
	}

	if (0 < iWindowCount)
	{
		for (INT nIdx = (INT)(iWindowCount - 1); 0 <= nIdx; nIdx--)
		{
			m_arwnd_ImageProc[nIdx].Render_Empty();

			if (m_bUseLive)
			{
				RemoveTab(nIdx + 1, FALSE); // Live Image 고정 1개
			}
			else
			{
				RemoveTab(nIdx, FALSE);
			}
		}

		// 다시 그리기
		RedrawWindow();
	}
}

//=============================================================================
// Method		: ShowVideo
// Access		: public  
// Returns		: void
// Parameter	: __in UINT iWndIndex
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::ShowVideo(__in UINT iWndIndex, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (iWndIndex < MAX_IMAGE_WINDOW)
	{
		m_arwnd_ImageProc[iWndIndex].Render(lpVideo, dwWidth, dwHeight);

		if ((m_dwWidth != dwWidth) || (m_dwHeight != dwHeight))
		{
			m_dwWidth	= dwWidth;
			m_dwHeight	= dwHeight;

			RedrawWindow();
		}
	}
}

//=============================================================================
// Method		: NoSignal_Ch
// Access		: public  
// Returns		: void
// Parameter	: __in UINT iWndIndex
// Qualifier	:
// Last Update	: 2018/2/14 - 14:40
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::NoSignal_Ch(__in UINT iWndIndex)
{
	if (iWndIndex < MAX_IMAGE_WINDOW)
	{
		m_arwnd_ImageProc[iWndIndex].Render_Empty();
	}
}

//=============================================================================
// Method		: ShowVideo_Live
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/14 - 14:47
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::ShowVideo_Live(__in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (m_bUseLive)
	{
		m_wnd_LiveImage.Render(lpVideo, dwWidth, dwHeight);

		if ((m_dwWidth != dwWidth) || (m_dwHeight != dwHeight))
		{
			m_dwWidth = dwWidth;
			m_dwHeight = dwHeight;

			RedrawWindow();
		}
	}
}

//=============================================================================
// Method		: NoSignal_Live
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/14 - 14:47
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::NoSignal_Live()
{
	if (m_bUseLive)
	{
		m_wnd_LiveImage.Render_Empty();
	}
}

//=============================================================================
// Method		: Set_ImageSize
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/19 - 14:18
// Desc.		:
//=============================================================================
void CWnd_TabImgProc::Set_ImageSize(__in DWORD dwWidth, __in DWORD dwHeight)
{
	if ((m_dwWidth != dwWidth) || (m_dwHeight != dwHeight))
	{
		m_dwWidth = dwWidth;
		m_dwHeight = dwHeight;

		RedrawWindow();
	}
}

