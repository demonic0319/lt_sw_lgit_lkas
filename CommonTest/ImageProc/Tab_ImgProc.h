//*****************************************************************************
// Filename	: 	Tab_ImgProc.h
// Created	:	2018/2/2 - 17:15
// Modified	:	2018/2/2 - 17:15
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Tab_ImgProc_h__
#define Tab_ImgProc_h__

#pragma once

#include "Wnd_ImgProc.h"
//#include "Wnd_ImgProc_V2.h"

#define MAX_IMAGE_WINDOW	30	// 2D Cal 기준 (15단계), Stereo Cal 기준 (max 30단계??)

//=============================================================================
// CTab_ImgProc
//=============================================================================
class CTab_ImgProc : public CMFCTabCtrl
{
	DECLARE_DYNAMIC(CTab_ImgProc)

public:
	CTab_ImgProc();
	virtual ~CTab_ImgProc();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	
	// 실시간 영상 출력용 윈도우
	CWnd_ImgProc	m_wnd_LiveImage;
	// 영상 이력 출력용 윈도우
	CWnd_ImgProc	m_arwnd_ImageProc[MAX_IMAGE_WINDOW];
	
	// 탭에 삽입된 영상 이력 윈도우 개수
	UINT			m_nWindowCount			= 0;

	// 실시간 영상 뷰 사용여부
	BOOL			m_bUseLive				= FALSE;

public:

	// 실시간 영상 뷰 사용여부 
	void			Set_UseLiveImage		(__in BOOL bUseLive);

	// 영상 이력 탭 추가
	virtual int		Add_ImageTab			(__in LPCTSTR lpszTabLabel);
	// 영상 이력 탭 전체 삭제
	virtual void	Remove_ImageTab			();

	// 영상 출력
	void			ShowVideo				(__in UINT iWndIndex, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void			NoSignal_Ch				(__in UINT iWndIndex);	

	void			ShowVideo_Live			(__in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void			NoSignal_Live			();	
	
	// 오버레이 출력



};

#endif // Tab_ImgProc_h__

