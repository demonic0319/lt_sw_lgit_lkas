//*****************************************************************************
// Filename	: 	OverlayImage.cpp
// Created	:	2017/3/16 - 10:14
// Modified	:	2017/3/16 - 10:14
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "OverlayImage.h"


COverlayImage::COverlayImage()
{
}


COverlayImage::~COverlayImage()
{
	m_imgOverlay.Destroy();
	//m_brush.DeleteObject();
}

void COverlayImage::MakeRectBitmap()
{
	//if (m_imgThreshold.IsNull())
	{
		m_imgOverlay.Create(m_nWidth, m_nHeight, 24);
		CDC *pDC = CDC::FromHandle(m_imgOverlay.GetDC());
		CBrush brush, *pOldBrush;
		brush.CreateSolidBrush(m_rgbBackground);
		pOldBrush = pDC->SelectObject(&brush);
		pDC->Rectangle(0, 0, m_nWidth, m_nHeight);     // background 그리기
		
		
		
		// ... 그리고 싶은 것들 그리기
		
		
		
		pDC->SelectObject(pOldBrush);
		brush.DeleteObject();
		m_imgOverlay.ReleaseDC();
	}
}

HBITMAP COverlayImage::Detach() throw()
{
	HBITMAP hBitmap;
// 	ATLASSUME(m_hBitmap != NULL);
// 	ATLASSUME(m_hDC == NULL);
// 	hBitmap = m_hBitmap;
// 	m_hBitmap = NULL;
// 	m_pBits = NULL;
// 	m_nWidth = 0;
// 	m_nHeight = 0;
// 	m_nBPP = 0;
// 	m_nPitch = 0;
// 	m_iTransparentColor = -1;
// 	m_bHasAlphaChannel = false;
// 	m_bIsDIBSection = false;

	return(hBitmap);
}

void COverlayImage::DrawOverlay(CDC *pDC, CRect rcROI, CRect rcView)
{
	m_imgOverlay.TransparentBlt(pDC->GetSafeHdc(), rcView, rcROI, m_rgbBackground);
}
