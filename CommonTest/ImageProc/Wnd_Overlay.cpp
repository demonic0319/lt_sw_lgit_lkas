//*****************************************************************************
// Filename	: 	Wnd_Overlay.cpp
// Created	:	2017/3/20 - 15:59
// Modified	:	2017/3/20 - 15:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Overlay.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Overlay.h"


// CWnd_Overlay

IMPLEMENT_DYNAMIC(CWnd_Overlay, CWnd)

CWnd_Overlay::CWnd_Overlay()
{

}

CWnd_Overlay::~CWnd_Overlay()
{
}


BEGIN_MESSAGE_MAP(CWnd_Overlay, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CWnd_Overlay message handlers




int CWnd_Overlay::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Add your specialized creation code here

	return 0;
}


void CWnd_Overlay::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
}


void CWnd_Overlay::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	


	// Do not call CWnd::OnPaint() for painting messages
}


BOOL CWnd_Overlay::OnEraseBkgnd(CDC* pDC)
{
	CImageList;

	return TRUE;
	//return CWnd::OnEraseBkgnd(pDC);
}


BOOL CWnd_Overlay::PreCreateWindow(CREATESTRUCT& cs)
{
	ModifyStyleEx(0, WS_EX_LAYERED | WS_EX_TRANSPARENT);
	
	SetWindowLong(m_hWnd, GWL_EXSTYLE, GetWindowLong(m_hWnd, GWL_EXSTYLE) | WS_EX_LAYERED);
	SetLayeredWindowAttributes(RGB(0,0,0), 128, LWA_ALPHA);

	return CWnd::PreCreateWindow(cs);
}
