//*****************************************************************************
// Filename	: 	Wnd_TabImgProc.h
// Created	:	2018/2/2 - 17:15
// Modified	:	2018/2/2 - 17:15
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Wnd_TabImgProc_h__
#define Wnd_TabImgProc_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Wnd_ImgProc.h"
#include "VGButton.h"
#include "VGStatic.h"

#define MAX_IMAGE_WINDOW	40	// 2D Cal 기준 (15단계), Stereo Cal 기준 (max 30단계??)
#define MAX_TAB_COUNT		MAX_IMAGE_WINDOW + 1

//=============================================================================
// CWnd_TabImgProc
//=============================================================================
class CWnd_TabImgProc : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_TabImgProc)

public:
	CWnd_TabImgProc();
	virtual ~CWnd_TabImgProc();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnRangeRbTab			(UINT nID);
	
	// 탭 버튼
	CArray<CWnd*, CWnd*>	m_arpWnd_Tab;

	//CStringArray	m_arsz_TabLabel;
	CMFCButton		m_rb_Tab[MAX_TAB_COUNT];
	//CVGButton		m_rb_Tab[MAX_TAB_COUNT];

	// Dummy Window
	CVGStatic		m_wnd_Dummy;

	// 실시간 영상 출력용 윈도우
	CWnd_ImgProc	m_wnd_LiveImage;
	// 영상 이력 출력용 윈도우
	CWnd_ImgProc	m_arwnd_ImageProc[MAX_IMAGE_WINDOW];
	
	// 실시간 영상 뷰 사용여부
	BOOL			m_bUseLive				= FALSE;

	DWORD			m_dwWidth				= 640;
	DWORD			m_dwHeight				= 480;

	INT				m_nActiveTabIndex		= -1;

	void			RedrawWindow			();

	virtual void	AddTab					(__in CWnd* pTabWnd, __in LPCTSTR lpszTabLabel);
	virtual BOOL	RemoveTab				(__in UINT iTab, __in BOOL bRedraw = TRUE);
	virtual BOOL	SetActiveTab			(__in UINT iTab);

public:

	// 실시간 영상 뷰 사용여부 
	void			Set_UseLiveImage		(__in BOOL bIN_UseLive);

	// 영상 이력 탭 추가
	virtual int		Add_ImageTab			(__in LPCTSTR lpszTabLabel);
	// 영상 이력 탭 전체 삭제
	virtual void	Remove_ImageTab			();

	// 영상 출력
	void			ShowVideo				(__in UINT iWndIndex, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void			NoSignal_Ch				(__in UINT iWndIndex);	

	void			ShowVideo_Live			(__in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void			NoSignal_Live			();	
	
	// 오버레이 출력

	// 영상 크기 설정
	void			Set_ImageSize			(__in DWORD dwWidth, __in DWORD dwHeight);


};

#endif // Wnd_TabImgProc_h__

