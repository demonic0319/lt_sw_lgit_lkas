//*****************************************************************************
// Filename	: 	Wnd_Chart.cpp
// Created	:	2017/3/15 - 19:05
// Modified	:	2017/3/15 - 19:05
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Chart.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Chart.h"


// CWnd_Chart

IMPLEMENT_DYNAMIC(CWnd_Chart, CWnd)

CWnd_Chart::CWnd_Chart()
{
	m_nChartType = Chart_Blank;
}

CWnd_Chart::~CWnd_Chart()
{
}


BEGIN_MESSAGE_MAP(CWnd_Chart, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()



// CWnd_Chart message handlers
int CWnd_Chart::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/3/14 - 14:52
// Desc.		:
//=============================================================================
void CWnd_Chart::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

}

//=============================================================================
// Method		: OnPaint
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/14 - 14:52
// Desc.		:
//=============================================================================
void CWnd_Chart::OnPaint()
{
	CPaintDC dc(this); // device context for painting

#ifdef USE_GDI_PLUS

	Graphics graphics(dc.GetSafeHdc());

	DrawBlank(graphics);

	switch (m_nChartType)
	{
	case Chart_Blank:
		//DrawBlank(graphics);
		break;

	case Chart_Circle:
		DrawChartCricle(graphics);
		break;

	case Chart_GridChess:
		DrawChartGridChess(graphics);
		break;

	case Chart_Crosshair:
		DrawChartCrosshair(graphics);
		break;

	case Chart_Shapes:
		break;

	default:
		break;
	}

#else	// Use GDI

	DrawBlank(&dc);
	
	switch (m_nChartType)
	{
	case Chart_Blank:
	 	//DrawBlank(&dc);
	 	break;
	 
	case Chart_Circle:
	 	DrawChartCricle(&dc);
	 	break;
	 
	case Chart_GridChess:
	 	DrawChartGridChess(&dc);
	 	break;

	case Chart_Crosshair:
		DrawChartCrosshair(&dc);
		break;

	case Chart_Shapes:
		break;

	default:
	 	break;
	}

#endif
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: protected  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2017/3/15 - 11:35
// Desc.		:
//=============================================================================
BOOL CWnd_Chart::OnEraseBkgnd(CDC* pDC)
{
	//Invalidate();

	return CWnd::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: DrawBlank
// Access		: protected  
// Returns		: void
// Parameter	: __in CDC * pDC
// Qualifier	:
// Last Update	: 2017/3/14 - 17:39
// Desc.		:
//=============================================================================
void CWnd_Chart::DrawBlank(__in CDC* pDC)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	try
	{
		CBrush	NewBrush;
		CBrush* pOldBrush = NULL;
		CRect rect;
		GetClientRect(&rect);

		NewBrush.CreateSolidBrush(RGB(255, 255, 255));
		pOldBrush = pDC->SelectObject(&NewBrush);

		pDC->Rectangle(rect);
		pDC->SelectObject(pOldBrush);
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

void CWnd_Chart::DrawBlank(__in Gdiplus::Graphics& graphics)
{
	try
	{
		CRect rect;
		GetClientRect(&rect);

		Gdiplus::SolidBrush gBrush(Color::White);
		Gdiplus::Rect		gRect(rect.left, rect.top, rect.Width(), rect.Height());

		graphics.FillRectangle(&gBrush, gRect);
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: Draw_Ellipse
// Access		: protected  
// Returns		: void
// Parameter	: __in CDC * pDC
// Parameter	: __in CRect rect
// Parameter	: __in COLORREF clrBack
// Qualifier	:
// Last Update	: 2017/3/15 - 18:59
// Desc.		:
//=============================================================================
void CWnd_Chart::Draw_Ellipse(__in CDC* pDC, __in CRect rect, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	try
	{
		CBrush	NewBrush;
		CBrush* pOldBrush = NULL;

		NewBrush.CreateSolidBrush(clrBack);
		pOldBrush = pDC->SelectObject(&NewBrush);

		pDC->Ellipse(&rect);
		pDC->SelectObject(pOldBrush);
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

void CWnd_Chart::Draw_Ellipse(__in Gdiplus::Graphics& graphics, __in Gdiplus::Rect rect, __in Gdiplus::Color clrBack /*= Color::Black*/)
{
	try
	{
		Gdiplus::SolidBrush gBrush(clrBack);

		graphics.FillEllipse(&gBrush, rect);
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: Draw_Rectangle
// Access		: protected  
// Returns		: void
// Parameter	: __in CDC * pDC
// Parameter	: __in CRect rect
// Parameter	: __in COLORREF clrBack
// Qualifier	:
// Last Update	: 2017/3/15 - 14:17
// Desc.		:
//=============================================================================
void CWnd_Chart::Draw_Rectangle(__in CDC* pDC, __in CRect rect, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	try
	{
		CBrush	NewBrush;
		CBrush* pOldBrush = NULL;

		NewBrush.CreateSolidBrush(clrBack);
		pOldBrush = pDC->SelectObject(&NewBrush);

		pDC->Rectangle(&rect);
		pDC->SelectObject(pOldBrush);
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

void CWnd_Chart::Draw_Rectangle(__in Gdiplus::Graphics& graphics, __in Gdiplus::Rect rect, __in Gdiplus::Color clrBack /*= Color::Black*/)
{
	try
	{
		Gdiplus::SolidBrush gBrush(clrBack);

		graphics.FillRectangle(&gBrush, rect);
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: DrawChartCrosshair
// Access		: protected  
// Returns		: void
// Parameter	: __in CDC * pDC
// Qualifier	:
// Last Update	: 2017/3/15 - 15:51
// Desc.		:
//=============================================================================
void CWnd_Chart::DrawChartCrosshair(__in CDC* pDC)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	try
	{
		CRect rectClient;
		GetClientRect(&rectClient);

		CPoint ptCenter = rectClient.CenterPoint();

		CPen OutterBorderPen(PS_SOLID, (int)m_Item_Crosshair.fCenterLineThickness, m_Item_Crosshair.clrCenterLine);
		CPen InnerBorderPen(PS_SOLID, (int)m_Item_Crosshair.fLineThickness, m_Item_Crosshair.clrLine);
		

		// 선그리기 -------------------------------------------
		pDC->SelectObject(OutterBorderPen);

		// 가로 선 그리기
		pDC->MoveTo(0, ptCenter.y);
		pDC->LineTo(rectClient.right, ptCenter.y);

		// 세로 선 그리기
		pDC->MoveTo(ptCenter.x, 0);
		pDC->LineTo(ptCenter.x, rectClient.bottom);

		//1/4, 2/4, 3/4
		LONG PosX = 0;
		LONG PosY = 0;

		// 4등분 선
		pDC->SelectObject(InnerBorderPen);

		PosX = (rectClient.right * 2 / 8);
		pDC->MoveTo(PosX, 0);
		pDC->LineTo(PosX, rectClient.bottom);

		PosX = (rectClient.right * 6 / 8);
		pDC->MoveTo(PosX, 0);
		pDC->LineTo(PosX, rectClient.bottom);

		PosY = (rectClient.bottom * 2 / 8);
		pDC->MoveTo(0, PosY);
		pDC->LineTo(rectClient.right, PosY);

		PosY = (rectClient.bottom * 6 / 8);
		pDC->MoveTo(0, PosY);
		pDC->LineTo(rectClient.right, PosY);

		// 8등분 선
		//PosX = (rectClient.right / 8);
		//pDC->MoveTo(PosX, 0);
		//pDC->LineTo(PosX, rectClient.bottom);

		//PosX = (rectClient.right * 3 / 8);
		//pDC->MoveTo(PosX, 0);
		//pDC->LineTo(PosX, rectClient.bottom);

		//PosX = (rectClient.right * 5 / 8);
		//pDC->MoveTo(PosX, 0);
		//pDC->LineTo(PosX, rectClient.bottom);

		//PosX = (rectClient.right * 7 / 8);
		//pDC->MoveTo(PosX, 0);
		//pDC->LineTo(PosX, rectClient.bottom);

		//PosY = (rectClient.bottom / 8);
		//pDC->MoveTo(0, PosY);
		//pDC->LineTo(rectClient.right, PosY);

		//PosY = (rectClient.bottom * 3 / 8);
		//pDC->MoveTo(0, PosY);
		//pDC->LineTo(rectClient.right, PosY);

		//PosY = (rectClient.bottom * 5 / 8);
		//pDC->MoveTo(0, PosY);
		//pDC->LineTo(rectClient.right, PosY);

		//PosY = (rectClient.bottom * 7 / 8);
		//pDC->MoveTo(0, PosY);
		//pDC->LineTo(rectClient.right, PosY);
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: DrawChartCrosshair
// Access		: protected  
// Returns		: void
// Parameter	: __in Gdiplus::Graphics & graphics
// Qualifier	:
// Last Update	: 2017/3/15 - 17:13
// Desc.		:
//=============================================================================
void CWnd_Chart::DrawChartCrosshair(__in Gdiplus::Graphics& graphics)
{
	try
	{
		CRect rectCl;
		GetClientRect(&rectCl);

		CPoint ptCenter = rectCl.CenterPoint();

		Gdiplus::Pen	OutterBorderPen(Color(GetRValue(m_Item_Crosshair.clrCenterLine), GetGValue(m_Item_Crosshair.clrCenterLine), GetBValue(m_Item_Crosshair.clrCenterLine)), (float)m_Item_Crosshair.fCenterLineThickness);
		Gdiplus::Pen	InnerBorderPen(Color(GetRValue(m_Item_Crosshair.clrLine), GetGValue(m_Item_Crosshair.clrLine), GetBValue(m_Item_Crosshair.clrLine)), (float)m_Item_Crosshair.fLineThickness);
		Gdiplus::Rect	rectClient = Gdiplus::Rect(rectCl.left, rectCl.top, rectCl.Width(), rectCl.Height());

		// 선그리기 -------------------------------------------

		// 가로 선 그리기
		graphics.DrawLine(&OutterBorderPen, (REAL)0.0, (REAL)ptCenter.y, (REAL)rectClient.GetRight(), (REAL)ptCenter.y);
		
		// 세로 선 그리기
		graphics.DrawLine(&OutterBorderPen, (REAL)ptCenter.x, (REAL)0.0, (REAL)ptCenter.x, (REAL)rectClient.GetBottom());

		//1/4, 2/4, 3/4
		LONG PosX = 0;
		LONG PosY = 0;

		// 4등분 선
		PosX = (rectClient.GetRight() * 2 / 8);
		graphics.DrawLine(&InnerBorderPen, (REAL)PosX, (REAL)0.0, (REAL)PosX, (REAL)rectClient.GetBottom());

		PosX = (rectClient.GetRight() * 6 / 8);
		graphics.DrawLine(&InnerBorderPen, (REAL)PosX, (REAL)0.0, (REAL)PosX, (REAL)rectClient.GetBottom());

		PosY = (rectClient.GetBottom() * 2 / 8);
		graphics.DrawLine(&InnerBorderPen, (REAL)0.0, (REAL)PosY, (REAL)rectClient.GetRight(), (REAL)PosY);

		PosY = (rectClient.GetBottom() * 6 / 8);
		graphics.DrawLine(&InnerBorderPen, (REAL)0.0, (REAL)PosY, (REAL)rectClient.GetRight(), (REAL)PosY);

		// 8등분 선
		//PosX = (rectClient.GetRight() / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)PosX, (REAL)0.0, (REAL)PosX, (REAL)rectClient.GetBottom());

		//PosX = (rectClient.GetRight() * 3 / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)PosX, (REAL)0.0, (REAL)PosX, (REAL)rectClient.GetBottom());

		//PosX = (rectClient.GetRight() * 5 / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)PosX, (REAL)0.0, (REAL)PosX, (REAL)rectClient.GetBottom());

		//PosX = (rectClient.GetRight() * 7 / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)PosX, (REAL)0.0, (REAL)PosX, (REAL)rectClient.GetBottom());

		//PosY = (rectClient.GetBottom() / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)0.0, (REAL)PosY, (REAL)rectClient.GetRight(), (REAL)PosY);

		//PosY = (rectClient.GetBottom() * 3 / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)0.0, (REAL)PosY, (REAL)rectClient.GetRight(), (REAL)PosY);

		//PosY = (rectClient.GetBottom() * 5 / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)0.0, (REAL)PosY, (REAL)rectClient.GetRight(), (REAL)PosY);
		
		//PosY = (rectClient.GetBottom() * 7 / 8);
		//graphics.DrawLine(&InnerBorderPen, (REAL)0.0, (REAL)PosY, (REAL)rectClient.GetRight(), (REAL)PosY);

	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: DrawCricle
// Access		: protected  
// Returns		: void
// Parameter	: __in CDC * pDC
// Qualifier	:
// Last Update	: 2017/3/14 - 14:55
// Desc.		:
//=============================================================================
void CWnd_Chart::DrawChartCricle(__in CDC* pDC)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	try
	{
		INT_PTR iCount = m_Item_Circle.GetCount();
		if (0 < iCount)
		{
			CBrush	NewBrush;
			CBrush* pOldBrush = NULL;

			for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
			{
				NewBrush.CreateSolidBrush(m_Item_Circle[nIdx].Color);
				pOldBrush = pDC->SelectObject(&NewBrush);

				pDC->Ellipse(&m_Item_Circle[nIdx].Rect);
				pDC->SelectObject(pOldBrush);
			}
		}
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

void CWnd_Chart::DrawChartCricle(__in Gdiplus::Graphics& graphics)
{
	try
	{
		INT_PTR iCount = m_Item_Circle.GetCount();
		if (0 < iCount)
		{
			CRect*	pRect = NULL;
			Gdiplus::Color	clrBack;
			graphics.SetSmoothingMode(SmoothingMode::SmoothingModeAntiAlias);

			for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
			{
				clrBack.SetFromCOLORREF(m_Item_Circle[nIdx].Color);
				pRect = &m_Item_Circle[nIdx].Rect;

				Gdiplus::SolidBrush gBrush(clrBack);
				Gdiplus::Rect		gRect(pRect->left, pRect->top, pRect->Width(), pRect->Height());

				graphics.FillEllipse(&gBrush, gRect);
			}
		}
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: DrawChartShape
// Access		: protected  
// Returns		: void
// Parameter	: __in CDC * pDC
// Qualifier	:
// Last Update	: 2017/3/15 - 15:43
// Desc.		:
//=============================================================================
void CWnd_Chart::DrawChartShape(__in CDC* pDC)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	try
	{
		INT_PTR iCount = m_Item_Shapes.GetCount();
		if (0 < iCount)
		{
			for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
			{
				switch (m_Item_Shapes[nIdx].ShapeType)
				{
				case Shape_Ellipse:
				{
					CBrush	NewBrush;					
					NewBrush.CreateSolidBrush(m_Item_Circle[nIdx].Color);
					CBrush* pOldBrush = pDC->SelectObject(&NewBrush);

					pDC->Ellipse(&m_Item_Circle[nIdx].Rect);
					pDC->SelectObject(pOldBrush);
				}
					break;
				
				case Shape_Rectangle:
					Draw_Rectangle(pDC, m_Item_Shapes[nIdx].Rect, m_Item_Shapes[nIdx].Color);
					break;

				case Shape_Polygon:
					break;

				default:
					break;
				}
			}
		}
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

void CWnd_Chart::DrawChartShape(__in Gdiplus::Graphics& graphics)
{
	try
	{
		INT_PTR iCount = m_Item_Shapes.GetCount();
		if (0 < iCount)
		{
			CRect*	pRect = NULL;
			Gdiplus::Color		clrBack;
			Gdiplus::SolidBrush gBrush(clrBack);
			Gdiplus::Rect		gRect;

			for (INT_PTR nIdx = 0; nIdx < iCount; nIdx++)
			{				
				clrBack.SetFromCOLORREF(m_Item_Shapes[nIdx].Color);
				pRect = &m_Item_Shapes[nIdx].Rect;

				gBrush.SetColor(clrBack);
				gRect = Gdiplus::Rect(pRect->left, pRect->top, pRect->Width(), pRect->Height());

				switch (m_Item_Shapes[nIdx].ShapeType)
				{
				case Shape_Ellipse:
					graphics.FillEllipse(&gBrush, gRect);
					break;

				case Shape_Rectangle:
					graphics.FillRectangle(&gBrush, gRect);
					break;

				case Shape_Polygon:
					break;

				default:
					break;
				}
			}
		}
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: DrawChartGridChess
// Access		: protected  
// Returns		: void
// Parameter	: __in CDC * pDC
// Qualifier	:
// Last Update	: 2017/3/14 - 16:08
// Desc.		:
//=============================================================================
void CWnd_Chart::DrawChartGridChess(__in CDC* pDC)
{
	if (pDC == NULL)
		pDC = this->GetDC();

	try
	{
		CRect	rectClient;
		CPoint	ptStart;
		CPoint	ptOffset;

		GetClientRect(rectClient);

		ptStart.x = rectClient.left - (m_Item_GridChess.nGridWidth / 2);
		ptStart.y = rectClient.top - (m_Item_GridChess.nGridHeight / 2);

		ptOffset.x = ptStart.x;
		ptOffset.y = ptStart.y;

		CRect gRect(ptStart.x, ptStart.y, ptOffset.x + m_Item_GridChess.nGridWidth, ptOffset.y + m_Item_GridChess.nGridHeight);
		//gRect.right = m_Item_GridChess.nGridWidth;
		//gRect.Height = m_Item_GridChess.nGridHeight;

		// 세로
		for (UINT nVert = 0;; nVert++)
		{
			gRect.MoveToY(ptOffset.y);
			//gRect.Y = ptOffset.y;

			// 가로
			for (UINT nHorz = 0;; nHorz++)
			{
				gRect.MoveToX(ptOffset.x);
				//gRect.left = ptOffset.x;

				Draw_Rectangle(pDC, gRect);

				ptOffset.x += (m_Item_GridChess.nGridWidth * 2);

				if (rectClient.right < ptOffset.x)
				{
					break;
				}
			}

			if (0 == (nVert % 2))
				ptOffset.x = ptStart.x + m_Item_GridChess.nGridWidth;
			else
				ptOffset.x = ptStart.x;

			ptOffset.y += m_Item_GridChess.nGridHeight;

			if (rectClient.bottom < ptOffset.y)
			{
				break;
			}
		}
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

void CWnd_Chart::DrawChartGridChess(__in Gdiplus::Graphics& graphics)
{
	try
	{
		CRect	rectClient;
		CPoint	ptStart;
		CPoint	ptOffset;

		GetClientRect(rectClient);

		ptStart.x = rectClient.left - (m_Item_GridChess.nGridWidth / 2);
		ptStart.y = rectClient.top - (m_Item_GridChess.nGridHeight / 2);

		ptOffset.x = ptStart.x;
		ptOffset.y = ptStart.y;

		Gdiplus::Rect gRect;
		gRect.Width	= m_Item_GridChess.nGridWidth;
		gRect.Height= m_Item_GridChess.nGridHeight;

		// 세로
		for (UINT nVert = 0; ; nVert++)
		{
			gRect.Y = ptOffset.y;

			// 가로
			for (UINT nHorz = 0; ; nHorz++)
			{
				gRect.X = ptOffset.x;

				Draw_Rectangle(graphics, gRect);

				ptOffset.x += (m_Item_GridChess.nGridWidth * 2);

				if (rectClient.right < ptOffset.x)
				{
					break;
				}
			}

			if (0 == (nVert % 2))
				ptOffset.x = ptStart.x + m_Item_GridChess.nGridWidth;
			else
				ptOffset.x = ptStart.x;

			ptOffset.y += m_Item_GridChess.nGridHeight;

			if (rectClient.bottom < ptOffset.y)
			{
				break;
			}
		}
	}
	catch (CResourceException* e)
	{
		e->ReportError();
		e->Delete();
	}
}

//=============================================================================
// Method		: SetChartType
// Access		: public  
// Returns		: void
// Parameter	: __in enChartType nChartType
// Qualifier	:
// Last Update	: 2017/3/14 - 17:21
// Desc.		:
//=============================================================================
void CWnd_Chart::SetChartType(__in enChartType nChartType)
{
	m_nChartType = nChartType;

	UpdateChart();
}

//=============================================================================
// Method		: SetChart_GridChess
// Access		: public  
// Returns		: void
// Parameter	: __in CRect rtPosition
// Parameter	: __in UINT nGridWidth
// Parameter	: __in UINT nGridHeight
// Parameter	: __in COLORREF bFstColor
// Parameter	: __in COLORREF b2ndColor
// Qualifier	:
// Last Update	: 2017/3/14 - 18:54
// Desc.		:
//=============================================================================
void CWnd_Chart::SetChart_GridChess(/*__in CRect rtPosition, */__in UINT nGridWidth, __in UINT nGridHeight, __in COLORREF bFstColor /*= RGB(0, 0, 0)*/, __in COLORREF b2ndColor /*= RGB(255, 255, 255)*/)
{
	//m_Item_GridChess.rtPosition		= rtPosition;
	m_Item_GridChess.nGridWidth		= nGridWidth;
	m_Item_GridChess.nGridHeight	= nGridHeight;
	m_Item_GridChess.clrFirst		= bFstColor;
	m_Item_GridChess.clrSecond		= b2ndColor;
}

//=============================================================================
// Method		: SetChart_Crosshair
// Access		: public  
// Returns		: void
// Parameter	: __in float fCenterLineThickness
// Parameter	: __in COLORREF clrCenterLine
// Parameter	: __in float fLineThickness
// Parameter	: __in COLORREF clrLine
// Qualifier	:
// Last Update	: 2017/3/15 - 17:44
// Desc.		:
//=============================================================================
void CWnd_Chart::SetChart_Crosshair(__in float fCenterLineThickness, __in COLORREF clrCenterLine, __in float fLineThickness, __in COLORREF clrLine)
{
	m_Item_Crosshair.fCenterLineThickness	= fCenterLineThickness;
	m_Item_Crosshair.clrCenterLine			= clrCenterLine;

	m_Item_Crosshair.fLineThickness			= fLineThickness;
	m_Item_Crosshair.clrLine				= clrLine;
}

//=============================================================================
// Method		: RemoveAll_ChartCircle
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/14 - 17:45
// Desc.		:
//=============================================================================
void CWnd_Chart::RemoveAll_ChartCircle()
{
	m_Item_Circle.RemoveAll();
}

//=============================================================================
// Method		: Add_ChartCircle
// Access		: public  
// Returns		: void
// Parameter	: __in POINT ptCenter
// Parameter	: __in UINT nRadius
// Parameter	: __in COLORREF clrBack
// Qualifier	:
// Last Update	: 2017/3/15 - 13:44
// Desc.		:
//=============================================================================
void CWnd_Chart::Add_ChartCircle(__in POINT ptCenter, __in UINT nRadius, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	ST_ChartRect stCircle;

	stCircle.Rect.SetRect(ptCenter.x - nRadius, ptCenter.y - nRadius, ptCenter.x + nRadius, ptCenter.y + nRadius);
	stCircle.Color = clrBack;

	m_Item_Circle.Add(stCircle);
}

void CWnd_Chart::Add_ChartCircle(__in CRect rectCircle, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	ST_ChartRect stCircle;

	stCircle.Rect = rectCircle;
	stCircle.Color = clrBack;

	m_Item_Circle.Add(stCircle);
}

//=============================================================================
// Method		: Delete_ChartCircle
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nIdex
// Qualifier	:
// Last Update	: 2017/3/15 - 13:44
// Desc.		:
//=============================================================================
void CWnd_Chart::Delete_ChartCircle(__in UINT nIdex)
{
	INT_PTR iCount = m_Item_Circle.GetCount();

	if (nIdex < (UINT)iCount)
	{
		m_Item_Circle.RemoveAt(nIdex);
	}
}

//=============================================================================
// Method		: Modify_ChartCircle
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nIdex
// Parameter	: __in POINT ptCenter
// Parameter	: __in UINT nRadius
// Parameter	: __in COLORREF clrBack
// Qualifier	:
// Last Update	: 2017/3/15 - 13:44
// Desc.		:
//=============================================================================
void CWnd_Chart::Modify_ChartCircle(__in UINT nIdex, __in POINT ptCenter, __in UINT nRadius, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	INT_PTR iCount = m_Item_Circle.GetCount();

	if (nIdex < (UINT)iCount)
	{
		m_Item_Circle[nIdex].Rect.SetRect(ptCenter.x - nRadius, ptCenter.y - nRadius, ptCenter.x + nRadius, ptCenter.y + nRadius);
		m_Item_Circle[nIdex].Color = clrBack;
	}
}

void CWnd_Chart::Modify_ChartCircle(__in UINT nIdex, __in CRect rectCircle, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	INT_PTR iCount = m_Item_Circle.GetCount();

	if (nIdex < (UINT)iCount)
	{
		m_Item_Circle[nIdex].Rect = rectCircle;
		m_Item_Circle[nIdex].Color = clrBack;
	}
}

//=============================================================================
// Method		: RemoveAll_ChartRectangle
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/15 - 16:11
// Desc.		:
//=============================================================================
void CWnd_Chart::RemoveAll_ChartShape()
{
	m_Item_Shapes.RemoveAll();
}

//=============================================================================
// Method		: Add_ChartShape
// Access		: public  
// Returns		: void
// Parameter	: __in enShapeType Shape
// Parameter	: __in POINT ptCenter
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __in COLORREF clrBack
// Qualifier	:
// Last Update	: 2017/3/15 - 18:00
// Desc.		:
//=============================================================================
void CWnd_Chart::Add_ChartShape(__in enShapeType Shape, __in POINT ptCenter, __in UINT nWidth, __in UINT nHeight, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	ST_ChartRect stCircle;

	stCircle.Rect.SetRect(ptCenter.x - (nWidth / 2), ptCenter.y - (nHeight / 2), ptCenter.x + (nWidth / 2), ptCenter.y + (nHeight / 2));	
	stCircle.Color = clrBack;
	stCircle.ShapeType = Shape;

	m_Item_Shapes.Add(stCircle);
}

void CWnd_Chart::Add_ChartShape(__in enShapeType Shape, __in CRect rectRectangle, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	ST_ChartRect stCircle;

	stCircle.Rect = rectRectangle;
	stCircle.Color = clrBack;
	stCircle.ShapeType = Shape;

	m_Item_Shapes.Add(stCircle);
}

//=============================================================================
// Method		: Delete_ChartRectangle
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nIdex
// Qualifier	:
// Last Update	: 2017/3/15 - 16:14
// Desc.		:
//=============================================================================
void CWnd_Chart::Delete_ChartShape(__in UINT nIdex)
{
	INT_PTR iCount = m_Item_Shapes.GetCount();

	if (nIdex < (UINT)iCount)
	{
		m_Item_Shapes.RemoveAt(nIdex);
	}
}

//=============================================================================
// Method		: Modify_ChartShape
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nIdex
// Parameter	: __in enShapeType Shape
// Parameter	: __in POINT ptCenter
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __in COLORREF clrBack
// Qualifier	:
// Last Update	: 2017/3/15 - 17:58
// Desc.		:
//=============================================================================
void CWnd_Chart::Modify_ChartShape(__in UINT nIdex, __in enShapeType Shape, __in POINT ptCenter, __in UINT nWidth, __in UINT nHeight, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	INT_PTR iCount = m_Item_Shapes.GetCount();

	if (nIdex < (UINT)iCount)
	{
		m_Item_Shapes[nIdex].Rect.SetRect(ptCenter.x - (nWidth / 2), ptCenter.y - (nHeight / 2), ptCenter.x + (nWidth / 2), ptCenter.y + (nHeight / 2));
		m_Item_Shapes[nIdex].Color = clrBack;
		m_Item_Shapes[nIdex].ShapeType = Shape;
	}
}

void CWnd_Chart::Modify_ChartShape(__in UINT nIdex, __in enShapeType Shape, __in CRect rectRectangle, __in COLORREF clrBack /*= RGB(0, 0, 0)*/)
{
	INT_PTR iCount = m_Item_Shapes.GetCount();

	if (nIdex < (UINT)iCount)
	{
		m_Item_Shapes[nIdex].Rect = rectRectangle;
		m_Item_Shapes[nIdex].Color = clrBack;
		m_Item_Shapes[nIdex].ShapeType = Shape;
	}
}

//=============================================================================
// Method		: UpdateChart
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/14 - 17:21
// Desc.		:
//=============================================================================
void CWnd_Chart::UpdateChart()
{
	if (GetSafeHwnd())
	{
		Invalidate();

		//UpdateWindow();
	}
}





