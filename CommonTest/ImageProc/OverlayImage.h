//*****************************************************************************
// Filename	: 	OverlayImage.h
// Created	:	2017/3/16 - 10:13
// Modified	:	2017/3/16 - 10:13
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef OverlayImage_h__
#define OverlayImage_h__

#pragma once

//-----------------------------------------------------------------------------
// 투명 배경의 이미지 출력용
//-----------------------------------------------------------------------------
class COverlayImage
{
public:
	COverlayImage();
	~COverlayImage();

protected:

	CImage		m_imgOverlay;
	const COLORREF m_rgbBackground = RGB(255,0,255);

	UINT		m_nWidth;
	UINT		m_nHeight;

	void			MakeRectBitmap();
	inline HBITMAP	Detach() throw();
	void			DrawOverlay(CDC *pDC, CRect rcROI, CRect rcView);

public:

	void			SetDC		(__in CDC* pDC);



};

#endif // OverlayImage_h__
