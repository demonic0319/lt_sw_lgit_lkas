//*****************************************************************************
// Filename	: 	Wnd_Overlay.h
// Created	:	2017/3/20 - 15:59
// Modified	:	2017/3/20 - 15:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Overlay_h__
#define Wnd_Overlay_h__

#pragma once

//-----------------------------------------------------------------------------
// CWnd_Overlay
//-----------------------------------------------------------------------------
class CWnd_Overlay : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Overlay)

public:
	CWnd_Overlay();
	virtual ~CWnd_Overlay();

protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

	DECLARE_MESSAGE_MAP()


public:
	


};

#endif // Wnd_Overlay_h__


