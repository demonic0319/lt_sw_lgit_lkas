//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestStep.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestStep.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestStep.h"
#include "Def_TestItem_Cm.h"
#include "resource.h"
#include "File_Recipe_Cm.h"

// CWnd_Cfg_TestStep
#define IDC_LC_STEPLIST			1000
#define IDC_CB_TESTITEM			1001
#define IDC_CB_CANCTRL			1002
#define IDC_CB_CHART_IDX		1003
#define IDC_CB_RETRY_CNT		1004
#define IDC_CB_TEST_LOOP		1005
#define IDC_BN_STEPCTRL_S		1100
#define IDC_BN_STEPCTRL_E		IDC_BN_STEPCTRL_S + SC_MaxEnum - 1
#define IDC_CHK_STEPITEM_S		1200
#define IDC_CHK_STEPITEM_E		IDC_CHK_STEPITEM_S + SI_MaxEnum - 1
#define IDC_ED_STEPITEM_S		1300
#define IDC_ED_STEPITEM_E		IDC_ED_STEPITEM_S + SI_MaxEnum - 1
#define	IDC_BN_LOAD_XML			2000
#define	IDC_BN_SAVE_XML			2001
#define IDC_CB_CHART_CHECK		2002

static LPCTSTR g_szTestStep[] =
{
	_T("Retry Cnt"),		// SI_Retry				// 추가 재검사 횟수
	_T("Delay"),			// SI_Delay				// 스텝 끝난 후 딜레이
	_T("Test Item"),		// SI_Test,				// 테스트 검사 항목
	_T("Move Y"),			// SI_MoveY,			// 팔레트 전/후진	
	_T("Move X"),			// SI_MoveX,			// 팔레트 좌/우
	_T("Chart Index"),		// SI_ChartIdx,			// 챠트 번호
	_T("Chart Rotation"),	// SI_ChartRot,			// 챠트 회전
	_T("Chart Move X"),		// SI_Chart_Move_X,		// 차트 X
	_T("Chart Move Z"),		// SI_Chart_Move_Z,		// 차트 Z
	_T("Chart Tilt X"),		// SI_Chart_Tilt_X,		// 차트 Tilt X
	_T("Chart Tilt Z"),		// SI_Chart_Tilt_Z,		// 차트 Tilt Z
	_T("Alpha"),			// SI_Alpha,			// 알파
	_T("Alpha2"),			// SI_Alpha2nd,			// 알파2
	_T("Board Control"),	// SI_BoardCtrl
	_T("Test Loop"),		// SI_TestLoop
	_T("Chart Check"),		// SI_ChartCheckEnablezz
	NULL
};

static LPCTSTR g_szTestCtrl[] =
{
	_T("Add"),		// SC_Add
	_T("Insert"),	// SC_Insert
	_T("Remove"),	// SC_Remove
	_T("Up"),		// SC_Order_Up
	_T("Down"),		// SC_Order_Down
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_TestStep, CWnd_BaseView)

CWnd_Cfg_TestStep::CWnd_Cfg_TestStep()
{
	VERIFY(m_font_Default.CreateFont(
		22,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_Font.CreateFont(
		22,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_TestStep::~CWnd_Cfg_TestStep()
{
	m_font_Default.DeleteObject();
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_Cfg_TestStep, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_COMMAND_RANGE(IDC_CHK_STEPITEM_S,	IDC_CHK_STEPITEM_E,		OnBnClickedBnStepItem)
	ON_COMMAND_RANGE(IDC_BN_STEPCTRL_S,		IDC_BN_STEPCTRL_E,		OnBnClickedBnStepCtrl)
	ON_CBN_SELENDOK	(IDC_CB_TESTITEM, OnCbnSelendokTestItem)
	ON_BN_CLICKED	(IDC_BN_LOAD_XML, OnBnClickedBnLoadXML)
	ON_BN_CLICKED	(IDC_BN_SAVE_XML, OnBnClickedBnSaveXML)
END_MESSAGE_MAP()


// CWnd_Cfg_TestStep message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestStep::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// 스텝 리스트
	m_lc_StepList.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LC_STEPLIST);
	
	m_bn_StepCtrl[SC_Order_Up].SetImage(IDB_ARROW_UP, IDB_ARROW_UP);
	m_bn_StepCtrl[SC_Order_Down].SetImage(IDB_ARROW_DOWN, IDB_ARROW_DOWN);
	//m_bn_StepCtrl[SC_Order_Down].m_bRightImage = FALSE;
	//m_bn_StepCtrl[SC_Order_Down].m_bTopImage = FALSE;

	for (UINT nIdx = 0; nIdx < SC_MaxEnum; nIdx++)
	{
		m_bn_StepCtrl[nIdx].m_bTransparent = TRUE;
		m_bn_StepCtrl[nIdx].Create(g_szTestCtrl[nIdx], dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_STEPCTRL_S + nIdx);
		m_bn_StepCtrl[nIdx].SetMouseCursorHand();
	}

	// XML 불러오기/저장하기
	m_bn_LoadXML.m_bTransparent = TRUE;
	m_bn_LoadXML.Create(_T("Load XML"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_LOAD_XML);
	m_bn_LoadXML.SetMouseCursorHand();

	m_bn_SaveXML.m_bTransparent = TRUE;
	m_bn_SaveXML.Create(_T("Save XML"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_SAVE_XML);
	m_bn_SaveXML.SetMouseCursorHand();

	// 검사 항목
	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CB_TESTITEM);
	m_cb_TestItem.SetFont(&m_Font);
	Set_TestItem();

	// CAN 제어
	m_cb_Board_Ctrl.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_CANCTRL);
	m_cb_Board_Ctrl.SetFont(&m_Font);
	for (UINT nIdx = 0; nIdx < BrdCtrl_MaxEnum; nIdx++)
	{
		m_cb_Board_Ctrl.AddString(g_szBoard_Ctrl[nIdx]);
	}
	m_cb_Board_Ctrl.SetCurSel(0);
	m_cb_Board_Ctrl.EnableWindow(FALSE);
	
	// Chart Index
	m_cb_ChartIdx.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_CHART_IDX);
	m_cb_ChartIdx.SetFont(&m_Font);
	CString szText;
	for (UINT nIdx = 0; nIdx <= 3; nIdx++)
	{
		szText.Format(_T("%d"), nIdx);
		m_cb_ChartIdx.AddString(szText);
	}
	m_cb_ChartIdx.SetCurSel(0);
	m_cb_ChartIdx.EnableWindow(FALSE);

	// Chart Check
	m_cb_ChartCheck.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_CHART_CHECK);
	m_cb_ChartCheck.SetFont(&m_Font);
	for (UINT nIdx = 0; nIdx < TestLoop_MaxEnum; nIdx++)
	{
		m_cb_ChartCheck.AddString(g_szTestLoop[nIdx]);
	}
	m_cb_ChartCheck.SetCurSel(0);
	m_cb_ChartCheck.EnableWindow(FALSE);

	// Retry Count
	m_cb_RetryCnt.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_RETRY_CNT);
	m_cb_RetryCnt.SetFont(&m_Font);
	for (UINT nIdx = 0; nIdx <= 5; nIdx++)
	{
		szText.Format(_T("%d"), nIdx);
		m_cb_RetryCnt.AddString(szText);
	}
	m_cb_RetryCnt.SetCurSel(0);
	m_cb_RetryCnt.EnableWindow(FALSE);
	
	// Test Loop
	m_cb_TestLoop.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_TEST_LOOP);
	m_cb_TestLoop.SetFont(&m_Font);
	for (UINT nIdx = 0; nIdx < TestLoop_MaxEnum; nIdx++)
	{
		m_cb_TestLoop.AddString(g_szTestLoop[nIdx]);
	}
	m_cb_TestLoop.SetCurSel(1);
	m_cb_TestLoop.EnableWindow(FALSE);

	// Step 항목 조건
	for (UINT nIdx = 0; nIdx < SI_MaxEnum; nIdx++)
	{
		m_chk_StepItem[nIdx].Create(g_szTestStep[nIdx], dwStyle | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_STEPITEM_S + nIdx);
		m_ed_StepItem[nIdx].Create(dwStyle | ES_CENTER | WS_BORDER, rectDummy, this, IDC_ED_STEPITEM_S + nIdx);

		m_chk_StepItem[nIdx].SetMouseCursorHand();
		m_chk_StepItem[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_StepItem[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_StepItem[nIdx].SizeToContent();

		m_ed_StepItem[nIdx].EnableWindow(FALSE);
		m_ed_StepItem[nIdx].SetFont(&m_font_Default);

		m_ed_StepItem[nIdx].SetWindowText(_T("0"));
	}
	
	m_ed_StepItem[SI_Delay].SetValidChars(_T("0123456789"));
	m_ed_StepItem[SI_MoveY].EnableMask(_T("DDD"), _T("___"));
	m_ed_StepItem[SI_MoveY].SetValidChars(_T("0123456789"));
	m_ed_StepItem[SI_MoveX].SetValidChars(_T("0123456789-"));
	m_ed_StepItem[SI_ChartRot].EnableMask(_T("*DDD"), _T("____"));
	m_ed_StepItem[SI_Chart_Move_X].SetValidChars(_T("0123456789-"));
	m_ed_StepItem[SI_Chart_Tilt_X].SetValidChars(_T("0123456789-"));
	m_ed_StepItem[SI_Chart_Tilt_Z].SetValidChars(_T("0123456789-"));
	m_ed_StepItem[SI_Alpha].SetValidChars(_T("0123456789"));
	m_ed_StepItem[SI_Alpha2nd].SetValidChars(_T("0123456789"));
	m_ed_StepItem[SI_ChartRot].EnableMask(_T("*DD"), _T("___"));
	m_ed_StepItem[SI_ChartRot].SetValidChars(_T("0123456789-"));

	if ((Sys_2D_Cal == m_InspectionType) || (Sys_3D_Cal == m_InspectionType))
	{
		m_cb_TestItem.EnableWindow(FALSE);
	}
	else
	{
		m_chk_StepItem[SI_Test].SetCheck(BST_CHECKED);
	}

	if (Sys_Image_Test == m_InspectionType)
	{
		m_chk_StepItem[SI_MoveY].EnableWindow(FALSE);
	}

	if (Sys_IR_Image_Test == m_InspectionType)
	{
		m_chk_StepItem[SI_ChartIdx].EnableWindow(FALSE);
		m_cb_ChartIdx.EnableWindow(FALSE);
		m_chk_StepItem[SI_Chart_Check].EnableWindow(FALSE);
		m_cb_ChartCheck.EnableWindow(FALSE);
	
		m_chk_StepItem[SI_MoveY].SetCheck(BST_UNCHECKED);
		m_chk_StepItem[SI_MoveY].EnableWindow(FALSE);
		m_ed_StepItem[SI_MoveY].EnableWindow(FALSE);
	}

	if (Sys_Focusing == m_InspectionType)
	{
		m_chk_StepItem[SI_TestLoop].EnableWindow(FALSE);
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/24 - 16:23
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iCateSpacing= 10;
	int iMagin		= 10;
	int iLeft		= iMagin;
	int iTop		= iMagin;
	int iWidth		= cx - (iMagin * 2);
	int iHeight		= cy - (iMagin * 2);
	int iHalfWidth	= (iWidth - iCateSpacing) / 3;
	int iHalfHeight	= ((iHeight - iCateSpacing) * 2 / 3);
	int iCapWidth	= iWidth / 5;
	//int iValWidth	= iHalfWidth - iCapWidth - iSpacing;
	int iValWidth	= 0;
	//int iCtrlHeight	= (iHeight - iHalfHeight - iCateSpacing - (iSpacing * (SC_MaxEnum - 1))) / SC_MaxEnum;
	int iCtrlHeight	= (iHeight - iHalfHeight - iCateSpacing - (iSpacing * (SC_MaxEnum + 2))) / (SC_MaxEnum + 3);
	int iLeftSub	= 0;

	// 스텝 리스트 -------------------------------------------------------------
	m_lc_StepList.MoveWindow(iLeft, iTop, iWidth, iHalfHeight);

	// 스텝 추가/삭제/이동 -----------------------------------------------------
	iLeft = iMagin;
	iLeftSub = iLeft + iCapWidth + iSpacing;
	iTop += iHalfHeight + iCateSpacing;

	iCapWidth = max(iCapWidth, 140);
	for (UINT nIdx = 0; nIdx < SC_MaxEnum; nIdx++)
	{
		m_bn_StepCtrl[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
	}
	
	// XML 불러오기/저장하기
	iTop = cy - iMagin - iCtrlHeight;
	iValWidth = (iCapWidth - iSpacing) / 2;
	m_bn_LoadXML.MoveWindow(iLeft, iTop, iValWidth, iCtrlHeight);
	iLeft += iValWidth + iSpacing;
	m_bn_SaveXML.MoveWindow(iLeft, iTop, iValWidth, iCtrlHeight);

	// 스텝 설정항목 -----------------------------------------------------------
	// 검사 항목
	if (Sys_Focusing == m_InspectionType)
	{
		iLeft += iHalfWidth;
		iLeftSub = iLeft + iCapWidth + iSpacing;
	}
	else
	{
		iHalfWidth	= (iWidth - iCateSpacing) / 2;
		iLeft += iCapWidth + iCateSpacing + iCateSpacing;

		iCapWidth = iHalfWidth - iLeft;
		iCapWidth = max(iCapWidth, 120);
		iLeftSub = iLeft + iCapWidth + iSpacing;
	}

	iTop = iMagin + iHalfHeight + iCateSpacing;
	iValWidth = iWidth - iLeftSub + iMagin;

	m_chk_StepItem[SI_Retry].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_RetryCnt.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_chk_StepItem[SI_Delay].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_ed_StepItem[SI_Delay].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_chk_StepItem[SI_Test].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_TestItem.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);	
	//m_ed_StepItem[SI_Test].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);

	// Step 항목 조건
	iTop += iCtrlHeight + iSpacing;

	// 	SI_MoveY
	if ((Sys_2D_Cal == m_InspectionType) || (Sys_3D_Cal == m_InspectionType) /*|| (Sys_IR_Image_Test == m_InspectionType)*/ || (Sys_Image_Test == m_InspectionType))
	{
		m_chk_StepItem[SI_MoveY].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_StepItem[SI_MoveY].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
		iTop += iCtrlHeight + iSpacing;
	}
	else
	{
		m_chk_StepItem[SI_MoveY].MoveWindow(0, 0, 0, 0);
		m_ed_StepItem[SI_MoveY].MoveWindow(0, 0, 0, 0);
	}

	// 	SI_ChartIdx
// 	if (Sys_IR_Image_Test == m_InspectionType)
// 	{
// 		m_chk_StepItem[SI_ChartIdx].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 		m_cb_ChartIdx.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
// 		iTop += iCtrlHeight + iSpacing;
// 
// 		m_chk_StepItem[SI_Chart_Check].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 		m_cb_ChartCheck.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
// 		iTop += iCtrlHeight + iSpacing;
// 	}
// 	else
	{
		m_chk_StepItem[SI_ChartIdx].MoveWindow(0, 0, 0, 0);
		m_cb_ChartIdx.MoveWindow(0, 0, 0, 0);

		m_chk_StepItem[SI_Chart_Check].MoveWindow(0, 0, 0, 0);
		m_cb_ChartCheck.MoveWindow(0, 0, 0, 0);
	}

	// 	SI_ChartIdx
	if (Sys_2D_Cal == m_InspectionType)
	{
		m_chk_StepItem[SI_ChartRot].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_StepItem[SI_ChartRot].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
		iTop += iCtrlHeight + iSpacing;

		m_chk_StepItem[SI_MoveX].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_StepItem[SI_MoveX].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
		iTop += iCtrlHeight + iSpacing;
	}
	else
	{
		m_chk_StepItem[SI_ChartRot].MoveWindow(0, 0, 0, 0);
		m_ed_StepItem[SI_ChartRot].MoveWindow(0, 0, 0, 0);

		m_chk_StepItem[SI_MoveX].MoveWindow(0, 0, 0, 0);
		m_ed_StepItem[SI_MoveX].MoveWindow(0, 0, 0, 0);
	}

	// 	SI_DummyHandMove
	if (Sys_3D_Cal == m_InspectionType)
	{
		m_chk_StepItem[SI_Alpha].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_StepItem[SI_Alpha].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
		iTop += iCtrlHeight + iSpacing;

		m_chk_StepItem[SI_Alpha2nd].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_StepItem[SI_Alpha2nd].MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
		iTop += iCtrlHeight + iSpacing;
	}
	else
	{
		m_chk_StepItem[SI_Alpha].MoveWindow(0, 0, 0, 0);
		m_ed_StepItem[SI_Alpha].MoveWindow(0, 0, 0, 0);

		m_chk_StepItem[SI_Alpha2nd].MoveWindow(0, 0, 0, 0);
		m_ed_StepItem[SI_Alpha2nd].MoveWindow(0, 0, 0, 0);
	}

	// SI_BoardCtrl
	m_chk_StepItem[SI_BoardCtrl].MoveWindow(0, 0, 0, 0);
	m_ed_StepItem[SI_BoardCtrl].MoveWindow(0, 0, 0, 0);

	// SI_TestLoop
	if (Sys_Focusing == m_InspectionType)
	{
		m_chk_StepItem[SI_TestLoop].MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_cb_TestLoop.MoveWindow(iLeftSub, iTop, iValWidth, iCtrlHeight);
		iTop += iCtrlHeight + iSpacing;
	}
	else
	{
		m_chk_StepItem[SI_TestLoop].MoveWindow(0, 0, 0, 0);
		m_cb_TestLoop.MoveWindow(0, 0, 0, 0);
	}

	m_chk_StepItem[SI_Chart_Move_X].MoveWindow(0, 0, 0, 0);
	m_ed_StepItem[SI_Chart_Move_X].MoveWindow(0, 0, 0, 0);

	m_chk_StepItem[SI_Chart_Move_Z].MoveWindow(0, 0, 0, 0);
	m_ed_StepItem[SI_Chart_Move_Z].MoveWindow(0, 0, 0, 0);

	m_chk_StepItem[SI_Chart_Tilt_X].MoveWindow(0, 0, 0, 0);
	m_ed_StepItem[SI_Chart_Tilt_X].MoveWindow(0, 0, 0, 0);

	m_chk_StepItem[SI_Chart_Tilt_Z].MoveWindow(0, 0, 0, 0);
	m_ed_StepItem[SI_Chart_Tilt_Z].MoveWindow(0, 0, 0, 0);
}

//=============================================================================
// Method		: OnBnClickedBnStepItem
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnBnClickedBnStepItem(UINT nID)
{
	enStepItem nIDIdx = (enStepItem)(nID - IDC_CHK_STEPITEM_S);
	
	BOOL bEnable = TRUE;

	if (BST_CHECKED == m_chk_StepItem[nIDIdx].GetCheck())
	{
		bEnable = TRUE;
	}
	else
	{
		bEnable = FALSE;
	}
	
	m_ed_StepItem[nIDIdx].EnableWindow(bEnable);

	switch (nIDIdx)
	{
	case CWnd_Cfg_TestStep::SI_Retry:
		if (FALSE == bEnable)
			m_cb_RetryCnt.SetCurSel(0);
		m_cb_RetryCnt.EnableWindow(bEnable);
		break;

	case CWnd_Cfg_TestStep::SI_Test:
		m_cb_TestItem.EnableWindow(bEnable);
		break;

	case CWnd_Cfg_TestStep::SI_MoveY:
		break;

	case CWnd_Cfg_TestStep::SI_MoveX:
		break;

	case CWnd_Cfg_TestStep::SI_ChartRot:
		break;

	case CWnd_Cfg_TestStep::SI_ChartIdx:
		if (FALSE == bEnable)
			m_cb_ChartIdx.SetCurSel(0);
		m_cb_ChartIdx.EnableWindow(bEnable);
		break;

	case CWnd_Cfg_TestStep::SI_Chart_Check:
		if (FALSE == bEnable)
			m_cb_ChartCheck.SetCurSel(0);
		m_cb_ChartCheck.EnableWindow(bEnable);
		break;

	case CWnd_Cfg_TestStep::SI_Chart_Move_X:
		break;

	case CWnd_Cfg_TestStep::SI_Chart_Move_Z:
		break;

	case CWnd_Cfg_TestStep::SI_Chart_Tilt_X:
		break;

	case CWnd_Cfg_TestStep::SI_Chart_Tilt_Z:
		break;

	case CWnd_Cfg_TestStep::SI_Alpha:
		if (FALSE == bEnable)
			m_ed_StepItem[SI_Alpha].SetWindowText(_T("0"));
		break;

	case CWnd_Cfg_TestStep::SI_Alpha2nd:
		if (FALSE == bEnable)
			m_ed_StepItem[SI_Alpha2nd].SetWindowText(_T("0"));
		break;

	case CWnd_Cfg_TestStep::SI_BoardCtrl:
	{
		if (FALSE == bEnable)
		{
			m_cb_Board_Ctrl.SetCurSel(BrdCtrl_NotUse);
		}

		m_cb_Board_Ctrl.EnableWindow(bEnable);
	}

	break;

	case CWnd_Cfg_TestStep::SI_TestLoop:
	{
		if (FALSE == bEnable)
		{
			m_cb_TestLoop.SetCurSel(TestLoop_NotUse);
		}

		m_cb_TestLoop.EnableWindow(bEnable);
	}
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnStepCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/9/25 - 9:51
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnBnClickedBnStepCtrl(UINT nID)
{
	enStepCtrl nIDIdx = (enStepCtrl)(nID - IDC_BN_STEPCTRL_S);

	switch (nIDIdx)
	{
	case CWnd_Cfg_TestStep::SC_Add:
		Item_Add();
		break;

	case CWnd_Cfg_TestStep::SC_Insert:
		Item_Insert();
		break;

	case CWnd_Cfg_TestStep::SC_Remove:
		Item_Remove();
		break;

	case CWnd_Cfg_TestStep::SC_Order_Up:
		Item_Up();
		break;

	case CWnd_Cfg_TestStep::SC_Order_Down:
		Item_Down();
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnCbnSelendokTestItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/12/4 - 11:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnCbnSelendokTestItem()
{
	int iSel = m_cb_TestItem.GetCurSel();

	if (0 <= iSel)
	{
		switch (m_InspectionType)
		{
		case Sys_Focusing:
			switch (iSel)
			{
			case TI_Foc_Motion_LockingScrew:
			case TI_Foc_Motion_ReleaseScrew:
			case TI_Foc_Fn_PreFocus:
			case TI_Foc_Fn_ActiveAlign:
			case TI_Foc_Fn_OpticalCenter:
			case TI_Foc_Fn_Rotation:
			case TI_Foc_Fn_SFR:
				m_chk_StepItem[SI_TestLoop].SetCheck(BST_CHECKED);
				m_cb_TestLoop.EnableWindow(TRUE);
				break;
			default:
				m_chk_StepItem[SI_TestLoop].SetCheck(BST_UNCHECKED);
				m_cb_TestLoop.EnableWindow(FALSE);
				break;
			}
			break;

		case Sys_2D_Cal:
			break;

		case Sys_Image_Test:
			break;

		case Sys_IR_Image_Test:
			switch (iSel)
			{
// 			case TI_IR_ImgT_Motion_Chart_Index:
// 				m_chk_StepItem[SI_MoveY].SetCheck(BST_CHECKED);
// 				m_chk_StepItem[SI_MoveY].EnableWindow(TRUE);
// 				m_ed_StepItem[SI_MoveY].EnableWindow(TRUE);
// 
// 				m_chk_StepItem[SI_ChartIdx].SetCheck(BST_CHECKED);
// 				m_chk_StepItem[SI_ChartIdx].EnableWindow(TRUE);
// 				m_cb_ChartIdx.EnableWindow(TRUE);
// 
// 				m_chk_StepItem[SI_Chart_Check].SetCheck(BST_CHECKED);
// 				m_chk_StepItem[SI_Chart_Check].EnableWindow(TRUE);
// 				m_cb_ChartCheck.EnableWindow(TRUE);
// 				break;

			default:
				m_chk_StepItem[SI_ChartIdx].SetCheck(BST_UNCHECKED);
				m_chk_StepItem[SI_ChartIdx].EnableWindow(FALSE);
				m_cb_ChartIdx.EnableWindow(FALSE);

				m_chk_StepItem[SI_Chart_Check].SetCheck(BST_UNCHECKED);
				m_chk_StepItem[SI_Chart_Check].EnableWindow(FALSE);
				m_cb_ChartCheck.EnableWindow(FALSE);

				m_chk_StepItem[SI_MoveY].SetCheck(BST_UNCHECKED);
				m_chk_StepItem[SI_MoveY].EnableWindow(FALSE);
				m_ed_StepItem[SI_MoveY].EnableWindow(FALSE);
				break;
			}
			break;


		case Sys_3D_Cal:
			switch (iSel)
			{
			case TI_3D_Depth_Alpha_Capture:
			case TI_3D_Eval_Alpha_Capture:
				m_chk_StepItem[SI_Alpha].SetCheck(BST_CHECKED);
				m_ed_StepItem[SI_Alpha].EnableWindow(TRUE);
				m_chk_StepItem[SI_Alpha2nd].SetCheck(BST_CHECKED);
				m_ed_StepItem[SI_Alpha2nd].EnableWindow(TRUE);
				break;

			case TI_3D_DummyHand:
				m_chk_StepItem[SI_Alpha].SetCheck(BST_UNCHECKED);
				m_ed_StepItem[SI_Alpha].EnableWindow(FALSE);
				m_chk_StepItem[SI_Alpha2nd].SetCheck(BST_UNCHECKED);
				m_ed_StepItem[SI_Alpha2nd].EnableWindow(FALSE);
				break;

			default:
				m_chk_StepItem[SI_Alpha].SetCheck(BST_UNCHECKED);
				m_ed_StepItem[SI_Alpha].EnableWindow(FALSE);
				m_chk_StepItem[SI_Alpha2nd].SetCheck(BST_UNCHECKED);
				m_ed_StepItem[SI_Alpha2nd].EnableWindow(FALSE);
				break;
			}
			break;

		default:
			break;
		}
	}
}

//=============================================================================
// Method		: OnBnClickedBnLoadXML
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/29 - 15:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnBnClickedBnLoadXML()
{
	LoadXML();
}

//=============================================================================
// Method		: OnBnClickedBnSaveXML
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/29 - 15:32
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::OnBnClickedBnSaveXML()
{
	SaveXML();
}

//=============================================================================
// Method		: Set_TestItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/25 - 20:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Set_TestItem()
{
	UINT		nMaxCount = 0;
	LPCTSTR*	pArString = NULL;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		nMaxCount = TI_Foc_MaxEnum;
		pArString = g_szTestItem_Focusing;
		break;

	case Sys_2D_Cal:
		nMaxCount = TI_2D_MaxEnum;
		pArString = g_szTestItem_2D_CAL;
		break;

	case Sys_Image_Test:
		nMaxCount = TI_ImgT_MaxEnum;
		pArString = g_szTestItem_ImgT;
		break;

	case Sys_IR_Image_Test:
		nMaxCount = TI_IR_ImgT_MaxEnum;
		pArString = g_szTestItem_IR_ImgT;
		break;

	case Sys_3D_Cal:
		nMaxCount = TI_3D_MaxEnum;
		pArString = g_szTestItem_3D_CAL;
		break;

	default:
		break;
	}

	for (UINT nIdx = 0; nIdx < nMaxCount; nIdx++)
	{
		m_cb_TestItem.AddString(pArString[nIdx]);
	}
	m_cb_TestItem.SetCurSel(0);
}

//=============================================================================
// Method		: GetTestStepData
// Access		: protected  
// Returns		: BOOL
// Parameter	: __out ST_StepUnit & stStepUnit
// Qualifier	:
// Last Update	: 2017/9/25 - 17:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestStep::GetTestStepData(__out ST_StepUnit& stStepUnit)
{
	CString		szText;

	// Retry Count
	if (BST_CHECKED == m_chk_StepItem[SI_Retry].GetCheck())
	{
		int iSel = m_cb_RetryCnt.GetCurSel();
		if (0 <= iSel)
		{
			stStepUnit.nRetryCnt = iSel;
		}
	}
	else
	{
		stStepUnit.nRetryCnt = 0;
	}

	// Delay
	if (BST_CHECKED == m_chk_StepItem[SI_Delay].GetCheck())
	{
		m_ed_StepItem[SI_Delay].GetWindowText(szText);

		stStepUnit.dwDelay = (DWORD)_ttol(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.dwDelay = 0;
	}

	// Test Item
	if (BST_CHECKED == m_chk_StepItem[SI_Test].GetCheck())
	{
		stStepUnit.bTest = TRUE;

		int iSel = m_cb_TestItem.GetCurSel();
		if (0 <= iSel)
		{
			stStepUnit.nTestItem = iSel;
		}
	}
	else
	{
		stStepUnit.bTest = FALSE;
		stStepUnit.nTestItem = 0;
	}

	// Move Z
	if (BST_CHECKED == m_chk_StepItem[SI_MoveY].GetCheck())
	{
		m_ed_StepItem[SI_MoveY].GetWindowText(szText);

		stStepUnit.bUseMoveY = TRUE;
		stStepUnit.nMoveY = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.bUseMoveY = FALSE;
		stStepUnit.nMoveY = 0;
	}

	// Move X
	if (BST_CHECKED == m_chk_StepItem[SI_MoveX].GetCheck())
	{
		m_ed_StepItem[SI_MoveX].GetWindowText(szText);

		stStepUnit.bUseMoveX = TRUE;
		stStepUnit.iMoveX = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.bUseMoveX = FALSE;
		stStepUnit.iMoveX = 0;
	}

	// Chart Rotation
	if (BST_CHECKED == m_chk_StepItem[SI_ChartRot].GetCheck())
	{
		stStepUnit.bUseChart_Rot = TRUE;

		m_ed_StepItem[SI_ChartRot].GetWindowText(szText);
		stStepUnit.dChart_Rot = _ttof(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.bUseChart_Rot = FALSE;
		stStepUnit.dChart_Rot = 0.0;
	}

	// Chart Index
	if (BST_CHECKED == m_chk_StepItem[SI_ChartIdx].GetCheck())
	{
		stStepUnit.bUseChart_Rot = TRUE;

		int iSel = m_cb_ChartIdx.GetCurSel();

		if (0 <= iSel)
		{
			stStepUnit.nChart_Idx = iSel;
		}
	}
	else
	{
		stStepUnit.bUseChart_Rot = FALSE;
		stStepUnit.nChart_Idx = 0;
	}

	// Chart Check
	if (BST_CHECKED == m_chk_StepItem[SI_Chart_Check].GetCheck())
	{
		stStepUnit.bUseChart_Rot = TRUE;

		int iSel = m_cb_ChartCheck.GetCurSel();

		if (0 <= iSel)
		{
			stStepUnit.bChart_Check = TRUE;
		}
	}
	else
	{
		stStepUnit.bUseChart_Rot = FALSE;
		stStepUnit.bChart_Check = FALSE;
	}

	// 차트 X
	if (BST_CHECKED == m_chk_StepItem[SI_Chart_Move_X].GetCheck())
	{
		m_ed_StepItem[SI_Chart_Move_X].GetWindowText(szText);

		stStepUnit.bUseChart_Move_X = TRUE;
		stStepUnit.iChart_Move_X = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.bUseChart_Move_X = FALSE;
		stStepUnit.iChart_Move_X = 0;
	}

	// 차트 Z
	if (BST_CHECKED == m_chk_StepItem[SI_Chart_Move_Z].GetCheck())
	{
		m_ed_StepItem[SI_Chart_Move_Z].GetWindowText(szText);

		stStepUnit.bUseChart_Move_Z = TRUE;
		stStepUnit.iChart_Move_Z = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.bUseChart_Move_Z = FALSE;
		stStepUnit.iChart_Move_Z = 0;
	}

	// 차트 Tilt X
	if (BST_CHECKED == m_chk_StepItem[SI_Chart_Tilt_X].GetCheck())
	{
		m_ed_StepItem[SI_Chart_Tilt_X].GetWindowText(szText);

		stStepUnit.bUseChart_Tilt_X = TRUE;
		stStepUnit.iChart_Tilt_X = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.bUseChart_Tilt_X = FALSE;
		stStepUnit.iChart_Tilt_X = 0;
	}

	// 차트 Tilt Z
	if (BST_CHECKED == m_chk_StepItem[SI_Chart_Tilt_Z].GetCheck())
	{
		m_ed_StepItem[SI_Chart_Tilt_Z].GetWindowText(szText);

		stStepUnit.bUseChart_Tilt_Z = TRUE;
		stStepUnit.iChart_Tilt_Z = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.bUseChart_Tilt_Z = FALSE;
		stStepUnit.iChart_Tilt_Z = 0;
	}

	// Alpha
	if (BST_CHECKED == m_chk_StepItem[SI_Alpha].GetCheck())
	{
		m_ed_StepItem[SI_Alpha].GetWindowText(szText);

		stStepUnit.wAlpha = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.wAlpha = 0;
	}

	// Alpha2
	if (BST_CHECKED == m_chk_StepItem[SI_Alpha2nd].GetCheck())
	{
		m_ed_StepItem[SI_Alpha2nd].GetWindowText(szText);

		stStepUnit.wAlpha_2nd = (UINT)_ttoi(szText.GetBuffer(0));
	}
	else
	{
		stStepUnit.wAlpha_2nd = 0;
	}

	// Board Control
	if (BST_CHECKED == m_chk_StepItem[SI_BoardCtrl].GetCheck())
	{
		int iSel = m_cb_Board_Ctrl.GetCurSel();
		if (0 <= iSel)
		{
			stStepUnit.nBoardCtrl = (enBoardCtrl)iSel;
		}
	}
	else
	{
		stStepUnit.nBoardCtrl = enBoardCtrl::BrdCtrl_NotUse;
	}


	// Test Loop
	if (BST_CHECKED == m_chk_StepItem[SI_TestLoop].GetCheck())
	{
		int iSel = m_cb_TestLoop.GetCurSel();
		if (0 <= iSel)
		{
			stStepUnit.nTestLoop = (enTestLoop)iSel;
		}
	}
	else
	{
		stStepUnit.nTestLoop = enTestLoop::TestLoop_NotUse;
	}


	return TRUE;
}

//=============================================================================
// Method		: Item_Add
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 10:25
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Add()
{
	ST_StepUnit stStepUnit;
	if (GetTestStepData(stStepUnit))
	{
		m_lc_StepList.Item_Add(stStepUnit);
	}
}

//=============================================================================
// Method		: Item_Insert
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Insert()
{
	ST_StepUnit stStepUnit;
	if (GetTestStepData(stStepUnit))
	{
		m_lc_StepList.Item_Insert(stStepUnit);
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Remove()
{
	m_lc_StepList.Item_Remove();	
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Up()
{
	m_lc_StepList.Item_Up();
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Item_Down()
{
	m_lc_StepList.Item_Down();
}

//=============================================================================
// Method		: LoadXML
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/29 - 15:28
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestStep::LoadXML()
{
	// 설정된 데이터가 삭제 됩니다.
	if (IDNO == AfxMessageBox(_T("The data being edited is deleted.\r\n Do you want to continue?"), MB_YESNO))
	{
		return TRUE;
	}

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt = _T("XML File (*.xml)| *.xml||");
	CString strFileSel;
	strFileSel = _T("*.xml");

	// 파일 불러오기
	CFileDialog fileDlg(TRUE, RECIPE_FILE_EXT, strFileSel, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, strFileExt);
	if (FALSE == m_szPath.IsEmpty())
	{
		fileDlg.m_ofn.lpstrInitialDir = m_szPath;
	}

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFileTitle = fileDlg.GetFileTitle();

		CFile_Recipe_Cm	FileRecipe;
		ST_StepInfo		stStepInfo;

		if (FileRecipe.LoadXML_StepInfo(strFullPath.GetBuffer(0), stStepInfo))
		{
			// UI에 세팅
			Set_StepInfo(&stStepInfo);
		}
		else
		{
			return FALSE;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveXML
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/29 - 15:28
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_TestStep::SaveXML()
{
	// UI상의 데이터 얻기
	ST_StepInfo stStepInfo;
	Get_StepInfo(stStepInfo);

	CString strFullPath;
	CString strFileTitle;
	CString strFileExt;
	strFileExt = _T("XML File (*.xml)| *.xml||");

	CFileDialog fileDlg(FALSE, RECIPE_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	if (FALSE == m_szPath.IsEmpty())
	{
		fileDlg.m_ofn.lpstrInitialDir = m_szPath;
	}

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFileTitle = fileDlg.GetFileTitle();

		// 저장	
		CFile_Recipe_Cm	FileRecipe;
		if (FALSE == FileRecipe.SaveXML_StepInfo(strFullPath, &stStepInfo, m_InspectionType))
		{
			AfxMessageBox(_T("Save Failed!!"), MB_SYSTEMMODAL);
			return FALSE;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:05
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;

	m_lc_StepList.SetSystemType(nSysType);
}

//=============================================================================
// Method		: Set_XMLFilePath
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szPath
// Qualifier	:
// Last Update	: 2018/3/29 - 15:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Set_XMLFilePath(__in LPCTSTR szPath)
{
	m_szPath = szPath;
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Set_StepInfo(__in const ST_StepInfo* pstInStepInfo)
{
	m_lc_StepList.Set_StepInfo(pstInStepInfo);
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:38
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Get_StepInfo(__out ST_StepInfo& stOutStepInfo)
{
	m_lc_StepList.Get_StepInfo(stOutStepInfo);
}

//=============================================================================
// Method		: Init_DefaultSet
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 20:56
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestStep::Init_DefaultSet()
{
	// 리스트 초기화
	m_lc_StepList.DeleteAllItems();

	// 체크 버튼 초기화
	for (UINT nIdx = 0; nIdx < SI_MaxEnum; nIdx++)
	{
		m_chk_StepItem[nIdx].SetCheck(BST_UNCHECKED);
		m_ed_StepItem[nIdx].EnableWindow(FALSE);
	}

	m_cb_TestItem.SetCurSel(0);
	m_cb_TestItem.EnableWindow(FALSE);

}
