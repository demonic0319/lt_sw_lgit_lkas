//*****************************************************************************
// Filename	: 	Def_T_Foc.h
// Created	:	2018/3/4 - 10:29
// Modified	:	2018/3/4 - 10:29
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_T_Foc_h__
#define Def_T_Foc_h__

#include <afxwin.h>

#pragma pack(push,1)

typedef enum enSpec_Foc
{
	
	Spec_Foc_Item_01,
	Spec_Foc_Item_02,
	Spec_Foc_Item_03,
	Spec_Foc_Item_04,
	Spec_Foc_Item_05,
	Spec_Foc_Item_06,
	Spec_Foc_Item_07,
	Spec_Foc_Item_08,
	Spec_Foc_Item_09,
	Spec_Foc_Item_10,
	Spec_Foc_Item_11,
	Spec_Foc_Item_12,
	Spec_Foc_MaxNum
};

static LPCTSTR	g_szSpecFoc[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef struct _tag_Foc_Result
{
	_tag_Foc_Result()
	{
		Reset();
	};

	_tag_Foc_Result& operator= (const _tag_Foc_Result& ref)
	{
		return *this;
	};

	void Reset()
	{
	};

}ST_Foc_Result, *PST_Foc_Result;

typedef struct _tag_Foc_Spec
{
	_tag_Foc_Spec()
	{
		Reset();
	};

	_tag_Foc_Spec& operator= (const _tag_Foc_Spec& ref)
	{

		return *this;
	};

	void Reset()
	{

	};

}ST_Foc_Spec, *PST_Foc_Spec;

typedef struct _tag_Foc_Info
{
	_tag_Foc_Info()
	{
		Reset();
	};

	_tag_Foc_Info& operator= (const _tag_Foc_Info& ref)
	{

		return *this;
	};

	void Reset()
	{

	};

}ST_Foc_Info, *PST_Foc_Info;

#pragma pack(pop)

#endif // Def_T_Foc_h__
