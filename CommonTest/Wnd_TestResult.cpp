﻿// Wnd_TestResult.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult.h"

// CWnd_TestResult
typedef enum TestResultID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult, CWnd)

CWnd_TestResult::CWnd_TestResult()
{
	m_InspectionType = Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult::~CWnd_TestResult()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
END_MESSAGE_MAP()

// CWnd_TestResult 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResultStatic[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_default.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_default.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_default.SetFont_Gdip(L"Arial", 9.0F);
	m_st_default.Create(_T("TEST RESULT"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

// 	m_List_CenterPoint.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM);
// 	m_List_Current.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 1);
// 	m_List_Distortion.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 2);
// 	m_List_Dynamic.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 3);
// 	m_List_Rotate.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 4);
// 	m_List_SFR.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 5);
// 	m_List_SNR_BW.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 6);
// 	m_List_Tilt.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 7);
// 	m_List_FOV.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 8);
// 	m_List_SNR_IQ.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 9);
// 	m_List_DefectPixel.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 10);
// 	m_List_Shading.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 11);
// 	m_List_SNR_Light.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 12);
// 	m_List_FPN.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LIST_ITEM + 13);
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 25;

	m_st_Item[STI_TR_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);
	
	iTop += iStHeight + 2;

	m_st_default.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);

	MoveWindow_Result(iLeft, iTop, iWidth, iHeight - iTop);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetShowWindowResult
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 10:59
// Desc.		:
//=============================================================================
void CWnd_TestResult::SetShowWindowResult(int iItem)
{
	m_st_default.ShowWindow(SW_HIDE);
// 	m_List_CenterPoint.ShowWindow(SW_HIDE);
// 	m_List_Current.ShowWindow(SW_HIDE);
// 	m_List_Distortion.ShowWindow(SW_HIDE);
// 	m_List_Dynamic.ShowWindow(SW_HIDE);
// 	m_List_Rotate.ShowWindow(SW_HIDE);
// 	m_List_SFR.ShowWindow(SW_HIDE);
// 	m_List_SNR_BW.ShowWindow(SW_HIDE);
// 	m_List_Tilt.ShowWindow(SW_HIDE);
// 	m_List_FOV.ShowWindow(SW_HIDE);
// 	m_List_SNR_IQ.ShowWindow(SW_HIDE);
// 	m_List_DefectPixel.ShowWindow(SW_HIDE);
// 	m_List_Shading.ShowWindow(SW_HIDE);
// 	m_List_SNR_Light.ShowWindow(SW_HIDE);
// 	m_List_FPN.ShowWindow(SW_HIDE);
	
	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_IR_Image_Test:

		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}

}

//=============================================================================
// Method		: MoveWindow_Result
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Qualifier	:
// Last Update	: 2017/10/21 - 11:16
// Desc.		:
//=============================================================================
void CWnd_TestResult::MoveWindow_Result(int x, int y, int nWidth, int nHeight)
{
	int iHeaderH = 40;
	int iListH = iHeaderH + 3 * 20;

// 	m_List_CenterPoint.MoveWindow(x, y, nWidth, iListH);
// 	m_List_CenterPoint.ShowWindow(SW_SHOW);
// 
// 	m_List_Current.MoveWindow(x, y, nWidth, iListH);
// 	m_List_Current.ShowWindow(SW_HIDE);
// 
// 	m_List_Distortion.MoveWindow(x, y, nWidth, iListH);
// 	m_List_Distortion.ShowWindow(SW_HIDE);
// 
// 	m_List_Dynamic.MoveWindow(x, y, nWidth, iListH);
// 	m_List_Dynamic.ShowWindow(SW_HIDE);
// 
// 	m_List_Rotate.MoveWindow(x, y, nWidth, iListH);
// 	m_List_Rotate.ShowWindow(SW_HIDE);
// 
// 	m_List_SFR.MoveWindow(x, y, nWidth, iListH);
// 	m_List_SFR.ShowWindow(SW_HIDE);
// 
// 	m_List_SNR_BW.MoveWindow(x, y, nWidth, iListH);
// 	m_List_SNR_BW.ShowWindow(SW_HIDE);
// 
// 	m_List_Tilt.MoveWindow(x, y, nWidth, iListH);
// 	m_List_Tilt.ShowWindow(SW_HIDE);
// 
// 	m_List_FOV.MoveWindow(x, y, nWidth, iListH);
// 	m_List_FOV.ShowWindow(SW_HIDE);
// 
// 	m_List_SNR_IQ.MoveWindow(x, y, nWidth, iListH);
// 	m_List_SNR_IQ.ShowWindow(SW_HIDE);
// 
// 	m_List_DefectPixel.MoveWindow(x, y, nWidth, iListH);
// 	m_List_DefectPixel.ShowWindow(SW_HIDE);
// 
// 	m_List_Shading.MoveWindow(x, y, nWidth, iListH);
// 	m_List_Shading.ShowWindow(SW_HIDE);
// 
// 	m_List_SNR_Light.MoveWindow(x, y, nWidth, iListH);
// 	m_List_SNR_Light.ShowWindow(SW_HIDE);
// 
// 	m_List_FPN.MoveWindow(x, y, nWidth, iListH);
// 	m_List_FPN.ShowWindow(SW_HIDE);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (SW_HIDE == bShow)
	{
		SetShowWindowResult(-1);
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetTestItemResult
// Access		: public  
// Returns		: void
// Parameter	: __in  int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 10:45
// Desc.		:
//=============================================================================
void CWnd_TestResult::SetTestItem(__in int iTestItem)
{
	SetShowWindowResult(iTestItem);
}

//=============================================================================
// Method		: SetUpdateResult
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Parameter	: __in int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 12:44
// Desc.		:
//=============================================================================
void CWnd_TestResult::SetUpdateResult(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem)
{
	if (pstRecipeInfo == NULL)
		return;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;
	case Sys_2D_Cal:
		break;
	case Sys_Image_Test:
		break;
	case Sys_IR_Image_Test:
		break;
	case Sys_3D_Cal:
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 14:23
// Desc.		:
//=============================================================================
void CWnd_TestResult::SetUpdateClear()
{
// 	m_List_CenterPoint.SetRectClear();
// 	m_List_Current.SetRectClear();
// 	m_List_Distortion.SetRectClear();
// 	m_List_Dynamic.SetRectClear();
// 	m_List_Rotate.SetRectClear();
// 	m_List_SFR.SetRectClear();
// 	m_List_SNR_BW.SetRectClear();
// 	m_List_Tilt.SetRectClear();
// 	m_List_FOV.SetRectClear();
// 	m_List_SNR_IQ.SetRectClear();
// 	m_List_DefectPixel.SetRectClear();
// 	m_List_Shading.SetRectClear();
// 	m_List_SNR_Light.SetRectClear();
// 	m_List_FPN.SetRectClear();
}
