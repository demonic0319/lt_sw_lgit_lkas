#ifndef Def_T_2DCal_h__
#define Def_T_2DCal_h__

#include <afxwin.h>
#include "Def_T_LGE_Common.h"
#include "Def_Test_Cm.h"
#include "Def_Enum_Cm.h"

#pragma pack(push,1)

#define		MAX_2DCAL_ROI				15
#define		MAX_2DCAL_Corner_Point		40
#define		MAX_2DCAL_Re_KK				4
#define		MAX_2DCAL_Re_Kc				5
#define		MAX_2DCAL_arcDate			6
#define		MAX_2DCAL_Position			MAX_2DCAL_ROI

#define		FNAME_CORNERPT				_T("%s\\CornerPT_%d.csv")
#define		FNAME_BIN_CORNERPT			_T("%s\\CornerPT_%d.bin")

typedef enum enParam2DCal_Old
{
	Param_2DCal_ImgW = 0,			 // nImgWidthFull
	Param_2DCal_ImgH,				 // nImgHeightFull
	Param_2DCal_CornerX,			 // nNumofCornerX
	Param_2DCal_CornerY,			 // nNumofCornerY	
	Param_2DCal_CornerRadius,		 // nCornerDetectWindowRadius
	Param_2DCal_CornerSize,			 // nCornerDetectWindowSize
	Param_2DCal_BinNum,				 // nBinNum
	Param_2DCal_GaussiLength,		 // nGaussianFilterLength
	Param_2DCal_SubpixRadius,		 // nSubpixWindowRadius
	Param_2DCal_SubpixWidth,		 // nSubpixWindowSizeWidth
	Param_2DCal_SubpixHeight,		 // nSubpixWindowSizeHeight
	Param_2DCal_SubpixRectSize,		 // nSubpixWindowRectSize
	Param_2DCal_UseOpenCV,			 // nUseOpenCV
	Param_2DCal_Old_MaxNum,
};

static LPCTSTR	g_Param_2DCal_Old[] =
{
	_T("Img Width"),
	_T("Img Height"),
	_T("Num of Corner X"),
	_T("Num of Corner Y"),
	_T("Corner Detect Radius"),
	_T("Corner Detect Size"),
	_T("BinNum"),
	_T("Gaussian Filter Length"),
	_T("Subpix Radius"),
	_T("Subpix Width"),
	_T("Subpix Height"),
	_T("Subpix Rect Size"),
	_T("Use OpenCV"),
	NULL
};

typedef enum enParam2DCal
{
	Param_2DCal_nImgWidth,
	Param_2DCal_nImgHeight,
	Param_2DCal_nNumofCornerX,
	Param_2DCal_nNumofCornerY,
	Param_2DCal_fPatternSizeX,
	Param_2DCal_fPatternSizeY,
	Param_2DCal_fRepThres,
	Param_2DCal_nMinNumImages,
	Param_2DCal_nMinNumValidPnts,
	Param_2DCal_fMinOriginOffset,
	Param_2DCal_fMaxOriginOffset,
	Param_2DCal_fMinFocalLength,
	Param_2DCal_fMaxFocalLength,
	Param_2DCal_arfKK_0,
	Param_2DCal_arfKK_1,
	Param_2DCal_arfKK_2,
	Param_2DCal_arfKK_3,
	Param_2DCal_arfKc_0,
	Param_2DCal_arfKc_1,
	Param_2DCal_arfKc_2,
	Param_2DCal_arfKc_3,
	Param_2DCal_arfKc_4,
	Param_2DCal_fR2Max,
	Param_2DCal_fVersion,

	Param_2DCal_MaxNum,
};

static LPCTSTR	g_Param_2DCal[] =
{
	_T("nImgWidth"),
	_T("nImgHeight"),
	_T("nNumofCornerX"),	// cols of chess corner
	_T("nNumofCornerY"),	// rows of chess corner
	_T("fPatternSizeX"),
	_T("fPatternSizeY"),
	_T("fRepThres"),		// Threshold of reprojection error
	_T("nMinNumImages"),	// Minimum number of corner images
	_T("nMinNumValidPnts"),	// threshold of number of inlier points
	_T("fMinOriginOffset"),	// maximum range of origin difference between real production stage and theoretical
	_T("fMaxOriginOffset"),	// minimum range of origin difference between real production stage and theoretical
	_T("fMinFocalLength"),	// maximum range of focal length
	_T("fMaxFocalLength"),	// minimum range of focal length
	_T("arfKK_0"),			// Fu
	_T("arfKK_1"),			// Fv
	_T("arfKK_2"),			// Pu
	_T("arfKK_3"),			// Pv
	_T("arfKc_0"),			// Disortion Coef
	_T("arfKc_1"),
	_T("arfKc_2"),
	_T("arfKc_3"),
	_T("arfKc_4"),
	_T("fR2Max"),
	_T("fVersion"),
	
	NULL
};

typedef enum enSpec_2DCal
{
	Spec_2DCal_Kk_0 = 0,		// arfKK[MAX_2DCAL_Re_KK];
	Spec_2DCal_Kk_1,			// arfKK[MAX_2DCAL_Re_KK];
	Spec_2DCal_Kk_2,			// arfKK[MAX_2DCAL_Re_KK];
	Spec_2DCal_Kk_3,			// arfKK[MAX_2DCAL_Re_KK];
	Spec_2DCal_Kc_4,			// arfKc[MAX_2DCAL_Re_Kc];
	Spec_2DCal_R2Max,			// fR2Max
	Spec_2DCal_RepErr,			// fRepError
	Spec_2DCal_EvalErr,			// fEvalError
	Spec_2DCal_OrgOffset,		// fOrgOffset
	Spec_2DCal_FailureCnt,		// nDetectionFailureCnt
	Spec_2DCal_InvalidCnt,		// nInvalidPointCnt
	Spec_2DCal_ValidCnt,		// nValidPointCnt
	Spec_2DCal_MaxNum
};

static LPCTSTR	g_szSpec2DCal[] =
{
	_T("Focal Length U"),		//_T("KK [0]"),
	_T("Focal Length V"),		//_T("KK [1]"),
	_T("Principal point U"),	//_T("KK [2]"),
	_T("Principal point V"),	//_T("KK [3]"),
	_T("Kc [4]"),
	_T("Intrinsic Fov"),		//_T("R2 Max"),
	_T("Reprojection Error"),	//_T("Rep Error"),
	_T("Eval Error"),
	_T("Org Offset"),
	_T("Detection Failure Cnt"),
	_T("Invalid Point Cnt"),
	_T("Valid Point Cnt"),
	NULL
};

//-------------------------------------------------------------------
// 2D CAL ROI 정보
//-------------------------------------------------------------------
typedef struct _tag_2DCal_ROI : public ST_LGE_ROI
{
	UINT	nKeyMoveY;
	INT		nKeyChartRot;

	_tag_2DCal_ROI()
	{
		nKeyMoveY		= 0;
		nKeyChartRot	= 0;
	};

	_tag_2DCal_ROI& operator= (const _tag_2DCal_ROI& ref)
	{
		nKeyMoveY		= ref.nKeyMoveY;
		nKeyChartRot	= ref.nKeyChartRot;

		return *this;
	};
}ST_2DCal_ROI, *PST_2DCal_ROI;

//-------------------------------------------------------------------
// 2D CAL Intrinsic Parameter 세팅 구조체
//-------------------------------------------------------------------
typedef struct _tag_2DCal_Para_Old
{
	int				nImgWidth;
	int				nImgHeight;
	int				nNumofCornerX;
	int				nNumofCornerY;
	int				nUseOpenCV;
	int				nCornerDetectWindowRadius;
	int				nCornerDetectWindowSize;
	int				nBinNum;
	int				nGaussianFilterLength;
	int				nSubpixWindowRadius;
	int				nSubpixWindowSizeWidth;
	int				nSubpixWindowSizeHeight;
	int				nSubpixWindowRectSize;
	ST_LGE_ROI		ROI[MAX_2DCAL_ROI];

	_tag_2DCal_Para_Old()
	{
		Reset();
	};

	_tag_2DCal_Para_Old& operator= (const _tag_2DCal_Para_Old& ref)
	{
		nImgWidth					= ref.nImgWidth;
		nImgHeight					= ref.nImgHeight;
		nNumofCornerX				= ref.nNumofCornerX;
		nNumofCornerY				= ref.nNumofCornerY;
		nUseOpenCV					= ref.nUseOpenCV;
		nCornerDetectWindowRadius	= ref.nCornerDetectWindowRadius;
		nCornerDetectWindowSize		= ref.nCornerDetectWindowSize;
		nBinNum						= ref.nBinNum;
		nGaussianFilterLength		= ref.nGaussianFilterLength;
		nSubpixWindowRadius			= ref.nSubpixWindowRadius;
		nSubpixWindowSizeWidth		= ref.nSubpixWindowSizeWidth;
		nSubpixWindowSizeHeight		= ref.nSubpixWindowSizeHeight;
		nSubpixWindowRectSize		= ref.nSubpixWindowRectSize;
		
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_ROI; nIdx++)
		{
			ROI[nIdx] = ref.ROI[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		nImgWidth					=  640;		
		nImgHeight					=  480;		
		nNumofCornerX				=  4;		
		nNumofCornerY				=  6;	
		nUseOpenCV					=  0;
		nCornerDetectWindowRadius	=  5;		
		nCornerDetectWindowSize		=  11;		
		nBinNum						=  32;		
		nGaussianFilterLength		=  7;		
		nSubpixWindowRadius			=  7;		
		nSubpixWindowSizeWidth		=  15;		
		nSubpixWindowSizeHeight		=  15;		
		nSubpixWindowRectSize		=  361;		

		for (UINT nIdx = 0; nIdx < MAX_2DCAL_ROI; nIdx++)
		{
			ROI[nIdx].Reset();
		}
	};
}ST_2DCal_Para_Old, *PST_2DCal_Para_Old;

typedef struct _tag_2DCal_IntrInfo
{
	float arfKK[4];
	float arfKc[5];
	float fR2Max;
	float fOrgOffset;

	_tag_2DCal_IntrInfo()
	{
		Reset();
	};

	_tag_2DCal_IntrInfo& operator= (const _tag_2DCal_IntrInfo& ref)
	{
		memcpy(arfKK, ref.arfKK, sizeof(float) * 4);
		memcpy(arfKc, ref.arfKc, sizeof(float) * 5);

		fR2Max		= ref.fR2Max;
		fOrgOffset	= ref.fOrgOffset;

		return *this;
	};

	void Reset()
	{
		arfKK[0]	= 310.f;
		arfKK[1]	= 310.f;
		arfKK[2]	= 320.f;
		arfKK[3]	= 240.f;
		arfKc[0]	= -0.2f;
		arfKc[1]	= 0.045f;
		arfKc[2]	= 0.f;
		arfKc[3]	= 0.f;
		arfKc[4]	= -0.0045f;
		fR2Max		= 4.4f;
	};

}ST_2DCal_IntrInfo;

typedef struct _tag_2DCal_IntrParam
{
	/* default param */
	ST_2DCal_IntrInfo stCalibInfo_default;

	/* Image */
	int nImgWidth;
	int nImgHeight;

	/* Chart Spec */
	int nNumofCornerX;
	int nNumofCornerY;
	float fPatternSizeX;
	float fPatternSizeY;

	/* Threshold */
	float fRepThres;
	int nMinNumImages;
	int nMinNumValidPnts;

	/* SearchRange */
	float fMaxOriginOffset;
	float fMinOriginOffset;
	float fMaxFocalLength;
	float fMinFocalLength;

	/* Version */
	float fVersion;

	_tag_2DCal_IntrParam()
	{
		Reset();
	};

	_tag_2DCal_IntrParam& operator= (const _tag_2DCal_IntrParam& ref)
	{
		stCalibInfo_default				= ref.stCalibInfo_default;

		nImgWidth						= ref.nImgWidth;
		nImgHeight						= ref.nImgHeight;
		nNumofCornerX					= ref.nNumofCornerX;
		nNumofCornerY					= ref.nNumofCornerY;
		fPatternSizeX					= ref.fPatternSizeX;
		fPatternSizeY					= ref.fPatternSizeY;
		fRepThres						= ref.fRepThres;
		nMinNumImages					= ref.nMinNumImages;
		nMinNumValidPnts				= ref.nMinNumValidPnts;
		fMinOriginOffset				= ref.fMinOriginOffset;
		fMaxOriginOffset				= ref.fMaxOriginOffset;
		fMinFocalLength					= ref.fMinFocalLength;
		fMaxFocalLength					= ref.fMaxFocalLength;
// 		stCalibInfo_default.arfKK[0]	= ref.stCalibInfo_default.arfKK[0];
// 		stCalibInfo_default.arfKK[1]	= ref.stCalibInfo_default.arfKK[1];
// 		stCalibInfo_default.arfKK[2]	= ref.stCalibInfo_default.arfKK[2];
// 		stCalibInfo_default.arfKK[3]	= ref.stCalibInfo_default.arfKK[3];
// 		stCalibInfo_default.arfKc[0]	= ref.stCalibInfo_default.arfKc[0];
// 		stCalibInfo_default.arfKc[1]	= ref.stCalibInfo_default.arfKc[1];
// 		stCalibInfo_default.arfKc[2]	= ref.stCalibInfo_default.arfKc[2];
// 		stCalibInfo_default.arfKc[3]	= ref.stCalibInfo_default.arfKc[3];
// 		stCalibInfo_default.arfKc[4]	= ref.stCalibInfo_default.arfKc[4];
// 		stCalibInfo_default.fR2Max		= ref.stCalibInfo_default.fR2Max;
		fVersion						= ref.fVersion;

		return *this;
	};

	void Reset()
	{
		stCalibInfo_default.Reset();

		nImgWidth						= 640;
		nImgHeight						= 480;
		nNumofCornerX					= 4;
		nNumofCornerY					= 6;
		fPatternSizeX					= 60.f;
		fPatternSizeY					= 60.f;
		fRepThres						= 1.f;
		nMinNumImages					= 13;
		nMinNumValidPnts				= 300;
		fMinOriginOffset				= -4.f;
		fMaxOriginOffset				= 4.f;
		fMinFocalLength					= 311.f;
		fMaxFocalLength					= 314.f;
// 		stCalibInfo_default.arfKK[0]	= 310.f;
// 		stCalibInfo_default.arfKK[1]	= 310.f;
// 		stCalibInfo_default.arfKK[2]	= 320.f;
// 		stCalibInfo_default.arfKK[3]	= 240.f;
// 		stCalibInfo_default.arfKc[0]	= -0.2f;
// 		stCalibInfo_default.arfKc[1]	= 0.045f;
// 		stCalibInfo_default.arfKc[2]	= 0.f;
// 		stCalibInfo_default.arfKc[3]	= 0.f;
// 		stCalibInfo_default.arfKc[4]	= -0.0045f;
// 		stCalibInfo_default.fR2Max		= 4.4f;
		fVersion						= 1.0f;
	};

}ST_2DCal_IntrParam, *PST_2DCal_IntrParam;

typedef struct _tag_2DCal_Para
{
	ST_2DCal_IntrParam	IntrParam;
	ST_LGE_ROI			ROI[MAX_2DCAL_ROI];

	_tag_2DCal_Para()
	{
		Reset();
	};

	_tag_2DCal_Para& operator= (const _tag_2DCal_Para& ref)
	{
		IntrParam = ref.IntrParam;
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_ROI; nIdx++)
		{
			ROI[nIdx] = ref.ROI[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		IntrParam.Reset();
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_ROI; nIdx++)
		{
			ROI[nIdx].Reset();
		}
	};
}ST_2DCal_Para, *PST_2DCal_Para;

//-------------------------------------------------------------------
// 2D CAL Intrinsic Corner Points
//-------------------------------------------------------------------
typedef struct _tag2DCAL_CornerPt
{
	ST_LGE_CORNER_PT	Position[MAX_2DCAL_Corner_Point];
	unsigned char		usCornerParamStatus;	

	_tag2DCAL_CornerPt()
	{
		Reset();
	};

	_tag2DCAL_CornerPt& operator= (const _tag2DCAL_CornerPt& ref)
	{
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Corner_Point; nIdx++)
		{
			Position[nIdx] = ref.Position[nIdx];			
		}

		usCornerParamStatus	= ref.usCornerParamStatus;

		return *this;
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Corner_Point; nIdx++)
		{
			Position[nIdx].Reset();
		}
		usCornerParamStatus	= 0;
	};
}ST_2DCal_CornerPt, *PST_2DCal_CornerPt;

//-------------------------------------------------------------------
// 2D CAL Intrinsic Results
//-------------------------------------------------------------------
typedef struct _tag_2DCal_Result
{
	float			arfKK[MAX_2DCAL_Re_KK];
	float			arfKc[MAX_2DCAL_Re_Kc];
	float			fDistFOV;
	float			fR2Max;
	float			fRepError;					// Pass: < 1.0
	float			fEvalError;
	float			fOrgOffset;
	int				nDetectionFailureCnt;		// Pass: = 0
	int				nInvalidPointCnt;
	int				nValidPointCnt;
	unsigned char	arcDate[MAX_2DCAL_arcDate];

	_tag_2DCal_Result()
	{
		Reset();
	};

	_tag_2DCal_Result& operator= (const _tag_2DCal_Result& ref)
	{
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
		{
			arfKK[nIdx] = ref.arfKK[nIdx];
		}
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
		{
			arfKc[nIdx] = ref.arfKc[nIdx];
		}		

		fDistFOV				= ref.fDistFOV;
		fR2Max					= ref.fR2Max;
		fRepError				= ref.fRepError;
		fEvalError				= ref.fEvalError;
		fOrgOffset				= ref.fOrgOffset;
		nDetectionFailureCnt	= ref.nDetectionFailureCnt;
		nInvalidPointCnt		= ref.nInvalidPointCnt;
		nValidPointCnt			= ref.nValidPointCnt;
		
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_arcDate; nIdx++)
		{
			arcDate[nIdx] = ref.arcDate[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
		{
			arfKK[nIdx] = 0.0f;
		}
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
		{
			arfKc[nIdx] = 0.0f;
		}		

		fDistFOV				= 0.0f;
		fR2Max					= 0.0f;
		fRepError				= 0.0f;
		fEvalError				= 0.0f;
		fOrgOffset				= 0.0f;
		nDetectionFailureCnt	= 0;
		nInvalidPointCnt		= 0;
		nValidPointCnt			= 0;
		
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_arcDate; nIdx++)
		{
			arcDate[nIdx] = 0;
		}
	};

}ST_2DCal_Result, *PST_2DCal_Result;

typedef struct _tag_2DCal_Spec
{
	float			arfKK[MAX_2DCAL_Re_KK];
	float			arfKc[MAX_2DCAL_Re_Kc];
	float			fDistFOV;
	float			fR2Max;
	float			fRepError;					// Pass: < 1.0
	float			fEvalError;
	float			fOrgOffset;
	int				nDetectionFailureCnt;		// Pass: = 0
	int				nInvalidPointCnt;
	int				nValidPointCnt;
	unsigned char	arcDate[MAX_2DCAL_arcDate];

	_tag_2DCal_Spec()
	{
		Reset();
	};

	_tag_2DCal_Spec& operator= (const _tag_2DCal_Spec& ref)
	{
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
		{
			arfKK[nIdx] = ref.arfKK[nIdx];
		}
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
		{
			arfKc[nIdx] = ref.arfKc[nIdx];
		}

		fDistFOV				= ref.fDistFOV;
		fR2Max					= ref.fR2Max;
		fRepError				= ref.fRepError;
		fEvalError				= ref.fEvalError;
		fOrgOffset				= ref.fOrgOffset;
		nDetectionFailureCnt	= ref.nDetectionFailureCnt;
		nInvalidPointCnt		= ref.nInvalidPointCnt;
		nValidPointCnt			= ref.nValidPointCnt;
		
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_arcDate; nIdx++)
		{
			arcDate[nIdx] = ref.arcDate[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_KK; nIdx++)
		{
			arfKK[nIdx] = 0.0f;
		}
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Re_Kc; nIdx++)
		{
			arfKc[nIdx] = 0.0f;
		}

		fDistFOV				= 0.0f;
		fR2Max					= 0.0f;
		fRepError				= 0.0f;
		fEvalError				= 0.0f;
		fOrgOffset				= 0.0f;
		nDetectionFailureCnt	= 0;
		nInvalidPointCnt		= 0;
		nValidPointCnt			= 0;
		
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_arcDate; nIdx++)
		{
			arcDate[nIdx] = 0;
		}
	};

}ST_2DCal_Spec, *PST_2DCal_Spec;


typedef CArray<ST_2DCal_ROI, ST_2DCal_ROI&>				ARR_2D_ROI;
typedef CArray<ST_2DCal_CornerPt, ST_2DCal_CornerPt&>	ARR_2D_CornerPoint;

typedef struct _tagST_2DCal_TeachMotorPos
{
	double dbDistanceX;
	double dbDistanceY;
	double dbDegreeR;

	_tagST_2DCal_TeachMotorPos()
	{
		dbDistanceX = 0.0;
		dbDistanceY = 0.0;
		dbDegreeR = 0.0;
	};

	_tagST_2DCal_TeachMotorPos& operator= (const _tagST_2DCal_TeachMotorPos& ref)
	{
		dbDistanceX = ref.dbDistanceX;
		dbDistanceY = ref.dbDistanceY;
		dbDegreeR	= ref.dbDegreeR;

		return *this;
	};

	void Reset()
	{
		dbDistanceX = 0.0;
		dbDistanceY = 0.0;
		dbDegreeR	= 0.0;
	};

}ST_2DCal_TeachMotor, *PST_2DCal_TeachMotor;

//-------------------------------------------------------------------
// 2D CAL에서 사용하는 데이터 구조체
//-------------------------------------------------------------------
typedef struct _tag_2DCal_Op
{
	ST_2DCal_Para		Parameter;							// Parameters
	ST_2DCal_CornerPt	CornerPoints[MAX_2DCAL_Position];	// Corner Points
	ST_2DCal_Result		Result;								// Results
	ST_2DCal_Spec		Spec_Min;							// Results Spec Min
	ST_2DCal_Spec		Spec_Max;							// Results Spec Max
	ST_2DCal_TeachMotor MotorPos[MAX_2DCAL_Position];		// 모터 위치

	_tag_2DCal_Op()
	{
		Reset();
	};

	_tag_2DCal_Op& operator= (const _tag_2DCal_Op& ref)
	{
		Parameter		= ref.Parameter;

		for (int _a = 0; _a < MAX_2DCAL_Position; _a++)
		{
			CornerPoints[_a] = ref.CornerPoints[_a];
			MotorPos[_a] = ref.MotorPos[_a];
		}

		Result			= ref.Result;
		Spec_Min		= ref.Spec_Min;
		Spec_Max		= ref.Spec_Max;
		
		return *this;
	};

	void Reset()
	{
		Parameter.Reset();

		for (int _a = 0; _a < MAX_2DCAL_Position; _a++)
		{
			CornerPoints[_a].Reset();
			MotorPos[_a].Reset();
		}
			
		Result.Reset();
		Spec_Min.Reset();
		Spec_Max.Reset();
	};

	void AddROI(__in ST_2DCal_ROI nROI)
	{
	}

	void RemoveROI(__in UINT nIdx)
	{
	}

	// CAN 전송 코드로 변환
	void SetMakeCanData(__out char* szSendCanData)
	{

	}

}ST_2DCal_Op, *PST_2DCal_Op;

typedef struct _tag_2DCal_Info
{
	ST_2DCal_Para		Parameter;							// Parameters
	ST_2DCal_Spec		Spec_Min;							// Results Spec Min
	ST_2DCal_Spec		Spec_Max;							// Results Spec Max
	ST_2DCal_TeachMotor MotorPos[MAX_2DCAL_Position];		// 모터 위치

	_tag_2DCal_Info()
	{
		Reset();
	};

	_tag_2DCal_Info& operator= (const _tag_2DCal_Info& ref)
	{
		Parameter = ref.Parameter;
		Spec_Min = ref.Spec_Min;
		Spec_Max = ref.Spec_Max;

		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Position; nIdx++)
		{
			MotorPos[nIdx] = ref.MotorPos[nIdx];
		}

		return *this;			
	};

	void Reset()
	{
		Parameter.Reset();
		
		Spec_Min.Reset();
		Spec_Max.Reset();

		for (UINT nIdx = 0; nIdx < MAX_2DCAL_Position; nIdx++)
		{
			MotorPos[nIdx].Reset();
		}
	};
}ST_2DCal_Info, *PST_2DCal_Info;

#pragma pack(pop)

#endif // Def_T_2DCal_h__
