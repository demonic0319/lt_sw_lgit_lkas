﻿//*****************************************************************************
// Filename	: 	List_Barcode.h
// Created	:	2016/10/22
// Modified	:	2016/10/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef List_Barcode_h__
#define List_Barcode_h__

#pragma once

#include "Def_Test_Cm.h"

//-----------------------------------------------------------------------------
// CList_Barcode
//-----------------------------------------------------------------------------
class CList_Barcode : public CMFCListCtrl
{
	DECLARE_DYNAMIC(CList_Barcode)

public:
	CList_Barcode();
	virtual ~CList_Barcode();

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnNMClick			(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	CFont		m_Font;

	virtual void	InitHeader		();
	void			AdjustColWidth	();

	BOOL		m_bIntiHeader		= FALSE;
	BOOL		m_bUseParaInfo		= TRUE;

public:
	
	void		Set_UseParaInfo		(__in BOOL bUse);

	void		ClearWorklist		();

	void		InsertWipID			(__in LPCTSTR szBarcode, __in const PSYSTEMTIME pstTime = NULL);

	void		InsertWipID			(__in LPCTSTR szBarcode, __in const PSYSTEMTIME pstTime, __in UINT nParaIdx);

	void		InsertWipID			(__in LPCTSTR szBarcode, __in enTestResult nJudge, __in const PSYSTEMTIME pstTime = NULL);

	void		InsertWipID			(__in LPCTSTR szBarcode, __in enTestResult nJudge, __in const PSYSTEMTIME pstTime, __in UINT nParaIdx);
};

#endif // List_Barcode_h__


