﻿//*****************************************************************************
// Filename	: 	Grid_Yield_Test.h
// Created	:	2016/11/14 - 20:13
// Modified	:	2016/11/14 - 20:13
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Grid_Yield_Test_h__
#define Grid_Yield_Test_h__

#pragma once

#include <afxwin.h>
#include "Grid_Base.h"
#include "Def_DataStruct_Cm.h"

class CGrid_Yield_Test : public CGrid_Base
{
public:
	CGrid_Yield_Test();
	virtual ~CGrid_Yield_Test();

	virtual void	OnSetup			();
	virtual int		OnHint			(int col, long row, int section, CString *string);
	virtual void	OnGetCell		(int col, long row, CUGCell *cell);
	virtual void	OnDrawFocusRect	(CDC *dc, RECT *rect);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline	();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline	();

	// 헤더를 초기화
	void			InitHeader		();

	CFont		m_font_Header;
	CFont		m_font_Data;

	UINT		m_nTestItemCount	= MAX_TESTITEM;

public:

	void		SetTestItemInfo		(__in const ST_TestItemInfo* pstTestItemInfo);
	void		SetYield			(__in const ST_YieldInfo* pstYield);

};

#endif // Grid_Yield_Test_h__

