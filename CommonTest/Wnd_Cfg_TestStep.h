//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestStep.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestStep_h__
#define Wnd_Cfg_TestStep_h__

#pragma once

#include <afxwin.h>
#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_Enum_Cm.h"
#include "Def_TestItem_Cm.h"
#include "List_TestStep.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_TestStep
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestStep : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestStep)

public:
	CWnd_Cfg_TestStep();
	virtual ~CWnd_Cfg_TestStep();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnStepItem		(UINT nID);
	afx_msg void	OnBnClickedBnStepCtrl		(UINT nID);
	afx_msg void	OnCbnSelendokTestItem		();
	afx_msg void	OnBnClickedBnLoadXML		();
	afx_msg void	OnBnClickedBnSaveXML		();

	CFont			m_font_Default;
	CFont			m_Font;

	enum enStepItem
	{
		SI_Retry,
		SI_Delay,
		SI_Test,			// 테스트 검사 항목
		SI_MoveY,			// 팔레트 전/후진		
		SI_MoveX,			// 팔레트 좌/우
		SI_ChartIdx,		// 차트 번호
		SI_ChartRot,		// 챠트 회전
		SI_Chart_Move_X,	// 차트 X
		SI_Chart_Move_Z,	// 차트 Z
		SI_Chart_Tilt_X,	// 차트 Tilt X
		SI_Chart_Tilt_Z,	// 차트 Tilt Z
		SI_Alpha,			// Alpha
		SI_Alpha2nd,		// Alpha 2nd
		SI_BoardCtrl,		// Can 제어
		SI_TestLoop,		// Loop
		SI_Chart_Check,		// 차트 체크 사용 여부
		//SI_TestContinue,		// 차트 체크 사용 여부
		SI_MaxEnum,
	};

	enum enStepCtrl
	{
		SC_Add,
		SC_Insert,
		SC_Remove,
		SC_Order_Up,
		SC_Order_Down,
		SC_MaxEnum,
	};

	// 스텝 목록
	CList_TestStep	m_lc_StepList;
	// 스텝 항목 추가/삭제/이동
	CMFCButton		m_bn_StepCtrl[SC_MaxEnum];
	// 스텝 설정 항목
	CMFCButton		m_chk_StepItem[SI_MaxEnum];
	CMFCMaskedEdit	m_ed_StepItem[SI_MaxEnum];
	// 개별 검사 항목
	CComboBox		m_cb_TestItem;
	// CAN 제어
	CComboBox		m_cb_Board_Ctrl;
	// Retry Count
	CComboBox		m_cb_RetryCnt;
	// Loop 
	CComboBox		m_cb_TestLoop;
	// Chart Index
	CComboBox		m_cb_ChartIdx;
	// Chart Index
	CComboBox		m_cb_ChartCheck;

	// XML 저장&불러오기
	CMFCButton		m_bn_LoadXML;
	CMFCButton		m_bn_SaveXML;


	// 검사기 설정
	enInsptrSysType	m_InspectionType	= enInsptrSysType::Sys_Focusing;

	// XML 파일 패스
	CString			m_szPath;


	// ComboBox에 TestItem 추가하기
	void	Set_TestItem			();

	// UI에 설정된 스텝 데이터 구하기
	BOOL	GetTestStepData			(__out ST_StepUnit& stStepUnit);

	// 버튼 제어 함수
	void	Item_Add();
	void	Item_Insert();
	void	Item_Remove();
	void	Item_Up();
	void	Item_Down();

	// XML 불러오기 / 저장하기
	BOOL	LoadXML();
	BOOL	SaveXML();

public:

	// 검사기 종류 설정
	void		SetSystemType		(__in enInsptrSysType nSysType);

	void		Set_XMLFilePath		(__in LPCTSTR szPath);

	// 저장된 Step Info 데이터 불러오기
	void		Set_StepInfo		(__in const ST_StepInfo* pstInStepInfo);
	void		Get_StepInfo		(__out ST_StepInfo& stOutStepInfo);

	// 초기 상태로 설정 (New)
	void		Init_DefaultSet		();

};

#endif // Wnd_Cfg_TestStep_h__


