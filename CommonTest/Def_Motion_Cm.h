﻿//*****************************************************************************
// Filename	: Def_Motion_Cm.h
// Created	: 2016/10/03
// Modified	: 2016/10/03
//
// Author	: Young
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_Motion_Cm_h__
#define Def_Motion_Cm_h__

#include <afxwin.h>

//---------------------------------------------------------
// 모션정보
typedef struct _tag_MotionTable
{
	BOOL bUseMotion;				// 모션 사용여부
	UINT nTotal_Axis;				// 총 축갯수
	UINT nTotal_IOBd;				// IO 보드 장착 갯수
	UINT nCntInport;				// 한보드당 사용 INPORT 갯수
	UINT nCntOutport;				// 한보드당 사용 OUTPORT 갯수
}ST_MotionTable;

static ST_MotionTable g_MotionTable[] =
{	// 모션 사용여부		// 총 축갯수		//IO보드 장착 갯수		//한보드당 사용 INPORT 갯수		//한보드당 사용 OUTPORT 갯수
	{ FALSE,				0,						0,							0,									0 },				//	SYS_FOCUSING
	{ TRUE,					9,						1,							16,									16 },				//	SYS_2D_CAL
	{ TRUE,					8,						2,							16,									16 },				//	SYS_IMAGE_TEST
	{ FALSE,				0,						0,							0,									0 },				//	SYS_STEREO_CAL
	{ TRUE,					9,						1,							16,									16 },				//	SYS_3D_CAL
	NULL
};

//=============================================================================
// Motor Setting, Combo Box UI View
//=============================================================================
static LPCTSTR g_szMotorOption_1st[] =
{
	_T(" 0 : LOW"),
	_T(" 1 : HIGH"),
	_T(" 2 : UNUSED"),
	_T(" 3 : USED"),
	NULL
};

//=============================================================================
// Motor Setting, Combo Box UI View
//=============================================================================
static LPCTSTR g_szMotorOption_2st[] =
{
	_T(" 0 : + LIMIT"),
	_T(" 1 : - LIMIT"),
	_T(" 2 : Home Sensor"),
	_T(" 3 : Encod Zphas"),
	NULL
};

//=============================================================================
// Motor Setting, Combo Box UI View
//=============================================================================
static LPCTSTR g_szMotorOption_3st[] =
{
	_T(" 4 : TwoCcwCwHigh"),
	_T(" 6 : TwoCwCcwHigh"),
	NULL
};

//=============================================================================
// Motor Setting, Combo Box UI View
//=============================================================================
static LPCTSTR g_szMotorOption_4st[] =
{
	_T(" 0 : Dir Ccw(-)"),
	_T(" 1 : Dir Cw(+)"),
	NULL
};

//=============================================================================
// Motor Setting, Combo Box UI View
//=============================================================================
static LPCTSTR g_szMotorOption_5st[] =
{
	_T(" 1 : Sqr 1Mode"),
	_T(" 2 : Sqr 2Mode"),
	_T(" 3 : Sqr 4Mode"),
	_T(" 5 : Reverse 1Mode"),
	_T(" 6 : Reverse 2Mode"),
	_T(" 7 : Reverse 4Mode"),
	NULL
};

//=============================================================================
// Motor Setting, Combo Box UI View name
//=============================================================================
static LPCTSTR g_szMotorOptionName[] =
{
	_T(" Motor Direction"),
	_T(" Alarm Level"),
	_T(" In Position"),
	_T(" Home Level"),
	_T(" + End Limit"),
	_T(" - End Limit"),
	_T(" Origin Signal"),
	_T(" Origin Direction"),
	_T(" Enc. Input"),
	NULL
};

//=============================================================================
// Motor State UI View Name
//=============================================================================
static LPCTSTR g_szMotorStateName[] =
{
	_T(" Motor Name"),
	_T(" Power"),
	_T(" Position"),
	_T(" Origin"),
	_T(" - Limt"),
	_T(" Hom"),
	_T(" + Limt"),
	_T(" Alarm"),
	NULL
};

//=============================================================================
//
//=============================================================================
typedef enum __SETTING_LIST 
{
	MotorDirection = 0,			// Motor Move Diretion +/-,
	AlarmLevel = 1,				// Motor Alarm Level H/L,
	InPosition = 2,				// Motor Move InPosition Level H/L,
	HomeLevel = 3,				// Motor Home Level H/L,
	PlusEndLimit = 4,			// Motor Plus End Limit H/L,
	MinEndLimit = 5,			// Motor Min End Limit H/L,
	OriginSignal = 6,			// Motor Origin Sensor Choice -L/H/+L,
	OriginDirection = 7,		// Motor Origin Diretion +/-,
	EncInput = 8				// Motor Origin Diretion +/-,
} SETTING_LIST;

//=============================================================================
// Motor detail Data UI View Name
//=============================================================================
static LPCTSTR g_szMotorControlName[] =
{
	_T(" ORIGIN 1ST_VAL"),
	_T(" ORIGIN 2ST_VAL"),
	_T(" ORIGIN ACC"),
	_T(" ORIGIN OFFSET"),
	_T(" MOTOR VAL"),
	_T(" MOTOR ACC"),
	NULL
};

//=============================================================================
//Motor Origin Result UI View Name
//=============================================================================
static LPCTSTR g_szMotorOriginResult[] =
{
	_T(" 원 점 수 행 완 료 !!"),
	_T(" 원 점 수 행 실 패 ..!!"),
	_T(" 원 점 수 행 진 행 중 ..."),
	NULL
};
#endif // Def_Motion_Cm_h__