//*****************************************************************************
// Filename	: 	Wnd_LGETestResult_Unit.cpp
// Created	:	2017/9/15 - 18:43
// Modified	:	2017/9/15 - 18:43
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_LGETestResult_Unit.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_LGETestResult_Unit.h"
#include "Def_Test_Cm.h"

// CWnd_LGETestResult_Unit

#define		COLOR_SELECT		Gdiplus::Color::SkyBlue
#define		COLOR_DEFAULT		Gdiplus::Color::White

#define		IDC_BN_AUTO			1001
#define		IDC_BN_NORMAL		1002

#define		IDC_BN_COLUMN		2000

static LPCTSTR g_szHeader[] =
{
	_T("NO"),			// TRH_NO,			
	_T("Test Name"),	// TRH_TestName,	
	_T("Judgment"),		// TRH_Judgment,	
	_T("Measure"),		// TRH_Measure,	
	_T("Min"),			// TRH_SpecMin,	
	_T("Max"),			// TRH_SpecMax,	
	_T("Elap. Time"),	// TRH_ElapsedTime
	NULL
};

// static int g_iCell_Width[] =
// {
// 	 30,	// TRH_NO,			
// 	200,	// TRH_TestName,	
// 	 80,	// TRH_Judgment,	
// 	100,	// TRH_Measure,	
// 	100,	// TRH_SpecMin,	
// 	100,	// TRH_SpecMax,	
// 	100,	// TRH_ElapsedTime
// };

static int g_iCell_Width[] =
{
	30,		// TRH_NO,			
	250,	// TRH_TestName,	
	70,		// TRH_Judgment,	
	0,		// TRH_Measure,	
	0,		// TRH_SpecMin,	
	0,		// TRH_SpecMax,	
	90,		// TRH_ElapsedTime
};

IMPLEMENT_DYNAMIC(CWnd_LGETestResult_Unit, CWnd)

CWnd_LGETestResult_Unit::CWnd_LGETestResult_Unit()
{
	m_iBtnColumn = 1;
	m_nBtnResource = 0;

	m_iTotalLengthRate = 0;
	for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
	{
		m_iTotalLengthRate += g_iCell_Width[nIdx];
		m_iCtrlLength[nIdx] = 0;
	}
}

CWnd_LGETestResult_Unit::~CWnd_LGETestResult_Unit()
{

}

BEGIN_MESSAGE_MAP(CWnd_LGETestResult_Unit, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_COLUMN, OnBnClick)
END_MESSAGE_MAP()

// CWnd_LGETestResult_Unit message handlers

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/15 - 19:28
// Desc.		:
//=============================================================================
int CWnd_LGETestResult_Unit::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
	{
		switch (m_nCellType)
		{
		case enCellType::TRCT_Header:
		{
			//m_st_Cell[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
			m_st_Cell[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
			m_st_Cell[nIdx].SetFont_Gdip(L"Arial", m_fFontSize);
			if (FALSE == m_st_Cell[nIdx].Create(g_szHeader[nIdx], WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC))
			{
				TRACE("m_st_Cell[%d] 창을 만들지 못했습니다.\n", nIdx);
				return -1;
			}
		}
			break;

		case enCellType::TRCT_Data:
		{
			//m_st_Cell[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);

			if (nIdx == 1)
			{
				m_st_Cell[nIdx].SetTextAlignment(StringAlignmentNear);
			}
			m_st_Cell[nIdx].SetColorStyle(CVGStatic::ColorStyle_White);
			m_st_Cell[nIdx].SetFont_Gdip(L"Arial", m_fFontSize);
			if (FALSE == m_st_Cell[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC))
			{
				TRACE("m_st_Cell[%d] 창을 만들지 못했습니다.\n", nIdx);
				return -1;
			}
		}
			break;

		case enCellType::TRCT_BTN:
		{
			if (m_iBtnColumn == nIdx)
			{
				if (FALSE == m_bt_Cell.Create(_T(""), dwStyle, rectDummy, this, IDC_BN_COLUMN))
				{
					TRACE("m_bt_Cell 창을 만들지 못했습니다.\n", nIdx);
					return -1;
				}
			}
			else
			{
				m_st_Cell[nIdx].SetColorStyle(CVGStatic::ColorStyle_White);
				m_st_Cell[nIdx].SetFont_Gdip(L"Arial", m_fFontSize);
				if (FALSE == m_st_Cell[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC))
				{
					TRACE("m_st_Cell[%d] 창을 만들지 못했습니다.\n", nIdx);
					return -1;
				}
			}
		}
			break;

		default:
			break;
		}
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/15 - 19:29
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx + (TRH_MaxEnum - 1);
	int iHeight		= cy;	
	int iRemind = 0;

	for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
	{
		m_iCtrlLength[nIdx] = (iWidth * g_iCell_Width[nIdx]) / m_iTotalLengthRate;
		iRemind += m_iCtrlLength[nIdx];
	}
	iRemind = iWidth - iRemind;

	for (INT iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		m_iCtrlLength[iIdx - 1] += 1;
	}

	for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
	{
		if (enCellType::TRCT_BTN == m_nCellType)
		{
			if (nIdx == m_iBtnColumn)
				m_bt_Cell.MoveWindow(iLeft, iTop, m_iCtrlLength[nIdx], iHeight);
			else
				m_st_Cell[nIdx].MoveWindow(iLeft, iTop, m_iCtrlLength[nIdx], iHeight);
		}
		else
		{
			m_st_Cell[nIdx].MoveWindow(iLeft, iTop, m_iCtrlLength[nIdx], iHeight);
		}

		iLeft += (m_iCtrlLength[nIdx] - 1);
	}
}

//=============================================================================
// Method		: ResetItem
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/17 - 17:20
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::ResetItem()
{
	if (m_nCellType == TRCT_Data)
	{
		for (UINT nIdx = 1; nIdx < TRH_MaxEnum; nIdx++)
		{
			m_st_Cell[nIdx].SetText(L"");
			m_st_Cell[nIdx].SetBackColor(Gdiplus::Color::White);
		}
	}
}

//=============================================================================
// Method		: ResetTestData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/26 - 9:58
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::ResetTestData()
{
	if (m_nCellType == TRCT_Data)
	{
		m_st_Cell[TRH_Judgment].SetText(L"");
		m_st_Cell[TRH_Judgment].SetBackColor(Gdiplus::Color::White);

		m_st_Cell[TRH_Measure].SetText(L"");
		m_st_Cell[TRH_Measure].SetBackColor(Gdiplus::Color::White);

		m_st_Cell[TRH_ElapsedTime].SetText(L"");
		m_st_Cell[TRH_ElapsedTime].SetBackColor(Gdiplus::Color::White);

		for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
		{
			m_st_Cell[nIdx].SetBackColor(COLOR_DEFAULT);
		}
	}
}

//=============================================================================
// Method		: SetCellType
// Access		: public  
// Returns		: void
// Parameter	: __in enCellType bCellType
// Qualifier	:
// Last Update	: 2017/9/17 - 17:20
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::SetCellType(__in enCellType bCellType /*= TRCT_Data*/)
{
	if (m_nCellType != bCellType)
	{
		m_nCellType = bCellType;

// 		if (m_nCellType == TRCT_Header)
// 		{
// 			for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
//			{
// 				m_st_Cell[nIdx].SetText(g_szHeader[nIdx]);
// 			}
// 		}
// 		else if (m_nCellType == TRCT_Data)
// 		{
// 			for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
//			{
// 				m_st_Cell[nIdx].SetText(L"");
// 			}
// 		}

		// 다시 그리기
		if (GetSafeHwnd())
		{
			CRect rc;
			GetClientRect(rc);
			OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
			//SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
		}
	}
}

void CWnd_LGETestResult_Unit::SetFontSize(__in FLOAT fSize)
{
	m_fFontSize = fSize;
}

//=============================================================================
// Method		: Set_Number
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nNumber
// Qualifier	:
// Last Update	: 2017/9/17 - 18:00
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::Set_Number(__in UINT nNumber)
{
	CStringW	szText;
	szText.Format(L"%d", nNumber);
	m_st_Cell[TRH_NO].SetText(szText.GetBuffer(0));
}

//=============================================================================
// Method		: Set_TestName
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szTestName
// Qualifier	:
// Last Update	: 2017/9/28 - 13:54
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::Set_TestName(__in LPCTSTR szTestName)
{
	if (enCellType::TRCT_BTN == m_nCellType)
		m_bt_Cell.SetWindowText(szTestName);
	else
		m_st_Cell[TRH_TestName].SetText(szTestName);
}

//=============================================================================
// Method		: Set_Spec
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_TestItemSpec * pstSpec
// Parameter	: __in_opt LPCTSTR szOptTestName
// Qualifier	:
// Last Update	: 2017/9/28 - 15:13
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::Set_Spec(__in const ST_TestItemSpec* pstSpec, __in_opt LPCTSTR szOptTestName /*= NULL*/)
{
	CStringW	szText;

	// * 검사 항목 명칭
	if (NULL == szOptTestName)
	{
		CString sz = ((ST_TestItemSpec*)pstSpec)->szName;

		if (enCellType::TRCT_BTN == m_nCellType)
			m_bt_Cell.SetWindowTextW(sz);
		else
			m_st_Cell[TRH_TestName].SetText(((ST_TestItemSpec*)pstSpec)->szName.GetBuffer(0));
	}
	else
	{
		szText.Format(L"%s (%s)", ((ST_TestItemSpec*)pstSpec)->szName.GetBuffer(0), szOptTestName);

		if (enCellType::TRCT_BTN == m_nCellType)
			m_bt_Cell.SetWindowText(szText);
		else
			m_st_Cell[TRH_TestName].SetText(szText.GetBuffer(0));
	}

	// ** ASSERT(0 < pstSpec->Spec.GetCount());
	ASSERT(0 < pstSpec->nResultCount);
	// ** if ((pstSpec->bUseMinMaxSpec) && (0 < pstSpec->Spec.GetCount()))
	if ((pstSpec->bUseMinMaxSpec) && (0 < pstSpec->nResultCount))
	{
		if (pstSpec->Spec[0].bUseSpecMin)
		{
			// * Spec Minimum
			if ((VT_I2 == pstSpec->vt) || (VT_I4 == pstSpec->vt) || ((VT_I1 <= pstSpec->vt) && (pstSpec->vt <= VT_UINT)))
			{
				szText.Format(L"%d", pstSpec->Spec[0].Spec_Min.intVal);
			}
			else if ((VT_R4 == pstSpec->vt) || (VT_R8 == pstSpec->vt))
			{
				szText.Format(L"%.04f", pstSpec->Spec[0].Spec_Min.dblVal);
			}
			m_st_Cell[TRH_SpecMin].SetText(szText.GetBuffer(0));
		}

		if (pstSpec->Spec[0].bUseSpecMax)
		{
			// * Spec Maximum
			if ((VT_I2 == pstSpec->vt) || (VT_I4 == pstSpec->vt) || ((VT_I1 <= pstSpec->vt) && (pstSpec->vt <= VT_UINT)))
			{
				szText.Format(L"%d", pstSpec->Spec[0].Spec_Max.intVal);
			}
			else if ((VT_R4 == pstSpec->vt) || (VT_R8 == pstSpec->vt))
			{
				szText.Format(L"%.04f", pstSpec->Spec[0].Spec_Max.dblVal);
			}
			m_st_Cell[TRH_SpecMax].SetText(szText.GetBuffer(0));
		}
	}
}

//=============================================================================
// Method		: Set_TestData
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_TestItemMeas * pstTestItem
// Parameter	: __in BOOL bTest
// Qualifier	:
// Last Update	: 2017/11/9 - 13:29
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::Set_TestData(__in const ST_TestItemMeas* pstTestItem, __in BOOL bTest /*= TRUE*/)
{
	CStringW	szText;	

	// * 측정값
	if (bTest)
	{
		UINT nPrintIndex = 0;

		if ((TR_Fail == pstTestItem->nJudgmentAll) && (1 < pstTestItem->nMeasurmentCount))
		{
			for (UINT nIdx = 0; nIdx < pstTestItem->nMeasurmentCount; nIdx++)
			{
				if (TR_Fail == pstTestItem->nJudgmentUnit[nIdx])
				{
					nPrintIndex = nIdx;
					break;
				}
			}
		}

		if ((VT_I2 == pstTestItem->vt) || (VT_I4 == pstTestItem->vt) || ((VT_I1 <= pstTestItem->vt) && (pstTestItem->vt <= VT_UINT)))
		{
			if (1 < pstTestItem->nMeasurmentCount)
			{
				szText.Format(L"[%d] %d", nPrintIndex, pstTestItem->Measurement[nPrintIndex].intVal);
			}
			else
			{
				szText.Format(L"%d", pstTestItem->Measurement[nPrintIndex].intVal);
			}
		}
		else if ((VT_R4 == pstTestItem->vt) || (VT_R8 == pstTestItem->vt))
		{
			if (1 < pstTestItem->nMeasurmentCount)
			{
				szText.Format(L"[%d] %.04f", nPrintIndex, pstTestItem->Measurement[nPrintIndex].dblVal);
			}
			else
			{
				szText.Format(L"%.04f", pstTestItem->Measurement[nPrintIndex].dblVal);
			}
		}
		else if (VT_BSTR == pstTestItem->vt)
		{
			if (1 < pstTestItem->nMeasurmentCount)
			{
				if (FALSE == IsBadStringPtr(pstTestItem->Measurement[nPrintIndex].bstrVal, 1))
				{
					szText.Format(L"[%d] %s", nPrintIndex, pstTestItem->Measurement[nPrintIndex].bstrVal);
				}
				else
				{
					szText.Format(L"[%d]", nPrintIndex);
				}
			}
			else
			{
				if (FALSE == IsBadStringPtr(pstTestItem->Measurement[nPrintIndex].bstrVal, 1))
				{
					szText.Format(L"%s", pstTestItem->Measurement[nPrintIndex].bstrVal);
				}
				else
				{
					szText = L"";
				}
			}
		}

		m_st_Cell[TRH_Measure].SetText(szText.GetBuffer(0));
	}

	// * 결과 판정
	USES_CONVERSION;
	szText = CT2W(g_TestResult[pstTestItem->nJudgmentAll].szText);
	m_st_Cell[TRH_Judgment].SetText(szText.GetBuffer(0));
	m_st_Cell[TRH_Judgment].SetBackColor_COLORREF(g_TestResult[pstTestItem->nJudgmentAll].BackColor);

	// * 검사 진행 시간
	UINT nMilisecond = (pstTestItem->dwElapTime % 1000) / 100;
	UINT nSecond = (pstTestItem->dwElapTime / 1000) % 60;
	UINT nMinute = (pstTestItem->dwElapTime / 60000);// % 60;	

	szText.Format(L"%02d:%02d.%03d", nMinute, nSecond, nMilisecond);
	m_st_Cell[TRH_ElapsedTime].SetText(szText.GetBuffer(0));
}

//=============================================================================
// Method		: Set_SelectItem
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bSelect
// Qualifier	:
// Last Update	: 2017/9/26 - 13:48
// Desc.		:
//=============================================================================
void CWnd_LGETestResult_Unit::Set_SelectItem(__in BOOL bSelect)
{
	if (bSelect)
	{
		for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
		{
			m_st_Cell[nIdx].SetBackColor(COLOR_SELECT);
		}
	}
	else
	{
		for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
		{
			if (COLOR_SELECT == m_st_Cell[nIdx].GetBackColor())
			{
				m_st_Cell[nIdx].SetBackColor(COLOR_DEFAULT);
			}
		}
	}
}

void CWnd_LGETestResult_Unit::OnBnClick()
{
	CMFCButton* pButton = (CMFCButton*)GetDlgItem(m_nBtnResource);
	for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
	{
		m_st_Cell[nIdx].SetBackColor(COLOR_SELECT);
	}
	GetParent()->GetParent()->SendMessage(WM_COMMAND, MAKELONG(m_nBtnResource, BN_CLICKED), (LPARAM)m_hWnd);

	for (UINT nIdx = 0; nIdx < TRH_MaxEnum; nIdx++)
	{
		if (COLOR_SELECT == m_st_Cell[nIdx].GetBackColor())
		{
			m_st_Cell[nIdx].SetBackColor(COLOR_DEFAULT);
		}
	}
}
