﻿//*****************************************************************************
// Filename	: 	List_Barcode.cpp
// Created	:	2016/10/22
// Modified	:	2016/10/22
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// List_Barcode.cpp : implementation file
//

#include "stdafx.h"
#include "Def_Enum_Cm.h"
#include "List_Barcode.h"

// CList_Barcode

typedef enum enWIP_IDHeader
{
	WLH_Time,
	WLH_Barcode,
	WLH_Judge,
	WLH_Para,
	WLH_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader[] = 
{
	_T("Time"),		// WLH_Time
	_T("Barcode"),	// WLH_Barcode
	_T("Judg"),		// WLH_Judge
	_T("Para"),		// WLH_Para
	NULL 
};

const int	iListAglin[] = 
{ 
	LVCFMT_LEFT,	// WLH_Time
	LVCFMT_LEFT,	// WLH_Barcode
	LVCFMT_CENTER,	// WLH_Judge
	LVCFMT_CENTER,	// WLH_Para
};

const int	iHeaderWidth[] = 
{
	25, 	// WLH_Time
	45,		// WLH_Barcode
	15,		// WLH_Judge
	15,		// WLH_Para
};


IMPLEMENT_DYNAMIC(CList_Barcode, CMFCListCtrl)

CList_Barcode::CList_Barcode()
{	
	VERIFY(m_Font.CreateFont(
		14,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		FIXED_PITCH,			// nPitchAndFamily
		_T("CONSOLAS")));			// lpszFacename
}

CList_Barcode::~CList_Barcode()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Barcode, CMFCListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//ON_NOTIFY_REFLECT(NM_CLICK,	&CList_Barcode::OnNMClick)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


// CList_Barcode message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
int CList_Barcode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMFCListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);	

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_Barcode::OnSize(UINT nType, int cx, int cy)
{
	CMFCListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iColWidth[WLH_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;
	int iMaxCol = m_bUseParaInfo ? WLH_MaxCol : (WLH_MaxCol - 1);

	for (int nCol = 0; nCol < iMaxCol; nCol++)
	{
		iColDivide += iHeaderWidth[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	

	for (int nCol = 0; nCol < iMaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth[WLH_Time]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(WLH_Time, iUnitWidth);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
BOOL CList_Barcode::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CMFCListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_Barcode::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	*pResult = 0;
}

//=============================================================================
// Method		: InitHeader
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/21 - 16:11
// Desc.		:
//=============================================================================
void CList_Barcode::InitHeader()
{
	if (FALSE == m_bIntiHeader)
	{
		m_bIntiHeader = TRUE;

		int iMaxCol = m_bUseParaInfo ? WLH_MaxCol : (WLH_MaxCol - 1);

		for (int nCol = 0; nCol < iMaxCol; nCol++)
		{
			InsertColumn(nCol, g_lpszHeader[nCol], iListAglin[nCol], iHeaderWidth[nCol]);
		}
	}
}

//=============================================================================
// Method		: Set_UseParaInfo
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2017/9/21 - 21:52
// Desc.		:
//=============================================================================
void CList_Barcode::Set_UseParaInfo(__in BOOL bUse)
{
	m_bUseParaInfo = bUse;
}

//=============================================================================
// Method		: ClearWorklist
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/12 - 17:13
// Desc.		:
//=============================================================================
void CList_Barcode::ClearWorklist()
{
	this->DeleteAllItems();
}

//=============================================================================
// Method		: InsertWipID
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in const PSYSTEMTIME pstTime
// Qualifier	:
// Last Update	: 2017/9/18 - 16:43
// Desc.		:
//=============================================================================
void CList_Barcode::InsertWipID(__in LPCTSTR szBarcode, __in const PSYSTEMTIME pstTime /*= NULL*/)
{
	ASSERT(GetSafeHwnd());

	int iNewCount = GetItemCount();

	CString szTime;

	if (NULL == pstTime)
	{
		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		//szTime.Format(_T("%02d:%02d:%02d.%03d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond, tmLocal.wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		//szTime.Format(_T("%02d:%02d:%02d.%03d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond, pstTime->wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond);
	}

	InsertItem(iNewCount, _T(""));

	SetItemText(iNewCount, WLH_Time, szTime);

	SetItemText(iNewCount, WLH_Barcode, szBarcode);

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: InsertWipID
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in const PSYSTEMTIME pstTime
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/21 - 16:44
// Desc.		:
//=============================================================================
void CList_Barcode::InsertWipID(__in LPCTSTR szBarcode, __in const PSYSTEMTIME pstTime /*= NULL*/, __in UINT nParaIdx)
{
	ASSERT(GetSafeHwnd());

	int iNewCount = GetItemCount();

	CString szTime;

	if (NULL == pstTime)
	{
		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		//szTime.Format(_T("%02d:%02d:%02d.%03d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond, tmLocal.wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		//szTime.Format(_T("%02d:%02d:%02d.%03d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond, pstTime->wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond);
	}

	InsertItem(iNewCount, _T(""));

	SetItemText(iNewCount, WLH_Time, szTime);

	SetItemText(iNewCount, WLH_Barcode, szBarcode);

	if (TRUE == m_bUseParaInfo)
	{
		if (Para_Left == nParaIdx)
			SetItemText(iNewCount, WLH_Para, _T("L"));
		else
			SetItemText(iNewCount, WLH_Para, _T("R"));
	}

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: InsertWipID
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in enTestResult nJudge
// Parameter	: __in const PSYSTEMTIME pstTime
// Qualifier	:
// Last Update	: 2017/9/28 - 21:20
// Desc.		:
//=============================================================================
void CList_Barcode::InsertWipID(__in LPCTSTR szBarcode, __in enTestResult nJudge, __in const PSYSTEMTIME pstTime /*= NULL*/)
{
	ASSERT(GetSafeHwnd());

	int iNewCount = GetItemCount();

	CString szTime;

	if (NULL == pstTime)
	{
		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		//szTime.Format(_T("%02d:%02d:%02d.%03d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond, tmLocal.wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		//szTime.Format(_T("%02d:%02d:%02d.%03d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond, pstTime->wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond);
	}

	InsertItem(iNewCount, _T(""));

	SetItemText(iNewCount, WLH_Time, szTime);

	SetItemText(iNewCount, WLH_Barcode, szBarcode);

	SetItemText(iNewCount, WLH_Judge, g_TestResult[nJudge].szTextInitial);
	if ((TR_Fail == nJudge) || (TR_Check == nJudge))
	{
		
	}

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: InsertWipID
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in enTestResult nJudge
// Parameter	: __in const PSYSTEMTIME pstTime
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/28 - 21:20
// Desc.		:
//=============================================================================
void CList_Barcode::InsertWipID(__in LPCTSTR szBarcode, __in enTestResult nJudge, __in const PSYSTEMTIME pstTime, __in UINT nParaIdx)
{
	ASSERT(GetSafeHwnd());

	int iNewCount = GetItemCount();

	CString szTime;

	if (NULL == pstTime)
	{
		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		//szTime.Format(_T("%02d:%02d:%02d.%03d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond, tmLocal.wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
	}
	else
	{
		//szTime.Format(_T("%02d:%02d:%02d.%03d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond, pstTime->wMilliseconds);
		szTime.Format(_T("%02d:%02d:%02d"), pstTime->wHour, pstTime->wMinute, pstTime->wSecond);
	}

	InsertItem(iNewCount, _T(""));

	SetItemText(iNewCount, WLH_Time, szTime);

	SetItemText(iNewCount, WLH_Barcode, szBarcode);

	SetItemText(iNewCount, WLH_Judge, g_TestResult[nJudge].szTextInitial);

	if (TRUE == m_bUseParaInfo)
	{
		if (Para_Left == nParaIdx)
			SetItemText(iNewCount, WLH_Para, _T("L"));
		else
			SetItemText(iNewCount, WLH_Para, _T("R"));
	}

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}
