//*****************************************************************************
// Filename	: 	Wnd_LGETestResult_Unit.h
// Created	:	2017/9/15 - 18:42
// Modified	:	2017/9/15 - 18:42
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_LGETestResult_Unit_h__
#define Wnd_LGETestResult_Unit_h__

#pragma once

typedef enum enTestResultHeader
{
	TRH_NO,				// Number
	TRH_TestName,		// Test Name
	TRH_Judgment,		// Judgment
	TRH_Measure,		// Measure	
	TRH_SpecMin,		// Spec Min
	TRH_SpecMax,		// Spec Max
	TRH_ElapsedTime,	// Elapsed Time
	TRH_MaxEnum,
};

typedef enum enCellType
{
	TRCT_Header,
	TRCT_Data,
	TRCT_BTN,
	TRCT_MaxEnum,
};

#include "VGStatic.h"
#include "Def_TestItem_Cm.h"

//-----------------------------------------------------------------------------
// CWnd_LGETestResult_Unit
//-----------------------------------------------------------------------------
class CWnd_LGETestResult_Unit : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LGETestResult_Unit)

public:
	CWnd_LGETestResult_Unit();
	virtual ~CWnd_LGETestResult_Unit();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg	void	OnBnClick					();
	
	CVGStatic		m_st_Cell[TRH_MaxEnum];

	enCellType		m_nCellType					= TRCT_Data;
	int				m_iTotalLengthRate			= 0;
	int				m_iCtrlLength[TRH_MaxEnum];

	int				m_iBtnColumn;

	CMFCButton		m_bt_Cell;
	UINT			m_nBtnResource;

	Gdiplus::REAL	m_fFontSize					= 8.5F;

public:
	// 모든 데이터 삭제
	void		ResetItem				();
	// 검사 진행 데이터 삭제
	void		ResetTestData			();

	// 헤더 / 데이터 구분
	void		SetCellType				(__in enCellType bCellType = TRCT_Data);

	void		SetFontSize				(__in FLOAT fSize);

	// 버튼 Column 지정
// 	void		SetColumnBtn(__in UINT nColumn)
// 	{
// 		m_iBtnColumn = nColumn;
// 	}

	// 버튼 Resource
	void		SetBtnResourceID(__in UINT nResourceID)
	{
		m_nBtnResource = nResourceID;
	}

	// 번호 표시
	void		Set_Number				(__in UINT nNumber);

	// 검사 명칭
	void		Set_TestName			(__in LPCTSTR szTestName);
	// 검사 스펙
	void		Set_Spec				(__in const ST_TestItemSpec* pstSpec, __in_opt LPCTSTR szOptTestName = NULL);
	// 검사 진행 데이터
	void		Set_TestData			(__in const ST_TestItemMeas* pstTestItem, __in BOOL bTest = TRUE);

	// 현재 검사 중인 아이템 표시
	void		Set_SelectItem			(__in BOOL bSelect);
};


#endif // Wnd_LGETestResult_Unit_h__
