//*****************************************************************************
// Filename	: 	List_TestStep.cpp
// Created	:	2017/9/24 - 16:35
// Modified	:	2017/9/24 - 16:35
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// List_TestStep.cpp : implementation file
//

#include "stdafx.h"
#include "List_TestStep.h"


// CList_TestStep

typedef enum enTestStepHeader
{
	TSH_No,
	TSH_TestItem,
	TSH_Retry,
	TSH_Delay,
	TSH_MoveY,
	TSH_MoveX,
	TSH_Chart_Idx,
	TSH_Chart_Rot,
	TSH_Chart_Move_X,
	TSH_Chart_Move_Z,
	TSH_Chart_Tilt_X,
	TSH_Chart_Tilt_Z,
	TSH_Alpha,
	TSH_Alpha2nd,
	TSH_DummyHandMove,
	TSH_Board_Ctrl,
	TSH_Test_Loop,
	TSH_Chart_Check,
	TSH_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader[] =
{
	_T("No"),				// TSH_No
	_T("Test"),				// TSH_TestItem
	_T("Retry"),			// TSH_Retry
	_T("Delay"),			// TSH_Delay
	_T("Move Y"),			// TSH_MoveY
	_T("Move X"),			// TSH_MoveX
	_T("Chart Index"),		// TSH_Chart_Idx
	_T("Chart Rotation"),	// TSH_Chart_Rot
	_T("Chart X"),			// TSH_Chart_Move_X
	_T("Chart Z"),			// TSH_Chart_Move_Z
	_T("Tilt X"),			// TSH_Chart_Tilt_X
	_T("Tilt Z"),			// TSH_Chart_Tilt_Z
	_T("Alpha"),			// TSH_Alpha
	_T("Alpha2"),			// TSH_Alpha2nd
	_T("Dummy Hand"),		// TSH_DummyHandMove
	_T("Board Ctrl"),		// TSH_Board_Ctrl
	_T("Test Loop"),		// TSH_Test_Loop
	_T("Chart Check"),		// TSH_Chart_Check
	NULL
};

const int	iListAglin[] =
{
	LVCFMT_LEFT,	 // TSH_No
	LVCFMT_LEFT,	 // TSH_TestItem
	LVCFMT_LEFT,	 // TSH_Retry
	LVCFMT_LEFT,	 // TSH_Retry
	LVCFMT_LEFT,	 // TSH_MoveY	
	LVCFMT_LEFT,	 // TSH_MoveY
	LVCFMT_LEFT,	 // TSH_Chart_Idx
	LVCFMT_LEFT,	 // TSH_Chart_Rot
	LVCFMT_LEFT,	 // TSH_Chart_Move_X
	LVCFMT_LEFT,	 // TSH_Chart_Move_Z
	LVCFMT_LEFT,	 // TSH_Chart_Tilt_X
	LVCFMT_LEFT,	 // TSH_Chart_Tilt_Z
	LVCFMT_LEFT,	 // TSH_Alpha
	LVCFMT_LEFT,	 // TSH_Alpha2nd
	LVCFMT_LEFT,	 // TSH_DummyHandMove
	LVCFMT_LEFT,	 // TSH_Board_Ctrl
	LVCFMT_LEFT,	 // TSH_Test_Loop
	LVCFMT_LEFT,	 // TSH_Chart_Check
};

// 540 기준
const int	iHeaderWidth[] =
{
	40, 	// TSH_No
	400,	// TSH_TestItem
	60,		// TSH_Retry
	100,	// TSH_Delay
	30,		// TSH_MoveY
	30,		// TSH_MoveX
	0,		// TSH_Chart_Idx
	30,		// TSH_Chart_Rot
	30,		// TSH_Chart_Move_X
	30,		// TSH_Chart_Move_Z
	30,		// TSH_Chart_Tilt_X
	30,		// TSH_Chart_Tilt_Z
	30,		// TSH_Alpha
	30,		// TSH_Alpha2nd
	30,		// TSH_DummyHandMove
	30,		// TSH_Board_Ctrl
	30,		// TSH_Test_Loop
};

const int	iHeaderWidth_Foc[] =
{
	40, 	// TSH_No
	400,	// TSH_TestItem
	60,		// TSH_Retry
	100,	// TSH_Delay
	0,		// TSH_MoveY
	0,		// TSH_MoveX
	0,		// TSH_Chart_Idx
	0,		// TSH_Chart_Rot
	0,		// TSH_Chart_Move_X
	0,		// TSH_Chart_Move_Z
	0,		// TSH_Chart_Tilt_X
	0,		// TSH_Chart_Tilt_Z
	0,		// TSH_Alpha
	0,		// TSH_Alpha2nd
	0,		// TSH_DummyHandMove
	0,		// TSH_Board_Ctrl
	100,	// TSH_Test_Loop
};

// 2D Cal
const int	iHeaderWidth_2D[] =
{
	40, 	// TSH_No
	400,	// TSH_TestItem
	60,		// TSH_Retry
	100,	// TSH_Delay
	100,	// TSH_MoveY
	100,	// TSH_MoveX
	0,		// TSH_Chart_Idx
	100,	// TSH_Chart_Rot
	0,		// TSH_Chart_Move_X
	0,		// TSH_Chart_Move_Z
	0,		// TSH_Chart_Tilt_X
	0,		// TSH_Chart_Tilt_Z
	0,		// TSH_Alpha
	0,		// TSH_Alpha2nd
	0,		// TSH_DummyHandMove
	0,		// TSH_Board_Ctrl
	0,		// TSH_Test_Loop
};

const int	iHeaderWidth_ImgT[] =
{
	40, 	// TSH_No
	400,	// TSH_TestItem
	60,		// TSH_Retry
	100,	// TSH_Delay
	100,	// TSH_MoveY
	 0,		// TSH_MoveX
	 0,		// TSH_Chart_Idx
	 0,		// TSH_Chart_Rot
	 0,		// TSH_Chart_Move_X
	 0,		// TSH_Chart_Move_Z
	 0,		// TSH_Chart_Tilt_X
	 0,		// TSH_Chart_Tilt_Z
	 0,		// TSH_Alpha
	 0,		// TSH_Alpha2nd
	 0,		// TSH_DummyHandMove
	 0,		// TSH_Board_Ctrl
	 0,		// TSH_Test_Loop
};

const int	iHeaderWidth_IR_ImgT[] =
{
	40, 	// TSH_No
	400,	// TSH_TestItem
	60,		// TSH_Retry
	100,	// TSH_Delay
	0,	// TSH_MoveY
	0,		// TSH_MoveX
	0,	// TSH_Chart_Idx
	0,		// TSH_Chart_Rot
	0,		// TSH_Chart_Move_X
	0,		// TSH_Chart_Move_Z
	0,		// TSH_Chart_Tilt_X
	0,		// TSH_Chart_Tilt_Z
	0,		// TSH_Alpha
	0,		// TSH_Alpha2nd
	0,		// TSH_DummyHandMove
	0,		// TSH_Board_Ctrl
	0,		// TSH_Test_Loop
	0,		// TSH_Chart_Check
};

const int	iHeaderWidth_3D[] =
{
	40, 	// TSH_No
	400,	// TSH_TestItem
	60,		// TSH_Retry
	100,	// TSH_Delay
	100,	// TSH_MoveY
	 0,		// TSH_MoveX
	 0,		// TSH_Chart_Idx
	 0,		// TSH_Chart_Rot
	 0,		// TSH_Chart_Move_X
	 0,		// TSH_Chart_Move_Z
	 0,		// TSH_Chart_Tilt_X
	 0,		// TSH_Chart_Tilt_Z
	100,	// TSH_Alpha
	100,	// TSH_Alpha2nd
	100,	// TSH_DummyHandMove
	 0,		// TSH_Board_Ctrl
	 0,		// TSH_Test_Loop
};

const int	iHeaderWidth_Mov[] =
{
	40, 	// TSH_No
	400,	// TSH_TestItem
	60,		// TSH_Retry
	100,	// TSH_Delay
	0,		// TSH_MoveY
	0,		// TSH_MoveX
	0,		// TSH_Chart_Idx
	0,		// TSH_Chart_Rot
	0,		// TSH_Chart_Move_X
	0,		// TSH_Chart_Move_Z
	0,		// TSH_Chart_Tilt_X
	0,		// TSH_Chart_Tilt_Z
	0,		// TSH_Alpha
	0,		// TSH_Alpha2nd
	0,		// TSH_DummyHandMove
	100,	// TSH_Board_Ctrl
	0,		// TSH_Test_Loop
};

IMPLEMENT_DYNAMIC(CList_TestStep, CListCtrl)

CList_TestStep::CList_TestStep()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
// 	VERIFY(m_Font.CreateFont(
// 		16,						// nHeight
// 		0,						// nWidth
// 		0,						// nEscapement
// 		0,						// nOrientation
// 		FW_BOLD,				// nWeight
// 		FALSE,					// bItalic
// 		FALSE,					// bUnderline
// 		0,						// cStrikeOut
// 		ANSI_CHARSET,			// nCharSet
// 		OUT_DEFAULT_PRECIS,		// nOutPrecision
// 		CLIP_DEFAULT_PRECIS,	// nClipPrecision
// 		ANTIALIASED_QUALITY,	// nQuality
// 		FIXED_PITCH,			// nPitchAndFamily
// 		_T("CONSOLAS")));		// lpszFacename

	m_pHeadWidth = iHeaderWidth;
}

CList_TestStep::~CList_TestStep()
{
	m_Font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CList_TestStep, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_NOTIFY_REFLECT(NM_CLICK,	&CList_TestStep::OnNMClick)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()



// CList_TestStep message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
int CList_TestStep::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	this->GetHeaderCtrl()->EnableWindow(FALSE);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_TestStep::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	/*int iColWidth[TSH_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;
	int iMaxCol = TSH_MaxCol;

	for (int nCol = 0; nCol < iMaxCol; nCol++)
	{
		iColDivide += iHeaderWidth[nCol];
	}

	//CRect rectClient;
	//GetClientRect(rectClient);

	for (int nCol = 0; nCol < iMaxCol; nCol++)
	{
		iUnitWidth = (cx * m_pHeadWidth[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((cx * m_pHeadWidth[TSH_TestItem]) / iColDivide) + (cx - iMisc);
	SetColumnWidth(TSH_TestItem, iUnitWidth);*/
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
BOOL CList_TestStep::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_TestStep::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	*pResult = 0;
}

//=============================================================================
// Method		: InitHeader
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/21 - 16:11
// Desc.		:
//=============================================================================
void CList_TestStep::InitHeader()
{
	if (FALSE == m_bIntiHeader)
	{
		m_bIntiHeader = TRUE;

		int iMaxCol = TSH_MaxCol;

		for (int nCol = 0; nCol < iMaxCol; nCol++)
		{
			InsertColumn(nCol, g_lpszHeader[nCol], iListAglin[nCol], iHeaderWidth[nCol]);
		}
	}

	for (int nCol = 0; nCol < TSH_MaxCol; nCol++)
	{
		SetColumnWidth(nCol, m_pHeadWidth[nCol]);
	}
}

//=============================================================================
// Method		: ResetOrderingNumbers
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 16:05
// Desc.		:
//=============================================================================
void CList_TestStep::ResetOrderingNumbers()
{
	CString szText;

	for (int nIdx = 0; nIdx < GetItemCount(); nIdx++)
	{
		// TSH_No
		szText.Format(_T("%d"), nIdx + 1);
		SetItemText(nIdx, TSH_No, szText);
	}
}

//=============================================================================
// Method		: SetSelectItem
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Qualifier	:
// Last Update	: 2017/9/25 - 18:28
// Desc.		:
//=============================================================================
void CList_TestStep::SetSelectItem(__in int nItem)
{
	if (nItem < GetItemCount())
	{
		SetHotItem(nItem);
		SetItemState(nItem, LVIS_FOCUSED, LVIS_FOCUSED);
		SetItemState(nItem, LVIS_SELECTED, LVIS_SELECTED);
		SetFocus();
	}
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in int nItem
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 19:57
// Desc.		:
//=============================================================================
void CList_TestStep::InsertTestStep(__in int nItem, __in const ST_StepUnit* pTestStep)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pTestStep)
		return;

	if (GetItemCount() <= nItem)
		return;

	int iNewCount = nItem;

	InsertItem(iNewCount, _T(""));

	CString szText;

	// TSH_No
	szText.Format(_T("%d"), iNewCount);
	SetItemText(iNewCount, TSH_No, szText);

	// TSH_TestItem
	if (pTestStep->bTest)
	{
		SetItemText(iNewCount, TSH_TestItem, GetTestItemName(m_InspectionType, pTestStep->nTestItem));
	}
	else
	{
		SetItemText(iNewCount, TSH_TestItem, _T("No Test"));
	}

	// TSH_Retry
	szText.Format(_T("%d"), pTestStep->nRetryCnt);
	SetItemText(iNewCount, TSH_Retry, szText);

	// TSH_Delay
	if (0 < pTestStep->dwDelay)
	{
		szText.Format(_T("%d"), pTestStep->dwDelay);
	}
	else
	{
		szText = _T("0");
	}
	SetItemText(iNewCount, TSH_Delay, szText);

	// TSH_MoveY
	if (pTestStep->bUseMoveY)
	{
		szText.Format(_T("%d"), pTestStep->nMoveY);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_MoveY, szText);

	// TSH_MoveX
	if (pTestStep->bUseMoveX)
	{
		szText.Format(_T("%d"), pTestStep->iMoveX);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_MoveX, szText);

	// TSH_Chart_Rot
	if (pTestStep->bUseChart_Rot)
	{
		szText.Format(_T("%.2f"), pTestStep->dChart_Rot);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Rot, szText);

	// TSH_ChartIdx
	if (pTestStep->bUseChart_Rot)
	{
		szText.Format(_T("%d"), pTestStep->nChart_Idx);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Idx, szText);

	// TSH_Chart_Move_X
	if (pTestStep->bUseChart_Move_X)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Move_X);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Move_X, szText);

	// TSH_Chart_Move_Z
	if (pTestStep->bUseChart_Move_Z)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Move_Z);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Move_Z, szText);

	// TSH_Chart_Tilt_X
	if (pTestStep->bUseChart_Tilt_X)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Tilt_X);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Tilt_X, szText);

	// TSH_Chart_Tilt_Z
	if (pTestStep->bUseChart_Tilt_Z)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Tilt_Z);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Tilt_Z, szText);

	// TSH_ChartCheck
	if (pTestStep->bUseChart_Rot)
	{
		if (pTestStep->bChart_Check == FALSE)
		{
			szText.Format(_T("%s"), g_szTestLoop[0]);
		}
		else
		{
			szText.Format(_T("%s"), g_szTestLoop[1]);//pTestStep->bChart_Check);
		}

	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Check, szText);

	// TSH_Alpha
	szText.Format(_T("%d"), pTestStep->wAlpha);
	SetItemText(iNewCount, TSH_Alpha, szText);

	// TSH_Alpha2nd
	szText.Format(_T("%d"), pTestStep->wAlpha_2nd);
	SetItemText(iNewCount, TSH_Alpha2nd, szText);

	// TSH_Board_Ctrl
	SetItemText(iNewCount, TSH_Board_Ctrl, g_szBoard_Ctrl[pTestStep->nBoardCtrl]);

	// TSH_Board_Ctrl
	SetItemText(iNewCount, TSH_Test_Loop, g_szTestLoop[pTestStep->nTestLoop]);

	// 번호 재부여
	ResetOrderingNumbers();

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: InsertTestStep
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepUnit * pTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 14:22
// Desc.		:
//=============================================================================
void CList_TestStep::AddTestStep(const __in ST_StepUnit* pTestStep)
{
	ASSERT(GetSafeHwnd());
	if (NULL == pTestStep)
		return;

	int iNewCount = GetItemCount();

	InsertItem(iNewCount, _T(""));

	CString szText;

	// TSH_No
	szText.Format(_T("%d"), GetItemCount());
	SetItemText(iNewCount, TSH_No, szText);

	// TSH_TestItem
	if (pTestStep->bTest)
	{
		SetItemText(iNewCount, TSH_TestItem, GetTestItemName(m_InspectionType, pTestStep->nTestItem));
	}
	else
	{
		SetItemText(iNewCount, TSH_TestItem, _T("No Test"));
	}

	// TSH_Retry
	szText.Format(_T("%d"), pTestStep->nRetryCnt);
	SetItemText(iNewCount, TSH_Retry, szText);

	// TSH_Delay
	if (0 < pTestStep->dwDelay)
	{
		szText.Format(_T("%d"), pTestStep->dwDelay);
	}
	else
	{
		szText = _T("0");
	}
	SetItemText(iNewCount, TSH_Delay, szText);

	// TSH_MoveY
	if (pTestStep->bUseMoveY)
	{
		szText.Format(_T("%d"), pTestStep->nMoveY);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_MoveY, szText);

	// TSH_MoveX
	if (pTestStep->bUseMoveX)
	{
		szText.Format(_T("%d"), pTestStep->iMoveX);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_MoveX, szText);

	// TSH_Chart_Rot
	if (pTestStep->bUseChart_Rot)
	{
		szText.Format(_T("%.2f"), pTestStep->dChart_Rot);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Rot, szText);

	// TSH_Chart_Idx
	if (pTestStep->bUseChart_Rot)
	{
		szText.Format(_T("%d"), pTestStep->nChart_Idx);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Idx, szText);

	// TSH_ChartCheck
	if (pTestStep->bUseChart_Rot)
	{
		if (pTestStep->bChart_Check == FALSE)
		{
			szText.Format(_T("%s"), g_szTestLoop[0]);
		}
		else
		{
			szText.Format(_T("%s"), g_szTestLoop[1]);//pTestStep->bChart_Check);
		}
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Check, szText);

	// TSH_Chart_Move_X
	if (pTestStep->bUseChart_Move_X)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Move_X);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Move_X, szText);

	// TSH_Chart_Move_Z
	if (pTestStep->bUseChart_Move_Z)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Move_Z);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Move_Z, szText);

	// TSH_Chart_Tilt_X
	if (pTestStep->bUseChart_Tilt_X)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Tilt_X);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Tilt_X, szText);

	// TSH_Chart_Tilt_Z
	if (pTestStep->bUseChart_Tilt_Z)
	{
		szText.Format(_T("%d"), pTestStep->iChart_Tilt_Z);
	}
	else
	{
		szText = _T("X");
	}
	SetItemText(iNewCount, TSH_Chart_Tilt_Z, szText);


	// TSH_Alpha
	szText.Format(_T("%d"), pTestStep->wAlpha);
	SetItemText(iNewCount, TSH_Alpha, szText);

	// TSH_Alpha2nd
	szText.Format(_T("%d"), pTestStep->wAlpha_2nd);
	SetItemText(iNewCount, TSH_Alpha2nd, szText);

	// TSH_Board_Ctrl
	SetItemText(iNewCount, TSH_Board_Ctrl, g_szBoard_Ctrl[pTestStep->nBoardCtrl]);

	// TSH_Board_Ctrl
	SetItemText(iNewCount, TSH_Test_Loop, g_szTestLoop[pTestStep->nTestLoop]);

	// 화면에 보이게 하기
	EnsureVisible(iNewCount, TRUE);
	ListView_SetItemState(GetSafeHwnd(), iNewCount, LVIS_FOCUSED | LVIS_SELECTED, 0x000F);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/25 - 11:52
// Desc.		:
//=============================================================================
void CList_TestStep::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		m_pHeadWidth = iHeaderWidth_Foc;
		break;

	case Sys_Image_Test:
		m_pHeadWidth = iHeaderWidth_ImgT;
		break;

	case Sys_IR_Image_Test:
		m_pHeadWidth = iHeaderWidth_IR_ImgT;
		break;

	case Sys_2D_Cal:
		m_pHeadWidth = iHeaderWidth_2D;
		break;

	case Sys_3D_Cal:
		m_pHeadWidth = iHeaderWidth_3D;
		break;

	default:
		m_pHeadWidth = iHeaderWidth;
		break;
	}
}

//=============================================================================
// Method		: Set_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 20:35
// Desc.		:
//=============================================================================
void CList_TestStep::Set_StepInfo(__in const ST_StepInfo* pstInStepInfo)
{
	m_stStepInfo.StepList.RemoveAll();
	DeleteAllItems();

	if (NULL != pstInStepInfo)
	{
		m_stStepInfo.StepList.Copy(pstInStepInfo->StepList);

		for (UINT nIdx = 0; nIdx < pstInStepInfo->StepList.GetCount(); nIdx++)
		{
			AddTestStep(&(pstInStepInfo->StepList[nIdx]));
		}
	}
}

//=============================================================================
// Method		: Get_StepInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_StepInfo & stOutStepInfo
// Qualifier	:
// Last Update	: 2017/9/25 - 23:27
// Desc.		:
//=============================================================================
void CList_TestStep::Get_StepInfo(__out ST_StepInfo& stOutStepInfo)
{
	stOutStepInfo.StepList.RemoveAll();
	stOutStepInfo.StepList.Copy(m_stStepInfo.StepList);
}

//=============================================================================
// Method		: Item_Add
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Add(__in ST_StepUnit& stTestStep)
{
	POSITION posSel = GetFirstSelectedItemPosition();

	// 데이터 정상인가 확인
	if (m_stStepInfo.StepList.GetCount() != GetItemCount())
	{
		// 에러
		AfxMessageBox(_T("CList_TestStep::Item_Add() Data Count Error"));
		return;
	}

	if (MAX_STEP_COUNT < m_stStepInfo.StepList.GetCount())
	{
		// 에러
		AfxMessageBox(_T("Limit Max Step"));
		return;
	}

	m_stStepInfo.StepList.Add(stTestStep);
	AddTestStep(&stTestStep);
}

//=============================================================================
// Method		: Item_Insert
// Access		: public  
// Returns		: void
// Parameter	: __in ST_StepUnit & stTestStep
// Qualifier	:
// Last Update	: 2017/9/25 - 23:22
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Insert(__in ST_StepUnit& stTestStep)
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		m_stStepInfo.StepList.InsertAt(iIndex, stTestStep);

		InsertTestStep(iIndex, &stTestStep);
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Remove
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Remove()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		DeleteItem(iIndex);
		ResetOrderingNumbers();

		m_stStepInfo.StepList.RemoveAt(iIndex);

		// 아이템 선택 활성화
		if (iIndex < GetItemCount())
		{
			SetSelectItem(iIndex);
		}
		else
		{
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Up
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Up()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 0번 인덱스는 위로 이동 불가
		if ((0 < iIndex) && (1 < GetItemCount()))
		{
			ST_StepUnit stStep = m_stStepInfo.StepList.GetAt(iIndex);

			DeleteItem(iIndex);
			InsertTestStep(iIndex - 1, &stStep);

			m_stStepInfo.StepList.RemoveAt(iIndex);
			m_stStepInfo.StepList.InsertAt(iIndex - 1, stStep);

			// 아이템 선택 활성화
			SetSelectItem(iIndex - 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}

//=============================================================================
// Method		: Item_Down
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/25 - 19:17
// Desc.		:
//=============================================================================
void CList_TestStep::Item_Down()
{
	if (0 < GetSelectedCount())
	{
		POSITION nPos = GetFirstSelectedItemPosition();
		int iIndex = GetNextSelectedItem(nPos);

		// 마지막 인덱스는 아래로 이동 불가
		if ((iIndex < (GetItemCount() - 1)) && (1 < GetItemCount()))
		{
			ST_StepUnit stStep = m_stStepInfo.StepList.GetAt(iIndex);

			DeleteItem(iIndex);
			m_stStepInfo.StepList.RemoveAt(iIndex);

			// 변경되는 위치가 최하단이면, Insert 대신 Add 사용
			if ((iIndex + 1) < (GetItemCount()))
			{
				InsertTestStep(iIndex + 1, &stStep);
				m_stStepInfo.StepList.InsertAt(iIndex + 1, stStep);
			}
			else
			{
				AddTestStep(&stStep);
				m_stStepInfo.StepList.Add(stStep);
			}

			// 아이템 선택 활성화
			SetSelectItem(iIndex + 1);
		}
	}
	else
	{
		// 항목을 선택 하세요.
	}
}