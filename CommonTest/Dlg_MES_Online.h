﻿//*****************************************************************************
// Filename	: 	Dlg_MES_Online.h
// Created	:	2016/11/6 - 19:05
// Modified	:	2016/11/6 - 19:05
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Dlg_MES_Online_h__
#define Dlg_MES_Online_h__

#pragma once

#include "resource.h"
#include "Def_Enum_Cm.h"
#include "VGStatic.h"
#include "afxwin.h"
#include "Reg_Management.h"


//-----------------------------------------------------------------------------
// CDlg_MES_Online dialog
//-----------------------------------------------------------------------------
class CDlg_MES_Online : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_MES_Online)

public:
	CDlg_MES_Online(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlg_MES_Online();

// Dialog Data
	enum { IDD = IDD_DLG_MES_ONLINE };

protected:
	virtual void	DoDataExchange		(CDataExchange* pDX);    // DDX/DDV support
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreTranslateMessage	(MSG* pMsg);
	virtual BOOL	OnInitDialog		();

	afx_msg void	OnEnChangeEdPassword();
	afx_msg void	OnBnClickedRbOnlineMode();
	afx_msg void	OnBnClickedRbOfflineMode();

	virtual void	OnOK();
	virtual void	OnCancel();



	DECLARE_MESSAGE_MAP()

	CFont			m_font_Large;
	CFont			m_font_Default;

	CVGStatic			m_st_Title;

	CMFCButton			m_rb_OnlineMode;
	CMFCButton			m_rb_OfflineMode;

	CVGStatic			m_st_Password;
	CEdit				m_ed_Password;

	CButton				m_bn_OK;
	CButton				m_bn_Cancel;

	enMES_Online		m_nMES_OnlineMode;
	CReg_Management		m_regManagement;
	CString				m_szUserName;

	void		LoadOnlineMode			();
	void		SaveOnlineMode			();

	BOOL		CheckPassword			();

public:

	void		Set_MES_OnlineMode			(__in enMES_Online nOnlineMode);

	enMES_Online	Get_MES_OnlineMode()
	{
		return m_nMES_OnlineMode;
	};
	
	CString		GetUserName()
	{
		return m_szUserName;
	};
	
};

#endif // Dlg_MES_Online_h__
