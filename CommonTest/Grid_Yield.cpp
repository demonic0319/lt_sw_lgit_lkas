﻿//*****************************************************************************
// Filename	: 	Grid_Yield.cpp
// Created	:	2016/3/30 - 18:36
// Modified	:	2016/3/30 - 18:36
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Grid_Yield.h"

typedef enum
{
	IDX_Y_Header = 0,
	IDX_Y_Total,
	IDX_Y_Pass,
	IDX_Y_Fail,
	IDX_Y_Yield,
	IDX_ROW_MAX,
}enumRowHeaderYield;

static LPCTSTR lpszRowHeader[] =
{
	_T(""),
	_T("TOTAL"),	//_T("총계"),
	_T("PASS"),		//_T("정상"),
	_T("FAIL"),		//_T("불량"),
	_T("YIELD"),	//_T("수율"),
	NULL
};

static const COLORREF clrRowHeader[] =
{
	RGB(0, 0, 0),
	RGB(0, 150, 0),
	RGB(50, 50, 200),
	RGB(200, 50, 50),
	RGB(86, 86, 86)
};

typedef enum
{
	IDX_X_Header = 0,
	IDX_X_Total,
	IDX_X_Para_L,
	IDX_X_Para_R,
	IDX_COL_MAX,
}enumColHeaderYield;

static LPCTSTR lpszColHeader[] =
{
	_T(""),
	_T("EQP"),
	_T("Para-L"),
	_T("Para-R"),
	NULL
};


#define		RGB_BLACK		RGB(0x00, 0x00, 0x00)
#define		RGB_WHITE		RGB(0xFF, 0xFF, 0xFF)
#define		RGB_YELLOW		RGB(0xFF, 0xFF, 0x00)
#define		RGB_ROW_HEADER	RGB(63, 101,  169)
#define		RGB_COL_HEADER	RGB(0xFF, 200, 100)
#define		RGB_TITLE		RGB(150, 200, 0xFF)
#define		RGB_BK_ID		RGB(135, 169, 213)

#define		RGB_BIT_SET		RGB(123, 255,  75) //RGB(112, 173, 71)
#define		RGB_BIT_CLEAR	RGB(100,  10,  10) //RGB(237, 125, 49)
//#define		RGB_SELECT		RGB(0xFF, 200, 100) //RGB(150, 200, 0xFF)

//=============================================================================
//
//=============================================================================
CGrid_Yield::CGrid_Yield()
{
	SetRowColCount(IDX_ROW_MAX, IDX_COL_MAX);

	//setup the fonts
	m_font_Header.CreateFont(16, 0, 0, 0, 900, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
	m_font_Data.CreateFont(14, 0, 0, 0, 600, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
}

//=============================================================================
//
//=============================================================================
CGrid_Yield::~CGrid_Yield()
{
	m_font_Header.DeleteObject();
	m_font_Data.DeleteObject();
}

//=============================================================================
// Method		: OnSetup
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/14 - 14:30
// Desc.		:
//=============================================================================
void CGrid_Yield::OnSetup()
{
	__super::OnSetup();
}

//=============================================================================
// Method		: DrawGridOutline
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield::DrawGridOutline()
{
	CGrid_Base::DrawGridOutline();

	SetDefFont(&m_font_Data);

	// 헤더를 설정한다.
	InitHeader();
}

//=============================================================================
// Method		: CGrid_Yield::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield::CalGridOutline()
{
	//CGrid_Base::CalGridOutline();

	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();

	if ((nWidth <= 0) || (nHeight <= 0))
		return;

	// 기본 열 추가 ---------------------------------------
	// 열 너비용 비율을 만듦 ()
	int		nColWidthRate[IDX_COL_MAX] = { 23, 29, 29, 29 };
	int		iTotalRate = 0;
	for (UINT nIdx = 0; nIdx < m_nMaxCols; nIdx++)
	{
		iTotalRate += nColWidthRate[nIdx];
	}

	int		iMisc = 0;
	int		iUnitWidth = 0;
	// 기본 열 추가 ---------------------------------------
	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		iUnitWidth = (nWidth * nColWidthRate[iCol]) / iTotalRate;
		iMisc += iUnitWidth;
		SetColWidth(iCol, iUnitWidth);
	}
	// Width 계산하고 남거나 모자르는 공간 계산하여 Name 영역에 추가	
	SetColWidth(IDX_X_Header, ((nWidth * nColWidthRate[IDX_X_Header]) / iTotalRate) + nWidth - iMisc);

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight = nHeight / m_nMaxRows;
	UINT nRemindHeight = nHeight - (nUnitHeight * m_nMaxRows);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight + 1);
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight);
	}
}

//=============================================================================
// Method		: CGrid_Yield::InitRowHeader
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield::InitHeader()
{
	for (UINT iRow = 0; iRow < m_nMaxRows; iRow++)
	{
		QuickSetFont		(IDX_X_Header, iRow, &m_font_Header);
		QuickSetBackColor	(IDX_X_Header, iRow, clrRowHeader[iRow]);
		QuickSetTextColor	(IDX_X_Header, iRow, RGB_WHITE);
		QuickSetText		(IDX_X_Header, iRow, lpszRowHeader[iRow]);
	}

	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		QuickSetFont		(iCol, IDX_Y_Header, &m_font_Header);
		QuickSetBackColor	(iCol, IDX_Y_Header, RGB_BLACK);
		QuickSetTextColor	(iCol, IDX_Y_Header, RGB_WHITE);
		QuickSetText		(iCol, IDX_Y_Header, lpszColHeader[iCol]);
	}

	for (UINT iRow = 1; iRow < m_nMaxRows; iRow++)
	{
		for (UINT iCol = 1; iCol < m_nMaxCols; iCol++)
		{
			QuickSetFont		(iCol, iRow, &m_font_Data);
			QuickSetBackColor	(iCol, iRow, RGB_WHITE);
			QuickSetTextColor	(iCol, iRow, RGB_BLACK);
		}
	}
}

//=============================================================================
// Method		: OnHint
// Access		: virtual protected  
// Returns		: int
// Parameter	: int col
// Parameter	: long row
// Parameter	: int section
// Parameter	: CString * string
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
int CGrid_Yield::OnHint(int col, long row, int section, CString *string)
{
	return FALSE;
}

//=============================================================================
// Method		: CGrid_Yield::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2015/12/10 - 23:28
// Desc.		:
//=============================================================================
void CGrid_Yield::OnGetCell(int col, long row, CUGCell *cell)
{
	CGrid_Base::OnGetCell(col, row, cell);

//   	switch (row)
//   	{
//   	case IDX_Y_Header:
//   		if (IDX_X_Total == col)
//   			cell->SetBorder(cell->GetBorder() | UG_BDR_RMEDIUM | UG_BDR_BMEDIUM);
//   		break;
//   	}
}

//=============================================================================
// Method		: OnDrawFocusRect
// Access		: virtual protected  
// Returns		: void
// Parameter	: CDC * dc
// Parameter	: RECT * rect
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield::OnDrawFocusRect(CDC *dc, RECT *rect)
{

}

//=============================================================================
// Method		: SetUseSocketCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/10/10 - 19:26
// Desc.		:
//=============================================================================
void CGrid_Yield::SetUseSocketCount(__in UINT nCount)
{
	m_nSocketCount = nCount;

	if (1 < m_nSocketCount)
	{
		SetRowColCount(IDX_ROW_MAX, IDX_X_Para_L + m_nSocketCount);
	}
	else
	{
		SetRowColCount(IDX_ROW_MAX, IDX_X_Para_L);
	}

	if (GetSafeHwnd())
	{
		CalGridOutline();
	}
}

//=============================================================================
// Method		: SetYield
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_YieldInfo * pstYield
// Qualifier	:
// Last Update	: 2016/3/31 - 18:04
// Desc.		:
//=============================================================================
void CGrid_Yield::SetYield(__in const ST_YieldInfo* pstYield)
{
	CString szText;

	szText.Format(_T("%d"), pstYield->TestYield.dwTotal);
	QuickSetText(IDX_X_Total, IDX_Y_Total, szText);

	szText.Format(_T("%d"), pstYield->TestYield.dwPass);
	QuickSetText(IDX_X_Total, IDX_Y_Pass, szText);

	szText.Format(_T("%d"), pstYield->TestYield.dwFail);
	QuickSetText(IDX_X_Total, IDX_Y_Fail, szText);

	szText.Format(_T("%.2f %%"), pstYield->TestYield.fYield);
	QuickSetText(IDX_X_Total, IDX_Y_Yield, szText);

	if (1 < m_nSocketCount)
	{
		UINT nIdx = 0;
		for (UINT nCol = IDX_X_Para_L; nCol < m_nMaxCols; nCol++, nIdx++)
		{
			szText.Format(_T("%d"), pstYield->TestSocket[nIdx].dwTotal);
			QuickSetText(nCol, IDX_Y_Total, szText);

			szText.Format(_T("%d"), pstYield->TestSocket[nIdx].dwPass);
			QuickSetText(nCol, IDX_Y_Pass, szText);

			szText.Format(_T("%d"), pstYield->TestSocket[nIdx].dwFail);
			QuickSetText(nCol, IDX_Y_Fail, szText);

			szText.Format(_T("%.2f %%"), pstYield->TestSocket[nIdx].fYield);
			QuickSetText(nCol, IDX_Y_Yield, szText);
		}
	}

	RedrawRange(1, 1, m_nMaxCols, m_nMaxRows);
}

