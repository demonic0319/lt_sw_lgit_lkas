//*****************************************************************************
// Filename	: 	Wnd_LGETestResult.h
// Created	:	2017/9/15 - 18:42
// Modified	:	2017/9/15 - 18:42
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_LGETestResult_h__
#define Wnd_LGETestResult_h__

#pragma once

#include "Def_Enum_Cm.h"
#include "Def_TestItem_Cm.h"
#include "VGStatic.h"
#include "Wnd_LGETestResult_Unit.h"

//-----------------------------------------------------------------------------
// CWnd_LGETestResult
//-----------------------------------------------------------------------------
class CWnd_LGETestResult : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LGETestResult)

public:
	CWnd_LGETestResult();
	virtual ~CWnd_LGETestResult();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
		
	CFont			m_font_Default;
	CFont			m_Font;
	
	BOOL			m_bColumnBtn;
	UINT			m_nBtnResourceID;

	CWnd_LGETestResult_Unit		m_wnd_Header;
	CWnd_LGETestResult_Unit		m_wnd_Data[MAX_STEP_COUNT];

	// UI 표시용 검사 항목 갯수
	UINT					m_nTestItemCount = MAX_STEP_COUNT;

	// 검사기 설정
	enInsptrSysType			m_InspectionType	= Sys_Focusing;

	// 스텝 정보
	const ST_StepInfo*		m_pstInStepInfo		= NULL;
	// 검사항목 정보
	const ST_TestItemInfo*	m_pstInTestItemInfo	= NULL;

	// 선택된 아이템 인덱스
	INT						m_iSelectIndex		= -1;

public:

	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	void		SetFontSize				(__in FLOAT fSize);

	// 모든 데이터 삭제
	void		DeleteItemAll			();
	// 검사 측정 관련 데이터 초기화
	void		ResetTestData			();

	// 검사 아이템 갯수 설정
	void		Set_TestItemCount		(__in UINT nCount);
	//
	void		Set_TestItem			(__in UINT nItemIdx);

	// 검사 정보 설정
	void		Set_TestInfo			(__in const ST_StepInfo* pstInStepInfo, __in const ST_TestItemInfo* pstInTestItemInfo);
	
	// 아이템 선택
	void		SelectItem				(__in UINT nItemIdx);

	void		Set_Result				(__in UINT nItemIdx, __in const ST_TestItemMeas* pMeasInfo, __in BOOL bTest = TRUE);

	// 버튼 타입으로 설정
	void		SetColumnBtnUse(__in	BOOL bUse)
	{
		m_bColumnBtn = bUse;
	}

	void		SetBtnResourceID(__in UINT nResourceID)
	{
		m_nBtnResourceID = nResourceID;
	}
};


#endif // Wnd_LGETestResult_h__
