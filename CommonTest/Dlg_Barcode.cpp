﻿//*****************************************************************************
// Filename	: 	Dlg_Barcode.cpp
// Created	:	2016/11/1 - 16:59
// Modified	:	2016/11/1 - 16:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Dlg_Barcode.cpp : implementation file
//

#include "stdafx.h"
#include "Dlg_Barcode.h"
#include "afxdialogex.h"
#include "CommonFunction.h"


#define IDC_ED_BARCODE_01		1001

#define		TABSTYLE_COUNT			1
static UINT g_TabOrder[TABSTYLE_COUNT] =
{
	1001
};

static LPCTSTR g_szBarcodeType[] = 
{
	_T("Input Barcode"),		// Barcode_SN,
	_T("Input WIP-ID"),			// Barcode_WIP_ID
	_T("Input Pallet ID"),		// Barcode_Pallet_ID,
	_T("Input Part No"),		// Barcode_PartNo
};

// CDlg_Barcode dialog

IMPLEMENT_DYNAMIC(CDlg_Barcode, CDialogEx)

CDlg_Barcode::CDlg_Barcode(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlg_Barcode::IDD, pParent)
{
	VERIFY(m_font_Large.CreateFont(
		42,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	VERIFY(m_font_Default.CreateFont(
		24,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	m_nBarcodeLength	= 15; // WIP-ID 길이 15자리

	m_bIsModal			= FALSE;
	m_bChk_BarcodeLength= TRUE;
}

CDlg_Barcode::~CDlg_Barcode()
{
	m_font_Large.DeleteObject();
	m_font_Default.DeleteObject();
}

void CDlg_Barcode::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlg_Barcode, CDialogEx)
	ON_WM_SIZE()
	ON_WM_CREATE()
	ON_WM_SHOWWINDOW()
	ON_WM_GETMINMAXINFO()
END_MESSAGE_MAP()


// CDlg_Barcode message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/11/4 - 11:45
// Desc.		:
//=============================================================================
int CDlg_Barcode::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	CString szText;
	m_st_Site.SetBackColor_COLORREF(RGB(0, 0, 0));
	m_st_Site.SetTextColor(Color::White, Color::White);
	m_st_Site.SetFont_Gdip(L"arial", 18.0F);
	m_st_Status.SetFont_Gdip(L"arial", 10.0F);

	szText = g_szBarcodeType[m_nBarcodeType];
	m_st_Site.Create(szText, dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_ed_Barcode.Create(dwStyle | WS_TABSTOP | WS_BORDER | ES_CENTER | ES_AUTOHSCROLL, rectDummy, this, IDC_ED_BARCODE_01);
	m_st_Status.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_Barcode.SetFont(&m_font_Default);
	m_ed_Barcode.SetFont(&m_font_Large);

	//MoveWindow(0, 0, 600, 300);
	SetWindowPos(this, 0, 0, 600, 280, SWP_HIDEWINDOW);
	CenterWindow();
	
	
	m_bn_OK.Create(_T("OK"), dwStyle | WS_TABSTOP | BS_PUSHBUTTON, rectDummy, this, IDOK);
	m_bn_Cancel.Create(_T("Cancel"), dwStyle | WS_TABSTOP | BS_PUSHBUTTON, rectDummy, this, IDCANCEL);

	m_ed_Barcode.SetFocus();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/11/4 - 11:45
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;
	int iCtrlWidth	= iWidth;
	int iCtrlHeight = 50;
	int iSubLeft	= iLeft;

	iTop = iMagrin;
	m_st_Site.MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);
	iTop += iCtrlHeight + iSpacing;
	m_ed_Barcode.MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);
	iTop += iCtrlHeight + iSpacing;
	m_st_Status.MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);

	iSubLeft += iCtrlWidth + iSpacing;

	iTop += iCtrlHeight + iCateSpacing;
	
	iCtrlWidth = 240;

	iTop = cy - iMagrin - iCtrlHeight;
	m_bn_OK.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft = cx - iMagrin - iCtrlWidth;
	m_bn_Cancel.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);
}

//=============================================================================
// Method		: OnGetMinMaxInfo
// Access		: protected  
// Returns		: void
// Parameter	: MINMAXINFO * lpMMI
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	lpMMI->ptMaxTrackSize.x = 600;
	lpMMI->ptMaxTrackSize.y = 280;

	lpMMI->ptMinTrackSize.x = 600;
	lpMMI->ptMinTrackSize.y = 280;

	CDialogEx::OnGetMinMaxInfo(lpMMI);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::PreCreateWindow(CREATESTRUCT& cs)
{
	ModifyStyle(0, WS_SIZEBOX);

	return CDialogEx::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if (VK_TAB == pMsg->wParam)
		{
			int nID = GetFocus()->GetDlgCtrlID();
			for (UINT iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
			{
				if (nID == g_TabOrder[iCnt])
				{
					if ((TABSTYLE_COUNT - 1) == iCnt)
					{
						nID = IDOK;
					}
					else
					{
						nID = g_TabOrder[iCnt + 1];
					}

					break;
				}
				else if (IDOK == nID)
				{
					nID = IDCANCEL;
					break;
				}
				else if (IDCANCEL == nID)
				{
					nID = g_TabOrder[0];
					break;
				}
			}

			GetDlgItem(nID)->SetFocus();
		}
		else if (pMsg->wParam == VK_RETURN)
		{
// 			int nID = GetFocus()->GetDlgCtrlID();
// 			if ((IDOK != nID) && (IDCANCEL != nID))
// 			{
// 				for (UINT iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
// 				{
// 					if (nID == g_TabOrder[iCnt])
// 					{
// 						if ((TABSTYLE_COUNT - 1) == iCnt)
// 						{
// 							nID = g_TabOrder[0];
// 						}
// 						else
// 						{
// 							nID = g_TabOrder[iCnt + 1];
// 						}
// 
// 						break;
// 					}
// 				}
// 
// 				GetDlgItem(nID)->SetFocus();
// 			}

			OnOK();
			return TRUE;
		}
		else if (pMsg->wParam == VK_ESCAPE)
		{
			// 여기에 ESC키 기능 작성       
			return TRUE;
		}
		break;

	default:
		break;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnInitDialog
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/11/5 - 20:03
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	if (m_bIsModal)
	{
		m_st_Status.ShowWindow(SW_HIDE);
	}

	m_ed_Barcode.SetFocus();
	//GotoDlgCtrl(&m_ed_Barcode);

	//return TRUE;  // return TRUE unless you set the focus to a control
	return FALSE;
	// EXCEPTION: OCX Property Pages should return FALSE
}

//=============================================================================
// Method		: OnOK
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/16 - 16:43
// Desc.		:
//=============================================================================
void CDlg_Barcode::OnOK()
{
	CString szValue;

	m_ed_Barcode.GetWindowText(szValue);

	m_szBarcode = szValue;
	
	//ShowWindow(SW_HIDE);
	CDialogEx::OnOK();
}

//=============================================================================
// Method		: DoModal
// Access		: virtual protected  
// Returns		: INT_PTR
// Qualifier	:
// Last Update	: 2016/11/18 - 12:25
// Desc.		:
//=============================================================================
INT_PTR CDlg_Barcode::DoModal()
{
	m_bIsModal = TRUE;

	return CDialogEx::DoModal();
}

//=============================================================================
// Method		: CheckBarcode
// Access		: protected  
// Returns		: enBarcodeChk
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/11/4 - 11:53
// Desc.		:
//=============================================================================
enBarcodeChk CDlg_Barcode::CheckBarcode(__in LPCTSTR szBarcode)
{
	if (m_bChk_BarcodeLength)
	{
		//if (m_nBarcodeLength != _tcslen(szBarcode))
		//if (16 < _tcslen(szBarcode))
		if (32 < _tcslen(szBarcode))
		{
			return Barcode_Length;
		}
	}

	//iCnt = (iCnt < m_nBarcodeCnt) ? iCnt : m_nBarcodeCnt;
	//Barcode_Length


	return Barcode_Pass;
}

//=============================================================================
// Method		: SetStatus
// Access		: protected  
// Returns		: void
// Parameter	: __in enBarcodeChk nStatus
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
void CDlg_Barcode::SetStatus(__in enBarcodeChk nStatus)
{
	m_st_Status.SetText(g_szBarcodeChk[nStatus]);

	if (Barcode_Pass == nStatus)
	{
		m_st_Status.SetBackColor(RGB(0, 176,  80));
	}
	else
	{
		m_st_Status.SetBackColor(RGB(192, 0, 0));
	}
}

//=============================================================================
// Method		: SetBarcodeLength
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nLength
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2017/2/17 - 17:00
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::SetBarcodeLength(__in UINT nLength, __in BOOL bUse /*= TRUE*/)
{
	m_nBarcodeLength = nLength;

	m_bChk_BarcodeLength = bUse;

	return TRUE;
}

//=============================================================================
// Method		: GetBarcode
// Access		: public  
// Returns		: CString
// Parameter	: __in CString & szBarcodez
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
CString CDlg_Barcode::GetBarcode()
{
	return m_szBarcode;
}

//=============================================================================
// Method		: InsertBarcode
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
BOOL CDlg_Barcode::InsertBarcode(__in LPCTSTR szBarcode)
{
	enBarcodeChk enError = CheckBarcode(szBarcode);
	
	SetStatus(enError);

	if (Barcode_Pass == enError)
	{
		m_szBarcode = szBarcode;

		if (GetSafeHwnd())
		{
			m_ed_Barcode.SetWindowText(szBarcode);
		}

		// 정해진 바코드가 모두 입력되면 2초후에 윈도우 숨김
		Delay(1000);

		ShowWindow(SW_HIDE);
		return TRUE;
	}
	else if (Barcode_Overap == enError)
	{
		Delay(2000);

		ShowWindow(SW_HIDE);
		return TRUE;
	}
	else
	{
		//Error;
		return FALSE;
	}

	return FALSE;
}

//=============================================================================
// Method		: ResetBarcode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/5 - 14:22
// Desc.		:
//=============================================================================
void CDlg_Barcode::ResetBarcode()
{
	m_szBarcode.Empty();

	m_ed_Barcode.SetWindowText(_T(""));
	m_st_Status.SetText(_T(""));
	m_st_Status.SetBackColor(RGB(255, 255, 255));

}

//=============================================================================
// Method		: SetBarcodeType
// Access		: public  
// Returns		: void
// Parameter	: __in enBarcodeType nBarcodeType
// Qualifier	:
// Last Update	: 2017/10/25 - 21:53
// Desc.		:
//=============================================================================
void CDlg_Barcode::SetBarcodeType(__in enBarcodeType nBarcodeType)
{
	if (m_nBarcodeType != nBarcodeType)
	{
		m_nBarcodeType = nBarcodeType;

		if (GetSafeHwnd())
		{
			m_st_Site.SetText(g_szBarcodeType[m_nBarcodeType]);
		}
	}
}
