//*****************************************************************************
// Filename	: 	Def_T_LGE_Common.h
// Created	:	2017/10/25 - 10:51
// Modified	:	2017/10/25 - 10:51
//
// Author	:	PiRing
//	
// Purpose	:	LGE 2D Cal & 3D Cal 파라미터 및 결과값 구조체 및 상수
//*****************************************************************************
#ifndef Def_T_LGE_Common_h__
#define Def_T_LGE_Common_h__

#pragma pack(push,1)

#define		MAX_CORNER					40	
#define		MAX_IMAGES_INTR				15	
#define		MAX_IMAGES_DIST				10	
#define		MAX_IMAGES_EVAL				 3	
//#define		OMSRESULT_ACCURACYMAP_SIZE	30	
#define		SHADING_SIZE				49	

typedef enum enCaliblrationType
{
	CalType_2D,
	CalType_3D,
	CalType_Evaluation,
	CalType_2DCornerPoint,
};

// typedef enum enAlphaType
// {
// 	Alpha_400	= 400,
// 	Alpha_150	= 150,
// 	Alpha_140	= 140,
// 	Alpha_130	= 130,
// };

//-------------------------------------------------------------------
// 2D CAL ROI 정보
//-------------------------------------------------------------------
typedef struct _tag_ST_LGE_ROI
{
	int	nU;			/*0*/
	int	nV;			/*0*/
	int	nW;			/*640/4*/
	int	nH;			/*480/4*/

	_tag_ST_LGE_ROI()
	{
		Reset();
	};

	_tag_ST_LGE_ROI& operator= (const _tag_ST_LGE_ROI& ref)
	{
		nU = ref.nU;
		nV = ref.nV;
		nW = ref.nW;
		nH = ref.nH;

		return *this;
	};

	void Reset()
	{
		nU = 50;
		nV = 25;
		nW = 60;
		nH = 70;
	};

}ST_LGE_ROI, *PST_LGE_ROI;

typedef struct _tag_ST_LGE_CORNER_PT
{
	float	fU;			/*0*/
	float	fV;			/*0*/

	_tag_ST_LGE_CORNER_PT()
	{
		Reset();
	};

	_tag_ST_LGE_CORNER_PT& operator= (const _tag_ST_LGE_CORNER_PT& ref)
	{
		fU = ref.fU;
		fV = ref.fV;

		return *this;
	};

	void Reset()
	{
		fU = 0;
		fV = 0;
	};

}ST_LGE_CORNER_PT, *PST_LGE_CORNER_PT;

#pragma pack(pop)

#endif // Def_T_LGE_Common_h__
