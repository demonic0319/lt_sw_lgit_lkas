﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Preset.h
// Created	:	2018/4/4 - 13:57
// Modified	:	2018/4/4 - 13:57
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Wnd_Cfg_Preset_h__
#define Wnd_Cfg_Preset_h__

#pragma once

#include "VGStatic.h"
#include "CommonFunction.h"
#include "Def_DataStruct_Cm.h"

//=============================================================================
// CWnd_Cfg_Preset
//=============================================================================
class CWnd_Cfg_Preset : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_Preset)

public:
	CWnd_Cfg_Preset();
	virtual ~CWnd_Cfg_Preset();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);	
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	afx_msg void	OnRangeChkCtrl	(UINT nID);

	// 검사기 설정
	enInsptrSysType		m_InspectionType	= enInsptrSysType::Sys_Image_Test;

	CFont				m_font;

	// 전체 검사 불량 시 Calibration Flag 변경 후 재 측정 기능 사용 여부
	CVGStatic			m_st_UsePresetMode;
	CComboBox			m_cb_UsePresetMode;

	CMFCButton			m_chk_Item[MAX_TESTSTEP_PRESET];
	CVGStatic			m_st_Item[MAX_TESTSTEP_PRESET];	
	CComboBox			m_cb_Item[MAX_TESTSTEP_PRESET];
	CMFCMaskedEdit		m_ed_Item[MAX_TESTSTEP_PRESET];

	UINT				m_nUsePresetCount	= MAX_TESTSTEP_PRESET;

public:

	// 검사기 종류 설정
	void	SetSystemType		(__in enInsptrSysType nSysType);

	void	Set_PresetInfo		(__in const ST_PresetStepInfo* pstPresetInfo);
	void	Get_PresetInfo		(__out ST_PresetStepInfo& stPresetInfo);

	// 사용하는 Preset 개수와 Perset 명칭
	void	Set_UsePresetCount	(__in UINT nUseCount);
};

#endif // Wnd_Cfg_Preset_h__
