﻿//*****************************************************************************
// Filename	: 	File_MES.h
// Created	:	2016/11/8 - 16:29
// Modified	:	2016/11/8 - 16:29
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_MES_h__
#define File_MES_h__

#pragma once

#include "Def_MES_Cm.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CFile_MES
{
public:
	CFile_MES();
	~CFile_MES();

protected:

	CString				m_szPgmName;
	CString				m_szEquipmentID;
	CString				m_szPath;

	virtual CString	Make_MES_Result_Header			(__in const CStringArray* pAddHeaderz = NULL);
	CString			Make_MES_Result					(__in const ST_MES_FinalResult* pstWorklist);

	virtual CString	Make_FinalResult_Header			(__in const CStringArray* pAddHeaderz = NULL);
	CString			Make_FinalResult				(__in const ST_MES_FinalResult* pstWorklist);

	virtual CString	Make_TestItemLog_Header			(__in const CStringArray* pAddHeaderz = NULL);
	CString			Make_TestItemLog				(__in const ST_MES_TestItemLog* pstWorklist);

public:
	// SYSTEMTIME -> 2016-09-09 15:04:19.030
	CString			Conv_SYSTEMTIME2String			(__in SYSTEMTIME* pTime);
	
	// 설비 ID 설정
	void			SetEquipmentID					(__in LPCTSTR szEqpModel)
	{
		m_szEquipmentID = szEqpModel;
	};

	// 설비 ID 구하기
	CString			GetEquipmentID()
	{
		return m_szEquipmentID;
	};

	// 세코닉스 MES 파일 저장 C:\\MES\제품임시바코드.csv
	virtual BOOL	Save_MES_Result					(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist);
	BOOL			Save_MES_Result_List			(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist);
	
	// 최종 결과 저장
	virtual BOOL	Save_FinalResult				(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist);
	BOOL			Save_FinalResult_List			(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist);

	// 검사 항목별 결과 저장
	virtual BOOL	Save_TestItemLog				(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in BOOL bAddSpaceLine = FALSE);
	BOOL			Save_TestItemLog_List			(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in const ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in BOOL bAddSpaceLine = FALSE);

	// EEPROM 데이터 저장
	CString			Make_EEPROM_String				(__in ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in DWORD dwStartAddr, __in DWORD dwEndAddr, __in char* pDataz, __in DWORD dwLength);
	virtual BOOL	Save_EEPROM_Log					(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in DWORD dwStartAddr, __in DWORD dwEndAddr, __in char* pDataz, __in DWORD dwLength);

	// 이미지 저장 전체 경로 구하는 함수
	CString			GetMESImage_FullPath			(__in LPCTSTR szPath, __in LPCTSTR szRecipe, __in LPCTSTR szLotID, __in LPCTSTR szSensorID, __in LPCTSTR szTestItem, __in LPCTSTR szBarcode, __in SYSTEMTIME* pTime, __in enMES_ImgFormat nFormat,  __in LPCTSTR szAddName = NULL);
	// 로그 저장 전체 경로 구하는 함수
	CString			GetMESLog_FullPath				(__in LPCTSTR szPath, __in LPCTSTR szRecipe, __in LPCTSTR szLotID, __in SYSTEMTIME* pTime, __in LPCTSTR szInFilename);

	// 컴퓨터 이름 구하는 함수
	CString			GetStationName					();
	// 프로그램 버전 구하는 함수
	CString			GetSWVersion					(__in LPCTSTR szPgmName);
	

	CString			Get_Report_Yield_FullPath		(__in LPCTSTR szPath, __in CString Model, __in CString LotName, __in SYSTEMTIME *pTime);
};

#endif // File_MES_h__

