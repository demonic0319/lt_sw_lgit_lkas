//*****************************************************************************
// Filename	: 	Wnd_LGETestResult.cpp
// Created	:	2017/9/15 - 18:43
// Modified	:	2017/9/15 - 18:43
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_LGETestResult.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_LGETestResult.h"
#include "CommonFunction.h"

// CWnd_LGETestResult

#define		IDC_BN_AUTO			1001
#define		IDC_BN_NORMAL		1002

#define		IDC_BTN_COLUMN		2000

static LPCTSTR g_szHeader[] =
{
	_T("NO"),			// TRH_NO,			
	_T("Test Name"),	// TRH_TestName,	
	_T("Judgment"),		// TRH_Judgment,	
	_T("Measure"),		// TRH_Measure,	
	_T("Spec Min"),		// TRH_SpecMin,	
	_T("Spec Max"),		// TRH_SpecMax,	
	_T("Elapsed Time"),	// TRH_ElapsedTime
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_LGETestResult, CWnd)

CWnd_LGETestResult::CWnd_LGETestResult()
{
	m_nBtnResourceID	= 0;
	m_bColumnBtn		= FALSE;
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_Font.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_LGETestResult::~CWnd_LGETestResult()
{
	m_font_Default.DeleteObject();
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_LGETestResult, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_LGETestResult message handlers

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/15 - 19:28
// Desc.		:
//=============================================================================
int CWnd_LGETestResult::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_wnd_Header.SetCellType(enCellType::TRCT_Header);
	if (!m_wnd_Header.Create(NULL, _T(""), dwStyle, rectDummy, this, 10))
	{
		TRACE(_T("윈도우 생성을 실패했습니다.\n"));
	}

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_wnd_Data[nIdx].SetBtnResourceID(m_nBtnResourceID + nIdx);

		if (m_bColumnBtn)
			m_wnd_Data[nIdx].SetCellType(enCellType::TRCT_BTN);

		if (!m_wnd_Data[nIdx].Create(NULL, _T(""), dwStyle, rectDummy, this, 20 + nIdx))
		{
			TRACE(_T("윈도우 생성을 실패했습니다.\n"));
		}

		m_wnd_Data[nIdx].Set_Number(nIdx + 1);
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/15 - 19:29
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;
	int iCellWidth	= iWidth;
	int iCellHeight = iHeight + (m_nTestItemCount);
	int iCtrlHeight[MAX_STEP_COUNT + 1] = { 0, };
	int iRemind		= 0;

	for (UINT nIdx = 0; nIdx < (m_nTestItemCount + 1); nIdx++)
	{
		iCtrlHeight[nIdx] = iCellHeight / (m_nTestItemCount + 1);
		iRemind += iCtrlHeight[nIdx];
	}
	iRemind = iCellHeight - iRemind;

	for (int iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		iCtrlHeight[iIdx - 1] += 1;
	}
	
	m_wnd_Header.MoveWindow(iLeft, iTop, iCellWidth, iCtrlHeight[m_nTestItemCount]);
	iTop += (iCtrlHeight[m_nTestItemCount] - 1);
	for (UINT nIdx = 0; nIdx < m_nTestItemCount; nIdx++)
	{
		m_wnd_Data[nIdx].MoveWindow(iLeft, iTop, iCellWidth, iCtrlHeight[nIdx]);
		iTop += (iCtrlHeight[nIdx] - 1);
	}

	for (UINT nIdx = m_nTestItemCount; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_wnd_Data[nIdx].MoveWindow(0, 0, 0, 0);
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/25 - 23:49
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}

void CWnd_LGETestResult::SetFontSize(__in FLOAT fSize)
{
	m_wnd_Header.SetFontSize(fSize);

	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_wnd_Data[nIdx].SetFontSize(fSize);
	}
}

//=============================================================================
// Method		: DeleteItemAll
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/22 - 20:27
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::DeleteItemAll()
{
	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_wnd_Data[nIdx].ResetItem();
	}

	Delay(100);
}

//=============================================================================
// Method		: ResetTestData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/26 - 14:26
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::ResetTestData()
{
	for (UINT nIdx = 0; nIdx < MAX_STEP_COUNT; nIdx++)
	{
		m_wnd_Data[nIdx].ResetTestData();
	}

	if (0 <= m_iSelectIndex)
 	{
		//m_wnd_Data[m_iSelectIndex].Set_SelectItem(FALSE);
		m_iSelectIndex = -1;
	}
}

//=============================================================================
// Method		: Set_TestItemCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/9/17 - 18:02
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::Set_TestItemCount(__in UINT nCount)
{
	if (nCount < MAX_STEP_COUNT)
	{
		m_nTestItemCount = nCount;
	}
	else
	{
		m_nTestItemCount = MAX_STEP_COUNT;
	}

	// 다시 그리기
	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
		//SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
	}
}

void CWnd_LGETestResult::Set_TestItem(__in UINT nItemIdx)
{

}

//=============================================================================
// Method		: Set_TestInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_StepInfo * pstInStepInfo
// Parameter	: __in const ST_TestItemInfo * pstInTestItemInfo
// Qualifier	:
// Last Update	: 2017/9/27 - 0:13
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::Set_TestInfo(__in const ST_StepInfo* pstInStepInfo, __in const ST_TestItemInfo* pstInTestItemInfo)
{
	if (NULL == pstInStepInfo)
		return;

	if (NULL == pstInTestItemInfo)
		return;

	DeleteItemAll();	

	// 스텝정보 설정
	m_pstInStepInfo = pstInStepInfo;

	// 검사 항목 정보 설정
	m_pstInTestItemInfo = pstInTestItemInfo;

	CString szText;
	INT_PTR iStepCount = pstInStepInfo->GetCount();
	for (UINT nIdx = 0; nIdx < iStepCount; nIdx++)
	{
		if (pstInStepInfo->StepList[nIdx].bTest)	// 검사 항목일 경우
		{
			//szText = GetTestItemName(m_InspectionType, pstInStepInfo->StepList[nIdx].nTestItem);
			
			if (pstInStepInfo->StepList[nIdx].nTestItem < pstInTestItemInfo->TestItemList.GetCount())
			{
				switch (m_InspectionType)
				{
				case Sys_Focusing:
				case Sys_Image_Test:
				case Sys_IR_Image_Test:
				{
					CStringArray szOptTestName;

					if (TRUE == pstInStepInfo->StepList[nIdx].bUseChart_Rot)
					{
						szText.Format(_T("%d"), pstInStepInfo->StepList[nIdx].nChart_Idx);
						szOptTestName.Add(szText);
					}

					m_wnd_Data[nIdx].Set_Spec(&pstInTestItemInfo->TestItemList[pstInStepInfo->StepList[nIdx].nTestItem]);
				}
					break;

				case Sys_2D_Cal:
				{
					CStringArray szOptTestName;

					if (pstInStepInfo->StepList[nIdx].bUseMoveY)
					{
						szText.Format(_T("Y:%d"), pstInStepInfo->StepList[nIdx].nMoveY);
						szOptTestName.Add(szText);
					}

					if (TRUE == pstInStepInfo->StepList[nIdx].bUseChart_Rot)
					{
						szText.Format(_T("%.2f"), pstInStepInfo->StepList[nIdx].dChart_Rot);
						szOptTestName.Add(szText);
					}

					if (pstInStepInfo->StepList[nIdx].bUseMoveX)
					{
						szText.Format(_T("X:%d"), pstInStepInfo->StepList[nIdx].iMoveX);
						szOptTestName.Add(szText);
					}

					UINT nCount = (Unit)szOptTestName.GetCount();
					if (0 < nCount)
					{
						szText.Empty();

						for (UINT nCntIdx = 0; nCntIdx < (nCount - 1); nCntIdx++)
						{
							szText += szOptTestName.GetAt(nCntIdx);
							szText += _T(", ");
						}
						szText += szOptTestName.GetAt(nCount - 1);
						m_wnd_Data[nIdx].Set_Spec(&pstInTestItemInfo->TestItemList[pstInStepInfo->StepList[nIdx].nTestItem], szText.GetBuffer(0));
					}
					else
					{
						m_wnd_Data[nIdx].Set_Spec(&pstInTestItemInfo->TestItemList[pstInStepInfo->StepList[nIdx].nTestItem]);
					}
				}
					break;
				
				case Sys_3D_Cal:
				{
					if (pstInStepInfo->StepList[nIdx].bUseMoveY)	// 2D Cal, 3D Cal 에서만 사용
					{
						szText.Format(_T("Y:%d"), pstInStepInfo->StepList[nIdx].nMoveY);
						m_wnd_Data[nIdx].Set_Spec(&pstInTestItemInfo->TestItemList[pstInStepInfo->StepList[nIdx].nTestItem], szText.GetBuffer(0));
					}
					else
					{
						m_wnd_Data[nIdx].Set_Spec(&pstInTestItemInfo->TestItemList[pstInStepInfo->StepList[nIdx].nTestItem]);
					}
				}
					break;

				default:
				{
					m_wnd_Data[nIdx].Set_Spec(&pstInTestItemInfo->TestItemList[pstInStepInfo->StepList[nIdx].nTestItem]);
				}
					break;
				}
			}
			else
			{
				TRACE(_T("Error : CWnd_LGETestResult::Set_TestInfo -> (pstInStepInfo->StepList[nIdx].nTestItem < pstInTestItemInfo->TestItemList)\n"));
			}
		}
		else // End of -> if (pstInStepInfo->StepList[nIdx].bTest)
		{
			CString szPreName = _T("No Test");
// 			if ((Sys_Focusing == m_InspectionType) || (Sys_Stereo_Cal == m_InspectionType))
// 			{
// 				// CAN Init, CAN Final
// 				if (enBoardCtrl::BrdCtrl_NotUse != pstInStepInfo->StepList[nIdx].nBoardCtrl)
// 				{
// 					szPreName = g_szBoard_Ctrl[pstInStepInfo->StepList[nIdx].nBoardCtrl];
// 				}
// 			}

			CStringArray szOptTestName;

			if (pstInStepInfo->StepList[nIdx].bUseMoveY)
			{
				szText.Format(_T("Y:%d"), pstInStepInfo->StepList[nIdx].nMoveY);
				szOptTestName.Add(szText);
			}
				
			if (TRUE == pstInStepInfo->StepList[nIdx].bUseChart_Rot)
			{
				szText.Format(_T("%.2f"), pstInStepInfo->StepList[nIdx].dChart_Rot);
				szOptTestName.Add(szText);
			}

			if (pstInStepInfo->StepList[nIdx].bUseMoveX)
			{
				szText.Format(_T("X:%d"), pstInStepInfo->StepList[nIdx].iMoveX);
				szOptTestName.Add(szText);
			}
				
			UINT nCount = (Unit)szOptTestName.GetCount();
			if (0 < nCount)
			{
				szText = szPreName + _T("(");

				for (UINT nCntIdx = 0; nCntIdx < (nCount - 1); nCntIdx++)
				{
					szText += szOptTestName.GetAt(nCntIdx);
					szText += _T(", ");
				}
				szText += szOptTestName.GetAt(nCount - 1);
				szText += _T(")");
			}
			else
			{
				szText = szPreName;
			}

			m_wnd_Data[nIdx].Set_TestName(szText.GetBuffer(0));
		}
	}
}

//=============================================================================
// Method		: SelectItem
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nItemIdx
// Qualifier	:
// Last Update	: 2017/9/27 - 17:20
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::SelectItem(__in UINT nItemIdx)
{
	if (m_iSelectIndex != nItemIdx)
	{
		if (0 <= m_iSelectIndex)
		{
			m_wnd_Data[m_iSelectIndex].Set_SelectItem(FALSE);
		}

		m_iSelectIndex = nItemIdx;

		m_wnd_Data[m_iSelectIndex].Set_SelectItem(TRUE);
	}
}

//=============================================================================
// Method		: Set_Result
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nItemIdx
// Parameter	: __in const ST_TestItemMeas * pMeasInfo
// Parameter	: __in BOOL bTest
// Qualifier	:
// Last Update	: 2017/11/9 - 13:29
// Desc.		:
//=============================================================================
void CWnd_LGETestResult::Set_Result(__in UINT nItemIdx, __in const ST_TestItemMeas* pMeasInfo, __in BOOL bTest /*= TRUE*/)
{
	m_wnd_Data[nItemIdx].Set_TestData(pMeasInfo, bTest);
}

