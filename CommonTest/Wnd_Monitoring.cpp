//*****************************************************************************
// Filename	: 	Wnd_Monitoring.cpp
// Created	:	2018/3/3 - 9:49
// Modified	:	2018/3/3 - 9:49
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Monitoring.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Monitoring.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_Monitoring

static LPCTSTR g_szConsumItem[] =
{
	_T("Chart [mm] "),
	_T("Chart TX [��]"),
	_T("Chart TZ [��]"),
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_Monitoring, CWnd_BaseView)

CWnd_Monitoring::CWnd_Monitoring()
{
	VERIFY(m_Font.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Monitoring::~CWnd_Monitoring()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_Monitoring, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_Monitoring message handlers

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/15 - 19:28
// Desc.		:
//=============================================================================
int CWnd_Monitoring::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < MI_MaxEnum; nIdx++)
	{
		m_st_Caption[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Caption[nIdx].SetFont_Gdip(L"Arial", 10.0F);

		m_st_Value[nIdx].SetColorStyle(CVGStatic::ColorStyle_White);
		m_st_Value[nIdx].SetFont_Gdip(L"Arial", 10.0F);

		m_st_Caption[nIdx].Create(g_szConsumItem[nIdx], WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
		m_st_Value[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/15 - 19:29
// Desc.		:
//=============================================================================
void CWnd_Monitoring::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;
	int iHalfWidth	= (iWidth - iSpacing) / 2;
	int iCapWidth	= 90;
	int iValWidth	= iWidth - iCapWidth;
	UINT iLimitRow	=  MI_MaxEnum;

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		iLimitRow = 2;
		break;

	case Sys_Image_Test:
	case Sys_IR_Image_Test:
		iLimitRow = 1;
		break;

	default:
		break;
	}

	int iCtrlHeight = (iHeight - (iSpacing * 2)) / (iLimitRow + 1);
	int iLeftSub	= iLeft + iCapWidth;
	int iCellHeight = iHeight - iTop + iLimitRow - 1;

	int iarCtrlHeight[MI_MaxEnum] = { 0, };
	int iRemind = 0;

	for (UINT nIdx = 0; nIdx < iLimitRow; nIdx++)
	{
		iarCtrlHeight[nIdx] = iCellHeight / iLimitRow;
		iRemind += iarCtrlHeight[nIdx];
	}

	iRemind = iCellHeight - iRemind;
	
	for (int iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		iarCtrlHeight[iIdx - 1] += 1;
	}

	for (UINT nIdx = 0; nIdx < MI_MaxEnum; nIdx++)
	{
		m_st_Caption[nIdx].MoveWindow(0, 0, 0, 0);
		m_st_Value[nIdx].MoveWindow(0, 0, 0, 0);
	}

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		m_st_Caption[MI_Stage].MoveWindow(iLeft, iTop, iCapWidth, iarCtrlHeight[MI_Stage]);
		m_st_Value[MI_Stage].MoveWindow(iLeftSub, iTop, iValWidth, iarCtrlHeight[MI_Stage]);

		iTop += (iarCtrlHeight[MI_Stage] - 1);

		m_st_Caption[MI_ChartTX].MoveWindow(iLeft, iTop, iCapWidth, iarCtrlHeight[MI_ChartTX]);
		m_st_Value[MI_ChartTX].MoveWindow(iLeftSub, iTop, iValWidth, iarCtrlHeight[MI_ChartTX]);
		break;

	case Sys_Image_Test:
	case Sys_IR_Image_Test:
		m_st_Caption[MI_Stage].MoveWindow(iLeft, iTop, iCapWidth, iarCtrlHeight[MI_Stage]);
		m_st_Value[MI_Stage].MoveWindow(iLeftSub, iTop, iValWidth, iarCtrlHeight[MI_Stage]);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/10/26 - 16:42
// Desc.		:
//=============================================================================
void CWnd_Monitoring::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}

//=============================================================================
// Method		: ResetItem
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/21 - 16:51
// Desc.		:
//=============================================================================
void CWnd_Monitoring::ResetItem()
{
	for (UINT nIdx = 0; nIdx < MI_MaxEnum; nIdx++)
	{
		m_st_Value[nIdx].SetText(L"");
	}
}

//=============================================================================
// Method		: Set_Chart
// Access		: public  
// Returns		: void
// Parameter	: __in CString szChart
// Qualifier	:
// Last Update	: 2018/3/4 - 11:29
// Desc.		:
//=============================================================================
void CWnd_Monitoring::Set_Chart_Tx(__in CString szChart)
{
	m_st_Value[MI_ChartTX].SetText(szChart);
}

//=============================================================================
// Method		: Set_Stage
// Access		: public  
// Returns		: void
// Parameter	: __in CString szStage
// Qualifier	:
// Last Update	: 2018/3/4 - 11:29
// Desc.		:
//=============================================================================
void CWnd_Monitoring::Set_Stage(__in CString szStage)
{
	m_st_Value[MI_Stage].SetText(szStage);
}

//=============================================================================
// Method		: Set_Chart_Tz
// Access		: public  
// Returns		: void
// Parameter	: __in CString szChart
// Qualifier	:
// Last Update	: 2018/3/12 - 13:51
// Desc.		:
//=============================================================================
void CWnd_Monitoring::Set_Chart_Tz(__in CString szChart)
{
	m_st_Value[MI_ChartTZ].SetText(szChart);
}
