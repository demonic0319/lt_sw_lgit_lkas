#pragma once


// CWnd_CycleTime

class CWnd_CycleTime : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CycleTime)

public:
	CWnd_CycleTime();
	virtual ~CWnd_CycleTime();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);


public:


};


