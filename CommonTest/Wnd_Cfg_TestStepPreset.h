//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestStepPreset.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestStepPreset_h__
#define Wnd_Cfg_TestStepPreset_h__

#pragma once

#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_DataStruct_Cm.h"
#include "Wnd_Cfg_TestStep.h"
#include "Wnd_Cfg_Preset.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_TestStepPreset
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestStepPreset : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestStepPreset)

public:
	CWnd_Cfg_TestStepPreset();
	virtual ~CWnd_Cfg_TestStepPreset();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);


	// 검사기 설정
	enInsptrSysType			m_InspectionType	= enInsptrSysType::Sys_Focusing;

	CFont					m_font;

	CMFCTabCtrl				m_tc_Presets;
	
	CWnd_Cfg_Preset			m_wnd_CfgPreset;
	CWnd_Cfg_TestStep		m_wnd_CfgPresetStep[MAX_TESTSTEP_PRESET];
	CString					m_szPresetName[MAX_TESTSTEP_PRESET];
	
	UINT					m_nUsePresetCount	= MAX_TESTSTEP_PRESET;

public:

	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	// Step Info 가져오기 / 설정하기
	void		Set_StepInfo			(__in UINT nIndex, __in const ST_StepInfo* pstRecipeInfo);
	void		Get_StepInfo			(__in UINT nIndex, __out ST_StepInfo& stOutRecipInfo);

	// Preset 정보 가져오기 / 설정하기
	void		Set_PresetInfo			(__in const ST_PresetStepInfo* pstPresetInfo);
	void		Get_PresetInfo			(__out ST_PresetStepInfo& stOutPresetInfo);

	// 사용하는 Preset 개수와 Perset 명칭
	void		Set_UsePresetCount		(__in UINT nUseCount);
	void		Set_UsePreset			(__in UINT nUseCount, __in CStringArray* szNames);
	UINT		Get_PresetCount			();

};

#endif // Wnd_Cfg_TestStepPreset_h__


