//*****************************************************************************
// Filename	: 	Wnd_Cfg_3DCal_SpecEval.cpp
// Created	:	2017/11/10 - 16:10
// Modified	:	2017/11/10 - 16:10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_3DCal_SpecEval.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_3DCal_SpecEval.h"
#include "resource.h"

// CWnd_Cfg_3DCal_SpecEval
typedef enum CurrentOptID
{
	IDC_ED_SPEC_MIN = 1001,
	IDC_ED_SPEC_MAX = 2001,
	IDC_CHK_SPEC_MIN = 3001,
	IDC_CHK_SPEC_MAX = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_Cfg_3DCal_SpecEval, CWnd)

CWnd_Cfg_3DCal_SpecEval::CWnd_Cfg_3DCal_SpecEval()
{
	VERIFY(m_font.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_3DCal_SpecEval::~CWnd_Cfg_3DCal_SpecEval()
{
	m_font.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_Cfg_3DCal_SpecEval, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_CHK_SPEC_MIN, IDC_CHK_SPEC_MIN + Spec_3D_Eval_MaxNum - 1, OnBnClickedChkSpecMin)
	ON_COMMAND_RANGE(IDC_CHK_SPEC_MAX, IDC_CHK_SPEC_MAX + Spec_3D_Eval_MaxNum - 1, OnBnClickedChkSpecMax)
END_MESSAGE_MAP()



// CWnd_Cfg_3DCal_SpecEval message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/11/11 - 22:07
// Desc.		:
//=============================================================================
int CWnd_Cfg_3DCal_SpecEval::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_CapItem.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapItem.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_CapItem.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapItem.Create(_T("Item"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMin.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMin.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_CapSpecMin.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMin.Create(_T("Spec Min"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_st_CapSpecMax.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CapSpecMax.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_CapSpecMax.SetFont_Gdip(L"Arial", 9.0F);
	m_st_CapSpecMax.Create(_T("Spec Max"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	for (UINT nIdx = 0; nIdx < Spec_3D_Eval_MaxNum; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szSpec3DCal_Eval[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

		m_ed_SpecMin[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MIN + nIdx);
		m_ed_SpecMin[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMin[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMax[nIdx].Create(WS_VISIBLE | WS_BORDER | ES_CENTER, rectDummy, this, IDC_ED_SPEC_MAX + nIdx);
		m_ed_SpecMax[nIdx].SetWindowText(_T("0"));
		m_ed_SpecMax[nIdx].SetValidChars(_T("0123456789.-"));

		m_ed_SpecMin[nIdx].SetFont(&m_font);
		m_ed_SpecMax[nIdx].SetFont(&m_font);

		m_chk_SpecMin[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MIN + nIdx);
		m_chk_SpecMax[nIdx].Create(_T(""), WS_VISIBLE | WS_BORDER | BS_AUTOCHECKBOX, rectDummy, this, IDC_CHK_SPEC_MAX + nIdx);

		m_chk_SpecMin[nIdx].SetMouseCursorHand();
		m_chk_SpecMin[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMin[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMin[nIdx].SizeToContent();

		m_chk_SpecMax[nIdx].SetMouseCursorHand();
		m_chk_SpecMax[nIdx].SetImage(IDB_UNCHECKED_16);
		m_chk_SpecMax[nIdx].SetCheckedImage(IDB_CHECKED_16);
		m_chk_SpecMax[nIdx].SizeToContent();
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 5;
	int iLeft			= iMargin;
	int iTop			= iMargin;
	int iWidth			= cx - iMargin - iMargin;
	int iHeight			= cy - iMargin - iMargin;

	int iCtrlWidth		= (iWidth - (iSpacing * 2)) / 3;	//170;
	int	iCtrlHeight		= 30;
	int iLeft_2nd		= iLeft + iCtrlWidth + iSpacing;
	int iLeft_3rd		= iLeft_2nd + iCtrlWidth + iSpacing;
	int iChkWidth		= iCtrlHeight;
	int iEdWidth		= iCtrlWidth - iSpacing - iChkWidth;

	m_st_CapItem.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	m_st_CapSpecMin.MoveWindow(iLeft_2nd, iTop, iCtrlWidth, iCtrlHeight);
	m_st_CapSpecMax.MoveWindow(iLeft_3rd, iTop, iCtrlWidth, iCtrlHeight);

	iTop += iCtrlHeight + iCateSpacing;
	for (UINT nIdx = 0; nIdx < Spec_3D_Eval_MaxNum; nIdx++)
	{
		m_st_Item[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		m_ed_SpecMin[nIdx].MoveWindow(iLeft_2nd, iTop, iEdWidth, iCtrlHeight);
		m_chk_SpecMin[nIdx].MoveWindow(iLeft_2nd + iEdWidth + iSpacing, iTop, iChkWidth, iCtrlHeight);
		m_ed_SpecMax[nIdx].MoveWindow(iLeft_3rd, iTop, iEdWidth, iCtrlHeight);
		m_chk_SpecMax[nIdx].MoveWindow(iLeft_3rd + iEdWidth + iSpacing, iTop, iChkWidth, iCtrlHeight);

		iTop += iCtrlHeight + iCateSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_3DCal_SpecEval::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnBnClickedChkSpecMin
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/12/8 - 15:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::OnBnClickedChkSpecMin(UINT nID)
{
	UINT nIdx = nID - IDC_CHK_SPEC_MIN;

	if (BST_CHECKED == m_chk_SpecMin[nIdx].GetCheck())
	{
		m_ed_SpecMin[nIdx].EnableWindow(TRUE);
	}
	else
	{
		m_ed_SpecMin[nIdx].EnableWindow(FALSE);
	}
}

//=============================================================================
// Method		: OnBnClickedChkSpecMax
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/12/8 - 15:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::OnBnClickedChkSpecMax(UINT nID)
{
	UINT nIdx = nID - IDC_CHK_SPEC_MAX;

	if (BST_CHECKED == m_chk_SpecMax[nIdx].GetCheck())
	{
		m_ed_SpecMax[nIdx].EnableWindow(TRUE);
	}
	else
	{
		m_ed_SpecMax[nIdx].EnableWindow(FALSE);
	}
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

// 	if (TRUE == bShow)
// 	{
// 		GetOwner()->SendMessage(WM_SELECT_ITEM_VIEW, 0, TI_ImgT_Current);
// 	}
}

//=============================================================================
// Method		: Set_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:10
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::Set_TestItemInfo(__in ST_TestItemInfo* pstTestItemInfo)
{
	ST_TestItemSpec* pSpec = NULL;
	CString szText;
	UINT nCtrlIdx = 0;

	for (UINT nIdx = enTestItem_3D_CAL::TI_3D_Re_Eval_Chess0; nIdx <= enTestItem_3D_CAL::TI_3D_Re_Eval_IntensityC; nIdx++)
	{
		pSpec = pstTestItemInfo->GetTestItem(nIdx);
		ASSERT(NULL != pSpec);

		//nCtrlIdx = nIdx - enTestItem_3D_CAL::TI_3D_Re_Eval_Chess;

		if ((1 < pSpec->nResultCount) && (pSpec->bUseMultiSpec))
		{
			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					m_chk_SpecMin[nCtrlIdx].SetCheck(BST_CHECKED);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						szText.Format(_T("%d"), pSpec->Spec[nArIdx].Spec_Min.intVal);
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						szText.Format(_T("%.04f"), pSpec->Spec[nArIdx].Spec_Min.dblVal);
					}

					m_ed_SpecMin[nCtrlIdx].SetWindowText(szText);
					m_ed_SpecMin[nCtrlIdx].EnableWindow(TRUE);
				}
				else
				{
					m_chk_SpecMin[nCtrlIdx].SetCheck(BST_UNCHECKED);

					m_ed_SpecMin[nCtrlIdx].SetWindowText(_T("0"));
					m_ed_SpecMin[nCtrlIdx].EnableWindow(FALSE);
				}

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					m_chk_SpecMax[nCtrlIdx].SetCheck(BST_CHECKED);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						szText.Format(_T("%d"), pSpec->Spec[nArIdx].Spec_Max.intVal);
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						szText.Format(_T("%.04f"), pSpec->Spec[nArIdx].Spec_Max.dblVal);
					}

					m_ed_SpecMax[nCtrlIdx].SetWindowText(szText);
					m_ed_SpecMax[nCtrlIdx].EnableWindow(TRUE);
				}
				else
				{
					m_chk_SpecMax[nCtrlIdx].SetCheck(BST_UNCHECKED);

					m_ed_SpecMax[nCtrlIdx].SetWindowText(_T("0"));
					m_ed_SpecMax[nCtrlIdx].EnableWindow(FALSE);
				}

				// 항목 인덱스 증가
				++nCtrlIdx;
			}
		}
		else
		{
			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					m_chk_SpecMin[nCtrlIdx].SetCheck(BST_CHECKED);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						szText.Format(_T("%d"), pSpec->Spec[nArIdx].Spec_Min.intVal);
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						szText.Format(_T("%.04f"), pSpec->Spec[nArIdx].Spec_Min.dblVal);
					}

					m_ed_SpecMin[nCtrlIdx].SetWindowText(szText);
					m_ed_SpecMin[nCtrlIdx].EnableWindow(TRUE);
				}
				else
				{
					m_chk_SpecMin[nCtrlIdx].SetCheck(BST_UNCHECKED);

					m_ed_SpecMin[nCtrlIdx].SetWindowText(_T("0"));
					m_ed_SpecMin[nCtrlIdx].EnableWindow(FALSE);
				}

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					m_chk_SpecMax[nCtrlIdx].SetCheck(BST_CHECKED);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						szText.Format(_T("%d"), pSpec->Spec[nArIdx].Spec_Max.intVal);
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						szText.Format(_T("%.04f"), pSpec->Spec[nArIdx].Spec_Max.dblVal);
					}

					m_ed_SpecMax[nCtrlIdx].SetWindowText(szText);
					m_ed_SpecMax[nCtrlIdx].EnableWindow(TRUE);
				}
				else
				{
					m_chk_SpecMax[nCtrlIdx].SetCheck(BST_UNCHECKED);

					m_ed_SpecMax[nCtrlIdx].SetWindowText(_T("0"));
					m_ed_SpecMax[nCtrlIdx].EnableWindow(FALSE);
				}
			}

			// 항목 인덱스 증가
			++nCtrlIdx;

		} // End of if ((1 < pSpec->nResultCount) && (pSpec->bUseMultiSpec))
	}
}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 21:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::Get_TestItemInfo(__out ST_TestItemInfo& stOutTestItemInfo)
{
	ST_TestItemSpec* pSpec = NULL;
	CString szText;
	UINT nCtrlIdx = 0;

	for (UINT nIdx = enTestItem_3D_CAL::TI_3D_Re_Eval_Chess0; nIdx <= enTestItem_3D_CAL::TI_3D_Re_Eval_IntensityC; nIdx++)
	{
		pSpec = &stOutTestItemInfo.TestItemList.GetAt(nIdx);
		ASSERT(NULL != pSpec);

		//nCtrlIdx = nIdx - enTestItem_3D_CAL::TI_3D_Re_Eval_Chess;

		// 결과 데이터가 1개 초과이고, 개별 Min Max 스펙을 가지는 경우
		if ((1 < pSpec->nResultCount) && (pSpec->bUseMultiSpec))
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				pSpec->Spec[nArIdx].bUseSpecMin = (BST_CHECKED == m_chk_SpecMin[nCtrlIdx].GetCheck()) ? TRUE : FALSE;

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					m_ed_SpecMin[nCtrlIdx].GetWindowText(szText);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}

				pSpec->Spec[nArIdx].bUseSpecMax = (BST_CHECKED == m_chk_SpecMax[nCtrlIdx].GetCheck()) ? TRUE : FALSE;

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					m_ed_SpecMax[nCtrlIdx].GetWindowText(szText);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}

				// 항목 인덱스 증가
				++nCtrlIdx;
			}
		}
		else // 결과 데이터가 동일한 MinMax 스펙을 가지는 경우
		{
			pSpec->bUseMinMaxSpec = FALSE;

			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
			{
				pSpec->Spec[nArIdx].bUseSpecMin = (BST_CHECKED == m_chk_SpecMin[nCtrlIdx].GetCheck()) ? TRUE : FALSE;

				if (pSpec->Spec[nArIdx].bUseSpecMin)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					m_ed_SpecMin[nCtrlIdx].GetWindowText(szText);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
					}
				}

				pSpec->Spec[nArIdx].bUseSpecMax = (BST_CHECKED == m_chk_SpecMax[nCtrlIdx].GetCheck()) ? TRUE : FALSE;

				if (pSpec->Spec[nArIdx].bUseSpecMax)
				{
					pSpec->bUseMinMaxSpec = TRUE;

					m_ed_SpecMax[nCtrlIdx].GetWindowText(szText);

					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
					}
				}
				else
				{
					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
					{
						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
					}
					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
					{
						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
					}
				}
			}

			// 항목 인덱스 증가
			++nCtrlIdx;
		}
	}
}

//=============================================================================
// Method		: Set_3DCal_Spec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_3DCal_Eval_Spec * pst3DCal_SpecMin
// Parameter	: __in ST_3DCal_Eval_Spec * pst3DCal_SpecMax
// Qualifier	:
// Last Update	: 2017/11/14 - 23:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::Set_3DCal_Spec(__in ST_3DCal_Eval_Spec* pst3DCal_SpecMin, __in ST_3DCal_Eval_Spec* pst3DCal_SpecMax)
{
	CString szText;

	// Evaluation -------------------------------------------------------------

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->fChess[0]);		//[MAX_EVAL_Re_Chess];
	m_ed_SpecMin[Spec_3D_Eval_Chess_00].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->fChess[1]);		//[MAX_EVAL_Re_Chess];
	m_ed_SpecMin[Spec_3D_Eval_Chess_01].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->fChess[2]);		//[MAX_EVAL_Re_Chess];
	m_ed_SpecMin[Spec_3D_Eval_Chess_02].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->fTofDist[0]);		//[MAX_EVAL_Re_TofDist];
	m_ed_SpecMin[Spec_3D_Eval_TofDist_00].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->fTofDist[1]);		//[MAX_EVAL_Re_TofDist];
	m_ed_SpecMin[Spec_3D_Eval_TofDist_01].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->fTofDist[2]);		//[MAX_EVAL_Re_TofDist];
	m_ed_SpecMin[Spec_3D_Eval_TofDist_02].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->arfError);		//[MAX_EVAL_Re_Error];
	m_ed_SpecMin[Spec_3D_Eval_Error].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->arstShadingH);	//[MAX_EVAL_Shading];
	m_ed_SpecMin[Spec_3D_Eval_ShadingH].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->arstShadingV);	//[MAX_EVAL_ShadingV];
	m_ed_SpecMin[Spec_3D_Eval_ShadingV].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->arstShadingD0);	//[MAX_EVAL_Shading];
	m_ed_SpecMin[Spec_3D_Eval_ShadingD0].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->arstShadingD1);	//[MAX_EVAL_Shading];
	m_ed_SpecMin[Spec_3D_Eval_ShadingD1].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_SpecMin->nFailureCnt);
	m_ed_SpecMin[Spec_3D_Eval_FailureCnt].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMin->fIntensityC);
	m_ed_SpecMin[Spec_3D_Eval_IntensityC].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->fChess[0]);		//[MAX_EVAL_Re_Chess];
	m_ed_SpecMax[Spec_3D_Eval_Chess_00].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->fChess[1]);		//[MAX_EVAL_Re_Chess];
	m_ed_SpecMax[Spec_3D_Eval_Chess_01].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->fChess[2]);		//[MAX_EVAL_Re_Chess];
	m_ed_SpecMax[Spec_3D_Eval_Chess_02].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->fTofDist[0]);		//[MAX_EVAL_Re_TofDist];
	m_ed_SpecMax[Spec_3D_Eval_TofDist_00].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->fTofDist[1]);		//[MAX_EVAL_Re_TofDist];
	m_ed_SpecMax[Spec_3D_Eval_TofDist_01].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->fTofDist[2]);		//[MAX_EVAL_Re_TofDist];
	m_ed_SpecMax[Spec_3D_Eval_TofDist_02].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->arfError);		//[MAX_EVAL_Re_Error];
	m_ed_SpecMax[Spec_3D_Eval_Error].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->arstShadingH);	//[MAX_EVAL_Shading];
	m_ed_SpecMax[Spec_3D_Eval_ShadingH].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->arstShadingV);	//[MAX_EVAL_ShadingV];
	m_ed_SpecMax[Spec_3D_Eval_ShadingV].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->arstShadingD0);	//[MAX_EVAL_Shading];
	m_ed_SpecMax[Spec_3D_Eval_ShadingD0].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->arstShadingD1);	//[MAX_EVAL_Shading];
	m_ed_SpecMax[Spec_3D_Eval_ShadingD1].SetWindowText(szText);

	szText.Format(_T("%d"), pst3DCal_SpecMax->nFailureCnt);
	m_ed_SpecMax[Spec_3D_Eval_FailureCnt].SetWindowText(szText);

	szText.Format(_T("%.4f"), pst3DCal_SpecMax->fIntensityC);
	m_ed_SpecMax[Spec_3D_Eval_IntensityC].SetWindowText(szText);
}

//=============================================================================
// Method		: Get_3DCal_Spec
// Access		: public  
// Returns		: void
// Parameter	: __out ST_3DCal_Eval_Spec & stOut3DCal_SpecMin
// Parameter	: __out ST_3DCal_Eval_Spec & stOut3DCal_SpecMax
// Qualifier	:
// Last Update	: 2017/11/14 - 23:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_3DCal_SpecEval::Get_3DCal_Spec(__out ST_3DCal_Eval_Spec& stOut3DCal_SpecMin, __out ST_3DCal_Eval_Spec& stOut3DCal_SpecMax)
{
	CString szText;

	// Evaluation -------------------------------------------------------------

	m_ed_SpecMin[Spec_3D_Eval_Chess_00].GetWindowText(szText);
	stOut3DCal_SpecMin.fChess[0] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_Chess_01].GetWindowText(szText);
	stOut3DCal_SpecMin.fChess[1] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_Chess_02].GetWindowText(szText);
	stOut3DCal_SpecMin.fChess[2] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_TofDist_00].GetWindowText(szText);
	stOut3DCal_SpecMin.fTofDist[0] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_TofDist_01].GetWindowText(szText);
	stOut3DCal_SpecMin.fTofDist[1] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_TofDist_02].GetWindowText(szText);
	stOut3DCal_SpecMin.fTofDist[2] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_Error].GetWindowText(szText);
	stOut3DCal_SpecMin.arfError = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_ShadingH].GetWindowText(szText);
	stOut3DCal_SpecMin.arstShadingH = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_ShadingV].GetWindowText(szText);
	stOut3DCal_SpecMin.arstShadingV = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_ShadingD0].GetWindowText(szText);
	stOut3DCal_SpecMin.arstShadingD0 = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_ShadingD1].GetWindowText(szText);
	stOut3DCal_SpecMin.arstShadingD1 = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_FailureCnt].GetWindowText(szText);
	stOut3DCal_SpecMin.nFailureCnt = _ttoi(szText.GetBuffer(0));

	m_ed_SpecMin[Spec_3D_Eval_IntensityC].GetWindowText(szText);
	stOut3DCal_SpecMin.fIntensityC = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_Chess_00].GetWindowText(szText);
	stOut3DCal_SpecMax.fChess[0] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_Chess_01].GetWindowText(szText);
	stOut3DCal_SpecMax.fChess[1] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_Chess_02].GetWindowText(szText);
	stOut3DCal_SpecMax.fChess[2] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_TofDist_00].GetWindowText(szText);
	stOut3DCal_SpecMax.fTofDist[0] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_TofDist_01].GetWindowText(szText);
	stOut3DCal_SpecMax.fTofDist[1] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_TofDist_02].GetWindowText(szText);
	stOut3DCal_SpecMax.fTofDist[2] = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_Error].GetWindowText(szText);
	stOut3DCal_SpecMax.arfError = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_ShadingH].GetWindowText(szText);
	stOut3DCal_SpecMax.arstShadingH = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_ShadingV].GetWindowText(szText);
	stOut3DCal_SpecMax.arstShadingH = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_ShadingD0].GetWindowText(szText);
	stOut3DCal_SpecMax.arstShadingD0 = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_ShadingD1].GetWindowText(szText);
	stOut3DCal_SpecMax.arstShadingD1 = (float)_ttof(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_FailureCnt].GetWindowText(szText);
	stOut3DCal_SpecMax.nFailureCnt = _ttoi(szText.GetBuffer(0));

	m_ed_SpecMax[Spec_3D_Eval_IntensityC].GetWindowText(szText);
	stOut3DCal_SpecMax.fIntensityC = (float)_ttof(szText.GetBuffer(0));
}
