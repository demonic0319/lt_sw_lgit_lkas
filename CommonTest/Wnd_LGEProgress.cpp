//*****************************************************************************
// Filename	: 	Wnd_LGEProgress.cpp
// Created	:	2017/9/18 - 17:16
// Modified	:	2017/9/18 - 17:16
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_LGEProgress.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_LGEProgress.h"


static LPCTSTR g_szProcCol[] =
{
	_T("L"),	// 
	_T("R"),	// 
	NULL
};

static int g_iCell_Width[] =
{
	50,		// Left
	150,	// Left Item
	50,		// Right
	150,	// Right Item
};

//-----------------------------------------------------------------------------
// CWnd_LGEProgress
//-----------------------------------------------------------------------------
IMPLEMENT_DYNAMIC(CWnd_LGEProgress, CWnd)

CWnd_LGEProgress::CWnd_LGEProgress()
{

}

CWnd_LGEProgress::~CWnd_LGEProgress()
{
}


BEGIN_MESSAGE_MAP(CWnd_LGEProgress, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()


// CWnd_LGEProgress message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/18 - 19:49
// Desc.		:
//=============================================================================
int CWnd_LGEProgress::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	UpdateControlFont();

	//m_st_Caption.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_Caption.SetColorStyle(CVGStatic::ColorStyle_Black);	
	//m_st_Caption.SetFont_Gdip(L"Arial", 16.0F);

	//m_st_Value.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_Progess.SetColorStyle(CVGStatic::ColorStyle_White);
	//m_st_Progess.SetFont_Gdip(L"Arial", 18.0F);

	m_st_Caption.Create(_T("Progress"), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
	m_st_Progess.Create(_T("0 %"), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);

	for (UINT nColIdx = 0; nColIdx < PP_X_MaxEnum; nColIdx++)
	{
		//m_st_Para[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Para[nColIdx].SetColorStyle(CVGStatic::ColorStyle_White);
		m_st_Para[nColIdx].SetTextRenderingHint(Gdiplus::TextRenderingHint::TextRenderingHintAntiAlias);
		//m_st_Para[nColIdx].SetFont_Gdip(L"Arial", 32.0F);
		m_st_Para[nColIdx].Create(g_szProcCol[nColIdx], WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
	}

	for (UINT nRowIdx = 0; nRowIdx < PI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < PP_X_MaxEnum; nColIdx++)
		{
			//m_st_Cell[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
			m_st_Cell[nRowIdx][nColIdx].SetColorStyle(CVGStatic::ColorStyle_White);
			//m_st_Cell[nRowIdx][nColIdx].SetFont_Gdip(L"Arial", 12.0F);
			m_st_Cell[nRowIdx][nColIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
		}
	}

	SetParaStatus(enTestProcess::TP_Ready, 0);
	SetParaStatus(enTestProcess::TP_Ready, 1);

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/18 - 19:49
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;
	int iRemind		= 0;
	int iCapWidth	= 110;
	int iProgWidth	= iWidth - iCapWidth;

	if (0 == m_nParaCount)
	{
		iProgWidth = iWidth - iCapWidth;
	}
	else if (1 == m_nParaCount)
	{
		iProgWidth = (iWidth * 3 / 5) - iCapWidth;
	}
	else
	{
		iProgWidth = (iWidth*3/7) - iCapWidth;
	}

	m_st_Caption.MoveWindow(iLeft, iTop, iCapWidth, iHeight);
	iLeft += iCapWidth;
	m_st_Progess.MoveWindow(iLeft, iTop, iProgWidth, iHeight);

	if (0 < m_nParaCount)
	{
		// Para 상세 진행 현황	
		// 높이
		int iCellHeight = iHeight + (PI_Y_MaxEnum - 1);
		int iCtrlHeight[PI_Y_MaxEnum] = { 0, };
		iRemind = 0;

		for (UINT nIdx = 0; nIdx < (PI_Y_MaxEnum); nIdx++)
		{
			iCtrlHeight[nIdx] = iCellHeight / (PI_Y_MaxEnum);
			iRemind += iCtrlHeight[nIdx];
		}
		iRemind = iCellHeight - iRemind;

		for (int iIdx = iRemind; 0 < iIdx; iIdx--)
		{
			iCtrlHeight[iIdx - 1] += 1;
		}

		// 너비
		iWidth = (iWidth - iCapWidth - iProgWidth) + (2 * m_nParaCount);
		int iCellWidth = 0;
		int iParaWidth = 0;
		if (0 != m_nParaCount)
		{
			iCellWidth = iWidth * 3 / (4 * m_nParaCount);
			iParaWidth = (iWidth / m_nParaCount) - iCellWidth;
		}

		// 컨트롤 이동	
		iLeft = iCapWidth + iProgWidth - 1;
		int iTempWidth = 0;
	
		for (UINT nColIdx = 0; nColIdx < m_nParaCount; nColIdx++)
		{
			m_st_Para[nColIdx].MoveWindow(iLeft, iTop, iParaWidth, iHeight);
			iLeft += (iParaWidth - 1);

			for (UINT nRowIdx = 0; nRowIdx < PI_Y_MaxEnum; nRowIdx++)
			{
				m_st_Cell[nRowIdx][nColIdx].MoveWindow(iLeft, iTop, iCellWidth, iCtrlHeight[nRowIdx]);

				iTop += (iCtrlHeight[nRowIdx] - 1);
			}

			iTop = 0;
			iLeft += (iCellWidth - 1);
		}
	}

	for (UINT nColIdx = m_nParaCount; nColIdx < PP_X_MaxEnum; nColIdx++)
	{
		m_st_Para[nColIdx].MoveWindow(0, 0, 0, 0);

		for (UINT nRowIdx = 0; nRowIdx < PI_Y_MaxEnum; nRowIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].MoveWindow(0, 0, 0, 0);
		}
	}
}

//=============================================================================
// Method		: UpdateControlFont
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/19 - 11:49
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::UpdateControlFont()
{
	switch (m_nFontLevel)
	{
	case enSizeLevel::SizeLv_Tiny:
	{
		m_fFontSize_Caption	= 8.0F;
		m_fFontSize_Progess	= 9.0F;
		m_fFontSize_Para	= 16.0F;
		m_fFontSize_Cell	= 6.0F;
	}
		break;

	case enSizeLevel::SizeLv_Small:
	{
		m_fFontSize_Caption	= 12.0F;
		m_fFontSize_Progess	= 14.0F;
		m_fFontSize_Para	= 24.0F;
		m_fFontSize_Cell	= 8.0F;
	}
		break;

	case enSizeLevel::SizeLv_Medium:
	{
		m_fFontSize_Caption	= 16.0F;
		m_fFontSize_Progess	= 18.0F;
		m_fFontSize_Para	= 32.0F;
		m_fFontSize_Cell	= 12.0F;
	}
		break;

	case enSizeLevel::SizeLv_Large:
	{
		m_fFontSize_Caption	= 24.0F;
		m_fFontSize_Progess	= 27.0F;
		m_fFontSize_Para	= 48.0F;
		m_fFontSize_Cell	= 18.0F;
	}
		break;

	case enSizeLevel::SizeLv_Huge:
	{
		m_fFontSize_Caption	= 32.0F;
		m_fFontSize_Progess	= 32.0F;
		m_fFontSize_Para	= 64.0F;
		m_fFontSize_Cell	= 24.0F;
	}
		break;

	default:
	{
		m_fFontSize_Caption	= 16.0F;
		m_fFontSize_Progess	= 18.0F;
		m_fFontSize_Para	= 32.0F;
		m_fFontSize_Cell	= 12.0F;
	}
		break;
	}

	m_st_Caption.SetFont_Gdip(L"Arial", m_fFontSize_Caption);
	m_st_Progess.SetFont_Gdip(L"Arial", m_fFontSize_Progess);

	for (UINT nColIdx = 0; nColIdx < PP_X_MaxEnum; nColIdx++)
	{
		m_st_Para[nColIdx].SetFont_Gdip(L"Arial", m_fFontSize_Para);
	}

	for (UINT nRowIdx = 0; nRowIdx < PI_Y_MaxEnum; nRowIdx++)
	{
		for (UINT nColIdx = 0; nColIdx < PP_X_MaxEnum; nColIdx++)
		{
			m_st_Cell[nRowIdx][nColIdx].SetFont_Gdip(L"Arial", m_fFontSize_Cell);
		}
	}
}

//=============================================================================
// Method		: SetParaCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/9/19 - 14:03
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::SetParaCount(__in UINT nCount)
{
	m_nParaCount = (nCount <= PP_X_MaxEnum) ? nCount : PP_X_MaxEnum;

	if (m_nParaCount <= 1)
	{
		Set_UseCameraSelect(FALSE);
	}
	else
	{
		Set_UseCameraSelect(TRUE);
	}

	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: SetFontSizeLevel
// Access		: public  
// Returns		: void
// Parameter	: __in enSizeLevel nFontLevel
// Qualifier	:
// Last Update	: 2018/3/19 - 11:45
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::SetFontSizeLevel(__in enSizeLevel nFontLevel)
{
	if (m_nFontLevel != nFontLevel)
	{
		m_nFontLevel = nFontLevel;

		if (GetSafeHwnd())
		{
			UpdateControlFont();
		}
	}
}

//=============================================================================
// Method		: ResetProgress
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 14:05
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::ResetProgress()
{
	m_st_Progess.SetText(L"0 %");
}

//=============================================================================
// Method		: ResetParaInfo
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIndex
// Qualifier	:
// Last Update	: 2017/9/22 - 20:05
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::ResetParaInfo(__in UINT nParaIndex /*= 0*/)
{
	if (nParaIndex < m_nParaCount)
	{
		SetParaStatus(enTestProcess::TP_Ready, nParaIndex);
		m_st_Cell[PI_Y_Barcode][nParaIndex].SetText(L"");
		m_st_Cell[PI_Y_Time][nParaIndex].SetText(L"");
	}
}

//=============================================================================
// Method		: SetProgress
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTotalStep
// Parameter	: __in UINT nProgStep
// Qualifier	:
// Last Update	: 2017/9/19 - 14:05
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::SetProgress(__in UINT nTotalStep, __in UINT nProgStep)
{
	FLOAT fPercent = (FLOAT)nProgStep / (FLOAT)nTotalStep * 100.0f;

	CStringW szText;
	szText.Format(L"%.2f %% ( %d / %d)", fPercent, nProgStep, nTotalStep);

	m_st_Progess.SetText(szText.GetBuffer(0));
}

//=============================================================================
// Method		: SetParaStatus
// Access		: public  
// Returns		: void
// Parameter	: __in enTestProcess nProgress
// Parameter	: __in UINT nParaIndex
// Qualifier	:
// Last Update	: 2017/9/19 - 14:08
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::SetParaStatus(__in enTestProcess nProgress, __in UINT nParaIndex /*= 0*/)
{
	if (nParaIndex < m_nParaCount)
	{
		if (FALSE == m_bUseCameraSelect)
		{
			m_st_Para[nParaIndex].SetBackColor_COLORREF(g_TestProcess[nProgress].BackColor);
		}

		m_st_Cell[PI_Y_Status][nParaIndex].SetBackColor_COLORREF(g_TestProcess[nProgress].BackColor);
		m_st_Cell[PI_Y_Status][nParaIndex].SetTextColor_COLORREF(g_TestProcess[nProgress].TextColor, g_TestProcess[nProgress].TextColor);
		m_st_Cell[PI_Y_Status][nParaIndex].SetText(g_TestProcess[nProgress].szText);
	}
}

//=============================================================================
// Method		: SetBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in UINT nParaIndex
// Qualifier	:
// Last Update	: 2017/9/19 - 14:14
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::SetBarcode(__in LPCTSTR szBarcode, __in UINT nParaIndex /*= 0*/)
{
	m_st_Cell[PI_Y_Barcode][nParaIndex].SetText(szBarcode);
}

//=============================================================================
// Method		: SetParaInputTime
// Access		: public  
// Returns		: void
// Parameter	: __in PSYSTEMTIME pstTime
// Parameter	: __in UINT nParaIndex
// Qualifier	:
// Last Update	: 2017/9/19 - 14:14
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::SetParaInputTime(__in PSYSTEMTIME pstTime, __in UINT nParaIndex /*= 0*/)
{
	if (nParaIndex < m_nParaCount)
	{
		CStringW szText;
		szText.Format(L"%02d:%02d:%02d.%03d", pstTime->wHour, pstTime->wMinute, pstTime->wSecond, pstTime->wMilliseconds);

		m_st_Cell[PI_Y_Time][nParaIndex].SetText(szText.GetBuffer(0));
	}
}

//=============================================================================
// Method		: Set_UseCameraSelect
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUseCameraSelect
// Qualifier	:
// Last Update	: 2018/2/20 - 16:29
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::Set_UseCameraSelect(__in BOOL bUseCameraSelect)
{
	m_bUseCameraSelect = bUseCameraSelect;

	if (FALSE == m_bUseCameraSelect)
	{
		if (GetSafeHwnd())
		{
			m_st_Para[PP_X_Left].SetBackColor_COLORREF(g_TestProcess[enTestProcess::TP_Ready].BackColor);
			m_st_Para[PP_X_Right].SetBackColor_COLORREF(g_TestProcess[enTestProcess::TP_Ready].BackColor);
		}
	}
}

//=============================================================================
// Method		: Set_CameraSelect
// Access		: public  
// Returns		: void
// Parameter	: __in INT nParaIndex
// Qualifier	:
// Last Update	: 2018/2/20 - 16:35
// Desc.		:
//=============================================================================
void CWnd_LGEProgress::Set_CameraSelect(__in INT nParaIndex)
{
	if ((m_bUseCameraSelect) && (1 < m_nParaCount))
	{
		if (PP_X_Left == nParaIndex)
		{
			m_st_Para[PP_X_Left].SetBackColor_COLORREF(RGB(0, 192, 255));
			m_st_Para[PP_X_Right].SetBackColor_COLORREF(g_TestProcess[enTestProcess::TP_Ready].BackColor);
		}
		else
		{
			m_st_Para[PP_X_Left].SetBackColor_COLORREF(g_TestProcess[enTestProcess::TP_Ready].BackColor);
			//m_st_Para[PP_X_Right].SetBackColor_COLORREF(RGB(91, 155, 213));
			m_st_Para[PP_X_Right].SetBackColor_COLORREF(RGB(0, 192, 255));
		}
	}
}
