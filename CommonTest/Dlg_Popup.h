﻿//*****************************************************************************
// Filename	: 	Dlg_Popup.h
// Created	:	2018/3/12 - 16:46
// Modified	:	2018/3/12 - 16:46
//
// Author	:	Luritech
//	
// Purpose	:	
//****************************************************************************
#ifndef Dlg_Popup_h__
#define Dlg_Popup_h__

#pragma once

#include "resource.h"
#include "VGStatic.h"

//=============================================================================
// CDlg_Popup 대화 상자입니다.
//=============================================================================
class CDlg_Popup : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_Popup)

public:
	CDlg_Popup(CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlg_Popup();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_POPUP };

protected:
	virtual void	DoDataExchange				(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnBnClickedButtenOK			();
	afx_msg void	OnBnClickedButtenCancel		();

	DECLARE_MESSAGE_MAP()

	CFont				m_font_Default;

	CButton				m_bn_Cancel;
	CButton				m_bn_Ok;
	CVGStatic			m_st_frame;

	CString				m_szMessage;

public:

	void		PopupMessage		(__in CString szInText);

};

#endif // Dlg_Popup_h__
