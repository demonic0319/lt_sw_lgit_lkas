//*****************************************************************************
// Filename	: 	Wnd_LGE_WIPID.cpp
// Created	:	2017/9/21 - 17:39
// Modified	:	2017/9/21 - 17:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_LGE_WIPID.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_LGE_WIPID.h"


// CWnd_LGE_WIPID

static LPCTSTR g_szWIPID_Item[] =
{
	_T("WIP ID"),		// WIP ID
	_T("S/N"),			// Serial Numbers
	_T("Pallet ID"),	// Pallet ID
	_T(""),
	_T(""),
	NULL
};

static LPCTSTR g_szOQA_Item[] =
{
	_T("WIP ID"),		// WIP ID
	_T("S/N"),			// Serial Numbers
	_T("Part No"),		// Part No
	_T("H/W Ver."),		// H/W Version
	_T("S/W Ver."),		// S/W Version
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_LGE_WIPID, CWnd)

CWnd_LGE_WIPID::CWnd_LGE_WIPID()
{

}

CWnd_LGE_WIPID::~CWnd_LGE_WIPID()
{
}


BEGIN_MESSAGE_MAP(CWnd_LGE_WIPID, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()



// CWnd_LGE_WIPID message handlers
//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/21 - 17:51
// Desc.		:
//=============================================================================
int CWnd_LGE_WIPID::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	LPCTSTR* parItemName = NULL;
// 	if (m_InspectionType == Sys_OQA)
// 	{
// 		parItemName = g_szOQA_Item;
// 	}
// 	else
	{
		parItemName = g_szWIPID_Item;
	}
	
	for (UINT nIdx = 0; nIdx < II_MaxEnum; nIdx++)
	{
		//m_st_Caption[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Caption[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Caption[nIdx].SetFont_Gdip(L"Arial", 18.0F);
		m_st_Caption[nIdx].SetBorderTickness(m_nBorderTickness);

		//m_st_Value[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Value[nIdx].SetColorStyle(CVGStatic::ColorStyle_White);
		m_st_Value[nIdx].SetFont_Gdip(L"Arial", 38.0F);
		m_st_Value[nIdx].SetTextRenderingHint(TextRenderingHint::TextRenderingHintAntiAliasGridFit);
		m_st_Value[nIdx].SetBorderTickness(m_nBorderTickness);

		m_st_Caption[nIdx].Create(parItemName[nIdx], WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
		m_st_Value[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
	}

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/21 - 17:51
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;
	int iCapWidth	= 150;
	int iValWidth	= iWidth - iCapWidth + m_nBorderTickness;
	int iLeftSub	= iLeft + iCapWidth - m_nBorderTickness;

	if (Sys_Focusing == m_InspectionType)
	{
		int iCellHeight = iHeight + ((II_Wip_MaxEnum - 1) * m_nBorderTickness);
		int iarCtrlHeight[II_Wip_MaxEnum] = { 0, };
		int iRemind = 0;

		for (UINT nIdx = 0; nIdx < II_Wip_MaxEnum; nIdx++)
		{
			iarCtrlHeight[nIdx] = iCellHeight / II_Wip_MaxEnum;
			iRemind += iarCtrlHeight[nIdx];
		}
		iRemind = iCellHeight - iRemind;

		for (int iIdx = iRemind; 0 < iIdx; iIdx--)
		{
			iarCtrlHeight[iIdx - 1] += 1;
		}

		for (UINT nIdx = 0; nIdx < II_Wip_MaxEnum; nIdx++)
		{
			m_st_Caption[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iarCtrlHeight[nIdx]);
			m_st_Value[nIdx].MoveWindow(iLeftSub, iTop, iValWidth, iarCtrlHeight[nIdx]);
			iTop += (iarCtrlHeight[nIdx] - m_nBorderTickness);
		}
	}
	else
	{
		int iCellHeight = iHeight + ((II_MaxEnum - 1) * m_nBorderTickness);
		int iarCtrlHeight[II_MaxEnum] = { 0, };
		int iRemind = 0;

		for (UINT nIdx = 0; nIdx < II_MaxEnum; nIdx++)
		{
			iarCtrlHeight[nIdx] = iCellHeight / II_MaxEnum;
			iRemind += iarCtrlHeight[nIdx];
		}
		iRemind = iCellHeight - iRemind;

		for (int iIdx = iRemind; 0 < iIdx; iIdx--)
		{
			iarCtrlHeight[iIdx - 1] += 1;
		}

		for (UINT nIdx = 0; nIdx < II_MaxEnum; nIdx++)
		{
			m_st_Caption[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iarCtrlHeight[nIdx]);
			m_st_Value[nIdx].MoveWindow(iLeftSub, iTop, iValWidth, iarCtrlHeight[nIdx]);
			iTop += (iarCtrlHeight[nIdx] - m_nBorderTickness);
		}
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/11/23 - 16:22
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}

//=============================================================================
// Method		: ResetItem
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/21 - 21:37
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::ResetItem()
{
	for (UINT nIdx = 0; nIdx < II_MaxEnum; nIdx++)
	{
		m_st_Value[nIdx].SetText(L"");
	}
}

//=============================================================================
// Method		: Set_Barcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2017/9/21 - 21:38
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::Set_Barcode(__in LPCTSTR szBarcode)
{
	m_st_Value[II_WIP_ID].SetText(szBarcode);
}

//=============================================================================
// Method		: Set_SerialNumbers
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szSerialNumbers
// Qualifier	:
// Last Update	: 2017/11/20 - 17:02
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::Set_SerialNumbers(__in LPCTSTR szSerialNumbers)
{
	m_st_Value[II_SerialNumbers].SetText(szSerialNumbers);
}

//=============================================================================
// Method		: Set_PalletID
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szPalletID
// Qualifier	:
// Last Update	: 2017/9/21 - 21:38
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::Set_PalletID(__in LPCTSTR szPalletID)
{
	m_st_Value[II_Pallet_ID].SetText(szPalletID);
}

//=============================================================================
// Method		: Set_PartNo
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szPartNo
// Qualifier	:
// Last Update	: 2017/12/18 - 10:36
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::Set_PartNo(__in LPCTSTR szPartNo)
{
	m_st_Value[II_PartNo].SetText(szPartNo);
}

//=============================================================================
// Method		: Set_HWVersion
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szHWVerison
// Qualifier	:
// Last Update	: 2017/12/18 - 10:36
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::Set_HWVersion(__in LPCTSTR szHWVerison)
{
	m_st_Value[II_HW_Version].SetText(szHWVerison);
}

//=============================================================================
// Method		: Set_SWVersion
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szSWVerison
// Qualifier	:
// Last Update	: 2017/11/23 - 16:21
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::Set_SWVersion(__in LPCTSTR szSWVerison)
{
	m_st_Value[II_SW_Version].SetText(szSWVerison);
}

//=============================================================================
// Method		: SetPart_Info
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szPartNo
// Parameter	: __in LPCTSTR szHWVer
// Parameter	: __in LPCTSTR szSWVer
// Qualifier	:
// Last Update	: 2017/12/18 - 10:42
// Desc.		:
//=============================================================================
void CWnd_LGE_WIPID::SetPart_Info(__in LPCTSTR szPartNo, __in LPCTSTR szHWVer, __in LPCTSTR szSWVer)
{
	m_st_Value[II_PartNo].SetText(szPartNo);

	m_st_Value[II_HW_Version].SetText(szHWVer);

	m_st_Value[II_SW_Version].SetText(szSWVer);
}
