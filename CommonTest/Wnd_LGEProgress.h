//*****************************************************************************
// Filename	: 	Wnd_LGEProgress.h
// Created	:	2017/9/18 - 17:15
// Modified	:	2017/9/18 - 17:15
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_LGEProgress_h__
#define Wnd_LGEProgress_h__

#pragma once

#include "Def_Test_Cm.h"
#include "VGStatic.h"

typedef enum enSizeLevel
{
	SizeLv_Tiny,
	SizeLv_Small,
	SizeLv_Medium,
	SizeLv_Large,
	SizeLv_Huge,
};

//-----------------------------------------------------------------------------
// CWnd_LGEProgress
//-----------------------------------------------------------------------------
class CWnd_LGEProgress : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LGEProgress)

public:
	CWnd_LGEProgress();
	virtual ~CWnd_LGEProgress();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);

	// 프로그레스 : 완료 항목 수 / 전체 항목 수 = 진행률 (%)
	CVGStatic		m_st_Caption;
	CVGStatic		m_st_Progess;

	// 대기-> Loading-> Wait Test-> Test-> 배출 대기-> Unloading 순서의 진행 상태 표시용 상수
	enum enProcItem
	{
		PI_Y_Status,
		PI_Y_Barcode,
		PI_Y_Time,
		PI_Y_MaxEnum,
	};

	enum enProcPara
	{
		PP_X_Left,
		PP_X_Right,
		PP_X_MaxEnum,
	};

	// 설비 파라 정보 표시용 UI
	CVGStatic		m_st_Para[PP_X_MaxEnum];
	CVGStatic		m_st_Cell[PI_Y_MaxEnum][PP_X_MaxEnum];

	// 설비의 Para 갯수 (WipID : 1, 2D Cal : 2, IQ : 2, 이물 : 1 or 0, 3D Cal : 2)
	UINT			m_nParaCount			= 2;		// 0 ~ 2개
	
	// UI 너비 보정용 변수
	int				m_iTotalWidthRate		= 0;
	int				m_iCtrlWidth[PP_X_MaxEnum * 2];

	// 왼/오른 카메라 선택 기능 사용여부
	BOOL			m_bUseCameraSelect		= TRUE;

	enSizeLevel		m_nFontLevel			= SizeLv_Medium;
	Gdiplus::REAL	m_fFontSize_Caption		= 16.0F;
	Gdiplus::REAL	m_fFontSize_Progess		= 18.0F;
	Gdiplus::REAL	m_fFontSize_Para		= 32.0F;
	Gdiplus::REAL	m_fFontSize_Cell		= 12.0F;

	virtual void	UpdateControlFont		();

public:
	
	// 설비의 Para 갯수 (0으로 설정하면 UI상에 Para 정보 표시하지 않음)
	void		SetParaCount		(__in UINT nCount);

	// 폰트 크기 단계 설정
	void		SetFontSizeLevel	(__in enSizeLevel nFontLevel);
	
	// 검사 상태 초기화
	void		ResetProgress		();
	// 파라미터 정보 초기화
	void		ResetParaInfo		(__in UINT nParaIndex = 0);

	// 검사 진행 상태
	void		SetProgress			(__in UINT nTotalStep, __in UINT nProgStep);

	// 파라미터 구동 상태
	void		SetParaStatus		(__in enTestProcess nProgress, __in UINT nParaIndex = 0);
	// 파라미터 별 WIP-ID
	void		SetBarcode			(__in LPCTSTR szBarcode, __in UINT nParaIndex = 0);
	// 파라미터 별 팔레트 투입 시간
	void		SetParaInputTime	(__in PSYSTEMTIME pstTime, __in UINT nParaIndex = 0);

	// 카메라 선택 (왼/오른)
	void		Set_UseCameraSelect	(__in BOOL bUseCameraSelect);
	void		Set_CameraSelect	(__in INT nParaIndex);
};

#endif // Wnd_LGEProgress_h__


