﻿//*****************************************************************************
// Filename	: 	Dlg_ChangeModel.h
// Created	:	2017/1/9 - 16:54
// Modified	:	2017/1/9 - 16:54
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Dlg_ChangeModel_h__
#define Dlg_ChangeModel_h__

#pragma once

#include "resource.h"
#include "File_WatchList.h"

#define	 USE_CUSTOM_FUNCTION

#ifdef USE_CUSTOM_FUNCTION
	#include "VGStatic.h"
#endif

//-----------------------------------------------------------------------------
// CDlg_ChangeModel dialog
//-----------------------------------------------------------------------------
class CDlg_ChangeModel : public CDialogEx
{
	DECLARE_DYNAMIC(CDlg_ChangeModel)

public:
	CDlg_ChangeModel(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlg_ChangeModel();

// Dialog Data
	enum { IDD = IDD_DLG_SEL_MODEL };

protected:
	virtual void	DoDataExchange			(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo			(MINMAXINFO* lpMMI);
	virtual BOOL	OnInitDialog			();
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg void	OnBnClickedBnClear		();
	afx_msg void	OnBnClickedBnSearch		();
	afx_msg void	OnEnChangeEdModelBarcode();
	afx_msg void	OnCbnSelEndCbModel		();
	afx_msg void	OnEnClickedBnOK			();

	DECLARE_MESSAGE_MAP()

	CFont			m_font_Large;
	CFont			m_font_Default;

#ifdef USE_CUSTOM_FUNCTION
	CVGStatic	m_st_Model;
	CVGStatic	m_st_ModelFile;
	CVGStatic	m_st_Verify;
	CVGStatic	m_st_Model_CB;
#else
	CStatic		m_st_Model;
	CStatic		m_st_ModelFile;
	CStatic		m_st_Verify;
	CStatic		m_st_Model_CB;
#endif
	
	CEdit		m_ed_Model;
	CButton		m_bn_Clear;
	CButton		m_bn_Search;

	CComboBox	m_cb_Model;

	CButton		m_bn_OK;

	CString		m_szRecipePath;
	CString		m_szFileExt;

	CString		m_szAppName;
	CString		m_szKeyName;

	CString		m_szFindModelFile;
	BOOL		m_bFindModelFile;

	CFile_WatchList		m_fileWatch;

	BOOL		ReadModelCode			(__in LPCTSTR szPath, __out CString& szModelCode);
	BOOL		FindModelByModelCode	(__in LPCTSTR szCode);
	
	//BOOL		LoadModel				(__in LPCTSTR szRecipe);

public:

	void		SetModleInfo			(__in LPCTSTR szPath, __in LPCTSTR szFileExt)
	{
		m_szRecipePath	= szPath;
		m_szFileExt		= szFileExt;
	};

	// AppName, KeyName
	void		SetIniFileInfo			(__in LPCTSTR szAppName, __in LPCTSTR szKeyName)
	{
		m_szAppName = szAppName;
		m_szKeyName	= szKeyName;
	};

	void		SetCurrentModel			(__in LPCTSTR szRecipe);

	BOOL		InsertBarcode			(__in LPCTSTR szBarcode);

	CString		GetModelFile			();

	
};

#endif // Dlg_ChangeModel_h__
