//*****************************************************************************
// Filename	: 	Def_T_IQ.h
// Created	:	2017/11/13 - 11:51
// Modified	:	2017/11/13 - 11:51
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_T_IQ_h__
#define Def_T_IQ_h__

#include <afxwin.h>

#pragma pack(push,1)


typedef enum enSpec_IQ
{
	Spec_IQ_TI01_Result = 0,
	Spec_IQ_TI02_Result = 0,
	Spec_IQ_TI03_Result = 0,
	Spec_IQ_TI04_Result = 0,
	Spec_IQ_TI05_Result = 0,
	Spec_IQ_MaxNum
};

static LPCTSTR	g_szSpec_IQ[] =
{
	_T(""),
	_T(""),
	NULL
};

typedef enum enSpec_Pat
{
	Spec_Pat_TI01_Result = 0,
	Spec_Pat_TI02_Result = 0,
	Spec_Pat_TI03_Result = 0,
	Spec_Pat_TI04_Result = 0,
	Spec_Pat_TI05_Result = 0,
	Spec_Pat_MaxNum
};

static LPCTSTR	g_szSpec_Pat[] =
{
	_T(""),
	_T(""),
	NULL
};

typedef struct _tag_IQ_Result
{
	_tag_IQ_Result()
	{
		Reset();
	};

	_tag_IQ_Result& operator= ( _tag_IQ_Result& ref)
	{
		return *this;
	};

	void Reset()
	{
	};

}ST_IQ_Result, *PST_IQ_Result;

typedef struct _tag_IQ_Spec
{
	_tag_IQ_Spec()
	{
		Reset();
	};

	_tag_IQ_Spec& operator= (const _tag_IQ_Spec& ref)
	{

		return *this;
	};

	void Reset()
	{
		
	};

}ST_IQ_Spec, *PST_IQ_Spec;

typedef struct _tag_IQ_Info
{
	_tag_IQ_Info()
	{
		Reset();
	};

	_tag_IQ_Info& operator= (const _tag_IQ_Info& ref)
	{
		return *this;
	};

	void Reset()
	{
		
	};

}ST_IQ_Info, *PST_IQ_Info;




#pragma pack(pop)

#endif // Def_T_IQ_h__
