//*****************************************************************************
// Filename	: 	Wnd_LGEInspInfo.cpp
// Created	:	2017/9/15 - 18:43
// Modified	:	2017/9/15 - 18:43
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_LGEInspInfo.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_LGEInspInfo.h"
#include "Def_WindowMessage_Cm.h"

// CWnd_LGEInspInfo

#define		IDC_BN_AUTO			1001
#define		IDC_BN_NORMAL		1002

static LPCTSTR g_szConsumItem[] =
{
	_T("Recipe		"),		// II_RecipeName,	
	_T("Model		"),		// II_Model,		
	_T("EQP ID		"),		// II_EQP_ID,		
	_T("Barcode (L)	"),		// II_Barcode_L,	
	_T("Lot Cnt	(L)	"),		// II_TestTime_L,	
	_T("TIME	(L)	"),		// II_TestTime_L,	
	_T("Barcode (R)	"),		// II_Barcode_R,	
	_T("Lot Cnt (R)	"),		// II_TestTime_R,	
	_T("TIME	(R)	"),		// II_TestTime_R,	
	NULL					 
};

IMPLEMENT_DYNAMIC(CWnd_LGEInspInfo, CWnd_BaseView)

CWnd_LGEInspInfo::CWnd_LGEInspInfo()
{
	VERIFY(m_font_Default.CreateFont(
		20,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	VERIFY(m_Font.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_LGEInspInfo::~CWnd_LGEInspInfo()
{
	m_font_Default.DeleteObject();
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_LGEInspInfo, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_BN_AUTO,		OnBnClickedBnAuto)
	ON_BN_CLICKED(IDC_BN_NORMAL,	OnBnClickedBnNormal)
END_MESSAGE_MAP()

// CWnd_LGEInspInfo message handlers

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/15 - 19:28
// Desc.		:
//=============================================================================
int CWnd_LGEInspInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_bn_Auto.Create(_T("Auto Start"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_AUTO);
	m_bn_Normal.Create(_T("Auto Stop"), dwStyle | BS_PUSHLIKE, rectDummy, this, IDC_BN_NORMAL);
	m_bn_Auto.SetMouseCursorHand();
	m_bn_Normal.SetMouseCursorHand();


	if (Sys_Focusing == m_InspectionType)
	{
		m_bn_Auto.SetWindowText(_T("Start"));
	}

	for (UINT nIdx = 0; nIdx < II_MaxEnum; nIdx++)
	{
		//m_st_Caption[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Caption[nIdx].SetColorStyle(CVGStatic::ColorStyle_Black);
		m_st_Caption[nIdx].SetFont_Gdip(L"Arial", 10.0F);

		//m_st_Value[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Value[nIdx].SetColorStyle(CVGStatic::ColorStyle_White);
		m_st_Value[nIdx].SetFont_Gdip(L"Arial", 10.0F);

		m_st_Caption[nIdx].Create(g_szConsumItem[nIdx], WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
		m_st_Value[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, rectDummy, this, IDC_STATIC);
	}


	SetPermissionMode(enPermissionMode::Permission_Operator);

	return 1;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/15 - 19:29
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;
	int iHalfWidth	= (iWidth - iSpacing) / 2;
	int iCapWidth	= 90;
	int iValWidth	= iWidth - iCapWidth;
	UINT iLimitRow	= (1 < m_nParaCnt) ? II_MaxEnum : (II_MaxEnum - 3);
	int iCtrlHeight = (iHeight - (iSpacing * 2)) / (iLimitRow + 1);
	int iLeftSub	= iLeft + iHalfWidth + iSpacing;

	if (1 == m_nParaCnt)
	{
		m_st_Caption[II_Barcode_L].SetText(L"Barcode");
		//m_st_Caption[II_LotCnt_L].SetText(L"Lot Cnt");
		m_st_Caption[II_TestTime_L].SetText(L"TIME");
	}
	else
	{
		m_st_Caption[II_Barcode_L].SetText(L"Barcode (L)");
		//m_st_Caption[II_LotCnt_L].SetText(L"Lot Cnt (L)");
		m_st_Caption[II_TestTime_L].SetText(L"TIME (L)");
	}

	if (m_bUseButton)
	{
		m_bn_Auto.MoveWindow(iLeft, iTop, iHalfWidth, iCtrlHeight);
		m_bn_Normal.MoveWindow(iLeftSub, iTop, iHalfWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
	}
	else
	{
		iCtrlHeight = 0;

		m_bn_Auto.MoveWindow(0, 0, 0, 0);
		m_bn_Normal.MoveWindow(0, 0, 0, 0);
	}

	iLeftSub = iLeft + iCapWidth;
	//int iCellHeight = (iHeight - iCtrlHeight - iSpacing) + (iLimitRow - 1);
	int iCellHeight = (iHeight - iTop) + (iLimitRow - 1);
	int iarCtrlHeight[II_MaxEnum] = { 0, };
	int iRemind = 0;

	for (UINT nIdx = 0; nIdx < iLimitRow; nIdx++)
	{
		iarCtrlHeight[nIdx] = iCellHeight / iLimitRow;
		iRemind += iarCtrlHeight[nIdx];
	}
	iRemind = iCellHeight - iRemind;
	
	for (int iIdx = iRemind; 0 < iIdx; iIdx--)
	{
		iarCtrlHeight[iIdx - 1] += 1;
	}

	for (UINT nIdx = 0; nIdx < iLimitRow; nIdx++)
	{
		m_st_Caption[nIdx].MoveWindow(iLeft, iTop, iCapWidth, iarCtrlHeight[nIdx]);
		m_st_Value[nIdx].MoveWindow(iLeftSub, iTop, iValWidth, iarCtrlHeight[nIdx]);
		iTop += (iarCtrlHeight[nIdx] - 1);
	}

	for (UINT nIdx = iLimitRow; nIdx < II_MaxEnum; nIdx++)
	{
		m_st_Caption[nIdx].MoveWindow(0, 0, 0, 0);
		m_st_Value[nIdx].MoveWindow(0, 0, 0, 0);
	}
}

//=============================================================================
// Method		: OnBnClickedBnAuto
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/26 - 16:49
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::OnBnClickedBnAuto()
{
	GetOwner()->SendNotifyMessage(WM_TEST_START, 0, 0);
}

//=============================================================================
// Method		: OnBnClickedBnNormal
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/26 - 16:49
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::OnBnClickedBnNormal()
{
	GetOwner()->SendNotifyMessage(WM_TEST_STOP, 0, 0);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/10/26 - 16:42
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2018/2/20 - 13:24
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::SetPermissionMode(__in enPermissionMode InspMode)
{
	if (GetSafeHwnd())
	{
		switch (InspMode)
		{
		case Permission_Operator:
		{
			m_bn_Auto.EnableWindow(FALSE);
			m_bn_Normal.EnableWindow(FALSE);
		}
			break;

		case Permission_Manager:
		case Permission_Administrator:
		{
			m_bn_Auto.EnableWindow(TRUE);
			m_bn_Normal.EnableWindow(TRUE);
		}
			break;

		default:
			break;
		}
	}
}

//=============================================================================
// Method		: SetUseButton
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2018/2/20 - 13:29
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::SetUseButton(__in BOOL bUse)
{
	if (m_bUseButton != bUse)
	{
		m_bUseButton = bUse;

		if (GetSafeHwnd())
		{
			CRect rc;
			GetClientRect(rc);
			OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
			//SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
		}
	}
}

//=============================================================================
// Method		: Set_ParaCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/9/23 - 22:28
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_ParaCount(__in UINT nCount)
{
	m_nParaCnt = (MAX_SITE_CNT < nCount) ? MAX_SITE_CNT : nCount;

	m_nParaCnt = (m_nParaCnt < 1) ? 1 : m_nParaCnt;

	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
	}
}

//=============================================================================
// Method		: ResetItem
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/21 - 16:51
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::ResetItem()
{
	for (UINT nIdx = 0; nIdx < II_MaxEnum; nIdx++)
	{
		m_st_Value[nIdx].SetText(L"");
	}
}

//=============================================================================
// Method		: ResetTestingInfo
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 22:36
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::ResetTestingInfo(__in UINT nParaIdx /*= 0*/)
{
	m_st_Value[II_Barcode_L].SetText(L"");
}

//=============================================================================
// Method		: ResetTestTime
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/26 - 13:40
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::ResetTestTime(__in UINT nParaIdx /*= 0*/)
{
	if (Para_Left == nParaIdx)
	{
		m_st_Value[II_TestTime_L].SetText(L"");
	}
	else
	{
		m_st_Value[II_TestTime_R].SetText(L"");
	}
}

//=============================================================================
// Method		: Set_Barcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/3 - 19:37
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_Barcode(__in LPCTSTR szBarcode, __in UINT nParaIdx /*= 0*/)
{
	if (Para_Left == nParaIdx)
	{
		m_st_Value[II_Barcode_L].SetText(szBarcode);
	}
	else
	{
		m_st_Value[II_Barcode_R].SetText(szBarcode);
	}
}

//=============================================================================
// Method		: Set_LotCnt
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/9 - 15:29
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_LotCnt(__in LPCTSTR szBarcode, __in UINT nParaIdx /*= 0*/)
{
	if (Para_Left == nParaIdx)
	{
		//m_st_Value[II_LotCnt_L].SetText(szBarcode);
	}
	else
	{
		//m_st_Value[II_LotCnt_R].SetText(szBarcode);
	}
}

//=============================================================================
// Method		: Set_SerialNumbers
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szSerialNumbers
// Qualifier	:
// Last Update	: 2017/11/20 - 16:53
// Desc.		:
//=============================================================================
// void CWnd_LGEInspInfo::Set_SerialNumbers(__in LPCTSTR szSerialNumbers)
// {
// 	m_st_Value[II_SN].SetText(szSerialNumbers);
// }

//=============================================================================
// Method		: Set_RecipeName
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szRecipeName
// Qualifier	:
// Last Update	: 2017/9/21 - 16:55
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_RecipeName(__in LPCTSTR szRecipeName)
{
	m_st_Value[II_RecipeName].SetText(szRecipeName);
}

//=============================================================================
// Method		: Set_Model
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szModel
// Qualifier	:
// Last Update	: 2018/2/20 - 17:24
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_Model(__in LPCTSTR szModel)
{
	m_st_Value[II_Model].SetText(szModel);
}

//=============================================================================
// Method		: Set_EquipmentID
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szEqpID
// Qualifier	:
// Last Update	: 2017/9/21 - 16:55
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_EquipmentID(__in LPCTSTR szEqpID)
{
	m_st_Value[II_EQP_ID].SetText(szEqpID);
}

//=============================================================================
// Method		: Set_ProcID
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szProcID
// Qualifier	:
// Last Update	: 2017/9/21 - 16:55
// Desc.		:
//=============================================================================
// void CWnd_LGEInspInfo::Set_ProcID(__in LPCTSTR szProcID)
// {
// 	m_st_Value[II_PROC_ID].SetText(szProcID);
// }

//=============================================================================
// Method		: Set_TestTime
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwMillisecond
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 22:30
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_CycleTime(__in DWORD dwMillisecond)
{
	CStringW szText;

	UINT nMilisecond	= (dwMillisecond % 1000) / 100;
	UINT nSecond		= (dwMillisecond / 1000) % 60;
	UINT nMinute		= (dwMillisecond / 60000) % 60;
	UINT nHour			= dwMillisecond / 3600000;

	szText.Format(L"%02d:%02d:%02d.%01d", nHour, nMinute, nSecond, nMilisecond);

	m_st_Value[II_TestTime_L].SetText(szText.GetBuffer(0));
	m_st_Value[II_TestTime_R].SetText(szText.GetBuffer(0));
}

//=============================================================================
// Method		: Set_TestTime
// Access		: public  
// Returns		: void
// Parameter	: __in DWORD dwMillisecond
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/3 - 19:39
// Desc.		:
//=============================================================================
void CWnd_LGEInspInfo::Set_TestTime(__in DWORD dwMillisecond, __in UINT nParaIdx /*= 0*/)
{
	CStringW szText;

	UINT nMilisecond	= (dwMillisecond % 1000) / 100;
	UINT nSecond		= (dwMillisecond / 1000) % 60;
	UINT nMinute		= (dwMillisecond / 60000) % 60;
	UINT nHour			= dwMillisecond / 3600000;

	szText.Format(L"%02d:%02d:%02d.%01d", nHour, nMinute, nSecond, nMilisecond);
	
	if (Para_Left == nParaIdx)
	{
		m_st_Value[II_TestTime_L].SetText(szText.GetBuffer(0));
	}
	else
	{
		m_st_Value[II_TestTime_R].SetText(szText.GetBuffer(0));
	}
}
