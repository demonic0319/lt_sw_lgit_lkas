﻿//*****************************************************************************
// Filename	: 	Grid_Yield_Test.cpp
// Created	:	2016/11/14 - 20:14
// Modified	:	2016/11/14 - 20:14
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Grid_Yield_Test.h"


static LPCTSTR lpszRowHeader[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef enum
{
	IDX_Y_Header = 0,
	IDX_Y_TestItem_S,
	IDX_Y_TestItem_E = IDX_Y_TestItem_S + MAX_TESTITEM - 1,
	IDX_ROW_MAX,
}enumRowHeaderCT;

static const COLORREF clrRowHeader[] =
{
	//RGB(50, 50, 200),
	RGB(0, 0, 0),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
	RGB(63, 101, 169),
};

typedef enum
{
	IDX_X_Header = 0,
	IDX_X_Item = IDX_X_Header,
	IDX_X_Count,
	IDX_X_FailRate,
	IDX_COL_MAX,
}enumColHeaderYield;

static LPCTSTR lpszColHeader[] =
{
	_T("Item"),
	_T("Cnt"),
	_T("Yield"),
	NULL
};

#define		RGB_BLACK		RGB(0x00, 0x00, 0x00)
#define		RGB_WHITE		RGB(0xFF, 0xFF, 0xFF)
#define		RGB_YELLOW		RGB(0xFF, 0xFF, 0x00)
#define		RGB_ROW_HEADER	RGB(63, 101,  169)
#define		RGB_COL_HEADER	RGB(0xFF, 200, 100)
#define		RGB_TITLE		RGB(150, 200, 0xFF)
#define		RGB_BK_ID		RGB(135, 169, 213)

#define		RGB_BIT_SET		RGB(123, 255,  75) //RGB(112, 173, 71)
#define		RGB_BIT_CLEAR	RGB(100,  10,  10) //RGB(237, 125, 49)
//#define		RGB_SELECT		RGB(0xFF, 200, 100) //RGB(150, 200, 0xFF)

//=============================================================================
//
//=============================================================================
CGrid_Yield_Test::CGrid_Yield_Test()
{
	m_nTestItemCount = 2;

	SetRowColCount(IDX_ROW_MAX, IDX_COL_MAX);

	//setup the fonts
	m_font_Header.CreateFont(14, 0, 0, 0, FW_NORMAL, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
	m_font_Data.CreateFont(14, 0, 0, 0, FW_LIGHT, 0, 0, 0, 0, 0, 0, ANTIALIASED_QUALITY, 0, _T("Arial"));
}

//=============================================================================
//
//=============================================================================
CGrid_Yield_Test::~CGrid_Yield_Test()
{
	m_font_Header.DeleteObject();
	m_font_Data.DeleteObject();
}

//=============================================================================
// Method		: OnSetup
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/14 - 14:30
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::OnSetup()
{
	__super::OnSetup();
}

//=============================================================================
// Method		: DrawGridOutline
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::DrawGridOutline()
{
	CGrid_Base::DrawGridOutline();

	SetDefFont(&m_font_Data);

	// 헤더를 설정한다.
	InitHeader();
}

//=============================================================================
// Method		: CGrid_Yield_Test::CalGridOutline
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::CalGridOutline()
{
	//CGrid_Base::CalGridOutline();

	// 윈도우 면적에 따라서 열의 너비를 결정
	CRect rect;
	GetWindowRect(&rect);
	int nWidth = rect.Width();
	int nHeight = rect.Height();

	if ((nWidth <= 0) || (nHeight <= 0))
		return;

	// 기본 열 추가 ---------------------------------------
	// 열 너비용 비율을 만듦 ()
	int		nColWidthRate[IDX_COL_MAX] = { 50, 25, 25};
	int		iTotalRate = 0;
	for (UINT nIdx = 0; nIdx < m_nMaxCols; nIdx++)
	{
		iTotalRate += nColWidthRate[nIdx];
	}

	int		iMisc = 0;
	int		iUnitWidth = 0;
	// 기본 열 추가 ---------------------------------------
	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		iUnitWidth = (nWidth * nColWidthRate[iCol]) / iTotalRate;
		iMisc += iUnitWidth;
		SetColWidth(iCol, iUnitWidth);
	}
	// Width 계산하고 남거나 모자르는 공간 계산하여 Name 영역에 추가	
	SetColWidth(IDX_X_Header, ((nWidth * nColWidthRate[IDX_X_Header]) / iTotalRate) + nWidth - iMisc);

	// 패턴 행 헤더 추가 ----------------------------------		
	UINT nUnitHeight = nHeight / m_nMaxRows;
	UINT nRemindHeight = nHeight - (nUnitHeight * m_nMaxRows);

	// Height 계산하고 남거나 모자르는 공간 계산하여 헤더 Height 추가 처리
	for (UINT iRow = 0; iRow < nRemindHeight; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight + 1);
	}

	for (UINT iRow = nRemindHeight; iRow < m_nMaxRows; iRow++)
	{
		SetRowHeight(iRow, nUnitHeight);
	}
}

//=============================================================================
// Method		: CGrid_Yield_Test::InitRowHeader
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::InitHeader()
{
	for (UINT iCol = 0; iCol < m_nMaxCols; iCol++)
	{
		QuickSetFont		(iCol, IDX_Y_Header, &m_font_Header);
		QuickSetBackColor	(iCol, IDX_Y_Header, RGB_BLACK);
		QuickSetTextColor	(iCol, IDX_Y_Header, RGB_WHITE);
		QuickSetText		(iCol, IDX_Y_Header, lpszColHeader[iCol]);
	}	

	for (UINT iRow = 1; iRow < m_nMaxRows; iRow++)
	{
		QuickSetFont		(IDX_X_Item, iRow, &m_font_Header);
		QuickSetBackColor	(IDX_X_Item, iRow, RGB_ROW_HEADER);
		QuickSetTextColor	(IDX_X_Item, iRow, RGB_WHITE);		

		QuickSetFont		(IDX_X_Count, iRow, &m_font_Data);
		QuickSetBackColor	(IDX_X_Count, iRow, RGB_WHITE);
		QuickSetTextColor	(IDX_X_Count, iRow, RGB_BLACK);
		QuickSetText		(IDX_X_Count, iRow, _T("0"));

		QuickSetFont		(IDX_X_FailRate, iRow, &m_font_Data);
		QuickSetBackColor	(IDX_X_FailRate, iRow, RGB_WHITE);
		QuickSetTextColor	(IDX_X_FailRate, iRow, RGB_BLACK);
		QuickSetText		(IDX_X_FailRate, iRow, _T("0.00 %"));
	}
}

//=============================================================================
// Method		: OnHint
// Access		: virtual protected  
// Returns		: int
// Parameter	: int col
// Parameter	: long row
// Parameter	: int section
// Parameter	: CString * string
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
int CGrid_Yield_Test::OnHint(int col, long row, int section, CString *string)
{
	return FALSE;
}

//=============================================================================
// Method		: CGrid_Yield_Test::OnGetCell
// Access		: virtual protected 
// Returns		: void
// Parameter	: int col
// Parameter	: long row
// Parameter	: CUGCell * cell
// Qualifier	:
// Last Update	: 2015/12/10 - 23:28
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::OnGetCell(int col, long row, CUGCell *cell)
{
	CGrid_Base::OnGetCell(col, row, cell);

	//   	switch (row)
	//   	{
	//   	case IDX_Y_Header:
	//   		if (IDX_X_Total == col)
	//   			cell->SetBorder(cell->GetBorder() | UG_BDR_RMEDIUM | UG_BDR_BMEDIUM);
	//   		break;
	//   	}
}

//=============================================================================
// Method		: OnDrawFocusRect
// Access		: virtual protected  
// Returns		: void
// Parameter	: CDC * dc
// Parameter	: RECT * rect
// Qualifier	:
// Last Update	: 2015/12/11 - 13:27
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::OnDrawFocusRect(CDC *dc, RECT *rect)
{

}

//=============================================================================
// Method		: SetTestItemInfo
// Access		:  
// Returns		: void
// Parameter	: __in const ST_TestItemInfo * pstTestItemInfo
// Qualifier	:
// Last Update	: 2017/10/12 - 13:18
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::SetTestItemInfo(__in const ST_TestItemInfo* pstTestItemInfo)
{
	if (NULL == pstTestItemInfo)
		return;

	m_nTestItemCount = (UINT)pstTestItemInfo->GetCount();

	SetRowColCount(IDX_Y_TestItem_S + m_nTestItemCount, IDX_COL_MAX);

	if (GetSafeHwnd())
	{
		DrawGridOutline();

		CalGridOutline();

		for (UINT nIdx = 0; nIdx < m_nTestItemCount; nIdx++)
		{
			QuickSetText(IDX_X_Item, IDX_Y_TestItem_S + nIdx, pstTestItemInfo->TestItemList[nIdx].szName);
		}

		RedrawCol(IDX_X_Item);
		//RedrawAll();
	}
}

//=============================================================================
// Method		: SetYield
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_YieldInfo * pstYield
// Qualifier	:
// Last Update	: 2016/11/14 - 20:21
// Desc.		:
//=============================================================================
void CGrid_Yield_Test::SetYield(__in const ST_YieldInfo* pstYield)
{
	CString szText;

	for (UINT nIdx = 0; nIdx < m_nTestItemCount; nIdx++)
	{
		szText.Format(_T("%d"), pstYield->TestItem[nIdx].dwTotal);
		QuickSetText(IDX_X_Count, IDX_Y_TestItem_S + nIdx, szText);

		szText.Format(_T("%.2f %%"), pstYield->TestItem[nIdx].fYield);
		QuickSetText(IDX_X_FailRate, IDX_Y_TestItem_S + nIdx, szText);
	}

	RedrawAll();
}

