//*****************************************************************************
// Filename	: 	Wnd_LGEInspInfo.h
// Created	:	2017/9/15 - 18:42
// Modified	:	2017/9/15 - 18:42
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_LGEInspInfo_h__
#define Wnd_LGEInspInfo_h__

#pragma once

#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_Enum_Cm.h"

//-----------------------------------------------------------------------------
// CWnd_LGEInspInfo
//-----------------------------------------------------------------------------
class CWnd_LGEInspInfo : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_LGEInspInfo)

public:
	CWnd_LGEInspInfo();
	virtual ~CWnd_LGEInspInfo();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	
	afx_msg void	OnBnClickedBnAuto			();
	afx_msg void	OnBnClickedBnNormal			();

	CFont			m_font_Default;
	CFont			m_Font;
	
	// MES ON/OFF

	// Auto / Normal
	CMFCButton		m_bn_Auto;
	CMFCButton		m_bn_Normal;

	enum enInspItem
	{
		
		II_RecipeName,		// 절차서
		II_Model,			// 제품 모델
		II_EQP_ID,			// Equipment ID
		II_Barcode_L,		// Barcode
		//II_LotCnt_L,		// Test Time (검사 시작 -> 종료까지의 1 Cycle의 시간을 Display : 0.1초 단위)
		II_TestTime_L,		// Test Time (검사 시작 -> 종료까지의 1 Cycle의 시간을 Display : 0.1초 단위)
		II_Barcode_R,		// Barcode
		II_LotCnt_R,		// Test Time (검사 시작 -> 종료까지의 1 Cycle의 시간을 Display : 0.1초 단위)
		II_TestTime_R,		// Test Time (검사 시작 -> 종료까지의 1 Cycle의 시간을 Display : 0.1초 단위)
		II_MaxEnum,
	};

	CVGStatic			m_st_Caption[II_MaxEnum];
	CVGStatic			m_st_Value[II_MaxEnum];

	UINT				m_nParaCnt				= MAX_SITE_CNT;
	BOOL				m_bUseButton			= TRUE;

	enInsptrSysType		m_InspectionType		= enInsptrSysType::Sys_Focusing;

public:

	// 검사기 종류 설정
	void	SetSystemType		(__in enInsptrSysType nSysType);

	// 사용자 권한 모드 설정
	void	SetPermissionMode	(__in enPermissionMode InspMode);

	// 상단 버튼 사용 여부
	void	SetUseButton		(__in BOOL bUse);

	// 파라/채널 개수
	void	Set_ParaCount		(__in UINT nCount);

	// 아이템 데이터 초기화
	void	ResetItem			();
	// 검사 진행 정보 초기화
	void	ResetTestingInfo	(__in UINT nParaIdx = 0);
	// 검사 아이템 데이터 초기화
	void	ResetTestTime		(__in UINT nParaIdx = 0);

	// Barcode
	void	Set_Barcode			(__in LPCTSTR szBarcode, __in UINT nParaIdx = 0);
	// Barcode
	void	Set_LotCnt			(__in LPCTSTR szBarcode, __in UINT nParaIdx = 0);
	// Recipe Name
	void	Set_RecipeName		(__in LPCTSTR szRecipeName);
	// Model
	void	Set_Model			(__in LPCTSTR szModel);
	// Equipment ID
	void	Set_EquipmentID		(__in LPCTSTR szEqpID);
	// Test Time
	void	Set_CycleTime		(__in DWORD dwMillisecond);
	void	Set_TestTime		(__in DWORD dwMillisecond, __in UINT nParaIdx = 0);


};


#endif // Wnd_LGEInspInfo_h__
