﻿//*****************************************************************************
// Filename	: Def_Enum_Cm.h
// Created	: 2010/11/23
// Modified	: 2016/06/30
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_Enum_Cm_h__
#define Def_Enum_Cm_h__

#include <afxwin.h>

//-------------------------------------------------------------------
// UI 관련
//-------------------------------------------------------------------
// 로그 탭
typedef enum
{
	LOGTYPE_NORMAL = 0,	// 기본
	LOGTYPE_GUI,		// UI 관련
	LOGTYPE_COM,		// 통신 관련
	LOGTYPE_OPR,		// 
	LOGTYPE_MAX,
}enumLogType;

static LPCTSTR lpszLogType[] = { _T(""), _T("GUI"), _T("COM"), _T("OPR"), NULL };

// 통신 연결 상태
typedef enum enCommStatusType
{
	COMM_STATUS_NOTCONNECTED = 0,
	COMM_STATUS_CONNECT,
	COMM_STATUS_DISCONNECT,
	COMM_STATUS_ERROR,
	//COMM_STATUS_WARNING,
	//COMM_STATUS_EXECUTE,
	//COMM_STATUS_PAUSE,
	COMM_STATUS_SYNC_OK,
	COMM_STATUS_NO_ACK,
};

static LPCTSTR lpszCommStatus[] = 
{
	_T("미 연결"),
	_T("연결"),
	_T("연결 해제"),
	_T("오류"),
	//_T("경고"),
	//_T("실행"),
	//_T("일시정지"),
	_T("싱크 완료"),
	_T("No ACK"),
	NULL
};

static COLORREF	clrJudgeColor[] =
{
	RGB(0xEE, 0x50, 0x50),	// NG 
	RGB(0x90, 0xEE, 0x90),	// OK
	RGB(0xFF, 0xFF, 0xFF),	// Wait	
	RGB(0xFF, 0xFF, 0x00),	// Error
	RGB(0xAA, 0xAA, 0xAA),	// No Data
	RGB(0x77, 0x77, 0x77),	// Not Used
};

typedef enum enJudgment
{
	JUDGE_NG		= 0,
	JUDGE_OK		= 1,
	JUDGE_EnumMax,
};

typedef enum enLotStatus
{
	LOT_End = 0,
	LOT_Start = 1
};

typedef enum enBitStatus
{
	Bit_Clear	= 0,
	Bit_Set		= 1,
	Bit_EnumMax,
};

typedef enum enPowerOnOff
{
	Power_Off,
	Power_On,
};

typedef enum enProductLoad
{
	Product_Unload,
	Product_Load,
};

static LPCTSTR g_szProductLoad[] =
{
	_T("Unloading"),
	_T("Loading"),
	NULL
};

typedef enum enMES_Online
{
	MES_Offline,
	MES_Online,
};

static LPCTSTR g_szMES_Online[] =
{
	_T("Offline"),
	_T("Online"),
	NULL
};

typedef enum enAutoMode
{
	AM_AutoMode,
	AM_ManualMode,
};

static LPCTSTR g_szAutoMode[] =
{
	_T("Auto"),
	_T("Manual"),
	NULL
};

//---------------------------------------------------------
// Access 모드 설정값
typedef enum enPermissionMode
{
	Permission_Operator,		// 옵션 보기/수정 불가
	Permission_Manager,			// 옵션 보기 가능, 수정 불가
	Permission_Administrator,	// 옵션 보기/수정 가능
	Permission_EnumMax,
};

static LPCTSTR g_szPermissionMode[] =
{
	_T("Operator"),
	_T("Manager"),
	_T("Administrator"),
	NULL
};

static COLORREF g_clrPermissionMode[] =
{
	RGB(  0, 255, 0),
	RGB(190, 190, 0),
	RGB(255,   0, 0),
};

//---------------------------------------------------------
// 설비 검사 구동 방식
typedef enum enOperateMode
{
	OpMode_Production,		// 제품 생산 모드
	OpMode_Master,			// 마스터 모드
	OpMode_StartUp_Check,	// 시업 점검 모드
	OpMode_DryRun,			// 기구 동작 테스트 모드
	OpMode_MaxEnum,
};

static LPCTSTR g_szOperateMode[] =
{
	_T("Product Production"),
	_T("Master Mode"),
	_T("Start-up Check"),
	_T("Dry Run"),
	NULL
};

//-----------------------------------------------------------------------------
// 검사기 구분용도
//-----------------------------------------------------------------------------
#define SYS_CUSTOMER			_T("LGIT")


#define SYS_2D_CAL				0	// 2D Calibration
#define SYS_3D_CAL				1	// 3D Calibration
#define SYS_FOCUSING			2	// Focusing
#define SYS_IMAGE_TEST			3	// Image Test
#define SYS_IR_IMAGE_TEST		4	// 반사판 Image Test (광원 제어 없음)

typedef enum enInsptrSysType
{
	Sys_2D_Cal				= SYS_2D_CAL,			// 2D Calibration
	Sys_3D_Cal				= SYS_3D_CAL,			// 3D Calibration
	Sys_Focusing			= SYS_FOCUSING,			// Focusing
	Sys_Image_Test			= SYS_IMAGE_TEST,		// Image Test
	Sys_IR_Image_Test		= SYS_IR_IMAGE_TEST,	// Image Test
};

static LPCTSTR g_szInsptrSysType[] =
{
	_T("2D Calibration"),			// 2D Calibration
	_T("3D Calibration"),			// 3D Calibration
	_T("Focusing Inspection"),		// Focusing
	_T("OMS Front Image Test"),		// Front Image Test (Front Set)
	_T("LKAS Image Test"),			// 반사판 Image Test (광원 제어 없음)
	NULL
};

static LPCTSTR g_szProgramName[] =
{
	_T("2D_CAL"),		// 2D Calibration
	_T("3D_CAL"),		// 3D Calibration
	_T("ST_CAL"),		// Stereo Calibration
	_T("FOCUS"),		// Focusing
	_T("IMT_FRT"),		// Front Image Test (Front Set)
	_T("IMT_IR"),		// 반사판 Image Test (광원 제어 없음)
	NULL
};

//-----------------------------------------------------------------------------
// 검사기 구분 테이블 (광원, 전원, 바코드, CAN, MES, PLC)
//-----------------------------------------------------------------------------
typedef struct _tag_InspectorTable
{
	LONG	SysType;			// 검사기 구분
	BOOL	UseMotion;			// Motion 보드 사용여부
	BOOL	UseDIO;				// I/O 보드 사용여부
	UINT	Grabber_Cnt;		// 영상 보드 개수
	UINT	CtrlBrd_Cnt;		// 카메라 보드 개수
	UINT	LightPSU_Cnt;		// ODA 광원 제어 보드 개수
	UINT	LightBrd_Cnt;		// 루리텍 광원 제어 보드 개수
	UINT	nIndigatorCnt;		// 모터 인디게이터 개수
	UINT	MaxStep_Cnt;		// 검사 스텝 최대 개수
}ST_InspectorTable;

static ST_InspectorTable g_InspectorTable[] =
{	//검사기				Motion,		IO,			그래버,		전원,	ODA광,		Luri광,		인디케이터,		스텝
	{ Sys_2D_Cal,			TRUE,		TRUE,		2,			2,		3,			0,			0,				30, },	// 2 Cam
	{ Sys_3D_Cal,			TRUE,		TRUE,		2,			0,		0,			0,			0,				30, },	// 2 Cam
	{ Sys_Focusing,			TRUE,		TRUE,		1,			1,		3,			1,			0,				35, },	// 2 Cam
	{ Sys_Image_Test,		TRUE,		TRUE,		2,			2,		0,			1,			0,				25, },	// 2 Cam
	{ Sys_IR_Image_Test,	TRUE,		TRUE,		1,			0,		0,			1,			0,				20, },	// 2 Cam
	NULL
};

//-----------------------------------------------------------------------------
// 카메라 모델 종류
//-----------------------------------------------------------------------------
typedef enum enModelType
{
	Model_OMS_Entry,
	Model_OMS_Front,		// Front 반제
	Model_MRA2,
	Model_IKC,
	Model_OMS_Front_Set,	// Front 세트

	Model_OV10642,			// LKAS
	Model_AR0220,			// LKAS

	Model_MaxEnum,
};

static	LPCTSTR	g_szModelName[] =
{
	_T("OMS Entry"),
	_T("OMS Front Single"),
	_T("MRA2"),
	_T("IKC"),
	_T("OMS Front Set"),

	_T("OV10642"),
	_T("AR0220"),
	NULL
};

static	LPCTSTR	g_szModelArg[] =
{
	_T("E"),
	_T("F"),
	_T("M"),
	_T("I"),
	_T("FS"),
	NULL
};

static	LPCTSTR	g_szModelFolder[] =
{
	_T("OMS_Entry"),
	_T("OMS_Front_Single"),
	_T("MRA2"),
	_T("IKC"),
	_T("OMS_Front_Set"),
	_T("OV10642"),
	_T("AR0220"),
	NULL
};

typedef struct _tag_IR_ModelTable
{
	LONG	ModelType;			// 카메라 모델 구분
	UINT	Camera_Cnt;			// 모듈 당 카메라 수
	UINT	VCSEL_Cnt;			// VCSEL 개수
	UINT	Img_Width;			// 해상도 Width
	UINT	Img_Height;			// 해상도 Height
	UINT	Etc;				// 기타
}ST_IR_ModelTable;

static ST_IR_ModelTable g_IR_ModelTable[] =
{	// 검사기				// 카메라	// VCSEL	// Width	// Height	// 기타
	{ Model_OMS_Entry,		1,			1,			640,		480,		0, },	// 1 Cam
	{ Model_OMS_Front,		1,			1,			640,		480,		0, },	// 1 Cam (반제)
	{ Model_MRA2,			2,			2,			1280,		960,		0, },	// 2 Cam
	{ Model_IKC,			1,			1,			1280,		720,		0, },	// 1 Cam
	{ Model_OMS_Front_Set,	2,			2,			640,		480,		0, },	// 2 Cam (세트)
	{ Model_OV10642,		1,			0,			1280,		1080,		0, },	// 1 Cam (세트)
	{ Model_AR0220,			1,			0,			1820,		940,		0, },	// 1 Cam (세트)
	NULL
};
//-----------------------------------------------------------------------------

//---------------------------------------------------------
// Lamp 색상
//---------------------------------------------------------
typedef enum enLampColor
{
	Lamp_Red,
	Lamp_Yellow,
	Lamp_Green,
	Lamp_All,
};

//---------------------------------------------------------
// Buzzer 종류
//---------------------------------------------------------
typedef enum enBuzzerType
{
	Buzzer_01,
	Buzzer_02,
	Buzzer_03,
	Buzzer_04,
	Buzzer_05,
};

typedef enum enBarcodeChk
{
	Barcode_UnknownErr,
	Barcode_Pass,	// 사용 가능
	Barcode_Overap,	// 중복
	Barcode_Length,	// 글자수 오류
	Barcode_Count,	// 입력 개수 오류
};

static LPCTSTR g_szBarcodeChk[] = 
{
	_T("Unknown Error"),
	_T("OK"),
	_T("바코드 중복"),
	_T("바코드 길이 오류"),
	_T("바코드 입력 개수 오류"),
	NULL
};

typedef enum ParaPos
{
	Para_Left,		// 첫번째 파라 (Left)
	Para_Right,		// 두번째 파라 (Right)
	Para_MaxEnum,
};

static	LPCTSTR	g_szParaName[] =
{
	_T("Left"),
	_T("Right"),
	NULL
};

typedef enum enLight_Item
{
	Light_I_ChartTest,
	Light_I_PixelDefectW,
	Light_I_PixelDefectB,
	Light_I_Ymean,
	Light_I_Shading,
	Light_I_MaxEnum,
};

static	LPCTSTR	g_szLightItem[] =
{
	_T("MTF, Distortion, Optical Axis"),
	_T("Pixel Defect White"),
	_T("Pixel Defect Black"),
	_T("Y mean"),
	_T("Shading"),
	NULL
};

typedef enum enChartType
{
	Chart_Center,
	Chart_Left,
	Chart_Right,
	Chart_MaxEnum,
};

static	LPCTSTR	g_szChartType[] =
{
	_T("Center"),
	_T("Left"),
	_T("Right"),
	NULL
};

// 전류 데이터
typedef enum enCurrentType_OMS
{
	Curr_OMS_1_8V,
	Curr_OMS_3_3V,
	Curr_OMS_9_0V,
	Curr_OMS_14_7V,
	Curr_OMS_M5_7V,
};

static	LPCTSTR	g_szCurrentType_OMS[] =
{
	_T("1.8 V"),
	_T("3.3 V"),
	_T("9.0 V"),
	_T("14.7 V"),
	_T("-5 ~ 7V"),
	NULL
};

typedef enum enCurrentType_MRA2
{
	Curr_MRA2_1_8V,
	Curr_MRA2_2_8V,
};

static	LPCTSTR	g_szCurrentType_MRA2[] =
{
	_T("1.8 V"),
	_T("2.8 V"),
	NULL
};

typedef enum enOverlayItem
{
	Ovr_Ymean = 0,
	Ovr_LCB,
	Ovr_BlackSpot,
	Ovr_Current,
	Ovr_DynamicBW,
	Ovr_SFR,
	Ovr_Chart,
	Ovr_Shading,
	Ovr_RI,
	Ovr_Displace,
	Ovr_Vision,
	Ovr_IIC,
    Ovr_Distortion,
	Ovr_DefectBlack,
	Ovr_DefectWhite,
	Ovr_OpticalCenter,
	Ovr_MaxEnum,
};

/*카메라의 영상상태*/
enum enCamState
{
	/*좌우반전*/
	CAM_STATE_MIRROR = 0,
	/*상하반전*/
	CAM_STATE_FLIP,
	/*오리지널*/
	CAM_STATE_ORIGINAL,
	/*로테이트*/
	CAM_STATE_ROTATE,
	CAM_STATE_MAXNUM,
};

static LPCTSTR	g_szCamState[] =
{
	_T("Horizontal Mirror"),
	_T("Vertical Flip"),
	_T("Original"),
	_T("Mirror & Flip"),
	NULL
};

//---------------------------------------------------------

// 레시피 설정 파일 확장자
#define	RECIPE_FILE_EXT			_T("luri")

// 소모품 설정 파일 확장자
#define	CONSUM_FILE_EXT			_T("ini")

// 모터 설정 파일 확장자
#define	MOTOR_FILE_EXT			_T("mot")

// 유지 설정 파일 확장자
#define	MAINTENANCE_FILE_EXT	_T("mai")
//---------------------------------------------------------

typedef enum _ConstVar
{
	// 대기 시간
	Timeout_WaitEndTest			= 600000,
	Timeout_WaitPLCOut			= 600000,
	Timeout_WaitFinish_TestItem	= 20000,	// OnCAN_ReqImageCapture : 5초 + Send_Req_CornerPt_Start : 5초 + Send_Req_CornerPt_Data : 10초
	Timeout_Wait_ReqCapture		= 11000,	// OnCAN_ReqImageCapture : 5초

	MAX_OPERATION_THREAD		= 6,	// 최대 개별 검사용 쓰레드 개수
	MAX_SITE_CNT				= 2,	// 최대 Para 개수
	MAX_MODULE_CNT				= 2,	// 최대 제품 투입 개수 (Left, Right)
	MAX_LIGHT_BRD_CNT			= 3,	// 최대 광원 보드 개수

	MAX_DIGITAL_IO				= 64,	// 최대 AJIN IO 사용 개수
	MAX_TESTITEM				= 16,	// 최대 검사 항목
	MAX_STEP_COUNT				= 60,	// 검사 스텝 최대 개수

	Barcode_Leng				= 15,
};

#endif // Def_Enum_Cm_h__

