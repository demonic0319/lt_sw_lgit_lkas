﻿//*****************************************************************************
// Filename	: 	Def_ErrorCode.h
// Created	:	2016/10/31 - 0:48
// Modified	:	2016/10/31 - 0:48
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_ErrorCode_h__
#define Def_ErrorCode_h__

#include <afxwin.h>
#include "SerialCom_Base.h"
#include "BSocketClient.h"

// 결과 코드 채널 개별 검사
enum enResultCode
{
	RC_UnknownError = 0,
	RC_OK,
	RC_TestSkip,
	RC_Invalid_Handle,
	RC_Invalid_Point,
	RC_Exception,
	RC_Safty_Err,
	RC_DeviceComm_Err,
	RC_Wrong_InspectionSystem,
	RC_Recipe_Err,
	RC_NoBarcode,	// 바코드 없음
	RC_NoPallet_ID,
	RC_LimitPogoCnt,
	RC_NotReadyTest,
	RC_AlreadyLoadUnload_OtherPara,
	RC_AlreadyTesting,
	RC_AlreadyTestItem_Thread,
	RC_Forced_Stoped,
	RC_WaitEndTest_TimeOut,
	RC_WaitEndTest_Wait_Faild,
	RC_WaitPLCOut_TimeOut,
	RC_WaitPLCOut_Wait_Faild,
	RC_LoadingFailed,

	RC_Grab_Err_Misc,
	RC_Grab_Err_NotOpen,
	RC_Grab_Err_NoSignal,
	RC_Grab_Err_NullBuffer,
	RC_Grab_Err_I2C_Write,
	RC_Grab_Err_I2C_Read,
	RC_Grab_Err_CapStartFail,
	RC_Grab_Err_CapStopFail,
	RC_Grab_Err_RegisterInitFail,
	RC_EEPROM_Err_Read,
	RC_EEPROM_Err_Write,
	RC_EEPROM_Err_Verify,
	RC_EEPROM_Err_Reboot,
	RC_EEPROM_Err_Checksum,

	RC_FixBCR_Err_Misc,
	RC_FixBCR_Err_PortOpen,
	RC_FixBCR_Err_Sync,
	RC_FixBCR_Err_SemaphoreLocked,
	RC_FixBCR_Err_Ack,
	RC_FixBCR_Err_AckTimeout,
	RC_FixBCR_Err_AckErrRecieved,
	RC_FixBCR_Err_Transfer,
	RC_FixBCR_Err_Parameter,
	RC_FixBCR_Err_Protocol,
	RC_FixBCR_Err_Protocol_CheckSum,

	RC_Light_Brd_Err_Misc,
	RC_Light_Brd_Err_PortOpen,
	RC_Light_Brd_Err_Sync,
	RC_Light_Brd_Err_SemaphoreLocked,
	RC_Light_Brd_Err_Ack,
	RC_Light_Brd_Err_AckTimeout,
	RC_Light_Brd_Err_AckErrRecieved,
	RC_Light_Brd_Err_Transfer,
	RC_Light_Brd_Err_Parameter,
	RC_Light_Brd_Err_Protocol,
	RC_Light_Brd_Err_Protocol_CheckSum,

	RC_CamBrd_Err_Misc,
	RC_CamBrd_Err_PortOpen,
	RC_CamBrd_Err_Sync,
	RC_CamBrd_Err_SemaphoreLocked,
	RC_CamBrd_Err_Ack,
	RC_CamBrd_Err_AckTimeout,
	RC_CamBrd_Err_AckErrRecieved,
	RC_CamBrd_Err_Transfer,
	RC_CamBrd_Err_Parameter,
	RC_CamBrd_Err_Protocol,
	RC_CamBrd_Err_Protocol_CheckSum,
	RC_CamBrd_Err_Short_Fail,

	RC_PSU_Err_Misc,
	RC_PSU_Err_PortOpen,
	RC_PSU_Err_Sync,
	RC_PSU_Err_SemaphoreLocked,
	RC_PSU_Err_Ack,
	RC_PSU_Err_AckTimeout,
	RC_PSU_Err_AckErrRecieved,
	RC_PSU_Err_Transfer,
	RC_PSU_Err_Parameter,
	RC_PSU_Err_Protocol,
	RC_PSU_Err_Protocol_CheckSum,
	RC_PSU_Err_Short_Fail,

	RC_MES_Err_Unknown,
	RC_MES_Err_NotOpen,
	RC_MES_Err_Offline,
	RC_MES_Err_Semaphore,
	RC_MES_Err_Transfer,
	RC_MES_Err_Parameter,
	RC_MES_Err_Ack,
	RC_MES_Err_Ack_Timeout,
	RC_MES_Err_Ack_RecvFail,
	RC_MES_Err_Ack_Protocol,
	RC_MES_Err_Protocol,
	RC_MES_Err_Protocol_CheckSum,
	RC_MES_Err_ACK_NotAccepted,
	RC_MES_Err_Wrong_EQP_Info,
	RC_MES_Err_BarcodeValidation,
	RC_MES_Err_SN_Verify,
	RC_MES_Err_PartNo_Verify,
	RC_MES_Err_HW_Ver_Verify,
	RC_MES_Err_SW_Ver_Verify,
	RC_MES_Err_19,

	RC_DIO_Err_MainPower,
	RC_DIO_Err_EMO,
	RC_DIO_Err_AreaSensor,
	RC_DIO_Err_DoorSensor,
	RC_DIO_Err_JIGCorverCheck,
	RC_DIO_Err_06,
	RC_DIO_Err_07,
	RC_DIO_Err_08,
	RC_DIO_Err_09,
	RC_DIO_Err_10,
	RC_DIO_Err_11,
	RC_DIO_Err_12,
	RC_DIO_Err_13,
	RC_DIO_Err_14,
	RC_DIO_Err_15,
	RC_DIO_Err_16,
	RC_DIO_Err_17,
	RC_DIO_Err_18,
	RC_DIO_Err_19,
	RC_DIO_Err_20,

	RC_Motion_Err_BoardOpen,
	RC_Motion_Err_PalletFix,
	RC_Motion_Err_PalletUnFix,
	RC_Motion_Err_MoveAxis,
	RC_Motion_Err_Left_StageX,
	RC_Motion_Err_Left_StageY,
	RC_Motion_Err_Left_StageZ,
	RC_Motion_Err_Left_StageR,
	RC_Motion_Err_Right_StageX,
	RC_Motion_Err_Right_StageY,
	RC_Motion_Err_Right_StageZ,
	RC_Motion_Err_Right_StageR,
	RC_Motion_Err_Alarm,
	RC_Motion_Err_Amp,
	RC_Motion_Err_Alarm_Clear,
	RC_Motion_Err_OriginTimeOut,
	RC_Motion_Err_OriginUserStop,
	RC_Motion_Err_OriginStop,
	RC_Motion_Err_Left_StageXY,
	RC_Motion_Err_Right_StageXY,
	RC_Motion_Err_MotorCrash,
	RC_Motion_Err_Chart,
	RC_Motion_Err_DummyIn,
	RC_Motion_Err_DummyOut,
	RC_Motion_Err_Dummy,
	RC_Motion_Err_Cylinger_Check,
	RC_Motion_Err_Moudle_Check,
	RC_Motion_Err_Port_TimeOut,
	RC_Motion_Err_Position,
	RC_Motion_Err_EmptyTeach,

	RC_LIB_2DCAL_SetParam,
	RC_LIB_2DCAL_CornerExt,
	RC_LIB_2DCAL_ExecIntrinsic,
	RC_LIB_SteCAL_SetParam,
	RC_LIB_SteCAL_CornerExt,
	RC_LIB_SteCAL_ExecIntrinsic,
	
	RC_TEST_Err_FiducialMark,
	RC_Err_TestFail,
	RC_Err_DTC,
	RC_NoImage,
	RC_Detect,

	RC_Rution_FixLocking_Err,
	RC_Motion_Err_Origin_Reset,
	RC_Motion_Err_Origin_EStop,
	RC_Motion_Err_Origin_Start,

	RC_Motor_Err_EStop,
	RC_Motor_Err_Alarm,
	RC_Motor_Err_Reset,
	RC_Motor_Err_Time,
	RC_Motor_Err_Origin,
	RC_Motor_Err_MultiMove,

	RC_Motor_Err_ReleaseTorque,
	RC_Motor_Err_LockingTorque,










	RC_Max,
};

static LPCTSTR g_szResultCode[] =
{
	_T("UnknownError"),						// RC_UnknownError = 0,			
	_T("OK"),								// RC_OK,		
	_T("TestSkip"),							// RC_TestSkip,			
	_T("Invalid_Handle"),					// RC_Invalid_Handle,		
	_T("Invalid_Point"),					// RC_Invalid_Point,		
	_T("Exception"),						// RC_Exception,		
	_T("Safty_Err"),						// RC_Safty_Err,		
	_T("DeviceComm_Err"),					// RC_DeviceComm_Err,		
	_T("Wrong_InspectionSystem"),			// RC_Wrong_InspectionSystem,		
	_T("Recipe_Err"),						// RC_Recipe_Err,		
	_T("No Barcode"),						// RC_NoBarcode,	// 바코드 없음			
	_T("NoPallet_ID"),						// RC_NoPallet_ID,		
	_T("LimitPogoCnt"),						// RC_LimitPogoCnt,		
	_T("NotReadyTest"),				  		// RC_NotReadyTest,		
	_T("AlreadyLoadUnload_OtherPara"),		// RC_AlreadyLoadUnload_OtherPara,		
	_T("AlreadyTest_OtherPara"),	  		// RC_AlreadyTesting,
	_T("AlreadyTestItem_Thread"),	  		// RC_AlreadyTestItem_Thread,	
	_T("Forced_Stoped"),			  		// RC_Forced_Stoped,		
	_T("WaitEndTest_TimeOut"),		  		// RC_WaitEndTest_TimeOut,		
	_T("WaitEndTest_Wait_Faild"),	  		// RC_WaitEndTest_Wait_Faild,		
	_T("WaitPLCOut_TimeOut"),		  		// RC_WaitPLCOut_TimeOut,		
	_T("WaitPLCOut_Wait_Faild"),	  		// RC_WaitPLCOut_Wait_Faild,
	_T("LoadingFailed"),					// RC_LoadingFailed,

	_T("Grab_Err_Misc"),					// RC_Grab_Err_Misc,
	_T("Grab_Err_NotOpen"),					// RC_Grab_Err_NotOpen,
	_T("Grab_Err_NoSignal"),				// RC_Grab_Err_NoSignal,
	_T("Grab_Err_NullBuffer"),				// RC_Grab_Err_NullBuffer,
	_T("Grab_Err_I2C_Write"),				// RC_Grab_Err_I2C_Write,
	_T("Grab_Err_I2C_Read"),				// RC_Grab_Err_I2C_Read,
	_T("Grab_Err_CapStartFail"),			// RC_Grab_Err_CapStartFail,
	_T("Grab_Err_CapStopFail"),				// RC_Grab_Err_CapStopFail,
	_T("Grab_Err_RegisterInitFail"),		// RC_Grab_Err_RegisterInitFail,
	_T("EEPROM_Err_Read"),					// RC_EEPROM_Err_Read,
	_T("EEPROM_Err_Write"),					// RC_EEPROM_Err_Write,
	_T("EEPROM_Err_Verify"),				// RC_EEPROM_Err_Verify,
	_T("EEPROM_Err_Reboot"),				// RC_EEPROM_Err_Reboot,
	_T("EEPROM_Err_Checksum"),				// RC_EEPROM_Err_Checksum,

	_T("FixBCR_Err_Misc"),					// RC_FixBCR_Err_Misc,		
	_T("FixBCR_Err_PortOpen"),				// RC_FixBCR_Err_PortOpen,		
	_T("FixBCR_Err_Sync"),					// RC_FixBCR_Err_Sync,		
	_T("FixBCR_Err_SemaphoreLocked"),		// RC_FixBCR_Err_SemaphoreLocked,		
	_T("FixBCR_Err_Ack"),					// RC_FixBCR_Err_Ack,		
	_T("FixBCR_Err_AckTimeout"),			// RC_FixBCR_Err_AckTimeout,		
	_T("FixBCR_Err_AckErrRecieved"),		// RC_FixBCR_Err_AckErrRecieved,		
	_T("FixBCR_Err_Transfer"),				// RC_FixBCR_Err_Transfer,		
	_T("FixBCR_Err_Parameter"),				// RC_FixBCR_Err_Parameter,		
	_T("FixBCR_Err_Protocol"),				// RC_FixBCR_Err_Protocol,		
	_T("FixBCR_Err_Protocol_CheckSum"),		// RC_FixBCR_Err_Protocol_CheckSum,

	_T("Light_Brd_Err_Misc"),				// RC_Light_Brd_Err_Misc,		
	_T("Light_Brd_Err_PortOpen"),			// RC_Light_Brd_Err_PortOpen,		
	_T("Light_Brd_Err_Sync"),				// RC_Light_Brd_Err_Sync,		
	_T("Light_Brd_Err_SemaphoreLocked"),	// RC_Light_Brd_Err_SemaphoreLocked,		
	_T("Light_Brd_Err_Ack"),				// RC_Light_Brd_Err_Ack,		
	_T("Light_Brd_Err_AckTimeout"),			// RC_Light_Brd_Err_AckTimeout,		
	_T("Light_Brd_Err_AckErrRecieved"),		// RC_Light_Brd_Err_AckErrRecieved,		
	_T("Light_Brd_Err_Transfer"),			// RC_Light_Brd_Err_Transfer,		
	_T("Light_Brd_Err_Parameter"),			// RC_Light_Brd_Err_Parameter,		
	_T("Light_Brd_Err_Protocol"),			// RC_Light_Brd_Err_Protocol,		
	_T("Light_Brd_Err_Protocol_CheckSum"),	// RC_Light_Brd_Err_Protocol_CheckSum,

	_T("CamBrd_Err_Misc"),					// RC_CamBrd_Err_Misc,		
	_T("CamBrd_Err_PortOpen"),				// RC_CamBrd_Err_PortOpen,		
	_T("CamBrd_Err_Sync"),					// RC_CamBrd_Err_Sync,		
	_T("CamBrd_Err_SemaphoreLocked"),		// RC_CamBrd_Err_SemaphoreLocked,		
	_T("CamBrd_Err_Ack"),					// RC_CamBrd_Err_Ack,		
	_T("CamBrd_Err_AckTimeout"),			// RC_CamBrd_Err_AckTimeout,		
	_T("CamBrd_Err_AckErrRecieved"),		// RC_CamBrd_Err_AckErrRecieved,		
	_T("CamBrd_Err_Transfer"),				// RC_CamBrd_Err_Transfer,
	_T("CamBrd_Err_Parameter"),				// RC_CamBrd_Err_Parameter,
	_T("CamBrd_Err_Protocol"),				// RC_CamBrd_Err_Protocol,
	_T("CamBrd_Err_Protocol_CheckSum"),		// RC_CamBrd_Err_Protocol_CheckSum,
	_T("CamBrd_Err_Short_Fail"),			// RC_CamBrd_Err_Short_Fail,

	_T("PSU_Err_Misc"),						// RC_PSU_Err_Misc,
	_T("PSU_Err_PortOpen"),					// RC_PSU_Err_PortOpen,
	_T("PSU_Err_Sync"),						// RC_PSU_Err_Sync,
	_T("PSU_Err_SemaphoreLocked"),			// RC_PSU_Err_SemaphoreLocked,
	_T("PSU_Err_Ack"),						// RC_PSU_Err_Ack,
	_T("PSU_Err_AckTimeout"),				// RC_PSU_Err_AckTimeout,
	_T("PSU_Err_AckErrRecieved"),			// RC_PSU_Err_AckErrRecieved,
	_T("PSU_Err_Transfer"),					// RC_PSU_Err_Transfer,
	_T("PSU_Err_Parameter"),				// RC_PSU_Err_Parameter,
	_T("PSU_Err_Protocol"),					// RC_PSU_Err_Protocol,
	_T("PSU_Err_Protocol_CheckSum"),		// RC_PSU_Err_Protocol_CheckSum,
	_T("PSU_Err_Short_Fail"),				// RC_PSU_Err_Short_Fail,

	_T("RC_MES_Err_Unknown"),				// RC_MES_Err_Unknown,
	_T("RC_MES_Err_NotOpen"),				// RC_MES_Err_NotOpen,
	_T("RC_MES_Err_Offline"),				// RC_MES_Err_Offline,
	_T("RC_MES_Err_Semaphore"),				// RC_MES_Err_Semaphore,
	_T("RC_MES_Err_Transfer"),				// RC_MES_Err_Transfer,
	_T("RC_MES_Err_Parameter"),				// RC_MES_Err_Parameter,
	_T("RC_MES_Err_Ack"),					// RC_MES_Err_Ack,
	_T("RC_MES_Err_Ack_Timeout"),			// RC_MES_Err_Ack_Timeout,
	_T("RC_MES_Err_Ack_RecvFail"),			// RC_MES_Err_Ack_RecvFail,
	_T("RC_MES_Err_Ack_Protocol"),			// RC_MES_Err_Ack_Protocol,
	_T("RC_MES_Err_Protocol"),				// RC_MES_Err_Protocol,
	_T("RC_MES_Err_Protocol_CheckSum"),		// RC_MES_Err_Protocol_CheckSum,
	_T("RC_MES_Err_ACK_NotAccepted"),		// RC_MES_Err_ACK_NotAccepted,
	_T("RC_MES_Err_Wrong_EQP_Info"),		// RC_MES_Err_Wrong_EQP_Info,
	_T("RC_MES_Err_BarcodeValidation"),		// RC_MES_Err_BarcodeValidation,
	_T("RC_MES_Err_SN_Verif,"),				// RC_MES_Err_SN_Verify,
	_T("RC_MES_Err_PartNo_Verify"),			// RC_MES_Err_PartNo_Verify,
	_T("RC_MES_Err_HW_Ver_Verify"),			// RC_MES_Err_HW_Ver_Verify,
	_T("RC_MES_Err_SW_Ver_Verify"),			// RC_MES_Err_SW_Ver_Verify,
	_T("MES_Err_19"),						// RC_MES_Err_19,
	
	_T("DIO_Err_MainPower"),				// RC_DIO_Err_MainPower,
	_T("DIO_Err_EMO"),						// RC_DIO_Err_EMO,
	_T("DIO_Err_AreaSensor"),				// RC_DIO_Err_AreaSensor,
	_T("DIO_Err_DoorSensor"),				// RC_DIO_Err_DoorSensor,
	_T("DIO_Err_JIGCorverCheck"),			// RC_DIO_Err_JIGCorverCheck,
	_T("DIO_Err_06"),						// RC_DIO_Err_06,
	_T("DIO_Err_07"),						// RC_DIO_Err_07,
	_T("DIO_Err_08"),						// RC_DIO_Err_08,
	_T("DIO_Err_09"),						// RC_DIO_Err_09,
	_T("DIO_Err_10"),						// RC_DIO_Err_10,
	_T("DIO_Err_11"),						// RC_DIO_Err_11,
	_T("DIO_Err_12"),						// RC_DIO_Err_12,
	_T("DIO_Err_13"),						// RC_DIO_Err_13,
	_T("DIO_Err_14"),						// RC_DIO_Err_14,
	_T("DIO_Err_15"),						// RC_DIO_Err_15,
	_T("DIO_Err_16"),						// RC_DIO_Err_16,
	_T("DIO_Err_17"),						// RC_DIO_Err_17,
	_T("DIO_Err_18"),						// RC_DIO_Err_18,
	_T("DIO_Err_19"),						// RC_DIO_Err_19,
	_T("DIO_Err_20"),						// RC_DIO_Err_20,
	
	_T("Motion_Err_BoardOpen"),				// RC_Motion_Err_BoardOpen,
	_T("Motion_Err_PalletFix"),				// RC_Motion_Err_PalletFix,
	_T("Motion_Err_PalletUnFix"),			// RC_Motion_Err_PalletUnFix,
	_T("Motion_Err_MoveAxis"),				// RC_Motion_Err_MoveAxis,
	_T("Motion_Err_Left_StageX"),			// RC_Motion_Err_Left_StageX,
	_T("Motion_Err_Left_StageY"),			// RC_Motion_Err_Left_StageY,
	_T("Motion_Err_Left_StageZ"),			// RC_Motion_Err_Left_StageZ,
	_T("Motion_Err_Left_StageR"),			// RC_Motion_Err_Left_StageR,
	_T("Motion_Err_Right_StageX"),			// RC_Motion_Err_Right_StageX,
	_T("Motion_Err_Right_StageY"),			// RC_Motion_Err_Right_StageY,
	_T("Motion_Err_Right_StageZ"),			// RC_Motion_Err_Right_StageZ,
	_T("Motion_Err_Right_StageR"),			// RC_Motion_Err_Right_StageR,
	_T("Motion_Err_Alarm"),					// RC_Motion_Err_Alarm,
	_T("Motion_Err_Amp"),					// RC_Motion_Err_Amp,
	_T("Motion_Err_Alarm_Clear"),			// RC_Motion_Err_Alarm_Clear,
	_T("Motion_Err_OriginTimeOut"),			// RC_Motion_Err_OriginTimeOut,
	_T("Motion_Err_OriginUserStop"),		// RC_Motion_Err_OriginUserStop,
	_T("Motion_Err_OriginStop"),			// RC_Motion_Err_OriginStop,
	_T("Motion_Err_Left_StageXY"),			// RC_Motion_Err_Left_StageXY,
	_T("Motion_Err_Right_StageXY"),			// RC_Motion_Err_Right_StageXY,
	_T("Motion_Err_MotorCrash"),			// RC_Motion_Err_MotorCrash,
	_T("Motion_Err_Chart"),					// RC_Motion_Err_Chart,
	_T("Motion_Err_DummyIn"),				// RC_Motion_Err_DummyIn,
	_T("Motion_Err_DummyOut"),				// RC_Motion_Err_DummyOut,
	_T("Motion_Err_Dummy"),					// RC_Motion_Err_Dummy,
	_T("Motion_Err_Cylinger_Check"),		// RC_Motion_Err_Cylinger_Check,
	_T("Motion_Err_Moudle_Check"),			// RC_Motion_Err_Moudle_Check,
	_T("Motion_Err_Port_TimeOut"),			// RC_Motion_Err_Port_TimeOut,
	_T("Motion_Err_Position"),				// RC_Motion_Err_Position,
	_T("Motion_Err_EmptyTeach"),			// RC_Motion_Err_EmptyTeach,

	_T("RC_LIB_2DCAL_SetParam"),			// RC_LIB_2DCAL_SetParam,
	_T("RC_LIB_2DCAL_CornerExt"),			// RC_LIB_2DCAL_CornerExt,
	_T("RC_LIB_2DCAL_ExecIntrinsic"),		// RC_LIB_2DCAL_ExecIntrinsic,
	_T("RC_LIB_SteCAL_SetParam"),			// RC_LIB_SteCAL_SetParam,
	_T("RC_LIB_SteCAL_CornerExt"),			// RC_LIB_SteCAL_CornerExt,
	_T("RC_LIB_SteCAL_ExecIntrinsic"),		// RC_LIB_SteCAL_ExecIntrinsic,

	_T("TEST_Err_FiducialMark"),			// RC_TEST_Err_FiducialMark,
	_T("TestFail"),							// RC_Err_TestFail,
	_T("Err_DTC"),							// RC_Err_DTC,
	_T("NoImage"),							// RC_NoImage,
	_T("No Detect"),						// RC_Detect,

	_T("[ERR] Module Locking Check"),		// RC_Rution_FixLocking_Err,
	_T("[ERR] Motion Origi Reset"),			// RC_Motion_Err_Origin_Reset,
	_T("[ERR] Motion Origi EStop"),			// RC_Motion_Err_Origin_EStop,
	_T("[ERR] Motion Origi Start"),			// RC_Motion_Err_Origin_Start,
	
	_T("[ERR] Motor EStop"),				// RC_Motor_Err_EStop,
	_T("[ERR] Motor Alarm"),				// RC_Motor_Err_Alarm,
	_T("[ERR] Motor Reset"),				// RC_Motor_Err_Reset,
	_T("[ERR] Motor Time"),					// RC_Motor_Err_Time,
	_T("[ERR] Motor Origin"),				// RC_Motor_Err_Origin,
	_T("[ERR] Motor MultiMove"),			// RC_Motor_Err_MultiMove,
	_T("[ERR] ReleaseTorque"),				// RC_Motor_Err_ReleaseTorque,
	_T("[ERR] LockingTorque"),				// RC_Motor_Err_LockingTorque,


	NULL
};

enum enResultCode_EEPROM
{
	RCRom_Err_Undefined = 0,
	RCRom_OK = 1,
	RCRom_Err_Erase,
	RCRom_Err_Read,
	RCRom_Err_Write,
	RCRom_Err_Verify,
	RCRom_Err_Reboot,
	RCRom_Err_Checksum,
};

static LPCTSTR g_szResultCode_EEPROM[] =
{
	_T("Err Undefined"),	// RCRom_Err_Undefined = 0,
	_T("OK"),				// RCRom_OK
	_T("Err Erase"),		// RCRom_Err_Erase
	_T("Err Read"),			// RCRom_Err_Read
	_T("Err Write"),		// RCRom_Err_Write
	_T("Err Verify"),		// RCRom_Err_Verify
	_T("Err Reboot"),		// RCRom_Err_Reboot
	_T("Err Checksum"),		// RCRom_Err_Checksum
};

enum enResultCode_SerialComm
{
	RCSC_OK = -1,
	RCSC_Err_Misc = 0,
	RCSC_Err_PortOpen,
	RCSC_Err_Sync,
	RCSC_Err_SemaphoreLocked,
	RCSC_Err_Ack,
	RCSC_Err_AckTimeout,
	RCSC_Err_AckErrRecieved,
	RCSC_Err_Transfer,
	RCSC_Err_Parameter,
	RCSC_Err_Protocol,
	RCSC_Err_Protocol_CheckSum,
};

enum enResultCode_EthernetComm
{
	RCEC_OK = -1,
	RCEC_Err_Misc = 0,
	RCEC_Err_PortOpen,
	RCEC_Err_Sync,
	RCEC_Err_SemaphoreLocked,
	RCEC_Err_Ack,
	RCEC_Err_AckTimeout,
	RCEC_Err_AckErrRecieved,
	RCEC_Err_Transfer,
	RCEC_Err_Parameter,
	RCEC_Err_Protocol,
	RCEC_Err_Protocol_CheckSum,
};

// MES 결과 코드
typedef enum enesultCode_GEMS
{
	RCMES_OK			= -1,
	RCMES_Err_Unknown	= 0,
	RCMES_Err_NotOpen,
	RCMES_Err_Semaphore,
	RCMES_Err_Transfer,
	RCMES_Err_Parameter,
	RCMES_Err_Ack,
	RCMES_Err_Ack_Timeout,
	RCMES_Err_Ack_RecvFail,
	RCMES_Err_Ack_Protocol,
	RCMES_Err_Protocol,
	RCMES_Err_Protocol_CheckSum,
	RCMES_Err_ACK_NotAccepted,	// 수신된 ACK 항목의 값이 Not Accepted 이다.
	RCMES_Err_Wrong_EQP_Info,	// MES로부터 수신된 설비 정보가 설비에 설정된 정보와 다름
};

static LPCTSTR g_szResultCode_LG_CalIntr[] =
{
	_T("OK"),					// eLGINTRMSG_NOERR,
	_T("ERR"),					// eLGINTRMSG_ERR,
	_T("FileName"),				// eLGINTRMSG_ERR_FILENAME,
	_T("Date"),					// eLGINTRMSG_ERR_DATE,
	_T("ImageSize"),			// eLGINTRMSG_ERR_IMAGESIZE,
	_T("ChartSpec"),			// eLGINTRMSG_ERR_CHARTSPEC,
	_T("Thres"),				// eLGINTRMSG_ERR_THRES,
	_T("SearchRange"),			// eLGINTRMSG_ERR_SEARCHRANGE,
	_T("Version"),				// eLGINTRMSG_ERR_VERSION,
	_T("CornerPoints"),			// eLGINTRMSG_ERR_CORNERPOINTS, /*
	_T("EvalDistance"),			// eLGINTRMSG_ERR_EVALDISTANCE, /*
	_T("NeedMoreImages"),		// eLGINTRMSG_ERR_NEEDMOREIMAGES,
	_T("NeedMorePoints"),		// eLGINTRMSG_ERR_NEEDMOREPOINTS,
	_T("NotExistFile"),			// eLGINTRMSG_ERR_NOTEXISTFILE,
	_T("InvalidFile"),			// eLGINTRMSG_ERR_INVALIDFILE,
	_T("FileWriteFailure"),		// eLGINTRMSG_ERR_FILEWRITEFAILURE
	_T("Size"),					// eLGINTRMSG_SIZE
};

//=============================================================================
// Method		: Conv_SerialResultToResult
// Access		: public static  
// Returns		: LRESULT
// Parameter	: __in enSerialResultCode nSRC
// Qualifier	:
// Last Update	: 2017/10/22 - 15:59
// Desc.		: 시리얼 통신 Result Code를 검사기 Result Code로 변환
//=============================================================================
static LRESULT Conv_SerialResultToResult(__in enSerialResultCode nSRC)
{
	LRESULT lReturn = RC_OK;

	switch (nSRC)
	{
	case SRC_Err_Unknown:
		lReturn = RCSC_Err_Misc;
		break;

	case SRC_OK:
		lReturn = RCSC_OK;
		break;

	case SRC_Err_NotOpen:
		lReturn = RCSC_Err_PortOpen;
		break;

	case SRC_Err_Semaphore:
		lReturn = RCSC_Err_SemaphoreLocked;
		break;

	case SRC_Err_Transfer:
		lReturn = RCSC_Err_Transfer;
		break;

	case SRC_Err_Parameter:
		lReturn = RCSC_Err_Parameter;
		break;

	case SRC_Err_Ack:
	case SRC_Err_Ack_Protocol:
		lReturn = RCSC_Err_Ack;
		break;

	case SRC_Err_Ack_Timeout:
		lReturn = RCSC_Err_AckTimeout;
		break;

	case SRC_Err_Ack_RecvFail:
		lReturn = RCSC_Err_AckErrRecieved;
		break;

	case SRC_Err_Protocol:
		lReturn = RCSC_Err_Protocol;
		break;

	case SRC_Err_Protocol_CheckSum:
		lReturn = RCSC_Err_Protocol_CheckSum;
		break;

	default:
		lReturn = RCSC_Err_Misc;
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: Conv_EthernetResultToResult
// Access		: public static  
// Returns		: LRESULT
// Parameter	: __in enEthernetResultCode nERC
// Qualifier	:
// Last Update	: 2017/11/1 - 20:34
// Desc.		: 이더넷 통신 Result Code를 검사기 Result Code로 변환
//=============================================================================
static LRESULT Conv_EthernetResultToResult(__in enEthernetResultCode nERC)
{
	LRESULT lReturn = RC_OK;

	switch (nERC)
	{
	case ERC_Err_Unknown:
		lReturn = RCEC_Err_Misc;
		break;

	case ERC_OK:
		lReturn = RCEC_OK;
		break;

	case ERC_Err_NotOpen:
		lReturn = RCEC_Err_PortOpen;
		break;

	case ERC_Err_Semaphore:
		lReturn = RCEC_Err_SemaphoreLocked;
		break;

	case ERC_Err_Transfer:
		lReturn = RCEC_Err_Transfer;
		break;

	case ERC_Err_Parameter:
		lReturn = RCEC_Err_Parameter;
		break;

	case ERC_Err_Ack:
	case ERC_Err_Ack_Protocol:
		lReturn = RCEC_Err_Ack;
		break;

	case ERC_Err_Ack_Timeout:
		lReturn = RCEC_Err_AckTimeout;
		break;

	case ERC_Err_Ack_RecvFail:
		lReturn = RCEC_Err_AckErrRecieved;
		break;

	case ERC_Err_Protocol:
		lReturn = RCEC_Err_Protocol;
		break;

	case ERC_Err_Protocol_CheckSum:
		lReturn = RCEC_Err_Protocol_CheckSum;
		break;

	default:
		lReturn = RCEC_Err_Misc;
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: Conv_ResultToEEPROMResult
// Access		: public static  
// Returns		: enResultCode_EEPROM
// Parameter	: __in enResultCode nResult
// Qualifier	:
// Last Update	: 2018/3/17 - 15:46
// Desc.		:
//=============================================================================
static enResultCode_EEPROM Conv_ResultToEEPROMResult(__in enResultCode nResult)
{
	enResultCode_EEPROM nResultCode = enResultCode_EEPROM::RCRom_OK;

	switch (nResult)
	{
	case RC_OK:
		nResultCode = enResultCode_EEPROM::RCRom_OK;
		break;

	case RC_EEPROM_Err_Read:
		nResultCode = enResultCode_EEPROM::RCRom_Err_Read;
		break;

	case RC_EEPROM_Err_Write:
		nResultCode = enResultCode_EEPROM::RCRom_Err_Write;
		break;

	case RC_EEPROM_Err_Reboot:
		nResultCode = enResultCode_EEPROM::RCRom_Err_Reboot;
		break;

	case RC_EEPROM_Err_Verify:
		nResultCode = enResultCode_EEPROM::RCRom_Err_Verify;
		break;

	case RC_EEPROM_Err_Checksum:
		nResultCode = enResultCode_EEPROM::RCRom_Err_Checksum;
		break;

	default:
		nResultCode = enResultCode_EEPROM::RCRom_Err_Undefined;
		break;
	}

	return nResultCode;
}

#endif // Def_ErrorCode_h__
