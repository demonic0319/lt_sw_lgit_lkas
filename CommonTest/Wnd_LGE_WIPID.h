//*****************************************************************************
// Filename	: 	Wnd_LGE_WIPID.h
// Created	:	2017/9/21 - 17:39
// Modified	:	2017/9/21 - 17:39
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_LGE_WIPID_h__
#define Wnd_LGE_WIPID_h__

#pragma once

#include "VGStatic.h"
#include "Def_Enum_Cm.h"

//-----------------------------------------------------------------------------
// CWnd_LGE_WIPID
//-----------------------------------------------------------------------------
class CWnd_LGE_WIPID : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LGE_WIPID)

public:
	CWnd_LGE_WIPID();
	virtual ~CWnd_LGE_WIPID();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);

	enum enInspItem
	{
		II_WIP_ID,			// WIP ID
		II_SerialNumbers,	// Serial Numbers
		II_Pallet_ID,		// Pallet ID		
		II_PartNo	= II_Pallet_ID, 
		II_HW_Version,
		II_SW_Version,
		II_MaxEnum,
		II_Wip_MaxEnum = 3,
	};

	CVGStatic		m_st_Caption[II_MaxEnum];
	CVGStatic		m_st_Value[II_MaxEnum];

	// 외곽선 두께
	UINT			m_nBorderTickness		= 2;

	enInsptrSysType m_InspectionType		= enInsptrSysType::Sys_Focusing;

public:

	// 검사기 종류 설정
	void		SetSystemType		(__in enInsptrSysType nSysType);

	void		ResetItem			();

	// WIP ID
	void		Set_Barcode			(__in LPCTSTR szBarcode);

	void		Set_SerialNumbers	(__in LPCTSTR szSerialNumbers);

	void		Set_PalletID		(__in LPCTSTR szPalletID);

	void		Set_PartNo			(__in LPCTSTR szPartNo);

	void		Set_HWVersion		(__in LPCTSTR szHWVerison);

	void		Set_SWVersion		(__in LPCTSTR szSWVerison);

	void		SetPart_Info		(__in LPCTSTR szPartNo, __in LPCTSTR szHWVer, __in LPCTSTR szSWVer);

};

#endif // Wnd_LGE_WIPID_h__


