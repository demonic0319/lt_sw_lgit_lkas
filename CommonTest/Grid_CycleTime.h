﻿//*****************************************************************************
// Filename	: 	Grid_CycleTime.h
// Created	:	2016/11/14 - 17:59
// Modified	:	2016/11/14 - 17:59
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Grid_CycleTime_h__
#define Grid_CycleTime_h__

#pragma once

#include "Grid_Base.h"
#include "Def_DataStruct_Cm.h"

//-----------------------------------------------------------------------------
// CGrid_CycleTime
//-----------------------------------------------------------------------------
class CGrid_CycleTime : public CGrid_Base
{
public:
	CGrid_CycleTime();
	virtual ~CGrid_CycleTime();

protected:
	virtual void	OnSetup			();
	virtual int		OnHint			(int col, long row, int section, CString *string);
	virtual void	OnGetCell		(int col, long row, CUGCell *cell);
	virtual void	OnDrawFocusRect	(CDC *dc, RECT *rect);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline	();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline	();

	// 헤더를 초기화
	void			InitHeader		();

	CFont		m_font_Header;
	CFont		m_font_Data;

	// 사용하는 소켓 수
	UINT		m_nSocketCount		= 2;

public:

	// 사용하는 소켓 수 설정
	void		SetUseSocketCount		(__in UINT nCount);

	// Cycle 타임 설정
	void		SetCycleTime			(__in const ST_CycleTime* pstCycleTime);

};

#endif // Grid_CycleTime_h__

