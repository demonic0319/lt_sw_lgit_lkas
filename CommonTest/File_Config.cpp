﻿//*****************************************************************************
// Filename	: 	File_Config.cpp
// Created	:	2016/12/14 - 17:51
// Modified	:	2016/12/14 - 17:51
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "File_Config.h"

#define		CONFIG_FILE		_T("config.ini")

CFile_Config::CFile_Config()
{
}


CFile_Config::~CFile_Config()
{
}

//=============================================================================
// Method		: Load_Recipe
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out CString & szRecipe
// Qualifier	:
// Last Update	: 2016/12/14 - 18:18
// Desc.		:
//=============================================================================
BOOL CFile_Config::Load_Recipe(__in LPCTSTR szPath, __out CString& szRecipe)
{
	BOOL bReturn(TRUE);
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	CString szFullPath;
	TCHAR   inBuff[1024] = { 0, };
	
	szFullPath.Format(_T("%s\\%s"), szPath, CONFIG_FILE);
	
	// 모델명
	GetPrivateProfileString(_T("System"), _T("ModelName"), _T("Default"), inBuff, 80, szFullPath);
	szRecipe = inBuff;

	return TRUE;
}

//=============================================================================
// Method		: Save_Recipe
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szRecipe
// Qualifier	:
// Last Update	: 2016/12/14 - 18:18
// Desc.		:
//=============================================================================
BOOL CFile_Config::Save_Recipe(__in LPCTSTR szPath, __in LPCTSTR szRecipe)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == szRecipe)
		return FALSE;

	::DeleteFile(szPath);

	BOOL bReturn = TRUE;

	CString szFullPath;
	CString strValue;

	szFullPath.Format(_T("%s\\%s"), szPath, CONFIG_FILE);

	// 모델명
	strValue = szRecipe;
	return WritePrivateProfileString(_T("System"), _T("ModelName"), strValue, szFullPath);
}

//=============================================================================
// Method		: Load_RecipeFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out CString & szRecipeFile
// Qualifier	:
// Last Update	: 2016/12/14 - 19:36
// Desc.		:
//=============================================================================
BOOL CFile_Config::Load_RecipeFile(__in LPCTSTR szPath, __out CString& szRecipeFile)
{
	BOOL bReturn(TRUE);
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	CString szFullPath;
	TCHAR   inBuff[1024] = { 0, };

	szFullPath.Format(_T("%s\\%s"), szPath, CONFIG_FILE);

	// 모델명
	GetPrivateProfileString(_T("System"), _T("ModelFile"), _T("Default"), inBuff, 80, szFullPath);
	szRecipeFile = inBuff;

	return TRUE;
}

//=============================================================================
// Method		: Save_RecipeFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szRecipeFile
// Qualifier	:
// Last Update	: 2016/12/14 - 19:36
// Desc.		:
//=============================================================================
BOOL CFile_Config::Save_RecipeFile(__in LPCTSTR szPath, __in LPCTSTR szRecipeFile)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == szRecipeFile)
		return FALSE;

	::DeleteFile(szPath);

	BOOL bReturn = TRUE;

	CString szFullPath;
	CString strValue;

	szFullPath.Format(_T("%s\\%s"), szPath, CONFIG_FILE);

	// 모델명
	strValue = szRecipeFile;
	return WritePrivateProfileString(_T("System"), _T("ModelFile"), strValue, szFullPath);
}


//=============================================================================
// Method		: Load_CAN_Timeout
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LGE_CAN_Timeout & stTimeout
// Qualifier	:
// Last Update	: 2017/12/16 - 15:01
// Desc.		:
//=============================================================================
// BOOL CFile_Config::Load_CAN_Timeout(__in LPCTSTR szPath, __out ST_LGE_CAN_Timeout& stTimeout)
// {
// 	BOOL bReturn = TRUE;
// 
// 	if (NULL == szPath)
// 		return FALSE;
// 
// 	// 파일이 존재하는가?
// 	if (!PathFileExists(szPath))
// 	{
// 		return FALSE;
// 	}
// 
// 	CString szFullPath;
// 	TCHAR   inBuff[1024] = { 0, };
// 
// 	szFullPath.Format(_T("%s\\%s"), szPath, CONFIG_FILE);
// 
// 	for (UINT nIdx = 0; nIdx < CanTO_MaxEnum; nIdx++)
// 	{
// 		GetPrivateProfileString(_T("CAN_Timeout"), g_szLGE_CAN_Timeout[nIdx], _T("2000"), inBuff, 80, szFullPath);
// 		stTimeout.dwTimes[nIdx] = _ttol(inBuff);
// 	}
// 
// 	return bReturn;
// }

//=============================================================================
// Method		: Save_CAN_Timeout
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in ST_LGE_CAN_Timeout * pstTimeout
// Qualifier	:
// Last Update	: 2017/12/16 - 15:08
// Desc.		:
//=============================================================================
// BOOL CFile_Config::Save_CAN_Timeout(__in LPCTSTR szPath, __in ST_LGE_CAN_Timeout* pstTimeout)
// {
// 	if (NULL == szPath)
// 		return FALSE;
// 
// 	if (NULL == pstTimeout)
// 		return FALSE;
// 
// 	//::DeleteFile(szPath);
// 
// 	BOOL bReturn = TRUE;
// 
// 	CString szFullPath;
// 	CString strValue;
// 
// 	szFullPath.Format(_T("%s\\%s"), szPath, CONFIG_FILE);
// 
// 	// 모델명
// 	for (UINT nIdx = 0; nIdx < CanTO_MaxEnum; nIdx++)
// 	{
// 		strValue.Format(_T("%d"), pstTimeout->dwTimes[nIdx]);
// 		WritePrivateProfileString(_T("CAN_Timeout"), g_szLGE_CAN_Timeout[nIdx], strValue, szFullPath);
// 	}
// 
// 	return bReturn;
// }
