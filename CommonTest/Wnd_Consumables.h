//*****************************************************************************
// Filename	: 	Wnd_Consumables.h
// Created	:	2017/9/18 - 9:49
// Modified	:	2017/9/18 - 9:49
//
// Author	:	PiRing
//	
// Purpose	:	소모품 관련 사용현황 표시 윈도우
//*****************************************************************************
#ifndef Wnd_Consumables_h__
#define Wnd_Consumables_h__

#pragma once

#include "VGStatic.h"

//-----------------------------------------------------------------------------
// CWnd_Consumables
//-----------------------------------------------------------------------------
class CWnd_Consumables : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Consumables)

public:
	CWnd_Consumables();
	virtual ~CWnd_Consumables();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate					(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize						(UINT nType, int cx, int cy);
	afx_msg void	OnCmdRangeBnReset			(UINT nID);

	enum enConsumItem
	{
		CI_Y_PogoCount_Left,	// Pogo Count Left
		CI_Y_PogoCount_Right,	// Pogo Count Right
		CI_Y_MaxEnum,
	};

	enum enConsumHeader
	{
		CH_X_ItemName,		// 아이템 명
		CH_X_Spec,			// 한계 설정치
		CH_X_Usage,			// 사용 횟수
		CH_X_Remain,		// 남은 횟수
		CH_X_Reset,
		CH_X_MaxEnum,
	};

	CVGStatic		m_st_Caption[CH_X_MaxEnum];
	CVGStatic		m_st_Cell[CI_Y_MaxEnum][CH_X_MaxEnum];
	CMFCButton		m_bn_Reset[CI_Y_MaxEnum];

	int				m_iTotalWidthRate		= 0;
	int				m_iCtrlWidth[CH_X_MaxEnum];

	UINT			m_nItemCount			= CI_Y_MaxEnum;

	BOOL			m_bUseResetButton		= TRUE;

public:

	void		Set_UseResetButton		(__in BOOL bUse);

	void		ResetAllItemData		();

	void		SetItemData				(__in UINT nItemIndex, __in UINT nSpec, __in UINT nUsage);

	void		SetItemCount			(__in UINT nCount);

};

#endif // Wnd_Consumables_h__

