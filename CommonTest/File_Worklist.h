﻿//*****************************************************************************
// Filename	: 	File_Worklist.h
// Created	:	2017/2/28 - 17:25
// Modified	:	2017/2/28 - 17:25
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef File_Worklist_h__
#define File_Worklist_h__

#pragma once

#include "File_MES.h"

class CFile_Worklist : public CFile_MES
{
public:
	CFile_Worklist();
	virtual ~CFile_Worklist();

	virtual BOOL	Save_FinalResult				(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_FinalResult* pstWorklist);

	virtual BOOL	Save_TestItemLog				(__in LPCTSTR szMESPath, __in SYSTEMTIME* pTime, __in const ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in BOOL bAddSpaceLine = FALSE);

	virtual BOOL	Save_EEPROM_Log					(__in LPCTSTR szPath, __in SYSTEMTIME* pTime, __in ST_MES_TestItemLog* pstWorklist, __in LPCTSTR szTestName, __in DWORD dwStartAddr, __in DWORD dwEndAddr, __in char* pDataz, __in DWORD dwLength);


	// WIP-ID 결과
	//ST_WipID_Info,ST_WipID_Result

	// 2D CAL Cornet Point

	// 2D CAL Result

	// 3D CAL Depth

	// 3D CAL Evaluation

	// 3D CAL 기타

	// 화질검사 결과

	// 이물검사 결과


};

#endif // File_Worklist_h__

