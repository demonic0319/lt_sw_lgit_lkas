﻿//*****************************************************************************
// Filename	: 	Def_MES_Cm.h
// Created	:	2016/9/6 - 14:07
// Modified	:	2016/9/6 - 14:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_MES_Cm_h__
#define Def_MES_Cm_h__

#include <afxwin.h>
#include "Def_Enum_Cm.h"

// C:\BMS_MES\설비코드_차수_YYYYMMDDHHMISS.txt
// LOTID, 차수, 0, A1R101:항목값1(계측치) : 불량종류 : 0, A2R102 : 항목값2(계측치) : 불량종류 : 0, …, N(n) : 항목값n :불량종류 : 0[CrLf]
// LOTID,차수,합부,항목값1:항목1합부,항목값2:항목2합부,…,항목값n:항목n합부[CrLf]


typedef enum enMES_ImgFormat
{
	EVMS_IMG_RAW,
	EVMS_IMG_BMP,
	EVMS_IMG_PNG,
};

static LPCTSTR g_szMES_ImgFormat[] =
{
	_T("raw"),
	_T("bmp"),
	_T("png"),
};

//-----------------------------------------------------------------------------
// Worklist 구조체
//-----------------------------------------------------------------------------
typedef struct _tag_MES_FinalResult
{
	CString		Time;
	CString		EqpID;
	CString		ProcID;
	CString		Model;
	CString		SWVersion;
	CString		Barcode;
	CString		Socket;
	CString		Result;

	CStringArray	Itemz;
	CStringArray	ItemHeaderz;
}ST_MES_FinalResult;

typedef struct _tag_MES_TestItemLog
{
	CString		Time;
	CString		Equipment;
	CString		ProcID;
	CString		Model;
	CString		SWVersion;
	CString		Barcode;
	CString		Socket;
	CString		Result;

	CStringArray	Itemz;
	CStringArray	ItemHeaderz;
}ST_MES_TestItemLog;

//=============================================================================
// 
//=============================================================================
// Focusing  --------------------------------------
typedef enum enResultItem_Focusing
{
	RI_Foc_WIP_ID,				// Write WIP-ID
	RI_Foc_PalletID,			// Read Pallet ID
	RI_Foc_LinxJuncTemp,
	RI_Foc_SensorTemp,
	RI_Foc_OperCurrent,			// Operation Current,
	RI_Foc_SleepCurrent,		// Sleep Current,
	RI_Foc_CAM_Error,
	RI_Foc_CAN_Brd_Error,
	
	//RI_Foc_Write_SN,			// Write Serial Numbers
	RI_Foc_MaxEnum,
};

static LPCTSTR g_szResultItem_Focusing[] =
{
	_T("WIP ID"),
	_T("Pallete ID"),
	_T("sys temp"),
	_T("chip temp"),
	_T("normal current"),
	_T("sleep current"),
	_T("CAM error"),
	_T("CAN error"),
	
	//_T("Write the S/N"),
	NULL
};

// 2D CAL 검사 -------------------------------------
typedef enum enResultItem_2D_CAL
{
	RI_2D_KK_0,					// _T("KK_0")
	RI_2D_KK_1,					// _T("KK_1")
	RI_2D_KK_2,					// _T("KK_2")
	RI_2D_KK_3,					// _T("KK_3")
	RI_2D_Kc_0,					// _T("Kc_0")
	RI_2D_Kc_1,					// _T("Kc_1")
	RI_2D_Kc_2,					// _T("Kc_2")
	RI_2D_Kc_3,					// _T("Kc_3")
	RI_2D_Kc_4,					// _T("Kc_4")
	RI_2D_DistFOV,				// _T("Dist FOV")
	RI_2D_R2Max,				// _T("R2 Max")
	RI_2D_RepError,				// _T("RepError")
	RI_2D_EvalError,			// _T("EvalError")
	RI_2D_OrgOffset,			// _T("OrgOffset")
	RI_2D_DetectionFailureCnt,	// _T("DetectionFailureCnt")
	RI_2D_InvalidPointCnt,		// _T("InvalidPointCnt")
	RI_2D_ValidPointCnt,		// _T("ValidPointCnt")
	RI_2D_Date,					// _T("Date")
	RI_2D_MaxEnum,
};

static LPCTSTR g_szResultItem_2D_CAL[] =
{
	_T("KK_0"),						// RI_2D_KK_0,					
	_T("KK_1"),						// RI_2D_KK_1,					
	_T("KK_2"),						// RI_2D_KK_2,					
	_T("KK_3"),						// RI_2D_KK_3,					
	_T("Kc_0"),						// RI_2D_Kc_0,					
	_T("Kc_1"),						// RI_2D_Kc_1,					
	_T("Kc_2"),						// RI_2D_Kc_2,					
	_T("Kc_3"),						// RI_2D_Kc_3,					
	_T("Kc_4"),						// RI_2D_Kc_4,					
	_T("Dist FOV"),					// RI_2D_DistFOV,				
	_T("R2 Max"),					// RI_2D_R2Max,				
	_T("RepError"),					// RI_2D_RepError,				
	_T("EvalError"),				// RI_2D_EvalError,			
	_T("OrgOffset"),				// RI_2D_OrgOffset,			
	_T("DetectionFailureCnt"),		// RI_2D_DetectionFailureCnt,	
	_T("InvalidPointCnt"),			// RI_2D_InvalidPointCnt,		
	_T("ValidPointCnt"),			// RI_2D_ValidPointCnt,
	_T("Date"),						// RI_2D_Date
	NULL
};

// 화질 검사 -------------------------------------
typedef enum enResultItem_ImgT
{
	RI_ImgT_WIP_ID,				// _T("WIP ID"),
	RI_ImgT_Arm_ID,				// _T("Arm ID"),
	RI_ImgT_CAM_Error,
	RI_ImgT_CAN_Brd_Error,
	//RI_ImgT_Pallet_ID,			// _T("Pallete ID"),
	RI_ImgT_OpticalCenter,		// Optical Center
	RI_ImgT_DynamicRange,			// Dynamic Range
	RI_ImgT_SNR_BW,				// SNR_BW : Zenith polymer
	RI_ImgT_SNR_IQ,				// SNR_IQ : Center Intensity Brightness
	RI_ImgT_FOV,					// FOV
	RI_ImgT_SFR,					// SFR : Mat3
	RI_ImgT_Distortion,			// Distortion
	RI_ImgT_Rotation,				// Rotation
	RI_ImgT_Tilt,					// Tilt
	RI_ImgT_Vcsel,				// Vcsel
	RI_ImgT_MaxEnum,
};

static LPCTSTR g_szResultItem_ImgT[] =
{
	_T("WIP ID"),						// RI_ImgT_WIP_ID,
	_T("Arm ID"),						// RI_ImgT_Arm_ID,
	_T("CAM error"),					// RI_ImgT_CAM_Error
	_T("CAN error"),					// RI_ImgT_CAN_Brd_Error
	//_T("Pallete ID"),					// RI_ImgT_Pallet_ID,
	_T("Optical Center"),				// RI_ImgT_OpticalCenter,
	_T("Dynamic Range"),				// RI_ImgT_DynamicRange,	
	_T("SNR BW"),						// RI_ImgT_SNR_BW,		
	_T("Center Intensity Brightness"),	// RI_ImgT_SNR_IQ,		
	_T("FOV"),							// RI_ImgT_FOV,			
	_T("SFR"),							// RI_ImgT_SFR,			
	_T("Distortion"),					// RI_ImgT_Distortion,	
	_T("Rotate Degree"),				// RI_ImgT_Rotation,		
	_T("Tilt"),							// RI_ImgT_Tilt,			
	_T("VCSEL"),						// RI_ImgT_Vcsel,		
	NULL
};

// 이물 검사 -------------------------------------
typedef enum enResultItem_Ste_CAL
{
	RI_Ste_WIP_ID,				// _T("WIP ID"),
	RI_Ste_Arm_ID,				// _T("Arm ID"),
	RI_Ste_CAM_Error,
	RI_Ste_CAN_Brd_Error,
	RI_Ste_Stain,				// 
	RI_Ste_Ymean,				// 
	RI_Ste_DeadPixel,			// 
	RI_Ste_DefectPixel,			// 
	RI_Ste_Rtllumination,		// 
	RI_Ste_Light_SNR,			// 
	//RI_Ste_FPN,					// 
	RI_Ste_MaxEnum,
};

static LPCTSTR g_szResultItem_SteCal[] =
{
	_T("WIP ID"),					// RI_Ste_WIP_ID,
	_T("Arm ID"),					// RI_Ste_Arm_ID,
	_T("CAM error"),				// RI_Ste_CAM_Error
	_T("CAN error"),				// RI_Ste_CAN_Brd_Error
	//_T("Pallete ID"),				// RI_Ste_Pallet_ID,
	_T("Stain"),					// RI_Ste_Stain,		
	_T("Ymean"),					// RI_Ste_Ymean,		
	_T("Dead Pixel"),				// RI_Ste_DeadPixel,	
	_T("Hot Pixel"),				// RI_Ste_DefectPixel,	
	_T("Relative Illumination"),	// RI_Ste_Rtllumination
	_T("Light SNR"),				// RI_Ste_Light_SNR,	
	//_T("FPN"),						// RI_Ste_FPN,			
	NULL
};

// 3D CAL 검사 -------------------------------------
typedef enum enResultItem_3D_CAL
{
	RI_3D_WIP_ID,					// _T("WIP ID"),
	RI_3D_Arm_ID,					// _T("Arm ID"),
	RI_3D_CAM_Error,
	RI_3D_CAN_Brd_Error,
	RI_3D_Depth_DistCoef,
	RI_3D_Depth_TotalFittingError,
	RI_3D_Depth_AccracyMap,
	RI_3D_Depth_InvalidRatio,
	RI_3D_Depth_FailureCnt,
	RI_3D_Eval_Chess,
	RI_3D_Eval_TofDist,
	RI_3D_Eval_Error,
	RI_3D_Eval_Shading,
	RI_3D_Eval_FailureCnt,
	RI_3D_Eval_IntensityC,
	RI_3D_DummyHand,
	RI_3D_DTC_Check,

	RI_3D_MaxEnum,
};

static LPCTSTR g_szResultItem_3D_CAL[] =
{
	_T("WIP ID"),						// RI_ImgT_WIP_ID,
	_T("Arm ID"),						// RI_ImgT_Arm_ID,
	_T("CAM error"),					// RI_ImgT_CAM_Error
	_T("CAN error"),					// RI_ImgT_CAN_Brd_Error
	//_T("Pallete ID"),					// RI_ImgT_Pallet_ID,
	_T("Depth Dist Coef"),				// RI_3D_Depth_DistCoef5,
	_T("Depth Total Fitting Error"),	// RI_3D_Depth_TotalFittingError,
	_T("Depth Accracy Map"),			// RI_3D_Depth_AccracyMap,
	_T("Depth Invalid Ratio"),			// RI_3D_Depth_InvalidRatio,
	_T("Depth Failure Cnt"),			// RI_3D_Depth_FailureCnt,
	_T("Eval Chess"),					// RI_3D_Eval_Chess,
	_T("Eval Tof Dist"),				// RI_3D_Eval_TofDist,
	_T("Eval Error"),					// RI_3D_Eval_Error,
	_T("Eval Shading"),					// RI_3D_Eval_Shading,
	_T("Eval Failure Cnt"),				// RI_3D_Eval_FailureCnt,
	_T("Eval Intensity C"),				// RI_3D_Eval_IntensityC,
	_T("Dummy Hand Test"),				// RI_3D_DummyHand,
	_T("DTC Check"),					// RI_3D_DTC_Check,

	NULL
};

//=============================================================================
// MES 결과 항목
//=============================================================================


typedef struct _tag_MES_Result_Info
{
	//CArray<ST_MES_ResultItem, ST_MES_ResultItem&> ItemList;


// 	virtual void SetSystemType(__in enInsptrSysType nSysType)
// 	{
// 		LPCTSTR*	ppszResultName = NULL;
// 		UINT		nMaxCount = 0;
// 
// 		switch (nSysType)
// 		{
// 		case Sys_Focusing:
// 			ppszResultName = g_szResultItem_Focusing;
// 			nMaxCount = RI_Foc_MaxEnum;
// 			break;
// 
// 		case Sys_2D_Cal:
// 			ppszResultName = g_szResultItem_2D_CAL;
// 			nMaxCount = RI_2D_MaxEnum;
// 			break;
// 
// 		case Sys_Image_Test:
// 			ppszResultName = g_szResultItem_ImgT;
// 			nMaxCount = RI_ImgT_MaxEnum;
// 			break;
// 
// 		case Sys_Stereo_Cal:
// 			ppszResultName = g_szResultItem_SteCal;
// 			nMaxCount = RI_Ste_MaxEnum;
// 			break;
// 
// 		case Sys_3D_Cal:
// 			ppszResultName = g_szResultItem_3D_CAL;
// 			nMaxCount = RI_3D_MaxEnum;
// 			break;
// 
// 		default:
// 			break;
// 		}
// 
// 		ItemList.RemoveAll();
// 
// 		for (UINT nIdx = 0; nIdx < nMaxCount; nIdx++)
// 		{
// 			ST_MES_ResultItem	stItem;
// 			stItem.bUse				= TRUE;
// 			stItem.nReultItem_Index = nIdx;
// 			stItem.NAME				= ppszResultName[nIdx];
// 
// 			ItemList.Add(stItem);
// 		}
// 	}

	
}ST_MES_Result_Info, *PST_MES_Result_Info;



#endif // Def_MES_Cm_h__
