#ifndef List_2DCal_ROI_h__
#define List_2DCal_ROI_h__

#pragma once

#include "Def_DataStruct_Cm.h"
#include "Def_T_2DCal.h"

typedef enum enListNum_2DCal_ROI
{
	ROI_2D_Object = 0,
	ROI_2D_U,
	ROI_2D_V,
	ROI_2D_W,
	ROI_2D_H,
	ROI_2D_MaxCol,
};

static LPCTSTR	g_lpszHeader_2DCal_ROI[] =
{
	_T("No"),	// ROI_2D_Object
	_T("U"),	// ROI_2D_U,
	_T("V"),	// ROI_2D_V,
	_T("W"),	// ROI_2D_W,
	_T("H"),	// ROI_2D_H,
	NULL		
};

typedef enum enListItemNum_2DCal_ROI
{
	ROI_2D_ItemNum = MAX_2DCAL_ROI,
};

const int	iListAglin_2DCal_ROI[] =
{
	LVCFMT_LEFT,		// ROI_2D_Object
	LVCFMT_CENTER,		// ROI_2D_U,
	LVCFMT_CENTER,		// ROI_2D_V,
	LVCFMT_CENTER,		// ROI_2D_W,
	LVCFMT_CENTER,		// ROI_2D_H,
};

const int	iHeaderWidth_2DCal_ROI[] =
{
	95,		// ROI_2D_Object
	95,		// ROI_2D_U,
	95,		// ROI_2D_V,
	95,		// ROI_2D_W,
	95,		// ROI_2D_H,
};

class CList_2DCal_ROI : public CListCtrl
{
	DECLARE_DYNAMIC(CList_2DCal_ROI)

public:
	CList_2DCal_ROI();
	virtual ~CList_2DCal_ROI();

	void	InsertFullData	(__in ST_2DCal_Para* pst2DCal_Para);
	void	GetCellData		(__out ST_2DCal_Para& stOut2DCal_Para);

protected:

	ST_2DCal_Para	m_st2DCal_Para;

	CFont	m_Font;
	CEdit	m_ed_CellEdit;
	UINT	m_nEditCol;
	UINT	m_nEditRow;

	BOOL	UpdateCellData	(UINT nRow, UINT nCol, int  iValue);

	void	InitHeader		();
	void	SetRectRow		(UINT nRow);

	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);
	afx_msg void	OnNMClick		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void	OnNMDblclk		(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg BOOL	OnMouseWheel	(UINT nFlags, short zDelta, CPoint pt);

	afx_msg void	OnEnKillFocusECpOpellEdit();
};

#endif // List_SFRInfo_h__
