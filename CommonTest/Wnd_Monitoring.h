//*****************************************************************************
// Filename	: 	Wnd_Monitoring.h
// Created	:	2018/3/3 - 9:49
// Modified	:	2018/3/3 - 9:49
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Monitoring_h__
#define Wnd_Monitoring_h__

#pragma once

#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_Enum_Cm.h"

//-----------------------------------------------------------------------------
// CWnd_Monitoring
//-----------------------------------------------------------------------------
class CWnd_Monitoring : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Monitoring)

public:
	CWnd_Monitoring();
	virtual ~CWnd_Monitoring();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);

	CFont			m_Font;
	
	enum enMonitorItem
	{
		MI_Stage,		// 거리
		MI_ChartTX,		// 각도
		MI_ChartTZ,		// 각도
		MI_MaxEnum,
	};

	CVGStatic			m_st_Caption[MI_MaxEnum];
	CVGStatic			m_st_Value[MI_MaxEnum];

	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_Focusing;

public:

	// 검사기 종류 설정
	void	SetSystemType		(__in enInsptrSysType nSysType);

	// 아이템 데이터 초기화
	void	ResetItem			();

	// 검사 아이템 데이터 초기화
	void	ResetTestTime		(__in UINT nParaIdx = 0);

	// Chart
	void	Set_Chart_Tx		(__in CString szChart);

	// Stage
	void	Set_Stage			(__in CString szStage);

	// Chart
	void	Set_Chart_Tz		(__in CString szChart);

};

#endif // Wnd_Monitoring_h__

