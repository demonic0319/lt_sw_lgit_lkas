//*****************************************************************************
// Filename	: 	MES_LGIT.cpp
// Created	:	2018/3/1 - 14:50
// Modified	:	2018/3/1 - 14:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "FileMES_LGIT.h"


CFileMES_LGIT::CFileMES_LGIT()
{
}


CFileMES_LGIT::~CFileMES_LGIT()
{
}

//=============================================================================
// Method		: Make_Dataz
// Access		: virtual protected  
// Returns		: CString
// Qualifier	:
// Last Update	: 2018/3/1 - 16:47
// Desc.		:
//=============================================================================
CString CFileMES_LGIT::Make_Dataz()
{
	return CFileMES_Base::Make_Dataz();
}

//=============================================================================
// Method		: Make_Header
// Access		: virtual protected  
// Returns		: CString
// Qualifier	:
// Last Update	: 2018/3/1 - 16:47
// Desc.		:
//=============================================================================
CString CFileMES_LGIT::Make_Header()
{
	return CFileMES_Base::Make_Header();
}

//=============================================================================
// Method		: Get_MESFileData
// Access		: virtual protected  
// Returns		: CString
// Qualifier	:
// Last Update	: 2018/3/1 - 16:47
// Desc.		:
//=============================================================================
CString CFileMES_LGIT::Get_MESFileData()
{
	return CFileMES_Base::Get_MESFileData();
}

//=============================================================================
// Method		: Get_MESFileData
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __out LPBYTE lpbyOutDATA
// Parameter	: __out DWORD & dwOutDataSize
// Qualifier	:
// Last Update	: 2018/3/1 - 16:47
// Desc.		:
//=============================================================================
BOOL CFileMES_LGIT::Get_MESFileData(__out LPBYTE lpbyOutDATA, __out DWORD& dwOutDataSize)
{
	return CFileMES_Base::Get_MESFileData(lpbyOutDATA, dwOutDataSize);
}

//=============================================================================
// Method		: SaveMESFile_Unicode
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/1 - 17:05
// Desc.		:
//=============================================================================
BOOL CFileMES_LGIT::SaveMESFile_Unicode()
{
	return CFileMES_Base::SaveMESFile_Unicode();
}

//=============================================================================
// Method		: SaveMESFile_ANSI
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/1 - 17:05
// Desc.		:
//=============================================================================
BOOL CFileMES_LGIT::SaveMESFile_ANSI()
{
	return CFileMES_Base::SaveMESFile_ANSI();
}

//=============================================================================
// Method		: Remake_TestItem_Judge
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/1 - 18:52
// Desc.		: 
//=============================================================================
void CFileMES_LGIT::Remake_TestItem_Judge()
{
	BOOL bPassed = TRUE;
	for (INT_PTR nIdx = 0; nIdx < ItemList.GetCount(); nIdx++)
	{
		if (FALSE == bPassed)
		{
			ItemList[nIdx].JUDGE = _T("1");
		}
		else
		{
			if (0 == ItemList[nIdx].JUDGE.Compare(_T("0")))
			{
				bPassed = FALSE;
			}
		}
	}
}

//=============================================================================
// Method		: Reset_ItemData
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/1 - 17:06
// Desc.		:
//=============================================================================
void CFileMES_LGIT::Reset_ItemData()
{
	CFileMES_Base::Reset_ItemData();
}

//=============================================================================
// Method		: Add_ItemData
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szName
// Parameter	: __in LPCTSTR szValue
// Parameter	: __in BOOL bPass
// Qualifier	:
// Last Update	: 2018/3/1 - 16:47
// Desc.		:
//=============================================================================
BOOL CFileMES_LGIT::Add_ItemData(__in LPCTSTR szName, __in LPCTSTR szValue, __in BOOL bPass /*= TRUE*/)
{
	return CFileMES_Base::Add_ItemData(szName, szValue, bPass);
}

//=============================================================================
// Method		: SaveMESFile
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInBarcode
// Parameter	: __in UINT nInRetryCnt
// Parameter	: __in BOOL bInJudgment
// Parameter	: __in const SYSTEMTIME * pstTime
// Qualifier	:
// Last Update	: 2018/3/1 - 16:47
// Desc.		:
//=============================================================================
BOOL CFileMES_LGIT::SaveMESFile(__in LPCTSTR szInBarcode, __in UINT nInRetryCnt, __in BOOL bInJudgment, __in const SYSTEMTIME* pstTime)
{
	//return CFileMES_Base::SaveMESFile(szInBarcode, nInRetryCnt, bInJudgment, pstTime);

	Set_Barcode(szInBarcode, nInRetryCnt, bInJudgment, pstTime);

	// 필요한 경우 사용 -> 여러개의 검사 항목 판정 처리 할때 Fail은 1개만 처리
	Remake_TestItem_Judge();

	return SaveMESFile_ANSI();
	//return SaveMESFile_Unicode();
}
