//*****************************************************************************
// Filename	: 	Wnd_Cfg_3DCal_Spec.h
// Created	:	2017/11/10 - 16:04
// Modified	:	2017/11/10 - 16:04
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_3DCal_Spec_h__
#define Wnd_Cfg_3DCal_Spec_h__

#pragma once

#include "Def_T_3DCal.h"
#include "VGStatic.h"
#include "Def_DataStruct_Cm.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_3DCal_Spec
//-----------------------------------------------------------------------------
class CWnd_Cfg_3DCal_Spec : public CWnd
{
	DECLARE_DYNAMIC(CWnd_Cfg_3DCal_Spec)

public:
	CWnd_Cfg_3DCal_Spec();
	virtual ~CWnd_Cfg_3DCal_Spec();

protected:
	afx_msg int			OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void		OnSize				(UINT nType, int cx, int cy);
	afx_msg void		OnShowWindow		(BOOL bShow, UINT nStatus);
	virtual BOOL		PreCreateWindow		(CREATESTRUCT& cs);
	afx_msg void		OnBnClickedChkSpecMin(UINT nID);
	afx_msg void		OnBnClickedChkSpecMax(UINT nID);

	DECLARE_MESSAGE_MAP()

	CFont				m_font;

	CVGStatic			m_st_CapItem;
	CVGStatic			m_st_CapSpecMin;
	CVGStatic			m_st_CapSpecMax;

	CVGStatic			m_st_Item[Spec_3DCal_MaxNum];
	CMFCMaskedEdit		m_ed_SpecMin[Spec_3DCal_MaxNum];
	CMFCMaskedEdit		m_ed_SpecMax[Spec_3DCal_MaxNum];
	CMFCButton			m_chk_SpecMin[Spec_3DCal_MaxNum];
	CMFCButton			m_chk_SpecMax[Spec_3DCal_MaxNum];


public:

	void			Set_TestItemInfo		(__in ST_TestItemInfo* pstTestItemInfo);
	void			Get_TestItemInfo		(__out ST_TestItemInfo& stOutTestItemInfo);

	void			Set_3DCal_Spec			(__in ST_3DCal_Depth_Spec* pst3DCal_SpecMin, __in ST_3DCal_Depth_Spec* pst3DCal_SpecMax);
	void			Get_3DCal_Spec			(__out ST_3DCal_Depth_Spec& stOut3DCal_SpecMin, __out ST_3DCal_Depth_Spec& stOut3DCal_SpecMax);

};

#endif // Wnd_Cfg_3DCal_Spec_h__


