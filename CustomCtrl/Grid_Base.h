﻿//*****************************************************************************
// Filename	: Grid_Base.h
// Created	: 2010/11/25
// Modified	: 2010/11/25 - 17:28
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Grid_Base_h__
#define Grid_Base_h__

#pragma once

#include "UGCtrl.h"

//=============================================================================
// CGrid_Base
//=============================================================================
class CGrid_Base : public CUGCtrl
{
public:
	CGrid_Base(void);
	virtual ~CGrid_Base(void);

protected:

	DECLARE_MESSAGE_MAP()

	afx_msg void	OnSize					(UINT nType, int cx, int cy);

	virtual void	OnSetup					();
	virtual void	OnGetCell				(int col,long row,CUGCell *cell);

	// 그리드 외형 및 내부 문자열을 채운다.
	virtual void	DrawGridOutline			();

	// 셀 갯수 가변에 따른 다시 그리기 위한 함수
	virtual void	CalGridOutline			();

	CPen			m_pen;	
	CFont			m_font;

	UINT			m_nMaxRows;				// 최대 Row 수
	UINT			m_nMaxCols;				// 최대 Column 수

	BOOL			m_bUseLeftHeader;
	BOOL			m_bUseTopHeader;

	// 콤보박스에 표시될 문자열 만드는 함수
	CString			GetDropListString		(__in const TCHAR** lpszString);

public:

	void			SetRowColCount			(UINT nRows, UINT nCols)
	{
		m_nMaxRows = nRows;
		m_nMaxCols = nCols;
	};
};

#endif // Grid_Base_h__
