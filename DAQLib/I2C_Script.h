﻿//*****************************************************************************
// Filename	: 	I2C_Script.h
// Created	:	2017/9/4 - 11:44
// Modified	:	2017/9/4 - 11:44
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef I2C_Script_h__
#define I2C_Script_h__

#pragma once

#include <afxwin.h>

// bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData
typedef enum enI2C_Command
{
	I2CCmd_SlaveID,
	I2CCmd_Sleep,
	I2CCmd_Write,
	I2CCmd_CommandMax,
};

static LPCTSTR g_szI2C_Command[] =
{
	_T("slave"),
	_T("sleep"),
	_T("0x"),
	NULL
};

#define MAX_I2C_WRITE_BYTES		1024	// 최대 1024 Byte 데이터를 한번에 전송 가능

typedef struct _tag_I2CFormat
{
	unsigned int	nCommand;	// Write, Sleep, SlaveID
	unsigned long	dwAddrLen;
	unsigned long	dwAddress;
	unsigned long	dwMilliseconds;
	unsigned long	dwDataCount;
	BYTE	byDataz[MAX_I2C_WRITE_BYTES];
	BYTE	byReadDataz[MAX_I2C_WRITE_BYTES];

	_tag_I2CFormat()
	{
		nCommand		= 0;
		dwAddrLen		= 0;
		dwAddress		= 0;
		dwMilliseconds	= 0;
		dwDataCount		= 0;
		memset(byDataz, 0, MAX_I2C_WRITE_BYTES);
		memset(byReadDataz, 0, MAX_I2C_WRITE_BYTES);
	};

	_tag_I2CFormat& operator= (_tag_I2CFormat& ref)
	{
		nCommand		= ref.nCommand;
		dwAddrLen		= ref.dwAddrLen;
		dwAddress		= ref.dwAddress;
		dwMilliseconds	= ref.dwMilliseconds;
		dwDataCount		= ref.dwDataCount;
		memcpy(byDataz, ref.byDataz, MAX_I2C_WRITE_BYTES);
		memcpy(byReadDataz, ref.byReadDataz, MAX_I2C_WRITE_BYTES);

		return *this;
	};

}ST_I2CFormat, *PST_I2CFormat;

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CI2C_Script
{
public:
	CI2C_Script();
	~CI2C_Script();

protected:

	CArray<ST_I2CFormat, ST_I2CFormat&>	m_arI2CList;

public:

	// Reset
	void			Reset_I2CList		();
	// Load
	BOOL			Load_I2CFile		(__in LPCTSTR szFileFullPath);
	ST_I2CFormat* 	Load_I2CFile		(__in LPCTSTR szFileFullPath, __out UINT& nItemCount);
	// Save
	BOOL			Save_I2CFile		(__in LPCTSTR szFileFullPath);
	BOOL			Save_I2CFile		(__in LPCTSTR szFileFullPath, __in const ST_I2CFormat* pstI2CDataz, __in UINT nItemCount);

	ST_I2CFormat*	GetI2CDataz			(__out UINT& nItemCount);
};

#endif // I2C_Script_h__

