﻿//*****************************************************************************
// Filename	: 	DAQWrapper.cpp
// Created	:	2016/2/11 - 15:07
// Modified	:	2016/2/11 - 15:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "DAQWrapper.h"
#include <strsafe.h>
#include "usb3_dio01_frm_import.h"


CDAQWrapper::CDAQWrapper()
{
	m_hWndOwner			= NULL;
	
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].SetBoardNumber(nBoard);
	}
}

CDAQWrapper::~CDAQWrapper()
{
	TRACE(_T("<<< Start ~CDAQWrapper >>> \n"));


	TRACE(_T("<<< End ~CDAQWrapper >>> \n"));
}

//=============================================================================
// Method		: Open_Device
// Access		: public  
// Returns		: BOOL
// Parameter	: __out int & iBoardCount
// Qualifier	:
// Last Update	: 2017/3/17 - 13:35
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::Open_Device(__out int& iBoardCount)
{
	if (m_bDeviceOpened)
	{
		iBoardCount = m_nBoardCount;
		TRACE(_T("DAQ Open DAQDevice: Aleady Opened\n"));
		return TRUE;
	}

	m_nBoardCount = OpenDAQDevice();
	iBoardCount = m_nBoardCount;

	if (0 < m_nBoardCount)
	{
		m_bDeviceOpened = TRUE;
		TRACE(_T("DAQ Open DAQ Device: Succeed\n"));
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Open DAQ Device: Failed\n"));
		return FALSE;
	}
}

//=============================================================================
// Method		: Reset_Board
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nBoard
// Qualifier	:
// Last Update	: 2017/3/17 - 13:35
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::Reset_Board(__in UINT nBoard)
{
	if (ResetBoard(nBoard))
	{
		TRACE(_T("DAQ Reset Board: Succeed\n"));
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Reset Board: Failed\n"));
		return FALSE;
	}
}

//=============================================================================
// Method		: Close_Device
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 13:35
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::Close_Device()
{
	if (FALSE == m_bDeviceOpened)
	{	
		TRACE(_T("DAQ Close DAQDevice: Aleady Closed\n"));
		return TRUE;
	}

	if (CloseDAQDevice())
	{
		m_bDeviceOpened = FALSE;
		TRACE(_T("DAQ Close DAQDevice: Succeed\n"));
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Close DAQDevice: Failed\n"));
		return FALSE;
	}
}

//=============================================================================
// Method		: GetBoardCount
// Access		: public  
// Returns		: UINT
// Qualifier	:
// Last Update	: 2017/3/17 - 13:35
// Desc.		:
//=============================================================================
UINT CDAQWrapper::GetBoardCount()
{
	return GetBoardNum();
}

//=============================================================================
// Method		: I2C_Reset
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nBoard
// Qualifier	:
// Last Update	: 2017/4/11 - 20:15
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::I2C_Reset(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].I2C_Reset();
}

BOOL CDAQWrapper::I2C_SetClock(__in UINT nBoard, __in int nClock)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].I2C_SetClock(nClock);
}

BOOL CDAQWrapper::I2C_Read(__in UINT nBoard, __in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in BYTE nByteSize, __out LPBYTE lpbyData)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].I2C_Read(bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData);
}

BOOL CDAQWrapper::I2C_Write(__in UINT nBoard, __in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in WORD nByteSize, __in LPBYTE lpbyData)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].I2C_Write(bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData);
}

BOOL CDAQWrapper::I2C_Read_Repeat(__in UINT nBoard, __in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in BYTE nByteSize, __out LPBYTE lpbyData, __in UINT nRetry /*= MAX_I2C_RETRY_CNT*/)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].I2C_Read_Repeat(bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData, nRetry);
}

BOOL CDAQWrapper::I2C_Write_Repeat(__in UINT nBoard, __in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in WORD nByteSize, __in LPBYTE lpbyData, __in UINT nRetry /*= MAX_I2C_RETRY_CNT*/)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].I2C_Write_Repeat(bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData, nRetry);
}

BOOL CDAQWrapper::LVDS_Init(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Init();
}

BOOL CDAQWrapper::LVDS_Close(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Close();
}

BOOL CDAQWrapper::LVDS_Start(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Start();
}

BOOL CDAQWrapper::LVDS_Stop(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Stop();
}

BOOL CDAQWrapper::LVDS_StartAll()
{
	return FALSE;
}

BOOL CDAQWrapper::LVDS_StopAll()
{
	return FALSE;
}

BOOL CDAQWrapper::LVDS_GetFrame(__in UINT nBoard, __out DWORD* pdwCnt, __out unsigned char* pchBuf)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].GetFrame(pdwCnt, pchBuf);
}

BOOL CDAQWrapper::LVDS_GetResolution(__in UINT nBoard, __out DWORD* pdwXRes, __out DWORD* pdwYRes)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].GetResolution(pdwXRes, pdwYRes);
}

BOOL CDAQWrapper::LVDS_SetDataMode(__in UINT nBoard, __in enDAQDataMode nMode)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].SetDataMode(nMode);
}

BOOL CDAQWrapper::LVDS_GetVersion(__in UINT nBoard, __out int* pnFpgaVer, __out int* pnFirmVer)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].GetVersion(pnFpgaVer, pnFirmVer);
}

BOOL CDAQWrapper::LVDS_BufferFlush(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].BufferFlush();
}

BOOL CDAQWrapper::LVDS_HsyncPol(__in UINT nBoard, __in enDAQHsyncPolarity bPol)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].HsyncPol(bPol);
}

BOOL CDAQWrapper::LVDS_PclkPol(__in UINT nBoard, __in enDAQPclkPolarity bPol)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].PclkPol(bPol);
}

BOOL CDAQWrapper::LVDS_SetDeUse(__in UINT nBoard, __in enDAQDvalUse bUse)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].SetDeUse(bUse);
}

BOOL CDAQWrapper::LVDS_VideoMode(__in UINT nBoard, __in enDAQVideoMode nMode)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].VideoMode(nMode);
}

BOOL CDAQWrapper::LVDS_GetFrameRate(__in UINT nBoard, __out DWORD* pdwRate)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].GetFrameRate(pdwRate);
}

BOOL CDAQWrapper::LVDS_SetDivide(__in UINT nBoard, __in DWORD dwWidth, __in DWORD dwHeight, __in DWORD dwX0, __in DWORD dwX1, __in DWORD dwX2, __in DWORD dwY0, __in DWORD dwY1, __in DWORD dwY2)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].SetDivide(dwWidth, dwHeight, dwX0, dwX1, dwX2, dwY0, dwY1, dwY2);
}

BOOL CDAQWrapper::LVDS_GetDivide(__in UINT nBoard, __out DWORD* pdwWidth, __out DWORD* pdwHeight, __out DWORD* pdwX0, __out DWORD* pdwX1, __out DWORD* pdwX2, __out DWORD* pdwY0, __out DWORD *pdwY1, __out DWORD* pdwY2)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].GetDivide(pdwWidth, pdwHeight, pdwX0, pdwX1, pdwX2, pdwY0, pdwY1, pdwY2);
}

BOOL CDAQWrapper::CLK_Init(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Clock_Init();
}

BOOL CDAQWrapper::CLK_Close(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Clock_Close();
}

BOOL CDAQWrapper::CLK_Select(__in UINT nBoard, __in int nSelect)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Clock_Select(nSelect);
}

DWORD CDAQWrapper::CLK_Get(__in UINT nBoard)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Clock_Get();
}

BOOL CDAQWrapper::CLK_Set(__in UINT nBoard, __in DWORD val)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Clock_Set(val);
}

BOOL CDAQWrapper::CLK_Off(__in UINT nBoard, __in BOOL bOff)
{
	ASSERT(nBoard < MAX_DAQ_CHANNEL);

	return m_DAQ_LVDS[nBoard].Clock_Off(bOff);
}

//=============================================================================
// Method		: Capture_Start
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoard
// Qualifier	:
// Last Update	: 2017/4/19 - 21:04
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::Capture_Start(__in UINT nBoard)
{
	BOOL Result = FALSE;

	if (nBoard < MAX_DAQ_CHANNEL)
		Result = m_DAQ_LVDS[nBoard].Capture_Start();

	return Result;
}

void CDAQWrapper::Capture_Start_All()
{
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].Capture_Start();
	}
}

BOOL CDAQWrapper::Capture_Stop(__in UINT nBoard)
{
	BOOL Result = FALSE;

	if (nBoard < MAX_DAQ_CHANNEL)
		Result = m_DAQ_LVDS[nBoard].Capture_Stop();

	return Result;
}

void CDAQWrapper::Capture_Stop_All()
{
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].Capture_Stop();
	}
}

BOOL CDAQWrapper::Cam_Restart(__in UINT nBoard)
{
	if (nBoard < MAX_DAQ_CHANNEL)
		return m_DAQ_LVDS[nBoard].Capture_Restart();

	return FALSE;
}

//=============================================================================
// Method		: SetFrameRate
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoard
// Parameter	: __in DOUBLE dFrameRate
// Qualifier	:
// Last Update	: 2017/4/19 - 21:04
// Desc.		:
//=============================================================================
void CDAQWrapper::SetFrameRate(__in UINT nBoard, __in DOUBLE dFrameRate)
{
	if (nBoard < MAX_DAQ_CHANNEL)
		m_DAQ_LVDS[nBoard].SetFixedFrameRate(dFrameRate);
}

void CDAQWrapper::SetFrameRate_All(__in DOUBLE dFrameRate)
{
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].SetFixedFrameRate(dFrameRate);
	}
}

//=============================================================================
// Method		: SetDAQOption
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoard
// Parameter	: __in ST_DAQOption stOption
// Qualifier	:
// Last Update	: 2017/4/20 - 21:14
// Desc.		:
//=============================================================================
void CDAQWrapper::SetDAQOption(__in UINT nBoard, __in ST_DAQOption stOption)
{
	if (nBoard < MAX_DAQ_CHANNEL)
		m_DAQ_LVDS[nBoard].SetOption(stOption);
}

void CDAQWrapper::SetDAQOption_All(__in ST_DAQOption stOption)
{
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].SetOption(stOption);
	}
}

BOOL CDAQWrapper::GetDAQOption(__in UINT nBoard, __out ST_DAQOption& stOption)
{
	if (nBoard < MAX_DAQ_CHANNEL)
	{
		stOption = m_DAQ_LVDS[nBoard].GetOption();
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//=============================================================================
// Method		: GetDAQStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nBoard
// Parameter	: __out ST_DAQStatus & stStatus
// Qualifier	:
// Last Update	: 2018/8/6 - 14:46
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::GetDAQStatus(__in UINT nBoard, __out ST_DAQStatus& stStatus)
{
	if (nBoard < MAX_DAQ_CHANNEL)
	{
		stStatus = m_DAQ_LVDS[nBoard].GetDAQStatus();
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//=============================================================================
// Method		: SetUse_Flip
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoard
// Parameter	: __in enFlipType nFlip
// Qualifier	:
// Last Update	: 2017/5/22 - 15:32
// Desc.		:
//=============================================================================
void CDAQWrapper::SetUse_Flip(__in UINT nBoard, __in enFlipType nFlip)
{
	if (nBoard < MAX_DAQ_CHANNEL)
	{
		m_DAQ_LVDS[nBoard].SetUse_Flip(nFlip);
	}
}

void CDAQWrapper::SetUse_Flip_All(__in enFlipType nFlip)
{
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].SetUse_Flip(nFlip);
	}
}

void CDAQWrapper::SetUse_Rotate(__in UINT nBoard, __in enRotateType nRotate)
{
	if (nBoard < MAX_DAQ_CHANNEL)
	{
		m_DAQ_LVDS[nBoard].SetUse_Rotate(nRotate);
	}
}

void CDAQWrapper::SetUse_Rotate_All(__in enRotateType nRotate)
{
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].SetUse_Rotate(nRotate);
	}
}

BOOL CDAQWrapper::Is_LVDS_Init(__in UINT nBoard)
{
	return m_DAQ_LVDS[nBoard].Is_LVDS_Init();
}

BOOL CDAQWrapper::Is_LVDS_Start(__in UINT nBoard)
{
	return m_DAQ_LVDS[nBoard].Is_LVDS_Start();
}

//=============================================================================
// Method		: SetI2CFilePath
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoard
// Parameter	: __in LPCTSTR szFilePath
// Qualifier	:
// Last Update	: 2018/2/5 - 11:56
// Desc.		:
//=============================================================================
void CDAQWrapper::SetI2CFilePath(__in UINT nBoard, __in LPCTSTR szFilePath)
{
	return m_DAQ_LVDS[nBoard].SetI2CFilePath(szFilePath);
}

void CDAQWrapper::SetI2CFilePath_All(__in LPCTSTR szFilePath)
{
	for (UINT nBoard = 0; nBoard < MAX_DAQ_CHANNEL; nBoard++)
	{
		m_DAQ_LVDS[nBoard].SetI2CFilePath(szFilePath);
	}
}

void CDAQWrapper::Set_UseAutoI2CWrite(__in UINT nBoard, __in BOOL bUse)
{
	m_DAQ_LVDS[nBoard].Set_UseAutoI2CWrite(bUse);
}

//=============================================================================
// Method		: SaveRawImage
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nBoard
// Parameter	: __in LPCTSTR szFilePath
// Qualifier	:
// Last Update	: 2017/9/8 - 12:46
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::SaveRawImage(__in UINT nBoard, __in LPCTSTR szFilePath)
{
	return m_DAQ_LVDS[nBoard].SaveRawImage(szFilePath);
}

//=============================================================================
// Method		: Write_RegisterData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nBoard
// Parameter	: __in UINT nRetry
// Parameter	: __in DWORD dwLoopDelay
// Parameter	: __in DWORD dwWeightDelay
// Qualifier	:
// Last Update	: 2018/2/6 - 17:39
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::Write_RegisterData(__in UINT nBoard, __in UINT nRetry /*= 9*/, __in DWORD dwLoopDelay /*= 200*/, __in DWORD dwWeightDelay /*= 100*/)
{
	return m_DAQ_LVDS[nBoard].Write_RegisterData(nRetry, dwLoopDelay, dwWeightDelay);
}

//=============================================================================
// Method		: Check_VideoSignal
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nBoard
// Parameter	: __in DWORD dwCheckTime
// Parameter	: __in DWORD dwGapDelay
// Qualifier	:
// Last Update	: 2018/2/4 - 16:27
// Desc.		:
//=============================================================================
BOOL CDAQWrapper::Check_VideoSignal(__in UINT nBoard, __in DWORD dwCheckTime, __in DWORD dwGapDelay /*= 100*/)
{
	return m_DAQ_LVDS[nBoard].Check_VideoSignal(dwCheckTime, dwGapDelay);
}

//=============================================================================
// Method		: Check_FrameCount
// Access		: public  
// Returns		: UINT
// Parameter	: __in UINT nBoard
// Parameter	: __in UINT nMinFPS
// Parameter	: __in UINT nMaxFPS
// Parameter	: __in DWORD dwCheckTime
// Parameter	: __in DWORD dwGapDelay
// Parameter	: __in UINT nRetry
// Qualifier	:
// Last Update	: 2018/2/4 - 16:27
// Desc.		:
//=============================================================================
UINT CDAQWrapper::Check_FrameCount(__in UINT nBoard, __in UINT nMinFPS, __in UINT nMaxFPS, __in DWORD dwCheckTime, __in DWORD dwGapDelay /*= 50*/, __in UINT nRetry /*= 5*/)
{
	return m_DAQ_LVDS[nBoard].Check_FrameCount(nMinFPS, nMaxFPS, dwCheckTime, dwGapDelay, nRetry);
}

//=============================================================================
// Method		: Set_RGB_Exposure
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoard
// Parameter	: __in DOUBLE dExposure
// Qualifier	:
// Last Update	: 2018/2/5 - 17:07
// Desc.		:
//=============================================================================
void CDAQWrapper::Set_RGB_Exposure(__in UINT nBoard, __in DOUBLE dExposure /*= 0.0f*/)
{
	m_DAQ_LVDS[nBoard].Set_RGB_Exposure(dExposure);
}

//=============================================================================
// Method		: Set_RGB_ContrastBrightness
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoard
// Parameter	: __in char chContrast
// Parameter	: __in char chBrightness
// Qualifier	:
// Last Update	: 2018/2/5 - 17:07
// Desc.		:
//=============================================================================
void CDAQWrapper::Set_RGB_ContrastBrightness(__in UINT nBoard, __in char chContrast /*= 0*/, __in char chBrightness /*= 0*/)
{
	m_DAQ_LVDS[nBoard].Set_RGB_ContrastBrightness(chContrast, chBrightness);
}
