﻿//*****************************************************************************
// Filename	: 	ImageProcessing.cpp
// Created	:	2017/6/16 - 13:20
// Modified	:	2017/6/16 - 13:20
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "ImageProcessing.h"
#include <math.h>

CImageProcessing::CImageProcessing()
{
}


CImageProcessing::~CImageProcessing()
{
}

void CImageProcessing::Contrast(double contrast, unsigned char* lpbyImage, const unsigned int width, const unsigned int height)
{
// 	byte[] contrast_lookup = new byte[256];
// 	double newValue = 0;
// 	double c = (100.0 + contrast) / 100.0;
// 
// 	c *= c;
// 
// 	for (int i = 0; i < 256; i++)
// 	{
// 		newValue = (double)i;
// 		newValue /= 255.0;
// 		newValue -= 0.5;
// 		newValue *= c;
// 		newValue += 0.5;
// 		newValue *= 255;
// 
// 		if (newValue < 0)
// 			newValue = 0;
// 		if (newValue > 255)
// 			newValue = 255;
// 		contrast_lookup[i] = (byte)newValue;
// 	}
// 
// 	var bitmapdata = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height),System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
// 
// 	int PixelSize = 4;
// 
// 	for (int y = 0; y < bitmapdata.Height; y++)
// 	{
// 		byte* destPixels = (byte*)bitmapdata.Scan0 + (y * bitmapdata.Stride);
// 		for (int x = 0; x < bitmapdata.Width; x++)
// 		{
// 			destPixels[x * PixelSize] = contrast_lookup[destPixels[x * PixelSize]]; // B
// 			destPixels[x * PixelSize + 1] = contrast_lookup[destPixels[x * PixelSize + 1]]; // G
// 			destPixels[x * PixelSize + 2] = contrast_lookup[destPixels[x * PixelSize + 2]]; // R
// 			//destPixels[x * PixelSize + 3] = contrast_lookup[destPixels[x * PixelSize + 3]]; //A
// 		}
// 	}
// 	bmp.UnlockBits(bitmapdata);
}

void CImageProcessing::Saturation(double saturation, unsigned char* lpbyImage, const unsigned int width, const unsigned int height)
{

// #define  Pr  .299
// #define  Pg  .587
// #define  Pb  .114

// 	double  P = sqrt((*R)*(*R)*Pr + (*G)*(*G)*Pg + (*B)*(*B)*Pb);
// 
// 	*R = P + ((*R) - P)*change;
// 	*G = P + ((*G) - P)*change;
// 	*B = P + ((*B) - P)*change;
//}
}



void CImageProcessing::Levels(unsigned int *rgb_buffer, const unsigned int width, const unsigned int height, const float exposure, const float brightness, const float contrast, const float saturation)
{
	const float exposureFactor = powf(2.0f, exposure);
	const float brightnessFactor = brightness / 10.0f;
	const float contrastFactor = contrast > 0.0f ? contrast : 0.0f;
	const float saturationFactor = 1.0f - saturation;

	const unsigned int limit = height*width;
	for (unsigned int pixelIndex = 0; pixelIndex < limit; pixelIndex++)
	{
		const int pixelValue = rgb_buffer[pixelIndex];

		float R = ((pixelValue & 0xff0000) >> 16) / 255.0f;
		float G = ((pixelValue & 0xff00) >> 8) / 255.0f;
		float B = (pixelValue & 0xff) / 255.0f;

		// Clamp values + exposure factor + contrast
		R = (((R > 1.0f ? exposureFactor : R < 0.0f ? 0.0f : exposureFactor*R) - 0.5f) * contrastFactor) + 0.5f;
		G = (((G > 1.0f ? exposureFactor : G < 0.0f ? 0.0f : exposureFactor*G) - 0.5f) * contrastFactor) + 0.5f;
		B = (((B > 1.0f ? exposureFactor : B < 0.0f ? 0.0f : exposureFactor*B) - 0.5f) * contrastFactor) + 0.5f;

		// Saturation + Brightness
		const float graySaturFactorPlusBrightness = brightnessFactor + ((R * 0.3f) + (G * 0.59f) + (B * 0.11f)) * saturationFactor;
		R = graySaturFactorPlusBrightness + R * saturation;
		G = graySaturFactorPlusBrightness + G * saturation;
		B = graySaturFactorPlusBrightness + B * saturation;

		// Clamp values
		R = R > 1.0f ? 255.0f : R < 0.0f ? 0.0f : 255.0f*R;
		G = G > 1.0f ? 255.0f : G < 0.0f ? 0.0f : 255.0f*G;
		B = B > 1.0f ? 255.0f : B < 0.0f ? 0.0f : 255.0f*B;

		rgb_buffer[pixelIndex] = ((int)R << 16) | ((int)G << 8) | (int)B;
	}
}

void CImageProcessing::applyLevels(unsigned int *rgb_buffer, const unsigned int width, const unsigned int height, const float exposure, const float brightness, const float contrast, const float saturation)
{
	float R, G, B;

	unsigned int pixelIndex = 0;

	float exposureFactor = powf(2.0f, exposure);
	float brightnessFactor = brightness / 10.0f;
	float contrastFactor = contrast > 0.0f ? contrast : 0.0f;

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			const int pixelValue = rgb_buffer[pixelIndex];

			R = ((pixelValue & 0xff0000) >> 16) / 255.0f;
			//R = ((pixelValue & 0xff0000) >> 10);
			G = ((pixelValue & 0xff00) >> 8) / 255.0f;
			//G = ((pixelValue & 0xff00) >> 2);
			B = (pixelValue & 0xff) / 255.0f;

			// Clamp values
			R = R > 1.0f ? 1.0f : R < 0.0f ? 0.0f : R;
			G = G > 1.0f ? 1.0f : G < 0.0f ? 0.0f : G;
			B = B > 1.0f ? 1.0f : B < 0.0f ? 0.0f : B;

			// Exposure
			R *= exposureFactor;
			G *= exposureFactor;
			B *= exposureFactor;

			// Contrast
			R = (((R - 0.5f) * contrastFactor) + 0.5f);
			G = (((G - 0.5f) * contrastFactor) + 0.5f);
			B = (((B - 0.5f) * contrastFactor) + 0.5f);

			// Saturation
			float gray = (R * 0.3f) + (G * 0.59f) + (B * 0.11f);
			R = gray * (1.0f - saturation) + R * saturation;
			G = gray * (1.0f - saturation) + G * saturation;
			B = gray * (1.0f - saturation) + B * saturation;

			// Brightness
			R += brightnessFactor;
			G += brightnessFactor;
			B += brightnessFactor;

			// Clamp values
			R = R > 1.0f ? 1.0f : R < 0.0f ? 0.0f : R;
			G = G > 1.0f ? 1.0f : G < 0.0f ? 0.0f : G;
			B = B > 1.0f ? 1.0f : B < 0.0f ? 0.0f : B;

			// Store new pixel value
			R *= 255.0f;
			G *= 255.0f;
			B *= 255.0f;

			rgb_buffer[pixelIndex] = ((int)R << 16) | ((int)G << 8) | (int)B;

			pixelIndex++;
		}
	}
}