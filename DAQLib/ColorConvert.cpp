﻿//*****************************************************************************
// Filename	: 	ColorConvert.cpp
// Created	:	2017/3/27 - 16:11
// Modified	:	2017/3/27 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "ColorConvert.h"
#include <math.h>

#define CLIP8(x) (((x) < 0) ? 0 : (((x) > 255) ? 255 : (x)))

#define RGB24_SIZE		3
#define RGB32_SIZE		4

CColorConvert::CColorConvert()
{
}


CColorConvert::~CColorConvert()
{
}
//=============================================================================
// Method		: ClearBorders
// Access		: public static  
// Returns		: void
// Parameter	: __inout LPBYTE pbyRGB
// Parameter	: __in int nXRes
// Parameter	: __in int nYRes
// Parameter	: __in int nWidth
// Qualifier	:
// Last Update	: 2017/3/28 - 13:21
// Desc.		:
//=============================================================================
void CColorConvert::ClearBorders (__inout LPBYTE pbyRGB, __in int nXRes, __in int nYRes, __in int nWidth)
{
	int i, j;
	// black edges are added with a width w:
	i = 3 * nXRes * nWidth - 1;
	j = 3 * nXRes * nYRes - 1;
	while (i >= 0)
	{
		pbyRGB[i--] = 0;
		pbyRGB[j--] = 0;
	}

	int low = nXRes * (nWidth - 1) * 3 - 1 + nWidth * 3;
	i = low + nXRes * (nYRes - nWidth * 2 + 1) * 3;
	while (i > low)
	{
		j = 6 * nWidth;
		while (j > 0)
		{
			pbyRGB[i--] = 0;
			j--;
		}

		i -= (nXRes - 2 * nWidth) * 3;
	}
}

//=============================================================================
// Method		: FlipVertical
// Access		: public static  
// Returns		: BOOL
// Parameter	: __in LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/28 - 13:22
// Desc.		:
//=============================================================================
BOOL CColorConvert::FlipVertical(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	UINT nChannelCount	= RGB32_SIZE;
	UINT nDepth			= 1;
	UINT nSize			= nWidth * nHeight * nChannelCount * nDepth; // in byte
	if (nSize <= 0)
		return FALSE;
	
	register UINT nWidthStep = nWidth * nChannelCount * nDepth;
	register char*	pTarget = nullptr;
	register char*	pSource = nullptr;

	for (register unsigned int nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		pSource = (char*)pbySrc + (nIdx_Y * nWidthStep);

		memcpy(pTarget, (char*)(pSource), nWidthStep);
		pTarget -= nWidthStep;
	}

	return TRUE;
}

BOOL CColorConvert::FlipVertical(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight)
{
	UINT nChannelCount = RGB32_SIZE;
	UINT nDepth = 1;
	UINT nSize = nWidth * nHeight * nChannelCount * nDepth; // in byte

	LPBYTE pbyTemp = new BYTE[nSize];
	memcpy(pbyTemp, pbySrc, nSize);

	BOOL bReturn = TRUE;
	if (FALSE == FlipVertical(pbyTemp, nWidth, nHeight, pbySrc))
	{
		bReturn =  FALSE;
	}

	delete[] pbyTemp;

	return bReturn;
}

//=============================================================================
// Method		: FlipHorizontal
// Access		: public static  
// Returns		: BOOL
// Parameter	: __in LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/28 - 13:22
// Desc.		:
//=============================================================================
BOOL CColorConvert::FlipHorizontal(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
// CM_Grey			: 1
// CM_YUYV422		: packs 3 channel YUV into 2 Bytes
// CM_RGB			: 3
// CM_BGRA			: 4
// CM_Bayer_GBRG	: 1 packs 3 channel RGB into 1 Byte Bayer pattern

	UINT nChannelCount	= RGB32_SIZE;
	UINT nDepth			= 1; // sizeof(char)
 	UINT nSize			= nWidth * nHeight * nChannelCount * nDepth; // in byte
 	if (nSize <= 0)
 		return FALSE;
 
 	UINT nPixelSize = nChannelCount * nDepth;
	UINT nWidthStep = nWidth * nDepth * nChannelCount; 	
 
 	register char*	pTarget = nullptr;
 	register char*	pSource = nullptr;

 	for (register UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
 	{
 		pTarget = (char*)pbyDst + (nIdx_Y + 1) * nWidthStep - nPixelSize;
 		pSource = (char*)pbySrc + (nIdx_Y * nWidthStep);
 
 		for (register UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X++)
 		{
 			memcpy(pTarget - (nIdx_X * nPixelSize), pSource + (nIdx_X * nPixelSize), nPixelSize);
 		}
 	}

	return TRUE;
}

BOOL CColorConvert::FlipHorizontal(__inout LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight)
{
	UINT nChannelCount = RGB32_SIZE;
	UINT nDepth = 1;
	UINT nSize = nWidth * nHeight * nChannelCount * nDepth; // in byte

	LPBYTE pbyTemp = new BYTE[nSize];
	memcpy(pbyTemp, pbySrc, nSize);

	BOOL bReturn = TRUE;
	if (FALSE == FlipHorizontal(pbyTemp, nWidth, nHeight, pbySrc))
	{
		bReturn = FALSE;
	}

	delete[] pbyTemp;

	return bReturn;
}

//=============================================================================
// Method		: YUV420pToRGB32
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/27 - 16:25
// Desc.		:
//=============================================================================
void CColorConvert::YUV420pToRGB32(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB32_SIZE];
	}

	float y, u = 0.0, v = 0.0;
	float r, g, b;
	UINT nOffset_U = 0;
	UINT nOffset_V = 0;
	UINT nOffset_RGB32 = 0;
	UINT iPixelCnt = nWidth * nHeight;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		if (nIdx_Y % 2 == 0)
		{
			nOffset_V = iPixelCnt + (nIdx_Y / 2 * nWidth / 2);
			nOffset_U = iPixelCnt + (iPixelCnt / 4) + (nIdx_Y / 2 * nWidth / 2);
		}

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X++)
		{
			y = 1.164f * (float)(pbySrc[(nIdx_Y * nWidth) + nIdx_X] - 16);

			if (nIdx_X % 2 == 0)
			{
				v = (float)(pbySrc[nOffset_V + (nIdx_X / 2)] - 128);
				u = (float)(pbySrc[nOffset_U + (nIdx_X / 2)] - 128);
			}

			b = float(rint(y + (2.018f * v)));
			g = float(rint(y - (0.813f * u) - (0.391f * v)));
			r = float(rint(y + (1.596f * u)));

			nOffset_RGB32 = ((nIdx_Y * nWidth) + nIdx_X) * RGB32_SIZE;

			pbyDst[nOffset_RGB32]		= (BYTE)CLIP8(b);
			pbyDst[nOffset_RGB32 + 1]	= (BYTE)CLIP8(g);
			pbyDst[nOffset_RGB32 + 2]	= (BYTE)CLIP8(r);
			pbyDst[nOffset_RGB32 + 3]	= 0xFF;
		}
	}
}

void CColorConvert::YUV420pToRGB24(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB24_SIZE];
	}

	float y, u = 0.0, v = 0.0;
	float r, g, b;
	UINT nOffset_U = 0;
	UINT nOffset_V = 0;
	UINT nOffset_RGB24 = 0;
	UINT iPixelCnt = nWidth * nHeight;

	for (UINT nIdx_Y = 0; nIdx_Y < nHeight; nIdx_Y++)
	{
		if (nIdx_Y % 2 == 0)
		{
			nOffset_V = iPixelCnt + (nIdx_Y / 2 * nWidth / 2);
			nOffset_U = iPixelCnt + (iPixelCnt / 4) + (nIdx_Y / 2 * nWidth / 2);
		}

		for (UINT nIdx_X = 0; nIdx_X < nWidth; nIdx_X++)
		{
			y = 1.164f * (float)(pbySrc[(nIdx_Y * nWidth) + nIdx_X] - 16);

			if (nIdx_X % 2 == 0)
			{
				v = (float)(pbySrc[nOffset_V + (nIdx_X / 2)] - 128);
				u = (float)(pbySrc[nOffset_U + (nIdx_X / 2)] - 128);
			}

			b = float(rint(y + (2.018f * v)));
			g = float(rint(y - (0.813f * u) - (0.391f * v)));
			r = float(rint(y + (1.596f * u)));

			nOffset_RGB24 = ((nIdx_Y * nWidth) + nIdx_X) * RGB24_SIZE;

			pbyDst[nOffset_RGB24] = (BYTE)CLIP8(b);
			pbyDst[nOffset_RGB24 + 1] = (BYTE)CLIP8(g);
			pbyDst[nOffset_RGB24 + 2] = (BYTE)CLIP8(r);			
		}
	}
}

//=============================================================================
// Method		: YUV422ToRGB32
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in BOOL bUYVY422
// Qualifier	:
// Last Update	: 2017/3/27 - 16:26
// Desc.		:
//=============================================================================
void CColorConvert::YUV422ToRGB32(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 /*= TRUE*/)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB32_SIZE];
	}

	UINT nIdx_Src, nIdx_Dst;
	register int y0, y1, u, v, r, g, b;
	UINT iPixelCnt = nWidth * nHeight;

	if (bUYVY422)
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 8)
		{
			u	= (BYTE)pbySrc[nIdx_Src + 0] - 128;
			y0	= (BYTE)pbySrc[nIdx_Src + 1];
			v	= (BYTE)pbySrc[nIdx_Src + 2] - 128;
			y1	= (BYTE)pbySrc[nIdx_Src + 3];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);
			pbyDst[nIdx_Dst + 3] = 0xFF;

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(b);
			pbyDst[nIdx_Dst + 5] = (BYTE)(g);
			pbyDst[nIdx_Dst + 6] = (BYTE)(r);
			pbyDst[nIdx_Dst + 7] = 0xFF;
		}
	}
	else
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 8)
		{
			u	= (BYTE)pbySrc[nIdx_Src + 1] - 128;
			y0	= (BYTE)pbySrc[nIdx_Src + 0];
			v	= (BYTE)pbySrc[nIdx_Src + 3] - 128;
			y1	= (BYTE)pbySrc[nIdx_Src + 2];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);
			pbyDst[nIdx_Dst + 3] = 0xFF;

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(b);
			pbyDst[nIdx_Dst + 5] = (BYTE)(g);
			pbyDst[nIdx_Dst + 6] = (BYTE)(r);
			pbyDst[nIdx_Dst + 7] = 0xFF;
		}
	}
}

void CColorConvert::YUV422ToRGB24(__in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in BOOL bUYVY422 /*= TRUE*/)
{
	if ((pbyDst == pbySrc) || (NULL == pbyDst))
	{
		pbyDst = new BYTE[nWidth * nHeight * RGB24_SIZE];
	}

	UINT nIdx_Src, nIdx_Dst;
	register int y0, y1, u, v, r, g, b;
	UINT iPixelCnt = nWidth * nHeight;

	if (bUYVY422)
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 6)
		{
// 			u	= (BYTE)pbySrc[nIdx_Src + 0] - 128;
// 			y0	= (BYTE)pbySrc[nIdx_Src + 1];
// 			v	= (BYTE)pbySrc[nIdx_Src + 2] - 128;
// 			y1	= (BYTE)pbySrc[nIdx_Src + 3];

			u	= (int)pbySrc[nIdx_Src + 2] - 128;
			y0	= (int)pbySrc[nIdx_Src + 1];
			v	= (int)pbySrc[nIdx_Src + 0] - 128;
			y1	= (int)pbySrc[nIdx_Src + 3];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);			

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 3] = (BYTE)(b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(g);
			pbyDst[nIdx_Dst + 5] = (BYTE)(r);
		}
	}
	else
	{
		for (nIdx_Src = 0, nIdx_Dst = 0; nIdx_Src < (iPixelCnt << 1); nIdx_Src += 4, nIdx_Dst += 6)
		{
			u	= (BYTE)pbySrc[nIdx_Src + 1] - 128;
			y0	= (BYTE)pbySrc[nIdx_Src + 0];
			v	= (BYTE)pbySrc[nIdx_Src + 3] - 128;
			y1	= (BYTE)pbySrc[nIdx_Src + 2];

			YUV2RGB(y0, u, v, r, g, b);
			pbyDst[nIdx_Dst + 0] = (BYTE)(b);
			pbyDst[nIdx_Dst + 1] = (BYTE)(g);
			pbyDst[nIdx_Dst + 2] = (BYTE)(r);

			YUV2RGB(y1, u, v, r, g, b);
			pbyDst[nIdx_Dst + 3] = (BYTE)(b);
			pbyDst[nIdx_Dst + 4] = (BYTE)(g);
			pbyDst[nIdx_Dst + 5] = (BYTE)(r);
		}
	}
}

//=============================================================================
// Method		: RGB24ToRGB32
// Access		: public static  
// Returns		: void
// Parameter	: __in LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/27 - 16:26
// Desc.		:
//=============================================================================
void CColorConvert::RGB24ToRGB32(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	UINT iPixelCnt = nWidth * nHeight;

	for (UINT i = 0; i < iPixelCnt; i++)
	{
		*pbyDst++ = *pbySrc++;
		*pbyDst++ = *pbySrc++;
		*pbyDst++ = *pbySrc++;
		*pbyDst++ = 0xFF;
	}
}

void CColorConvert::RGB32ToRGB24(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	UINT iPixelCnt = nWidth * nHeight;

	for (UINT i = 0; i < iPixelCnt; i++)
	{
		*pbyDst++ = *pbySrc++;
		*pbyDst++ = *pbySrc++;
		*pbyDst++ = *pbySrc++;
		*pbySrc++;
	}
}

//=============================================================================
// Method		: BayerToRGBSimple
// Access		: public static  
// Returns		: void
// Parameter	: __in LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in EColorModel nColorModel
// Qualifier	:
// Last Update	: 2017/3/27 - 16:27
// Desc.		:
//=============================================================================
void CColorConvert::BayerToRGBSimple(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel)
{
	const int bayerStep = nWidth;
	const int rgbStep = 3 * nWidth;
	int width = nWidth;
	int height = nHeight;
	int blue = nColorModel == CM_Bayer_BGGR || nColorModel == CM_Bayer_GBRG ? -1 : 1;
	int start_with_green = nColorModel == CM_Bayer_GBRG || nColorModel == CM_Bayer_GRBG;
	int i, imax, iinc;

	// add black border
	imax = nWidth * nHeight * 3;
	for (i = nWidth * (nHeight - 1) * 3; i < imax; i++)
	{
		pbyDst[i] = 0;
	}
	
	iinc = (nWidth - 1) * 3;
	for (i = (nWidth - 1) * 3; i < imax; i += iinc)
	{
		pbyDst[i++] = 0;
		pbyDst[i++] = 0;
		pbyDst[i++] = 0;
	}

	pbyDst	+= 1;
	width	-= 1;
	height	-= 1;

	for (; height--; pbySrc += bayerStep, pbyDst += rgbStep)
	{
		const LPBYTE bayerEnd = pbySrc + width;

		if (start_with_green)
		{
			pbyDst[-blue]	= pbySrc[1];
			pbyDst[0]		= (pbySrc[0] + pbySrc[bayerStep + 1] + 1) >> 1;
			pbyDst[blue]	= pbySrc[bayerStep];
			pbySrc++;
			pbyDst += 3;
		}

		if (blue > 0)
		{
			for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
				pbyDst[-1]	= pbySrc[0];
				pbyDst[0]	= (pbySrc[1] + pbySrc[bayerStep] + 1) >> 1;
				pbyDst[1]	= pbySrc[bayerStep + 1];

				pbyDst[2]	= pbySrc[2];
				pbyDst[3]	= (pbySrc[1] + pbySrc[bayerStep + 2] + 1) >> 1;
				pbyDst[4]	= pbySrc[bayerStep + 1];
			}
		}
		else
		{
			for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
				pbyDst[1]	= pbySrc[0];
				pbyDst[0]	= (pbySrc[1] + pbySrc[bayerStep] + 1) >> 1;
				pbyDst[-1]	= pbySrc[bayerStep + 1];

				pbyDst[4]	= pbySrc[2];
				pbyDst[3]	= (pbySrc[1] + pbySrc[bayerStep + 2] + 1) >> 1;
				pbyDst[2]	= pbySrc[bayerStep + 1];
			}
		}

		if (pbySrc < bayerEnd)
		{
			pbyDst[-blue]	= pbySrc[0];
			pbyDst[0]		= (pbySrc[1] + pbySrc[bayerStep] + 1) >> 1;
			pbyDst[blue]	= pbySrc[bayerStep + 1];
			pbySrc++;
			pbyDst += 3;
		}

		pbySrc -= width;
		pbyDst -= width * 3;

		blue = -blue;
		start_with_green = !start_with_green;
	}
}

//=============================================================================
// Method		: BayerToRGBBilinear
// Access		: public static  
// Returns		: void
// Parameter	: __in LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in EColorModel nColorModel
// Qualifier	:
// Last Update	: 2017/3/27 - 16:30
// Desc.		:
//=============================================================================
void CColorConvert::BayerToRGBBilinear(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel)
{
	const int bayerStep = nWidth;
    const int rgbStep = 3 * nWidth;
    int width = nWidth;
    int height = nHeight;
    int blue = nColorModel == CM_Bayer_BGGR || nColorModel == CM_Bayer_GBRG ? -1 : 1;
    int start_with_green = nColorModel == CM_Bayer_GBRG || nColorModel == CM_Bayer_GRBG;

	ClearBorders(pbyDst, nWidth, nHeight, 1);
    pbyDst	+= rgbStep + 3 + 1;
    height	-= 2;
    width	-= 2;

    for (; height--; pbySrc += bayerStep, pbyDst += rgbStep)
	{
        int t0, t1;
        const LPBYTE bayerEnd = pbySrc + width;

        if (start_with_green)
		{
            /* OpenCV has a bug in the next line, which was
               t0 = (pbySrc[0] + pbySrc[bayerStep * 2] + 1) >> 1; */
            t0 = (pbySrc[1] + pbySrc[bayerStep * 2 + 1] + 1) >> 1;
            t1 = (pbySrc[bayerStep] + pbySrc[bayerStep + 2] + 1) >> 1;
            pbyDst[-blue]	= (BYTE) t0;
            pbyDst[0]		= pbySrc[bayerStep + 1];
            pbyDst[blue]	= (BYTE) t1;
            pbySrc++;
            pbyDst += 3;
        }

        if (blue > 0)
		{
            for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
                t0 = (pbySrc[0] + pbySrc[2] + pbySrc[bayerStep * 2] + pbySrc[bayerStep * 2 + 2] + 2) >> 2;
                t1 = (pbySrc[1] + pbySrc[bayerStep] + pbySrc[bayerStep + 2] + pbySrc[bayerStep * 2 + 1] + 2) >> 2;
                pbyDst[-1]	= (BYTE) t0;
                pbyDst[0]	= (BYTE) t1;
                pbyDst[1]	= pbySrc[bayerStep + 1];

                t0 = (pbySrc[2] + pbySrc[bayerStep * 2 + 2] + 1) >> 1;
                t1 = (pbySrc[bayerStep + 1] + pbySrc[bayerStep + 3] + 1) >> 1;
                pbyDst[2] = (BYTE) t0;
                pbyDst[3] = pbySrc[bayerStep + 2];
                pbyDst[4] = (BYTE) t1;
            }
        }
		else
		{
            for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
                t0 = (pbySrc[0] + pbySrc[2] + pbySrc[bayerStep * 2] + pbySrc[bayerStep * 2 + 2] + 2) >> 2;
                t1 = (pbySrc[1] + pbySrc[bayerStep] + pbySrc[bayerStep + 2] + pbySrc[bayerStep * 2 + 1] + 2) >> 2;
                pbyDst[1]	= (BYTE) t0;
                pbyDst[0]	= (BYTE) t1;
                pbyDst[-1]	= pbySrc[bayerStep + 1];

                t0 = (pbySrc[2] + pbySrc[bayerStep * 2 + 2] + 1) >> 1;
                t1 = (pbySrc[bayerStep + 1] + pbySrc[bayerStep + 3] + 1) >> 1;
                pbyDst[4] = (BYTE) t0;
                pbyDst[3] = pbySrc[bayerStep + 2];
                pbyDst[2] = (BYTE) t1;
            }
        }

        if (pbySrc < bayerEnd)
		{
            t0 = (pbySrc[0] + pbySrc[2] + pbySrc[bayerStep * 2] + pbySrc[bayerStep * 2 + 2] + 2) >> 2;
            t1 = (pbySrc[1] + pbySrc[bayerStep] + pbySrc[bayerStep + 2] + pbySrc[bayerStep * 2 + 1] + 2) >> 2;
            pbyDst[-blue]	= (BYTE) t0;
            pbyDst[0]		= (BYTE) t1;
            pbyDst[blue]	= pbySrc[bayerStep + 1];
            pbySrc++;
            pbyDst += 3;
        }

        pbySrc -= width;
        pbyDst -= width * 3;

        blue = -blue;
        start_with_green = !start_with_green;
    }
}

//=============================================================================
// Method		: BayerToRGBNearestNeighbour
// Access		: public static  
// Returns		: void
// Parameter	: __in LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in EColorModel nColorModel
// Qualifier	:
// Last Update	: 2017/3/27 - 16:30
// Desc.		:
//=============================================================================
void CColorConvert::BayerToRGBNearestNeighbour(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel)
{
	const int bayerStep = nWidth;
	const int rgbStep = 3 * nWidth;
	int width = nWidth;
	int height = nHeight;
	int blue = nColorModel == CM_Bayer_BGGR || nColorModel == CM_Bayer_GBRG ? -1 : 1;
	int start_with_green = nColorModel == CM_Bayer_GBRG || nColorModel == CM_Bayer_GRBG;
	int i, imax, iinc;

	imax = nWidth * nHeight * 3;
	for (i = nWidth * (nHeight - 1) * 3; i < imax; i++)
	{
		pbyDst[i] = 0;
	}

	iinc = (nWidth - 1) * 3;
	for (i = (nWidth - 1) * 3; i < imax; i += iinc)
	{
		pbyDst[i++] = 0;
		pbyDst[i++] = 0;
		pbyDst[i++] = 0;
	}

	pbyDst += 1;
	width -= 1;
	height -= 1;

	for (; height--; pbySrc += bayerStep, pbyDst += rgbStep)
	{
		//int t0, t1;
		const LPBYTE bayerEnd = pbySrc + width;

		if (start_with_green)
		{
			pbyDst[-blue]	= pbySrc[1];
			pbyDst[0]		= pbySrc[bayerStep + 1];
			pbyDst[blue]	= pbySrc[bayerStep];
			pbySrc++;
			pbyDst += 3;
		}

		if (blue > 0)
		{
			for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
				pbyDst[-1] = pbySrc[0];
				pbyDst[0] = pbySrc[1];
				pbyDst[1] = pbySrc[bayerStep + 1];

				pbyDst[2] = pbySrc[2];
				pbyDst[3] = pbySrc[bayerStep + 2];
				pbyDst[4] = pbySrc[bayerStep + 1];
			}
		}
		else
		{
			for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
				pbyDst[1] = pbySrc[0];
				pbyDst[0] = pbySrc[1];
				pbyDst[-1] = pbySrc[bayerStep + 1];

				pbyDst[4] = pbySrc[2];
				pbyDst[3] = pbySrc[bayerStep + 2];
				pbyDst[2] = pbySrc[bayerStep + 1];
			}
		}

		if (pbySrc < bayerEnd)
		{
			pbyDst[-blue]	= pbySrc[0];
			pbyDst[0]		= pbySrc[1];
			pbyDst[blue]	= pbySrc[bayerStep + 1];
			pbySrc++;
			pbyDst += 3;
		}

		pbySrc -= width;
		pbyDst -= width * 3;

		blue = -blue;
		start_with_green = !start_with_green;
	}
}

//=============================================================================
// Method		: BayerToRGBHQLinear
// Access		: public static  
// Returns		: void
// Parameter	: __in LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in EColorModel nColorModel
// Qualifier	:
// Last Update	: 2017/3/27 - 16:31
// Desc.		:
//=============================================================================
void CColorConvert::BayerToRGBHQLinear(__in LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enColorModel nColorModel)
{
	const int bayerStep = nWidth;
	const int rgbStep = 3 * nWidth;
	int width = nWidth;
	int height = nHeight;
	int blue = nColorModel == CM_Bayer_BGGR || nColorModel == CM_Bayer_GBRG ? -1 : 1;
	int start_with_green = nColorModel == CM_Bayer_GBRG || nColorModel == CM_Bayer_GRBG;


	ClearBorders(pbyDst, nWidth, nHeight, 2);
	pbyDst += 2 * rgbStep + 6 + 1;
	height -= 4;
	width -= 4;

	/* We begin with a (+1 line,+1 column) offset with respect to bilinear decoding, so start_with_green is the same, but blue is opposite */
	blue = -blue;

	for (; height--; pbySrc += bayerStep, pbyDst += rgbStep)
	{
		int t0, t1;
		const unsigned char *bayerEnd = pbySrc + width;
		const int bayerStep2 = bayerStep * 2;
		const int bayerStep3 = bayerStep * 3;
		const int bayerStep4 = bayerStep * 4;

		if (start_with_green)
		{
			/* at green pixel */
			pbyDst[0] = pbySrc[bayerStep2 + 2];
			t0 = pbyDst[0] * 5
				+ ((pbySrc[bayerStep + 2] + pbySrc[bayerStep3 + 2]) << 2)
				- pbySrc[2]
				- pbySrc[bayerStep + 1]
				- pbySrc[bayerStep + 3]
				- pbySrc[bayerStep3 + 1]
				- pbySrc[bayerStep3 + 3]
				- pbySrc[bayerStep4 + 2]
				+ ((pbySrc[bayerStep2] + pbySrc[bayerStep2 + 4] + 1) >> 1);
			t1 = pbyDst[0] * 5 +
				((pbySrc[bayerStep2 + 1] + pbySrc[bayerStep2 + 3]) << 2)
				- pbySrc[bayerStep2]
				- pbySrc[bayerStep + 1]
				- pbySrc[bayerStep + 3]
				- pbySrc[bayerStep3 + 1]
				- pbySrc[bayerStep3 + 3]
				- pbySrc[bayerStep2 + 4]
				+ ((pbySrc[2] + pbySrc[bayerStep4 + 2] + 1) >> 1);
			t0 = (t0 + 4) >> 3;
			//CLIP((unsigned char&)t0, pbyDst[-blue]);
			pbyDst[-blue] = CLIP8((BYTE&)t0);
			t1 = (t1 + 4) >> 3;
			//CLIP((unsigned char&)t1, pbyDst[blue]);
			pbyDst[blue] = CLIP8((BYTE&)t1);

			pbySrc++;
			pbyDst += 3;
		}

		if (blue > 0)
		{
			for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
				/* B at B */
				pbyDst[1] = pbySrc[bayerStep2 + 2];
				/* R at B */
				t0 = ((pbySrc[bayerStep + 1] + pbySrc[bayerStep + 3] + pbySrc[bayerStep3 + 1] + pbySrc[bayerStep3 + 3]) << 1)
					- (((pbySrc[2] + pbySrc[bayerStep2] + pbySrc[bayerStep2 + 4] + pbySrc[bayerStep4 + 2]) * 3 + 1) >> 1)
					+ pbyDst[1] * 6;
				/* G at B */
				t1 = ((pbySrc[bayerStep + 2] + pbySrc[bayerStep2 + 1] + pbySrc[bayerStep2 + 3] + pbySrc[bayerStep3 + 2]) << 1)
					- (pbySrc[2] + pbySrc[bayerStep2] + pbySrc[bayerStep2 + 4] + pbySrc[bayerStep4 + 2])
					+ (pbyDst[1] << 2);
				t0 = (t0 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t0, pbyDst[-1]);
				pbyDst[-1] = CLIP8((BYTE&)t0);
				t1 = (t1 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t1, pbyDst[0]);
				pbyDst[0] = CLIP8((BYTE&)t1);
				/* at green pixel */
				pbyDst[3] = pbySrc[bayerStep2 + 3];
				t0 = pbyDst[3] * 5
					+ ((pbySrc[bayerStep + 3] + pbySrc[bayerStep3 + 3]) << 2)
					- pbySrc[3]
					- pbySrc[bayerStep + 2]
					- pbySrc[bayerStep + 4]
					- pbySrc[bayerStep3 + 2]
					- pbySrc[bayerStep3 + 4]
					- pbySrc[bayerStep4 + 3]
					+
					((pbySrc[bayerStep2 + 1] + pbySrc[bayerStep2 + 5] +
					1) >> 1);
				t1 = pbyDst[3] * 5 +
					((pbySrc[bayerStep2 + 2] + pbySrc[bayerStep2 + 4]) << 2)
					- pbySrc[bayerStep2 + 1]
					- pbySrc[bayerStep + 2]
					- pbySrc[bayerStep + 4]
					- pbySrc[bayerStep3 + 2]
					- pbySrc[bayerStep3 + 4]
					- pbySrc[bayerStep2 + 5]
					+ ((pbySrc[3] + pbySrc[bayerStep4 + 3] + 1) >> 1);
				t0 = (t0 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t0, pbyDst[2]);
				pbyDst[2] = CLIP8((BYTE&)t0);
				t1 = (t1 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t1, pbyDst[4]);
				pbyDst[4] = CLIP8((BYTE&)t1);
			}
		}
		else
		{
			for (; pbySrc <= bayerEnd - 2; pbySrc += 2, pbyDst += 6)
			{
				/* R at R */
				pbyDst[-1] = pbySrc[bayerStep2 + 2];
				/* B at R */
				t0 = ((pbySrc[bayerStep + 1] + pbySrc[bayerStep + 3] +
					pbySrc[bayerStep3 + 1] + pbySrc[bayerStep3 + 3]) << 1)
					-
					(((pbySrc[2] + pbySrc[bayerStep2] +
					pbySrc[bayerStep2 + 4] + pbySrc[bayerStep4 +
					2]) * 3 + 1) >> 1)
					+ pbyDst[-1] * 6;
				/* G at R */
				t1 = ((pbySrc[bayerStep + 2] + pbySrc[bayerStep2 + 1] +
					pbySrc[bayerStep2 + 3] + pbySrc[bayerStep * 3 +
					2]) << 1)
					- (pbySrc[2] + pbySrc[bayerStep2] +
					pbySrc[bayerStep2 + 4] + pbySrc[bayerStep4 + 2])
					+ (pbyDst[-1] << 2);
				t0 = (t0 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t0, pbyDst[1]);
				pbyDst[1] = CLIP8((BYTE&)t0);
				t1 = (t1 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t1, pbyDst[0]);
				pbyDst[0] = CLIP8((BYTE&)t1);

				/* at green pixel */
				pbyDst[3] = pbySrc[bayerStep2 + 3];
				t0 = pbyDst[3] * 5
					+ ((pbySrc[bayerStep + 3] + pbySrc[bayerStep3 + 3]) << 2)
					- pbySrc[3]
					- pbySrc[bayerStep + 2]
					- pbySrc[bayerStep + 4]
					- pbySrc[bayerStep3 + 2]
					- pbySrc[bayerStep3 + 4]
					- pbySrc[bayerStep4 + 3]
					+
					((pbySrc[bayerStep2 + 1] + pbySrc[bayerStep2 + 5] +
					1) >> 1);
				t1 = pbyDst[3] * 5 +
					((pbySrc[bayerStep2 + 2] + pbySrc[bayerStep2 + 4]) << 2)
					- pbySrc[bayerStep2 + 1]
					- pbySrc[bayerStep + 2]
					- pbySrc[bayerStep + 4]
					- pbySrc[bayerStep3 + 2]
					- pbySrc[bayerStep3 + 4]
					- pbySrc[bayerStep2 + 5]
					+ ((pbySrc[3] + pbySrc[bayerStep4 + 3] + 1) >> 1);
				t0 = (t0 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t0, pbyDst[4]);
				pbyDst[4] = CLIP8((BYTE&)t0);
				t1 = (t1 + 4) >> 3;
				//CLIP<unsigned char>((unsigned char&)t1, pbyDst[2]);
				pbyDst[2] = CLIP8((BYTE&)t1);
			}
		}

		if (pbySrc < bayerEnd)
		{
			/* B at B */
			pbyDst[blue] = pbySrc[bayerStep2 + 2];
			/* R at B */
			t0 = ((pbySrc[bayerStep + 1] + pbySrc[bayerStep + 3] +
				pbySrc[bayerStep3 + 1] + pbySrc[bayerStep3 + 3]) << 1)
				-
				(((pbySrc[2] + pbySrc[bayerStep2] +
				pbySrc[bayerStep2 + 4] + pbySrc[bayerStep4 +
				2]) * 3 + 1) >> 1)
				+ pbyDst[blue] * 6;
			/* G at B */
			t1 = (((pbySrc[bayerStep + 2] + pbySrc[bayerStep2 + 1] +
				pbySrc[bayerStep2 + 3] + pbySrc[bayerStep3 + 2])) << 1)
				- (pbySrc[2] + pbySrc[bayerStep2] +
				pbySrc[bayerStep2 + 4] + pbySrc[bayerStep4 + 2])
				+ (pbyDst[blue] << 2);
			t0 = (t0 + 4) >> 3;
			//CLIP<unsigned char>((unsigned char&)t0, pbyDst[-blue]);
			pbyDst[-blue] = CLIP8((BYTE&)t0);
			t1 = (t1 + 4) >> 3;
			//CLIP<unsigned char>((unsigned char&)t1, pbyDst[0]);
			pbyDst[0] = CLIP8((BYTE&)t1);

			pbySrc++;
			pbyDst += 3;
		}

		pbySrc -= width;
		pbyDst -= width * 3;

		blue = -blue;
		start_with_green = !start_with_green;
	}
}

//=============================================================================
// Method		: BayerToRGB_
// Access		: public  
// Returns		: int
// Parameter	: __in EColorModel nColorModel
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/28 - 13:36
// Desc.		:
//=============================================================================
int CColorConvert::BayerToRGB24(__in enColorModel nColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	LPBYTE pTarget = pbyDst;
	LPBYTE pSource = pbySrc;
	UINT nDstWidth = nWidth;
	UINT nChCount  = RGB24_SIZE;

	int Mask[8];
	BYTE R = 0, G = 0, B = 0;
	unsigned int nSrcSkip = nWidth;	

	Mask[0] = -(int(nWidth) + 1);
	Mask[1] = -int(nWidth);
	Mask[2] = -(int(nWidth) - 1);
	Mask[3] = -1;
	Mask[4] = +1;
	Mask[5] = int(nWidth) - 1;
	Mask[6] = +int(nWidth);
	Mask[7] = int(nWidth) + 1;

	unsigned int nOffset_X = 0;
	unsigned int nOffset_Y = 0;
	
	switch (nColorModel)
	{
	case CM_Bayer_RGGB:
		// first pixel in first line
		R = *pSource;
		G = (*(pSource + nSrcSkip) + *(pSource + 1)) / 2;
		B = *(pSource + nSrcSkip + 1);
		break;

	case CM_Bayer_GBRG:
		nOffset_Y = 1;
		// first pixel in first line
		G = *pSource;
		R = *(pSource + nSrcSkip);
		B = *(pSource + 1);
		break;

	case CM_Bayer_GRBG:
		nOffset_X = 1;
		// first pixel in first line
		G = *pSource;
		B = *(pSource + nSrcSkip);
		R = *(pSource + 1);
		break;

	case CM_Bayer_BGGR:
		nOffset_X = 1; nOffset_Y = 1;
		// first pixel in first line
		B = *pSource;
		G = (*(pSource + nSrcSkip) + *(pSource + 1)) / 2;
		R = *(pSource + nSrcSkip + 1);
		break;

	default:;
	}

	pTarget[0] = R; 
	pTarget[1] = G; 
	pTarget[2] = B;
	pTarget += 3; 
	pSource++;

	// first line
	if (0 == nOffset_Y)
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = (*(pSource + Mask[5]) + *(pSource + Mask[7])) / 2;
			}
			else
			{ // staying on Green
				R = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
				G = *pSource;
				B = (*(pSource + Mask[6]));
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;
			pSource++;
		}
	}
	else
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[6]));
				G = *pSource;
				B = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[5]) + *(pSource + Mask[7])) / 2;
				G = (*(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = *pSource;
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;
			pSource++;
		}
	}
	// last pixel in first line
	if (0 == nOffset_Y)
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Red
			R = *pSource;
			G = (*(pSource + Mask[3]) + *(pSource + Mask[6])) / 2;
			B = *(pSource + Mask[5]);
		}
		else
		{ // staying on Green
			R = *(pSource + Mask[3]);
			G = *pSource;
			B = (*(pSource + Mask[6]));
		}
	}
	else
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Green
			R = (*(pSource + Mask[6]));
			G = *pSource;
			B = *(pSource + Mask[3]);
		}
		else
		{ // staying on Blue
			R = *(pSource + Mask[5]);
			G = (*(pSource + Mask[3]) + *(pSource + Mask[6])) / 2;
			B = *pSource;
		}
	}
	*pTarget++ = R;
	*pTarget++ = G;
	*pTarget++ = B;
	pSource++;

	//loop
	for (unsigned int y = 1; y < (unsigned int)nHeight - 1; y++)
	{
		// first pixel in line
		if (y % 2 == nOffset_Y)
		{
			if (0 % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = (*(pSource + Mask[2]) + *(pSource + Mask[7])) / 2;
			}
			else
			{ // staying on Green
				R = *(pSource + Mask[4]);
				G = *pSource;
				B = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
			}
		}
		else
		{
			if (0 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
				G = *pSource;
				B = *(pSource + Mask[4]);
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[0]) + *(pSource + Mask[7])) / 2;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = *pSource;
			}
		}

		*pTarget++ = R;
		*pTarget++ = G;
		*pTarget++ = B;

		pSource++;

		// main part of line
		for (unsigned int x = 1; x < (unsigned int)nWidth - 1; x++)
		{
			if (y % 2 == nOffset_Y)
			{
				if (x % 2 == nOffset_X)
				{  // staying on Red
					R = *pSource;
					G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 4;
					B = (*(pSource + Mask[0]) + *(pSource + Mask[2]) + *(pSource + Mask[5]) + *(pSource + Mask[7])) / 4;
				}
				else
				{ // staying on Green
					R = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
					G = *pSource;
					B = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
				}
			}
			else
			{
				if (x % 2 == nOffset_X)
				{  // staying on Green
					R = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
					G = *pSource;
					B = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
				}
				else
				{ // staying on Blue
					R = (*(pSource + Mask[0]) + *(pSource + Mask[2]) + *(pSource + Mask[5]) + *(pSource + Mask[7])) / 4;
					G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 4;
					B = *pSource;
				}
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;

			pSource++;
		}

		// last pixel in line
		if (y % 2 == nOffset_Y)
		{
			if ((nSrcSkip - 1) % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[6])) / 3;
				B = (*(pSource + Mask[0]) + *(pSource + Mask[5])) / 2;
			}
			else
			{ // staying on Green
				R = *(pSource + Mask[3]);
				G = *pSource;
				B = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
			}
		}
		else
		{
			if ((nSrcSkip - 1) % 2 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
				G = *pSource;
				B = *(pSource + Mask[3]);
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[0]) + *(pSource + Mask[5])) / 2;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[6])) / 3;
				B = *pSource;
			}
		}

		*pTarget++ = R;
		*pTarget++ = G;
		*pTarget++ = B;

		pSource++;
	}
	
	// first pixel in last line
	if ((nHeight - 1) % 2 == nOffset_Y)
	{
		if (0 == nOffset_X)
		{  // staying on Red
			R = *pSource;
			G = (*(pSource + Mask[1]) + *(pSource + Mask[4])) / 2;
			B = *(pSource + Mask[2]);
		}
		else
		{ // staying on Green
			R = *(pSource + Mask[4]);
			G = *pSource;
			B = (*(pSource + Mask[1]));
		}
	}
	else
	{
		if (0 == nOffset_X)
		{  // staying on Green
			R = (*(pSource + Mask[1]));
			G = *pSource;
			B = *(pSource + Mask[4]);
		}
		else
		{ // staying on Blue
			R = *(pSource + Mask[2]);
			G = (*(pSource + Mask[1]) + *(pSource + Mask[4])) / 2;
			B = *pSource;
		}
	}

	*pTarget++ = R;
	*pTarget++ = G;
	*pTarget++ = B;

	pSource++;
	
	// last line
	if ((nHeight - 1) % 2 == nOffset_Y)
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[3]) + *(pSource + Mask[1]) + *(pSource + Mask[4])) / 3;
				B = (*(pSource + Mask[0]) + *(pSource + Mask[2])) / 2;
			}
			else
			{ // staying on Green
				R = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
				G = *pSource;
				B = (*(pSource + Mask[1]));
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;

			pSource++;
		}
	}
	else
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[1]));
				G = *pSource;
				B = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[0]) + *(pSource + Mask[2])) / 2;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[4])) / 3;
				B = *pSource;
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;

			pSource++;
		}
	}

	// last pixel in last line
	if ((nHeight - 1) % 2 == nOffset_Y)
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Red
			R = *pSource;
			G = (*(pSource + Mask[3]) + *(pSource + Mask[1])) / 2;
			B = *(pSource + Mask[0]);
		}
		else
		{ // staying on Green
			R = *(pSource + Mask[3]);
			G = *pSource;
			B = (*(pSource + Mask[1]));
		}
	}
	else 
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Green
			R = (*(pSource + Mask[1]));
			G = *pSource;
			B = *(pSource + Mask[3]);
		}
		else
		{ // staying on Blue
			R = *(pSource + Mask[0]);
			G = (*(pSource + Mask[3]) + *(pSource + Mask[1])) / 2;
			B = *pSource;
		}
	}

	*pTarget++ = R;
	*pTarget++ = G;
	*pTarget++ = B;

	pSource++;


	return 0;
}

//=============================================================================
// Method		: BayerToRGB32
// Access		: public  
// Returns		: int
// Parameter	: __in EColorModel nColorModel
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/28 - 15:05
// Desc.		:
//=============================================================================
int CColorConvert::BayerToRGB32(__in enColorModel nColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	LPBYTE pTarget = pbyDst;
	LPBYTE pSource = pbySrc;
	UINT nDstWidth = nWidth;
	UINT nChCount = RGB32_SIZE;

	int Mask[8];
	BYTE R = 0, G = 0, B = 0;
	unsigned int nSrcSkip = nWidth;

	Mask[0] = -(int(nWidth) + 1);
	Mask[1] = -int(nWidth);
	Mask[2] = -(int(nWidth) - 1);
	Mask[3] = -1;
	Mask[4] = +1;
	Mask[5] = int(nWidth) - 1;
	Mask[6] = +int(nWidth);
	Mask[7] = int(nWidth) + 1;

	unsigned int nOffset_X = 0;
	unsigned int nOffset_Y = 0;

	switch (nColorModel)
	{
	case CM_Bayer_RGGB:
		// first pixel in first line
		R = *pSource;
		G = (*(pSource + nSrcSkip) + *(pSource + 1)) / 2;
		B = *(pSource + nSrcSkip + 1);
		break;

	case CM_Bayer_GBRG:
		nOffset_Y = 1;
		// first pixel in first line
		G = *pSource;
		R = *(pSource + nSrcSkip);
		B = *(pSource + 1);
		break;

	case CM_Bayer_GRBG:
		nOffset_X = 1;
		// first pixel in first line
		G = *pSource;
		B = *(pSource + nSrcSkip);
		R = *(pSource + 1);
		break;

	case CM_Bayer_BGGR:
		nOffset_X = 1; nOffset_Y = 1;
		// first pixel in first line
		B = *pSource;
		G = (*(pSource + nSrcSkip) + *(pSource + 1)) / 2;
		R = *(pSource + nSrcSkip + 1);
		break;

	default:;
	}

	pTarget[0] = R;
	pTarget[1] = G;
	pTarget[2] = B;
	pTarget[3] = 0xFF;
	pTarget += RGB32_SIZE;
	pSource++;

	// first line
	if (0 == nOffset_Y)
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = (*(pSource + Mask[5]) + *(pSource + Mask[7])) / 2;
			}
			else
			{ // staying on Green
				R = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
				G = *pSource;
				B = (*(pSource + Mask[6]));
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;
			*pTarget++ = 0xFF;
			pSource++;
		}
	}
	else
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[6]));
				G = *pSource;
				B = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[5]) + *(pSource + Mask[7])) / 2;
				G = (*(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = *pSource;
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;
			*pTarget++ = 0xFF;
			pSource++;
		}
	}
	// last pixel in first line
	if (0 == nOffset_Y)
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Red
			R = *pSource;
			G = (*(pSource + Mask[3]) + *(pSource + Mask[6])) / 2;
			B = *(pSource + Mask[5]);
		}
		else
		{ // staying on Green
			R = *(pSource + Mask[3]);
			G = *pSource;
			B = (*(pSource + Mask[6]));
		}
	}
	else
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Green
			R = (*(pSource + Mask[6]));
			G = *pSource;
			B = *(pSource + Mask[3]);
		}
		else
		{ // staying on Blue
			R = *(pSource + Mask[5]);
			G = (*(pSource + Mask[3]) + *(pSource + Mask[6])) / 2;
			B = *pSource;
		}
	}
	*pTarget++ = R;
	*pTarget++ = G;
	*pTarget++ = B;
	*pTarget++ = 0xFF;
	pSource++;

	//loop
	for (unsigned int y = 1; y < (unsigned int)nHeight - 1; y++)
	{
		// first pixel in line
		if (y % 2 == nOffset_Y)
		{
			if (0 % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = (*(pSource + Mask[2]) + *(pSource + Mask[7])) / 2;
			}
			else
			{ // staying on Green
				R = *(pSource + Mask[4]);
				G = *pSource;
				B = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
			}
		}
		else
		{
			if (0 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
				G = *pSource;
				B = *(pSource + Mask[4]);
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[0]) + *(pSource + Mask[7])) / 2;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 3;
				B = *pSource;
			}
		}

		*pTarget++ = R;
		*pTarget++ = G;
		*pTarget++ = B;
		*pTarget++ = 0xFF;
		pSource++;

		// main part of line
		for (unsigned int x = 1; x < (unsigned int)nWidth - 1; x++)
		{
			if (y % 2 == nOffset_Y)
			{
				if (x % 2 == nOffset_X)
				{  // staying on Red
					R = *pSource;
					G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 4;
					B = (*(pSource + Mask[0]) + *(pSource + Mask[2]) + *(pSource + Mask[5]) + *(pSource + Mask[7])) / 4;
				}
				else
				{ // staying on Green
					R = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
					G = *pSource;
					B = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
				}
			}
			else
			{
				if (x % 2 == nOffset_X)
				{  // staying on Green
					R = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
					G = *pSource;
					B = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
				}
				else
				{ // staying on Blue
					R = (*(pSource + Mask[0]) + *(pSource + Mask[2]) + *(pSource + Mask[5]) + *(pSource + Mask[7])) / 4;
					G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[4]) + *(pSource + Mask[6])) / 4;
					B = *pSource;
				}
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;
			*pTarget++ = 0xFF;
			pSource++;
		}

		// last pixel in line
		if (y % 2 == nOffset_Y)
		{
			if ((nSrcSkip - 1) % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[6])) / 3;
				B = (*(pSource + Mask[0]) + *(pSource + Mask[5])) / 2;
			}
			else
			{ // staying on Green
				R = *(pSource + Mask[3]);
				G = *pSource;
				B = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
			}
		}
		else
		{
			if ((nSrcSkip - 1) % 2 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[1]) + *(pSource + Mask[6])) / 2;
				G = *pSource;
				B = *(pSource + Mask[3]);
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[0]) + *(pSource + Mask[5])) / 2;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[6])) / 3;
				B = *pSource;
			}
		}

		*pTarget++ = R;
		*pTarget++ = G;
		*pTarget++ = B;
		*pTarget++ = 0xFF;
		pSource++;
	}

	// first pixel in last line
	if ((nHeight - 1) % 2 == nOffset_Y)
	{
		if (0 == nOffset_X)
		{  // staying on Red
			R = *pSource;
			G = (*(pSource + Mask[1]) + *(pSource + Mask[4])) / 2;
			B = *(pSource + Mask[2]);
		}
		else
		{ // staying on Green
			R = *(pSource + Mask[4]);
			G = *pSource;
			B = (*(pSource + Mask[1]));
		}
	}
	else
	{
		if (0 == nOffset_X)
		{  // staying on Green
			R = (*(pSource + Mask[1]));
			G = *pSource;
			B = *(pSource + Mask[4]);
		}
		else
		{ // staying on Blue
			R = *(pSource + Mask[2]);
			G = (*(pSource + Mask[1]) + *(pSource + Mask[4])) / 2;
			B = *pSource;
		}
	}

	*pTarget++ = R;
	*pTarget++ = G;
	*pTarget++ = B;
	*pTarget++ = 0xFF;
	pSource++;

	// last line
	if ((nHeight - 1) % 2 == nOffset_Y)
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Red
				R = *pSource;
				G = (*(pSource + Mask[3]) + *(pSource + Mask[1]) + *(pSource + Mask[4])) / 3;
				B = (*(pSource + Mask[0]) + *(pSource + Mask[2])) / 2;
			}
			else
			{ // staying on Green
				R = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
				G = *pSource;
				B = (*(pSource + Mask[1]));
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;
			*pTarget++ = 0xFF;
			pSource++;
		}
	}
	else
	{
		for (unsigned int x = 1; x < nDstWidth - 1; x++)
		{
			if (x % 2 == nOffset_X)
			{  // staying on Green
				R = (*(pSource + Mask[1]));
				G = *pSource;
				B = (*(pSource + Mask[3]) + *(pSource + Mask[4])) / 2;
			}
			else
			{ // staying on Blue
				R = (*(pSource + Mask[0]) + *(pSource + Mask[2])) / 2;
				G = (*(pSource + Mask[1]) + *(pSource + Mask[3]) + *(pSource + Mask[4])) / 3;
				B = *pSource;
			}

			*pTarget++ = R;
			*pTarget++ = G;
			*pTarget++ = B;
			*pTarget++ = 0xFF;
			pSource++;
		}
	}

	// last pixel in last line
	if ((nHeight - 1) % 2 == nOffset_Y)
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Red
			R = *pSource;
			G = (*(pSource + Mask[3]) + *(pSource + Mask[1])) / 2;
			B = *(pSource + Mask[0]);
		}
		else
		{ // staying on Green
			R = *(pSource + Mask[3]);
			G = *pSource;
			B = (*(pSource + Mask[1]));
		}
	}
	else
	{
		if ((nDstWidth - 1) % 2 == nOffset_X)
		{  // staying on Green
			R = (*(pSource + Mask[1]));
			G = *pSource;
			B = *(pSource + Mask[3]);
		}
		else
		{ // staying on Blue
			R = *(pSource + Mask[0]);
			G = (*(pSource + Mask[3]) + *(pSource + Mask[1])) / 2;
			B = *pSource;
		}
	}

	*pTarget++ = R;
	*pTarget++ = G;
	*pTarget++ = B;
	*pTarget++ = 0xFF;
	pSource++;


	return 0;
}

//=============================================================================
// Method		: BayerToRGB
// Access		: public  
// Returns		: void
// Parameter	: __in EColorModel nColorModel
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Parameter	: __in BayerDemosaicMethod nMethod
// Parameter	: __in BOOL bFlip
// Qualifier	:
// Last Update	: 2017/3/27 - 16:51
// Desc.		:
//=============================================================================
void CColorConvert::BayerToRGB(__in enColorModel nColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst, __in enBayerDemosaicMethod nMethod, __in BOOL bFlip)
{
	switch (nMethod)
	{
	case BAYER_DEMOSAIC_METHOD_SIMPLE:
		BayerToRGBSimple(pbySrc, nWidth, nHeight, pbyDst, nColorModel);
		break;

	case BAYER_DEMOSAIC_METHOD_NEAREST:
		BayerToRGBNearestNeighbour(pbySrc, nWidth, nHeight, pbyDst, nColorModel);
		break;

	case BAYER_DEMOSAIC_METHOD_BILINEAR:
		BayerToRGBBilinear(pbySrc, nWidth, nHeight, pbyDst, nColorModel);
		break;

	case BAYER_DEMOSAIC_METHOD_HQLINEAR:
		BayerToRGBHQLinear(pbySrc, nWidth, nHeight, pbyDst, nColorModel);
		break;

	case BAYER_DEMOSAIC_METHOD_AHD:
		//BIASERR("NOT IMPLEMENTED");
		break;

	case BAYER_DEMOSAIC_METHOD_VNG:
		//BIASERR("NOT IMPLEMENTED YET");
		break;

	default:
		//BIASERR("NO DEFAULT OPERATION");
		break;
	}

	if (bFlip)
	{
		//dest.FlipHorizontal();
	}
}

//=============================================================================
// Method		: Grey16ToRGB24
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPWORD pbySrc
// Parameter	: __in UINT nPixelCount
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/6/13 - 19:52
// Desc.		:	Gray16bit 표현 범위 (0 ~ 4095)
//					Gray8bit 표현 범위 (0 ~ 255)
//=============================================================================
void CColorConvert::Grey16ToRGB24(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst)
{
	register LPWORD src_start = pbySrc;
	register const LPWORD src_stop = src_start + nPixelCount;
	register LPBYTE pDest = pbyDst;

	unsigned short value16 = 0;
	for (; src_start < src_stop; src_start++)
	{
		value16 = *src_start / 16; // 16 == (4096 / 256)

		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
	}
}

void CColorConvert::Grey16ToRGB32(__in const LPWORD pbySrc, __in UINT nPixelCount, __out LPBYTE pbyDst)
{
	register LPWORD src_start = pbySrc;
	register const LPWORD src_stop = src_start + nPixelCount;
	register LPBYTE pDest = pbyDst;

	unsigned short value16 = 0;
	for (; src_start < src_stop; src_start++)
	{
		value16 = *src_start / 16;

		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
		*(pDest++) = (BYTE)(value16);
		*(pDest++) = 0x00;
	}
}

//=============================================================================
// Method		: Grey16ToRGB24_CV
// Access		: public static  
// Returns		: void
// Parameter	: __in const LPWORD pbySrc
// Parameter	: __in UINT nHeight
// Parameter	: __in UINT nWidth
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/6/13 - 18:55
// Desc.		:
//=============================================================================
void CColorConvert::Grey16ToRGB24_CV(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst)
{
	// 	cv::Mat Bit16Mat(nHeight, nWidth, CV_16UC1, pbySrc);
	// 	Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.0625);
	// 
	// 	cv::Mat rgb8BitMat(nHeight, nWidth, CV_8UC3, pbyDst);
	// 	cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);
}

void CColorConvert::Grey16ToRGB32_CV(__in const LPWORD pbySrc, __in UINT nHeight, __in UINT nWidth, __out LPBYTE pbyDst)
{
	// 	cv::Mat Bit16Mat(nHeight, nWidth, CV_16UC1, pbySrc);
	// 	Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.0625);
	// 
	// 	cv::Mat rgb8BitMat(nHeight, nWidth, CV_8UC4, pbyDst);
	// 	cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);
}

//=============================================================================
// Method		: ConvertToRGB32
// Access		: public  
// Returns		: void
// Parameter	: __in EColorModel nSrcColorModel
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/3/27 - 17:08
// Desc.		:
//=============================================================================
void CColorConvert::ConvertToRGB32(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	switch (nSrcColorModel)
	{
	case CM_Grey:
		//GreyToRGB_(pbySrc, pbyDst);
		break;
	case CM_BGR:
		//BGRToRGB_(pbySrc, pbyDst);
		break;
	case CM_RGB:
		//pbyDst = pbySrc;		
		break;
	case CM_HSL:
		//HSLToRGB_(pbySrc, pbyDst);
		break;

	case CM_UYVY422:
		YUV422ToRGB32(pbySrc, nWidth, nHeight, pbyDst);
		break;

	case CM_YUYV422:
		YUV422ToRGB32(pbySrc, nWidth, nHeight, pbyDst, FALSE);
		break;

	case CM_YUV420P:
		YUV420pToRGB32(pbySrc, nWidth, nHeight, pbyDst);
		break;

	case CM_HSV:
		//HSVToRGB_(pbySrc, pbyDst);
		break;

	case CM_Bayer_RGGB:
	case CM_Bayer_GBRG:
	case CM_Bayer_GRBG:
	case CM_Bayer_BGGR:
		BayerToRGB32(nSrcColorModel, pbySrc, nWidth, nHeight, pbyDst);
		break;

	case CM_RGBA:
		//RGBAToRGB_(pbySrc, pbyDst);
		break;
	case CM_BGRA:
		//BGRAToRGB_(pbySrc, pbyDst);
		break;

	default:
		break;
	}
}

void CColorConvert::ConvertToRGB32(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPRGBQUAD pbyDst)
{
	ConvertToRGB32(nSrcColorModel, pbySrc, nWidth, nHeight, (LPBYTE)pbyDst);
}

//=============================================================================
// Method		: ConvertToRGB24
// Access		: public  
// Returns		: void
// Parameter	: __in enColorModel nSrcColorModel
// Parameter	: __in const LPBYTE pbySrc
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Parameter	: __out LPBYTE pbyDst
// Qualifier	:
// Last Update	: 2017/4/3 - 13:19
// Desc.		:
//=============================================================================
void CColorConvert::ConvertToRGB24(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPBYTE pbyDst)
{
	switch (nSrcColorModel)
	{
	case CM_Grey:
		//GreyToRGB_(pbySrc, pbyDst);
		break;
	case CM_BGR:
		//BGRToRGB_(pbySrc, pbyDst);
		break;
	case CM_RGB:
		//pbyDst = pbySrc;		
		break;
	case CM_HSL:
		//HSLToRGB_(pbySrc, pbyDst);
		break;

	case CM_UYVY422:
		YUV422ToRGB24(pbySrc, nWidth, nHeight, pbyDst);
		break;

	case CM_YUYV422:
		YUV422ToRGB24(pbySrc, nWidth, nHeight, pbyDst, FALSE);
		break;

	case CM_YUV420P:
		YUV420pToRGB24(pbySrc, nWidth, nHeight, pbyDst);
		break;

	case CM_HSV:
		//HSVToRGB_(pbySrc, pbyDst);
		break;

	case CM_Bayer_RGGB:
	case CM_Bayer_GBRG:
	case CM_Bayer_GRBG:
	case CM_Bayer_BGGR:
		BayerToRGB24(nSrcColorModel, pbySrc, nWidth, nHeight, pbyDst);
		break;

	case CM_RGBA:
		//RGBAToRGB_(pbySrc, pbyDst);
		break;
	case CM_BGRA:
		//BGRAToRGB_(pbySrc, pbyDst);
		break;

	default:
		break;
	}
}

void CColorConvert::ConvertToRGB24(__in enColorModel nSrcColorModel, __in const LPBYTE pbySrc, __in UINT nWidth, __in UINT nHeight, __out LPRGBTRIPLE pbyDst)
{
	ConvertToRGB24(nSrcColorModel, pbySrc, nWidth, nHeight, (LPBYTE)pbyDst);
}
