﻿//*****************************************************************************
// Filename	: 	I2C_Script.cpp
// Created	:	2017/9/4 - 11:45
// Modified	:	2017/9/4 - 11:45
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "I2C_Script.h"


CI2C_Script::CI2C_Script()
{
}


CI2C_Script::~CI2C_Script()
{
}

//=============================================================================
// Method		: Reset_I2CList
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/11 - 16:53
// Desc.		:
//=============================================================================
void CI2C_Script::Reset_I2CList()
{
	m_arI2CList.RemoveAll();
}

//=============================================================================
// Method		: Load_I2CFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFileFullPath
// Qualifier	:
// Last Update	: 2017/9/11 - 19:27
// Desc.		:
//=============================================================================
BOOL CI2C_Script::Load_I2CFile(__in LPCTSTR szFileFullPath)
{
	CStdioFile	fileI2C;
	BOOL		bLoop		= TRUE;
	CString		szLine;
	CString		szTemp;
	int			iSecondIdx	= 0;
	WORD		wTemp		= 0; 

	USHORT nShort = 0xfeff;  // 유니코드 바이트 오더마크

	// 리스트 초기화
	Reset_I2CList();

	try
	{
		if (!fileI2C.Open(szFileFullPath, CFile::modeRead | CFile::typeText))
		{
			TRACE(_T("I2C Setting File을 찾을 수 없습니다.\n"));
			return FALSE;
		}

		//fileI2C.Seek(2, CFile::begin); // 유니코드 BOM 건너 뜀

		do 
		{
			bLoop = fileI2C.ReadString(szLine);

			if (bLoop)
			{
				if ((szLine.IsEmpty()) || (szLine.GetAt(0) == '/'))
					continue;
				
				szLine.Trim(_T(" ,\t\r\n"));
								
				szLine.Remove(_T(' '));
				szLine.Remove(_T(','));
				szLine.Remove(_T('\t'));
				szLine.Remove(_T('\r'));
				szLine.Remove(_T('\n'));

				szLine.MakeLower();

				// Slave ID
				if (0 <= szLine.Find(g_szI2C_Command[I2CCmd_SlaveID]))
				{
					szLine.Delete(0, 5);
					ST_I2CFormat	stI2C;
					stI2C.nCommand	= I2CCmd_SlaveID;
					stI2C.dwAddress = _tcstoul(szLine.GetBuffer(0), NULL, 16);
					
					m_arI2CList.Add(stI2C);
				}
				// Sleep Millisecond
				else if (0 <= szLine.Find(g_szI2C_Command[I2CCmd_Sleep]))
				{
					szLine.Delete(0, 5);
					ST_I2CFormat			stI2C;
					stI2C.nCommand			= I2CCmd_Sleep;
					stI2C.dwMilliseconds	= _ttoi(szLine.GetBuffer(0));

					m_arI2CList.Add(stI2C);
				}
				// Write Address Data
				else if (0 <= szLine.Find(g_szI2C_Command[I2CCmd_Write]))
				{
					iSecondIdx = szLine.Find(g_szI2C_Command[I2CCmd_Write], 2);
					szTemp = szLine.Mid(0, iSecondIdx);

					ST_I2CFormat		 stI2C;
					stI2C.nCommand		 = I2CCmd_Write;
					stI2C.dwAddrLen		 = (szTemp.GetLength() - 2) / 2;
					stI2C.dwAddress		 = _tcstoul(szTemp.GetBuffer(0), NULL, 16);

					szTemp = szLine.Mid(iSecondIdx);

					if (szTemp.GetLength() <= 4) // 0x00 형태
					{
						stI2C.dwDataCount = 1; // "0x"

						stI2C.byDataz[0] = (BYTE)_tcstoul(szTemp.GetBuffer(0), NULL, 16);
					}
					else // 0x0000 형태
					{
						stI2C.dwDataCount = 2; // "0x"

						wTemp = (WORD)_tcstoul(szTemp.GetBuffer(0), NULL, 16);
						stI2C.byDataz[0] = HIBYTE(wTemp);
						stI2C.byDataz[1] = LOBYTE(wTemp);
					}

					m_arI2CList.Add(stI2C);
				}
				// 주석
				else
				{

				}
			}
		} 
		while (bLoop);

		TRACE(_T("I2C File (%s) Readed %d Items\n"), szFileFullPath, m_arI2CList.GetCount());

		fileI2C.Close();
	}
	catch (CFileException* pe)
	{
		TRACE(_T("File could not be opened, cause = %d\n"), pe->m_cause);
		pe->Delete();
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_I2CFile
// Access		: public  
// Returns		: ST_I2CFormat*
// Parameter	: __in LPCTSTR szFileFullPath
// Parameter	: __out UINT & nItemCount
// Qualifier	:
// Last Update	: 2017/9/11 - 20:20
// Desc.		:
//=============================================================================
ST_I2CFormat* CI2C_Script::Load_I2CFile(__in LPCTSTR szFileFullPath,  __out UINT& nItemCount)
{
	if (Load_I2CFile(szFileFullPath))
	{
		nItemCount = (UINT)m_arI2CList.GetCount();

		if (0 < nItemCount)
		{
			return m_arI2CList.GetData();
		}
	}

	return NULL;
}

//=============================================================================
// Method		: Save_I2CFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFileFullPath
// Qualifier	:
// Last Update	: 2017/9/11 - 20:20
// Desc.		:
//=============================================================================
BOOL CI2C_Script::Save_I2CFile(__in LPCTSTR szFileFullPath)
{
	if (PathFileExists(szFileFullPath))
	{
		::DeleteFile(szFileFullPath);
	}

	CStdioFile fileI2C;
	if (!fileI2C.Open(szFileFullPath, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		TRACE(_T("I2C Setting File을 찾을 수 없습니다.\n"));
		return FALSE;
	}

	UINT nItemCount = (UINT)m_arI2CList.GetCount();

	CString szTemp;
	for (UINT nIdx = 0; nIdx < nItemCount; nIdx++)
	{
		szTemp.Empty();

		switch (m_arI2CList[nIdx].nCommand)
		{
		case I2CCmd_SlaveID:
		{
			szTemp.Format(_T("Slave 0x%02X\n"), m_arI2CList[nIdx].dwAddress);
		}
		break;

		case I2CCmd_Sleep:
		{
			szTemp.Format(_T("Sleep %d\n"), m_arI2CList[nIdx].dwMilliseconds);
		}
		break;

		case I2CCmd_Write:
		{
			if (1 == m_arI2CList[nIdx].dwDataCount) // 1 Byte
			{
					szTemp.Format(_T("0x%04X 0x%02X\n"), m_arI2CList[nIdx].dwAddress, m_arI2CList[nIdx].byDataz[0]);
			}
			else // 2 Byte
			{
					szTemp.Format(_T("0x%04X 0x%02X%02X\n"), m_arI2CList[nIdx].dwAddress, m_arI2CList[nIdx].byDataz[0], m_arI2CList[nIdx].byDataz[1]);
			}
		}
		break;
		}

		if (FALSE == szTemp.IsEmpty())
		{
			fileI2C.WriteString(szTemp);
		}
	}

	fileI2C.Close();

	return TRUE;
}

//=============================================================================
// Method		: Save_I2CFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFileFullPath
// Parameter	: __in const ST_I2CFormat * pstI2CDataz
// Parameter	: __in UINT nItemCount
// Qualifier	:
// Last Update	: 2017/9/11 - 20:26
// Desc.		:
//=============================================================================
BOOL CI2C_Script::Save_I2CFile(__in LPCTSTR szFileFullPath, __in const ST_I2CFormat* pstI2CDataz, __in UINT nItemCount)
{
	if (PathFileExists(szFileFullPath))
	{
		::DeleteFile(szFileFullPath);
	}

	CStdioFile fileI2C;
	if (!fileI2C.Open(szFileFullPath, CFile::modeCreate | CFile::modeWrite | CFile::typeText))
	{
		TRACE(_T("I2C Setting File을 찾을 수 없습니다.\n"));
		return FALSE;
	}	

	CString szTemp;
	for (UINT nIdx = 0; nIdx < nItemCount; nIdx++, pstI2CDataz++)
	{
		szTemp.Empty();

		switch (pstI2CDataz->nCommand)
		{
		case I2CCmd_SlaveID:
		{
			szTemp.Format(_T("Slave 0x%02X\n"), pstI2CDataz->dwAddress);
		}
		break;

		case I2CCmd_Sleep:
		{
			szTemp.Format(_T("Sleep %d\n"), pstI2CDataz->dwMilliseconds);
		}
		break;

		case I2CCmd_Write:
		{
			if (1 == pstI2CDataz->dwDataCount) // 1 Byte
			{
				szTemp.Format(_T("0x%04X 0x%02X\n"), pstI2CDataz->dwAddress, pstI2CDataz->byDataz[0]);
			}
			else // 2 Byte
			{
				szTemp.Format(_T("0x%04X 0x%02X%02X\n"), pstI2CDataz->dwAddress, pstI2CDataz->byDataz[0], pstI2CDataz->byDataz[1]);
			}
		}
		break;
		}

		if (FALSE == szTemp.IsEmpty())
		{
			fileI2C.WriteString(szTemp);
		}
	}

	fileI2C.Close();

	return TRUE;
}

//=============================================================================
// Method		: GetI2CDataz
// Access		: public  
// Returns		: ST_I2CFormat*
// Parameter	: __out UINT & nItemCount
// Qualifier	:
// Last Update	: 2017/9/11 - 20:15
// Desc.		:
//=============================================================================
ST_I2CFormat* CI2C_Script::GetI2CDataz(__out UINT& nItemCount)
{
	nItemCount = (UINT)m_arI2CList.GetCount();

	if (0 < nItemCount)
	{
		return m_arI2CList.GetData();
	}
	else
	{
		return NULL;
	}
}
