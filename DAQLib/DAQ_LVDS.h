﻿//*****************************************************************************
// Filename	: 	DAQ_LVDS.h
// Created	:	2017/3/17 - 10:46
// Modified	:	2017/3/17 - 10:46
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef DAQ_LVDS_h__
#define DAQ_LVDS_h__

#pragma once

#include "Def_DAQWrapper.h"
#include "Def_ImageProcessing.h"
#include "ColorConvert.h"
#include "ColorSpace.h"
#include "I2C_Script.h"

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CDAQ_LVDS
{
public:
	CDAQ_LVDS();
	~CDAQ_LVDS();

protected:
	// --------------------------------------------------------------
	// 멤버 변수, 클래스
	// --------------------------------------------------------------
	int				m_nBoardNumber				= 0;		// 보드 채널 번호	
	ST_DAQOption	m_stOption;								// 보드, 영상 설정
	ST_DAQStatus	m_stStatus;								// 보드 상태
	ST_FrameData	m_stFrameBuf;							// 보드에서 수신된 프레임 데이터??
	//CColorConvert	m_ColorConv;							// 영상 포맷 변환용 클래스	
	CColorSpace		m_ColorConv;							// 영상 포맷 변환용 클래스	
	ST_IP_Option	m_stImageProcOpt;						// 영상 처리용 설정
	CI2C_Script		m_I2C_Dataz;							// I2C 파일 처리용 클래스

	DWORD			m_dwFrameSize				= 0;		// 소스 영상 데이터 크기
	LPBYTE			m_pbyFrame					= nullptr;	// 소스 영상 데이터
	void			AssignFrameMem				();			// 소스 영상 메모리 할당
	void			ReleaseFrameMem				();			// 소스 영상 메모리 해제

	// 소스 영상의 포맷
	enColorModel	m_nSrcColorModel			= CM_Bayer_BGGR;
	enBitsPerPixel	m_nBitsPerPixel				= BPP_8_bit;		// 1, 4, 8, 9, 9.5, 12, 16, 24, 32
	// 출력 영상의 포맷
	enColorModelOut	m_nOutColorModel			= CMOUT_RGBA;	

	// 영상 신호
	BOOL			m_bSignal					= FALSE;	// 영상 신호 상태
	LARGE_INTEGER	m_liComputeFPS_PC;						// 고정 FPS 계산용 변수
	LARGE_INTEGER	m_liFixFPS_PC;							// 고정 FPS 계산용 변수
	LONGLONG		m_llFixFPSStart				= 0;		// 고정 FPS 계산용 변수 (시작 시간 체크)
	LONGLONG		m_llFixFPSDuration			= 10000;	// 고정 FPS 계산용 변수 (진행 시간 체크)
	DOUBLE			m_dFixFPS_Remainder			= 0.0f;

	// I2C 처리 기능
	CString			m_szI2CFilePath;
	BOOL			m_bUseAutoI2CWrite			= TRUE;

	// --------------------------------------------------------------
	// FPS 측정용
	// --------------------------------------------------------------
	double			m_dCPU_Frequency			= 0.0;	// CPU 클럭
	__int64			m_iCounterStart				= 0;	// FPS 계산용 변수
	int				m_iFrameCount				= 0;	// FPS 계산용 변수 (프레임 수)
	inline void		StartCounter				();
	inline double	GetCounter					();
	
	// 프레인 카운트 초기화
	void			InitFrameCount				();
	// 프레임 카운트 증가, FPS 계산
	void			IncreaseFrameCount			(__in BOOL bFail = FALSE);
	// 고정 프레임 설정 체크용도
	BOOL			CheckFrameRate				();

	// --------------------------------------------------------------
	// 시간 체크용
	// --------------------------------------------------------------
	//double			m_dCPU_Frequency		= 0.0;		// CPU 클럭
	LARGE_INTEGER	m_liPC;									// 계산용 변수
	LONGLONG		m_llStart					= 0;		// 계산용 변수 (시작 시간 체크)
	LONGLONG		m_llDuration				= 10000;	// 계산용 변수 (진행 시간 체크)
	inline void		StartTickCounter();
	inline double	GetTickCounter();

	// --------------------------------------------------------------
	// 타이머 (영상 출력 상태 체크)
	// --------------------------------------------------------------
	HANDLE			m_hTimer_Status				= nullptr;
	HANDLE			m_hTimerQueue				= nullptr;
	DWORD			m_dwCycle_Status			= 100;		// 100ms 마다 영상신호와 H/W FPS 체크
	BOOL			m_bThrGetFrameFlag			= FALSE;	// 영상 캡쳐 관련 Loop 플래그
	void			CreateTimerQueue_Mon		();
	void			DeleteTimerQueue_Mon		();
	void			CreateTimer_Status			();
	void			DeleteTimer_Status			();
	static VOID CALLBACK	TimerRoutine_Status	(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);
	void			Monitor_Status				();

	// --------------------------------------------------------------
	// 쓰레드 (영상 수신 프레임 데이터 처리)
	// --------------------------------------------------------------
	HANDLE			m_hThr_Frame				= nullptr;
	static UINT WINAPI	Thread_Frame			(__in LPVOID lParam);
	void			Monitor_Frame				();
	BOOL			StartThread_Frame			();
	BOOL			StopThread_Frame			();

	//---------------------------------------------------------------
	// 윈도우 메세지 처리용
	//---------------------------------------------------------------
	HWND			m_hWndOwner					= nullptr;	// 오너 윈도우 핸들
	UINT			m_nWM_ChgStatus				= NULL;		// 카메라 영상 상태 변경시 처리 메세지
	UINT			m_nWM_RecvVideo				= NULL;		// 비데오 영상 수신 메세지
	inline void		NotifyReceiveVideo			();			// 영상 수신 이벤트 통보
	inline void		NotifyFailReceiveVideo		();			// 영상 수신 실패 이벤트 통보
	inline void		NotifyChangeStatus			();			// 영상 상태 변화 통보

	// --------------------------------------------------------------
	// 콜백 함수 처리용
	// --------------------------------------------------------------
	Callback_RecvFrame		m_pfnRecvFrame		= nullptr;	// 수신된 영상 출력 처리용 콜백함수 포인터
	PVOID					m_pvExtRecvFrame	= nullptr;	// 콜백함수 리턴 포인터
	void			SetCallbackFn_RecvFrame		(__in Callback_RecvFrame FnCallback, __in PVOID lpParam)
	{
		m_pfnRecvFrame	 = FnCallback;
		m_pvExtRecvFrame = lpParam;
	};
	Callback_ChgStatus		m_pfnChgStatus		= nullptr;	// 수신된 영상 출력 처리용 콜백함수 포인터
	PVOID					m_pvExtChgStatus	= nullptr;	// 콜백함수 리턴 포인터
	void			SetCallbackFn_ChgStatus		(__in Callback_ChgStatus FnCallback, __in PVOID lpParam)
	{
		m_pfnChgStatus	 = FnCallback;
		m_pvExtChgStatus = lpParam;
	};
	
	// --------------------------------------------------------------
	// 기타 운영 함수들
	// --------------------------------------------------------------

	// 해상도 기반으로 프레임 버퍼의 크기 할당
	virtual inline void		CheckFrameSize			();

	// 해상도 구하기 (설정에 따라서 조정 처리)
	virtual inline BOOL		GetResolutionAndAdjust	();

	// 영상 포맷 변환 (ex. YUV -> RGB)
	virtual inline void		ConvertImageFormat		(__in LPBYTE lpbyFrame);

	// 영상 플립, 회전, 반전 등의 후처리 작업 수행
	virtual inline void		Apply_ImageProcessing	();
	
	// MIPI I2C 설정 
	virtual inline BOOL		Write_MIPI_I2C			(__in enDAQMIPILane byMIPILane);

public:

	//------------------------------------------------------------------
	// DAQ 보드 제어용 함수
	//------------------------------------------------------------------
	// 
	// 보드 번호 설정
	void		SetBoardNumber		(__in UINT nBoardNumber);

	// I2C Functions
	BOOL		I2C_Reset			();
	BOOL		I2C_SetClock		(__in int nClock);
	BOOL		I2C_Read			(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in BYTE nByteSize, __out LPBYTE lpbyData);// Max 256 Byte
	BOOL		I2C_Write			(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in WORD nByteSize, __in LPBYTE lpbyData);// Max 1024 Byte
	BOOL		I2C_Read_Repeat		(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in BYTE nByteSize, __out LPBYTE lpbyData, __in UINT nRetry = MAX_I2C_RETRY_CNT);// Max 256 Byte
	BOOL		I2C_Write_Repeat	(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in WORD nByteSize, __in LPBYTE lpbyData, __in UINT nRetry = MAX_I2C_RETRY_CNT);// Max 1024 Byte

	// LVDS Camera Link Functions
	BOOL		Init				();
	BOOL		Close				();
	BOOL		Start				();
	BOOL		Stop				();

	BOOL		GetFrame			(__out PDWORD pdwDataSize, __out unsigned char* lpbyData);
	BOOL		GetResolution		(__out PDWORD pdwXRes, __out PDWORD pdwYRes);
	BOOL		SetDataMode			(__in enDAQDataMode nMode);
	BOOL		GetVersion			(__out int* pnFpgaVer, __out int* pnFirmVer);
	BOOL		BufferFlush			();

	// 추가 함수
	BOOL		HsyncPol			(__in enDAQHsyncPolarity bPol);
	BOOL		PclkPol				(__in enDAQPclkPolarity bPol);
	BOOL		SetDeUse			(__in enDAQDvalUse bUse);
	BOOL		VideoMode			(__in enDAQVideoMode nMode);
	BOOL		GetFrameRate		(__out PDWORD pdwRate);
	BOOL		SetDivide			(__in DWORD dwWidth, __in DWORD dwHeight, __in DWORD dwX0, __in DWORD dwX1, __in DWORD dwX2, __in DWORD dwY0, __in DWORD dwY1, __in DWORD dwY2);
	BOOL		GetDivide			(__out PDWORD pdwWidth, __out PDWORD pdwHeight, __out PDWORD pdwX0, __out PDWORD pdwX1, __out PDWORD pdwX2, __out PDWORD pdwY0, __out PDWORD pdwY1, __out PDWORD pdwY2);

	// Clock Functions
	BOOL		Clock_Init			();
	BOOL		Clock_Close			();
	BOOL		Clock_Select		(__in int nSelect);
	DWORD		Clock_Get			();
	BOOL		Clock_Set			(__in DWORD dwVal);
	BOOL		Clock_Off			(__in BOOL bOff);		// bOff TRUE : clock off, FALSE : clock on


	// LVDS 초기화 여부
	BOOL		Is_LVDS_Init				();
	
	// LVDS 영상 캡쳐 시작 여부
	BOOL		Is_LVDS_Start				();

	//---------------------------------------------------------------
	// 기본 설정 함수들
	//---------------------------------------------------------------
	// 오너 윈도우 핸들 설정
	void			SetOwner				(__in HWND hOwner);

	// 카메라 영상 상태 변경시 처리 메세지
	void			SetWM_ChgStatus			(__in UINT nWinMsg);

	// 비데오 영상 수신 메세지
	void			SetWM_RecvVideo			(__in UINT nWinMsg);
	
	// 영상 포맷 설정
	void			SetColorModel			(__in enColorModel nSrcColorModel, __in enColorModelOut nOutColorModel = CMOUT_RGBA);
	void			SetSourceColorFormat	(__in enColorModel nSrcColorModel, __in enBitsPerPixel nBitsPerPixel);


	// 수신된 영상 프레임 데이터 구조체
	ST_VideoRGB*	GetRecvVideoRGB			();
	LPBYTE			GetRecvRGBData			();
	LPBYTE			GetRecvRGBData			(__out DWORD& dwDataSize);
	// 원본 영상 프레임 데이터
	ST_FrameData*	GetSourceFrameData_ST	();
	LPBYTE			GetSourceFrameData		();
	LPBYTE			GetSourceFrameData		(__out DWORD& dwDataSize);

	// FPS 구하기
	DOUBLE			GetFPS					();
	DWORD			GetFPS_HW				();
	// 고정 FPS 설정
	void			SetFixedFrameRate		(__in DOUBLE dFrameRate);

	// 영상 수신 
	BOOL			GetSignalStatus			();

	// 영상 캡쳐 시작/중지/재시작
	virtual BOOL	Capture_Start			();
	virtual BOOL	Capture_Stop			();
	virtual BOOL	Capture_Restart			();

	// 보드, 영상 설정
	void			SetOption				(__in ST_DAQOption stOption);
	ST_DAQOption	GetOption				();

	// 상태가져오기
	ST_DAQStatus	GetDAQStatus			();

	// 영상 처리 함수 사용여부
	void			SetUse_Flip				(__in enFlipType nFlip);
	void			SetUse_Rotate			(__in enRotateType nRotate);


	// I2C 처리 기능
	void			SetI2CFilePath			(__in LPCTSTR szFilePath);
	BOOL			SetI2CDataFromFile		();
	BOOL			SetI2CDataFromFile		(__in LPCTSTR szFilePath);
	BOOL			SetI2CDataFromFile_Old	(__in LPCTSTR szFilePath);
	BOOL			SetI2CData				();
	void			Set_UseAutoI2CWrite		(__in BOOL bUse);
		
	// GetFrame으로 읽은 프레임 데이터 저장
	BOOL			SaveRawImage			(__in LPCTSTR szFilePath, __in LPBYTE lpbySrc, __in DWORD dwFileSize);
	BOOL			SaveRawImage			(__in LPCTSTR szFilePath);
	BOOL			SaveRawImage_10Bit		(__in LPCTSTR szFilePath);
	BOOL			SaveRawImage_12Bit		(__in LPCTSTR szFilePath);

	// 파일에서 이미지 읽어 Frame 데이터에 설정
	BOOL			SetFrameDataFromFile	(__in LPCTSTR szFilePath);
	
	// 수집 데이터 초기화 (Frame Count)
	void			Reset_CaptureInfo		();

	// 영상 구동용 Register Data 쓰기
	BOOL			Write_RegisterData			(__in UINT nRetry = 9, __in DWORD dwLoopDelay = 200, __in DWORD dwWeightDelay = 100);
	// 영상 신호가 들어올때까지 체크
	BOOL			Check_VideoSignal			(__in DWORD dwCheckTime, __in DWORD dwGapDelay = 100);
	// 프레임 카운트가 정상 기준으로 될때까지 FPS 체크
	UINT			Check_FrameCount			(__in UINT nMinFPS, __in UINT nMaxFPS, __in DWORD dwCheckTime, __in DWORD dwGapDelay = 50, __in UINT nRetry = 5);

	void			Set_RGB_Exposure			(__in DOUBLE dExposure = 0.0f);
	void			Set_RGB_ContrastBrightness	(__in char chContrast = 0, __in char chBrightness = 0);
	void VideoChangeMode(BOOL bMode);
};

#endif // DAQ_LVDS_h__
