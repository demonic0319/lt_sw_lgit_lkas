﻿//*****************************************************************************
// Filename	: 	DAQ_LG_IRCam.cpp
// Created	:	2018/1/10 - 16:12
// Modified	:	2018/1/10 - 16:12
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#include "stdafx.h"
#include "DAQ_LG_IRCam.h"


CDAQ_LG_IRCam::CDAQ_LG_IRCam()
{
	for (UINT nIdx = 0; nIdx < MAX_DAQ_CHANNEL; nIdx++)
	{
		m_DAQ_LVDS[nIdx].Set_UseAutoI2CWrite(FALSE);
	}
}

CDAQ_LG_IRCam::~CDAQ_LG_IRCam()
{
}

void CDAQ_LG_IRCam::Reset_CaptureInfo()
{
	m_DAQ_LVDS[0].Reset_CaptureInfo();
}

//=============================================================================
// Method		: Send_SensorInit
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nIdxBrd
// Parameter	: __in UINT nRetry
// Qualifier	:
// Last Update	: 2018/2/6 - 17:33
// Desc.		:
//=============================================================================
BOOL CDAQ_LG_IRCam::Send_SensorInit(__in UINT nIdxBrd, __in UINT nRetry /*= 9*/)
{
	return m_DAQ_LVDS[nIdxBrd].Write_RegisterData(nRetry);
}


//=============================================================================
// Method		: Send_SensorInit
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nIdxBrd
// Parameter	: __in UINT nRetry
// Qualifier	:
// Last Update	: 2018/2/6 - 17:33
// Desc.		:
//=============================================================================
BOOL CDAQ_LG_IRCam::Send_I2CWrite(__in UINT nIdxBrd, __in UINT nViselData)
{
	BYTE I2C_Write_Data_Buf[1];
	BYTE I2C_Write_Data_ViselOn[2] = {0x01, 0x00};

	m_DAQ_LVDS[nIdxBrd].I2C_Write(0x10 << 1, (DWORD)2, (DWORD)0x3046, (WORD)2, I2C_Write_Data_ViselOn); //Visel On

	I2C_Write_Data_Buf[0] = nViselData; // LED 값 넣기
	return m_DAQ_LVDS[nIdxBrd].I2C_Write(0x51 << 1, (DWORD)1, (DWORD)0xF9, (WORD)1, I2C_Write_Data_Buf);
}


// BOOL CDAQ_LG_IRCam::Check_FrameCount(__out float& fOutFPS)
// {
// 	BOOL bReturn = TRUE;
// 
// 	// 영상 On??
// 	if (m_DAQ_LVDS[0].GetSignalStatus())
// 	{
// 		fOutFPS = (float)m_DAQ_LVDS[0].GetFPS();
// 	}
// 	else
// 	{
// 		fOutFPS = 0.0f;
// 		bReturn = FALSE;
// 	}
// 
// 	return bReturn;
// }

//=============================================================================
// Method		: Send_I2C_SelChannel
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nChIdx
// Qualifier	:
// Last Update	: 2018/1/12 - 11:08
// Desc.		:
//=============================================================================
// BOOL CDAQ_LG_IRCam::Send_I2C_SelChannel(__in UINT nChIdx)
// {
// 	BOOL bReturn = TRUE;
// 
// 	BYTE byData[3] = { 0x00, 0x22, 0x23 };
// 
// 	if ((0 <= nChIdx) && (nChIdx <= 4))
// 	{
// 		bReturn = m_DAQ_LVDS[0].I2C_Write(0x44, 1, 0x06, 1, &byData[0]);
// 		bReturn &= m_DAQ_LVDS[0].I2C_Write(0x44, 1, 0x02, 1, &byData[1]);
// 	}
// 	else if ((5 <= nChIdx) && (nChIdx <= 9))
// 	{
// 		bReturn = m_DAQ_LVDS[0].I2C_Write(0x44, 1, 0x06, 1, &byData[0]);
// 		bReturn &= m_DAQ_LVDS[0].I2C_Write(0x44, 1, 0x02, 1, &byData[2]);
// 	}
// 
// 	return bReturn;
// }

//=============================================================================
// Method		: GetFPS
// Access		: public  
// Returns		: FLOAT
// Qualifier	:
// Last Update	: 2018/1/13 - 20:17
// Desc.		:
//=============================================================================
FLOAT CDAQ_LG_IRCam::GetFPS()
{
	return (FLOAT)m_DAQ_LVDS[0].GetFPS();
}

//=============================================================================
// Method		: GetFPS_HW
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2018/1/30 - 19:54
// Desc.		:
//=============================================================================
DWORD CDAQ_LG_IRCam::GetFPS_HW()
{
	return m_DAQ_LVDS[0].GetFPS_HW();
}
