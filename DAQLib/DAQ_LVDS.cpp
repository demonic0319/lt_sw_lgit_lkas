﻿//*****************************************************************************
// Filename	: 	DAQ_LVDS.cpp
// Created	:	2017/3/17 - 10:46
// Modified	:	2017/3/17 - 10:46
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "DAQ_LVDS.h"
#include "usb3_dio01_frm_import.h"
#include "CommonFunction.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

//#pragma comment (lib,"../DAQLib/USB3-DIO01.lib")
//#pragma comment (lib,"../DAQLib/USB3-DIO01_x64.lib")

CDAQ_LVDS::CDAQ_LVDS()
{
	// CPU 클럭 구하기
	if (!QueryPerformanceFrequency(&m_liFixFPS_PC))
	{
		TRACE(_T("QueryPerformanceFrequency failed!\n"));
	}
	//m_dCPU_Frequency = double(m_liFixFPS_PC.QuadPart); // Second
	m_dCPU_Frequency = double(m_liFixFPS_PC.QuadPart) / 1000.0; // Millisecond
	//m_dCPU_Frequency = double(m_liFixFPS_PC.QuadPart) / 1000000.0; // Micro Second
	
	m_stOption.nDataMode		= DAQ_Data_16bit_Mode;
	m_stOption.nHsyncPolarity	= DAQ_HsyncPol_Inverse;
	m_stOption.nPClockPolarity	= DAQ_PclkPol_FallingEdge;

	AssignFrameMem();

	CreateTimerQueue_Mon();
}


CDAQ_LVDS::~CDAQ_LVDS()
{
	TRACE(_T("<<< Start ~CDAQ_LVDS >>> \n"));

	DeleteTimerQueue_Mon();

	ReleaseFrameMem();

	TRACE(_T("<<< End ~CDAQ_LVDS >>> \n"));
}

//=============================================================================
// Method		: AssignFrameMem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/28 - 17:34
// Desc.		:
//=============================================================================
void CDAQ_LVDS::AssignFrameMem()
{
	m_pbyFrame = new BYTE[MAX_FRAME_SIZE];
}

//=============================================================================
// Method		: ReleaseFrameMem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/28 - 17:34
// Desc.		:
//=============================================================================
void CDAQ_LVDS::ReleaseFrameMem()
{
	if (nullptr != m_pbyFrame)
	{
		delete[] m_pbyFrame;

		m_pbyFrame = nullptr;
	}
}

//=============================================================================
// Method		: StartCounter
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 10:52
// Desc.		:
//=============================================================================
void CDAQ_LVDS::StartCounter()
{
	QueryPerformanceCounter(&m_liComputeFPS_PC);
	m_iCounterStart = m_liComputeFPS_PC.QuadPart;
}

//=============================================================================
// Method		: GetCounter
// Access		: protected  
// Returns		: double
// Qualifier	:
// Last Update	: 2017/3/30 - 11:10
// Desc.		:
//=============================================================================
double CDAQ_LVDS::GetCounter()
{
	QueryPerformanceCounter(&m_liComputeFPS_PC);

	return double(m_liComputeFPS_PC.QuadPart - m_iCounterStart) / m_dCPU_Frequency;
}

//=============================================================================
// Method		: InitFrameCount
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 11:11
// Desc.		:
//=============================================================================
void CDAQ_LVDS::InitFrameCount()
{
	m_stStatus.ResetCapture();

	m_iFrameCount = 1;

	StartCounter(); // QueryPerformanceCounter
}

//=============================================================================
// Method		: IncreaseFrameCount
// Access		: protected  
// Returns		: void
// Parameter	: __in BOOL bFail
// Qualifier	:
// Last Update	: 2017/3/30 - 11:13
// Desc.		:
//=============================================================================
void CDAQ_LVDS::IncreaseFrameCount(__in BOOL bFail /*= FALSE*/)
{
	if (bFail)
	{
		m_stStatus.dwFailFrameCount++;	// 영상 캡쳐 전체 실패 카운트
		m_stStatus.dwFailContiCount++;	// 영상 캡쳐 연속 실패 카운트

		// 연속 불량 개수에 따라서 영상 신호 Off 처리
		if (5 < m_stStatus.dwFailContiCount)
		{
			m_bSignal = FALSE;
		}
	}
	else
	{
		m_iFrameCount++;
		m_stStatus.dwFrameCount++;			// 프레임 카운트 증가
		m_stStatus.dwFailContiCount = 0;	// 연속 불량 초기화

		// 10 프레임마다 FPS 계산
		if (m_iFrameCount >= 20)
		{
			// timeafter StartCounter() (sec)
			m_stStatus.dFPS = (double)(m_iFrameCount / GetCounter() * 1000.0f);
			StartCounter();
			m_iFrameCount = 0;
		}
	}
}

//=============================================================================
// Method		: CheckFrameRate
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/29 - 19:37
// Desc.		: 설정된 FPS에 맞추기 위해 사용
//=============================================================================
BOOL CDAQ_LVDS::CheckFrameRate()
{
	// 시간 체크
	QueryPerformanceCounter(&m_liFixFPS_PC);

	//m_llFixFPSDuration = (LONGLONG)(((DOUBLE)(m_liFixFPS_PC.QuadPart - m_llFixFPSStart)) * 1000000.0f / m_dCPU_Frequency);
	m_llFixFPSDuration = (LONGLONG)(((DOUBLE)(m_liFixFPS_PC.QuadPart - m_llFixFPSStart)) * 1000.0f / m_dCPU_Frequency);
	
	if (m_stStatus.dDelay_GetFrame <= m_llFixFPSDuration)
	{
		m_llFixFPSStart = m_liFixFPS_PC.QuadPart;

		//TRACE(_T("Check Frame Rate: %.4f FPS, Elapsed: %.4f ms, Time: %d\n"), m_stStatus.dFPS, (FLOAT)m_llFixFPSDuration / 1000.0f, m_llFixFPSStart);

		return TRUE;
	}

	// 소수점 자리에 대해서 반 올림
	DWORD dwDelay = (DWORD)round(((m_stStatus.dDelay_GetFrame - (DOUBLE)m_llFixFPSDuration) / 1000.0f));
	Sleep(dwDelay);

	return FALSE;
}

//=============================================================================
// Method		: StartTickCounter
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/5 - 19:40
// Desc.		:
//=============================================================================
void CDAQ_LVDS::StartTickCounter()
{
	QueryPerformanceCounter(&m_liPC);

	m_llStart = m_liPC.QuadPart;
}

double CDAQ_LVDS::GetTickCounter()
{
	QueryPerformanceCounter(&m_liPC);

	return double(m_liPC.QuadPart - m_llStart) / m_dCPU_Frequency;
}

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:13
// Desc.		:
//=============================================================================
void CDAQ_LVDS::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CreateTimerQueue_Mon ()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CDAQ_LVDS::DeleteTimerQueue_Mon()
{
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE("DeleteTimerQueue failed (%d)\n", GetLastError());

	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CDAQ_LVDS::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CDAQ_LVDS::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimer_Dev_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CDAQ_LVDS::CreateTimer_Status()
{
	__try
	{
		TRACE(_T("CreateTimer_Status : Start\n"));

		if (!CreateTimerQueueTimer(&m_hTimer_Status, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_Status, (PVOID)this, 0, m_dwCycle_Status, 0))
		{
			TRACE(_T("CreateTimer_Status failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CreateTimer_Status ()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimer_Dev_Mon
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CDAQ_LVDS::DeleteTimer_Status()
{
	__try
	{
		TRACE(_T("DeleteTimer_Status : Stop\n"));

		if (NULL != m_hTimer_Status)
		{
			if (!DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer_Status, INVALID_HANDLE_VALUE))
			{
				TRACE(_T("DeleteTimerQueueTimer(m_hTimer_Status) Error : \n"));
			}
			m_hTimer_Status = NULL;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : DeleteTimer_Status ()\n"));
	}	
}

//=============================================================================
// Method		: TimerRoutine_Dev_Mon
// Access		: public static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
VOID CALLBACK CDAQ_LVDS::TimerRoutine_Status(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	((CDAQ_LVDS*)lpParam)->Monitor_Status();
}

//=============================================================================
// Method		: Monitor_Dev
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/2/12 - 10:14
// Desc.		:
//=============================================================================
void CDAQ_LVDS::Monitor_Status()
{
	// 상태 체크
	BOOL bChange = FALSE;

	if (m_stStatus.bInitBoard && m_stStatus.bStartCapture)
	{
		// Frame Rate
		GetFrameRate(&m_stStatus.dwFPS_HW);
	}

	// 영상 신호 (현재 영상 상태와 이전 영상 상태 비교)
	if (m_bSignal != m_stStatus.bSignal)
	{
		if ((TRUE == m_bSignal) && (FALSE == m_stStatus.RGBData.m_bStore))
			m_stStatus.RGBData.m_bStore = TRUE;

		m_stStatus.bSignal = m_bSignal;

		bChange = TRUE;
	}

	// 오너 윈도우로 상태 변경 메세지 통지
	if (bChange)
	{
		NotifyChangeStatus();
	}
}

//=============================================================================
// Method		: Thread_Frame
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2017/3/17 - 16:40
// Desc.		:
//=============================================================================
UINT WINAPI CDAQ_LVDS::Thread_Frame(__in LPVOID lParam)
{
	CDAQ_LVDS* pThis = (CDAQ_LVDS*)lParam;

	pThis->Monitor_Frame();

	return 1;
}

//=============================================================================
// Method		: Monitor_Frame
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/21 - 14:22
// Desc.		:
//=============================================================================
void CDAQ_LVDS::Monitor_Frame()
{
	// DAQ 보드 초기화 후 딜레이 필요
	Sleep(600);

	// Sleep 해상력 높임
	timeBeginPeriod(1);

	// 해상도, 프레임 크기, 프레임 버퍼 메모리 할당
	CheckFrameSize();
	
	// FPS 계산 초기화
	InitFrameCount();

	m_bThrGetFrameFlag = TRUE;
	while (m_bThrGetFrameFlag)
	{
		if (m_stStatus.bStartCapture)
		{
			// 시간 체크 (프레임 레이트 고정하기)
			if (CheckFrameRate())
			{
				if (GetFrame(&m_dwFrameSize, m_pbyFrame))
				{
					m_bSignal = TRUE;


					if (m_stStatus.bVideoChangeMode == FALSE)
					{
						// 영상 포맷 변환
						ConvertImageFormat(m_pbyFrame);

						// 이미지 플립, 회전, 반전 등의 후처리 기능 수행
						//Apply_ImageProcessing();

						// FPS 계산
						IncreaseFrameCount();

						// 메세지/콜백함수로 통보
						NotifyReceiveVideo();
					}
				}
				else
				{
					//m_bSignal = FALSE;

					// Fail 카운트 증가
					IncreaseFrameCount(TRUE);

					// Get Frame 실패시 대기 시간 짧게..
 					//Sleep(10);
 					//continue;

					// 메세지/콜백함수로 통보
					//NotifyReceiveVideo();
				}
			}
			else
			{
				//Sleep(1);
				//continue;
			}

 			//if (FALSE == m_bThrGetFrameFlag)
 			//	break;

			//Sleep(33);	// 30프레임
			//Sleep(25);	// 40프레임
			//Sleep(20);	// 50프레임
			//Sleep(16);	// 60프레임
			//Sleep(m_stStatus.dwDelay_GetFrame - m_llFixFPSDuration);
			//Sleep(1);
		}
	}
}

//=============================================================================
// Method		: StartThread_Frame
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/21 - 14:42
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::StartThread_Frame()
{
	if (nullptr != m_hThr_Frame)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_Frame, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
			return FALSE;
	}

	if (nullptr != m_hThr_Frame)
	{
		CloseHandle(m_hThr_Frame);
		m_hThr_Frame = nullptr;
	}

	m_hThr_Frame = HANDLE(_beginthreadex(NULL, 0, Thread_Frame, this, 0, NULL));

	return TRUE;
}

//=============================================================================
// Method		: StopThread_Frame
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/21 - 14:42
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::StopThread_Frame()
{
	m_bThrGetFrameFlag = FALSE;
	Delay(100);

	if (nullptr != m_hThr_Frame)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_Frame, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThr_Frame, dwExitCode);
			WaitForSingleObject(m_hThr_Frame, WAIT_ABANDONED);
		}

		CloseHandle(m_hThr_Frame);
		m_hThr_Frame = nullptr;
	}

	return TRUE;
}

//=============================================================================
// Method		: NotifyReceiveVideo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/28 - 18:17
// Desc.		: 윈도우 메세지나 콜백함수로 통보
//=============================================================================
void CDAQ_LVDS::NotifyReceiveVideo()
{
	if ((NULL != m_hWndOwner) && (NULL != m_nWM_RecvVideo))
		::SendNotifyMessage(m_hWndOwner, m_nWM_RecvVideo, (WPARAM)m_nBoardNumber, (LPARAM)&m_stStatus.RGBData);


//m_nDeviceType

	// 콜백함수
// 	PVOID this;
// 	UINT m_nBoardNumber;
// 	m_stStatus.GetRGBData();
// 	m_stStatus.dwXRes;
// 	m_stStatus.dwYRes;
// 	영상포맷?
}

//=============================================================================
// Method		: NotifyFailReceiveVideo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/20 - 19:03
// Desc.		:
//=============================================================================
void CDAQ_LVDS::NotifyFailReceiveVideo()
{

}

//=============================================================================
// Method		: NotifyChangeStatus
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 14:17
// Desc.		: 영상 수신 상태 변경에 따른 알림 처리
//=============================================================================
void CDAQ_LVDS::NotifyChangeStatus()
{
	if ((NULL != m_hWndOwner) && (NULL != m_nWM_ChgStatus))
		::SendNotifyMessage(m_hWndOwner, m_nWM_ChgStatus, (WPARAM)m_nBoardNumber, (LPARAM)m_stStatus.bSignal);
}

//=============================================================================
// Method		: CheckFrameSize
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/13 - 13:56
// Desc.		:
//=============================================================================
void CDAQ_LVDS::CheckFrameSize()
{
	// 해상도 구하기
	DWORD dwWidth	= 0;
	DWORD dwHeight	= 0;
	if (GetResolution(&dwWidth, &dwHeight))
	{

	}
	else
	{
		;
	}

	// 영상 너비 보정
	if (m_stOption.nVideoMode == DAQ_Video_MIPI_Mode)
	{
		m_stStatus.dwXRes = m_stOption.Get_MipiLaneXres(m_stOption.nMIPILane, dwWidth);

		m_stStatus.dwXRes = m_stOption.Get_AdjustXres(m_stOption.nSensorType, m_stStatus.dwXRes);
		m_stStatus.dwXRes = (DWORD)(m_stStatus.dwXRes * m_stOption.dWidthMultiple);
	}
	else
	{
		//m_stStatus.dwXRes = m_stOption.Get_AdjustXres(m_stOption.nSensorType, m_stStatus.dwXRes);
		m_stStatus.dwXRes = (DWORD)(dwWidth * m_stOption.dWidthMultiple);
	}
	
	// 영상 높이 보정
	m_stStatus.dwYRes = (DWORD)(dwHeight * m_stOption.dHeightMultiple);

	// 실제 수신되는 프레임 크기 계산
	DWORD dwCount = dwWidth * dwHeight * 2;
	if ((m_stOption.nDataMode == DAQ_Data_24bit_Mode) || (DAQ_Data_32bit_Mode == m_stOption.nDataMode))	// 24,32bit mode		
	{
		dwCount *= 2;
	}
	m_dwFrameSize = ((dwCount % DAQ_PACKET_SIZE) != 0) ? (((dwCount / DAQ_PACKET_SIZE) + 1) * DAQ_PACKET_SIZE) : dwCount;

	//m_stStatus.RGBData.AssignMem(m_stStatus.dwXRes, m_stStatus.dwYRes);

	// 이미지 상단, 하단을 잘라내야 하는 경우
	if ((0 < m_stOption.nCropLine_Top) || (m_stOption.nCropLine_Bottom))
	{
		m_stStatus.dwYRes = m_stStatus.dwYRes - m_stOption.nCropLine_Top - m_stOption.nCropLine_Bottom;
	}

	// 짝수 행 보정
 	int Yres = m_stStatus.dwYRes;
 	if (m_stStatus.dwYRes % 2 == 1)
 	{
 		Yres++;
 	}
 
 	// 출력용 영상 버퍼에 메모리 할당 	
	m_stStatus.RGBData.AssignMem(m_stStatus.dwXRes, Yres);

	// 테스트용 임시
	m_stFrameBuf.AssignMem(m_stStatus.dwXRes, m_stStatus.dwYRes, m_stStatus.dwXRes * m_stStatus.dwYRes * 2);
}

//=============================================================================
// Method		: GetResolutionAndAdjust
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/29 - 17:59
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::GetResolutionAndAdjust()
{
	DWORD dwXRes = 0;
	DWORD dwYRes = 0;

	if (LVDS_GetResolution(m_nBoardNumber, &dwXRes, &dwYRes))
	{
		m_stStatus.dwHW_XRes = dwXRes;
		m_stStatus.dwHW_YRes = dwYRes;

		m_stStatus.dwXRes = dwXRes;
		m_stStatus.dwYRes = dwYRes;

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//=============================================================================
// Method		: ConvertImageFormat
// Access		: protected  
// Returns		: void
// Parameter	: __in LPBYTE lpbyFrame
// Qualifier	:
// Last Update	: 2017/6/13 - 14:00
// Desc.		:
//=============================================================================
void CDAQ_LVDS::ConvertImageFormat(__in LPBYTE lpbyFrame)
{
#ifdef _DEBUG
	StartTickCounter();
#endif	
	LPBYTE lpOutVideo = m_stStatus.RGBData.GetWriteRGBData();

	if (enImgSensorType_Monochrome_12bit == m_stOption.nSensorType) // LGIT OMS 모델
	{
		// RAW -> Gray12 -> RGB

		// * 카메라 영상과 Depth 영상 구분하기
		if (m_stOption.nCropFrame == Crop_OddFrame)
			m_ColorConv.CropOddFrame(lpbyFrame, m_stStatus.dwHW_XRes, m_stStatus.dwHW_YRes, 2);
		else if (m_stOption.nCropFrame == Crop_EvenFrame)
			m_ColorConv.CropEvenFrame(lpbyFrame, m_stStatus.dwHW_XRes, m_stStatus.dwHW_YRes, 2);

		// * RAW 이미지에서 Gray 12bit 이미지로 변환
		m_ColorConv.Make12BitMode(lpbyFrame, (LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes);
		//m_ColorConv.ConvertToGray(lpbyFrame, m_stStatus.dwXRes, m_stStatus.dwYRes, m_stFrameBuf.GetWriteFrameData(), m_stOption.nConvFormat, m_stOption.byBitPerPixels);

		// * 영상 출력용 RGB 영상으로 변환
		if (m_nOutColorModel == CMOUT_RGB)
		{
			//m_ColorConv.Grey16ToRGB24((LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes * m_stStatus.dwYRes, lpOutVideo);
			m_ColorConv.Grey16ToRGB24_CV((LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes, lpOutVideo, m_stOption.dAlpha, m_stOption.dBeta);
			//m_ColorConv.ConvertToRGB24(m_stStatus.dwXRes, m_stStatus.dwYRes, m_stFrameBuf.GetWriteFrameData(), lpOutVideo, m_stOption.nSensorType, m_stOption.nConvFormat, m_stOption.byBitPerPixels, m_stOption.dAlpha, m_stOption.dBeta);
		}
		else
		{
			m_ColorConv.Grey16ToRGB32_CV((LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes, lpOutVideo, m_stOption.dAlpha, m_stOption.dBeta);
			//m_ColorConv.ConvertToRGB32(m_stStatus.dwXRes, m_stStatus.dwYRes, m_stFrameBuf.GetWriteFrameData(), lpOutVideo, m_stOption.nSensorType, m_stOption.nConvFormat, m_stOption.byBitPerPixels, m_stOption.dAlpha, m_stOption.dBeta);
		}
	}
	else if (enImgSensorType_Monochrome_CCCC ==  m_stOption.nSensorType)// LGIT MRA2, IKC
	{
		if (0 < m_stOption.nCropLine_Top)
		{
			DWORD dwCropedOffset = m_stStatus.dwHW_XRes * 2 * m_stOption.nCropLine_Top; // 너비 Byte수 * m_stOption.nCropLine_Top
			LPBYTE lpbyCropedFrame = &lpbyFrame[dwCropedOffset];

			//m_ColorConv.ConvertBayerToGray16(lpbyCropedFrame, m_stStatus.dwXRes, m_stStatus.dwYRes, m_stFrameBuf.GetWriteFrameData(), m_stOption.nConvFormat, 12);
			m_ColorConv.Make12BitMode_RCCC(lpbyCropedFrame, (LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes);
			//m_ColorConv.Make12BitMode(lpbyCropedFrame, (LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes);
			//m_ColorConv.Make12to16_CCCR_16to12((LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes);
		}
		else
		{
		//	m_ColorConv.ConvertBayerToGray16(lpbyFrame, m_stStatus.dwXRes, m_stStatus.dwYRes, m_stFrameBuf.GetWriteFrameData(), m_stOption.nConvFormat, 12);
		//	m_ColorConv.Make12BitMode(lpbyFrame, (LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes);
			m_ColorConv.Make12BitMode_RCCC(lpbyFrame, (LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes);
		//	m_ColorConv.Make12to16_CCCR_16to12((LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes);
		}

		m_ColorConv.Grey16ToRGB24_CV((LPWORD)m_stFrameBuf.GetWriteFrameData(), m_stStatus.dwXRes, m_stStatus.dwYRes, lpOutVideo, m_stOption.dAlpha, m_stOption.dBeta);
		//m_ColorConv.ConvertToRGB24(m_stStatus.dwXRes, m_stStatus.dwYRes, lpbyFrame, lpOutVideo, m_stOption.nSensorType, m_stOption.nConvFormat, m_stOption.byBitPerPixels, m_stOption.dAlpha, m_stOption.dBeta);
	}
	else
	{
		// * RAW 데이터 더블 버퍼링 용도
		memcpy(m_stFrameBuf.GetWriteFrameData(), lpbyFrame, m_stFrameBuf.dwSize);

		// * RAW 영상에서 RGB 영상으로 변환
		if (m_nOutColorModel == CMOUT_RGB)
		{
			m_ColorConv.ConvertToRGB24(m_stStatus.dwXRes, m_stStatus.dwYRes, m_stFrameBuf.GetWriteFrameData(), lpOutVideo, m_stOption.nSensorType, m_stOption.nConvFormat, m_stOption.byBitPerPixels, m_stOption.dAlpha, m_stOption.dBeta);
		}
		else
		{
			m_ColorConv.ConvertToRGB32(m_stStatus.dwXRes, m_stStatus.dwYRes, m_stFrameBuf.GetWriteFrameData(), lpOutVideo, m_stOption.nSensorType, m_stOption.nConvFormat, m_stOption.byBitPerPixels, m_stOption.dAlpha, m_stOption.dBeta);
		}
	}

	// 더블 버퍼링 처리
 	m_stFrameBuf.IncreaseBufferIndex();
 	m_stStatus.RGBData.IncreaseBufferIndex();

#ifdef _DEBUG
	double dDur = GetTickCounter();
//	TRACE(_T("CDAQ_LVDS::ConvertImageFormat = %.4f ms\n"), dDur);
#endif
}

//=============================================================================
// Method		: Apply_ImageProcessing
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/22 - 15:46
// Desc.		: 플립, 회전, 반전
//=============================================================================
void CDAQ_LVDS::Apply_ImageProcessing()
{	
	LPBYTE lpVideo = m_stStatus.RGBData.GetWriteRGBData();

	switch (m_stImageProcOpt.Flip)
	{
	case Flip_Horizontal:
		m_ColorConv.FlipHorizontal(lpVideo, m_stStatus.dwXRes, m_stStatus.dwYRes);
		break;

	case Flip_Vertical:
		m_ColorConv.FlipVertical(lpVideo, m_stStatus.dwXRes, m_stStatus.dwYRes);
		break;

	case Flip_Horz_Vert:
		m_ColorConv.FlipHorizontal(lpVideo, m_stStatus.dwXRes, m_stStatus.dwYRes);
		m_ColorConv.FlipVertical(lpVideo, m_stStatus.dwXRes, m_stStatus.dwYRes);
		break;

	case Flip_None:
	default:
		break;
	}

	switch (m_stImageProcOpt.Rotate)
	{
	case Rotate_CW_90:
	case Rotate_CW_180:
	case Rotate_CW_270:
		break;

	case Rotate_CW_None:
	default:
		break;
	}

	// 더블 버퍼링 처리
	m_stStatus.RGBData.IncreaseBufferIndex();
	
}

//=============================================================================
// Method		: Write_MIPI_I2C
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in enDAQMIPILane byMIPILane
// Qualifier	:
// Last Update	: 2017/9/5 - 14:46
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Write_MIPI_I2C(__in enDAQMIPILane nMIPILane)
{
	int nVIOData = (int)abs((m_stOption.fMIPI_VIO - 1.25) / 0.00322);
	
	BYTE btVIO[2] = { 0, };
	
	btVIO[0] = nVIOData & 0xff;
	btVIO[1] = nVIOData / 0x100;
	
	BYTE btVIOEn[2] = { 0x00, 0x01 };
	
	if (!I2C_Write(0x20 << 1, 1, 0x10, 2, btVIO))
	{
	
	}

	BYTE nMIPI = (BYTE)nMIPILane;
	return I2C_Write(0x14 << 1, 1, 0x1d, 1, &nMIPI);
}

//=============================================================================
// Method		: SetBoardNumber
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nBoardNumber
// Qualifier	:
// Last Update	: 2017/3/17 - 13:23
// Desc.		:
//=============================================================================
void CDAQ_LVDS::SetBoardNumber(__in UINT nBoardNumber)
{
	if (nBoardNumber < MAX_DAQ_CHANNEL)
	{
		m_nBoardNumber = nBoardNumber;
	}
	else
	{
		TRACE(_T("DAQ SetBoardNumber Failed: Board Number %d\n"), m_nBoardNumber);
	}
}

//=============================================================================
// Method		: I2C_Reset
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 13:34
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::I2C_Reset()
{
	if (I2C_SYS_Reset(m_nBoardNumber))
	{
		TRACE(_T("DAQ I2C SYS Reset [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ I2C SYS Reset [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: I2C_SetClock
// Access		: public  
// Returns		: BOOL
// Parameter	: __in int nClock
// Qualifier	:
// Last Update	: 2017/3/17 - 15:18
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::I2C_SetClock(__in int nClock)
{
	if (I2C_SYS_Set_Clock(m_nBoardNumber, nClock))
	{
		TRACE(_T("DAQ I2C SYS Set Clock [Brd %d]: Succeed (Clock: %d)\n"), m_nBoardNumber, nClock);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ I2C SYS Set Clock [Brd %d]: Failed (Clock: %d)\n"), m_nBoardNumber, nClock);
		return FALSE;
	}
}

//=============================================================================
// Method		: I2C_Read
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BYTE bySlaveID
// Parameter	: __in DWORD dwAddrLen
// Parameter	: __in DWORD dwAddress
// Parameter	: __in BYTE nByteSize
// Parameter	: __out LPBYTE lpbyData
// Qualifier	:
// Last Update	: 2017/3/17 - 15:18
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::I2C_Read(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in BYTE nByteSize, __out LPBYTE lpbyData)
{
	if (I2C_SYS_Read(m_nBoardNumber, bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData))
	{
		TRACE(_T("DAQ I2C SYS Read [Brd %d]: Succeed (Slave : 0x%02X, Addr: 0x%04X)\n"), m_nBoardNumber, bySlaveID, dwAddress);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ I2C SYS Read [Brd %d]: Failed (Slave : 0x%02X, Addr: 0x%04X)\n"), m_nBoardNumber, bySlaveID, dwAddress);
		return FALSE;
	}
}

//=============================================================================
// Method		: I2C_Write
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BYTE bySlaveID
// Parameter	: __in DWORD dwAddrLen
// Parameter	: __in DWORD dwAddress
// Parameter	: __in WORD nByteSize
// Parameter	: __in LPBYTE lpbyData
// Qualifier	:
// Last Update	: 2017/3/17 - 15:18
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::I2C_Write(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in WORD nByteSize, __in LPBYTE lpbyData)
{
	if (1024 < nByteSize)
	{
		TRACE(_T("DAQ I2C SYS Write [Brd %d]: Failed (ByteSize: %d, Max 1024 Byte)\n"), m_nBoardNumber, nByteSize);
		return FALSE;
	}

	if (I2C_SYS_Write(m_nBoardNumber, bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData))
	{
		//TRACE(_T("DAQ I2C SYS Write [Brd %d]: Succeed (Slave : 0x%02X, Addr: 0x%04X)\n"), m_nBoardNumber, bySlaveID, dwAddress);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ I2C SYS Write [Brd %d]: Failed (Slave : 0x%02X, Addr: 0x%04X)\n"), m_nBoardNumber, bySlaveID, dwAddress);
		return FALSE;
	}
}

//=============================================================================
// Method		: I2C_Read_Repeat
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BYTE bySlaveID
// Parameter	: __in DWORD dwAddrLen
// Parameter	: __in DWORD dwAddress
// Parameter	: __in BYTE nByteSize
// Parameter	: __out LPBYTE lpbyData
// Parameter	: __in UINT nRetry
// Qualifier	:
// Last Update	: 2018/2/9 - 13:23
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::I2C_Read_Repeat(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in BYTE nByteSize, __out LPBYTE lpbyData, __in UINT nRetry /*= MAX_I2C_RETRY_CNT*/)
{
	BOOL bReturn = FALSE;

	for (UINT nCnt = 0; nCnt < nRetry; nCnt++)
	{
		bReturn = I2C_Read(bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData);
		
		if (bReturn == TRUE)
			break;

		Sleep(50);
	}
	return bReturn;
}

//=============================================================================
// Method		: I2C_Write_Repeat
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BYTE bySlaveID
// Parameter	: __in DWORD dwAddrLen
// Parameter	: __in DWORD dwAddress
// Parameter	: __in WORD nByteSize
// Parameter	: __in LPBYTE lpbyData
// Parameter	: __in UINT nRetry
// Qualifier	:
// Last Update	: 2018/2/9 - 13:22
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::I2C_Write_Repeat(__in BYTE bySlaveID, __in DWORD dwAddrLen, __in DWORD dwAddress, __in WORD nByteSize, __in LPBYTE lpbyData, __in UINT nRetry /*= MAX_I2C_RETRY_CNT*/)
{
	BOOL bReturn = FALSE;

	for (UINT nCnt = 0; nCnt < nRetry; nCnt++)
	{
		bReturn = I2C_Write(bySlaveID, dwAddrLen, dwAddress, nByteSize, lpbyData);

		if (bReturn == TRUE)
			break;

		Sleep(50 + (nCnt + 50));
	}
	return bReturn;
}

//=============================================================================
// Method		: Init
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 14:29
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Init()
{
	if (LVDS_Init(m_nBoardNumber))
	{
		m_stStatus.bInitBoard = TRUE;
		TRACE(_T("DAQ LVDS Init [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ LVDS Init [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: Close
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 14:38
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Close()
{
	if (LVDS_Close(m_nBoardNumber))
	{
		m_stStatus.bInitBoard = FALSE;
		TRACE(_T("DAQ LVDS Close [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ LVDS Close [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: Start
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 14:38
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Start()
{
	if (LVDS_Start(m_nBoardNumber))
	{
		m_stStatus.bStartCapture = TRUE;
		TRACE(_T("DAQ LVDS Start [Brd %d]: Succeed\n"), m_nBoardNumber);

		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ LVDS Start [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: Stop
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 14:38
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Stop()
{
	if (m_stStatus.bStartCapture)
	{
		if (LVDS_Stop(m_nBoardNumber))
		{
			m_stStatus.bStartCapture = FALSE;

			m_bSignal = FALSE;
			m_stStatus.bSignal = m_bSignal;

			NotifyChangeStatus();

			TRACE(_T("DAQ LVDS Stop [Brd %d]: Succeed\n"), m_nBoardNumber);
			return TRUE;
		}
		else
		{
			TRACE(_T("DAQ LVDS Stop [Brd %d]: Failed\n"), m_nBoardNumber);
			return FALSE;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: GetFrame
// Access		: public  
// Returns		: BOOL
// Parameter	: __out PDWORD pdwDataSize
// Parameter	: __out unsigned char * lpbyData
// Qualifier	:
// Last Update	: 2017/3/17 - 16:11
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::GetFrame(__out PDWORD pdwDataSize, __out unsigned char* lpbyData)
{
	if (LVDS_GetFrame(m_nBoardNumber, pdwDataSize, lpbyData))
	{
		//TRACE(_T("DAQ LVDS GetFrame [Brd %d]: Succeed (Size: %d)\n"), m_nBoardNumber, *pdwDataSize);
		return TRUE;
	}
	else
	{
		//TRACE(_T("DAQ LVDS GetFrame [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: GetResolution
// Access		: public  
// Returns		: BOOL
// Parameter	: __out PDWORD pdwXRes0
// Parameter	: __out PDWORD pdwYRes
// Qualifier	:
// Last Update	: 2017/3/17 - 16:11
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::GetResolution(__out PDWORD pdwXRes, __out PDWORD pdwYRes)
{
	if (LVDS_GetResolution(m_nBoardNumber, pdwXRes, pdwYRes))
	{
		TRACE(_T("DAQ LVDS GetResolution [Brd %d]: Succeed (XRes: %d, YRes: %d)\n"), m_nBoardNumber, *pdwXRes, *pdwYRes);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ LVDS GetResolution [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: SetDataMode
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enDAQDataMode nMode
// Qualifier	:
// Last Update	: 2017/3/17 - 16:11
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SetDataMode(__in enDAQDataMode nMode)
{
	if (LVDS_SetDataMode(m_nBoardNumber, nMode))
	{
		TRACE(_T("DAQ LVDS SetDataMode [Brd %d]: Succeed (Mode: %d)\n"), m_nBoardNumber, nMode);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ LVDS SetDataMode [Brd %d]: Failed (Mode: %d)\n"), m_nBoardNumber, nMode);
		return FALSE;
	}
}

//=============================================================================
// Method		: GetVersion
// Access		: public  
// Returns		: BOOL
// Parameter	: __out int * pnFpgaVer
// Parameter	: __out int * pnFirmVer
// Qualifier	:
// Last Update	: 2017/3/17 - 16:11
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::GetVersion(__out int* pnFpgaVer, __out int* pnFirmVer)
{
	if (LVDS_GetVersion(m_nBoardNumber, pnFpgaVer, pnFirmVer))
	{
		TRACE(_T("DAQ LVDS GetVersion [Brd %d]: Succeed (FPGA: %d, FW: %d)\n"), m_nBoardNumber, *pnFpgaVer, *pnFirmVer);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ LVDS GetVersion [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: BufferFlush
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 16:11
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::BufferFlush()
{
	if (LVDS_BufferFlush(m_nBoardNumber))
	{
		TRACE(_T("DAQ LVDS BufferFlush [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ LVDS BufferFlush [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: HsyncPol
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enDAQHsyncPolarity bPol
// Qualifier	:
// Last Update	: 2017/3/17 - 16:20
// Desc.		: LVDS Set Hsync Polarity(2014. 03. 21)
//=============================================================================
BOOL CDAQ_LVDS::HsyncPol(__in enDAQHsyncPolarity bPol)
{
	if (LVDS_HsyncPol(m_nBoardNumber, bPol))
	{
		TRACE(_T("DAQ HsyncPol [Brd %d]: Succeed (Hsync: %d)\n"), m_nBoardNumber, bPol);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ HsyncPol [Brd %d]: Failed (Hsync: %d)\n"), m_nBoardNumber, bPol);
		return FALSE;
	}
}

//=============================================================================
// Method		: PclkPol
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enDAQPclkPolarity bPol
// Qualifier	:
// Last Update	: 2017/3/17 - 16:30
// Desc.		: LVDS Set Pclk Polarity  (2015. 01. 07 add)
//=============================================================================
BOOL CDAQ_LVDS::PclkPol(__in enDAQPclkPolarity bPol)
{
	if (LVDS_PclkPol(m_nBoardNumber, bPol))
	{
		TRACE(_T("DAQ PclkPol [Brd %d]: Succeed (Hsync: %d)\n"), m_nBoardNumber, bPol);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ PclkPol [Brd %d]: Failed (Hsync: %d)\n"), m_nBoardNumber, bPol);
		return FALSE;
	}
}

//=============================================================================
// Method		: SetDeUse
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enDAQDvalUse bUse
// Qualifier	:
// Last Update	: 2017/3/17 - 16:30
// Desc.		: LVDS Set DVAL use  (2014. 03. 21)
//=============================================================================
BOOL CDAQ_LVDS::SetDeUse(__in enDAQDvalUse bUse)
{
	if (LVDS_SetDeUse(m_nBoardNumber, bUse))
	{
		TRACE(_T("DAQ SetDeUse [Brd %d]: Succeed (Edge: %d)\n"), m_nBoardNumber, bUse);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ SetDeUse [Brd %d]: Failed (Edge: %d)\n"), m_nBoardNumber, bUse);
		return FALSE;
	}
}

//=============================================================================
// Method		: VideoMode
// Access		: public  
// Returns		: BOOL
// Parameter	: __in enDAQVideoMode nMode
// Qualifier	:
// Last Update	: 2017/3/17 - 16:30
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::VideoMode(__in enDAQVideoMode nMode)
{
	if (LVDS_VideoMode(m_nBoardNumber, nMode))
	{
		TRACE(_T("DAQ VideoMode [Brd %d]: Succeed (Mode: %d)\n"), m_nBoardNumber, nMode);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ VideoMode [Brd %d]: Failed (Mode: %d)\n"), m_nBoardNumber, nMode);
		return FALSE;
	}
}

//=============================================================================
// Method		: GetFrameRate
// Access		: public  
// Returns		: BOOL
// Parameter	: __out PDWORD pdwRate
// Qualifier	:
// Last Update	: 2017/3/17 - 16:30
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::GetFrameRate(__out PDWORD pdwRate)
{
	if (LVDS_GetFrameRate(m_nBoardNumber, pdwRate))
	{
//		TRACE(_T("DAQ GetFrameRate [Brd %d]: Succeed (Frame: %d)\n"), m_nBoardNumber, *pdwRate);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ GetFrameRate [Brd %d]: Failed (Frame: %d)\n"), m_nBoardNumber, *pdwRate);
		return FALSE;
	}
}

//=============================================================================
// Method		: SetDivide
// Access		: public  
// Returns		: BOOL
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Parameter	: __in DWORD dwX0
// Parameter	: __in DWORD dwX1
// Parameter	: __in DWORD dwX2
// Parameter	: __in DWORD dwY0
// Parameter	: __in DWORD dwY1
// Parameter	: __in DWORD dwY2
// Qualifier	:
// Last Update	: 2017/3/17 - 16:30
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SetDivide(__in DWORD dwWidth, __in DWORD dwHeight, __in DWORD dwX0, __in DWORD dwX1, __in DWORD dwX2, __in DWORD dwY0, __in DWORD dwY1, __in DWORD dwY2)
{
	if (LVDS_SetDivide(m_nBoardNumber, dwWidth, dwHeight, dwX0, dwX1, dwX2, dwY0, dwY1, dwY2))
	{
		TRACE(_T("DAQ SetDivide [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ SetDivide [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: GetDivide
// Access		: public  
// Returns		: BOOL
// Parameter	: __out PDWORD pdwWidth
// Parameter	: __out PDWORD pdwHeight
// Parameter	: __out PDWORD pdwX0
// Parameter	: __out PDWORD pdwX1
// Parameter	: __out PDWORD pdwX2
// Parameter	: __out PDWORD pdwY0
// Parameter	: __out PDWORD pdwY1
// Parameter	: __out PDWORD pdwY2
// Qualifier	:
// Last Update	: 2017/3/17 - 16:30
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::GetDivide(__out PDWORD pdwWidth, __out PDWORD pdwHeight, __out PDWORD pdwX0, __out PDWORD pdwX1, __out PDWORD pdwX2, __out PDWORD pdwY0, __out PDWORD pdwY1, __out PDWORD pdwY2)
{
	if (LVDS_GetDivide(m_nBoardNumber, pdwWidth, pdwHeight, pdwX0, pdwX1, pdwX2, pdwY0, pdwY1, pdwY2))
	{
		TRACE(_T("DAQ GetDivide [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ GetDivide [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: Clock_Init
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 16:26
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Clock_Init()
{
	if (CLK_Init(m_nBoardNumber))
	{
		TRACE(_T("DAQ Clock_Init [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Clock_Init [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: Clock_Close
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/17 - 16:30
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Clock_Close()
{
	if (CLK_Close(m_nBoardNumber))
	{
		TRACE(_T("DAQ Clock_Close [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Clock_Close [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: Clock_Select
// Access		: public  
// Returns		: BOOL
// Parameter	: __in int nSelect
// Qualifier	:
// Last Update	: 2017/3/17 - 16:31
// Desc.		:	nSelect == 0 : fixed clock
//					nSelect == others : Program clock
//=============================================================================
BOOL CDAQ_LVDS::Clock_Select(__in int nSelect)
{
	if (CLK_Select(m_nBoardNumber, nSelect))
	{
		TRACE(_T("DAQ Clock_Select [Brd %d]: Succeed (Sel: %d)\n"), m_nBoardNumber, nSelect);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Clock_Select [Brd %d]: Failed (Sel: %d)\n"), m_nBoardNumber, nSelect);
		return FALSE;
	}
}

//=============================================================================
// Method		: Clock_Get
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2017/3/17 - 16:31
// Desc.		:
//=============================================================================
DWORD CDAQ_LVDS::Clock_Get()
{
	if (CLK_Get(m_nBoardNumber))
	{
		TRACE(_T("DAQ Clock_Get [Brd %d]: Succeed\n"), m_nBoardNumber);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Clock_Get [Brd %d]: Failed\n"), m_nBoardNumber);
		return FALSE;
	}
}

//=============================================================================
// Method		: Clock_Set
// Access		: public  
// Returns		: BOOL
// Parameter	: __in DWORD dwVal
// Qualifier	:
// Last Update	: 2017/3/17 - 16:31
// Desc.		: 1039Hz to 68Mhz
//=============================================================================
BOOL CDAQ_LVDS::Clock_Set(__in DWORD dwVal)
{
	if (CLK_Set(m_nBoardNumber, dwVal))
	{
		TRACE(_T("DAQ Clock_Set [Brd %d]: Succeed (Clock: %d)\n"), m_nBoardNumber, dwVal);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Clock_Set [Brd %d]: Failed (Clock: %d)\n"), m_nBoardNumber, dwVal);
		return FALSE;
	}
}

//=============================================================================
// Method		: Clock_Off
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bOff
// Qualifier	:
// Last Update	: 2017/3/17 - 16:31
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Clock_Off(__in BOOL bOff)
{
	if (CLK_Off(m_nBoardNumber, bOff))
	{
		TRACE(_T("DAQ Clock_Off [Brd %d]: Succeed (Off: %d)\n"), m_nBoardNumber, bOff);
		return TRUE;
	}
	else
	{
		TRACE(_T("DAQ Clock_Off [Brd %d]: Failed (Off: %d)\n"), m_nBoardNumber, bOff);
		return FALSE;
	}
}

BOOL CDAQ_LVDS::Is_LVDS_Init()
{
	return m_stStatus.bInitBoard;
}

BOOL CDAQ_LVDS::Is_LVDS_Start()
{
	return m_stStatus.bStartCapture;
}

void CDAQ_LVDS::SetOwner(__in HWND hOwner)
{
	m_hWndOwner = hOwner;
}

void CDAQ_LVDS::SetWM_ChgStatus(__in UINT nWinMsg)
{
	m_nWM_ChgStatus = nWinMsg;
}

void CDAQ_LVDS::SetWM_RecvVideo(__in UINT nWinMsg)
{
	m_nWM_RecvVideo = nWinMsg;
}

void CDAQ_LVDS::SetColorModel(__in enColorModel nSrcColorModel, __in enColorModelOut nOutColorModel /*= CMOUT_RGBA*/)
{
	m_nSrcColorModel = nSrcColorModel;
	m_nOutColorModel = nOutColorModel;

	m_stStatus.RGBData.m_bRGB32 = (m_nOutColorModel == CMOUT_RGBA) ? TRUE : FALSE;
}

void CDAQ_LVDS::SetSourceColorFormat(__in enColorModel nSrcColorModel, __in enBitsPerPixel nBitsPerPixel)
{
	m_nSrcColorModel	= nSrcColorModel;
	m_nBitsPerPixel		= nBitsPerPixel;
}

//=============================================================================
// Method		: GetRecvVideoRGB
// Access		: public  
// Returns		: ST_VideoRGB*
// Qualifier	:
// Last Update	: 2017/9/5 - 16:12
// Desc.		:
//=============================================================================
ST_VideoRGB* CDAQ_LVDS::GetRecvVideoRGB()
{
	return &m_stStatus.RGBData;
}

//=============================================================================
// Method		: GetRecvRGBData
// Access		: public  
// Returns		: LPBYTE
// Qualifier	:
// Last Update	: 2017/9/7 - 14:55
// Desc.		:
//=============================================================================
LPBYTE CDAQ_LVDS::GetRecvRGBData()
{
	return m_stStatus.RGBData.GetReadRGBData();
}

//=============================================================================
// Method		: GetRecvRGBData
// Access		: public  
// Returns		: LPBYTE
// Parameter	: __out DWORD & dwDataSize
// Qualifier	:
// Last Update	: 2018/8/6 - 14:07
// Desc.		:
//=============================================================================
LPBYTE CDAQ_LVDS::GetRecvRGBData(__out DWORD& dwDataSize)
{
	dwDataSize = m_stStatus.RGBData.m_dwSize;

	return m_stStatus.RGBData.GetReadRGBData();
}

//=============================================================================
// Method		: GetSourceFrameData_ST
// Access		: public  
// Returns		: ST_FrameData*
// Qualifier	:
// Last Update	: 2017/9/7 - 14:56
// Desc.		:
//=============================================================================
ST_FrameData* CDAQ_LVDS::GetSourceFrameData_ST()
{
	return &m_stFrameBuf;
}

//=============================================================================
// Method		: GetSourceFrameData
// Access		: public  
// Returns		: LPBYTE
// Qualifier	:
// Last Update	: 2017/9/7 - 14:54
// Desc.		:
//=============================================================================
LPBYTE CDAQ_LVDS::GetSourceFrameData()
{
	return m_stFrameBuf.GetReadFrameData();
}

//=============================================================================
// Method		: GetSourceFrameData
// Access		: public  
// Returns		: LPBYTE
// Parameter	: __out DWORD & dwDataSize
// Qualifier	:
// Last Update	: 2018/8/6 - 14:07
// Desc.		:
//=============================================================================
LPBYTE CDAQ_LVDS::GetSourceFrameData(__out DWORD& dwDataSize)
{
	dwDataSize = m_stFrameBuf.dwSize;

	return m_stFrameBuf.GetReadFrameData();
}

//=============================================================================
// Method		: GetFPS
// Access		: public  
// Returns		: DOUBLE
// Qualifier	:
// Last Update	: 2017/9/5 - 16:12
// Desc.		:
//=============================================================================
DOUBLE CDAQ_LVDS::GetFPS()
{
	return m_stStatus.dFPS;
}

//=============================================================================
// Method		: GetFPS_HW
// Access		: public  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2018/1/30 - 19:49
// Desc.		:
//=============================================================================
DWORD CDAQ_LVDS::GetFPS_HW()
{
	return m_stStatus.dwFPS_HW;
}

//=============================================================================
// Method		: SetFixedFrameRate
// Access		: public  
// Returns		: void
// Parameter	: __in DOUBLE dFrameRate
// Qualifier	:
// Last Update	: 2018/8/6 - 14:07
// Desc.		:
//=============================================================================
void CDAQ_LVDS::SetFixedFrameRate(__in DOUBLE dFrameRate)
{
	m_stStatus.SetFixedFrameRate(dFrameRate);
}

//=============================================================================
// Method		: GetSignalStatus
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/8/6 - 14:07
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::GetSignalStatus()
{
	return m_stStatus.bSignal;
}

//=============================================================================
// Method		: Capture_Start
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/29 - 15:16
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Capture_Start()
{
	if (FALSE == m_stStatus.bInitBoard)
		return FALSE;

	if (TRUE == m_stStatus.bStartCapture)
		return FALSE;

	// MIPI 설정
	if (DAQ_Video_MIPI_Mode == m_stOption.nVideoMode)
	{
		Write_MIPI_I2C(m_stOption.nMIPILane);
	}

	// Clcok Setting
	if (FALSE == Clock_Set(m_stOption.dwClock))
	{

	}

	VideoMode	(m_stOption.nVideoMode);

	SetDataMode	(m_stOption.nDataMode);

	HsyncPol	(m_stOption.nHsyncPolarity);

	PclkPol		(m_stOption.nPClockPolarity);

	SetDeUse	(m_stOption.nDvalUse);
	
	// I2C 파일 처리
	if (m_bUseAutoI2CWrite)
	{
		if (FALSE == m_szI2CFilePath.IsEmpty())
		{
			Sleep(100);
			SetI2CDataFromFile();
		}
	}

	// 해상도
	GetResolutionAndAdjust();

	// 영상 그래빙 시작
	if (Start())
	{
		CreateTimer_Status();
		StartThread_Frame();
		Sleep(300);
	}
	else
	{

	}

	return TRUE;
}

//=============================================================================
// Method		: Capture_Stop
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/4/19 - 19:01
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Capture_Stop()
{
	if (FALSE == m_stStatus.bInitBoard)
		return TRUE;

	if (FALSE == m_stStatus.bStartCapture)
		return TRUE;

	StopThread_Frame();
	DeleteTimer_Status();

	if (Stop())
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

//=============================================================================
// Method		: Capture_Restart
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/4/20 - 20:57
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Capture_Restart()
{

	Capture_Stop();

	Delay(100);

	if (!Close())
	{
		return FALSE;
	}

	Delay(100);

	if (!Init())
	{
		return FALSE;
	}

	Delay(100);

	Capture_Start();

	return TRUE;
}

//=============================================================================
// Method		: SetOption
// Access		: public  
// Returns		: void
// Parameter	: __in ST_DAQOption stOption
// Qualifier	:
// Last Update	: 2017/4/20 - 21:13
// Desc.		:
//=============================================================================
void CDAQ_LVDS::SetOption(__in ST_DAQOption stOption)
{
	m_stOption = stOption;
}

ST_DAQOption CDAQ_LVDS::GetOption()
{
	return m_stOption;
}

//=============================================================================
// Method		: GetDAQStatus
// Access		: public  
// Returns		: ST_DAQStatus
// Qualifier	:
// Last Update	: 2018/8/6 - 14:45
// Desc.		:
//=============================================================================
ST_DAQStatus CDAQ_LVDS::GetDAQStatus()
{
	return m_stStatus;
}

//=============================================================================
// Method		: SetUse_Flip
// Access		: public  
// Returns		: void
// Parameter	: __in enFlipType nFlip
// Qualifier	:
// Last Update	: 2017/5/22 - 15:19
// Desc.		:
//=============================================================================
void CDAQ_LVDS::SetUse_Flip(__in enFlipType nFlip)
{
	m_stImageProcOpt.Flip = nFlip;
}

//=============================================================================
// Method		: SetUse_Rotate
// Access		: public  
// Returns		: void
// Parameter	: __in enRotateType nRotate
// Qualifier	:
// Last Update	: 2017/5/22 - 15:21
// Desc.		:
//=============================================================================
void CDAQ_LVDS::SetUse_Rotate(__in enRotateType nRotate)
{
	m_stImageProcOpt.Rotate = nRotate;
}

//=============================================================================
// Method		: SetI2CFilePath
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szFilePath
// Qualifier	:
// Last Update	: 2017/7/18 - 14:33
// Desc.		:
//=============================================================================
void CDAQ_LVDS::SetI2CFilePath(__in LPCTSTR szFilePath)
{
	m_szI2CFilePath = szFilePath;

	if (FALSE == m_I2C_Dataz.Load_I2CFile(szFilePath))
	{
		TRACE(_T("I2C File 불러오기 실패!! \n"));
	}
}

//=============================================================================
// Method		: SetI2CDataFromFile
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/9/11 - 20:57
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SetI2CDataFromFile()
{
	ST_I2CFormat*	pstI2CDataz = NULL;
	UINT			nItemCount	= 0;
	pstI2CDataz = m_I2C_Dataz.GetI2CDataz(nItemCount);

	if ((0 == nItemCount) || (NULL == pstI2CDataz))
	{
		TRACE(_T("I2C Data가 없습니다.\n"));
		return FALSE;
	}

	BYTE SlaveID = 0x00;
	BOOL bReturn = FALSE;

	timeBeginPeriod(1);

	for (UINT nIdx = 0; nIdx < nItemCount; nIdx++, pstI2CDataz++)
	{
		switch (pstI2CDataz->nCommand)
		{
			case enI2C_Command::I2CCmd_SlaveID:
			{
				SlaveID = (BYTE)pstI2CDataz->dwAddress;
				Sleep(150);
			}
			break;

			case enI2C_Command::I2CCmd_Sleep:
			{
				Sleep(pstI2CDataz->dwMilliseconds);
			}
			break;

			case enI2C_Command::I2CCmd_Write:
			{
				if (FALSE == I2C_Write(SlaveID << 1, pstI2CDataz->dwAddrLen, pstI2CDataz->dwAddress, (WORD)pstI2CDataz->dwDataCount, pstI2CDataz->byDataz))
				{
					TRACE(_T("[Brd:%02d] I2C_Write가 실패했습니다.\n"), m_nBoardNumber);
					return FALSE;
				}

				Sleep(1); // 필요한가??
			}
			break;
		}
	}

	return TRUE;
}

//=============================================================================
// Method		: SetI2CDataFromFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFilePath
// Qualifier	:
// Last Update	: 2017/9/11 - 20:58
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SetI2CDataFromFile(__in LPCTSTR szFilePath)
{
	m_szI2CFilePath = szFilePath;

	if (FALSE == m_I2C_Dataz.Load_I2CFile(szFilePath))
	{
		TRACE(_T("I2C File 불러오기 실패!! \n"));

		return FALSE;
	}

	return SetI2CDataFromFile();
}

//=============================================================================
// Method		: SetI2CDataFromFile_Old
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFilePath
// Qualifier	:
// Last Update	: 2017/9/11 - 20:58
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SetI2CDataFromFile_Old(__in LPCTSTR szFilePath)
{
	BOOL bRet(FALSE);

	CStdioFile file;

	CString lineBuffer;
	int nID, nAddr, nData, nPos, nDoEvents;

	BOOL	bSuccess = TRUE;

	if (NULL == szFilePath)
		return FALSE;

	if (!file.Open(szFilePath, CFile::modeRead))
	{
		bRet = FALSE;
		AfxMessageBox(_T("I2C Setting File을 찾을 수 없습니다."));

		return bRet;
	}
	else
	{
		while (bSuccess)
		{
			bSuccess = file.ReadString(lineBuffer);

			if ((lineBuffer == "") || (lineBuffer[0] == '/'))
				continue;

			lineBuffer.MakeLower();
			lineBuffer.Remove(_T(' '));
			lineBuffer.Remove(_T('\t'));

			nPos = lineBuffer.Find(_T("slave"));

			if (nPos != -1)
			{
				nID = _tcstoul(lineBuffer.Mid(5, 4), 0, 16);
				Sleep(1);
			}
			else
			{
				nPos = lineBuffer.Find(_T("0x"));
				if (nPos != -1)
				{
					//nAddr = _tcstoul(lineBuffer.Mid(0, 4), 0, 16);
					//nData = _tcstoul(lineBuffer.Mid(4, 4), 0, 16);

					nAddr = _tcstoul(lineBuffer.Mid(2, 4).GetBuffer(0), 0, 16);
					nData = _tcstoul(lineBuffer.Mid(8, 4).GetBuffer(0), 0, 16);

					bSuccess = I2C_Write(nID << 1, 1, nAddr, 1, (unsigned char*)&nData);
				}

				if (bSuccess == FALSE)
				{
					bRet = FALSE;
					file.Close();
					AfxMessageBox(_T("I2C File 에 Write 작업을 실패하였습니다."));
					return bRet;
				}

				nPos = lineBuffer.Find(_T("sleep"));
				if (nPos != -1)
				{
					nDoEvents = _ttoi(lineBuffer.Mid(5, 7));
					Sleep(nDoEvents);
				}
			}

			Sleep(10);
		}
	}

	file.Close();

	return bRet;
}

//=============================================================================
// Method		: SaveRawImage
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFilePath
// Parameter	: __in LPBYTE lpbySrc
// Parameter	: __in DWORD dwFileSize
// Qualifier	:
// Last Update	: 2017/9/8 - 13:16
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SaveRawImage(__in LPCTSTR szFilePath, __in LPBYTE lpbySrc, __in DWORD dwFileSize)
{
	if ((NULL == lpbySrc) || (0 == dwFileSize))
		return FALSE;

	CFile	fileBin;
	CFileException ex;
	
	try
	{
		if (!fileBin.Open(szFilePath, CFile::modeWrite | CFile::shareExclusive | CFile::modeCreate, &ex))
		{
			return FALSE;
		}

		fileBin.Write(lpbySrc, dwFileSize);
		fileBin.Flush();
	}
	catch (CFileException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
	}
	catch (CMemoryException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
		AfxAbort();
	}

	fileBin.Close();

	return TRUE;
}

//=============================================================================
// Method		: SaveRawImage
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFilePath
// Qualifier	:
// Last Update	: 2017/9/8 - 11:44
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SaveRawImage(__in LPCTSTR szFilePath)
{
	DWORD dwFileSize = m_stStatus.dwHW_XRes * m_stStatus.dwHW_YRes * 2;

	return SaveRawImage(szFilePath, m_pbyFrame, dwFileSize);
}

BOOL CDAQ_LVDS::SaveRawImage_10Bit(__in LPCTSTR szFilePath)
{
	DWORD dwFileSize = m_stStatus.dwXRes * m_stStatus.dwYRes * 2;
	LPBYTE pbyDst = new BYTE[dwFileSize];

	m_ColorConv.Make10BitMode(m_pbyFrame, (LPWORD)pbyDst, m_stStatus.dwXRes, m_stStatus.dwYRes);

	BOOL bReturn = SaveRawImage(szFilePath, pbyDst, dwFileSize);

	delete[] pbyDst;

	return bReturn;
}

BOOL CDAQ_LVDS::SaveRawImage_12Bit(__in LPCTSTR szFilePath)
{
	DWORD dwFileSize = m_stStatus.dwXRes * m_stStatus.dwYRes * 2;
	LPBYTE pbyDst = new BYTE[dwFileSize];

	m_ColorConv.Make12BitMode(m_pbyFrame, (LPWORD)pbyDst, m_stStatus.dwXRes, m_stStatus.dwYRes);

	BOOL bReturn = SaveRawImage(szFilePath, pbyDst, dwFileSize);

	delete[] pbyDst;

	return bReturn;
}

//=============================================================================
// Method		: SetFrameDataFromFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFilePath
// Qualifier	:
// Last Update	: 2018/2/5 - 15:14
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SetFrameDataFromFile(__in LPCTSTR szFilePath)
{
	BOOL bReturn = TRUE;



	return bReturn;
}

//=============================================================================
// Method		: SetI2CData
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/1/11 - 11:46
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::SetI2CData()
{
	BOOL bReturn = TRUE;
	if (FALSE == m_szI2CFilePath.IsEmpty())
	{
		bReturn = SetI2CDataFromFile(m_szI2CFilePath);
	}

	return bReturn;
}

//=============================================================================
// Method		: Set_UseAutoI2CWrite
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Qualifier	:
// Last Update	: 2018/1/11 - 11:42
// Desc.		:
//=============================================================================
void CDAQ_LVDS::Set_UseAutoI2CWrite(__in BOOL bUse)
{
	m_bUseAutoI2CWrite = bUse;
}

//=============================================================================
// Method		: Reset_CaptureInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/11 - 11:51
// Desc.		:
//=============================================================================
void CDAQ_LVDS::Reset_CaptureInfo()
{
	m_stStatus.ResetCapture();
	m_iFrameCount = 1;

	// FPS 계산 초기화
	//InitFrameCount();
}

//=============================================================================
// Method		: Write_RegisterData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nRetry
// Qualifier	:
// Last Update	: 2018/2/6 - 17:35
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Write_RegisterData(__in UINT nRetry /*= 9*/, __in DWORD dwLoopDelay /*= 200*/, __in DWORD dwWeightDelay /*= 100*/)
{
	BOOL	bInit = TRUE;
	UINT	nAddDelay = 0;

	for (UINT nCnt = 0; nCnt <= nRetry; nCnt++)	// 추가 시도회수 9회
	{
		bInit = SetI2CData();

		// Sensor Init 성공하면 루프 빠져나감
		if (bInit)
			break;

		// Sensor Init Fail
		nAddDelay = dwWeightDelay * nCnt;
		Sleep(dwLoopDelay + nAddDelay);
	}

	return bInit;
}

//=============================================================================
// Method		: Check_VideoSignal
// Access		: public  
// Returns		: BOOL
// Parameter	: __in DWORD dwCheckTime
// Parameter	: __in DWORD dwGapDelay
// Qualifier	:
// Last Update	: 2018/2/4 - 16:11
// Desc.		:
//=============================================================================
BOOL CDAQ_LVDS::Check_VideoSignal(__in DWORD dwCheckTime, __in DWORD dwGapDelay /*= 100*/)
{
	DWORD	dwDealy = dwGapDelay;
	UINT	nCheckCount = dwCheckTime / dwDealy;

	nCheckCount = MAX(1, nCheckCount);

	for (UINT nIdx = 0; nIdx < nCheckCount; nIdx++)
	{
		if (m_stStatus.bSignal)
		{
			break;
		}

		Sleep(dwDealy);
	}

	return m_stStatus.bSignal;
}

//=============================================================================
// Method		: Check_FrameCount
// Access		: public  
// Returns		: UINT
// Parameter	: __in UINT nMinFPS
// Parameter	: __in UINT nMaxFPS
// Parameter	: __in DWORD dwCheckTime
// Parameter	: __in DWORD dwGapDelay
// Parameter	: __in UINT nRetry
// Qualifier	:
// Last Update	: 2018/2/4 - 16:23
// Desc.		:
//=============================================================================
UINT CDAQ_LVDS::Check_FrameCount(__in UINT nMinFPS, __in UINT nMaxFPS, __in DWORD dwCheckTime, __in DWORD dwGapDelay /*= 50*/, __in UINT nRetry /*= 5*/)
{
	UINT	nResultFPS	= 0;
	DWORD	dwMeasFPS	= 0;
	DWORD	dwSum		= 0;
	float	fAve		= 0.0f;
	DWORD	dwMin		= 0xFFFF;
	DWORD	dwMax		= 0;

	UINT nMeasCount = dwCheckTime / dwGapDelay;
	nMeasCount = MAX(3, nMeasCount);	// 최소 3회 (min, max 제외 해야 함)

	// ------------------------------------------------------------------------
	for (UINT nIdx = 0; nIdx < nRetry; nIdx++)
	{
		dwMeasFPS	= 0;
		dwSum		= 0;
		fAve		= 0.0f;
		dwMin		= 0xFFFF;
		dwMax		= 0;

		for (UINT nIdx = 0; nIdx < nMeasCount; nIdx++)
		{
			dwMeasFPS = m_stStatus.dwFPS_HW;

			dwSum += dwMeasFPS;

			dwMin = MIN(dwMin, dwMeasFPS);
			dwMax = MAX(dwMax, dwMeasFPS);

			Sleep(dwGapDelay);
		}

		// min/max 튀는 수치는 버림
		dwSum = dwSum - dwMin - dwMax;

		// 22개의 데이터 중 min/max 버리고 20개 데이터의 평균 구함
		fAve = (float)dwSum / (float)(nMeasCount - 2);

		TRACE(_T("FPS CH_%02d Average : %.2f"), m_nBoardNumber, fAve);

		nResultFPS = (UINT)fAve;

		if ((nMinFPS <= nResultFPS) && (nResultFPS <= nMaxFPS))
		{
		 	break;
		}
	}
	// ------------------------------------------------------------------------

	return nResultFPS;
}

//=============================================================================
// Method		: Set_RGB_Exposure
// Access		: public  
// Returns		: void
// Parameter	: __in DOUBLE dExposure
// Qualifier	:
// Last Update	: 2018/2/5 - 17:06
// Desc.		:
//=============================================================================
void CDAQ_LVDS::Set_RGB_Exposure(__in DOUBLE dExposure /*= 0.0f*/)
{
	m_stOption.SetExposure(dExposure);
}

//=============================================================================
// Method		: Set_RGB_ContrastBrightness
// Access		: public  
// Returns		: void
// Parameter	: __in char chContrast
// Parameter	: __in char chBrightness
// Qualifier	:
// Last Update	: 2018/2/5 - 17:06
// Desc.		:
//=============================================================================
void CDAQ_LVDS::Set_RGB_ContrastBrightness(__in char chContrast /*= 0*/, __in char chBrightness /*= 0*/)
{
	m_stOption.SetContrastBrightness(chContrast, chBrightness);
}


void CDAQ_LVDS::VideoChangeMode(BOOL bMode){
	m_stStatus.bVideoChangeMode = bMode;
}