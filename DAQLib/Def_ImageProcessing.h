﻿//*****************************************************************************
// Filename	: 	Def_ImageProcessing.h
// Created	:	2017/5/22 - 14:23
// Modified	:	2017/5/22 - 14:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_ImageProcessing_h__
#define Def_ImageProcessing_h__


// Flip 종류
typedef enum enFlipType
{
	Flip_None,
	Flip_Horizontal,	// 좌우반전
	Flip_Vertical,		// 상하반전
	Flip_Horz_Vert,
};

typedef enum enRotateType
{
	Rotate_CW_None,
	Rotate_CW_90,
	Rotate_CW_180,
	Rotate_CW_270,
};

typedef struct _tag_IP_Option
{
	enFlipType		Flip;	// Flip
	enRotateType	Rotate;	// Rotate
	// Invert
	// Brighten
	// Huerotae
	// Contrast
	// Grayscale
	// Blur
	// Sharpen

	_tag_IP_Option()
	{
		Flip	= Flip_None;
		Rotate	= Rotate_CW_None;
	}
}ST_IP_Option, *PST_IP_Option;

#endif // Def_ImageProcessing_h__
