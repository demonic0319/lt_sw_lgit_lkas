﻿//*****************************************************************************
// Filename	: 	VisionCam.h
// Created	:	2016/5/10 - 15:25
// Modified	:	2016/5/10 - 15:25
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef VisionCam_h__
#define VisionCam_h__

#pragma once

#include <PvSystem.h>
#include <PvNetworkAdapter.h>
#include <PvDevice.h>
#include <PvGenParameter.h>
#include <PvBuffer.h>
#include <PvStream.h>
#include <PvPipeline.h>
#include <PvAcquisitionStateManager.h>
#include <PvDeviceGEV.h>
#include <PvDeviceInfoGEV.h>
//#include <PvDeviceInfoU3V.h>
#include <PvStreamGEV.h>

typedef enum enAcqMode
{
	AcqMode_Continuous,
	AcqMode_SingleFrame,
	AcqMode_MultiFrame,
	AcqMode_ContinuousRecording,
	AcqMode_ContinuousReadout,
	AcqMode_SingleFrameRecording,
	AcqMode_SingleFrameReadout,
};

static LPCTSTR g_szAcqMode[] =
{
	_T("Continuous"),
	_T("SingleFrame"),
	_T("MultiFrame"),
	_T("ContinuousRecording"),
	_T("ContinuousReadout"),
	_T("SingleFrameRecording"),
	_T("SingleFrameReadout"),
	NULL
};



//-----------------------------------------------------------------------------
// CVisionCam
//-----------------------------------------------------------------------------
class CVisionCam : public PvGenEventSink, PvAcquisitionStateEventSink, PvDeviceEventSink, PvStreamEventSink
{
public:
	CVisionCam();
	virtual ~CVisionCam();

	enum enCommStatus
	{
		COMM_NOTCONNECT = 0,
		COMM_CONNECTED,
		COMM_DISCONNECT,
		COMM_CONNECT_DROP,
		COMM_CONNECT_ERROR,
	};

protected:

	PvDevice*					m_pDevice;
	PvStream*					m_pStream;
	PvPipeline*					m_pPipeline;
	PvAcquisitionStateManager*	m_pAcqStateMgr;
	
	int64_t		m_lAcquisitionMode;	// 데이터 수집 모드

	PvString	m_szConnectionID;

	enCommStatus	m_CommStatus;
	BOOL		m_bConnectionLost;	// 연결 끊김
	BOOL		m_bConnected;		// 연결 상태
	BOOL		m_bLocked;			// 잠금



	uint32_t	m_lDevGEVCnt;		// GigE Vision device 개수

	PvBuffer*	m_pvBuf;

	//해상도
	int			m_nWidth;
	int			m_nHeight;
	int64_t		m_nPixelFormat;
	int			m_nPixelSize;

	LPBYTE		m_lpbyMono8;
	uint32_t	m_Mono8Size;

	LPBYTE		m_lpbyRGB32;
	uint32_t	m_RGB32Size;

	double_t	m_dExposureTime;

	// 재정의 -------------------------------------------------------
	// PvGenEventSink implementation
	virtual void	OnParameterUpdate			(PvGenParameter *aParameter);
	// PvAcquisitionStateEventSink implementation
	virtual void	OnAcquisitionStateChanged	(PvDevice* aDevice, PvStream* aStream, uint32_t aSource, PvAcquisitionState aState);
	// PvStreamEventSink implementation
	virtual void	OnBufferQueued				(PvBuffer *  aBuffer);
	virtual void	OnBufferRetrieved			(PvBuffer *  aBuffer);
	// PvDeviceEventSink implementation
	virtual void	OnLinkDisconnected			(PvDevice* aDevice);
	virtual void	OnLinkReconnected			(PvDevice* aDevice);
	virtual void	OnEvent						(PvDevice* aDevice, uint16_t  aEventID, uint16_t  aChannel, uint64_t  aBlockID, uint64_t  aTimestamp, const void *  aData, uint32_t  aDataLength);

	//---------------------------------------------------------------
	void	Loop();

	void	SetListAcquisitionMode	(PvGenEnum *lMode);
	void	ChangeAcquisitionMode	(uint64_t lValue);
	void	AcquireImages			(PvDevice *aDevice, PvStream *aStream, PvPipeline *aPipeline);
	void	EnableInterface			();

	
	const PvDeviceInfo*	SelectDevice	(PvSystem *aPvSystem);

 	BOOL	ConnectDevice();
	void 	DisconnectDevice();
 	BOOL	OpenStream();
	void 	CloseStream();
	void 	TearDown(bool aStopAcquisition);
	BOOL	StartAcquisition();
	BOOL	StopAcquisition();

	// 통신 끊김 체크하여 재 연결 시도
	BOOL	CheckConnection();
	BOOL	m_bLoopThread;
	HANDLE	m_hEvent_ExitLoop;
	HANDLE	m_hThreadLoop;

	static UINT WINAPI	Thread_Loop(LPVOID lParam);
	void	CreateThread_Loop();
	void	DeleteThread_Loop();


	BOOL	m_bAcquiringImages;
	BOOL	ConvertMonoToRGB32		(__in const LPBYTE pbyInMono, __in DWORD dwBufSize);

	HWND	m_hOwnerWnd;
	UINT	m_nWM_CommStatus;
	UINT	m_nWM_RecvVideo;
	BYTE	m_byDeviceType;
	void	SendWM_CommStatus		(enCommStatus nStatus);
	void	SendWM_RecvVideo		();

	BOOL	SaveImage( LPCTSTR szPath, PvBuffer *aBuffer, bool aUpdateStats );

public:

	BOOL	IsConnected()
	{	
			return m_bConnected;
	};

	void	SetOwner				(HWND hOwnerWnd);
	void	SetWinMessage			(UINT nWM_CommStatus, UINT nWM_RecvVideo);
	void	SetDeviceType			(BYTE byType);

	// GigE Vision 장비 찾기
	int		DeviceFinding			();

	void	ConnectAndRun			(LPCTSTR szIPAddress);
	void	DisconnectAndExit		();

	BOOL	Connect					(LPCTSTR szIPAddress);	
	void	Disconnect				();

	void	Start_AcquisitionMgr	();
	void	Stop_AcquisitionMgr		();
	void	StartStreaming			();
	void	StopStreaming			();

	// Single Frame 용도
	void	Start					();
	void	Stop					();
	
	LPBYTE	GetAcquiredImage		();
	BOOL	GetAcquiredImage		(__out LPBYTE& lpbyOutBuf, __out DWORD& dwWidth, __out DWORD& dwHeight);

	double GetColletAngle(BYTE *Original_image, int Width, int Height);
};

#endif // VisionCam_h__

