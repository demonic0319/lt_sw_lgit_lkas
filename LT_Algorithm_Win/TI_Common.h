//*****************************************************************************
// Filename	: 	TI_Common.h
// Created	:	2018/2/10 - 16:11
// Modified	:	2018/2/10 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef TI_Common_h__
#define TI_Common_h__

#pragma once

#include <math.h>
#include "cv.h"
#include "highgui.h"

typedef enum enMark_Color
{
	MarkCol_White = 0,
	MarkCol_Black,
	MarkCol_Max,
};

#endif // TI_Common_h__
