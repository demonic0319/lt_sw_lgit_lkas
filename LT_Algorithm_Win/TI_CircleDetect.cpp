﻿#include <afxwin.h>
#include "TI_CircleDetect.h"


CTI_CircleDetect::CTI_CircleDetect()
{
}

CTI_CircleDetect::~CTI_CircleDetect()
{
}

//=============================================================================
// Method		: CircleDetect_Test
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPBYTE IN_RGB
// Parameter	: __in int iWidth
// Parameter	: __in int iHeight
// Parameter	: __in CRect rtROI
// Parameter	: __in int iThresBright
// Parameter	: __out int & iPosX
// Parameter	: __out int & iPosY
// Parameter	: __in int iColor
// Parameter	: __in unsigned int nSharpness
// Qualifier	:
// Last Update	: 2018/3/10 - 16:26
// Desc.		:
//=============================================================================
BOOL CTI_CircleDetect::CircleDetect_Test(__in LPBYTE IN_RGB, __in int iWidth, __in int iHeight, __in CRect rtROI, __in int iThresBright, __out int &iPosX, __out int &iPosY, __in int iColor /*= 0*/, __in unsigned int nSharpness /*= 0*/)
{
	BOOL bResult = FALSE;

	IplImage *RGBOrgImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);
	IplImage *GrayImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
	IplImage *GrayImage_SharpnessCal = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);

	BYTE R, G, B;

	int iStartX = rtROI.left;
	int iStartY = rtROI.top;

	int iEndX = rtROI.right;
	int iEndY = rtROI.bottom;

	for (int y = 0; y < iHeight; y++)
	{
		for (int x = 0; x < iWidth; x++)
		{
			B = IN_RGB[y * (iWidth * 3) + x * 3 + 0];
			G = IN_RGB[y * (iWidth * 3) + x * 3 + 1];
			R = IN_RGB[y * (iWidth * 3) + x * 3 + 2];

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	// Gray Scale
	cvCvtColor(RGBOrgImage, GrayImage, CV_BGR2GRAY);
	cvCopy(GrayImage, GrayImage_SharpnessCal);
	cvSmooth(GrayImage, GrayImage);
	cvSmooth(GrayImage, GrayImage);
	cvSmooth(GrayImage, GrayImage);

	// 이진화
	if (iThresBright == 0)
	{
		cvThreshold(GrayImage, GrayImage, iThresBright, 255, CV_THRESH_OTSU);
	}
	else{
		cvThreshold(GrayImage, GrayImage, iThresBright, 255, CV_THRESH_BINARY);
	}

	if (iColor == MarkCol_Black)
	{
		cvNot(GrayImage, GrayImage);
	}

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(GrayImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;
	int iCounter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
	{
		iCounter++;
	}

	CvRect rect;
	double area = 0, arcCount = 0;
	CvPoint nCenterPoint;

	double dbMin_Dist = 99999;
	double dbDist = 0;
	double circularity = 0;
	int rtCenterX = 0;
	int rtCenterY = 0;
	CvPoint StartPoint;
	CvPoint EndPoint;

	// 오브젝트가 있는경우
	if (iCounter != 0)
	{
		for (; contour != 0; contour = contour->h_next)
		{
			rect = cvContourBoundingRect(contour, 1);
			area = cvContourArea(contour, CV_WHOLE_SEQ);
			arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);
			circularity = (4.0 * 3.14 * area) / (arcCount*arcCount);

			rtCenterX = rect.x + rect.width / 2;
			rtCenterY = rect.y + rect.height / 2;

			StartPoint.x = rect.x;
			StartPoint.y = rect.y;

			EndPoint.x = rect.x + rect.width;
			EndPoint.y = rect.y + rect.height;

			cvRectangle(RGBOrgImage, StartPoint, EndPoint, CV_RGB(255, 255, 0), 1);

			if (rect.x > iStartX && rect.x + rect.width < iEndX
				&& rect.y > iStartY && rect.y + rect.height < iEndY)
			{
				dbDist = GetDistance(iStartX + rtROI.Width() / 2, iStartY + rtROI.Height() / 2, rtCenterX, rtCenterY);

				//원에 가깝고
				if (circularity > 0.8)
				{
					// 이미지 중심에서 제일 가까운 오브젝트 탐색
					if (dbMin_Dist > dbDist)
					{
						dbMin_Dist = dbDist;

						nCenterPoint.x = rtCenterX;
						nCenterPoint.y = rtCenterY;

						//됐다!
						iPosX = nCenterPoint.x;
						iPosY = nCenterPoint.y;
						bResult = TRUE;

						StartPoint.x = nCenterPoint.x - rect.width / 2;
						StartPoint.y = nCenterPoint.y - rect.height / 2;

						EndPoint.x = nCenterPoint.x + rect.width / 2;
						EndPoint.y = nCenterPoint.y + rect.height / 2;
						cvRectangle(RGBOrgImage, StartPoint, EndPoint, CV_RGB(255, 0, 0), 4);
					}
				}
			}
		}
	}
	else
	{
		iPosX = 0;
		iPosY = 0;
		bResult = FALSE;
	}

	if ( nSharpness != 0)
	{
		if (iPosX != 0 && iPosY != 0)
		{
			StartPoint.x = iPosX - 100;
			StartPoint.y = iPosY - 100;

			EndPoint.x = iPosX + 100;
			EndPoint.y = iPosY + 100;

			cvNormalize(GrayImage_SharpnessCal, GrayImage_SharpnessCal, 0, 255, CV_MINMAX);
			double STDdev = 0, AVG = 0, Dev = 0;
			CvScalar Tmp;
			int _c = 0;
			//Pre-Focus 광축 샤프니스
			for (int j = StartPoint.y; j < EndPoint.y; j++)
			{
				for (int i = StartPoint.x; i < EndPoint.x; i++)
				{
					if (i >= 0 && i < GrayImage_SharpnessCal->width && j >= 0 && j < GrayImage_SharpnessCal->height)
					{
						Tmp = cvGet2D(GrayImage_SharpnessCal, j, i);
						AVG += Tmp.val[0];
						_c++;
					}
				}
			}
			if (_c)
			{
				AVG /= _c;

				//분산
				for (int j = StartPoint.y; j < EndPoint.y; j++)
				{
					for (int i = StartPoint.x; i < EndPoint.x; i++)
					{
						if (i >= 0 && i < GrayImage_SharpnessCal->width && j >= 0 && j < GrayImage_SharpnessCal->height)
						{
							Tmp = cvGet2D(GrayImage_SharpnessCal, j, i);
							Dev += abs(Tmp.val[0] - AVG);
						}
					}
				}

				// 편차 평균 
				STDdev = Dev / _c;

				if (STDdev > nSharpness)
				{
					bResult = TRUE;
				}
				else
				{
					iPosX = 0;
					iPosY = 0;
					bResult = FALSE;
				}

			}
			else
			{
				AVG = -999;
				bResult = FALSE;
			}
		}
		else
		{
			iPosX = 0;
			iPosY = 0;
			bResult = FALSE;
		}
	}



	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&GrayImage_SharpnessCal);
	cvReleaseImage(&GrayImage);

	return bResult;
}

//=============================================================================
// Method		: GetDistance
// Access		: public  
// Returns		: double
// Parameter	: __in int iSrcX
// Parameter	: __in int iSrcY
// Parameter	: __in int iDstX
// Parameter	: __in int iDstY
// Qualifier	:
// Last Update	: 2018/2/27 - 19:58
// Desc.		:
//=============================================================================
double CTI_CircleDetect::GetDistance(__in int iSrcX, __in int iSrcY, __in int iDstX, __in int iDstY)
{
	return sqrt((double)((iDstX - iSrcX) * (iDstX - iSrcX) + (iDstY - iSrcY) * (iDstY - iSrcY)));
}


//=============================================================================
// Method		: RectDetect_Test
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPBYTE IN_RGB
// Parameter	: __in int iWidth
// Parameter	: __in int iHeight
// Parameter	: __in CRect rtROI
// Parameter	: __in int iThresBright
// Parameter	: __out int & iPosX
// Parameter	: __out int & iPosY
// Parameter	: __in int iColor
// Parameter	: __in unsigned int nSharpness
// Qualifier	:
// Last Update	: 2018/3/10 - 16:26
// Desc.		:
//=============================================================================
BOOL CTI_CircleDetect::RectDetect_Test(__in LPBYTE IN_RGB, __in int iWidth, __in int iHeight, __in CRect rtROI, __in int iThresBright, __out int &iPosX, __out int &iPosY, __in int iColor /*= 0*/, __in unsigned int nSharpness /*= 0*/)
{
	BOOL bResult = FALSE;

	IplImage *RGBOrgImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 3);
	IplImage *GrayImage = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);
	IplImage *GrayImage_SharpnessCal = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 1);

	BYTE R, G, B;

	int iStartX = rtROI.left;
	int iStartY = rtROI.top;

	int iEndX = rtROI.right;
	int iEndY = rtROI.bottom;

	for (int y = 0; y < iHeight; y++)
	{
		for (int x = 0; x < iWidth; x++)
		{
			B = IN_RGB[y * (iWidth * 3) + x * 3 + 0];
			G = IN_RGB[y * (iWidth * 3) + x * 3 + 1];
			R = IN_RGB[y * (iWidth * 3) + x * 3 + 2];

			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 0] = B;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 1] = G;
			RGBOrgImage->imageData[y*RGBOrgImage->widthStep + 3 * x + 2] = R;
		}
	}

	// Gray Scale
	cvCvtColor(RGBOrgImage, GrayImage, CV_BGR2GRAY);
	cvCopy(GrayImage, GrayImage_SharpnessCal);
	cvSmooth(GrayImage, GrayImage);
	cvSmooth(GrayImage, GrayImage);
	cvSmooth(GrayImage, GrayImage);

	// 이진화
	if (iThresBright == 0)
	{
		cvThreshold(GrayImage, GrayImage, iThresBright, 255, CV_THRESH_OTSU);
	}
	else{
		cvThreshold(GrayImage, GrayImage, iThresBright, 255, CV_THRESH_BINARY);
	}

	if (iColor == MarkCol_Black)
	{
		cvNot(GrayImage, GrayImage);
	}

	CvMemStorage* contour_storage = cvCreateMemStorage(0);
	CvSeq *contour = 0;
	CvSeq *temp_contour = 0;

	cvFindContours(GrayImage, contour_storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	temp_contour = contour;
	int iCounter = 0;

	for (; temp_contour != 0; temp_contour = temp_contour->h_next)
	{
		iCounter++;
	}

	CvRect rect;
	double area = 0, arcCount = 0;
	CvPoint nCenterPoint;

	double dbMin_Dist = 99999;
	double dbDist = 0;
	double circularity = 0;
	int rtCenterX = 0;
	int rtCenterY = 0;
	CvPoint StartPoint;
	CvPoint EndPoint;

	// 오브젝트가 있는경우
	if (iCounter != 0)
	{
		for (; contour != 0; contour = contour->h_next)
		{
			rect = cvContourBoundingRect(contour, 1);
			area = cvContourArea(contour, CV_WHOLE_SEQ);
			arcCount = cvArcLength(contour, CV_WHOLE_SEQ, -1);
			circularity = (4.0 * 3.14 * area) / (arcCount*arcCount);

			rtCenterX = rect.x + rect.width / 2;
			rtCenterY = rect.y + rect.height / 2;

			StartPoint.x = rect.x;
			StartPoint.y = rect.y;

			EndPoint.x = rect.x + rect.width;
			EndPoint.y = rect.y + rect.height;

			cvRectangle(RGBOrgImage, StartPoint, EndPoint, CV_RGB(255, 255, 0), 1);

			if (rect.x > iStartX && rect.x + rect.width < iEndX
				&& rect.y > iStartY && rect.y + rect.height < iEndY)
			{
				dbDist = GetDistance(iStartX + rtROI.Width() / 2, iStartY + rtROI.Height() / 2, rtCenterX, rtCenterY);

				//원에 가깝고
				if (circularity > 0.6)
				{
					// 이미지 중심에서 제일 가까운 오브젝트 탐색
					if (dbMin_Dist > dbDist)
					{
						dbMin_Dist = dbDist;

						nCenterPoint.x = rtCenterX;
						nCenterPoint.y = rtCenterY;

						//됐다!
						iPosX = nCenterPoint.x;
						iPosY = nCenterPoint.y;
						bResult = TRUE;

						StartPoint.x = nCenterPoint.x - rect.width / 2;
						StartPoint.y = nCenterPoint.y - rect.height / 2;

						EndPoint.x = nCenterPoint.x + rect.width / 2;
						EndPoint.y = nCenterPoint.y + rect.height / 2;
						cvRectangle(RGBOrgImage, StartPoint, EndPoint, CV_RGB(255, 0, 0), 4);
					}
				}
			}
		}
	}
	else
	{
		iPosX = 0;
		iPosY = 0;
		bResult = FALSE;
	}

	if (nSharpness != 0)
	{
		if (iPosX != 0 && iPosY != 0)
		{
			StartPoint.x = iPosX - 100;
			StartPoint.y = iPosY - 100;

			EndPoint.x = iPosX + 100;
			EndPoint.y = iPosY + 100;

			cvNormalize(GrayImage_SharpnessCal, GrayImage_SharpnessCal, 0, 255, CV_MINMAX);
			double STDdev = 0, AVG = 0, Dev = 0;
			CvScalar Tmp;
			int _c = 0;
			//Pre-Focus 광축 샤프니스
			for (int j = StartPoint.y; j < EndPoint.y; j++)
			{
				for (int i = StartPoint.x; i < EndPoint.x; i++)
				{
					if (i >= 0 && i < GrayImage_SharpnessCal->width && j >= 0 && j < GrayImage_SharpnessCal->height)
					{
						Tmp = cvGet2D(GrayImage_SharpnessCal, j, i);
						AVG += Tmp.val[0];
						_c++;
					}
				}
			}
			if (_c)
			{
				AVG /= _c;

				//분산
				for (int j = StartPoint.y; j < EndPoint.y; j++)
				{
					for (int i = StartPoint.x; i < EndPoint.x; i++)
					{
						if (i >= 0 && i < GrayImage_SharpnessCal->width && j >= 0 && j < GrayImage_SharpnessCal->height)
						{
							Tmp = cvGet2D(GrayImage_SharpnessCal, j, i);
							Dev += abs(Tmp.val[0] - AVG);
						}
					}
				}

				// 편차 평균 
				STDdev = Dev / _c;

				if (STDdev > nSharpness)
				{
					bResult = TRUE;
				}
				else
				{
					iPosX = 0;
					iPosY = 0;
					bResult = FALSE;
				}

			}
			else
			{
				AVG = -999;
				bResult = FALSE;
			}
		}
		else
		{
			iPosX = 0;
			iPosY = 0;
			bResult = FALSE;
		}
	}



	cvReleaseMemStorage(&contour_storage);
	cvReleaseImage(&RGBOrgImage);
	cvReleaseImage(&GrayImage_SharpnessCal);
	cvReleaseImage(&GrayImage);

	return bResult;
}