﻿#ifndef TI_CircleDetect_h__
#define TI_CircleDetect_h__

#pragma once

#include <windows.h>

#include "TI_Common.h"

class CTI_CircleDetect 
{
public:
	CTI_CircleDetect();
	virtual ~CTI_CircleDetect();

	// 측정알고리즘
	BOOL	CircleDetect_Test		(__in LPBYTE IN_RGB, __in int iWidth, __in int iHeight, __in CRect rtROI, __in int iThresBright, __out int &iPosX, __out int &iPosY, __in int iColor = 0, __in unsigned int nSharpness = 0);
	BOOL	RectDetect_Test			(__in LPBYTE IN_RGB, __in int iWidth, __in int iHeight, __in CRect rtROI, __in int iThresBright, __out int &iPosX, __out int &iPosY, __in int iColor = 0, __in unsigned int nSharpness = 0);

	double  GetDistance				(__in int iSrcX, __in int iSrcY, __in int iDstX, __in int iDstY);
};

#endif // TI_CircleDetect_h__
