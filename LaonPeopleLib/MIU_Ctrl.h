#pragma once

#include <cv.h>
#include <highgui.h>
#include "LPMC500DLLEx.h"

#define MIU_MONO							0x01000000
#define	MIU_BAYER							0x02000000
#define	MIU_YUV								0x03000000
#define	MIU_RGB								0x04000000

#define MIU_OCCUPY8BIT						0x00080000
#define MIU_OCCUPY10BIT						0x000A0000
#define MIU_OCCUPY12BIT						0x000C0000
#define MIU_OCCUPY16BIT						0x00100000
#define MIU_OCCUPY24BIT						0x00180000

#define MIU_MONO8							(MIU_MONO	|	MIU_OCCUPY8BIT	|	0x0001)
#define	MIU_MONO10_PACKED					(MIU_MONO	|	MIU_OCCUPY10BIT	|   0x0002)
#define MIU_MONO12_PACKED					(MIU_MONO	|	MIU_OCCUPY12BIT	|   0x0003)
#define MIU_MONO14                          (MIU_MONO   |   MIU_OCCUPY16BIT |   0x0004)

#define MIU_BAYERGR8						(MIU_BAYER	|	MIU_OCCUPY8BIT	|	0x0001)
#define MIU_BAYERRG8						(MIU_BAYER	|	MIU_OCCUPY8BIT	|	0x0002)
#define MIU_BAYERGB8						(MIU_BAYER	|	MIU_OCCUPY8BIT	|   0x0003)
#define	MIU_BAYERBG8						(MIU_BAYER	|	MIU_OCCUPY8BIT	|   0x0004)

#define MIU_BAYERGR10_PACKED				(MIU_BAYER	|	MIU_OCCUPY10BIT	|	0x0005)
#define MIU_BAYERRG10_PACKED				(MIU_BAYER	|	MIU_OCCUPY10BIT	|	0x0006)
#define MIU_BAYERGB10_PACKED				(MIU_BAYER	|	MIU_OCCUPY10BIT	|   0x0007)
#define	MIU_BAYERBG10_PACKED				(MIU_BAYER	|	MIU_OCCUPY10BIT	|   0x0008)

#define MIU_BAYERGR12_PACKED				(MIU_BAYER	|	MIU_OCCUPY12BIT	|	0x0009)
#define MIU_BAYERRG12_PACKED				(MIU_BAYER	|	MIU_OCCUPY12BIT	|	0x000A)
#define MIU_BAYERGB12_PACKED				(MIU_BAYER	|	MIU_OCCUPY12BIT	|   0x000B)
#define	MIU_BAYERBG12_PACKED				(MIU_BAYER	|	MIU_OCCUPY12BIT	|   0x000C)

#define MIU_RGB565							(MIU_RGB	|	MIU_OCCUPY16BIT	|	0x0001)
#define MIU_BGR565							(MIU_RGB	|	MIU_OCCUPY16BIT	|	0x0002)
#define	MIU_RGB8_PACKED						(MIU_RGB	|	MIU_OCCUPY24BIT	|	0x0001)
#define	MIU_BGR8_PACKED						(MIU_RGB	|	MIU_OCCUPY24BIT	|	0x0002)

#define	MIU_YUV422_PACKED					(MIU_YUV	|	MIU_OCCUPY16BIT	|	0x0001)
#define	MIU_YUV422_YUYV_PACKED				(MIU_YUV	|	MIU_OCCUPY16BIT	|	0x0002)

typedef struct strMIUDevice
{
	unsigned char		bMIUOpen;
	MIU_INITIALValue	InitialValue;
	unsigned char		CurrentState;
	unsigned short		iSensorID;			// Sensor Product ID
	unsigned short		iSensorType;		// Sensor Resolution
	unsigned int		iPixelFormat;		// Sensor Out Format
	unsigned short		nMaxWidth;			// Max Width
	unsigned short		nMaxHeight;			// Max Height   
	unsigned short		nWidth;				// Sensor Width
	unsigned short		nHeight;			// Sensor Height    
	unsigned short		nBitPerPixel;		// Bit Per Pixel
	unsigned int		nFrameImageSize;	// 1 Frame Image size  ( PAYLOAD_SIZE) 
	unsigned int		nFrameRawSize;		// 1 Frame RAW size (Only Data Size)

	IplImage*			imageItp;			// Display 
	IplImage*			image12bitRaw;		// pack raw = 12bit 
	IplImage*			image16bitRaw;		// uppack raw = 16bit 
	IplImage*			image;				// Image Receive
	IplImage*			imageResize;		// Image Resize
	IplImage*			ColorConvertItp;	// Color Convert
	char				szWindowName[256];

	//COSTestDlg*			pOSTestWindow;		// OS Test window

	unsigned char		fResizeFlag;
	unsigned short		nDisplayWidth;  // Display Width
	unsigned short		nDisplayHeight; // Display Height

	unsigned char		iImageSave;     // Image 저장 관련 flag 0:저장 안함, 1: Still Capture, 2:동영상 저장 
	unsigned char		fSaveNewFrame;  // 저장할 Image가 있는지 알려주는 flag
	unsigned char		bRCCC;

	BOOL				bSignal;

} MIU_DEVICE, *PMIU_DEVICE;



class CMIU_Ctrl
{
public:
	CMIU_Ctrl();
	~CMIU_Ctrl();

	BOOL DeviceOpen();
	BOOL DeviceClose();

	BOOL ISPOnOff(__in BOOL bOnOff);
	BOOL Initialize_OV10642();
	BOOL Initialize_AR0220();
	BOOL SensorInit(__in CStringA sziniFile);

	BOOL ChangeMono();
	BOOL ChangeRaw();
	
	BOOL RegisterWrite(UINT nType, unsigned short nAddress, unsigned short nData);

	BOOL InitCalibration();
	BOOL CurrentMeasureModeSelect(__in UINT nMode, __out double* dVal);
	BOOL CurrentMeasure_Dynamic(__out double* dVal);
	BOOL CurrentMeasure(__in UINT nSensorType, __in CStringA sziniFile, __out double* dValue);

	BOOL Start();
	BOOL Stop();

	BOOL m_bSignal;

	IplImage* m_pDisplayimage;
	IplImage* m_praw12image;
	IplImage* m_praw16image;
	IplImage* m_pimageQ;


	IplImage* GetDisplayImage()
	{
		return m_pDisplayimage;
	};

	IplImage* GetRaw12Image()
	{
		return m_praw12image;
	};

	IplImage* GetRaw16Image()
	{
		return m_praw16image;
	};

	BOOL GetVideoSignalStatus()
	{
		return m_bSignal;
	}

	UINT m_nWM_ChgStatus = NULL;		// 카메라 영상 상태 변경시 처리 메세지
	UINT m_nWM_RecvVideo = NULL;		// 비데오 영상 수신 메세지
	HWND m_hOwnerWnd = NULL;			// 이 클래스를 호출한 윈도우 핸들	

	// 오너 핸들
	void SetOwnerHwnd(HWND hOwnerWnd)
	{
		m_hOwnerWnd = hOwnerWnd;
	};

	// 카메라 영상 상태 변경시 처리 메세지
	void SetWM_ChgStatus(__in UINT nWinMsg)
	{
		m_nWM_ChgStatus = nWinMsg;
	};

	// 비디오 영상 수신 메세지
	void SetWM_RecvVideo(__in UINT nWinMsg)
	{
		m_nWM_RecvVideo = nWinMsg;
	}

	CWinThread* m_pGrabThread = NULL;
	volatile bool m_isThreadRun_Grab;

	static UINT GrabThread(LPVOID _method);
	void CreateThread();
	void DestroyThread();
	UINT ThreadFunction();

protected:
	void Shift16to12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight);
	void Shift16to8BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight);
	void Shift12to16BitMode(unsigned char* p12bitRaw, unsigned char* p16bitRaw, int nWidth, int nHeight);
	void CCCR_Interpolation(unsigned short* pData, int nWidth, int nHeight, BOOL bRCCC_mx, BOOL bRCCC_my, BOOL bChkDir);

	void ConvertColor(unsigned int iPixelFormat, IplImage* pSrc, IplImage* pDest, IplImage* pTemp, unsigned int nWidth, unsigned int nHeight);
	void Shift12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight);
	void Shift10BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight);

	void RawToBmpQuater(unsigned char *pRaw8, unsigned char *pBMP, int image_width, int image_height, int ch);
	void PRI_12BitToRaw8(unsigned char *p12Bit, unsigned char *pRawBit8, int w, int h);
};

