#include "MIU_Ctrl.h"

#define MAX_DEVICE		4
#define BUFFER_COUNT	7

char gDeviceCount = 0;
char gDeviceIndex = 0;
MIU_DEVICE gMIUDevice[MAX_DEVICE];
unsigned int gMIUOriginImageFormat[MAX_DEVICE];

void CALLBACK USBEventHandler(char iIndex, int Event)
{
	switch (Event)
	{
	case MIU_EVENT_TIMEOUT:
		TRACE("USB Restart Event : board number %d , evnet %d\n", iIndex, Event);
		break;

	case MIU_EVENT_ATTACHED:
		TRACE("USB attached Event \n");
	//	AfxMessageBox("attached");
		break;

	case MIU_EVNET_DETACHED:
		TRACE("USB detached Event\n");
	//	AfxMessageBox("detached");
		break;

	default:

		break;
	}
}


CMIU_Ctrl::CMIU_Ctrl()
{
	
}


CMIU_Ctrl::~CMIU_Ctrl()
{
}


UINT CMIU_Ctrl::GrabThread(LPVOID _method)
{
	CMIU_Ctrl* pClass = (CMIU_Ctrl*)_method;

	int iReturn = pClass->ThreadFunction();

	return 0;
}


void CMIU_Ctrl::CreateThread()
{
	if (m_pGrabThread != NULL)
	{
		// 이미 실행중..
		return;
	}

	m_pGrabThread = AfxBeginThread(GrabThread, this, THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);

	if (m_pGrabThread == NULL)
	{
		// 생성 실패..
	}

	m_pGrabThread->m_bAutoDelete = FALSE;
	m_pGrabThread->ResumeThread();
}


void CMIU_Ctrl::DestroyThread()
{
	if (m_pGrabThread != NULL)
	{
		DWORD dwResult = ::WaitForSingleObject(m_pGrabThread->m_hThread, 2000);

		if (dwResult == WAIT_TIMEOUT)
		{
			//
		}
		else if (dwResult == WAIT_OBJECT_0)
		{
			//
		}

		delete m_pGrabThread;
		m_pGrabThread = NULL;
	}
}


UINT CMIU_Ctrl::ThreadFunction()
{
	while (m_isThreadRun_Grab)
	{
		if (!m_isThreadRun_Grab)
			break;

		INT64 TimeStamp;
		int ErrorCode = MIUGetImageData(gDeviceIndex, (unsigned char**)&gMIUDevice[gDeviceIndex].image->imageData, &TimeStamp);

		if (ErrorCode == MIU_NEW_FRAME)
		{
			if (gMIUDevice[gDeviceIndex].bRCCC)
			{
				Shift12to16BitMode((unsigned char*)gMIUDevice[gDeviceIndex].image->imageData,
					(unsigned char*)gMIUDevice[gDeviceIndex].image16bitRaw->imageData,
					gMIUDevice[gDeviceIndex].nWidth,
					gMIUDevice[gDeviceIndex].nHeight);

				CCCR_Interpolation((unsigned short*)gMIUDevice[gDeviceIndex].image16bitRaw->imageData,
					gMIUDevice[gDeviceIndex].nWidth,
					gMIUDevice[gDeviceIndex].nHeight,
					FALSE,
					FALSE,
					FALSE
					);

				Shift16to12BitMode((unsigned char*)gMIUDevice[gDeviceIndex].image16bitRaw->imageData,
					(unsigned char*)gMIUDevice[gDeviceIndex].image12bitRaw->imageData,
					gMIUDevice[gDeviceIndex].nWidth,
					gMIUDevice[gDeviceIndex].nHeight);

				ConvertColor(gMIUDevice[gDeviceIndex].iPixelFormat,
					gMIUDevice[gDeviceIndex].image12bitRaw,
					gMIUDevice[gDeviceIndex].imageItp,
					gMIUDevice[gDeviceIndex].ColorConvertItp,
					gMIUDevice[gDeviceIndex].nWidth,
					gMIUDevice[gDeviceIndex].nHeight);
			}
			else
			{
				ConvertColor(gMIUDevice[gDeviceIndex].iPixelFormat, gMIUDevice[gDeviceIndex].image, gMIUDevice[gDeviceIndex].imageItp,
					gMIUDevice[gDeviceIndex].ColorConvertItp, gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight);
			}
			
			m_pDisplayimage = gMIUDevice[gDeviceIndex].imageItp;
			m_praw12image = gMIUDevice[gDeviceIndex].image12bitRaw;

			// quater
			m_praw16image = gMIUDevice[gDeviceIndex].image16bitRaw;
// 			PRI_12BitToRaw8(
// 				(unsigned char *)gMIUDevice[gDeviceIndex].image12bitRaw->imageData,
// 				(unsigned char *)gMIUDevice[gDeviceIndex].ColorConvertItp->imageData,
// 				gMIUDevice[gDeviceIndex].nWidth,
// 				gMIUDevice[gDeviceIndex].nHeight
// 				);
// 
// 			RawToBmpQuater(
// 				(unsigned char *)gMIUDevice[gDeviceIndex].ColorConvertItp->imageData,
// 				(unsigned char*)gMIUDevice[gDeviceIndex].imageResize->imageData, 
// 				gMIUDevice[gDeviceIndex].nWidth,
// 				gMIUDevice[gDeviceIndex].nHeight,
// 				0
// 				);
// 
			

			if (NULL != m_hOwnerWnd && NULL != m_nWM_RecvVideo)
				::SendNotifyMessage(m_hOwnerWnd, m_nWM_RecvVideo, (WPARAM)0, (LPARAM)0);

			gMIUDevice[gDeviceIndex].bSignal = TRUE;
		}
		else
		{
			gMIUDevice[gDeviceIndex].bSignal = FALSE;
		}

		m_bSignal = gMIUDevice[gDeviceIndex].bSignal;

		if (NULL != m_hOwnerWnd && NULL != m_nWM_ChgStatus)
			::SendNotifyMessage(m_hOwnerWnd, m_nWM_ChgStatus, (WPARAM)0, (LPARAM)gMIUDevice[gDeviceIndex].bSignal);

		Sleep(33);
	}

	return 1;
}


BOOL CMIU_Ctrl::DeviceOpen()
{
	BOOL bResult = FALSE;

	int ErrCode = 0;
	char gIndexArray[MAX_DEVICE] = { 0, };

	ErrCode = MIUGetDeviceList(&gDeviceCount, gIndexArray);

	if (ErrCode) bResult = FALSE;
	else
	{
		if (gDeviceCount != NULL)
		{
			gDeviceIndex = gIndexArray[gDeviceCount - 1];

			USBRestartCallback(USBEventHandler, 3000);

			ErrCode = MIUOpenDevice(gDeviceIndex);

			if (ErrCode) bResult = FALSE;
			else bResult = TRUE;
		}
		else
		{
			bResult = FALSE;
		}
	}

	return gMIUDevice[gDeviceIndex].bMIUOpen = bResult;
}

BOOL CMIU_Ctrl::DeviceClose()
{
	if (gMIUDevice[gDeviceIndex].bMIUOpen)
	{
		if (gMIUDevice[gDeviceIndex].CurrentState == 1)
		{
			m_isThreadRun_Grab = false;
			DestroyThread();

			for (int j = 0; j < 100; j++)
			{
				int errorCode = MIUStop(gDeviceIndex);
				if (errorCode != 0)
				{
					errorCode = MIUStop(gDeviceIndex);
					Sleep(10);
				}
				else
				{
					break;
				}
			}

			cvReleaseImage(&gMIUDevice[gDeviceIndex].image);
			cvReleaseImage(&gMIUDevice[gDeviceIndex].imageItp);
			cvReleaseImage(&gMIUDevice[gDeviceIndex].image12bitRaw);
			cvReleaseImage(&gMIUDevice[gDeviceIndex].image16bitRaw);
			cvReleaseImage(&gMIUDevice[gDeviceIndex].ColorConvertItp);

			MIUSetIOPowerChannel(gDeviceIndex, 0.0f, 0);
			MIUSetPowerChannel0(gDeviceIndex, 0.0f, 0);
			MIUSetPowerChannel1(gDeviceIndex, 0.0f, 0);
			MIUSetPowerChannel2(gDeviceIndex, 0.0f, 0);
			MIUSetPowerChannel3(gDeviceIndex, 0.0f, 0);
			MIUSetPowerChannel4(gDeviceIndex, 0.0f, 0);
			MIUSetPowerChannel5V(gDeviceIndex, 0);
			MIUSetPowerChannel12V(gDeviceIndex, 0);
		}

		MIUCloseDevice(gDeviceIndex);
	}

	return TRUE;
}

BOOL CMIU_Ctrl::ISPOnOff(__in BOOL bOnOff)
{
	if (bOnOff)
	{
		unsigned short nwriteData = 0;
		unsigned char bLsc = 0;
		unsigned char bAWB = 0;
		unsigned char bColorCorrection = 0;
		unsigned char bGammaCorrection = 0;

		unsigned char bColorOrder = 0;
		unsigned char bInterpolation = 0;

		//bLsc = m_chkLSC;

		if (gMIUDevice[gDeviceIndex].iPixelFormat == MIU_YUV422_YUYV_PACKED)
		{
			bInterpolation = 1;
			//bAWB = m_chkAWB;
		}
		else
		{
			bInterpolation = 2;

		}

		//if(gMIUDevice[gDeviceIndex].iPixelFormat == MIU_YUV422_YUYV_PACKED)
		{
			if ((gMIUOriginImageFormat[gDeviceIndex] & 0x0000000F) % 4 == 0) //BG
			{
				bColorOrder = 2;
			}
			else if ((gMIUOriginImageFormat[gDeviceIndex] & 0x0000000F) % 4 == 1) //GR
			{
				bColorOrder = 1;
			}
			else if ((gMIUOriginImageFormat[gDeviceIndex] & 0x0000000F) % 4 == 2) //RG
			{
				bColorOrder = 0;
			}
			else if ((gMIUOriginImageFormat[gDeviceIndex] & 0x0000000F) % 4 == 3) //GB
			{
				bColorOrder = 3;
			}
		}
		//else //Mono
		//{
		//	if( (gMIUOriginImageFormat[gDeviceIndex] & 0x0000000F) % 2)
		//	{
		//		bColorOrder = 1; //G First
		//	}
		//	else
		//	{
		//		bColorOrder = 0; //G Second
		//	}
		//}

		Sleep(100);
		nwriteData = ((bAWB << 11) + (bColorCorrection << 10) + (bGammaCorrection << 9) + (bLsc << 8) + (bColorOrder << 4) + bInterpolation);

		MIUWriteRegister(gDeviceIndex, REG_MIU, 0x36, nwriteData);
	}
	else
	{
		MIUWriteRegister(gDeviceIndex, REG_MIU, 0x36, 0x0000);
	}

	return TRUE;
}

BOOL CMIU_Ctrl::Initialize_OV10642()
{
	int ErrCode = 0;
	BOOL bResult = FALSE;

	gMIUDevice[gDeviceIndex].InitialValue.SensorMode = 2;
	gMIUDevice[gDeviceIndex].InitialValue.nWidth = 1280;
	gMIUDevice[gDeviceIndex].InitialValue.nHeight = 1080;
	gMIUDevice[gDeviceIndex].InitialValue.MCLK = 4;
	gMIUDevice[gDeviceIndex].InitialValue.MCLKOnOff = 1;
	gMIUDevice[gDeviceIndex].InitialValue.MCLKSelection = 0;
	gMIUDevice[gDeviceIndex].InitialValue.PCLKInversion = 1;
	gMIUDevice[gDeviceIndex].InitialValue.IICDeviceID = 0x10;
	gMIUDevice[gDeviceIndex].InitialValue.IICMode = 1;
	gMIUDevice[gDeviceIndex].InitialValue.IICSpeed = 0;
	gMIUDevice[gDeviceIndex].InitialValue.IICReadRestart = 0;
	gMIUDevice[gDeviceIndex].InitialValue.IICReadRestartInterval = 0;
	gMIUDevice[gDeviceIndex].InitialValue.IICSCKPinCheck = 1;
	gMIUDevice[gDeviceIndex].InitialValue.IICAddressLength = 2;
	gMIUDevice[gDeviceIndex].InitialValue.IICDataLength = 2;
	gMIUDevice[gDeviceIndex].InitialValue.MIPILaneEnable = 7;
	gMIUDevice[gDeviceIndex].InitialValue.MIPIDataType = 0x2c;
	gMIUDevice[gDeviceIndex].InitialValue.MIPI8bitMode = 0;
	gMIUDevice[gDeviceIndex].InitialValue.MIUIOVoltage = 1.8;
	gMIUDevice[gDeviceIndex].InitialValue.FirstPowerChannel = 0;
	gMIUDevice[gDeviceIndex].InitialValue.FirstPowerVoltage = 1.8;
	gMIUDevice[gDeviceIndex].InitialValue.SecondPowerChannel = 1;
	gMIUDevice[gDeviceIndex].InitialValue.SecondPowerVoltage = 3.3;
	gMIUDevice[gDeviceIndex].InitialValue.ThirdPowerChannel = 2;
	gMIUDevice[gDeviceIndex].InitialValue.ThirdPowerVoltage = 3.3;
	gMIUDevice[gDeviceIndex].InitialValue.FourthPowerChannel = 3;
	gMIUDevice[gDeviceIndex].InitialValue.FourthPowerVoltage = 3;
	gMIUDevice[gDeviceIndex].InitialValue.FifthPowerChannel = 4;
	gMIUDevice[gDeviceIndex].InitialValue.FifthPowerVoltage = 3;
	gMIUDevice[gDeviceIndex].InitialValue.Power5VoltOnOff = 1;
	gMIUDevice[gDeviceIndex].InitialValue.Power12VoltOnOff = 0;
	gMIUDevice[gDeviceIndex].InitialValue.InitialSkipCount = 0;
	gMIUDevice[gDeviceIndex].InitialValue.PreviewSkipCount = 0;
	gMIUDevice[gDeviceIndex].InitialValue.ParallelSamplingMode = 0;
	gMIUDevice[gDeviceIndex].InitialValue.ParallelBitsPerPixel = 2;
	gMIUDevice[gDeviceIndex].InitialValue.ParallelPixelComponent = 1;
	gMIUDevice[gDeviceIndex].InitialValue.ParallelBitShift = 0;

	gMIUDevice[gDeviceIndex].nWidth			= gMIUDevice[gDeviceIndex].InitialValue.nWidth;
	gMIUDevice[gDeviceIndex].nHeight		= gMIUDevice[gDeviceIndex].InitialValue.nHeight;
	gMIUDevice[gDeviceIndex].nMaxWidth		= gMIUDevice[gDeviceIndex].InitialValue.nWidth;
	gMIUDevice[gDeviceIndex].nMaxHeight		= gMIUDevice[gDeviceIndex].InitialValue.nHeight;
	gMIUDevice[gDeviceIndex].bRCCC			= 1;
	gMIUDevice[gDeviceIndex].iPixelFormat	= MIU_BAYERBG12_PACKED;// MIU_MONO8;
	gMIUDevice[gDeviceIndex].nBitPerPixel	= ((gMIUDevice[gDeviceIndex].iPixelFormat & 0x00FF0000) >> 16);

	ErrCode = MIUInitialize(gDeviceIndex, gMIUDevice[gDeviceIndex].InitialValue);

	//ISPOnOff(TRUE);

	if (ErrCode) bResult = FALSE;
	else bResult = TRUE;

	return bResult;
}

BOOL CMIU_Ctrl::Initialize_AR0220()
{
	int ErrCode = 0;
	BOOL bResult = FALSE;

	return bResult;
}

BOOL CMIU_Ctrl::SensorInit(__in CStringA sziniFile)
{
	CStdioFile stdFile;
	CFileException  e;
	CStringA szLineRead;

	char fInitMode = 0;
	int	BurstWriteLength = 0;
	int	BurstWriteLengthaddr = 0;
	int	errorCode;
	int	errorCount = 0;

	unsigned int nAddress, nData;
	unsigned int bustSleep = 0;
	unsigned short  data;

	BYTE BurstWriteData[2048];

	char szTemp[MAX_PATH];

	if (!stdFile.Open(sziniFile, CFile::modeRead, &e))
	{
		e.ReportError(MB_OK);
		return FALSE;
	}

	stdFile.SeekToBegin();
	while (stdFile.ReadString(szLineRead))
	{
		if ((szLineRead.Find("[MIU]") != -1) && (szLineRead.Find("[MIU]") < 2))
		{
			fInitMode = 1;

		}
		else if ((szLineRead.Find("[Sensor]") != -1) && (szLineRead.Find("[Sensor]") < 2))
		{
			fInitMode = 2;
		}
		else if (((szLineRead.Find("Sleep") != -1) && ((szLineRead.Find("Sleep") < 2))) || ((szLineRead.Find("SLEEP") != -1) && ((szLineRead.Find("SLEEP") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			if (BurstWriteLength == 0)
			{
				Sleep(nData);
				bustSleep = 0;
			}
			else
			{
				bustSleep = nData;

			}
		}
		else if (((szLineRead.Find("RESET") != -1) && ((szLineRead.Find("RESET") < 2))) || ((szLineRead.Find("reset") != -1) && ((szLineRead.Find("reset") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUWriteRegister(gDeviceIndex, REG_MIU, 0x04, (unsigned short)nData);
		}
		else if (((szLineRead.Find("ENABLE") != -1) && ((szLineRead.Find("ENABLE") < 2))) || ((szLineRead.Find("enable") != -1) && ((szLineRead.Find("enable") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUWriteRegister(gDeviceIndex, REG_MIU, 0x05, (unsigned short)nData);
		}
		else if (((szLineRead.Find("GPIO0") != -1) && ((szLineRead.Find("GPIO0") < 2))) || ((szLineRead.Find("gpio0") != -1) && ((szLineRead.Find("gpio0") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOStatus(gDeviceIndex, 0, nData);
		}
		else if (((szLineRead.Find("GPIO1") != -1) && ((szLineRead.Find("GPIO1") < 2))) || ((szLineRead.Find("gpio1") != -1) && ((szLineRead.Find("gpio1") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOStatus(gDeviceIndex, 1, nData);
		}
		else if (((szLineRead.Find("GPIO2") != -1) && ((szLineRead.Find("GPIO2") < 2))) || ((szLineRead.Find("gpio2") != -1) && ((szLineRead.Find("gpio2") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOStatus(gDeviceIndex, 2, nData);
		}
		else if (((szLineRead.Find("GPIO3") != -1) && ((szLineRead.Find("GPIO3") < 2))) || ((szLineRead.Find("gpio3") != -1) && ((szLineRead.Find("gpio3") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOStatus(gDeviceIndex, 3, nData);
		}
		else if (((szLineRead.Find("GPO0") != -1) && ((szLineRead.Find("GPO0") < 2))) || ((szLineRead.Find("gpo0") != -1) && ((szLineRead.Find("gpo0") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPOStatus(gDeviceIndex, 0, nData);
		}
		else if (((szLineRead.Find("GPO1") != -1) && ((szLineRead.Find("GPO1") < 2))) || ((szLineRead.Find("gpo1") != -1) && ((szLineRead.Find("gpo1") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPOStatus(gDeviceIndex, 0, nData);
		}
		else if (((szLineRead.Find("GPIOMODE0") != -1) && ((szLineRead.Find("GPIOMODE0") < 2))) || ((szLineRead.Find("gpiomode0") != -1) && ((szLineRead.Find("gpiomode0") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOMode(gDeviceIndex, 0, nData);
		}
		else if (((szLineRead.Find("GPIOMODE1") != -1) && ((szLineRead.Find("GPIOMODE1") < 2))) || ((szLineRead.Find("gpiomode1") != -1) && ((szLineRead.Find("gpiomode1") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOMode(gDeviceIndex, 1, nData);
		}
		else if (((szLineRead.Find("GPIOMODE2") != -1) && ((szLineRead.Find("GPIOMODE2") < 2))) || ((szLineRead.Find("gpiomode2") != -1) && ((szLineRead.Find("gpiomode2") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOMode(gDeviceIndex, 2, nData);
		}
		else if (((szLineRead.Find("GPIOMODE3") != -1) && ((szLineRead.Find("GPIOMODE3") < 2))) || ((szLineRead.Find("gpiomode3") != -1) && ((szLineRead.Find("gpiomode3") < 2))))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetGPIOMode(gDeviceIndex, 3, nData);
		}
		else if (((szLineRead.Find("BULK") != -1) && ((szLineRead.Find("BULK") < 2))) || ((szLineRead.Find("Bulk") != -1) && ((szLineRead.Find("Bulk") < 2))))
		{
			unsigned int	nCount;
			unsigned char	pData[256];
			CString	 strInput;
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nCount);

			switch (nCount)
			{
			case 1:
				sscanf(szLineRead.GetBuffer(0), "%s%d %x", szTemp, &nCount, pData);
				break;
			case 2:
				sscanf(szLineRead.GetBuffer(0), "%s%d %x %x", szTemp, &nCount, &pData[0], &pData[1]);
				break;
			case 3:
				sscanf(szLineRead.GetBuffer(0), "%s%d %x %x %x", szTemp, &nCount, &pData[0], &pData[1], &pData[2]);
				break;
			case 4:
				sscanf(szLineRead.GetBuffer(0), "%s%d %x %x %x %x", szTemp, &nCount, &pData[0], &pData[1], &pData[2], &pData[3]);
				break;
			case 5:
				sscanf(szLineRead.GetBuffer(0), "%s%d %x %x %x %x %x", szTemp, &nCount, &pData[0], &pData[1], &pData[2], &pData[3], &pData[4]);
				break;
			default:
				break;
			}

			if (fInitMode == 2)
			{
				errorCode = MIUI2CBurstWrite(gDeviceIndex, gMIUDevice[gDeviceIndex].InitialValue.IICDeviceID, pData[0], 1, &pData[1], nCount - 1);
				if (errorCode)
				{
					errorCount++;
					TRACE("Error Code : 0x%x\n", errorCode);
				}
			}

		}
		else if (((szLineRead.Find("BITFIELD") != -1) && ((szLineRead.Find("BITFIELD") < 2))) || ((szLineRead.Find("Bitfield") != -1) && ((szLineRead.Find("Bitfield") < 2))))
		{
			unsigned int	bitfield;
			unsigned int    ReadValue;
			unsigned char	value;
			CString	 strInput;
			sscanf(szLineRead.GetBuffer(0), "%s %x %x %d", nAddress, &bitfield, &value);
			if (fInitMode == 1)
			{
				MIUReadRegister(gDeviceIndex, REG_MIU, (unsigned short)nAddress, (unsigned short*)&ReadValue);
			}
			else if (fInitMode == 2)
			{
				MIUReadRegister(gDeviceIndex, REG_IIC1, (unsigned short)nAddress, (unsigned short*)&ReadValue);
			}


			if (value == 0)
			{
				ReadValue &= bitfield ^ 0xFFFFFFFF;
			}
			else
			{
				ReadValue |= bitfield;
			}

			if (fInitMode == 1)
			{
				TRACE("MIU Addr %x, DAta %x \n", nAddress, ReadValue);
				MIUWriteRegister(gDeviceIndex, REG_MIU, (unsigned short)nAddress, (unsigned short)ReadValue);
			}
			else if (fInitMode == 2)
			{
				TRACE("Sensor Addr %x, DAta %x \n", nAddress, ReadValue);
				MIUWriteRegister(gDeviceIndex, REG_IIC1, (unsigned short)nAddress, (unsigned short)ReadValue);
				Sleep(1);
			}

		}
		else if (((szLineRead.Find("0x") != -1) || (szLineRead.Find("0X") != -1)) && (szLineRead.Find("0") == 0))
		{
			sscanf_s(szLineRead.GetBuffer(0), "%x%x", &nAddress, &nData);

			if (fInitMode == 1)
			{
				TRACE("MIU Addr %x, DAta %x \n", nAddress, nData);
				errorCode = MIUWriteRegister(gDeviceIndex, REG_MIU, (unsigned short)nAddress, (unsigned short)nData);
				if (errorCode)
				{
					errorCount++;
					TRACE("Error Code : 0x%x\n", errorCode);
				}

			}
			else if (fInitMode == 2)
			{
				// 0722
				// m_Burst
				if (/*m_Burst == TRUE  &&*/  (nAddress == 0x0F12 || nAddress == 0x0F14))
				{
					if (BurstWriteLength == 0)
					{
						memset(BurstWriteData, 0, sizeof(BurstWriteData));

						BurstWriteData[0] = nData >> 8;
						BurstWriteData[1] = nData & 0xFF;

						BurstWriteLength = 2;
						BurstWriteLengthaddr = 2;
					}
					else
					{
						BurstWriteData[BurstWriteLength] = nData >> 8;
						BurstWriteData[BurstWriteLength + 1] = nData & 0xFF;
						BurstWriteLength += 2;
					}

					if (BurstWriteLength == 2046)
					{
						errorCode = MIUI2CBurstWrite(gDeviceIndex, gMIUDevice[gDeviceIndex].InitialValue.IICDeviceID, (unsigned short)nAddress, BurstWriteLengthaddr, BurstWriteData, BurstWriteLength);
						if (errorCode)
						{
							errorCount++;
							TRACE("Burst Write Fail Error Code : 0x%x\n", errorCode);
						}
						else
						{
							TRACE("Burst Write Ok  : %d Byte \n", BurstWriteLength);

						}
						BurstWriteLength = 0;
						BurstWriteLengthaddr = 0;
					}
				}
				else
				{
					if (BurstWriteLength > 0)
					{
						if (BurstWriteLength == 2)
						{
							data = (unsigned short)(BurstWriteData[0] << 8) + BurstWriteData[1];
							TRACE("Sensor Addr %x, DAta %x \n", nAddress, data);
							errorCode = MIUWriteRegister(gDeviceIndex, REG_IIC1, (unsigned short)nAddress, (unsigned short)data);
							if (errorCode)
							{
								errorCount++;
								TRACE("Error Code : 0x%x\n", errorCode);
							}
						}
						else
						{
							errorCode = MIUI2CBurstWrite(gDeviceIndex, gMIUDevice[gDeviceIndex].InitialValue.IICDeviceID, (unsigned short)nAddress, BurstWriteLengthaddr, BurstWriteData, BurstWriteLength);
							if (errorCode)
							{
								errorCount++;
								TRACE("Burst Write Fail Error Code : 0x%x\n", errorCode);
							}
							else
							{
								TRACE("Burst Write Ok  : %d Byte \n", BurstWriteLength);
							}
							//Sleep(400);
						}

						if (bustSleep > 0)
						{
							Sleep(bustSleep);
							bustSleep = 0;
						}
						BurstWriteLength = 0;
						BurstWriteLengthaddr = 0;
						//Sleep(100);

					}

					TRACE("Sensor Addr %x, DAta %x \n", nAddress, nData);
					errorCode = MIUWriteRegister(gDeviceIndex, REG_IIC1, (unsigned short)nAddress, (unsigned short)nData);
					if (errorCode)
					{
						errorCount++;
						TRACE("Error Code : 0x%x\n", errorCode);
					}
				}
			}
		}
		else if (((szLineRead.Find("I2C_MODE") != -1) || (szLineRead.Find("I2C_Mode") != -1)) && (szLineRead.Find("I") == 0))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%d", szTemp, &nData);
			MIUSetI2CMode(gDeviceIndex, nData);
		}
		else if ((szLineRead.Find("I2CID") != -1) && (szLineRead.Find("I") == 0))
		{
			sscanf(szLineRead.GetBuffer(0), "%s%x", szTemp, &nData);
			MIUSetI2CID(gDeviceIndex, nData);
		}
	}

	if (BurstWriteLength > 0)
	{
		if (BurstWriteLength == 2)
		{
			data = (unsigned short)(BurstWriteData[0] << 8) + BurstWriteData[1];
			TRACE("Sensor Addr %x, DAta %x \n", nAddress, data);
			errorCode = MIUWriteRegister(gDeviceIndex, REG_IIC1, (unsigned short)nAddress, (unsigned short)data);
			if (errorCode)
			{
				errorCount++;
				TRACE("Error Code : 0x%x\n", errorCode);
			}
		}
		else
		{
			errorCode = MIUI2CBurstWrite(gDeviceIndex, gMIUDevice[gDeviceIndex].InitialValue.IICDeviceID, (unsigned short)nAddress, BurstWriteLengthaddr, BurstWriteData, BurstWriteLength);
			if (errorCode)
			{
				errorCount++;
				TRACE("Burst Write Fail Error Code : 0x%x\n", errorCode);
			}
			else
			{
				TRACE("Burst Write Ok  : %d Byte \n", BurstWriteLength);
			}
		}

		////////////////////////// 20130711 추가
		if (bustSleep > 0)
		{
			TRACE("burst Sleep = %d \n", bustSleep);
			Sleep(bustSleep);
			bustSleep = 0;

		}

		BurstWriteLength = 0;
		BurstWriteLengthaddr = 0;
		//Sleep(100);
	}

	if (errorCount != 0)
	{
		return FALSE;
	}


	return TRUE;
}

BOOL CMIU_Ctrl::ChangeMono()
{
	int errorCode = 0;

	// Image Display Stop
	m_isThreadRun_Grab = false;
	DestroyThread();


	gMIUDevice[gDeviceIndex].bRCCC = FALSE;

	Sleep(1000);

	// Pause
	MIUPause(gDeviceIndex);

	cvReleaseImage(&gMIUDevice[gDeviceIndex].image);
	cvReleaseImage(&gMIUDevice[gDeviceIndex].imageItp);
	cvReleaseImage(&gMIUDevice[gDeviceIndex].ColorConvertItp);

	///////////////////////////////////////////////////////////////////////////////////////////////

	gMIUDevice[gDeviceIndex].iPixelFormat = MIU_MONO8;
	gMIUDevice[gDeviceIndex].nBitPerPixel = ((gMIUDevice[gDeviceIndex].iPixelFormat & 0x00FF0000) >> 16);

	ISPOnOff(TRUE);

	if ((gMIUDevice[gDeviceIndex].iPixelFormat == MIU_RGB8_PACKED) || (gMIUDevice[gDeviceIndex].iPixelFormat == MIU_BGR8_PACKED))
	{
		gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 3);
	}
	else if ((gMIUDevice[gDeviceIndex].iPixelFormat == MIU_RGB565) || (gMIUDevice[gDeviceIndex].iPixelFormat == MIU_BGR565))
	{
		gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 2);
	}
	else
	{
		gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 1);
		if (gMIUDevice[gDeviceIndex].nBitPerPixel > 8)
		{
			gMIUDevice[gDeviceIndex].ColorConvertItp = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 1);
		}
	}

	gMIUDevice[gDeviceIndex].imageItp = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 3);


	///////////////////////////////////////////////////////////////////////////////////////////////
	// Resume
	MIUResumeChageFormat(gDeviceIndex, gMIUDevice[gDeviceIndex].iPixelFormat);
	Sleep(300);

	// Image Display restart
	m_isThreadRun_Grab = true;
	CreateThread();

	return TRUE;
}

BOOL CMIU_Ctrl::ChangeRaw()
{
	int errorCode = 0;
	// Image Display Stop
	m_isThreadRun_Grab = false;
	DestroyThread();

	gMIUDevice[gDeviceIndex].bRCCC = TRUE;

	// Pause
	MIUPause(gDeviceIndex);

	cvReleaseImage(&gMIUDevice[gDeviceIndex].image);
	cvReleaseImage(&gMIUDevice[gDeviceIndex].imageItp);
	cvReleaseImage(&gMIUDevice[gDeviceIndex].ColorConvertItp);

	gMIUDevice[gDeviceIndex].iPixelFormat = MIU_BAYERBG12_PACKED;	// Start RAW

	gMIUDevice[gDeviceIndex].nBitPerPixel = ((gMIUDevice[gDeviceIndex].iPixelFormat & 0x00FF0000) >> 16);

	ISPOnOff(FALSE);

	if ((gMIUDevice[gDeviceIndex].iPixelFormat == MIU_RGB8_PACKED) || (gMIUDevice[gDeviceIndex].iPixelFormat == MIU_BGR8_PACKED))
	{
		gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 3);
	}
	else if ((gMIUDevice[gDeviceIndex].iPixelFormat == MIU_RGB565) || (gMIUDevice[gDeviceIndex].iPixelFormat == MIU_BGR565))
	{
		gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 2);
	}
	else
	{
		gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 1);
		if (gMIUDevice[gDeviceIndex].nBitPerPixel > 8)
		{
			gMIUDevice[gDeviceIndex].ColorConvertItp = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 1);
		}
	}

	gMIUDevice[gDeviceIndex].imageItp = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 3);


	///////////////////////////////////////////////////////////////////////////////////////////////
	// Resume
	MIUResumeChageFormat(gDeviceIndex, gMIUDevice[gDeviceIndex].iPixelFormat);
	Sleep(500);

	// Image Display restart
	m_isThreadRun_Grab = true;
	CreateThread();

	return TRUE;
}

BOOL CMIU_Ctrl::RegisterWrite(UINT nType, unsigned short nAddress, unsigned short nData)
{
	int errorCode = MIUWriteRegister(gDeviceIndex, (MIU_REG_TYPE)nType, (unsigned short)nAddress, (unsigned short)nData);
	if (errorCode)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL CMIU_Ctrl::InitCalibration()
{
	InitializeCalibration(gDeviceIndex);

	return TRUE;
}

BOOL CMIU_Ctrl::CurrentMeasureModeSelect(__in UINT nMode, __out double* dVal)
{
	int	errorCode = CurrentMeasureMode(gDeviceIndex, nMode);
	if (errorCode != MIU_OK)
	{
		return FALSE;
	}

	switch (nMode)
	{
	case 0:
		CurrentMeasure_Dynamic(dVal);
		break;

	case 1:
		break;

	default:
		break;
	}


	return TRUE;
}

BOOL CMIU_Ctrl::CurrentMeasure_Dynamic(__out double* dVal)
{
	double dCurrentVal[5] = { 0, };

	for (int i = 0; i < 5; i++)
	{
		int errorCode = CurrentMeasureResult(gDeviceIndex, i, &dCurrentVal[i], 1);
		if (errorCode != MIU_OK)
		{
			dVal[i] = 0;
		}

		dVal[i] = dCurrentVal[i];
	}

	return TRUE;
}


BOOL CMIU_Ctrl::CurrentMeasure(__in UINT nSensorType, __in CStringA sziniFile, __out double* dValue)
{
	unsigned int	errorCode;
	double			CurrentValue[5];
	//CString*		strCur[5];
	CString			szTemp;

	 	//InitializeCalibration(gDeviceIndex);
	 
	LARGE_INTEGER liCounter1, liCounter2, liCounter3, liCounter4, liCounter5, liCounter6, liCounter7, liFrequency;
	 	QueryPerformanceFrequency(&liFrequency);
	 	QueryPerformanceCounter(&liCounter1);
	 
	 	InitializeCalibration(gDeviceIndex);
	 
	 	QueryPerformanceCounter(&liCounter2);
	 
	 	szTemp.Format("InitializeCalibration Time = %.3f Sec\n", (double)(liCounter2.QuadPart - liCounter1.QuadPart) / (double)liFrequency.QuadPart);
	 	TRACE(szTemp);
	 
	 	// Sensor Initialize
	 	if (nSensorType == 0)
	 		Initialize_OV10642();
	 	else if (nSensorType == 1)
	 		Initialize_AR0220();
	 
	 	SensorInit(sziniFile);
	 
	 	Sleep(1000);
	 
	 	////////////////////////////////////////////////////////////////////////// Dynamic 
	 	QueryPerformanceCounter(&liCounter3);
	 
	 	szTemp.Format("Sensor Initial Time = %.3f Sec\n", (double)(liCounter3.QuadPart - liCounter2.QuadPart) / (double)liFrequency.QuadPart);
	 	TRACE(szTemp);
	 
	 	errorCode = CurrentMeasureMode(gDeviceIndex, 0);  //Dynamic Mode
	 	if (errorCode != MIU_OK)
	 	{
	 		//AfxMessageBox(_T("Current Mode error \n"));
	 		return FALSE;
	 	}
	 
	 	for (int i = 0; i < 5; i++)
	 	{
	 		errorCode = CurrentMeasureResult(gDeviceIndex, i, &CurrentValue[i], 0);
	 		if (errorCode != MIU_OK)
	 		{
	 			//AfxMessageBox(_T("Current Measure error \n"));
	 			break;
	 		}
	 		//strCur[i]->Format("%.2f mA", CurrentValue[i]);
	 	}
	 
	 	QueryPerformanceCounter(&liCounter4);
	 
	 	szTemp.Format("Measure Dynamic Time = %.3f Sec\n", (double)(liCounter4.QuadPart - liCounter3.QuadPart) / (double)liFrequency.QuadPart);
	 	TRACE(szTemp);


	////////////////////////////////////////////////////////////////////////////// Stand by
	//MIUSetIOPowerChannel(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel0(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel1(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel2(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel3(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel4(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel5V(gDeviceIndex, 0);
	//MIUSetPowerChannel12V(gDeviceIndex, 0);

	//Sleep(100);

	//MIUInitialize(gDeviceIndex, gMIUDevice[gDeviceIndex].InitialValue);

	//// MCLK Off , GPO Control (Enable & Reset)
	//MIUWriteRegister(gDeviceIndex, REG_MIU, 0x05, 0x00);	//Enable
	//Sleep(10);
	//MIUWriteRegister(gDeviceIndex, REG_MIU, 0x04, 0x00);	//Reset
	//Sleep(50);
	//MIUWriteRegister(gDeviceIndex, REG_MIU, 0x05, 0x01);	//Enable
	//Sleep(10);
	//MIUWriteRegister(gDeviceIndex, REG_MIU, 0x04, 0x01);	//Reset
	//Sleep(10);
	//MIUWriteRegister(gDeviceIndex, REG_MIU, 0x44, 0x00);	// MCLK Off
	//Sleep(10);

	//QueryPerformanceCounter(&liCounter5);

	////szTemp.Format("Set Standby mode Time = %.3f Sec\n", (double)(liCounter5.QuadPart - liCounter4.QuadPart) / (double)liFrequency.QuadPart);
	////TRACE(szTemp);


	//errorCode = CurrentMeasureMode(gDeviceIndex, 1);  //Stand by Mode
	//if (errorCode != MIU_OK)
	//{
	//	//AfxMessageBox(_T("Current Mode error \n"));
	//	return FALSE;
	//}

	//for (int i = 0; i < 5; i++)
	//{
	//	errorCode = CurrentMeasureResult(gDeviceIndex, i, &CurrentValue[i], 1);
	//	if (errorCode != MIU_OK)
	//	{
	//		//AfxMessageBox(_T("Current Measure error \n"));
	//		return FALSE;
	//	}

	//	//strCur[i]->Format("%.2f uA", CurrentValue[i]);
	//}

	//QueryPerformanceCounter(&liCounter6);
	//// 	szTemp.Format("Measure Standby Time = %.3f Sec\n", (double)(liCounter6.QuadPart - liCounter5.QuadPart) / (double)liFrequency.QuadPart);
	//// 	TRACE(szTemp);

	//MIUSetIOPowerChannel(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel0(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel1(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel2(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel3(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel4(gDeviceIndex, 0.0f, 0);
	//MIUSetPowerChannel5V(gDeviceIndex, 0);
	//MIUSetPowerChannel12V(gDeviceIndex, 0);

	//QueryPerformanceCounter(&liCounter7);
	//// 	szTemp.Format("Sensor Power Off Time = %.3f Sec\n", (double)(liCounter7.QuadPart - liCounter6.QuadPart) / (double)liFrequency.QuadPart);
	//// 	TRACE(szTemp);

	//// 	szTemp.Format("Total Time = %.3f Sec\n", (double)(liCounter7.QuadPart - liCounter1.QuadPart) / (double)liFrequency.QuadPart);
	//// 	TRACE(szTemp);
	for (int i = 0; i < 5; i++)
		dValue[i] = CurrentValue[i];

	return TRUE;
}


BOOL CMIU_Ctrl::Start()
{
	if (gMIUDevice[gDeviceIndex].CurrentState == FALSE)
	{
		MIUStop(gDeviceIndex);

		if (gMIUDevice[gDeviceIndex].image != NULL)
			cvReleaseImage(&gMIUDevice[gDeviceIndex].image);

		if (gMIUDevice[gDeviceIndex].imageItp != NULL)
			cvReleaseImage(&gMIUDevice[gDeviceIndex].imageItp);

		if (gMIUDevice[gDeviceIndex].image16bitRaw != NULL)
			cvReleaseImage(&gMIUDevice[gDeviceIndex].image16bitRaw);

		if (gMIUDevice[gDeviceIndex].image12bitRaw != NULL)
			cvReleaseImage(&gMIUDevice[gDeviceIndex].image12bitRaw);

		if (gMIUDevice[gDeviceIndex].ColorConvertItp != NULL)
			cvReleaseImage(&gMIUDevice[gDeviceIndex].ColorConvertItp);

		// Create
		if ((gMIUDevice[gDeviceIndex].iPixelFormat == MIU_RGB8_PACKED) || (gMIUDevice[gDeviceIndex].iPixelFormat == MIU_BGR8_PACKED))
		{
			gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 3);
		}
		else if ((gMIUDevice[gDeviceIndex].iPixelFormat == MIU_RGB565) || (gMIUDevice[gDeviceIndex].iPixelFormat == MIU_BGR565))
		{
			gMIUDevice[gDeviceIndex].image = cvCreateImageHeader(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 2);
		}
		else
		{
			gMIUDevice[gDeviceIndex].image = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 1);

			if (gMIUDevice[gDeviceIndex].nBitPerPixel > 8)
				gMIUDevice[gDeviceIndex].ColorConvertItp = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 1);
		}

		gMIUDevice[gDeviceIndex].image12bitRaw = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 2);
		gMIUDevice[gDeviceIndex].image16bitRaw = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 2);
		gMIUDevice[gDeviceIndex].imageItp = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 3);

		//m_pDisplayimage = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 3);
		//m_praw12image = cvCreateImage(cvSize(gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight), IPL_DEPTH_8U, 2);

		MIUInitializeImageValue(gDeviceIndex, gMIUDevice[gDeviceIndex].iPixelFormat, gMIUDevice[gDeviceIndex].nWidth, gMIUDevice[gDeviceIndex].nHeight, gMIUDevice[gDeviceIndex].nMaxWidth, gMIUDevice[gDeviceIndex].nMaxHeight, BUFFER_COUNT);

		gMIUDevice[gDeviceIndex].CurrentState = TRUE;
		MIUStart(gDeviceIndex);

		m_isThreadRun_Grab = true;
		CreateThread();
	}

	return TRUE;
}

BOOL CMIU_Ctrl::Stop()
{
	if (gMIUDevice[gDeviceIndex].CurrentState == FALSE)
		return FALSE;

	m_isThreadRun_Grab = false;
	DestroyThread();

	if (NULL != m_hOwnerWnd && NULL != m_nWM_ChgStatus)
		::SendNotifyMessage(m_hOwnerWnd, m_nWM_ChgStatus, (WPARAM)0, (LPARAM)FALSE);

	int ErrCode = 0;

	for (int i = 0; i < 100; i++)
	{
		if (MIUStop(gDeviceIndex) == 0)
			break;

		Sleep(10);
	}

	// power
	MIUSetIOPowerChannel(gDeviceIndex, 0.0f, 0);
	MIUSetPowerChannel0(gDeviceIndex, 0.0f, 0);
	MIUSetPowerChannel1(gDeviceIndex, 0.0f, 0);
	MIUSetPowerChannel2(gDeviceIndex, 0.0f, 0);
	MIUSetPowerChannel3(gDeviceIndex, 0.0f, 0);
	MIUSetPowerChannel4(gDeviceIndex, 0.0f, 0);
	MIUSetPowerChannel5V(gDeviceIndex, 0);
	MIUSetPowerChannel12V(gDeviceIndex, 0);

	gMIUDevice[gDeviceIndex].CurrentState = FALSE;

	return TRUE;
}



void CMIU_Ctrl::Shift16to12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight)
{
	unsigned int i, j, k;
	unsigned int nScrWidth, nDestWidth;
	unsigned char *pcDest;

	nDestWidth = nWidth * 3 / 2;
	nScrWidth = nWidth * 2;

	pcDest = (unsigned char *)pDest;

	for (i = 0; i < (nHeight); i++)
	{
		for (j = 0, k = 0; j < nDestWidth; j += 6, k += 8)
		{
			pcDest[j + 0] = ((pImage[k] & 0xF0) >> 4) + ((pImage[k + 1] & 0x0f) << 4);
			pcDest[j + 1] = ((pImage[k + 2] & 0xF0) >> 4) + ((pImage[k + 3] & 0x0f) << 4);
			pcDest[j + 2] = (pImage[k] & 0x0F) + ((pImage[k + 2] & 0x0f) << 4);

			pcDest[j + 3] = ((pImage[k + 4] & 0xF0) >> 4) + ((pImage[k + 5] & 0x0f) << 4);
			pcDest[j + 4] = ((pImage[k + 6] & 0xF0) >> 4) + ((pImage[k + 7] & 0x0f) << 4);
			pcDest[j + 5] = (pImage[k + 4] & 0x0F) + ((pImage[k + 6] & 0x0f) << 4);
		}
		pcDest += nDestWidth;
		pImage += nScrWidth;
	}
}


void CMIU_Ctrl::Shift16to8BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight)
{
	unsigned int i, j, k;
	unsigned int nByteWidth;
	unsigned char *pcDest;

	nByteWidth = nWidth * 2;

	pcDest = (unsigned char *)pDest;

	for (i = 0; i < (nHeight); i++)
	{
		for (j = 0, k = 0; j < (nWidth); j += 4, k += 8)
		{
			pcDest[j + 0] = ((pImage[k] & 0xF0) >> 4) + ((pImage[k + 1] & 0x0f) << 4);
			pcDest[j + 1] = ((pImage[k + 2] & 0xF0) >> 4) + ((pImage[k + 3] & 0x0f) << 4);

			pcDest[j + 2] = ((pImage[k + 4] & 0xF0) >> 4) + ((pImage[k + 5] & 0x0f) << 4);
			pcDest[j + 3] = ((pImage[k + 6] & 0xF0) >> 4) + ((pImage[k + 7] & 0x0f) << 4);

		}
		pcDest += nWidth;
		pImage += nByteWidth;
	}
}


void CMIU_Ctrl::Shift12to16BitMode(unsigned char* p12bitRaw, unsigned char* p16bitRaw, int nWidth, int nHeight)
{
	int k = 0;
	int n12bitRawSize = nWidth * nHeight * 1.5;

	for (int i = 0; i < n12bitRawSize; i += 3)
	{
		p16bitRaw[k + 1] = (p12bitRaw[i] & 0xF0) >> 4;
		p16bitRaw[k + 0] = ((p12bitRaw[i] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0x0F));

		p16bitRaw[k + 3] = (p12bitRaw[i + 1] & 0xF0) >> 4;
		p16bitRaw[k + 2] = ((p12bitRaw[i + 1] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0xF0) >> 4);

		k += 4;
	}
}


void CMIU_Ctrl::CCCR_Interpolation(unsigned short* pData, int nWidth, int nHeight, BOOL bRCCC_mx, BOOL bRCCC_my, BOOL bChkDir)
{
	int nNewValue = 0;
	int start_x = 1;
	int start_y = 1;

	if (bRCCC_mx) start_x = 2;
	if (bRCCC_my) start_y = 2;

	for (int p = start_y; p < nHeight - 1; p += 2)
	{
		for (int k = start_x; k < nWidth - 1; k += 2)
		{
			if (bChkDir)
			{
				int absV = abs(pData[(p - 1) * nWidth + (k + 0)] - pData[(p + 1) * nWidth + (k + 0)]);
				int absH = abs(pData[(p + 0) * nWidth + (k - 1)] - pData[(p + 0) * nWidth + (k + 1)]);
				pData[p * nWidth + k] = (absV < absH) ? (pData[(p - 1) * nWidth + (k + 0)] + pData[(p + 1) * nWidth + (k + 0)]) / 2 : (pData[(p + 0) * nWidth + (k - 1)] + pData[(p + 0) * nWidth + (k + 1)]) / 2;

			}
			else
			{
				nNewValue = pData[(p - 1) * nWidth + (k + 0)] + pData[(p + 1) * nWidth + (k + 0)] + pData[(p + 0) * nWidth + (k - 1)] + pData[(p + 0) * nWidth + (k + 1)];

				nNewValue = nNewValue / 4;
				pData[p * nWidth + k] = (unsigned short)nNewValue;
			}
		}
	}
}


void CMIU_Ctrl::ConvertColor(unsigned int iPixelFormat, IplImage* pSrc, IplImage* pDest, IplImage* pTemp, unsigned int nWidth, unsigned int nHeight)
{
#ifdef CONVTIMECHECK
	LARGE_INTEGER start, finish;
	LARGE_INTEGER ticksPerSec;
	double timeSec;

	if (!QueryPerformanceFrequency(&ticksPerSec))
	{
		return;
	}
	QueryPerformanceCounter(&start);
#endif
	switch (iPixelFormat)
	{
	case MIU_MONO8:
		cvCvtColor(pSrc, pDest, CV_GRAY2RGB);
		break;

	case MIU_MONO10_PACKED:
		Shift10BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_GRAY2RGB);
		break;

	case MIU_MONO12_PACKED:
		Shift12BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_GRAY2RGB);
		break;

	case MIU_MONO14:

		break;

	case MIU_BAYERGR8:
		cvCvtColor(pSrc, pDest, CV_BayerGR2RGB);  // interpolation  
		break;

	case MIU_BAYERGB8:
		cvCvtColor(pSrc, pDest, CV_BayerGB2RGB);  // interpolation					
		break;

	case MIU_BAYERRG8:
		cvCvtColor(pSrc, pDest, CV_BayerRG2RGB);  // interpolation  
		break;

	case MIU_BAYERBG8:
		cvCvtColor(pSrc, pDest, CV_BayerBG2RGB);  // interpolation					
		break;

	case MIU_BAYERRG10_PACKED:
		Shift10BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerRG2RGB);  // interpolation 
		break;

	case MIU_BAYERGR10_PACKED:
		Shift10BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerGR2RGB);  // interpolation 
		break;

	case MIU_BAYERBG10_PACKED:
		Shift10BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerBG2RGB);  // interpolation 
		break;

	case MIU_BAYERGB10_PACKED:
		Shift10BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerGB2RGB);  // interpolation 

		break;

	case MIU_BAYERGR12_PACKED:
		Shift12BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerGR2RGB);  // interpolation 
		break;

	case MIU_BAYERGB12_PACKED:
		Shift12BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerGB2RGB);  // interpolation 
		break;

	case MIU_BAYERRG12_PACKED:
		Shift12BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerRG2RGB);  // interpolation 
		break;

	case MIU_BAYERBG12_PACKED:
		Shift12BitMode((unsigned char*)pSrc->imageData, (unsigned char*)pTemp->imageData, nWidth, nHeight);
		cvCvtColor(pTemp, pDest, CV_BayerBG2RGB);  // interpolation 
		break;

	case MIU_YUV422_YUYV_PACKED:
		//ConvYUYVtoRGB((unsigned char*)pSrc->imageData, (unsigned char*)pDest->imageData, nWidth, nHeight);
		break;

	case MIU_YUV422_PACKED:
		//ConvUYVYtoRGB((unsigned char*)pSrc->imageData, (unsigned char*)pDest->imageData, nWidth, nHeight);
		break;

	case MIU_RGB565:
		cvCvtColor(pSrc, pDest, CV_BGR5652BGR);
		break;

	case MIU_BGR565:
		cvCvtColor(pSrc, pDest, CV_BGR5652RGB);
		break;

	case MIU_RGB8_PACKED:
		cvCopyImage(pSrc, pDest);
		break;

	case MIU_BGR8_PACKED:
		cvCvtColor(pSrc, pDest, CV_BGR2RGB);
		break;

	default:
		break;
	}
#ifdef CONVTIMECHECK
	QueryPerformanceCounter(&finish);
	// 경과시간 구하기 (초단위)
	timeSec = (finish.QuadPart - start.QuadPart) / (double)ticksPerSec.QuadPart;
	TRACE("Display : %.6f\n", timeSec);
#endif
}


void CMIU_Ctrl::Shift12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight)
{
	unsigned int	i, j, k;
	unsigned int	nByteWidth;
	unsigned short *psDest;

	nByteWidth = nWidth * 3 / 2;
	psDest = (unsigned short *)pDest;
	nWidth >>= 1;
	for (i = 0; i < nHeight; i++)
	{
		for (j = 0, k = 0; j < nWidth; j++, k += 3)
		{
			psDest[j] = (pImage[k + 1] << 8) + (pImage[k]);
		}

		psDest += nWidth;
		pImage += nByteWidth;
	}
}


void CMIU_Ctrl::Shift10BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight)
{
	unsigned int i, j, k;
	unsigned int nByteWidth;
	unsigned int *piDest;

	nByteWidth = nWidth * 5 / 4;
	piDest = (unsigned int *)pDest;
	nWidth >>= 2;

	for (i = 0; i < nHeight; i++)
	{
		for (j = 0, k = 0; j < nWidth; j++, k += 5)
		{
			piDest[j] = (pImage[k + 3] << 24) + (pImage[k + 2] << 16) + (pImage[k + 1] << 8) + (pImage[k]);
		}

		piDest += nWidth;
		pImage += nByteWidth;
	}
}

void CMIU_Ctrl::RawToBmpQuater(unsigned char *pRaw8, unsigned char *pBMP, int image_width, int image_height, int ch)
{
	int i, j, sx = 0, sy = 0;

	if (ch == 0)		sx = 0, sy = 0;
	else if (ch == 1)		sx = 1, sy = 0;
	else if (ch == 2)		sx = 0, sy = 1;
	else if (ch == 3)		sx = 1, sy = 1;

	for (j = sy; j < image_height; j += 2)
	{
		for (i = sx; i < image_width; i += 2)
		{
			*pBMP++ = pRaw8[(image_height - j - 1)*image_width + i];
			*pBMP++ = pRaw8[(image_height - j - 1)*image_width + i];
			*pBMP++ = pRaw8[(image_height - j - 1)*image_width + i];
		}
	}
}

void CMIU_Ctrl::PRI_12BitToRaw8(unsigned char *p12Bit, unsigned char *pRawBit8, int w, int h)
{
	int x, y, m_iWidth = w, m_iHeight = h;

	for (y = 0; y < m_iHeight; y++)
	{
		for (x = 0; x < m_iWidth / 2; x = x++)
		{
			pRawBit8[y*m_iWidth + x * 2] = (p12Bit[(y + 2)*m_iWidth * 2 + 4 * x + 0]);
			pRawBit8[y*m_iWidth + x * 2 + 1] = (p12Bit[(y + 2)*m_iWidth * 2 + 4 * x + 2]);
		}
	}
}
