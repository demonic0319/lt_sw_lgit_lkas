//*****************************************************************************
// Filename	: 	LG_CalIntr.h
// Created	:	2017/10/16 - 14:50
// Modified	:	2017/10/16 - 14:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef LG_CalIntr_h__
#define LG_CalIntr_h__

#pragma once

#include <afxwin.h>

#include "../LT_Cal_Library/LGIT_OMS_Entry_2DCal/LGCalibIntrinsic.h"
#include "../LT_Cal_Library/LGIT_OMS_Entry_2DCal/LGCalibIntrParam.h"
#include "../LT_Cal_Library/LGIT_OMS_Entry_2DCal/LGCalibIntrResult.h"

#define MAX_2DCAL_Positions			15	// 영상 캡쳐 후 코너점 추출을 하는 최대 단계
#define MAX_2DCAL_Corner_Points		40	// 코너점 추출 데이터 최대 개수
#define MAX_2DCAL_Re_KK				4	// KK 배열 크기
#define MAX_2DCAL_Re_Kc				5	// Kc 배열 크기
#define MAX_2DCAL_arcDate			6	// arcDate 배열 크기

//=============================================================================
// CLG_CalIntr
//=============================================================================
class CLG_CalIntr
{
public:
	CLG_CalIntr();
	~CLG_CalIntr();
	
protected:	

	UINT*				m_pnLoadIdx			= NULL;
	
	LGCalibIntrinsic	m_LG_CalIntr;		// Intrinsic 라이브러리 클래스
	LGCalibIntrParam	m_stLG_CalParam;	// 설정 파라미터 클래스
	LGCalibIntrResult	m_stLG_CalResult;	// Intrinsic 함수 결과 구조체
	eLGCalibIntrMsg		m_nLG_CalMessage;	// Intrinsic 함수 결과 메세지
	
	// 코너점 추출 데이터
	float				m_arfCornerPt[MAX_2DCAL_Positions][MAX_2DCAL_Corner_Points * 2];	// U, V float 데이터 2개

	void				OnTracePrint		(__in eLGCalibIntrMsg eRet);

public:

	eLGCalibIntrMsg	SetCalResultExecute		(__in UINT nLoadFileCnt);

	void			SetCalCornerPtData		(__in float* pfCornetPt, __in UINT nIndex)
	{
		memcpy(&m_arfCornerPt[nIndex], pfCornetPt, sizeof(float) * 2 * MAX_2DCAL_Corner_Points);
	}

	void			SetCalCornerPtData		(__in float* pfCornetPt, __in UINT* pnLoadIdx, __in UINT nIndex)
	{
		memcpy(&m_arfCornerPt[nIndex], pfCornetPt, sizeof(float) * 2 * MAX_2DCAL_Corner_Points);
		m_pnLoadIdx = pnLoadIdx;
	}

	void			GetCalResultParam		(__out LGCalibIntrResult* stResult)
	{
		memcpy(&stResult->arfKK,	&m_stLG_CalResult.arfKK,	sizeof(float)* MAX_2DCAL_Re_KK);
		memcpy(&stResult->arfKc,	&m_stLG_CalResult.arfKc,	sizeof(float)* MAX_2DCAL_Re_Kc);
		memcpy(&stResult->arcDate,	&m_stLG_CalResult.arcDate,	sizeof(unsigned char)* MAX_2DCAL_arcDate);

		stResult->fDistFOV				= m_stLG_CalResult.fDistFOV;
		stResult->fR2Max				= m_stLG_CalResult.fR2Max;
		stResult->fRepError				= m_stLG_CalResult.fRepError;
		stResult->fEvalError			= m_stLG_CalResult.fEvalError;
		stResult->fOrgOffset			= m_stLG_CalResult.fOrgOffset;
		stResult->nDetectionFailureCnt	= m_stLG_CalResult.nDetectionFailureCnt;
		stResult->nInvalidPointCnt		= m_stLG_CalResult.nInvalidPointCnt;
		stResult->nValidPointCnt		= m_stLG_CalResult.nValidPointCnt;
	}
	
	// 코너점 추출 데이터로 ExecCalIntrinsic 실행
	//DLLAPI eLGCalibIntrMsg setParam(LGCalibIntrParam *pstParam_ext);
	BOOL			Exec_CalIntrinsic		(__in LGCalibIntrParam* pstInParameter, __in const float** ppInCornerPt, __in UINT nConerPtStepCount, __out LGCalibIntrResult& stOutResult);

	// 이미지로 코너점 추출
	//DLLAPI eLGCalibIntrMsg findChessBoard(const unsigned short *imgData, float* fcbResult, LGCalibRoiParam* calibRoi = NULL);
	BOOL			Get_ConerPoint			(__in const unsigned short* pwInImage, __in LGCalibRoiParam* pstInROI, __out float* pfOutResult, __in_opt bool bImageShow = false);

	// 코너 데이터 초기화
	BOOL			Clear(){};

	BOOL			Set_Parameter			(__in LGCalibIntrParam *pstParam_ext);
};

#endif // LG_CalIntr_h__

