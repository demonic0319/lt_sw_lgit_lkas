/**
@file	LGCalibIntrResult.h
@date   2017.10.11
@author
Yongji Yun(yongji.yun@lge.com)
@brief
Factory Intrinsic Calibration Result for OMS Camera
@remark
@warning
Copyright(C) 2012 LG Electronics Co., LTD., Seoul, Korea
All Rights are Reserved.
*/

#pragma once

/**
@brief LG Factory Intrinsic Calibration Result
*/
typedef struct
{

	float arfKK[4];				/*!< camera intrinsic parameter 0: Fu, 1: Fv, 2: Pu, 3: Pv*/
	float arfKc[5];				/*!< distortion coefficient */
	float fDistFOV;				/*!< distortion FOV(degree) */
	float fR2Max;				/*!< Internal value related in distortion */
	float fRepError;			/*!< Reprojection error */
	float fEvalError;			/*!< Distance error between real distance (RMS) */
	float fOrgOffset;			/*!< Origin offset for distance calibration (distance between theoratical value and external measered)
								GT = Chess - Offset */
	int nDetectionFailureCnt;	/*!< Corner detection failure count */
	int nInvalidPointCnt;		/*!< Number of outlier */
	int nValidPointCnt;			/*!< Number of inlier */

	char arcDate[6];				/* yy/mm/dd */

}LGCalibIntrResult;
