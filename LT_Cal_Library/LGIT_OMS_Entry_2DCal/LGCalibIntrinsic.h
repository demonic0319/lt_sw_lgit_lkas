/**
@file	LGCalibIntrinsic.h
@date   2017.10.11
@author
Yongji Yun(yongji.yun@lge.com)
@brief
Factory Intrinsic Calibration for OMS Camera
@remark
@warning
Copyright(C) 2012 LG Electronics Co., LTD., Seoul, Korea
All Rights are Reserved.
*/

/**
@mainpage Factory Intrinsic Calibration for OMS Camera
@section Intro Definition
- The available space to maneuver a road vehicle so as to avoid collision with objects. Any obstacle limits the free space.
@section CREATEINFO Create Information
- @author yongji.yun@lge.com
- @date 2017.10
@section  MODIFYINFO Modify Information
- 2017.10.11 / yongji.yun@lge.com : Make Initial verstion & doxygen page
- 2018.01.24 / khyun.kim@lginnotek.com : Add and modify some API for LGIT EOL
@section INOUTPUT Input/Output
- INPUT Corner points of chess board chart, Ground truth of distance
- OUTPUT Intrinsic file
@section Example
\code

// OMS_CAL_TEST.cpp : APPLICATION EXAMPLE CODE
//

#include "stdafx.h"
#include <assert.h> 
#include <stdio.h>
#include "LGCalibIntrinsic.h"


void main()
{
	LGCalibIntrinsic CalibIntr;
	LGCalibIntrParam stParam;
	eLGCalibIntrMsg eRet;
	LGCalibIntrResult stIntrResult;

	// initalize
	stParam.nImgWidth = 640;
	stParam.nImgHeight = 480;
	stParam.nNumofCornerX = 4;
	stParam.nNumofCornerY = 6;
	stParam.fPatternSizeX = 60.f;
	stParam.fPatternSizeY = 60.f;
	stParam.fRepThres = 1.f;
	stParam.nMinNumImages = 11;
	stParam.nMinNumValidPnts = 200;
	stParam.fMinOriginOffset = -2.f;
	stParam.fMaxOriginOffset = 2.5f;
	stParam.fMinFocalLength = 311.f;
	stParam.fMaxFocalLength = 314.f;
	stParam.stCalibInfo_default.arfKK[0] = 310.f;
	stParam.stCalibInfo_default.arfKK[1] = 310.f;
	stParam.stCalibInfo_default.arfKK[2] = 320.f;
	stParam.stCalibInfo_default.arfKK[3] = 240.f;
	stParam.stCalibInfo_default.arfKc[0] = -0.2f;
	stParam.stCalibInfo_default.arfKc[1] = 0.045f;
	stParam.stCalibInfo_default.arfKc[2] = 0.f;
	stParam.stCalibInfo_default.arfKc[3] = 0.f;
	stParam.stCalibInfo_default.arfKc[4] = -0.0045f;
	stParam.stCalibInfo_default.fR2Max = 4.4f;
	stParam.fVersion = 1.0f;
	eRet = CalibIntr.setParam(&stParam);
	if (eRet != eLGINTRMSG_NOERR)
	{
		assert(0);
	}

	eRet = CalibIntr.setOutputFileName("./omscal_intrinsic.bin");
	if (eRet != eLGINTRMSG_NOERR)
	{
		assert(0);
	}
	eRet = CalibIntr.setDate("180124");
	if (eRet != eLGINTRMSG_NOERR)
	{

	}

	// execute
	CalibIntr.clearAll();
	const int nNumofEval = 3;
	const int arnEvalIdx[nNumofEval] = { 1, 6, 8 };
	const float arfEvalDist[nNumofEval] = { 200.f, 300.f, 400.f };
	const int nDataSize = stParam.nNumofCornerX * stParam.nNumofCornerY * 2;
	for (int nIdx0 = 0; nIdx0 < stParam.nMinNumImages; nIdx0++)
	{
		// receive corner points
		char arcFileName[512];
		float *pfPoints = new float[nDataSize];
		sprintf(arcFileName, "./data/CapOA_%05d.png", nIdx0);
		Mat img = imread(arcFileName, CV_LOAD_IMAGE_ANYDEPTH);
		//eRet = CalibIntr.findChessBoard(arcFileName, pfPoints);
		eRet = CalibIntr.findChessBoard((const unsigned short *)img.data, pfPoints);
		if (eRet != eLGINTRMSG_NOERR)
		{
			assert(0);
		}
		eRet = CalibIntr.addPointsCalib(pfPoints);
		if (eRet != eLGINTRMSG_NOERR)
		{
			assert(0);
		}

		for (int nEvalIdx = 0; nEvalIdx < nNumofEval; nEvalIdx++)
		{
			const int nIdx1 = arnEvalIdx[nEvalIdx];
			if (nIdx1 > nIdx0)
			{
				break;
			}
			else if (nIdx0 == nIdx1)
			{
				eRet = CalibIntr.addPointsEval(pfPoints, arfEvalDist[nEvalIdx]);
				if (eRet != eLGINTRMSG_NOERR)
				{
					assert(0);
				}
			}
		}
		delete[] pfPoints;
	}

	eRet = CalibIntr.execIntrinsic(&stIntrResult);
	if (eRet != eLGINTRMSG_NOERR)
	{
		assert(0);
	}
	CalibIntr.printResult("./omscal_intrinsic.bin");
	printf("2d cal end\n");
	getchar();
}


\endcode
*/


#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
using namespace cv;

#include "LGCalibIntrParam.h"
#include "LGCalibIntrResult.h"
#include <vector>

#ifdef _USRDLL
#define DLLAPI __declspec(dllexport) // 내자신(DLL을 만드는 프로젝트)이 include하면 export
#else
#define DLLAPI __declspec(dllimport) // DLL사용자가 include 하면 import되도록 한다.
#endif


typedef enum
{
	eLGINTRMSG_NOERR,
	eLGINTRMSG_ERR,
	eLGINTRMSG_ERR_FILENAME,
	eLGINTRMSG_ERR_DATE,
	eLGINTRMSG_ERR_IMAGESIZE,
	eLGINTRMSG_ERR_CHARTSPEC,
	eLGINTRMSG_ERR_THRES,
	eLGINTRMSG_ERR_SEARCHRANGE,
	eLGINTRMSG_ERR_VERSION,
	eLGINTRMSG_ERR_CORNERPOINTS, /* invalid corner points */
	eLGINTRMSG_ERR_EVALDISTANCE, /* the evaluation distance is already exist */
	eLGINTRMSG_ERR_NEEDMOREIMAGES,
	eLGINTRMSG_ERR_NEEDMOREPOINTS,
	eLGINTRMSG_ERR_NOTEXISTFILE,
	eLGINTRMSG_ERR_INVALIDFILE,
	eLGINTRMSG_ERR_FILEWRITEFAILURE,
	eLGINTRMSG_SIZE
}eLGCalibIntrMsg;

//EXTERNC DLLAPI
class LGCalibIntrinsic
{
public:
	DLLAPI LGCalibIntrinsic();
	DLLAPI ~LGCalibIntrinsic();

	/**
	@fn       eLGCalibIntrMsg clearAll()
	@brief	  clear all corner point information
	@see
	*/
	DLLAPI void clearAll() { m_vvCornerCalib.clear(); m_vvCornerEval.clear(); m_vDistanceEval.clear(); m_nFailureCnt = 0; m_nCnt = 0; };


	/**
	@fn       eLGCalibIntrMsg setOutputFileName(const char *pcOutputFileName)
	@brief    Set output file name function.
	@param    pcOutputFileName: Full path or relative path and file name. maximum size is 512. ex) "./omscal_intrinsic.bin"
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_FILENAME: input is null
	@see
	*/
	DLLAPI eLGCalibIntrMsg setOutputFileName(const char *pcOutputFileName);

	/**
	@fn       eLGCalibIntrMsg setDate(const char *pcDate)
	@brief    Set current date function.
	@param    pcDate: current date. need char 6 of array ex) "171011"
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_DATE: input is null
	@see
	*/
	DLLAPI eLGCalibIntrMsg setDate(const char *pcDate); /* YYMMDD */

	/**
	@fn       eLGCalibIntrMsg setParam(LGCalibIntrParam *pstParam_ext)
	@brief    Set param function
	@param    pstParam_ext: parameter set
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_IMAGESIZE: need image size parameter (nImgWidth, nImgheight)
	- eLGINTRMSG_ERR_CHARTSPEC: need chart spec parameter (fPatternSizeX, fPatternSizeY, nNumofCornerX, nNumofCornerY)
	- eLGINTRMSG_ERR_THRES: need threshold parameter (fRepThres, nMinNumImages, nMinNumValidPnts)
	- eLGINTRMSG_ERR_SEARCHRANGE: need search range parameter (fMaxOriginOffset, fMinOriginOffset, fMaxFocalLength, fMinFocalLength)
	@see
	*/
	DLLAPI eLGCalibIntrMsg setParam(LGCalibIntrParam *pstParam_ext);

	/**
	@fn       eLGCalibIntrMsg addPointsCalib(const float *pstPoints)
	@brief    add points for calculating intrinsic
	@param    pstPoints: points array size is nNumofCornerX * nNumofCornerY * 2, even is u, odd is v
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_CORNERPOINTS: input is null
	@see
	*/
	DLLAPI eLGCalibIntrMsg addPointsCalib(const float *pstPoints);

	/**
	@fn       eLGCalibIntrMsg addPointsEval(const float *pstPoints, const float fDistance)
	@brief	  add points for evaluation to finding optimal value
	@param    pstPoints: points array size is nNumofCornerX * nNumofCornerY * 2, even is u, odd is v
	@param    fDistance: real distance
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_CORNERPOINTS: input points is null
	- eLGINTRMSG_ERR_EVALDISTANCE: distance already exist
	@see
	*/
	DLLAPI eLGCalibIntrMsg addPointsEval(const float *pstPoints, const float fDistance);

	/**
	@fn       eLGCalibIntrMsg execIntrinsic(LGCalibIntrResult *pstResult)
	@brief    execute intrinsic calibration
	@param    pstResult: result structure
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_CHARTSPEC: Does not match the number of points defined in paramter
	- eLGINTRMSG_ERR_NEEDMOREIMAGES: The number of images is less than the threshold
	- eLGINTRMSG_ERR_NEEDMOREPOINTS: The number of points is less than the threshold
	- eLGINTRMSG_ERR_FILEWRITEFAILURE: file writing is failure
	@see
	*/
	DLLAPI eLGCalibIntrMsg execIntrinsic(LGCalibIntrResult *pstResult);

	/**
	@fn       eLGCalibIntrMsg execMinIntrinsic(LGCalibIntrResult *pstResult)
	@brief    execute minimized intrinsic calibration(without GT offset finding)
	@param    pstResult: result structure
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_CHARTSPEC: Does not match the number of points defined in paramter
	- eLGINTRMSG_ERR_NEEDMOREIMAGES: The number of images is less than the threshold
	- eLGINTRMSG_ERR_NEEDMOREPOINTS: The number of points is less than the threshold
	- eLGINTRMSG_ERR_FILEWRITEFAILURE: file writing is failure
	@see
	*/
	DLLAPI eLGCalibIntrMsg execMinIntrinsic(LGCalibIntrResult *pstResult);

	/**
	@fn       eLGCalibIntrMsg printResult(const char *pcOutputFileName)
	@brief    print result for debugging
	@param    pcOutputFileName: path of result file
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_NOTEXISTFILE: Result file does not exist.
	- eLGINTRMSG_ERR_INVALIDFILE: Result file is invalid.

	@see
	*/
	DLLAPI eLGCalibIntrMsg printResult(const char *pcOutputFileName);

	/**
	@fn       eLGCalibIntrMsg findChessBoard(const std::string &imgFileName, float* fcbResult)
	@brief    find chessboard corner points
	@param    imgFileName : path of image file which is to find corner.
	@param    fcbResult : array for corner points.
	@param    calibRoi : ROI to find corners.
	@param    imgShow : flag whether image diagram is shown.
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_CORNERPOINTS: Can't find chessboard corner.

	@see
	*/
	DLLAPI eLGCalibIntrMsg findChessBoard(const std::string &imgFileName, float* fcbResult, LGCalibRoiParam* calibRoi = NULL, bool imgShow = false);

	/**
	@fn       eLGCalibIntrMsg findChessBoard(const unsigned short *imgData, float* fcbResult)
	@brief    find chessboard corner points
	@param    imgData : image frame data.
	@param    fcbResult : array for corner points.
	@param    calibRoi : ROI to find corners.
	@param    imgShow : flag whether image diagram is shown.
	@return	  eLGCalibIntrMsg
	- eLGINTRMSG_NOERR
	- eLGINTRMSG_ERR_CORNERPOINTS: Can't find chessboard corner.

	@see
	*/
	DLLAPI eLGCalibIntrMsg findChessBoard(const unsigned short *imgData, float* fcbResult, LGCalibRoiParam* calibRoi = NULL, bool imgShow = false);


private:
	eLGCalibIntrMsg LGCalibIntrinsic::findChessBoard_(const unsigned short *imgData, float* fcbResult, LGCalibRoiParam* calibRoi, bool imgShow);
	char m_arcDate[6];							/* YYMMDD */
	char m_arcOutputFileName[512];
	vector<vector<Point2f>> m_vvCornerCalib;
	vector<vector<Point2f>> m_vvCornerEval;
	vector<float> m_vDistanceEval;
	vector< Point3f> m_vWorldTemplete;
	LGCalibIntrParam m_stParam;
	int m_nFailureCnt;
	int m_nCnt;
};


