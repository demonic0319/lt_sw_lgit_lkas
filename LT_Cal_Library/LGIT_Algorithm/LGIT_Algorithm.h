//*****************************************************************************
// Filename	: 	LG_LGIT_Algorithm.h
// Created	:	2017/10/16 - 14:50
// Modified	:	2017/10/16 - 14:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef LG_LGIT_Algorithm_h__
#define LG_LGIT_Algorithm_h__

#pragma once

#include <afxwin.h>

#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISResolution/ACMISResolutionSFR.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISImage/ACMISImageBlemish.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISImage/ACMISImageStain.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISImage/ACMISSignalNoiseRatio.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISShading/ACMISShadingUniformity.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISShading/ACMISShadingRelativeIllumination.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISImage/ACMISMechanicalOpticalCenter.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISImage/ACMISImageDarkTest.h"
#include "../LT_Cal_Library/LGIT_Algorithm/LibACMISSoftISP\SoftISP.h"

#include <opencv2/core/core.hpp>  
#include <opencv2/highgui/highgui.hpp>  
#include <opencv2/imgproc/imgproc_c.h>

#define PDC_LAVENDER				RGB(230, 230, 250)
#define PDC_GREEN					RGB(0, 128, 0)
#define PDC_LIGHTGREEN				RGB(144, 238, 144)
#define PDC_YELLOW					RGB(255, 255, 0)
#define PDC_BLACK					RGB(0, 0, 0)
#define PDC_RED						RGB(255, 0, 0)
#define PDC_BLUE					RGB(0, 0, 255)

#define CV_LAVENDER					CV_RGB(230, 230, 250)
#define CV_GREEN					CV_RGB(0, 128, 0)
#define CV_LIGHTGREEN				CV_RGB(144, 238, 144)
#define CV_YELLOW					CV_RGB(255, 255, 0)
#define CV_BLACK					CV_RGB(0, 0, 0)
#define CV_RED						CV_RGB(255, 0, 0)
#define CV_BLUE						CV_RGB(0, 0, 255)

#define R_RESULT_PASS							0	//양품 
#define R_RESULT_FAIL							1	//불량
#define R_FAIL_NOISE							2
#define R_FAIL_BLACK_SPOT						3

using namespace std;

typedef enum _CM_TEST_MODEL
{
	MODEL_IKC = 0,
	MODEL_MRA2,
	MODEL_NIO, //나중에 없애야함 LJE
	MODEL_MAX,

} CM_TEST_MODEL;


typedef struct _RawImgInfo
{
	int nSensorWidth;
	int nSensorHeight;

	int nDisplayStartPosY;
	int nDisplayStartPosX;
	int nDisplaySizeY;
	int nDisplaySizeX;

} RawImgInfo;

typedef enum
{
	Edge_gradient_Fail = 101,
	ROI_Width_Fail = 103,
	ROI_Height_Fail,
	Spatial_frequency_Fail,
	SFR_Result_Fail,
	ROI_Range_Fail = 108,
	Parameter_err,
	Edge_direction_Fail,
	Fail_Max,
} eSFRFailMsg; //102,107 없음

typedef struct __TStainSpec
{
	TBlackSpotContrast stSpecBlackSpot;
	TLCBSpec stSpecLCB;
	TRU_YmeanSpec stSpecYmean;
} TStainSpec;


//=============================================================================
// LG_LGIT_Algorithm
//=============================================================================
class CLG_LGIT_Algorithm
{
public:
	CLG_LGIT_Algorithm();
	~CLG_LGIT_Algorithm();

protected:

	CACMISResolutionSFR											m_LG_SFR;						// SFR 라이브러리 클래스
	CACMISSignalNoiseRatioBW									m_LG_Dynamic_Range_SNR_BW;		// IDynamic_Range_SNR_BW 라이브러리 클래스
	CACMISShadingUniformity										m_LG_RelativeUniformity;		// RelativeUniformity 라이브러리 클래스
	CACMISImageBlackSpotContrastCommon							m_LG_BlackSpot;					// BlackSpot 라이브러리 클래스
								
	// 공통 부문
	EDATAFORMAT				m_eDataFormat;
	EOUTMODE				m_eOutMode;
	ESENSORTYPE				m_eSensorType;
	int						m_iBlackLevel;

	// SFR Config
	int						m_iMaxROIWidth;
	int						m_iMaxROIHeight;
	double					m_dbMaxEdgeAngle;
	double					m_dbPixelSize;
	ESFRAlgorithmType		m_eType;
	ESFRAlgorithmMethod		m_eMethod;
	bool					m_bEnableLog;
	ESFRFrequencyUnit		m_eConverse;

	// Rotation Config
	TFiducialMarkInfo		m_stFiducialMarkInfo;

	// Shading Config
	bool					m_bUsing8Bit;
	TShadingUniformitySpec  m_ShadingUniformitySpec;
	TRelativeIlluminationSpec m_stRelativeIlluminationSpec;
	
	// DynamicBW Config 
	TSNRBWSpec				m_SNRBWSpec;

	// Ymean Config
	TStainSpec				tStainSpec;

	TBlackSpotContrast		m_stSpecBlackSpot;
	TLCBSpec				m_stSpecLCB;
	TRU_YmeanSpec			m_stSpecYmean;

	TDefectPixel			m_stDarkDefectPixel;
	TDefectPixel			m_stBrightDefectPixel;

public:

	// LGIT 공통 부문
	void LGIT_SetDataSpec	(__in EDATAFORMAT eDataFormat, __in EOUTMODE eOutMode, __in ESENSORTYPE eSensorType, __in int iBlackLevel)
	{
		m_eDataFormat	= eDataFormat;
		m_eOutMode		= (EOUTMODE)(eOutMode + 1);	// UI이랑 맞출려고..
		m_eSensorType	= eSensorType;
		m_iBlackLevel	= iBlackLevel;
	}

	// LGIT SFR Config
	void LGIT_SFR_Config	(__in int iMaxROIWidth, __in int iMaxROIHeight, __in double dbMaxEdgeAngle, __in double dbPixelSize, __in ESFRAlgorithmType eType, __in ESFRAlgorithmMethod eMethod, __in bool	bEnableLog)
	{
		m_iMaxROIWidth		= iMaxROIWidth;
		m_iMaxROIHeight		= iMaxROIHeight;
		m_dbMaxEdgeAngle	= dbMaxEdgeAngle;
		m_dbPixelSize		= dbPixelSize;
		m_eType				= eType;	
		m_eMethod			= eMethod;	
		m_bEnableLog		= bEnableLog;
	}


	// LGIT SFR Config
	void LGIT_SFR_Config(__in int iMaxROIWidth, __in int iMaxROIHeight, __in double dbMaxEdgeAngle, __in double dbPixelSize, __in ESFRAlgorithmType eType, __in ESFRAlgorithmMethod eMethod, __in bool	bEnableLog, __in ESFRFrequencyUnit eConverse)
	{
		m_iMaxROIWidth = iMaxROIWidth;
		m_iMaxROIHeight = iMaxROIHeight;
		m_dbMaxEdgeAngle = dbMaxEdgeAngle;
		m_dbPixelSize = dbPixelSize;
		m_eType = eType;
		m_eMethod = eMethod;
		m_bEnableLog = bEnableLog;
		m_eConverse = eConverse;
	}

	// LGIT Shading Config
	void LGIT_RI_Config(__in double dBoxField, __in UINT nBoxSize, __in double dSpecRIcornerMax, __in double dSpecRIcornerMin, __in double dSpecRIminMax, __in double dSpecRIminMin, __in bool bUsing8Bit)
	{
		memset(&m_stRelativeIlluminationSpec, 0x00, sizeof(TRelativeIlluminationSpec));

		m_stRelativeIlluminationSpec.dBoxField = dBoxField; // 0.8;
		m_stRelativeIlluminationSpec.nBoxSize = nBoxSize; // 150;
		m_stRelativeIlluminationSpec.dSpecRIcornerMax = dSpecRIcornerMax; // 200;
		m_stRelativeIlluminationSpec.dSpecRIcornerMin = dSpecRIcornerMin;  // - 200;
		m_stRelativeIlluminationSpec.dSpecRIminMax = dSpecRIminMax; //200;
		m_stRelativeIlluminationSpec.dSpecRIminMin = dSpecRIminMin; // -200;
		m_bUsing8Bit = bUsing8Bit;

		//m_bEnableLog = bEnableLog;
	}

	// LGIT Shading Config
	void LGIT_Shading_Config(__in int iShadingROIWidth, __in int iShadingROIHeight, __in int iShadingROIMax, __in int iNormalizedIndex, __in double dHorizonThresMin, __in double dHorizonThresMax, __in double dVerticalThresMin, __in double dVerticalThresMax, __in double dDiagolThresMin, __in double dDiagolThresMax)// , __in bool	bEnableLog)
	{
		memset(&m_ShadingUniformitySpec, 0x00, sizeof(TShadingUniformitySpec));

		m_ShadingUniformitySpec.nROIWidth = 32;//iShadingROIWidth;
		m_ShadingUniformitySpec.nROIHeight = 24;// iShadingROIHeight;
		m_ShadingUniformitySpec.nMaxROICount = iShadingROIMax;
		m_ShadingUniformitySpec.nNormalizeIndex = iNormalizedIndex;
		m_ShadingUniformitySpec.dHorThdMin = 9.5; //dHorizonThresMin;
		m_ShadingUniformitySpec.dHorThdMax = 9.5; //dHorizonThresMax;
		m_ShadingUniformitySpec.dVerThdMin = 9.5; //dVerticalThresMin;
		m_ShadingUniformitySpec.dVerThdMax = 9.5; // dVerticalThresMax;		
		m_ShadingUniformitySpec.dDiaThdMin = 9.5; // dDiagolThresMin;
		m_ShadingUniformitySpec.dDiaThdMax = 9.5; //dDiagolThresMax;

		//m_bEnableLog = bEnableLog;
	}

	// LGIT Shading Config Array
	void LGIT_Shading_Config_Array(__in double* dHorizonThreshold, __in double* dVerticalThreshold, __in double* dDiaThreshold, __in POINT* ppHorizonPoint, __in POINT* ppVerticalPoint, __in POINT* ppDiaAPoint, __in POINT* ppDiaBPoint, __in int MaxShadingRoiNum)// , __in bool	bEnableLog)
	{
		m_ShadingUniformitySpec.dHorThreshold = dHorizonThreshold;
		m_ShadingUniformitySpec.dVerThreshold = dVerticalThreshold;
		m_ShadingUniformitySpec.dDiaThreshold = dDiaThreshold;
		m_ShadingUniformitySpec.ptHorROI = ppHorizonPoint;
		m_ShadingUniformitySpec.ptVerROI = ppVerticalPoint;
		m_ShadingUniformitySpec.ptDiaAROI = ppDiaAPoint;
		m_ShadingUniformitySpec.ptDiaBROI = ppDiaBPoint;
	}

	// LGIT Dynamic BW Config
	void LGIT_DynamicBW_Config(__in int nDynamicROIWidth, __in int nDynamicROIHeight, __in int nLscBlockSize, __in int nDynamicMaxROICount, __in double dSNRThreshold, __in double dDRThreshold, __in POINT* ptROICenter)
	{
		memset(&m_SNRBWSpec, 0x00, sizeof(TSNRBWSpec));
		m_SNRBWSpec.nROIWidth = nDynamicROIWidth;
		m_SNRBWSpec.nROIHeight = nDynamicROIHeight;
		m_SNRBWSpec.nLscBlockSize = nLscBlockSize;
		m_SNRBWSpec.nMaxROICount = nDynamicMaxROICount;
		m_SNRBWSpec.dSNRThreshold = dSNRThreshold;
		m_SNRBWSpec.dDRThreshold = dDRThreshold;
		m_SNRBWSpec.ptROICenter = ptROICenter;
		//for (int iNum = 0; iNum < nDynamicMaxROICount;iNum++)
		//{
		//	m_SNRBWSpec.ptROICenter[iNum].x = ptROICenter[iNum].x;
		//	m_SNRBWSpec.ptROICenter[iNum].y = ptROICenter[iNum].y;
		//}
	}

	// LGIT Ymean Config
	void LGIT_Ymean_Config(__in int nDefectBlockSize, __in int nEdgeSize, __in float fCenterThreshold, __in float fEdgeThreshold, __in float fCornerThreshold, __in int nLscBlockSize, __in bool bEnableCircle, __in int nPosOffsetX, __in int nPosOffsetY, __in double dRadiusRatioX, __in double dRadiusRatioY)
	{
		memset(&m_stSpecYmean, 0x00, sizeof(TRU_YmeanSpec));
		m_stSpecYmean.nDefectBlockSize = nDefectBlockSize;
		m_stSpecYmean.nEdgeSize = nEdgeSize;
		m_stSpecYmean.fCenterThreshold = fCenterThreshold;
		m_stSpecYmean.fEdgeThreshold = fEdgeThreshold;
		m_stSpecYmean.fCornerThreshold = fCornerThreshold;
		m_stSpecYmean.nLscBlockSize = nLscBlockSize;
		m_stSpecYmean.tCircleSpec.bEnableCircle = bEnableCircle;
		m_stSpecYmean.tCircleSpec.nPosOffsetX = nPosOffsetX;
		m_stSpecYmean.tCircleSpec.nPosOffsetY = nPosOffsetY;
		m_stSpecYmean.tCircleSpec.dRadiusRatioX = dRadiusRatioX;
		m_stSpecYmean.tCircleSpec.dRadiusRatioY = dRadiusRatioY;
	}

	// LGIT LCB Config
	void LGIT_LCB_Config(__in double dCenterThreshold, __in double dEdgeThreshold, __in double dCornerThreshold, __in int nMaxSingleDefectNum, __in int nMinDefectWidthHeight, __in bool bEnableCircle, __in int nPosOffsetX, __in int nPosOffsetY, __in double dRadiusRatioX, __in double dRadiusRatioY)
	{
		memset(&m_stSpecLCB, 0x00, sizeof(TLCBSpec));
		m_stSpecLCB.dCenterThreshold = dCenterThreshold;
		m_stSpecLCB.dEdgeThreshold = dEdgeThreshold;
		m_stSpecLCB.dCornerThreshold = dCornerThreshold;
		m_stSpecLCB.nMaxSingleDefectNum = nMaxSingleDefectNum;
		m_stSpecLCB.nMinDefectWidthHeight = nMinDefectWidthHeight;
		m_stSpecLCB.tCircleSpec.bEnableCircle = bEnableCircle;
		m_stSpecLCB.tCircleSpec.nPosOffsetX = nPosOffsetX;
		m_stSpecLCB.tCircleSpec.nPosOffsetY = nPosOffsetY;
		m_stSpecLCB.tCircleSpec.dRadiusRatioX = dRadiusRatioX;
		m_stSpecLCB.tCircleSpec.dRadiusRatioY = dRadiusRatioY;
	}


	// LGIT BlackSpot Config
	void LGIT_BlackSpot_Config(__in int nBlockWidth, __in int nBlockHeight, __in int nClusterSize, __in int nDefectInCluster, __in double dDefectRatio, __in int nMaxSingleDefectNum, __in bool bEnableCircle, __in int nPosOffsetX, __in int nPosOffsetY, __in double dRadiusRatioX, __in double dRadiusRatioY)
	{
		memset(&m_stSpecBlackSpot, 0x00, sizeof(TBlackSpotContrast));
		m_stSpecBlackSpot.nBlockWidth = nBlockWidth;
		m_stSpecBlackSpot.nBlockHeight = nBlockHeight;
		m_stSpecBlackSpot.nClusterSize = nClusterSize;
		m_stSpecBlackSpot.nDefectInCluster = nDefectInCluster;
		m_stSpecBlackSpot.dDefectRatio = dDefectRatio;
		m_stSpecBlackSpot.nMaxSingleDefectNum = nMaxSingleDefectNum;
		m_stSpecBlackSpot.tCircleSpec.bEnableCircle = bEnableCircle;
		m_stSpecBlackSpot.tCircleSpec.nPosOffsetX = nPosOffsetX;
		m_stSpecBlackSpot.tCircleSpec.nPosOffsetY = nPosOffsetY;
		m_stSpecBlackSpot.tCircleSpec.dRadiusRatioX = dRadiusRatioX;
		m_stSpecBlackSpot.tCircleSpec.dRadiusRatioY = dRadiusRatioY;
	}

	// LGIT Defect Black Config
	void LGIT_DefectBlack_Config(__in int nDefectBlockSize, __in double dDefectRatio, __in int nMaxSingleDefectNum)
	{
		memset(&m_stDarkDefectPixel, 0x00, sizeof(TDefectPixel));
		m_stDarkDefectPixel.nBlockSize = nDefectBlockSize;
		m_stDarkDefectPixel.dDefectRatio = dDefectRatio;
		m_stDarkDefectPixel.nMaxSingleDefectNum = nMaxSingleDefectNum;
	}

	// LGIT Defect White Config
	void LGIT_DefectWhite_Config(__in int nDefectBlockSize, __in double dDefectRatio, __in int nMaxSingleDefectNum)
	{
		memset(&m_stBrightDefectPixel, 0x00, sizeof(TDefectPixel));
		m_stBrightDefectPixel.nBlockSize = nDefectBlockSize;
		m_stBrightDefectPixel.dDefectRatio = dDefectRatio;
		m_stBrightDefectPixel.nMaxSingleDefectNum = nMaxSingleDefectNum;
	}

	void LGIT_Rotation_Config(__in int nROIBoxSize, __in int nMaxROIBoxSize, __in double dRadius, __in double dRealGapX, __in double dRealGapY, __in int nFiducialMarkNum, __in int nFiducialMarkType, __in double dModuleChartDistance, __in int nDistortionAlrotithmType)
	{
		memset(&m_stFiducialMarkInfo, 0x00, sizeof(TFiducialMarkInfo));

		m_stFiducialMarkInfo.dDistanceXFromCenter = 220;
		m_stFiducialMarkInfo.dDistanceYFromCenter = 160;
		m_stFiducialMarkInfo.nROIBoxSize = nROIBoxSize;
		m_stFiducialMarkInfo.nMaxROIBoxSize = nMaxROIBoxSize;
		m_stFiducialMarkInfo.dRadius = dRadius;
		m_stFiducialMarkInfo.dRealGapX = dRealGapX;
		m_stFiducialMarkInfo.dRealGapY = dRealGapY;
		m_stFiducialMarkInfo.nFiducialMarkNum = 9;///nFiducialMarkNum;
		m_stFiducialMarkInfo.nFiducialMarkType = 2;// nFiducialMarkType;
		m_stFiducialMarkInfo.dModuleChartDistance = dModuleChartDistance;
		m_stFiducialMarkInfo.nDistortionAlrotithmType = 2;//nDistortionAlrotithmType;

		switch (m_stFiducialMarkInfo.nFiducialMarkType)
		{
		case 1:
			m_stFiducialMarkInfo.pszChartType = "DOT";
			break;
		case 2:
			m_stFiducialMarkInfo.pszChartType = "GRID";
			break;
		case 3:
			m_stFiducialMarkInfo.pszChartType = "CROSSDOT";
			break;
		}
	}


	// LGIT SFR Result
	bool LGIT_SFR_Func		(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in RECT rtROI, __in int iEdgeDir, __in double dbFrequencyLists, __in double dbSFR, __in int iMaxFrequency, __out double& dbResult);
	
	bool LGIT_Shading_Func	(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out double& dbCenterResult,
		__out CPoint* resultHorizonROI, __out CPoint* resultVerticalROI, __out CPoint* resultDiaAROI, __out CPoint* resDiaBROI,
		__out double* resultHorizonData, __out double* resultVerticalData, __out double* resultDiaAData, __out double* resultDiaBData,
		__out bool* Horresult, __out bool* Verresult, __out bool* DiaAresult, __out bool* DiaBresult);

	bool LGIT_DynamicBW_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CPoint* resultROI, __out double* resultVarData, __out double* resultAverData, __out double& resultSNRData, __out double& resultDRData);
	
	bool LGIT_RelativeIllumination_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* rtResultROI, __out double& dResultRICorner, __out double& dResultRIMin, __out double* dResultData, __in double dbCorneroffset, __in double dbMinoffset);

	int LGIT_Ymean_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI,	__out int& nDefectCount,
		__out int& nCenterCount, __out double& dCenterMaxValue, __out CPoint& pCenterMaxPoint,
		__out int& nEdgeCount, __out double& dEdgeMaxValue, __out CPoint& pEdgeMaxPoint,
		__out int& nCornerCount, __out double& dCornerMaxValue, __out CPoint& pCornerMaxPoint);
	
	int LGIT_Ymean_Wide_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI,
		__out int& nDefectCount, __out int& nCircleCount, __out double& pCircleMaxValue, __out CPoint& pCircleMaxPoint,
		__out int& ocx, __out int& ocy, __out int& radx, __out int& rady);

	int LGIT_BlackSpot_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI, __out int& nDefectCount);
	int LGIT_BlackSpot_Wide_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI,
		__out int& nDefectCount, __out int& ocx, __out int& ocy, __out int& radx, __out int& rady);

	int LGIT_LCB_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI, __out int& nDefectCount);
	int LGIT_LCB_Wide_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI,
		__out int& nDefectCount, __out int& ocx, __out int& ocy, __out int& radx, __out int& rady);

	int LGIT_Rotation_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in RECT *rtROI, __out double &dResultRotation);


	int LGIT_DefectBlack_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CPoint* resultROI, __out int& nDefectCount);
	int LGIT_DefectWhite_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CPoint* resultROI, __out int& nDefectCount);


	int LoadImageData(unsigned char* pDataBuffer, const char * _Filename);						//LAW Image 불러오는 함수
	CvSize LoadBMPImageData(unsigned char* pDataBuffer, const char* _Filename);					//BMP Image 불러오는 함수
	CvSize LoadPNGImageData(unsigned char* pDataBuffer, const char* _Filename);					//PNG Image 불러오는 함수
	int GetImageData(CM_TEST_MODEL eTestModel, TDATASPEC &tDataSpec, RawImgInfo &stImgInfo);	//Model Setting 함수
	int GetImageOffset(TDATASPEC &tDataSpec, RawImgInfo &stImgInfo);							//이미지에 대한 픽셀당 바이트 보정 (이미지 데이터 크기 보정)
	void OnTraceSFRPrint(__in eSFRFailMsg eRet);												//SFRFail에 대한 정보
	void SpiltFileName(string &pathname);														//파일 이름 자르기

	//SFR 관련 함수
	double* CalulateSFR(TDATASPEC &tDataSpec, BYTE *pBuffer, int nWidth, int nHeight, int nMaxROINum, int nMaxFreqeuncyNum, int *nDirection, POINT *ptROICenter, int ROI_WIDTH, int ROI_HEIGHT, double *dFrequency, int *pTxtPosition, bool bRaw, double dPixelSize, double dMaxEdgeAngle, const char *ImagePath = nullptr, bool bSaveResultImage = false);
	double* CalulateFreq(TDATASPEC &tDataSpec, BYTE *pBuffer, int nWidth, int nHeight, int nMaxROINum, int *nDirection, POINT *ptROICenter, int ROI_WIDTH, int ROI_HEIGHT, double dSFR, int *pTxtPosition, bool bRaw, double dPixelSize, double dMaxEdgeAngle, const char *ImagePath = nullptr, bool bSaveResultImage = false);
	int DisplayOutImage(char *BmpBuffer, int nWidth, int nHeight, RECT *pROI, double *pFinalResult, int *pTxtPosition, int nMaxROINum, const char *ImagePath = nullptr, bool bSaveResultImage = false);
	void DisplaySFRGraphics(IplImage* _cvImgBuf, RECT* _pROI, double* _SFRROIResult, int* _TxtPositon, TChartSpec* _spec, int _nMaxROINum, int _nImageWidth, double nFontSize, CvScalar color);

	//Blemish 관련 함수
	int InspectStain(TDATASPEC &tDataSpec, TStainSpec &tStainSpec, BYTE *pBuffer, int nWidth, int nHeight, bool bRaw = true, const char *ImagePath = nullptr, bool bEnableBlackSpot = true,  bool bSaveResultImage = false);
	int InspectBlackSpotContrast(const BYTE* pBuffer, int nWidth, int nHeight, TBlackSpotContrast& _Spec, EDATAFORMAT dataFormat, EOUTMODE outMode, ESENSORTYPE sensorType, int nBlackLevel, IplImage *cvImgBlackSpot);
	
	int Inspect_ShaingUniformity(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TShadingUniformitySpec& _Spec, EDATAFORMAT dataFormat, EOUTMODE outMode, ESENSORTYPE sensorType, int nBlackLevel);
	int InspectSNRBW2(TDATASPEC &tDataSpec, TSNRBWSpec &tSNRSpec, BYTE *pBuffer, int nWidth, int nHeight, bool bRaw = true, const char *ImagePath = nullptr, bool bSaveResultImage = false);
	void DisplaySNRGraphics(IplImage* _cvImgBuf, const TSNRResult* _SNRResult, int _nImageWidth, CvScalar color);

	////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////SH MAKE ALGOL////////////////////////////////////////////
};

#endif // LG_LGIT_Algorithm_h__

