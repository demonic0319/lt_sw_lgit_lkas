#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"

class CColorSensitivity;

class ACMISSHADING_API CACMISShadingColorSensitivity
{
public:
	CACMISShadingColorSensitivity(void);
	~CACMISShadingColorSensitivity(void);

	bool SetFactorValue(bool bEnable, double rg_factor, double bg_factor, double grgb_factor);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TColorShadingSpec& _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TColorShadingSpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TColorShadingSpec& _Spec);

	const TColorRatio* GetInspectionResult(void);

	const char* GetLogHeader(void);

	const char* GetLogData(void);

	bool InSpec(const TColorShadingSpec& _Spec);

	const RECT* GetInspectionROI(); 

private:
	CColorSensitivity* m_pMethod;
};

