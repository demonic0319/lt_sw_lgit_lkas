#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"
//#include "LibACMISSoftISP\Include\SoftISP.h"

// DO NOT INCLUDE HEADER FILE!
class CRelativeIllumination_Common;

class ACMISSHADING_API CACMISShadingRelativeIllumination_Common
{
public:
	CACMISShadingRelativeIllumination_Common(void);
	~CACMISShadingRelativeIllumination_Common(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TRelativeIlluminationSpec_Common& _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TRelativeIlluminationSpec_Common& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TRelativeIlluminationSpec_Common& _Spec);

	const TRelativeIlluminationResult_Common* GetInspectionResult(EROIPosition pos, int nIndex);
	const TRelativeIlluminationResult_Common* GetInspectionCenterResult();
	bool Inspec(EROIPosition ePosition, int nIndex);
	int GetInspectROISize();

	const char* GetLogHeader(void);
	const char* GetLogData(void);
	const char* GetVersion(void);

private:
	CRelativeIllumination_Common* m_pMethod;
};

