#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"
//#include "LibACMISCommon\ACMISCommon.h"

// DO NOT INCLUDE HEADER FILE!
class CRelativeUniformity;

class ACMISSHADING_API CACMISShadingRelativeUniformity
{
public:
	CACMISShadingRelativeUniformity(void);
	~CACMISShadingRelativeUniformity(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TRelativeUniformitySpec &_Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TRelativeUniformitySpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TRelativeUniformitySpec& _Spec);

	const TRelativeUniformityResult* GetInspectionResult(void);

	const char* GetLogHeader(void);
	const char* GetLogData(void);
	const char* GetVersion(void);

	bool InSpec(const TRelativeUniformitySpec& _Spec);

private:
	CRelativeUniformity* m_pMethod;
};
