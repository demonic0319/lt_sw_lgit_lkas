#pragma once

#include "export.h"
#include <LibACMISCommon\ACMISCommon.h>
#include "ACMISShadingColorShadingDef.h"

// DO NOT INCLUDE HEADER FILE!
class CColorReproduction;

class ACMISSHADING_API CACMISColorReproduction
{
public:
	CACMISColorReproduction(void);
	~CACMISColorReproduction(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TColorReproductionSpec& _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TColorReproductionSpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TColorReproductionSpec& _Spec);

	int GetInspectionRegionCount();

	const TColorReproductionResult* GetInspectionResult(int nIndex);

	const char* GetLogHeader(void);

	const char* GetLogData(void);

	const char* GetVersion(void);

	bool InSpec(const TColorReproductionSpec& _Spec);

	const RECT* GetInspectionROI(int nIndex);

private:
	CColorReproduction* m_pMethod;
};
