#pragma once

#include <memory>
#include <XGraphic\xDataTypes.h>

typedef enum _EPos
{
	POS_CENTER,
	POS_LT,
	POS_RT,
	POS_LB,
	POS_RB
} EPos;

typedef enum _EROIPOSITION
{
	eHorizontal = 0,
	eVertical,
	eDiagonalA,
	eDiagonalB,
	eCenter
} EROIPosition;

typedef enum _ERICommonType
{
	RICommon_Coordinate,
	RICommon_Block_Uniform,
	RICommon_Block_NonUniform
}ERICommonType;

struct TColorRatio
{
	double RG;
	double BG;
	double GrGb;
	double R;
	double G;
	double B;
	TColorRatio(double _RG = 0, double _BG = 0, double _GrGb = 0, double _R = 0, double _G = 0, double _B = 0)
		: RG(_RG), BG(_BG), GrGb(_GrGb), R(_R),  G(_G),  B(_B) {}
};

typedef struct _TColorShadingSpec
{
	double dSpecROISizeRatio;

	double dSpecDevMaxRG;
	double dSpecDevMinRG;
	double dSpecDevMaxBG;
	double dSpecDevMinBG;
	double dSpecDevMaxGrGb;
	double dSpecDevMinGrGb;

	double dSpecOffsetMaxRG;
	double dSpecOffsetMinRG;
	double dSpecOffsetMaxBG;
	double dSpecOffsetMinBG;
	double dSpecOffsetMaxGrGb;
	double dSpecOffsetMinGrGb;

	double dSpecDiffRG;
	double dSpecDiffBG;
	double dSpecDiffGrGb;
} TColorShadingSpec;

typedef struct _TColorShadingResult
{
	double dDevMaxRG;
	double dDevMinRG;
	double dDevMaxBG;
	double dDevMinBG;
	double dDevMaxGrGb;
	double dDevMinGrGb;

	double dOffsetRG;
	double dOffsetBG;
	double dOffsetGrGb;

	double dDiffRG;
	double dDiffBG;
	double dDiffGrGb;
} TColorShadingResult;

typedef struct _TRelativeIlluminationSpec
{
	double dBoxField;
	int	nBoxSize;
	double dSpecRIcornerMin;
	double dSpecRIcornerMax;
	double dSpecRIminMin;
	double dSpecRIminMax;
} TRelativeIlluminationSpec;

typedef struct _TRelativeIlluminationResult
{
	double dRIcornerRAW[4];
	double dRIcenterRAW;
	double dRIcorner;
	double dRImin;
} TRelativeIlluminationResult;

typedef struct _TRelativeUniformitySpec
{
	int	nROISize;
	int nUseOverlap;
	double dSpecMax;
} TRelativeUniformitySpec;

typedef struct _TRelativeUniformityResult
{
	double dResultMax;
	int nMaxBlockX;
	int nMaxBlockY;
	int nRefBlockX; // 기준점
	int nRefBlockY; // 기준점
} TRelativeUniformityResult;

typedef struct _TShadingUniformitySpec
{
	int nROIWidth;
	int nROIHeight;
	int nMaxROICount;
	int nNormalizeIndex;
	POINT *ptHorROI;
	POINT *ptVerROI;
	POINT *ptDiaAROI;
	POINT *ptDiaBROI;
	double *dHorThreshold;
	double *dVerThreshold;
	double *dDiaThreshold;	
	double dHorThdMin;
	double dHorThdMax;
	double dVerThdMin;
	double dVerThdMax;
	double dDiaThdMin;
	double dDiaThdMax;
} TShadingUniformitySpec;

typedef struct _TShadingUniformityResult
{
	RECT rtROI;
	int nROIPosition;
	int nIndex;
	double dResult;
	double dNormalResult;
} TShadingUniformityResult;

typedef struct _TRICommon_Coordinate
{
	POINT *ptHorROI;
	POINT *ptVerROI;
	POINT *ptDiaAROI;
	POINT *ptDiaBROI;
}TRICommon_Coordinate;

typedef struct _TRICommon_Block
{
	int *ptHorROI;
	int *ptVerROI;
	int *ptDiaAROI;
	int *ptDiaBROI;
}TRICommon_Block;

typedef struct _TRelativeIlluminationSpec_Common
{
	int ntype;
	int nBlockWidth;
	int nBlockHeight;
	int nMaxROICount;
	int nNormalizeIndex;
	int *ptHorROI;
	int *ptVerROI;
	int *ptDiaAROI;
	int *ptDiaBROI;
	double *dHorThreshold;
	double *dVerThreshold;
	double *dDiaThreshold;
	double dHorThdMin;
	double dHorThdMax;
	double dVerThdMin;
	double dVerThdMax;
	double dDiaThdMin;
	double dDiaThdMax;
} TRelativeIlluminationSpec_Common;

typedef struct _TRelativeIlluminationResult_Common
{
	RECT rtROI;
	int nROIPosition;
	int nIndex;
	double dResult;
	double dNormalResult;
} TRelativeIlluminationResult_Common;

typedef struct _TColorReproduction
{
	double L;
	double a;
	double b;
} TColorReproduction;

typedef struct _TColorReproductionResult
{
	int nIndex;
	double dDelta;
	TColorReproduction tColorLab;
	RECT rtROI;
} TColorReproductionResult;

typedef struct _TColorReproductionSpec
{
	int nROIWidth;
	int nROIHeight;
	int nMaxROICount;
	POINT *ptROICenter;
	TColorReproduction *ptRefColorLab;
	double *pdThresholdDelta;
} TColorReproductionSpec;

typedef struct _TIRFilterSpec
{
	double dMinThreshold;
	double dMaxThreshold;
	double dROIVarThreshold;
	RECT rtROI;
} TIRFilterSpec;

typedef struct _TIRFilterResult
{
	double dFullBrightAvg;
	double dROIBrightAvg;
	double dROIBrightVar;
} TIRFilterResult;

typedef struct _TLowLightSpec
{
	int nThreshold_ParticleCount;
	double dThreshold_Center;
	double dThreshold_Middle;
	double dThreshold_Full;
	int nFullRectWidth;
	int nFullRectHeight;
	bool bEnableEllipseMiddle;
	bool bEnableEllipseCenter;
} TLowLightSpec;

typedef struct _TLowLightResult
{
	int nParticleCnt;
	POINT *ptParticlePnt;
	RECT *rtParticleRect;
	RECT rtROIRect[3];
} TLowLightResult;
