#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"
#include "ACMISShadingDevDef.h"
#include "LibACMISSoftISP\Include\SoftISP.h"

enum
{
	VIGNETTING_Y_MEAN_RATIO = 0,
	VIGNETTING_DSTRIPE,
	VIGNETTING_ALL
};

// DO NOT INCLUDE HEADER FILE!
class CVignetting;

class ACMISSHADING_API CACMISShadingVignetting
{
public:
	CACMISShadingVignetting(void);
	~CACMISShadingVignetting(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TVignettingSpec _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TVignettingSpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TVignettingSpec& _Spec);

	RECT* GetInspectionROI(EPos eROIPos, int nVignettingMode = VIGNETTING_Y_MEAN_RATIO);

	double GetInspectionResult(EPos ePos, int nVignettingMode = VIGNETTING_Y_MEAN_RATIO);

	bool InSpec(EPos ePos, double dVignettingSpec, int nVignettingMode = VIGNETTING_Y_MEAN_RATIO);

	/// @brief Get the header data of CSV log file.
	/// @return log file header string
	const char* GetLogHeader();

	/// @brief Get the data of CSV log file.
	/// @return log file data string
	const char* GetLogData();

private:
	CVignetting* m_pMethod;
};

