#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"

// DO NOT INCLUDE HEADER FILE!
class CShadingUniformity;

class ACMISSHADING_API CACMISShadingUniformity
{
public:
	CACMISShadingUniformity(void);
	~CACMISShadingUniformity(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TShadingUniformitySpec &_Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TShadingUniformitySpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TShadingUniformitySpec& _Spec);

	const TShadingUniformityResult* GetInspectionResult(EROIPosition pos, int nIndex);
	const TShadingUniformityResult* GetInspectionCenterResult();
	bool Inspec(EROIPosition ePosition, int nIndex);

	const char* GetLogHeader(void);
	const char* GetLogData(void);
	const char* GetVersion(void);

private:
	CShadingUniformity* m_pMethod;
};
