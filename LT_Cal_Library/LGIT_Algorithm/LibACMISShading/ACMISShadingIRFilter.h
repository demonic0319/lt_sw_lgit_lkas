#pragma once

#include "export.h"
#include "ACMISShadingColorShadingDef.h"
#include <iostream>

// DO NOT INCLUDE HEADER FILE!
class CIRFilter;

class ACMISSHADING_API CACMISShadingIRFilter
{
public:
	CACMISShadingIRFilter(void);
	~CACMISShadingIRFilter(void);

	bool Inspect(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TIRFilterSpec& _Spec, EDATAFORMAT nDataFormat, EOUTMODE nOutMode, ESENSORTYPE nSensorType, int nBlackLevel, bool bUsing8BitOnly = false);
	bool Inspect(TBufferInfo& tBufferInfo, TIRFilterSpec& _Spec);
	bool Inspect(TFileInfo& tFileInfo, TIRFilterSpec& _Spec);

	const TIRFilterResult* GetInspectionResult(void);

	const char* GetLogHeader(void);

	const char* GetLogData(void);

	const char* GetVersion(void);

	bool CACMISShadingIRFilter::InSpec(const TIRFilterSpec& _Spec);

	const RECT* GetInspectionROI(void);

private:
	CIRFilter* m_pMethod;
};