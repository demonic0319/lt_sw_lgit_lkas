#pragma once

#include "export.h"
#include "ACMISResolutionDef.h"


class ACMISRESOLUTION_API CACMISResolutionSFR
{
	CACMISResolutionSFR(const CACMISResolutionSFR& c);
	CACMISResolutionSFR& operator=(const CACMISResolutionSFR& c);

public:
	typedef struct _TSFRConfig
	{
		int nMaxROIWidth;
		int nMaxROIHeight;
		double dMaxEdgeAngle;
		double dPixelSize;
		ESFRAlgorithmType eAlgorithmType;
		ESFRAlgorithmMethod eAlgorithmMethod;
		ESFRFrequencyUnit eFrequencyUnit;
	} TSFRConfig;

	CACMISResolutionSFR(void);
	~CACMISResolutionSFR(void);

	///@brief This function sets imageBuffer & imageInfo for inspection.
	///       it must be called before inspection.
	bool SetImageBuffer(ResolutionImageBufferManager& mgr);

	///@brief This function set the config for calculating SFR
	void SetAlgorithmConfig(TSFRConfig _Config, bool bEnableLog = false);
	void SetAlgorithmConfig(int nMaxROIWidth, int nMaxROIHeight, double dMaxEdgeAngle = 45.0, double dPixelSize = 0.0, ESFRAlgorithmType eType = ESFRAlgorithm_ISO12233, ESFRAlgorithmMethod eMethod = ESFRMethod_Freq2SFR, ESFRFrequencyUnit eUnit = ESFRFreq_CyclePerPixel, bool bEnableLog = false);

	///@brief This function calculates SFR.
	///@param [in] rtROI Calculating area
	///@param [in] nEdgeDir An Edge direction. if an edge is vertical, this value is 0. if it's vertical edge, this value is 1.
	///@param [in] pFrequency Nyquist frequency lists which you want to get. ex) N/4=0.125, N/8=0.0625
	///@param [in] nFrequencyNum Frequency lists number
	///@param [out] pdSFRResult The calculated SFR result buffer. It must be inputed allocated memory buffer as nFrequencyNum.
	///@return success or fail
	bool CalcSFR(RECT& rtROI, int nEdgeDir, double* pFrequencyLists, int nFrequencyNum, double* pdSFRResult);

	///@brief This function calculates Spatial Frequency.
	///@param [in] rtROI Calculating area
	///@param [in] dSFR SFR value which you want to get.
	///@param [out] pdFreqResult The calculated Spatial frequency result buffer.
	///@return success or fail	
	bool CalcFreq(RECT& rtROI, int nEdgeDir, double dSFR, double* pdFreqResult);

	/// @brief Get the header data of CSV log file.
	/// @return log file header string
	const char* GetLogHeader();

	/// @brief Get the data of CSV log file.
	/// @return log file data string
	const char* GetLogData();

	/// @brief Get the version of Resolution algorithm.
	/// @return version data string
	const char* GetVersion();

	/// Chart Process
	bool SetChartSpec(TChartSpec* p,  int nImageWidth, int nImageHeight);
	bool SetFiducialMarkSpec(TFiducialMarkInfo* p, RECT *ptRect, int nImageWidth, int nImageHeight);

	bool ScanMainFiducialMark();
	bool ScanSubFiducialMark();

	int GetMaxMainFiducialMarkCount() const;
	const CxDRect& GetMainFiducialMarkROI(int nIndex) const;
	const CxDPoint& GetMainFiducialMarkPoint(int nIndex) const;
	const CxDRect& GetMainFiducialMarkRadius(int nIndex) const;
	const TFindROIResult& GetSFRAutoROI(int nIndex) const;
	int GetSFRAutoROICount() const;

	int GetMaxSubFiducialMarkCount() const;
	const CxDRect& GetSubFiducialMarkROI(int nIndex) const;
	const CxDPoint& GetSubFiducialMarkPoint(int nIndex) const;
	const CxDRect& GetSubFiducialMarkRadius(int nIndex) const;

	const CxDPoint& GetRefChartLength() const;

	bool CalcDFOV(int nImageWidth, int nImageHeight);
	bool CalcTiltAndRotation(int nImageWidth, int nImageHeight);
	bool CalcDistortion();

	double GetDFOV() const;
	double GetHFOV() const;
	double GetVFOV() const;
	double GetRotation() const;
	double GetDistortion() const;

	const CxDPoint& GetTilt() const;
	const CxDPoint& GetRealCenter() const; // it's NOT an Image Center. Chart center
	const CxDPoint& GetCmPerPixel() const;
	
	void InitResolution();
	bool CalcResolution();
	int GetMaxResolutionCount() const;

	double GetFinalSFR(int nFieldNo, int nFrequencyIndex = 0);
	EResolutionClass GetResolutionClass() const;
	const TSFRROIResult* GetSFRROIResult(int nFieldNo) const;
	const double GetMaxDeltaSFR() const;

	/// Check Spec
	bool InSpecDFOV(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecTiltX(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecTiltY(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecRotation(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecDistortion(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecResolution(int nFieldNo, int nFrequencyIndex = 0, double dValue = UNDEFINED_RESOLUTION_VALUE);

	const DRANGE& GetSpecDFOV() const;
	const DRANGE& GetSpecTiltX() const;
	const DRANGE& GetSpecTiltY() const;
	const DRANGE& GetSpecRotation() const;
	const DRANGE& GetSpecDistortion() const;
	double GetSpecResolution(int nFieldNo, int nFrequencyIndex = 0) const;
	const TSFRROIInfo& GetSFRROIInfo(int nFieldNo) const;

	/// forced Setting data
	void SetFiducialMarkPoint(CxDPoint* ptData, int nMaxIndex);
	void SetDFOV(double dDFOV);
	void SetTilt(double dTiltX, double dTiltY);
	void SetRotation(double dRotation);
	void SetResolutionAll(double* pSFR, int nMaxFieldNum, EResolutionClass eClassResult);

private:
	struct ResolutionImpl;
	std::unique_ptr<ResolutionImpl> m_pImpl;
};


