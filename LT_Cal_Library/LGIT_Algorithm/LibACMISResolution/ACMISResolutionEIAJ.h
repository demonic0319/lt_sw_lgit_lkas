#pragma once

#include "export.h"
#include "ACMISResolutionDef.h"


class ACMISRESOLUTION_API CACMISResolutionEIAJ
{
	CACMISResolutionEIAJ(const CACMISResolutionEIAJ&);
	CACMISResolutionEIAJ& operator=(const CACMISResolutionEIAJ&);

public:
	CACMISResolutionEIAJ(void);
	~CACMISResolutionEIAJ(void);

	///@brief This function sets imageBuffer & imageInfo for inspection.
	///       it must be called before inspection.
	bool SetImageBuffer(ResolutionImageBufferManager& mgr);

	///@brief This function set the config for calculating EIAJ MTF
	///@param [in] eType The Calculating method of EIAJ MTF. [EEIAJAlgorithm_GETLINEMTF, ESFRAlgorithm_GETSOMTHING]
	///@param [in] bEnableLog Enable/Disable the Log for debugging. [true, false]
	void SetAlgorithmConfig(EEIAJAlgorithmType eType, bool bEnableLog = false);

	///@brief This function calculates EIAJ MTF.
	///@param [in] rtROI Calculating area
	///@param [in] nRoiDirection An ROI direction. [ROI_LEFT, ROI_RIGHT, ROI_TOP, ROI_BOTTOM]
	///@param [in] tROISpec An ROI spec(MTF spec, Sharpness spec, etc).
	///@param [out] pdEIAJResult The calculated EIAJ result buffer.
	///@return success or fail
	bool CalcEIAJ(RECT& rtROI, int nRoiDirection, TEIAJROISpec& tROISpec, double* pdEIAJResult);

	/// @brief Get the header data of CSV log file.
	/// @return log file header string
	const char* GetLogHeader();

	/// @brief Get the data of CSV log file.
	/// @return log file data string
	const char* GetLogData();

	/// @brief Get the version of Resolution algorithm.
	/// @return version data string
	const char* GetVersion();

	/// Chart Process
	bool SetChartSpec(TChartSpec* p, int nImageWidth, int nImageHeight);
	bool SetFiducialMarkSpec(TFiducialMarkInfo* p, RECT* ptRect, int nImageWidth, int nImageHeight);

	bool ScanMainFiducialMark();
	bool ScanSubFiducialMark();

	int GetMaxMainFiducialMarkCount() const;
	const CxDRect& GetMainFiducialMarkROI(int nIndex) const;
	const CxDPoint& GetMainFiducialMarkPoint(int nIndex) const;
	const CxDRect& GetMainFiducialMarkRadius(int nIndex) const;

	int GetMaxSubFiducialMarkCount() const;
	const CxDRect& GetSubFiducialMarkROI(int nIndex) const;
	const CxDPoint& GetSubFiducialMarkPoint(int nIndex) const;
	const CxDRect& GetSubFiducialMarkRadius(int nIndex) const;

	bool CalcDFOV(int nImageWidth, int nImageHeight);
	bool CalcTiltAndRotation(int nImageWidth, int nImageHeight);
	bool CalcDistortion();

	double GetDFOV() const;
	double GetHFOV() const;
	double GetVFOV() const;
	double GetRotation() const;
	double GetDistortion() const;
	const CxDPoint& GetTilt() const;
	const CxDPoint& GetRealCenter() const; // it's NOT an Image Center.
	const CxDPoint& GetCmPerPixel() const;

	void InitResolution();
	bool CalcResolution();
	int GetMaxResolutionCount() const;

	int GetFinalResolution(int nFieldNo, int nFrequencyIndex = 0);
	EResolutionClass GetResolutionClass() const;
	const TEIAJROIResult* GetEIAJROIResult(int nFieldNo) const;

	/// Check Spec
	bool InSpecDFOV(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecTiltX(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecTiltY(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecRotation(double dValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecResolution(int nFieldNo, int nFrequencyIndex = 0, int nValue = UNDEFINED_RESOLUTION_VALUE);
	bool InSpecDistortion(double dValue = UNDEFINED_RESOLUTION_VALUE);

	const DRANGE& GetSpecDFOV() const;
	const DRANGE& GetSpecTiltX() const;
	const DRANGE& GetSpecTiltY() const;
	const DRANGE& GetSpecRotation() const;
	const DRANGE& GetSpecDistortion() const;
	double GetSpecResolution(int nFieldNo, int nFrequencyIndex = 0) const;
	const TEIAJROIInfo& GetEIAJROIInfo(int nFieldNo) const;
	double GetFinalMTF(int nFieldNo) const;

	/// forced Setting data
	void SetFiducialMarkPoint(CxDPoint* ptData, int nMaxIndex);
	void SetDFOV(double dDFOV);
	void SetTilt(double dTiltX, double dTiltY);
	void SetRotation(double dRotation);
	void SetResolutionAll(int* pEIAJ, int nMaxFieldNum, EResolutionClass eClassResult);
	void SetThresholdAxisAll(int* pEIAJ, int nMaxFieldNum);

private:
	struct ResolutionImpl;
	std::unique_ptr<ResolutionImpl> m_pImpl;
};


