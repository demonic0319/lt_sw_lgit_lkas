//*****************************************************************************
// Filename	: 	LG_LGIT_Algoriithm.cpp
// Created	:	2017/10/16 - 14:50
// Modified	:	2017/10/16 - 14:50
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "LGIT_Algorithm.h"


#ifdef _WIN32
#ifdef _WIN64	// 64bit
#ifdef _DEBUG
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISImage.lib")
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISResolution.lib")
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISShading.lib")
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISSoftISP.lib")
#else
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISImage.lib")
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISResolution.lib")
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISShading.lib")
#pragma comment (lib,"../LT_Cal_Library/LGIT_Algorithm/Lib/x64/LibACMISSoftISP.lib")
#endif
#else			// 32bit

#endif
#endif


CLG_LGIT_Algorithm::CLG_LGIT_Algorithm()
{
	
}

CLG_LGIT_Algorithm::~CLG_LGIT_Algorithm()
{
}

//=============================================================================
// Method		: LGIT_SFR_Func
// Access		: public  
// Returns		: bool
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in RECT rtROI
// Parameter	: __in int iEdgeDir
// Parameter	: __in double dbFrequencyLists
// Parameter	: __in double dbSFR
// Parameter	: __in int iMaxFrequency
// Parameter	: __out double dbResult
// Qualifier	:
// Last Update	: 2018/8/7 - 0:54
// Desc.		:
//=============================================================================
bool CLG_LGIT_Algorithm::LGIT_SFR_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in RECT rtROI, __in int iEdgeDir, __in double dbFrequencyLists, __in double dbSFR, __in int iMaxFrequency, __out double& dbResult)
{
	bool bReturn = false;

	shared_ptr<CACMISResolutionSFR> m_pChartProc = make_shared<CACMISResolutionSFR>();
	
	// ROI regions
	RECT vRectROI;

	// SFR results
	vector<double> vSFRResult(iMaxFrequency);

	shared_ptr<ResolutionImageBufferManager> mgr = make_shared<ResolutionImageBufferManager>((const VOIDTYPE*)pImageRaw, iImageW, iImageH, iImageW);
	(*mgr.get()).SetDataSpec(m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);
	m_pChartProc->SetImageBuffer(*mgr.get());

	//m_pChartProc->SetAlgorithmConfig(m_iMaxROIWidth, m_iMaxROIHeight, m_dbMaxEdgeAngle, m_dbPixelSize, m_eType, m_eMethod, false); // option 설정 ESFRAlgorithm_ISO12233 고정 사용
	m_pChartProc->SetAlgorithmConfig(m_iMaxROIWidth, m_iMaxROIHeight, m_dbMaxEdgeAngle, m_dbPixelSize, m_eType, m_eMethod, m_eConverse, false); // option 설정 ESFRAlgorithm_ISO12233 고정 사용


	int iWidth		= iEdgeDir == 0 ? m_iMaxROIWidth  : m_iMaxROIHeight;
	int iHeight		= iEdgeDir == 0 ? m_iMaxROIHeight : m_iMaxROIWidth;

	vRectROI.left	= rtROI.left;
	vRectROI.top	= rtROI.top;
	vRectROI.right	= rtROI.right;
	vRectROI.bottom = rtROI.bottom;

	//데이터 받아오는 함수
	switch (m_eMethod)
	{
	case ESFRMethod_Freq2SFR:
	{
								// Calculate SFR, 추후 변경 할 수 도 있음
								//!SH _180810: 값 맞는지 확인 필요
		double FrequencyTemp[1] ={ dbFrequencyLists };//{ dbFrequencyLists * m_dbPixelSize / 1000 }; //"cycle/pixel" = "lp/mm" x "Pixel Size" / 1000

		bReturn = m_pChartProc->CalcSFR(vRectROI, iEdgeDir, FrequencyTemp, iMaxFrequency, vSFRResult.data());

		if (bReturn == true)
		{
			dbResult = vSFRResult[0];
		}
		else
		{
			dbResult = vSFRResult[0];
			//printf("SFR_ERROR_CODE : %d \n", vSFRResult.data()); //!SH _180806: 이런 느낌의 경고문 출력
			//RC_FAIL 같은거 설정해주어야 함
		}
	}
		break;
	case ESFRMethod_SFR2Freq:
	{
		bReturn = m_pChartProc->CalcFreq(vRectROI, iEdgeDir, dbSFR, vSFRResult.data());
		if (bReturn == true)
		{
			dbResult = vSFRResult[0];
		}
		else
		{
			dbResult = vSFRResult[0];
			//printf("SFR_ERROR_CODE : %d \n", vSFRResult.data()); //!SH _180806: 이런 느낌의 경고문 출력
			//RC_FAIL 같은거 설정해주어야 함
		}
	}
		break;
	default:
		break;
	}
	
	return bReturn;
}

int CLG_LGIT_Algorithm::LGIT_Rotation_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in RECT *rtROI, __out double &dResultRotation)
{
	std::shared_ptr<CACMISResolutionSFR> m_pChartProc = std::make_shared<CACMISResolutionSFR>();

	if (m_pChartProc->SetFiducialMarkSpec(&m_stFiducialMarkInfo, rtROI, iImageW, iImageH) == false)
	{
		//printf("[Resolution][WARNING] SetFiducialMarkSpec");
		return -1;
	}

	m_pChartProc->CalcDistortion();
	//m_pChartProc->CalcDFOV(nWidth, nHeight);
	//m_pChartProc->CalcTiltAndRotation(iImageW, iImageH);
	dResultRotation = m_pChartProc->GetDistortion();

	//printf("[SFR]DFOV %.3f(%.3f, %.3f)\n", m_pChartProc->GetDFOV(), m_pChartProc->GetHFOV(), m_pChartProc->GetVFOV());
	//printf("[SFR]Distortion %.3f\n", m_pChartProc->GetDistortion());

	//const CxDPoint& ptTilt = m_pChartProc->GetTilt();
	//printf("[SFR]TILT %.3f,%.3f\n", ptTilt.x, ptTilt.y);
	//printf("[SFR]ROT %.3f\n", m_pChartProc->GetRotation());

	return 0;
}

bool CLG_LGIT_Algorithm::LGIT_Shading_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out double& dbCenterResult, 
	__out CPoint* resultHorizonROI, __out CPoint* resultVerticalROI, __out CPoint* resultDiaAROI, __out CPoint* resultDiaBROI,
	__out double* resultHorizonData, __out double* resultVerticalData, __out double* resultDiaAData, __out double* resultDiaBData,
	__out bool* Horresult, __out bool* Verresult, __out bool* DiaAresult, __out bool* DiaBresult)
{
	bool bReturn = false;

	CACMISShadingUniformity *m_pShadingUniformity = new CACMISShadingUniformity();
	//Inspection
	m_pShadingUniformity->Inspect(pImageRaw, iImageW, iImageH, m_ShadingUniformitySpec, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);

	const TShadingUniformityResult *pCenterResult = m_pShadingUniformity->GetInspectionCenterResult();
	dbCenterResult = pCenterResult->dResult;

	for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < (m_ShadingUniformitySpec.nMaxROICount); i++)
		{
			const TShadingUniformityResult* pResult  = m_pShadingUniformity->GetInspectionResult((EROIPosition)j, i);
			//!SH _180812: 예외 처리 필요 (결과가 0일경우 안돌게) 
			if (pResult == NULL )//|| pResult->rtROI == NULL)
			{
				continue;
			}
			if (j == 0)
			{
				resultHorizonROI[i].x = pResult->rtROI.left + m_ShadingUniformitySpec.nROIWidth / 2;
				resultHorizonROI[i].y = pResult->rtROI.top + m_ShadingUniformitySpec.nROIHeight / 2;
				resultHorizonData[i] = pResult->dNormalResult;
				//resultHorizonData[i] = pResult->dResult;
				//Horresult[i] = ret;
			}
			else if (j == 1)
			{
				resultVerticalROI[i].x = pResult->rtROI.left + m_ShadingUniformitySpec.nROIWidth / 2;
				resultVerticalROI[i].y = pResult->rtROI.top + m_ShadingUniformitySpec.nROIHeight / 2;
				resultVerticalData[i] = pResult->dNormalResult;
				//resultHorizonData[i] = pResult->dResult;
				//Verresult[i] = ret;
			}
			else if (j == 2)
			{
				resultDiaAROI[i].x = pResult->rtROI.left + m_ShadingUniformitySpec.nROIWidth / 2;
				resultDiaAROI[i].y = pResult->rtROI.top + m_ShadingUniformitySpec.nROIHeight / 2;
				resultDiaAData[i] = pResult->dNormalResult;
				//resultHorizonData[i] = pResult->dResult;
				//Verresult[i] = ret;
			}
			else if (j == 3)
			{
				resultDiaBROI[i].x = pResult->rtROI.left + m_ShadingUniformitySpec.nROIWidth / 2;
				resultDiaBROI[i].y = pResult->rtROI.top + m_ShadingUniformitySpec.nROIHeight / 2;
				resultDiaBData[i] = pResult->dNormalResult;
				//resultHorizonData[i] = pResult->dResult;
				//Verresult[i] = ret;
			}
			//bool ret = m_pShadingUniformity->Inspec((EROIPosition)j, i);
		}
	}

	delete m_pShadingUniformity;
	return bReturn;
}



bool CLG_LGIT_Algorithm::LGIT_RelativeIllumination_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* rtResultROI, __out double& dResultRICorner, __out double& dResultRIMin, __out double* dResultData, __in double dbCorneroffset, __in double dbMinoffset)
{
	bool bReturn = false;

	//CACMISShadingRelativeIllumination *m_pRelativeIllumination = new CACMISShadingRelativeIllumination();
	//
	////Inspection 
	//m_pRelativeIllumination->Inspect(pImageRaw, iImageW, iImageH, m_stRelativeIlluminationSpec, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel, m_bUsing8Bit);

	//TRelativeIlluminationResult m_stRelativeIlluminationResult;
	//m_stRelativeIlluminationResult = m_pRelativeIllumination->GetInspectionResult();
	//// Check RI corner
	////if ((_Spec.dSpecRIcornerMin > m_stRelativeIlluminationResult.dRIcorner) || (_Spec.dSpecRIcornerMax < m_stRelativeIlluminationResult.dRIcorner))
	////	nResult = R_RESULT_FAIL;
	////// Check min RI corner
	////if ((_Spec.dSpecRIminMin > m_stRelativeIlluminationResult.dRImin) || (_Spec.dSpecRIminMax < m_stRelativeIlluminationResult.dRImin))
	////	nResult = R_RESULT_FAIL;

	////const TShadingUniformityResult *pCenterResult;// = m_pShadingUniformity->GetShadingUniformityCenterResult();
	////dbCenterResult = pCenterResult->dResult;
	////Logging
	////std::cout << "[Relative Illumination] Center= " << m_stRelativeIlluminationResult.dRIcenterRAW << std::endl;
	////std::cout << "[Relative Illumination] UL=" << m_stRelativeIlluminationResult.dRIcornerRAW[0] << std::endl;
	////std::cout << "[Relative Illumination] UR=" << m_stRelativeIlluminationResult.dRIcornerRAW[1] << std::endl;
	////std::cout << "[Relative Illumination] LL=" << m_stRelativeIlluminationResult.dRIcornerRAW[2] << std::endl;
	////std::cout << "[Relative Illumination] LR=" << m_stRelativeIlluminationResult.dRIcornerRAW[3] << std::endl;
	////std::cout << "[Relative Illumination] RIcorner=" << m_stRelativeIlluminationResult.dRIcorner << std::endl;
	////std::cout << "[Relative Illumination] RImin=" << m_stRelativeIlluminationResult.dRImin << std::endl;
	//dResultRICorner = m_stRelativeIlluminationResult.dRIcorner + dbCorneroffset;
	//dResultRIMin = m_stRelativeIlluminationResult.dRImin + dbMinoffset;
	//dResultData[0] = m_stRelativeIlluminationResult.dRIcenterRAW;

	//for (int i = 0; i < 4; i++)
	//{
	//	dResultData[i + 1] = (m_stRelativeIlluminationResult.dRIcornerRAW[i] / m_stRelativeIlluminationResult.dRIcenterRAW) * 100;
	//}

	////Graphic
	//for (int i = 0; i < 5; i++)
	//{
	//	const RECT* rt = m_pRelativeIllumination->GetInspectionROI((EPos)i);


	//	rtResultROI[i].left = rt->left;
	//	rtResultROI[i].right = rt->right;
	//	rtResultROI[i].top = rt->top;
	//	rtResultROI[i].bottom = rt->bottom;
	//}

	//delete m_pRelativeIllumination;

	return bReturn;
}

bool CLG_LGIT_Algorithm::LGIT_DynamicBW_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CPoint* resultROI, __out double* resultVarData, __out double* resultAverData, __out double& resultSNRData, __out double& resultDRData)
{
	bool bReturn = false;

	std::shared_ptr<CACMISSignalNoiseRatioBW> pSNRBW = std::make_shared<CACMISSignalNoiseRatioBW>();
	const TSNRResult* pSNRResult = nullptr;

	pSNRBW->Inspect(pImageRaw, iImageW, iImageH, m_SNRBWSpec, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);


	for (int i = 0; i < pSNRBW->GetSNRRegionCount(); i++)
	{
		pSNRResult = pSNRBW->GetSNRResult(i);
		if (pSNRResult)
		{
			resultVarData[i] = pSNRResult->dVariance;
			resultAverData[i] = pSNRResult->dAverage;
			resultROI[i].SetPoint(pSNRResult->rtROI.left + m_SNRBWSpec.nROIWidth / 2, pSNRResult->rtROI.top + m_SNRBWSpec.nROIHeight / 2);
		}
	}

	pSNRResult = pSNRBW->GetMinSNRResult();
	if (pSNRResult)
	{
		resultSNRData = pSNRResult->dSNRResult;
		resultDRData = pSNRResult->dDRResult;
	}

	return bReturn;
}


int CLG_LGIT_Algorithm::LGIT_DefectBlack_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CPoint* resultROI, __out int& nDefectCount)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageBrightModeFixedCommon> pDarkDefect = std::make_shared<CACMISImageBrightModeFixedCommon>();

	// inspection
	if (pDarkDefect->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stDarkDefectPixel, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel))
	{
		const TDefectResult* pDResult = pDarkDefect->GetMaxDefectResult(EIMAGEREGION_CENTER);

		if (nDefectCount > 0)
		{
			nResult = R_FAIL_BLACK_SPOT;
		}

		//if (nResult)
		{

			for (int i = 0; i < pDarkDefect->GetDefectBlobCount(); i++)
			{
				const RECT* rt = pDarkDefect->GetDefectBlobRect(i);
				//resultROI[i].left = rt->left;
				//resultROI[i].right = rt->right;
				//resultROI[i].top = rt->top;
				//resultROI[i].bottom = rt->bottom;
				resultROI[i].x = rt->left;
				resultROI[i].y = rt->top;
			}

			//nDefectCount = pDarkDefect->GetDefectBlobCount();
		}

	}

	
	return nResult;
}

int CLG_LGIT_Algorithm::LGIT_DefectWhite_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CPoint* resultROI, __out int& nDefectCount)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageDarkModeFixedCommon> pBrightDefect = std::make_shared<CACMISImageDarkModeFixedCommon>();

	// inspection
	if (pBrightDefect->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stDarkDefectPixel, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel))
	{
		const TDefectResult* pDResult = pBrightDefect->GetMaxDefectResult(EIMAGEREGION_CENTER);

		if (nDefectCount > 0)
		{
			nResult = R_FAIL_BLACK_SPOT;
		}

		//if (nResult)
		{

			for (int i = 0; i < pBrightDefect->GetDefectBlobCount(); i++)
			{
				const RECT* rt = pBrightDefect->GetDefectBlobRect(i);
				//resultROI[i].left = rt->left;
				//resultROI[i].right = rt->right;
				//resultROI[i].top = rt->top;
				//resultROI[i].bottom = rt->bottom;
				resultROI[i].x = rt->left;
				resultROI[i].y = rt->top;
			}

			//nDefectCount = pBrightDefect->GetDefectBlobCount();
		}
	}
	

	return nResult;
}


int CLG_LGIT_Algorithm::LGIT_Ymean_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI, __out int& nDefectCount,
	__out int& nCenterCount, __out double& dCenterMaxValue, __out CPoint& pCenterMaxPoint,
	__out int& nEdgeCount, __out double& dEdgeMaxValue, __out CPoint& pEdgeMaxPoint,
	__out int& nCornerCount, __out double& dCornerMaxValue, __out CPoint& pCornerMaxPoint)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageStainRU_YmeanCommon> pInspectBlemish_Ymean = std::make_shared<CACMISImageStainRU_YmeanCommon>();

	// inspection
	if (pInspectBlemish_Ymean->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stSpecYmean, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel))
	{
		if (pInspectBlemish_Ymean->GetSingleDefectCount() > 0)
		{
			nResult = R_FAIL_BLACK_SPOT;
		}

		//if (nResult != R_RESULT_PASS)
		{
			// logging
			nCenterCount = pInspectBlemish_Ymean->GetDefectCount(EIMAGEREGION_CENTER);
			const TDefectResult* pCenterMaxResult = pInspectBlemish_Ymean->GetMaxDefectResult(EIMAGEREGION_CENTER);
			dCenterMaxValue = pCenterMaxResult->dValue;
			pCenterMaxPoint = pCenterMaxResult->ptPos;
			/*std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
			std::cout << "[RUYMean] Center=" << nCenterCount << std::endl;
			std::cout << "[RUYMean] maxValue=" << pCenterMaxResult->dValue << std::endl;
			std::cout << "[RUYMean] posX=" << pCenterMaxResult->ptPos.x << std::endl;
			std::cout << "[RUYMean] posY=" << pCenterMaxResult->ptPos.y << std::endl;*/

			nEdgeCount = pInspectBlemish_Ymean->GetDefectCount(EIMAGEREGION_EDGE);
			const TDefectResult* pEdgeMaxResult = pInspectBlemish_Ymean->GetMaxDefectResult(EIMAGEREGION_EDGE);
			dEdgeMaxValue = pEdgeMaxResult->dValue;
			pEdgeMaxPoint = pEdgeMaxResult->ptPos;
			//std::cout << "[RUYMean] Edge=" << nEdgeCount << std::endl;
			//std::cout << "[RUYMean] maxValue=" << pEdgeMaxResult->dValue << std::endl;
			//std::cout << "[RUYMean] posX=" << pEdgeMaxResult->ptPos.x << std::endl;
			//std::cout << "[RUYMean] posY=" << pEdgeMaxResult->ptPos.y << std::endl;

			nCornerCount = pInspectBlemish_Ymean->GetDefectCount(EIMAGEREGION_CORNER);
			const TDefectResult* pCornerMaxResult = pInspectBlemish_Ymean->GetMaxDefectResult(EIMAGEREGION_CORNER);
			dCornerMaxValue = pCornerMaxResult->dValue;
			pCornerMaxPoint = pCornerMaxResult->ptPos;
			//std::cout << "[RUYMean] Corner=" << nCornerCount << std::endl;
			//std::cout << "[RUYMean] maxValue=" << pCornerMaxResult->dValue << std::endl;
			//std::cout << "[RUYMean] posX=" << pCornerMaxResult->ptPos.x << std::endl;
			//std::cout << "[RUYMean] posY=" << pCornerMaxResult->ptPos.y << std::endl;

			for (int i = 0; i < pInspectBlemish_Ymean->GetDefectBlobCount(); i++)
			{
				const RECT* rt = pInspectBlemish_Ymean->GetDefectBlobRect(i);
				resultROI[i].left = rt->left;
				resultROI[i].right = rt->right;
				resultROI[i].top = rt->top;
				resultROI[i].bottom = rt->bottom;
			}

			nDefectCount = pInspectBlemish_Ymean->GetDefectBlobCount();
		}
//		else
// 		{
// 			//!SH _180809: 예외처리 필요
// 			//std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
// 		}
	}

	

	return nResult;
}

int CLG_LGIT_Algorithm::LGIT_BlackSpot_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI, __out int& nDefectCount)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageBlackSpotContrastCommon> pInspectBlemish_BlackSpot = std::make_shared<CACMISImageBlackSpotContrastCommon>();

	// inspection
	nDefectCount = pInspectBlemish_BlackSpot->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stSpecBlackSpot, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);

	if (pInspectBlemish_BlackSpot->GetSingleDefectCount() > m_stSpecBlackSpot.nMaxSingleDefectNum)
		nResult = R_FAIL_NOISE;
	else if (pInspectBlemish_BlackSpot->GetDefectBlobCount() > 0)
		nResult = R_FAIL_BLACK_SPOT;

	if (nResult)
	{
		for (int i = 0; i < pInspectBlemish_BlackSpot->GetDefectBlobCount(); i++)
		{
			const RECT* rt = pInspectBlemish_BlackSpot->GetDefectBlobRect(i);
			resultROI[i].left = rt->left;
			resultROI[i].right = rt->right;
			resultROI[i].top = rt->top;
			resultROI[i].bottom = rt->bottom;
		}

		nDefectCount = pInspectBlemish_BlackSpot->GetDefectBlobCount();
	}
	else
	{
		//!SH _180809: 예외처리 필요
		//std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
	}

	return nResult;
}

int CLG_LGIT_Algorithm::LGIT_LCB_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out CRect* resultROI, __out int& nDefectCount)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageStainLCBCommon> pInspectLCB = std::make_shared<CACMISImageStainLCBCommon>();

	// inspection
	nDefectCount = pInspectLCB->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stSpecLCB, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);

	if (pInspectLCB->GetSingleDefectCount() > 0)
	{
		nResult = R_FAIL_BLACK_SPOT;
	}

	if (nResult)
	{		
		for (int i = 0; i < pInspectLCB->GetDefectBlobCount(); i++)
		{
			const RECT* rt = pInspectLCB->GetDefectBlobRect(i);
			resultROI[i].left = rt->left;
			resultROI[i].right = rt->right;
			resultROI[i].top = rt->top;
			resultROI[i].bottom = rt->bottom;
		}

		nDefectCount = pInspectLCB->GetDefectBlobCount();
	}
	else
	{
		//!SH _180809: 예외처리 필요
		//std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
	}

	return nResult;
}



int CLG_LGIT_Algorithm::LGIT_Ymean_Wide_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out  CRect* resultROI, 
	__out int& nDefectCount, __out int& nCircleCount, __out double& pCircleMaxValue, __out CPoint& pCircleMaxPoint,
	__out int& ocx, __out int& ocy, __out int& radx, __out int& rady)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageStainRU_YmeanCommon> pInspectBlemish_Ymean = std::make_shared<CACMISImageStainRU_YmeanCommon>();

	// inspection
	nDefectCount = pInspectBlemish_Ymean->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stSpecYmean, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);

	if (pInspectBlemish_Ymean->GetSingleDefectCount() > 0)
	{
		nResult = R_FAIL_BLACK_SPOT;
	}

	//std::cout << "[RUYMean] Version: " << pInspectBlemish_Ymean->GetVersion() << std::endl;
	if (nResult)
	{
		nCircleCount = pInspectBlemish_Ymean->GetDefectCount(EIMAGEREGION_CIRCLE);
		const TDefectResult* pCircleMaxResult = pInspectBlemish_Ymean->GetMaxDefectResult(EIMAGEREGION_CIRCLE);

		/*std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
		std::cout << "[RUYMean] Circle=" << nCircleCount << std::endl;
		std::cout << "[RUYMean] maxValue=" << pCircleMaxResult->dValue << std::endl;
		std::cout << "[RUYMean] posX=" << pCircleMaxResult->ptPos.x << std::endl;
		std::cout << "[RUYMean] posY=" << pCircleMaxResult->ptPos.y << std::endl;*/

		//int ocx = 0, ocy = 0, radx = 0, rady = 0;
		pCircleMaxValue = pCircleMaxResult->dValue;
		pCircleMaxPoint = pCircleMaxResult->ptPos;

		ocx = (int)pCircleMaxResult->dContrastMaxR;
		ocy = (int)pCircleMaxResult->dContrastMaxGb;
		radx = (int)pCircleMaxResult->dContrastMaxGr;
		rady = (int)pCircleMaxResult->dContrastMaxB;

		//std::cout << "[RUYMean] ocx=" << ocx << "ocy = " << ocy << "radx = " << radx << "rady = " << rady << std::endl;
		//cvEllipse(cvImgYmean, cvPoint(ocx, ocy), cvSize(radx, rady), 0, 0, 360, CV_RGB(0, 255, 255));


		for (int i = 0; i < pInspectBlemish_Ymean->GetDefectBlobCount(); i++)
		{
			const RECT* rt = pInspectBlemish_Ymean->GetDefectBlobRect(i);
			resultROI[i].left = rt->left;
			resultROI[i].right = rt->right;
			resultROI[i].top = rt->top;
			resultROI[i].bottom = rt->bottom;
			//cvRectangle(cvImgYmean, cvPoint(rt->left, rt->top), cvPoint(rt->right, rt->bottom), CV_RGB(138, 43, 226));
		}

		nDefectCount = pInspectBlemish_Ymean->GetDefectBlobCount();
	}
	else
	{
		//!SH _180809: 예외처리 필요
		//std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
	}
	return nResult;
}



int CLG_LGIT_Algorithm::LGIT_BlackSpot_Wide_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out  CRect* resultROI,
	__out int& nDefectCount, __out int& ocx, __out int& ocy, __out int& radx, __out int& rady)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageBlackSpotContrastCommon> pInspectBlemish_BlackSpot = std::make_shared<CACMISImageBlackSpotContrastCommon>();

	// inspection
	nDefectCount = pInspectBlemish_BlackSpot->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stSpecBlackSpot, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);

	if (pInspectBlemish_BlackSpot->GetSingleDefectCount() > 0)
	{
		nResult = R_FAIL_BLACK_SPOT;
	}

	if (nResult)
	{
		const TDefectResult* pCircleMaxResult = pInspectBlemish_BlackSpot->GetMaxDefectResult(EIMAGEREGION_CIRCLE);

		ocx = (int)pCircleMaxResult->dContrastMaxR;
		ocy = (int)pCircleMaxResult->dContrastMaxGb;
		radx = (int)pCircleMaxResult->dContrastMaxGr;
		rady = (int)pCircleMaxResult->dContrastMaxB;

		for (int i = 0; i < pInspectBlemish_BlackSpot->GetDefectBlobCount(); i++)
		{
			const RECT* rt = pInspectBlemish_BlackSpot->GetDefectBlobRect(i);
			resultROI[i].left = rt->left;
			resultROI[i].right = rt->right;
			resultROI[i].top = rt->top;
			resultROI[i].bottom = rt->bottom;
		}

		nDefectCount = pInspectBlemish_BlackSpot->GetDefectBlobCount();
	}
	else
	{
		//!SH _180809: 예외처리 필요
		//std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
	}
	return nResult;
}


int CLG_LGIT_Algorithm::LGIT_LCB_Wide_Func(__in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __out  CRect* resultROI,
	__out int& nDefectCount, __out int& ocx, __out int& ocy, __out int& radx, __out int& rady)
{
	int nResult = R_RESULT_PASS;

	std::shared_ptr<CACMISImageStainLCBCommon> pInspectLCB = std::make_shared<CACMISImageStainLCBCommon>();

	// inspection
	nDefectCount = pInspectLCB->Inspect((BYTE*)pImageRaw, iImageW, iImageH, m_stSpecLCB, m_eDataFormat, m_eOutMode, m_eSensorType, m_iBlackLevel);

	if (pInspectLCB->GetSingleDefectCount() > 0)
	{
		nResult = R_FAIL_BLACK_SPOT;
	}

	if (nResult)
	{
		const TDefectResult* pCircleMaxResult = pInspectLCB->GetMaxDefectResult(EIMAGEREGION_CIRCLE);

		ocx = (int)pCircleMaxResult->dContrastMaxR;
		ocy = (int)pCircleMaxResult->dContrastMaxGb;
		radx = (int)pCircleMaxResult->dContrastMaxGr;
		rady = (int)pCircleMaxResult->dContrastMaxB;
		
		for (int i = 0; i < pInspectLCB->GetDefectBlobCount(); i++)
		{
			const RECT* rt = pInspectLCB->GetDefectBlobRect(i);
			resultROI[i].left = rt->left;
			resultROI[i].right = rt->right;
			resultROI[i].top = rt->top;
			resultROI[i].bottom = rt->bottom;
		}
		nDefectCount = pInspectLCB->GetDefectBlobCount();
	}
	else
	{
		//!SH _180809: 예외처리 필요
		//std::cout << "[RUYMean] SingleDefectCount=" << nDefectCount << std::endl;
	}
	return nResult;
}

int CLG_LGIT_Algorithm::LoadImageData(unsigned char* pDataBuffer, const char * _Filename) //LAW Image 불러오는 함수
{
	FILE* _File;
	CString strstate;
	errno_t err;      //file open check 변수

	err = fopen_s(&_File, _Filename, "rb"); // 성공시 0 실패시 다른 값

	if (err != 0)
	{
		TRACE(_T("File Open Fail.... [FileName:%s \r\n"), _Filename);
		return -1;
	}

	fseek(_File, 0, SEEK_END);
	int _lengthByte = ftell(_File);
	rewind(_File);

	fread((BYTE*)pDataBuffer, sizeof(BYTE), _lengthByte, _File);
	fclose(_File);

	return _lengthByte;
}


CvSize CLG_LGIT_Algorithm::LoadBMPImageData(unsigned char* pDataBuffer, const char* _Filename) //BMP Image 불러오는 함수
{
	IplImage*	srcImage;
	CvSize		image_size;

	srcImage = cvLoadImage(_Filename, -1);
	image_size = cvGetSize(srcImage);

	copy(srcImage->imageData, srcImage->imageData + sizeof(BYTE)* image_size.width * image_size.height * srcImage->nChannels, pDataBuffer);
	cvReleaseImage(&srcImage);

	return image_size;
}

CvSize CLG_LGIT_Algorithm::LoadPNGImageData(unsigned char* pDataBuffer, const char* _Filename) //PNG Image 불러오는 함수
{
	IplImage*	srcImage;
	CvSize		image_size;

	srcImage = cvLoadImage(_Filename, -1);
	image_size = cvGetSize(srcImage);

	if (srcImage->nChannels == 3)
		copy(srcImage->imageData, srcImage->imageData + sizeof(BYTE)* image_size.width * image_size.height * srcImage->nChannels, pDataBuffer);
	else if (srcImage->nChannels == 1)
	{
		int count = 0;

		for (int y = 0; y < image_size.height; y++)
		{
			for (int x = 0; x < image_size.width; x++)
			{
				pDataBuffer[count++] = srcImage->imageData[y * image_size.width + x];
				pDataBuffer[count++] = srcImage->imageData[y * image_size.width + x];
				pDataBuffer[count++] = srcImage->imageData[y * image_size.width + x];
			}
		}
	}
	cvReleaseImage(&srcImage);

	return image_size;
}


int CLG_LGIT_Algorithm::GetImageData(CM_TEST_MODEL eTestModel, TDATASPEC &tDataSpec, RawImgInfo &stImgInfo)
{
	tDataSpec.nBlackLevel = 64;
	
	if (eTestModel == MODEL_IKC)
	{
		stImgInfo.nDisplaySizeX = 1280;
		stImgInfo.nDisplaySizeY = 720;
		stImgInfo.nSensorWidth = stImgInfo.nDisplaySizeX;
		stImgInfo.nSensorHeight = 724; //stImgInfo.nDisplaySizeY;
		stImgInfo.nDisplayStartPosY = 2;
		stImgInfo.nDisplayStartPosX = 0;

		tDataSpec.eDataFormat = DATAFORMAT_BAYER_PARALLEL_12BIT;
		tDataSpec.eOutMode = OUTMODE_BAYER_BLACKWHITE;
		tDataSpec.eSensorType = SENSORTYPE_CCCC;
	}
	else if (eTestModel == MODEL_MRA2) //Model Setting 불분명ㅜ
	{
		stImgInfo.nDisplaySizeX = 1280;
		stImgInfo.nDisplaySizeY = 960;
		stImgInfo.nSensorWidth = stImgInfo.nDisplaySizeX;
		stImgInfo.nSensorHeight = 964;// stImgInfo.nDisplaySizeY;
		stImgInfo.nDisplayStartPosY = 2;
		stImgInfo.nDisplayStartPosX = 0;

		tDataSpec.eDataFormat = DATAFORMAT_BAYER_PARALLEL_12BIT;
		tDataSpec.eOutMode = OUTMODE_BAYER_BLACKWHITE;
		tDataSpec.eSensorType = SENSORTYPE_CCCC;
	}
	else if (eTestModel == MODEL_NIO)  //나중에 없애야함 LJE
	{
		stImgInfo.nDisplaySizeX = 1280;
		stImgInfo.nDisplaySizeY = 1080;
		stImgInfo.nSensorWidth = stImgInfo.nDisplaySizeX;
		stImgInfo.nSensorHeight = stImgInfo.nDisplaySizeY;
		stImgInfo.nDisplayStartPosY = 0;
		stImgInfo.nDisplayStartPosX = 0;

		tDataSpec.eDataFormat = DATAFORMAT_BAYER_12BIT;
		//tDataSpec.eOutMode = OUTMODE_BAYER_GBRG;
		tDataSpec.eOutMode = OUTMODE_BAYER_BGGR;
		tDataSpec.eSensorType = SENSORTYPE_RCCC;
	}
	else
	{
	
		TRACE(_T("Invalid CM Model\n"));
		return -1;
	}

	return 0;
}

int CLG_LGIT_Algorithm::GetImageOffset(TDATASPEC &tDataSpec, RawImgInfo &stImgInfo) //이미지에 대한 픽셀당 바이트 보정 (이미지 데이터 크기 보정)
{
	int offset = 0;

	if (stImgInfo.nDisplaySizeX != stImgInfo.nSensorWidth || stImgInfo.nDisplaySizeY != stImgInfo.nSensorHeight)
	{
		switch (tDataSpec.eDataFormat)
		{
		case DATAFORMAT_BAYER_8BIT:
			offset = stImgInfo.nSensorWidth * stImgInfo.nDisplayStartPosY + stImgInfo.nDisplayStartPosX; // need 1byte per 1pixel
			break;
		case DATAFORMAT_BAYER_10BIT:
			offset = (stImgInfo.nSensorWidth * stImgInfo.nDisplayStartPosY + stImgInfo.nDisplayStartPosX) * 5 / 4; // need 5bytes per 4pixels
			break;
		case DATAFORMAT_BAYER_12BIT:
			offset = (stImgInfo.nSensorWidth * stImgInfo.nDisplayStartPosY + stImgInfo.nDisplayStartPosX) * 3 / 2; // need 3bytes per 2pixels
			break;
		case DATAFORMAT_BAYER_PARALLEL_10BIT:
		case DATAFORMAT_BAYER_PARALLEL_12BIT:
			offset = (stImgInfo.nSensorWidth * stImgInfo.nDisplayStartPosY + stImgInfo.nDisplayStartPosX) * 2; // need 2bytes per 1pixel
			break;
		}
	}

	return offset;
}


//=============================================================================
// Method		: OnTracePrint
// Access		: protected  
// Returns		: void
// Parameter	: __in eLGCalibIntrMsg eRet
// Qualifier	:
// Last Update	: 2018/2/7 - 21:10
// Desc.		:
//=============================================================================
void CLG_LGIT_Algorithm::OnTraceSFRPrint(__in eSFRFailMsg eRet)
{
	switch (eRet)
	{
	case Edge_gradient_Fail:
		TRACE(_T("Result ErrName :101 - The slope of the edge is less than 0 or greater than 11.25.  \r\n"));
		break;

	case ROI_Width_Fail:
		TRACE(_T("Result ErrName : 103 - The width of the ROI is less than 10.\r\n"));
		break;

	case ROI_Height_Fail:
		TRACE(_T("Result ErrName : 104 - The height of the ROI is less than 10. \r\n"));
		break;

	case Spatial_frequency_Fail:
		TRACE(_T("Result ErrName : 105 - The spatial frequency to be obtained is less than 0 or greater than 1. \r\n"));
		break;

	case SFR_Result_Fail:
		TRACE(_T("Result ErrName : 106 - The measured SFR value exceeds 120%. \r\n"));
		break;

	case ROI_Range_Fail:
		TRACE(_T("Result ErrName : 108 - The range of the ROI is outside the size of the Image. \r\n"));
		break;

	case Parameter_err:
		TRACE(_T("Result ErrName : 109 - The parameter is wrong. \r\n"));
		break;

	case Edge_direction_Fail:
		TRACE(_T("Result ErrName : 110 - The direction of the edge is wrong. \r\n"));
		break;

	default:
		break;
	}
}


void CLG_LGIT_Algorithm::SpiltFileName(string &pathname)
{
	const size_t last_slash_idx = pathname.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		pathname.erase(0, last_slash_idx + 1);
	}
}


double* CLG_LGIT_Algorithm::CalulateSFR(TDATASPEC &tDataSpec, BYTE *pBuffer, int nWidth, int nHeight, int nMaxROINum, int nMaxFreqeuncyNum, int *nDirection, POINT *ptROICenter, int ROI_WIDTH, int ROI_HEIGHT, double *dFrequency, int *pTxtPosition, bool bRaw, double dPixelSize, double dMaxEdgeAngle, const char *ImagePath , bool bSaveResultImage)
{
#ifdef _DEBUG_
	bool bFileExist = true;
	ofstream fp;

	if (_access("SFR_RESULT.csv", 0))
		bFileExist = false;
	fp.open("SFR_RESULT.csv", std::ofstream::out | std::ofstream::app);
	if (bFileExist == false)
	{
		fp << "Image File" << ",";
		for (int i = 0; i < nMaxROINum; i++)
		{
			fp << "left" << i << ",";
			fp << "top" << i << ",";
			fp << "right" << i << ",";
			fp << "bottom" << i << ",";
			for (int j = 0; j < nMaxFreqeuncyNum; j++)
				fp << "SFR" << i << "-" << dFrequency[j] << ",";
		}
		fp << ",";
		fp << "Avg Center" << ",";
		fp << "Avg TL(T)" << ",";
		fp << "Avg TR(B)" << ",";
		fp << "Avg BL(L)" << ",";
		fp << "Avg BR(R)" << "\n";
	}

	// 소수점 3자리까지 출력
	fp << fixed;
	fp.precision(3);

	// Image file name
	fp << ImagePath << ",";
#endif

	shared_ptr<CACMISResolutionSFR> m_pChartProc = make_shared<CACMISResolutionSFR>();

	// ROI regions
	vector<RECT> vRectROI(nMaxROINum);
	// SFR results
	vector<double> vSFRResult(nMaxFreqeuncyNum);
	vector<double> vFinalResult(nMaxROINum);

	// Image buffers
	vector<BYTE> vBmpBuffer(nWidth * nHeight * 3, 0);

	if (bRaw == false) // BMP image일 때
	{
		// Image buffers
		vector<BYTE> vYImgBuffer(nWidth * nHeight, 0);

		// Obtain Y image
		ACMISSoftISP::xBMPtoY(vYImgBuffer.data(), pBuffer, nWidth, nHeight);

		// Set image buffer
		shared_ptr<ResolutionImageBufferManager> mgr =
			make_shared<ResolutionImageBufferManager>(vYImgBuffer.data(), nWidth, nHeight, nWidth);
		m_pChartProc->SetImageBuffer(*mgr.get());

		copy(pBuffer, pBuffer + sizeof(BYTE)* nWidth * nHeight * 3, vBmpBuffer.data());
	}
	else // Raw image일 때
	{
		// for display
		ACMISSoftISP::xMakeBMP(pBuffer, vBmpBuffer.data(), nWidth, nHeight, tDataSpec);

		// Set image buffer
		shared_ptr<ResolutionImageBufferManager> mgr =
			make_shared<ResolutionImageBufferManager>((const VOIDTYPE*)pBuffer, nWidth, nHeight, nWidth);
		(*mgr.get()).SetDataSpec(tDataSpec.eDataFormat, tDataSpec.eOutMode, tDataSpec.eSensorType, tDataSpec.nBlackLevel);
		m_pChartProc->SetImageBuffer(*mgr.get());
	}

	printf("SFR Algorithm Version: %s\n", m_pChartProc->GetVersion());
	//m_pChartProc->SetAlgorithmConfig(ROI_WIDTH, ROI_HEIGHT, dMaxEdgeAngle, dPixelSize, ESFRAlgorithm_ISO12233, ESFRMethod_Freq2SFR, false); //option 설정 ESFRAlgorithm_ISO12233 고정 사용

	// Set ROI regions
	for (int i = 0; i < nMaxROINum; i++)  //ROI 중심점 값을 받아 ROI 영역 설정
	{
		int width = nDirection[i] == 0 ? ROI_WIDTH : ROI_HEIGHT;
		int height = nDirection[i] == 0 ? ROI_HEIGHT : ROI_WIDTH;

		vRectROI[i].left = ptROICenter[i].x - width / 2;
		vRectROI[i].top = ptROICenter[i].y - height / 2;
		vRectROI[i].right = vRectROI[i].left + width - 1;
		vRectROI[i].bottom = vRectROI[i].top + height - 1;
#ifdef _DEBUG_
		fp << vRectROI[i].left << ",";
		fp << vRectROI[i].top << ",";
		fp << vRectROI[i].right << ",";
		fp << vRectROI[i].bottom << ",";
#endif

		// Calculate SFR
		int ret = m_pChartProc->CalcSFR(vRectROI[i], nDirection[i], dFrequency, nMaxFreqeuncyNum, vSFRResult.data());  //데이터 받아오는 함수
		vFinalResult[i] = vSFRResult[0];
		printf("%2d ROI(%4d %4d %4d %4d) Ret(%d) SFR(", i, vRectROI[i].left, vRectROI[i].top, vRectROI[i].right, vRectROI[i].bottom, ret);
		for (int j = 0; j < nMaxFreqeuncyNum; j++)
		{
			printf("%2.3f%s", vSFRResult[j], j < nMaxFreqeuncyNum - 1 ? ", " : "");
#ifdef _DEBUG_
			fp << (vSFRResult[j] / 100.0) << ",";
#endif
		}
		printf(")\n");
	}

#ifdef _DEBUG_
	double avg = 0.0;

	fp << ",";
	avg = (vFinalResult[0] + vFinalResult[1] + vFinalResult[2] + vFinalResult[3]) / 4.0 / 100.0;
	fp << avg << ",";
	avg = (vFinalResult[4] + vFinalResult[5]) / 2.0 / 100.0;
	fp << avg << ",";
	avg = (vFinalResult[6] + vFinalResult[7]) / 2.0 / 100.0;
	fp << avg << ",";
	avg = (vFinalResult[8] + vFinalResult[9]) / 2.0 / 100.0;
	fp << avg << ",";
	avg = (vFinalResult[10] + vFinalResult[11]) / 2.0 / 100.0;
	fp << avg << "\n";
	fp.close();
#endif

	DisplayOutImage((char*)vBmpBuffer.data(), nWidth, nHeight, vRectROI.data(), vFinalResult.data(), pTxtPosition,nMaxROINum, ImagePath, bSaveResultImage); //PIC 그리는 함수

	return vFinalResult.data();
}

double* CLG_LGIT_Algorithm::CalulateFreq(TDATASPEC &tDataSpec, BYTE *pBuffer, int nWidth, int nHeight, int nMaxROINum, int *nDirection, POINT *ptROICenter, int ROI_WIDTH, int ROI_HEIGHT, double dSFR, int *pTxtPosition, bool bRaw, double dPixelSize, double dMaxEdgeAngle, const char *ImagePath, bool bSaveResultImage)
{
#ifdef _DEBUG_
	bool bFileExist = true;
	ofstream fp;

	if (_access("Freq_RESULT.csv", 0))
		bFileExist = false;
	fp.open("Freq_RESULT.csv", std::ofstream::out | std::ofstream::app);
	if (bFileExist == false)
	{
		fp << "Image File" << ",";
		fp << "SFR" << ",";
		for (int i = 0; i < nMaxROINum; i++)
		{
			fp << "left" << i << ",";
			fp << "top" << i << ",";
			fp << "right" << i << ",";
			fp << "bottom" << i << ",";
			fp << "Freq" << i << ",";
		}
	}

	// 소수점 3자리까지 출력
	fp << fixed;
	fp.precision(3);

	// Image file name
	fp << ImagePath << ",";
	fp << dSFR << ",";
#endif

	shared_ptr<CACMISResolutionSFR> m_pChartProc = std::make_shared<CACMISResolutionSFR>();

	// ROI regions
	vector<RECT> vRectROI(nMaxROINum);
	// Frequency results
	double dFreqResult = 0.0;
	vector<double> vFinalResult(nMaxROINum);

	// Image buffers
	vector<BYTE> vBmpBuffer(nWidth * nHeight * 3, 0);

	if (bRaw == false) // BMP image
	{
		// Image buffers
		vector<BYTE> vYImgBuffer(nWidth * nHeight, 0);

		// Obtain Y image
		ACMISSoftISP::xBMPtoY(vYImgBuffer.data(), pBuffer, nWidth, nHeight);

		// Set image buffer
		std::shared_ptr<ResolutionImageBufferManager> mgr =
			std::make_shared<ResolutionImageBufferManager>(vYImgBuffer.data(), nWidth, nHeight, nWidth);
		m_pChartProc->SetImageBuffer(*mgr.get());

		std::copy(pBuffer, pBuffer + sizeof(BYTE)* nWidth * nHeight * 3, vBmpBuffer.data());
	}
	else // Raw image
	{
		// for display
		ACMISSoftISP::xMakeBMP(pBuffer, vBmpBuffer.data(), nWidth, nHeight, tDataSpec);

		// Set image buffer
		std::shared_ptr<ResolutionImageBufferManager> mgr =
			std::make_shared<ResolutionImageBufferManager>((const VOIDTYPE*)pBuffer, nWidth, nHeight, nWidth);
		(*mgr.get()).SetDataSpec(tDataSpec.eDataFormat, tDataSpec.eOutMode, tDataSpec.eSensorType, tDataSpec.nBlackLevel);
		m_pChartProc->SetImageBuffer(*mgr.get());
	}

	printf("SFR Algorithm Version: %s\n", m_pChartProc->GetVersion());
	//m_pChartProc->SetAlgorithmConfig(ROI_WIDTH, ROI_HEIGHT, 45.0, 5.6, ESFRAlgorithm_ISO12233, ESFRMethod_SFR2Freq, false);

	// Set ROI regions
	for (int i = 0; i < nMaxROINum; i++)
	{
		int width = nDirection[i] == 0 ? ROI_WIDTH : ROI_HEIGHT;
		int height = nDirection[i] == 0 ? ROI_HEIGHT : ROI_WIDTH;

		vRectROI[i].left = ptROICenter[i].x - width / 2;
		vRectROI[i].top = ptROICenter[i].y - height / 2;
		vRectROI[i].right = vRectROI[i].left + width - 1;
		vRectROI[i].bottom = vRectROI[i].top + height - 1;
#ifdef _DEBUG_
		fp << vRectROI[i].left << ",";
		fp << vRectROI[i].top << ",";
		fp << vRectROI[i].right << ",";
		fp << vRectROI[i].bottom << ",";
#endif

		// Calculate SFR
		int ret = m_pChartProc->CalcFreq(vRectROI[i], nDirection[i], dSFR, &dFreqResult);
		vFinalResult[i] = dFreqResult;
		printf("%2d ROI(%4d %4d %4d %4d) Ret(%d) SFR(%.3f) Freq(%2.3f)\n", i, vRectROI[i].left, vRectROI[i].top, vRectROI[i].right, vRectROI[i].bottom, ret, dSFR, dFreqResult);
#ifdef _DEBUG_
		fp << dFreqResult << ",";
#endif
	}

	DisplayOutImage((char*)vBmpBuffer.data(), nWidth, nHeight, vRectROI.data(), vFinalResult.data(), pTxtPosition, nMaxROINum,  ImagePath, bSaveResultImage);

#ifdef _DEBUG_
	fp.close();
#endif

	return vFinalResult.data();
}

int CLG_LGIT_Algorithm::DisplayOutImage(char *BmpBuffer, int nWidth, int nHeight, RECT *pROI, double *pFinalResult, int *pTxtPosition, int nMaxROINum, const char *ImagePath , bool bSaveResultImage )
{
	TChartSpec m_stChartSpec;
	int scale = (nWidth > 640 ? 1 : 2);
	double nFontSize = 0.5 / scale;
	char title[100];

	// Output image
	IplImage *cvImgMTF = cvCreateImage(cvSize(nWidth, nHeight), 8, 3);

	cvImgMTF->imageData = BmpBuffer;

	m_stChartSpec.nDisplayGapFromROI = 10;
	DisplaySFRGraphics(cvImgMTF, pROI, pFinalResult, pTxtPosition, &m_stChartSpec, nMaxROINum, nWidth, nFontSize, CV_RED); //실질적으로 PIC 그리는 함수

	if (ImagePath != nullptr && bSaveResultImage == true)
	{
		char filename[100];
		char *tmp = (char *)strrchr(ImagePath, '.');

		if (tmp != nullptr)
		{
			strncpy_s(filename, ImagePath, (int)(tmp - ImagePath));
		}
		else
		{
			strcpy_s(filename, ImagePath);
		}
		strcat_s(filename, "_Resolution_result.bmp");
		printf("Saving Result image: %s\n", filename);
		cvSaveImage(filename, cvImgMTF);
	}

	sprintf(title, "Resolution%s%s", ImagePath != nullptr ? " - " : "", ImagePath != nullptr ? ImagePath : "");
	cvShowImage(title, cvImgMTF); //나중에 없애야 함
	cvWaitKey(0);
	cvReleaseImage(&cvImgMTF);

	return 0;
}

void CLG_LGIT_Algorithm::DisplaySFRGraphics(IplImage* _cvImgBuf, RECT* _pROI, double* _SFRROIResult, int* _TxtPositon, TChartSpec* _spec, int _nMaxROINum, int _nImageWidth,  double nFontSize, CvScalar color)
{
	IplImage* cvImgBuf = _cvImgBuf;
	int i = 0;

	if (cvImgBuf == nullptr || _pROI == nullptr || _SFRROIResult == nullptr)
		return;

	CvFont cvfont;
	CvPoint pt;
	char strTmp[1024];
	int scale = (_nImageWidth > 640 ? 1 : 2);
#define FONTSIZE	9

	for (int i = 0; i < _nMaxROINum; i++)
	{
		cvRectangle(cvImgBuf, cvPoint((int)_pROI[i].left, (int)_pROI[i].top), cvPoint((int)_pROI[i].right, (int)_pROI[i].bottom), color);

		cvInitFont(&cvfont, CV_FONT_HERSHEY_SIMPLEX | CV_FONT_NORMAL, nFontSize, nFontSize, 0, 1, 10);
		sprintf_s(strTmp, "[%02d]%2.3f", i, _SFRROIResult[i]);

		if (_TxtPositon[i] == ROI_TOP)
		{
			pt.x = (int)_pROI[i].left;
			pt.y = (int)_pROI[i].top - _spec->nDisplayGapFromROI;
		}
		else if (_TxtPositon[i] == ROI_BOTTOM)
		{
			pt.x = (int)_pROI[i].left;
			pt.y = (int)_pROI[i].bottom + _spec->nDisplayGapFromROI + FONTSIZE / scale;
		}
		else if (_TxtPositon[i] == ROI_LEFT)
		{
			pt.x = (int)(_pROI[i].left - (int)(strlen(strTmp) * FONTSIZE / scale) - _spec->nDisplayGapFromROI + FONTSIZE / 2);
			pt.y = (int)(_pROI[i].top + (_pROI[i].bottom - _pROI[i].top) / 2 + 5 / scale);
		}
		else
		{
			pt.x = (int)(_pROI[i].right + _spec->nDisplayGapFromROI - FONTSIZE / 2);
			pt.y = (int)(_pROI[i].top + (_pROI[i].bottom - _pROI[i].top) / 2 + 5 / scale);
		}
		cvPutText(cvImgBuf, strTmp, pt, &cvfont, color);
	}

	
}


int CLG_LGIT_Algorithm::InspectStain(TDATASPEC &tDataSpec, TStainSpec &tStainSpec, BYTE *pBuffer, int nWidth, int nHeight, bool bRaw , const char *ImagePath, bool bEnableBlackSpot,  bool bSaveResultImage)
{
	int nResultBlackSpot = -1;
	char title[100];

	// Image buffers
	vector<BYTE> vBmpBuffer(nWidth * nHeight * 3, 0);

	// output image
	IplImage *cvImgStain = cvCreateImage(cvSize(nWidth, nHeight), 8, 3);
	cvImgStain->imageData = (char*)vBmpBuffer.data();

	// for display
	if (bRaw)
	{
		ACMISSoftISP::xMakeBMP(pBuffer, vBmpBuffer.data(), nWidth, nHeight, tDataSpec);
	}
	else
	{
		copy(pBuffer, pBuffer + sizeof(BYTE)* nWidth * nHeight * 3, vBmpBuffer.data());
	}

	if (bEnableBlackSpot)
	{   //nResultBlackSpot은 Fail 유형
		nResultBlackSpot = InspectBlackSpotContrast(pBuffer, nWidth, nHeight, tStainSpec.stSpecBlackSpot, tDataSpec.eDataFormat, tDataSpec.eOutMode, tDataSpec.eSensorType, tDataSpec.nBlackLevel, cvImgStain);
	}


	if (ImagePath != nullptr && bSaveResultImage == true)
	{
		char filename[100];
		char *tmp = (char *)strrchr(ImagePath, '.');

		if (tmp != nullptr)
		{
			strncpy_s(filename, ImagePath, (int)(tmp - ImagePath));
		}
		else
		{
			strcpy_s(filename, ImagePath);
		}
		strcat_s(filename, "_Stain_result.bmp");
		printf("Saving Result image: %s\n", filename);
		cvSaveImage(filename, cvImgStain);
	}

	sprintf(title, "Stain%s%s", ImagePath != nullptr ? " - " : "", ImagePath != nullptr ? ImagePath : "");
	cvShowImage(title, cvImgStain); //나중에 없애야 함
	cvReleaseImage(&cvImgStain);
	cvWaitKey(0);

	return 0;
}

int CLG_LGIT_Algorithm::InspectBlackSpotContrast(const BYTE* pBuffer, int nWidth, int nHeight, TBlackSpotContrast& _Spec, EDATAFORMAT dataFormat, EOUTMODE outMode, ESENSORTYPE sensorType, int nBlackLevel, IplImage *cvImgBlackSpot)
{
	int nResult = R_RESULT_PASS;
	shared_ptr<CACMISImageBlackSpotContrastCommon> pInspectBlackSpot = make_shared<CACMISImageBlackSpotContrastCommon>();

	// inspection
	int nDefectCount = pInspectBlackSpot->Inspect((BYTE*)pBuffer, nWidth, nHeight, _Spec, dataFormat, outMode, sensorType, nBlackLevel);

	// logging
	//pInspectBlackSpot->GetVersion();
	//nDefectCount;
	//pInspectBlackSpot->GetSingleDefectCount();
	//pInspectBlackSpot->GetDefectBlobCount();

	if (pInspectBlackSpot->GetSingleDefectCount() > _Spec.nMaxSingleDefectNum)
		nResult = R_FAIL_NOISE;
	else if (pInspectBlackSpot->GetDefectBlobCount() > 0)
		nResult = R_FAIL_BLACK_SPOT;

	if (nResult)
	{
		for (int i = 0; i < (int)pInspectBlackSpot->GetDefectBlobCount(); i++)
		{
			const RECT* rt = pInspectBlackSpot->GetDefectBlobRect(i);
			cvRectangle(cvImgBlackSpot, cvPoint(rt->left, rt->top), cvPoint(rt->right, rt->bottom), CV_RGB(255, 0, 0));
		}

		const TDefectResult* pMaxResult = pInspectBlackSpot->GetMaxDefectResult();

		int crossSize = 20;
		cvLine(cvImgBlackSpot, cvPoint(pMaxResult->ptPos.x - crossSize, pMaxResult->ptPos.y - crossSize),
			cvPoint(pMaxResult->ptPos.x + crossSize, pMaxResult->ptPos.y + crossSize), CV_RGB(255, 0, 0));
		cvLine(cvImgBlackSpot, cvPoint(pMaxResult->ptPos.x + crossSize, pMaxResult->ptPos.y - crossSize),
			cvPoint(pMaxResult->ptPos.x - crossSize, pMaxResult->ptPos.y + crossSize), CV_RGB(255, 0, 0));

		if (_Spec.tCircleSpec.bEnableCircle)
		{
			int ocx = (int)pMaxResult->dContrastMaxR;
			int ocy = (int)pMaxResult->dContrastMaxGb;
			int radx = (int)pMaxResult->dContrastMaxGr;
			int rady = (int)pMaxResult->dContrastMaxB;

			/*std::cout << "[BlackSpot] ocx=" << ocx << "ocy = " << ocy << "radx = " << radx << "rady = " << rady << std::endl;
			std::cout << "[BlackSpot] posx=" << pMaxResult->ptPos.x << "poxy = " << pMaxResult->ptPos.y << std::endl;*/

			cvEllipse(cvImgBlackSpot, cvPoint(ocx, ocy), cvSize(radx, rady), 0, 0, 360, CV_RGB(0, 255, 255));
		}
	}

	return nResult;
}


int CLG_LGIT_Algorithm::Inspect_ShaingUniformity(const BYTE* pBuffer, int nImageWidth, int nImageHeight, TShadingUniformitySpec& _Spec, EDATAFORMAT dataFormat, EOUTMODE outMode, ESENSORTYPE sensorType, int nBlackLevel)
{

	// Normalized Spatial frequency for SFR calculation : 0.0 ~ 1.0

	int nResult = R_RESULT_PASS;
	char strTmp[1024];

	TDATASPEC tDataSpec;
	tDataSpec.eDataFormat = dataFormat;
	tDataSpec.eOutMode = outMode;
	tDataSpec.eSensorType = sensorType;
	tDataSpec.nBlackLevel = nBlackLevel;

	// Image buffers
	std::vector<BYTE> vBmpBuffer(nImageWidth * nImageHeight * 3, 0);
	std::vector<BYTE> vBuffer(nImageWidth * nImageHeight, 0);

	IplImage *cvImgRU = cvCreateImage(cvSize(nImageWidth, nImageHeight), 8, 3);
	cvImgRU->imageData = (char*)vBmpBuffer.data();

	ACMISSoftISP::xMakeBMP((BYTE*)pBuffer, vBmpBuffer.data(), nImageWidth, nImageHeight, tDataSpec);

	CACMISShadingUniformity *m_pShadingUniformity = new CACMISShadingUniformity();

	//Inspection
	m_pShadingUniformity->Inspect(pBuffer, nImageWidth, nImageHeight, _Spec, dataFormat, outMode, sensorType, nBlackLevel);

	//Logging
	std::cout << "[Shading Uniformity] Version = " << m_pShadingUniformity->GetVersion() << std::endl;

	CvFont cvfont;
	CvPoint pt;
	int scale = (nImageWidth > 640 ? 1 : 2);
	double nFontSize = 0.5 / scale;

	cvInitFont(&cvfont, CV_FONT_HERSHEY_SIMPLEX | CV_FONT_NORMAL, nFontSize, nFontSize, 0, 1, 10);

	const TShadingUniformityResult *pCenterResult;//= m_pShadingUniformity->GetShadingUniformityCenterResult();

	sprintf_s(strTmp, "Center Intensity(%.3f)", pCenterResult->dResult);
	pt.x = 60;
	pt.y = 10;
	cvPutText(cvImgRU, strTmp, pt, &cvfont, CV_YELLOW);

	for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < (_Spec.nMaxROICount); i++)
		{
			const TShadingUniformityResult* pResult;// = m_pShadingUniformity->GetShadingUniformityResult((EROIPosition)j, i);
			printf("[Position=%d]i=%2d,RECT(%d, %d, %d, %d), NorVal=%.2f, Val=%.2f\n", j, i + 1, pResult->rtROI.left, pResult->rtROI.top, pResult->rtROI.right, pResult->rtROI.bottom, pResult->dNormalResult, pResult->dResult);
			cvRectangle(cvImgRU, cvPoint(pResult->rtROI.left, pResult->rtROI.top), cvPoint(pResult->rtROI.right, pResult->rtROI.bottom), CV_LIGHTGREEN);
			sprintf_s(strTmp, "[%02d]%2.3f", (pResult->nIndex + 1), pResult->dNormalResult);

			pt.x = pResult->rtROI.left;
			pt.y = pResult->rtROI.top + 10;

			bool ret = m_pShadingUniformity->Inspec((EROIPosition)j, i);
			if (ret)
			{
				cvPutText(cvImgRU, strTmp, pt, &cvfont, CV_BLUE);
			}
			else
			{
				cvPutText(cvImgRU, strTmp, pt, &cvfont, CV_RED);
			}
		}
	}

	cvShowImage("Shading Uniformity", cvImgRU);
	cvReleaseImage(&cvImgRU);
	cvWaitKey(0);

	delete m_pShadingUniformity;

	return nResult;
}


int CLG_LGIT_Algorithm::InspectSNRBW2(TDATASPEC &tDataSpec, TSNRBWSpec &tSNRSpec, BYTE *pBuffer, int nWidth, int nHeight, bool bRaw, const char *ImagePath, bool bSaveResultImage)
{
	char title[100];

	// Image buffers
	std::vector<BYTE> vBmpBuffer(nWidth * nHeight * 3, 0);

	// output image
	IplImage *cvImgSNR = cvCreateImage(cvSize(nWidth, nHeight), 8, 3);
	cvImgSNR->imageData = (char*)vBmpBuffer.data();

	// for display
	if (bRaw)
	{
		ACMISSoftISP::xMakeBMP(pBuffer, vBmpBuffer.data(), nWidth, nHeight, tDataSpec);
	}
	else
	{
		std::copy(pBuffer, pBuffer + sizeof(BYTE)* nWidth * nHeight * 3, vBmpBuffer.data());
	}

	std::shared_ptr<CACMISSignalNoiseRatioBW> pSNRBW = std::make_shared<CACMISSignalNoiseRatioBW>();
	const TSNRResult* pSNRResult = nullptr;

	std::cout << "[SNRBW2] Version: " << pSNRBW->GetVersion() << std::endl;

	if (!pSNRBW->Inspect(pBuffer, nWidth, nHeight, tSNRSpec, tDataSpec.eDataFormat, tDataSpec.eOutMode, tDataSpec.eSensorType, tDataSpec.nBlackLevel))
	{
		std::cout << "[SNRBW2] Inspection Fail! " << std::endl;
		return FALSE;
	}

	std::cout << "[SNRBW2] Region Count : " << pSNRBW->GetSNRRegionCount() << std::endl;

	for (int i = 0; i < pSNRBW->GetSNRRegionCount(); i++)
	{
		pSNRResult = pSNRBW->GetSNRResult(i);
		if (pSNRResult)
		{
			std::cout << "[SNRBW2] Index : " << pSNRResult->nIndex << std::endl;
			std::cout << "[SNRBW2] Variance Value : " << pSNRResult->dVariance << std::endl;
			std::cout << "[SNRBW2] Average Value : " << pSNRResult->dAverage << std::endl;
			std::cout << "[SNRBW2] Region : " << pSNRResult->rtROI.left << ", " << pSNRResult->rtROI.top << ", " << pSNRResult->rtROI.right << ", " << pSNRResult->rtROI.bottom << std::endl;

			DisplaySNRGraphics(cvImgSNR, pSNRResult, nWidth, CV_GREEN);
		}
	}

	pSNRResult = pSNRBW->GetMinSNRResult();
	if (pSNRResult)
	{
		std::cout << "[SNRBW2] SNRValue : " << pSNRResult->dSNRResult << std::endl;
		std::cout << "[SNRBW2] DRValue : " << pSNRResult->dDRResult << std::endl;

		DisplaySNRGraphics(cvImgSNR, pSNRResult, nWidth, CV_RED);
	}

	std::cout << pSNRBW->GetLogHeader() << std::endl;
	std::cout << pSNRBW->GetLogData() << std::endl;

	if (ImagePath != nullptr && bSaveResultImage == true)
	{
		char filename[100];
		char *tmp = (char *)strrchr(ImagePath, '.');

		if (tmp != nullptr)
		{
			strncpy_s(filename, ImagePath, (int)(tmp - ImagePath));
		}
		else
		{
			strcpy_s(filename, ImagePath);
		}
		strcat_s(filename, "_SNRBW2_result.bmp");
		printf("Saving Result image: %s\n", filename);
		cvSaveImage(filename, cvImgSNR);
	}

	sprintf(title, "SNRBW2%s%s", ImagePath != nullptr ? " - " : "", ImagePath != nullptr ? ImagePath : "");
	cvShowImage(title, cvImgSNR);
	cvReleaseImage(&cvImgSNR);
	cvWaitKey(0);

	return 0;
}

void CLG_LGIT_Algorithm::DisplaySNRGraphics(IplImage* _cvImgBuf, const TSNRResult* _SNRResult, int _nImageWidth, CvScalar color)
{
	IplImage* cvImgBuf = _cvImgBuf;

	if (cvImgBuf == nullptr || _SNRResult == nullptr)
		return;

	CvFont cvfont;
	CvPoint pt, pt2;
	char strTmp[256], strTmp2[256];
	int scale = (_nImageWidth > 640 ? 1 : 2);
	double nFontSize = 0.5 / scale;
#define FONTSIZE	9

	cvInitFont(&cvfont, CV_FONT_HERSHEY_SIMPLEX | CV_FONT_NORMAL, nFontSize, nFontSize, 0, 1, 10);
	if (_SNRResult->rtROI.right != 0 && _SNRResult->rtROI.bottom != 0)
	{
		if (_SNRResult->dSNRResult == 0.0 && _SNRResult->dDRResult == 0.0)
		{
			sprintf_s(strTmp, "[%d]Avg : %.3f", _SNRResult->nIndex, _SNRResult->dAverage);
			sprintf_s(strTmp2, "[%d]Var : %.3f", _SNRResult->nIndex, _SNRResult->dVariance);
		}
		else if (_SNRResult->dDRResult != 0.0)
		{
			sprintf_s(strTmp, "[%d]SNR : %2.3f", _SNRResult->nIndex, _SNRResult->dSNRResult);
			sprintf_s(strTmp2, "[%d]DR : %2.3f", _SNRResult->nIndex, _SNRResult->dDRResult);
		}
		else
		{
			sprintf_s(strTmp, "[%d]SNR : %2.3f", _SNRResult->nIndex, _SNRResult->dSNRResult);
			memset(strTmp2, 0, sizeof(strTmp2));
		}

		cvRectangle(cvImgBuf, cvPoint((int)_SNRResult->rtROI.left, (int)_SNRResult->rtROI.top), cvPoint((int)_SNRResult->rtROI.right, (int)_SNRResult->rtROI.bottom), color);

		pt.x = (_SNRResult->rtROI.left + _SNRResult->rtROI.right - 1) / 2 - (int)(strlen(strTmp) * FONTSIZE / 2 / scale);
		pt.y = (_SNRResult->rtROI.top + _SNRResult->rtROI.bottom - 1) / 2 + FONTSIZE / 2 / scale;
		pt2.x = (_SNRResult->rtROI.left + _SNRResult->rtROI.right - 1) / 2 - (int)(strlen(strTmp2) * FONTSIZE / 2 / scale);
		pt2.y = (_SNRResult->rtROI.top + _SNRResult->rtROI.bottom - 1) / 2 + FONTSIZE / 2 / scale + FONTSIZE * 2;
	}
	else
	{
		if (_SNRResult->dSNRResult == 0.0 && _SNRResult->dDRResult == 0.0)
		{
			sprintf_s(strTmp, "Avg : %.3f", _SNRResult->dAverage);
			sprintf_s(strTmp2, "VAR : %.3f", _SNRResult->dVariance);
		}
		else if (_SNRResult->dDRResult != 0.0)
		{
			sprintf_s(strTmp, "SNR : %2.3f", _SNRResult->dSNRResult);
			sprintf_s(strTmp2, "DR : %2.3f", _SNRResult->dDRResult);
		}
		else
		{
			sprintf_s(strTmp, "SNR : %2.3f", _SNRResult->dSNRResult);
			memset(strTmp2, 0, sizeof(strTmp2));
		}

		pt.x = _nImageWidth / 2 - (int)(strlen(strTmp) * FONTSIZE / 2 / scale);
		pt.y = FONTSIZE * 2;
		pt2.x = _nImageWidth / 2 - (int)(strlen(strTmp2) * FONTSIZE / 2 / scale);
		pt2.y = FONTSIZE * 2 + FONTSIZE * 2;
	}
	cvPutText(cvImgBuf, strTmp, pt, &cvfont, color);
	cvPutText(cvImgBuf, strTmp2, pt2, &cvfont, color);
}
