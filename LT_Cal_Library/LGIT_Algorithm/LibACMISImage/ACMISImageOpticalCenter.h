#pragma once

#include "export.h"
#include <memory>
#include "ACMISImageDef.h"


class ACMISIMAGE_API CACMISOpticalCenterPolynomialFit : public CACMISOpticalCenter<TOpticalCenterPolyFit>
{
public:
	CACMISOpticalCenterPolynomialFit();
	~CACMISOpticalCenterPolynomialFit();
};

class ACMISIMAGE_API CACMISOpticalCenterCentroid : public CACMISOpticalCenter<TOpticalCenter>
{
public:
	CACMISOpticalCenterCentroid();
	~CACMISOpticalCenterCentroid();
};

class ACMISIMAGE_API CACMISOpticalCenterCentroidRgb : public CACMISOpticalCenter<TOpticalCenterRgb>
{
public:
	CACMISOpticalCenterCentroidRgb();
	~CACMISOpticalCenterCentroidRgb();
};
