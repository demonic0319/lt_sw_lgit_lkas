﻿//*****************************************************************************
// Filename	: PageOpt_HT.cpp
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 15:33
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_HT.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"
#include <memory>
#include "MFCPropertyGridProperties.h"

#define ID_PROPGRID_IPADDR (21)

IMPLEMENT_DYNAMIC(CPageOpt_HT, CPageOption)

//=============================================================================
// 생성자
//=============================================================================
CPageOpt_HT::CPageOpt_HT(void)
{
}

CPageOpt_HT::CPageOpt_HT(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
// 소멸자
//=============================================================================
CPageOpt_HT::~CPageOpt_HT(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_HT, CPageOption)	
END_MESSAGE_MAP()

//=============================================================================
// CPageOpt_HT 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CPageOpt_HT::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_HT::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_HT::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_HT::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_HT::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_HT::InitPropList()
{
	CPageOption::InitPropList();

	//--------------------------------------------------------
	// 통신 설정
	//--------------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Comm(new CMFCPropertyGridProperty(_T("연결 주소 설정")));

	CMFCPropertyGridProperty* pProp = NULL;
	in_addr addr;

	// 서버 구동 설정 --------------------------------------

	
	// IP Address			
	addr.s_addr = inet_addr("192.168.101.10");
	pProp = new CMFCPropertyGridIPAdressProperty(_T("Server IP Address"), addr, OPT_DESC_IP_ADDRESS, ID_PROPGRID_IPADDR);
	apGroup_Comm->AddSubItem(pProp);

	// Port
	CString szText;
	szText.Format(_T("Server Port: %d"), 1);
	pProp = new CMFCPropertyGridProperty(szText, (_variant_t)1296l, OPT_DESC_IP_PORT);
	pProp->EnableSpinControl(TRUE, 0, 9999);
	apGroup_Comm->AddSubItem(pProp);

// 	if (m_InsptrType != Sys_Handler)
// 	{
// 		// 클라이언트 주소 (HandlerAddr)
// 		addr.s_addr = inet_addr("192.168.101.10");
// 		pProp = new CMFCPropertyGridIPAdressProperty(_T("Handler IP Address"), addr, OPT_DESC_IP_ADDRESS, ID_PROPGRID_IPADDR);
// 		apGroup_Comm->AddSubItem(pProp);
// 
// 	}
// 	else{	
		addr.s_addr = inet_addr("192.168.101.10");
		pProp = new CMFCPropertyGridIPAdressProperty(_T("NIC IP Address"), addr, OPT_DESC_NIC_IP_ADDRESS, ID_PROPGRID_IPADDR);
		apGroup_Comm->AddSubItem(pProp);
// 	}
	

	
	m_wndPropList.AddProperty(apGroup_Comm.release());

	//-----------------------------------------------------
	// 통신 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Ack(new CMFCPropertyGridProperty(_T("통신 설정")));

	// Ping 주기 (1초) dwPingCycle
	pProp = new CMFCPropertyGridProperty(_T("Ping 주기 (ms)"), (_variant_t)1000l, OPT_DESC_IP_PORT);
	pProp->EnableSpinControl(TRUE, 0, 9999);
	apGroup_Ack->AddSubItem(pProp);

	// Ping Ack Timeout 시간 (3분간 응답 대기) dwPinigAckTimeout
	pProp = new CMFCPropertyGridProperty(_T("Ping Ack Timeout (Sec)"), (_variant_t)180l, OPT_DESC_IP_PORT);
	pProp->EnableSpinControl(TRUE, 0, 9999);
	apGroup_Ack->AddSubItem(pProp);

	// Ack Timeout (dwAckTimeout)
	pProp = new CMFCPropertyGridProperty(_T("통신 응답 대기시간 (ms)"), (_variant_t)180l, OPT_DESC_IP_PORT);
	pProp->EnableSpinControl(TRUE, 0, 9999);
	apGroup_Ack->AddSubItem(pProp);

	m_wndPropList.AddProperty(apGroup_Ack.release());
}

//=============================================================================
// Method		: CPageOpt_HT::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_HT::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_HT(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_HT::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_HT::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_HT(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: GetOption
// Access		: protected  
// Returns		: Luritech_Option::stOpt_HT
// Qualifier	:
// Last Update	: 2016/5/18 - 16:22
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_HT CPageOpt_HT::GetOption()
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	COleVariant rVariant;
	VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 그룹 1 
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();

	USES_CONVERSION;

	// 서버 구동 설정 --------------------------------------
	// 서버 IP Address
	m_stOption.ServerAddr.dwAddress = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;
	m_stOption.ServerAddr.dwAddress = m_stOption.ServerAddr.dwAddress;
		
	// 서버 IP Port
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_I4);
	m_stOption.ServerAddr.dwPort = varData.intVal;

// 	if (m_InsptrType != Sys_Handler)
// 	{
// 		// 클라이언트 주소
// 		m_stOption.HandlerAddr.dwAddress = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;
// 	}
// 	else{
		// NIC IP Address
		m_stOption.dwNIC_Address = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;
// 	}
	

	//---------------------------------------------------------------
	// 그룹 2
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Ping 주기 (1초)
	m_stOption.dwPingCycle = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	// Ping Ack Timeout 시간 (3분간 응답 대기)
	m_stOption.dwPinigAckTimeout = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	// Ack Timeout
	m_stOption.dwAckTimeout = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	return m_stOption;
}

//=============================================================================
// Method		: SetOption
// Access		: protected  
// Returns		: void
// Parameter	: stOpt_HT stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 16:22
// Desc.		:
//=============================================================================
void CPageOpt_HT::SetOption( stOpt_HT stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	//---------------------------------------------------------------
	// 그룹 1 통신 종류
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();
	
	// 서버 구동 설정 --------------------------------------
	// 서버 IP Address
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.ServerAddr.dwAddress));		

	// 서버 IP Port
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.ServerAddr.dwPort);

	// 클라이언트 주소	
// 	if (m_InsptrType != Sys_Handler)
// 	{
// 		(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.HandlerAddr.dwAddress));
// 	}
// 	else{
		// NIC IP Address
		(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.dwNIC_Address));
//	}
	
	//---------------------------------------------------------------
	// 그룹 2
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Ping 주기 (1초)		
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.dwPingCycle);

	// Ping Ack Timeout 시간 (3분간 응답 대기)		
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.dwPinigAckTimeout);

	// Ack Timeout		
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue((_variant_t)(long int)m_stOption.dwAckTimeout);
}
