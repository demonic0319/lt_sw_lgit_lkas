//*****************************************************************************
// Filename	: PageOpt_TCP_IP.h
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 18:05
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef PageOpt_TCP_IP_h__
#define PageOpt_TCP_IP_h__

#pragma once
#include "PageOption.h"
#include "Define_Option.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CPageOpt_TCP_IP : public CPageOption
{
	DECLARE_DYNAMIC(CPageOpt_TCP_IP)

public:
	CPageOpt_TCP_IP						(void);
	CPageOpt_TCP_IP						(UINT nIDTemplate, UINT nIDCaption = 0);
	virtual ~CPageOpt_TCP_IP				(void);

protected:

	DECLARE_MESSAGE_MAP()

	virtual void			AdjustLayout		();
	virtual void			SetPropListFont		();
	virtual void			InitPropList		();

	stOpt_Ethernet			m_stOption;

	stOpt_Ethernet			GetOption			();
	void					SetOption			(stOpt_Ethernet stOption);

public:

	virtual void			SaveOption			();
	virtual void			LoadOption			();
};

#endif // PageOpt_TCP_IP_h__
