﻿//*****************************************************************************
// Filename	: 	PageOpt_PCB.cpp
// Created	:	2015/12/16 - 15:23
// Modified	:	2015/12/16 - 15:23
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_PCB.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"

#include <memory>
#include "CustomProperties.h"

IMPLEMENT_DYNAMIC(CPageOpt_PCB, CPageOption)

//=============================================================================
//
//=============================================================================
CPageOpt_PCB::CPageOpt_PCB(LPCTSTR lpszCaption /*= NULL*/) : CPageOption(lpszCaption)
{
}

CPageOpt_PCB::CPageOpt_PCB(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
//
//=============================================================================
CPageOpt_PCB::~CPageOpt_PCB(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_PCB, CPageOption)
END_MESSAGE_MAP()

// CPageOpt_PCB 메시지 처리기입니다.

//=============================================================================
// Method		: CPageOpt_PCB::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCB::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_PCB::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCB::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_PCB::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCB::InitPropList()
{
	CPageOption::InitPropList();

	CString szGroupName;

	//--------------------------------------------------------
	// 컨트롤 보드 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].CtrlBrd_Cnt; nIdx++)
	{
		szGroupName.Format(_T("Control Board %d : COM Port Setting"), nIdx + 1);

		std::auto_ptr<CMFCPropertyGridProperty> apGroup_ComPort_Ctrl(new CMFCPropertyGridProperty(szGroupName));
		InitPropList_Comport(apGroup_ComPort_Ctrl);
		m_wndPropList.AddProperty(apGroup_ComPort_Ctrl.release());
	}

	//--------------------------------------------------------
	// 광원 제어 보드(루리텍) 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightBrd_Cnt; nIdx++)
	{
		szGroupName.Format(_T("Light Board %d : COM Port Setting"), nIdx + 1);

		std::auto_ptr<CMFCPropertyGridProperty> apGroup_ComPort_Light(new CMFCPropertyGridProperty(szGroupName));
		InitPropList_Comport(apGroup_ComPort_Light);
		m_wndPropList.AddProperty(apGroup_ComPort_Light.release());
	}

	//--------------------------------------------------------
	// 광원 파워 서플라이(ODA PT) 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
	{
		szGroupName.Format(_T("Light Power Supply : COM Port Setting"));

		std::auto_ptr<CMFCPropertyGridProperty> apGroup_ComPort_LightPSU(new CMFCPropertyGridProperty(szGroupName));
		InitPropList_Comport(apGroup_ComPort_LightPSU);
		m_wndPropList.AddProperty(apGroup_ComPort_LightPSU.release());
	}

	//--------------------------------------------------------
	// 변위 센서 통신 포트 설정
	//--------------------------------------------------------
	//for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
	{
		szGroupName.Format(_T("Displace Sensor : COM Port Setting"));

		std::auto_ptr<CMFCPropertyGridProperty> apGroup_ComPort_Displace(new CMFCPropertyGridProperty(szGroupName));
		InitPropList_Comport(apGroup_ComPort_Displace);
		m_wndPropList.AddProperty(apGroup_ComPort_Displace.release());
	}
}

//=============================================================================
// Method		: CPageOpt_PCB::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCB::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption = GetOption();

	m_pLT_Option->SaveOption_PCB(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_PCB::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCB::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_PCB(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_PCB::GetOption
// Access		: protected 
// Returns		: Luritech_Option::stOpt_100KPower
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_PCB CPageOpt_PCB::GetOption()
{
	UINT nGroupIndex = 0;
	UINT nSubItemIndex = 0;
	UINT nIndex = 0;

	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;
	
	//--------------------------------------------------------
	// 컨트롤 보드 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].CtrlBrd_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		GetOption_ComPort(pPropertyGroup, m_stOption.ComPort_CtrlBrd[nIdx]);
	}

	//--------------------------------------------------------
	// 광원 제어 보드(루리텍) 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightBrd_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		GetOption_ComPort(pPropertyGroup, m_stOption.ComPort_LightBrd[nIdx]);
	}

	//--------------------------------------------------------
	// 광원 파워 서플라이(ODA PT) 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		GetOption_ComPort(pPropertyGroup, m_stOption.ComPort_LightPSU[nIdx]);
	}

	//--------------------------------------------------------
	// 키옌스 변위 센서 통신 포트 설정
	//--------------------------------------------------------
	//for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		GetOption_ComPort(pPropertyGroup, m_stOption.ComPort_Displace);
	}

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_PCB::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOpt_100KPower stOption
// Qualifier	:
// Last Update	: 2015/12/16 - 15:23
// Desc.		:
//=============================================================================
void CPageOpt_PCB::SetOption(stOpt_PCB stOption)
{
 	UINT nGroupIndex = 0;
	UINT nSubItemIndex	= 0;

	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	//--------------------------------------------------------
	// 컨트롤 보드 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].CtrlBrd_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		SetOption_ComPort(pPropertyGroup, m_stOption.ComPort_CtrlBrd[nIdx]);
	}

	//--------------------------------------------------------
	// 광원 제어 보드(루리텍) 통신 포트 설정
	//--------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightBrd_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		SetOption_ComPort(pPropertyGroup, m_stOption.ComPort_LightBrd[nIdx]);
	}

	//---------------------------------------------------------------
	// 파워 서플라이(ODA PT) 통신 포트 설정
	//---------------------------------------------------------------
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		SetOption_ComPort(pPropertyGroup, m_stOption.ComPort_LightPSU[nIdx]);
	}

	//---------------------------------------------------------------
	// 키옌스 변위 센서 통신 포트 설정
	//---------------------------------------------------------------
	//for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
	{
		pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
		iSubItemCount = pPropertyGroup->GetSubItemsCount();

		SetOption_ComPort(pPropertyGroup, m_stOption.ComPort_Displace);
	}

}