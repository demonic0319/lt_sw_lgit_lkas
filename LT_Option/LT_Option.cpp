﻿//*****************************************************************************
// Filename	: LT_Option.cpp
// Created	: 2016/03/09
// Modified	: 2016/03/09
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "stdafx.h"
#include "LT_Option.h"
#include "Define_OptionDescription.h"
#include "Registry.h"

#define		REG_PATH_OPTION_DEFALT	_T("Software\\Luritech\\Environment\\Option")

//=============================================================================
//
//=============================================================================
CLT_Option::CLT_Option()
{	
	m_strRegPath_Base = REG_PATH_OPTION_DEFALT;

	m_InsptrType	= enInsptrSysType::Sys_Focusing;
}

//=============================================================================
//
//=============================================================================
CLT_Option::~CLT_Option()
{

}

//=============================================================================
// Method		: CLT_Option::SetRegistryPath
// Access		: public 
// Returns		: void
// Parameter	: LPCTSTR lpszRegPath
// Qualifier	:
// Last Update	: 2016/03/09
// Desc.		: 옵션이 저장될 레지스트리의 경로 설정
//=============================================================================
void CLT_Option::SetRegistryPath( LPCTSTR lpszRegPath )
{
	if (NULL == lpszRegPath)
		AfxMessageBox(_T("lpszRegPath is NULL"));

	m_strRegPath_Base = lpszRegPath;
}

//=============================================================================
// Method		: CLT_Option::SaveOption_Inspector
// Access		: public 
// Returns		: void
// Parameter	: stOpt_Insp stOption
// Qualifier	:
// Last Update	: 2016/03/09
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_Inspector( stOpt_Insp stOption )
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\Inspector");
	CString		strValue;
	
	m_stOption.Inspector = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Area Sensor 사용 여부
		reg.WriteDWORD(_T("UseAreaSensor"), (DWORD)m_stOption.Inspector.bUseAreaSensor_Err);

		// Door Sensor 사용 여부
		reg.WriteDWORD(_T("UseDoorSensor"), (DWORD)m_stOption.Inspector.bUseDoorOpen_Err);

		// MES 사용 여부
		reg.WriteDWORD(_T("UseMES"), (DWORD)m_stOption.Inspector.bUseMES);

		if (Sys_2D_Cal == m_InsptrType)
		{
			// 영상 View 윈도우 사용 여부
			reg.WriteDWORD(_T("UseImageViewWnd"), (DWORD)m_stOption.Inspector.bUseImageViewWnd);
		}

		if (Sys_2D_Cal == m_InsptrType || Sys_IR_Image_Test == m_InsptrType || Sys_Image_Test == m_InsptrType)
		{
			// 2D CAL 암막 셔터 모터 제어 기능 사용 여부
			reg.WriteDWORD(_T("UseBlindShutter"), (DWORD)m_stOption.Inspector.bUseBlindShutter);
		}

		// RAW 이미지 저장
		reg.WriteDWORD(_T("UseSaveImage_RAW"), (DWORD)m_stOption.Inspector.bSaveImage_RAW);
		// Gray12 이미지 저장
		reg.WriteDWORD(_T("UseSaveImage_Gray12"), (DWORD)m_stOption.Inspector.bSaveImage_Gray12);
		// RGB 이미지 저장
		reg.WriteDWORD(_T("UseSaveImage_RGB"), (DWORD)m_stOption.Inspector.bSaveImage_RGB);

		// Log 경로
		reg.WriteString(_T("Path_Log"), m_stOption.Inspector.szPath_Log);
		// Report 경로
		reg.WriteString(_T("Path_Report"), m_stOption.Inspector.szPath_Report);
		// Model 경로
		reg.WriteString(_T("Path_Recipe"), m_stOption.Inspector.szPath_Recipe);
		// Pogo 경로
		reg.WriteString(_T("Path_Consumables"), m_stOption.Inspector.szPath_Consumables);
		// Motor 경로
		reg.WriteString(_T("Path_Motor"), m_stOption.Inspector.szPath_Motor);
		// Maintenance 경로
		reg.WriteString(_T("Path_Maintenance"), m_stOption.Inspector.szPath_Maintenance);
		// Image 경로
		reg.WriteString(_T("Path_Image"), m_stOption.Inspector.szPath_Image);
		// I2c 경로
		reg.WriteString(_T("Path_I2C"), m_stOption.Inspector.szPath_I2c);
		// I2c 경로
		reg.WriteString(_T("Path_I2CImage"), m_stOption.Inspector.szPath_I2cImage);
		// I2c 경로
		reg.WriteString(_T("Path_VisionImage"), m_stOption.Inspector.szPath_VisionImage);

		// 자동 재 실행 사용 여부
		reg.WriteDWORD(OPT_KEY_USE_AUTO_RESTART, (DWORD)m_stOption.Inspector.UseAutoRestart);
		// 장치 연결상태 표시 윈도우 사용여부
		reg.WriteDWORD(OPT_KEY_USE_DEVICE_INIFO_PANE, (DWORD)m_stOption.Inspector.UseDeviceInfoPane);
		// 자동 통신 연결 사용 여부
		reg.WriteDWORD(OPT_KEY_USE_AUTO_CONN, (DWORD)m_stOption.Inspector.UseAutoConnection);
		// Chart Monitor Index
	}

	reg.Close();

	if (reg.Open(HKEY_CURRENT_USER, m_strRegPath_Base))
	{
		//Password
		reg.WriteString(OPT_KEY_PASSWORD, m_stOption.Inspector.Password_Admin);
		reg.Close();
	}
}

//=============================================================================
// Method		: CLT_Option::LoadOption_Inspector
// Access		: public 
// Returns		: BOOL
// Parameter	: stOpt_Insp & stOption
// Qualifier	:
// Last Update	: 2016/03/09
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_Inspector(stOpt_Insp& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\Inspector");
	DWORD		dwValue		= 0;
	CString		strValue;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Area Sensor 사용 여부
		if (reg.ReadDWORD(_T("UseAreaSensor"), dwValue))
			m_stOption.Inspector.bUseAreaSensor_Err = (BOOL)dwValue;

		// Door Sensor 사용 여부
		if (reg.ReadDWORD(_T("UseDoorSensor"), dwValue))
			m_stOption.Inspector.bUseDoorOpen_Err = (BOOL)dwValue;

		// MES 사용 여부
		if (reg.ReadDWORD(_T("UseMES"), dwValue))
			m_stOption.Inspector.bUseMES = (BOOL)dwValue;

		if (Sys_2D_Cal == m_InsptrType)
		{
			// 영상 View 윈도우 사용 여부
			if (reg.ReadDWORD(_T("UseImageViewWnd"), dwValue))
				m_stOption.Inspector.bUseImageViewWnd = (BOOL)dwValue;
		}

		if (Sys_2D_Cal == m_InsptrType || Sys_IR_Image_Test == m_InsptrType || Sys_Image_Test == m_InsptrType)
		{
			// 2D CAL 암막 셔터 모터 제어 기능 사용 여부
			if (reg.ReadDWORD(_T("UseBlindShutter"), dwValue))
				m_stOption.Inspector.bUseBlindShutter = (BOOL)dwValue;
		}

		// RAW 이미지 저장
		if (reg.ReadDWORD(_T("UseSaveImage_RAW"), dwValue))
			m_stOption.Inspector.bSaveImage_RAW = (BOOL)dwValue;

		// Gray12 이미지 저장
		if (reg.ReadDWORD(_T("UseSaveImage_Gray12"), dwValue))
			m_stOption.Inspector.bSaveImage_Gray12 = (BOOL)dwValue;

		// RGB 이미지 저장
		if (reg.ReadDWORD(_T("UseSaveImage_RGB"), dwValue))
			m_stOption.Inspector.bSaveImage_RGB = (BOOL)dwValue;

		// Log 경로
		reg.ReadString(_T("Path_Log"), m_stOption.Inspector.szPath_Log);
		// Report 경로
		reg.ReadString(_T("Path_Report"), m_stOption.Inspector.szPath_Report);
		// Model 경로
		reg.ReadString(_T("Path_Recipe"), m_stOption.Inspector.szPath_Recipe);
		// Pogo 경로
		reg.ReadString(_T("Path_Consumables"), m_stOption.Inspector.szPath_Consumables);
		// Motor 경로
		reg.ReadString(_T("Path_Motor"), m_stOption.Inspector.szPath_Motor);
		// Maintenance 경로
		reg.ReadString(_T("Path_Maintenance"), m_stOption.Inspector.szPath_Maintenance);
		// Image 경로
		reg.ReadString(_T("Path_Image"), m_stOption.Inspector.szPath_Image);
		// I2c 경로
		reg.ReadString(_T("Path_I2C"), m_stOption.Inspector.szPath_I2c);
		// I2c 경로
		reg.ReadString(_T("Path_I2CImage"), m_stOption.Inspector.szPath_I2cImage);
		// I2c 경로
		reg.ReadString(_T("Path_VisionImage"), m_stOption.Inspector.szPath_VisionImage);

		// 자동 재 실행 사용 여부
		if (reg.ReadDWORD(OPT_KEY_USE_AUTO_RESTART, dwValue))
			m_stOption.Inspector.UseAutoRestart = (BOOL)dwValue;
		
		// 장치 연결상태 표시 윈도우 사용여부
		if (reg.ReadDWORD(OPT_KEY_USE_DEVICE_INIFO_PANE, dwValue))
			m_stOption.Inspector.UseDeviceInfoPane = (BOOL)dwValue;

		// 자동 통신 연결 사용 여부
		if (reg.ReadDWORD(OPT_KEY_USE_AUTO_CONN, dwValue))
			m_stOption.Inspector.UseAutoConnection = (BOOL) dwValue;
	}
	else
	{
		return FALSE;
	}

	reg.Close();

	if (reg.Open(HKEY_CURRENT_USER, m_strRegPath_Base))
	{
		//Password
		if (reg.ReadString(OPT_KEY_PASSWORD, strValue))
			m_stOption.Inspector.Password_Admin = strValue;

		reg.Close();
	}

	stOption = m_stOption.Inspector;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_BCR
// Access		: public  
// Returns		: void
// Parameter	: stOpt_BCR stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_BCR( stOpt_BCR stOption )
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\BCR");
	CString		strValue;

	m_stOption.BCR = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		CString szKey;

		// Handy Barcode Reader -----------------------------------------------
		// 통신 포트
		szKey.Format(_T("Handy_%s"), OPT_KEY_COMPORT);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Handy_ComPort.Port);

		// Baud Rate 
		szKey.Format(_T("Handy_%s"), OPT_KEY_BAUDRATE);
		reg.WriteDWORD(szKey, m_stOption.BCR.Handy_ComPort.BaudRate);

		// ByteSize 
		szKey.Format(_T("Handy_%s"), OPT_KEY_BYTESIZE);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Handy_ComPort.ByteSize);

		// Parity 
		szKey.Format(_T("Handy_%s"), OPT_KEY_PARITY);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Handy_ComPort.Parity);

		// StopBits 
		szKey.Format(_T("Handy_%s"), OPT_KEY_STOPBITS);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Handy_ComPort.StopBits);

		if (SYS_FOCUSING == g_InspectorTable[m_InsptrType].SysType)
		{
			// Fixed Barcode Reader -----------------------------------------------
			// 통신 포트
			szKey.Format(_T("Fixed_%s"), OPT_KEY_COMPORT);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Fixed_ComPort.Port);

			// Baud Rate 
			szKey.Format(_T("Fixed_%s"), OPT_KEY_BAUDRATE);
			reg.WriteDWORD(szKey, m_stOption.BCR.Fixed_ComPort.BaudRate);

			// ByteSize 
			szKey.Format(_T("Fixed_%s"), OPT_KEY_BYTESIZE);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Fixed_ComPort.ByteSize);

			// Parity 
			szKey.Format(_T("Fixed_%s"), OPT_KEY_PARITY);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Fixed_ComPort.Parity);

			// StopBits 
			szKey.Format(_T("Fixed_%s"), OPT_KEY_STOPBITS);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.BCR.Fixed_ComPort.StopBits);
		}
	}

	reg.Close();
}

//=============================================================================
// Method		: LoadOption_BCR
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_BCR & stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_BCR(stOpt_BCR& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\BCR");
	DWORD		dwValue		= 0;
	CString		strValue;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		CString szKey;

		// Handy Barcode Reader -----------------------------------------------
		// 통신 포트 
		szKey.Format(_T("Handy_%s"), OPT_KEY_COMPORT);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.Handy_ComPort.Port = (BYTE)dwValue;

		// Baud Rate 
		szKey.Format(_T("Handy_%s"), OPT_KEY_BAUDRATE);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.Handy_ComPort.BaudRate = dwValue;

		// ByteSize
		szKey.Format(_T("Handy_%s"), OPT_KEY_BYTESIZE);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.Handy_ComPort.ByteSize = (BYTE)dwValue;

		// Parity
		szKey.Format(_T("Handy_%s"), OPT_KEY_PARITY);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.Handy_ComPort.Parity = (BYTE)dwValue;

		// StopBits 
		szKey.Format(_T("Handy_%s"), OPT_KEY_STOPBITS);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.BCR.Handy_ComPort.StopBits = (BYTE)dwValue;

		if (SYS_FOCUSING == g_InspectorTable[m_InsptrType].SysType)
		{
			// Fixed Barcode Reader -----------------------------------------------
			szKey.Format(_T("Fixed_%s"), OPT_KEY_COMPORT);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.BCR.Fixed_ComPort.Port = (BYTE)dwValue;

			// Baud Rate 
			szKey.Format(_T("Fixed_%s"), OPT_KEY_BAUDRATE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.BCR.Fixed_ComPort.BaudRate = dwValue;

			// ByteSize
			szKey.Format(_T("Fixed_%s"), OPT_KEY_BYTESIZE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.BCR.Fixed_ComPort.ByteSize = (BYTE)dwValue;

			// Parity
			szKey.Format(_T("Fixed_%s"), OPT_KEY_PARITY);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.BCR.Fixed_ComPort.Parity = (BYTE)dwValue;

			// StopBits 
			szKey.Format(_T("Fixed_%s"), OPT_KEY_STOPBITS);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.BCR.Fixed_ComPort.StopBits = (BYTE)dwValue;
		}
	}
	else
	{
		return FALSE;
	}

	reg.Close();

	stOption = m_stOption.BCR;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_MES
// Access		: public  
// Returns		: void
// Parameter	: stOpt_MES stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:50
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_MES(stOpt_MES stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\MES");
	CString		strValue;

	m_stOption.MES = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 설비 코드
		reg.WriteString(_T("EquipmentID"), m_stOption.MES.szEquipmentID);

		// 서버 주소
		reg.WriteDWORD(OPT_KEY_IP_ADDRESS, m_stOption.MES.Address.dwAddress);
		reg.WriteDWORD(OPT_KEY_IP_PORT, m_stOption.MES.Address.dwPort);

		// MES 경로
		reg.WriteString(_T("Path_MES_Log"), m_stOption.MES.szPath_MESLog);

		// NIC IP Address
		reg.WriteDWORD(OPT_KEY_NIC_IP_ADDRESS, m_stOption.MES.dwNIC_Address);
	}

	reg.Close();
}

//=============================================================================
// Method		: LoadOption_MES
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_MES & stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:50
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_MES(stOpt_MES& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\MES");
	DWORD		dwValue = 0;
	CString		strValue;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 설비 코드
		reg.ReadString(_T("EquipmentID"), m_stOption.MES.szEquipmentID);

		// 서버 주소
 		if (reg.ReadDWORD(OPT_KEY_IP_ADDRESS, dwValue))
 			m_stOption.MES.Address.dwAddress = dwValue;
 		if (reg.ReadDWORD(OPT_KEY_IP_PORT, dwValue))
 			m_stOption.MES.Address.dwPort = dwValue;

		// MES 경로
		if (FALSE == reg.ReadString(_T("Path_MES_Log"), m_stOption.MES.szPath_MESLog))
		{
			m_stOption.MES.szPath_MESLog = _T("C:\\MES\\");
		}

		// NIC IP Address
		if (reg.ReadDWORD(OPT_KEY_NIC_IP_ADDRESS, dwValue))
			m_stOption.MES.dwNIC_Address = dwValue;
	}
	else
	{
		return FALSE;
	}

	reg.Close();

	stOption = m_stOption.MES;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_PCB
// Access		: public  
// Returns		: void
// Parameter	: stOpt_PCB stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_PCB(stOpt_PCB stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\PCB");

	m_stOption.PCB = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		
		CString szKey;

		// 제어 보드 ---------------------------------------
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].CtrlBrd_Cnt; nIdx++)
		{
			// 통신 포트
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_COMPORT);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CtrlBrd[nIdx].Port);

			// Baud Rate 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_BAUDRATE);
			reg.WriteDWORD(szKey, m_stOption.PCB.ComPort_CtrlBrd[nIdx].BaudRate);

			// ByteSize 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_BYTESIZE);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CtrlBrd[nIdx].ByteSize);

			// Parity 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_PARITY);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CtrlBrd[nIdx].Parity);

			// StopBits 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_STOPBITS);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_CtrlBrd[nIdx].StopBits);
		}

		// 광원 제어 보드(루리텍) ---------------------------------------
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightBrd_Cnt; nIdx++)
		{
			// 통신 포트
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_COMPORT);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].Port);

			// Baud Rate 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_BAUDRATE);
			reg.WriteDWORD(szKey, m_stOption.PCB.ComPort_LightBrd[nIdx].BaudRate);

			// ByteSize 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_BYTESIZE);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].ByteSize);

			// Parity 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_PARITY);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].Parity);

			// StopBits 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_STOPBITS);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightBrd[nIdx].StopBits);
		}

		// 광원 파워 서플라이(ODA PT) 통신 포트 설정 ---------------------------------------
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
		{
			// 통신 포트
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_COMPORT);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightPSU[nIdx].Port);

			// Baud Rate 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_BAUDRATE);
			reg.WriteDWORD(szKey, m_stOption.PCB.ComPort_LightPSU[nIdx].BaudRate);

			// ByteSize 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_BYTESIZE);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightPSU[nIdx].ByteSize);

			// Parity 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_PARITY);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightPSU[nIdx].Parity);

			// StopBits 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_STOPBITS);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_LightPSU[nIdx].StopBits);
		}

		// 키옌스 변위 센서 통신 포트 설정 ---------------------------------------
		//for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
		{
			// 통신 포트
			szKey.Format(_T("Displace_%s"), OPT_KEY_COMPORT);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_Displace.Port);

			// Baud Rate 
			szKey.Format(_T("Displace_%s"), OPT_KEY_BAUDRATE);
			reg.WriteDWORD(szKey, m_stOption.PCB.ComPort_Displace.BaudRate);

			// ByteSize 
			szKey.Format(_T("Displace_%s"), OPT_KEY_BYTESIZE);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_Displace.ByteSize);

			// Parity 
			szKey.Format(_T("Displace_%s"), OPT_KEY_PARITY);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_Displace.Parity);

			// StopBits 
			szKey.Format(_T("Displace_%s"), OPT_KEY_STOPBITS);
			reg.WriteDWORD(szKey, (DWORD)m_stOption.PCB.ComPort_Displace.StopBits);
		}
	}
	reg.Close();
}

//=============================================================================
// Method		: LoadOption_PCB
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_PCB & stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 15:58
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_PCB(stOpt_PCB& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\PCB");
	DWORD		dwValue = 0;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 제어 보드 ---------------------------------------
		CString szKey;
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].CtrlBrd_Cnt; nIdx++)
		{
			// 통신 포트 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_COMPORT);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CtrlBrd[nIdx].Port = (BYTE)dwValue;

			// Baud Rate 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_BAUDRATE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CtrlBrd[nIdx].BaudRate = dwValue;

			// ByteSize 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_BYTESIZE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CtrlBrd[nIdx].ByteSize = (BYTE)dwValue;

			// Parity 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_PARITY);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CtrlBrd[nIdx].Parity = (BYTE)dwValue;

			// StopBits 
			szKey.Format(_T("Ctrl_BD%d_%s"), nIdx + 1, OPT_KEY_STOPBITS);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_CtrlBrd[nIdx].StopBits = (BYTE)dwValue;
		}

		// 광원 제어 보드(루리텍) ---------------------------------------
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightBrd_Cnt; nIdx++)
		{
			// 통신 포트 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_COMPORT);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].Port = (BYTE)dwValue;

			// Baud Rate 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_BAUDRATE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].BaudRate = dwValue;

			// ByteSize 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_BYTESIZE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].ByteSize = (BYTE)dwValue;

			// Parity 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_PARITY);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].Parity = (BYTE)dwValue;

			// StopBits 
			szKey.Format(_T("Light_BD%d_%s"), nIdx + 1, OPT_KEY_STOPBITS);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightBrd[nIdx].StopBits = (BYTE)dwValue;
		}

		// 파워 서플라이 ---------------------------------------
		for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
		{
			// 통신 포트 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_COMPORT);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightPSU[nIdx].Port = (BYTE)dwValue;

			// Baud Rate 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_BAUDRATE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightPSU[nIdx].BaudRate = dwValue;

			// ByteSize 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_BYTESIZE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightPSU[nIdx].ByteSize = (BYTE)dwValue;

			// Parity 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_PARITY);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightPSU[nIdx].Parity = (BYTE)dwValue;

			// StopBits 
			szKey.Format(_T("PowerSupply%d_%s"), nIdx + 1, OPT_KEY_STOPBITS);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_LightPSU[nIdx].StopBits = (BYTE)dwValue;
		}

		// 파워 서플라이 ---------------------------------------
		//for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
		{
			// 통신 포트 
			szKey.Format(_T("Displace_%s"), OPT_KEY_COMPORT);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_Displace.Port = (BYTE)dwValue;

			// Baud Rate 
			szKey.Format(_T("Displace_%s"), OPT_KEY_BAUDRATE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_Displace.BaudRate = dwValue;

			// ByteSize 
			szKey.Format(_T("Displace_%s"), OPT_KEY_BYTESIZE);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_Displace.ByteSize = (BYTE)dwValue;

			// Parity 
			szKey.Format(_T("Displace_%s"), OPT_KEY_PARITY);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_Displace.Parity = (BYTE)dwValue;

			// StopBits 
			szKey.Format(_T("Displace_%s"), OPT_KEY_STOPBITS);
			if (reg.ReadDWORD(szKey, dwValue))
				m_stOption.PCB.ComPort_Displace.StopBits = (BYTE)dwValue;
		}
	}
	else
	{
		return FALSE;
	}
	reg.Close();

	stOption = m_stOption.PCB;

	return TRUE;
}


//=============================================================================
// Method		: SaveOption_Torque
// Access		: public  
// Returns		: void
// Parameter	: __in stOpt_Torque stOption
// Qualifier	:
// Last Update	: 2018/3/8 - 8:16
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_Torque(__in stOpt_Torque stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\Torque");

	m_stOption.Torque = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 광원 ---------------------------------------		
		CString szKey;

		// 통신 포트
		szKey.Format(_T("Torque_%s"), OPT_KEY_COMPORT);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.Torque.ComPort_Torque.Port);

		// Baud Rate 
		szKey.Format(_T("Torque_%s"), OPT_KEY_BAUDRATE);
		reg.WriteDWORD(szKey, m_stOption.Torque.ComPort_Torque.BaudRate);

		// ByteSize 
		szKey.Format(_T("Torque_%s"), OPT_KEY_BYTESIZE);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.Torque.ComPort_Torque.ByteSize);

		// Parity 
		szKey.Format(_T("Torque_%s"), OPT_KEY_PARITY);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.Torque.ComPort_Torque.Parity);

		// StopBits 
		szKey.Format(_T("Torque_%s"), OPT_KEY_STOPBITS);
		reg.WriteDWORD(szKey, (DWORD)m_stOption.Torque.ComPort_Torque.StopBits);

	}
	reg.Close();
}

//=============================================================================
// Method		: LoadOption_Torque
// Access		: public  
// Returns		: BOOL
// Parameter	: __out stOpt_Torque & stOption
// Qualifier	:
// Last Update	: 2018/3/8 - 8:16
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_Torque(__out stOpt_Torque& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\Torque");
	DWORD		dwValue = 0;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// 광원 ---------------------------------------
		CString szKey;
		// 통신 포트 
		szKey.Format(_T("Torque_%s"), OPT_KEY_COMPORT);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.Torque.ComPort_Torque.Port = (BYTE)dwValue;

		// Baud Rate 
		szKey.Format(_T("Torque_%s"), OPT_KEY_BAUDRATE);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.Torque.ComPort_Torque.BaudRate = dwValue;

		// ByteSize 
		szKey.Format(_T("Torque_%s"), OPT_KEY_BYTESIZE);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.Torque.ComPort_Torque.ByteSize = (BYTE)dwValue;

		// Parity 
		szKey.Format(_T("Torque_%s"), OPT_KEY_PARITY);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.Torque.ComPort_Torque.Parity = (BYTE)dwValue;

		// StopBits 
		szKey.Format(_T("Torque_%s"), OPT_KEY_STOPBITS);
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.Torque.ComPort_Torque.StopBits = (BYTE)dwValue;
	}
	else
	{
		return FALSE;
	}
	reg.Close();

	stOption = m_stOption.Torque;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_PLC
// Access		: public  
// Returns		: void
// Parameter	: __in stOpt_PLC stOption
// Qualifier	:
// Last Update	: 2017/7/7 - 11:20
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_PLC(__in stOpt_PLC stOption)
{
// 	CRegistry	reg;
// 	CString		strRegPath = m_strRegPath_Base + _T("\\PLC");
// 	CString		strValue;
// 
// 	m_stOption.PLC = stOption;
// 
// 	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
// 	{
// 		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
// 	}
// 
// 	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
// 	{
// 		// PLC 사용여부
// 		//reg.WriteDWORD(OPT_KEY_PLC_USE, m_stOption.PLC.UsePLC);
// 
// 		// PLC 주소
// 		reg.WriteDWORD(OPT_KEY_IP_ADDRESS, m_stOption.PLC.Address.dwAddress);
// 		reg.WriteDWORD(OPT_KEY_IP_PORT, m_stOption.PLC.Address.dwPort);
// 		
// 		// NIC IP Address
// 		reg.WriteDWORD(OPT_KEY_NIC_IP_ADDRESS, m_stOption.PLC.dwNIC_Address);
// 
// 		// PLC 모니터링 주기
// 		reg.WriteDWORD(OPT_KEY_PLC_MONITOR_CYCLE, m_stOption.PLC.MonitorCycle);
// 
// 		// Device Code
// 		reg.WriteString(OPT_KEY_PLC_DEVICE_CODE, m_stOption.PLC.DeviceCode);
// 
// 		// 읽기용 Head Device
// 		reg.WriteDWORD(OPT_KEY_PLC_HEAD_DEVICE_READ, m_stOption.PLC.HeadDevice_Read);
// 
// 		// 쓰기용 Head Device
// 		reg.WriteDWORD(OPT_KEY_PLC_HEAD_DEVICE_WRITE, m_stOption.PLC.HeadDevice_Write);
// 
// 	}
// 
// 	reg.Close();
}

//=============================================================================
// Method		: LoadOption_PLC
// Access		: public  
// Returns		: BOOL
// Parameter	: __out stOpt_PLC & stOption
// Qualifier	:
// Last Update	: 2017/7/7 - 11:20
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_PLC(__out stOpt_PLC& stOption)
{
// 	CRegistry	reg;
// 	CString		strRegPath = m_strRegPath_Base + _T("\\PLC");
// 	DWORD		dwValue = 0;
// 	CString		strValue;
// 
// 	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
// 	{
// 		// PLC 사용여부		
// // 		if (reg.ReadDWORD(OPT_KEY_PLC_USE, dwValue))
// // 			m_stOption.PLC.UsePLC = dwValue;
// 
// 		// PLC 주소
// 		if (reg.ReadDWORD(OPT_KEY_IP_ADDRESS, dwValue))
// 			m_stOption.PLC.Address.dwAddress = dwValue;
// 		if (reg.ReadDWORD(OPT_KEY_IP_PORT, dwValue))
// 			m_stOption.PLC.Address.dwPort = dwValue;
// 
// 		// NIC IP Address
// 		if (reg.ReadDWORD(OPT_KEY_NIC_IP_ADDRESS, dwValue))
// 			m_stOption.PLC.dwNIC_Address = dwValue;
// 
// 		// PLC 모니터링 주기
// 		if (reg.ReadDWORD(OPT_KEY_PLC_MONITOR_CYCLE, dwValue))
// 			m_stOption.PLC.MonitorCycle = dwValue;
// 
// 		// Device Code
// 		if (reg.ReadString(OPT_KEY_PLC_DEVICE_CODE, strValue))
// 			m_stOption.PLC.DeviceCode = strValue;
// 
// 		// 읽기용 Head Device
// 		if (reg.ReadDWORD(OPT_KEY_PLC_HEAD_DEVICE_READ, dwValue))
// 			m_stOption.PLC.HeadDevice_Read = dwValue;
// 
// 		// 쓰기용 Head Device
// 		if (reg.ReadDWORD(OPT_KEY_PLC_HEAD_DEVICE_WRITE, dwValue))
// 			m_stOption.PLC.HeadDevice_Write = dwValue;
// 
// 	}
// 	else
// 	{
// 		return FALSE;
// 	}
// 
// 	reg.Close();
// 
// 	stOption = m_stOption.PLC;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_Misc
// Access		: public  
// Returns		: void
// Parameter	: stOpt_Misc stOption
// Qualifier	:
// Last Update	: 2016/9/26 - 16:12
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_Misc(stOpt_Misc stOption)
{
// 	CRegistry	reg;
// 	CString		strRegPath = m_strRegPath_Base + _T("\\Misc");
// 	CString		strValue;
// 
// 	m_stOption.Misc = stOption;
// 
// 	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
// 	{
// 		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
// 	}
// 
// 	CString szkey;
// 	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
// 	{
// 
// 	}
// 
// 	reg.Close();
}

//=============================================================================
// Method		: LoadOption_Misc
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_Misc & stOption
// Qualifier	:
// Last Update	: 2016/9/26 - 16:12
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_Misc(stOpt_Misc& stOption)
{
// 	CRegistry	reg;
// 	CString		strRegPath = m_strRegPath_Base + _T("\\Misc");
// 	DWORD		dwValue = 0;
// 	CString		strValue;
// 
// 	CString szkey;
// 	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
// 	{
// 
// 	}
// 	else
// 	{
// 		return FALSE;
// 	}
// 
// 	reg.Close();
// 
// 	stOption = m_stOption.Misc;

	return TRUE;
}

//=============================================================================
// Method		: SaveOption_HT
// Access		: public  
// Returns		: void
// Parameter	: stOpt_HT stOption
// Qualifier	:
// Last Update	: 2016/9/26 - 16:27
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_HT(stOpt_HT stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\FCM30");
	CString		strValue;

	m_stOption.FCM30 = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		CString szKey;

		// 		// 서버 구동 설정
		// 		szKey.Format(_T("Server_IP"));
		// 		reg.WriteDWORD(szKey, m_stOption.FCM30.ServerAddr.dwAddress);
		// 
		// 		szKey.Format(_T("Server_Port"));
		// 		reg.WriteDWORD(szKey, m_stOption.FCM30.ServerAddr.dwPort);
		// 
		// 		// 클라이언트 주소 (핸들러)
		// 		reg.WriteDWORD(_T("Handler_IP"), m_stOption.FCM30.HandlerAddr.dwAddress);

		// 서버 주소 (테스터 1~3)
		BYTE nParaIndex = 'A';

		// IP Address 
		szKey.Format(_T("Client_IP"));
		reg.WriteDWORD(szKey, m_stOption.FCM30.ServerAddr.dwAddress);

		// Port
		szKey.Format(_T("Client_Port"));
		reg.WriteDWORD(szKey, m_stOption.FCM30.ServerAddr.dwPort);

		reg.WriteDWORD(OPT_KEY_NIC_IP_ADDRESS, m_stOption.FCM30.dwNIC_Address);

		// Ping 주기 (1초)
		reg.WriteDWORD(_T("PingCycle"), m_stOption.FCM30.dwPingCycle);

		// Ping Ack Timeout 시간 (3분간 응답 대기)
		reg.WriteDWORD(_T("PingAckTimeout"), m_stOption.FCM30.dwPinigAckTimeout);

		// Ack Timeout
		reg.WriteDWORD(_T("AckTimeout"), m_stOption.FCM30.dwAckTimeout);
	}

	reg.Close();

	return;
}

//=============================================================================
// Method		: LoadOption_HT
// Access		: public  
// Returns		: BOOL
// Parameter	: stOpt_HT & stOption
// Qualifier	:
// Last Update	: 2016/9/26 - 17:33
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_HT(stOpt_HT& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\FCM30");
	DWORD		dwValue = 0;
	CString		strValue;

	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		CString szKey;
		CStringA szIp;

		// 		// 서버 구동 설정
		// 		szKey.Format(_T("Server_IP"));
		// 		if (reg.ReadDWORD(szKey, dwValue))
		// 		{
		// 			m_stOption.FCM30.ServerAddr.dwAddress = dwValue;
		// 		}
		// 		else
		// 		{
		// 			//DWORD dwIP = bytes[0] + (bytes[1] << 8) + (bytes[2] << 16) + (bytes[3] << 24);
		// 			dwValue = 192 + (168 << 8) + (0 << 16) + (200 << 24);
		// 			m_stOption.FCM30.ServerAddr.dwAddress = dwValue;
		// 		}
		// 
		// 		szKey.Format(_T("Server_Port"));
		// 		if (reg.ReadDWORD(szKey, dwValue))
		// 			m_stOption.FCM30.ServerAddr.dwPort = dwValue;
		// 		else
		// 			m_stOption.FCM30.ServerAddr.dwPort = 5001;
		// 
		// 		szKey.Format(_T("Handler_IP"));
		// 		if (reg.ReadDWORD(szKey, dwValue))
		// 		{
		// 			m_stOption.FCM30.HandlerAddr.dwAddress = dwValue;
		// 		}
		// 		else
		// 		{
		// 			//DWORD dwIP = bytes[0] + (bytes[1] << 8) + (bytes[2] << 16) + (bytes[3] << 24);
		// 			dwValue = 192 + (168 << 8) + (0 << 16) + (200 << 24);
		// 			m_stOption.FCM30.HandlerAddr.dwAddress = dwValue;
		// 		}

		// 서버 주소 (테스터 1~3)
		BYTE nParaIndex = 'A';

		// IP Address (4개씩 같은 IP 주소 사용)
		szKey.Format(_T("Client_IP"));
		if (reg.ReadDWORD(szKey, dwValue))
		{
			m_stOption.FCM30.ServerAddr.dwAddress = dwValue;
		}
		else
		{
			dwValue = 192 + (168 << 8) + (0 << 16) + (200 << 24);
			m_stOption.FCM30.ServerAddr.dwAddress = dwValue;
		}

		// Port
		szKey.Format(_T("Client_Port"));
		if (reg.ReadDWORD(szKey, dwValue))
			m_stOption.FCM30.ServerAddr.dwPort = dwValue;
		else
			m_stOption.FCM30.ServerAddr.dwPort = 5001;

		// NIC IP Address
		if (reg.ReadDWORD(OPT_KEY_NIC_IP_ADDRESS, dwValue))
			m_stOption.FCM30.dwNIC_Address = dwValue;

		// Ping 주기 (1초)
		if (reg.ReadDWORD(_T("PingCycle"), dwValue))
			m_stOption.FCM30.dwPingCycle = dwValue;

		// Ping Ack Timeout 시간 (3분간 응답 대기)
		if (reg.ReadDWORD(_T("PingAckTimeout"), dwValue))
			m_stOption.FCM30.dwPinigAckTimeout = dwValue;

		// Ack Timeout
		if (reg.ReadDWORD(_T("AckTimeout"), dwValue))
			m_stOption.FCM30.dwAckTimeout = dwValue;
	}
	else
	{
		return FALSE;
	}

	reg.Close();

	stOption = m_stOption.FCM30;

	return TRUE;

}

void CLT_Option::SaveOption_VisionCam(__in stOpt_Ethernet stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\VisionCam");
	CString		strValue;

	m_stOption.VisionCam = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	CString strKey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Vision Camera 1
		strKey.Format(_T("Cam1_%s"), OPT_KEY_IP_ADDRESS);
		reg.WriteDWORD(strKey, m_stOption.VisionCam.dwAddress);

		// Image 경로
		//reg.WriteString(_T("Path_VImage"), m_stOption.VisionCam.szPath_VImage);

	}

	reg.Close();
}

BOOL CLT_Option::LoadOption_VisionCam(__out stOpt_Ethernet& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\VisionCam");
	DWORD		dwValue = 0;
	CString		strValue;

	CString strKey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Vision Camera 1
		strKey.Format(_T("Cam1_%s"), OPT_KEY_IP_ADDRESS);
		if (reg.ReadDWORD(strKey, dwValue))
			m_stOption.VisionCam.dwAddress = dwValue;

		// Image 경로
		//reg.ReadString(_T("Path_VImage"), m_stOption.VisionCam.szPath_VImage);

	}
	else
	{
		return FALSE;
	}

	reg.Close();

	stOption = m_stOption.VisionCam;
	return TRUE;
}

void CLT_Option::SaveOption_TCP_IP(__in stOpt_Ethernet stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\TCP_IP");
	CString		strValue;

	m_stOption.TCPIP = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	CString strKey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Vision Camera 1
		strKey.Format(_T("Cam1_%s"), OPT_KEY_IP_ADDRESS);
		reg.WriteDWORD(strKey, m_stOption.TCPIP.dwAddress);

		// Image 경로
		//reg.WriteString(_T("Path_VImage"), m_stOption.VisionCam.szPath_VImage);

	}

	reg.Close();
}

BOOL CLT_Option::LoadOption_TCP_IP(__out stOpt_Ethernet& stOption)
{
	CRegistry	reg;
	CString		strRegPath = m_strRegPath_Base + _T("\\TCP_IP");
	CString		strValue;

	m_stOption.TCPIP = stOption;

	if (!reg.VerifyKey(HKEY_CURRENT_USER, strRegPath))
	{
		reg.CreateKey(HKEY_CURRENT_USER, strRegPath);
	}

	CString strKey;
	if (reg.Open(HKEY_CURRENT_USER, strRegPath))
	{
		// Vision Camera 1
		strKey.Format(_T("Cam1_%s"), OPT_KEY_IP_ADDRESS);
		reg.WriteDWORD(strKey, m_stOption.TCPIP.dwAddress);

		// Image 경로
		//reg.WriteString(_T("Path_VImage"), m_stOption.VisionCam.szPath_VImage);

	}

	reg.Close();
	return TRUE;
}

//=============================================================================
// Method		: SaveOption_All
// Access		: public  
// Returns		: void
// Parameter	: stLT_Option stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 16:01
// Desc.		:
//=============================================================================
void CLT_Option::SaveOption_All( stLT_Option stOption )
{
	SaveOption_Inspector	(stOption.Inspector);
	if (Sys_Focusing == m_InsptrType)
	{
		SaveOption_BCR			(stOption.BCR);
	}
	SaveOption_MES			(stOption.MES);
	SaveOption_PCB			(stOption.PCB);
	//SaveOption_PLC			(stOption.PLC);
	SaveOption_Misc			(stOption.Misc);
	SaveOption_Torque		(stOption.Torque);
	SaveOption_VisionCam	(stOption.VisionCam);
	SaveOption_TCP_IP		(stOption.TCPIP);

	//SaveOption_HT			(stOption.HandlerTester);
}

//=============================================================================
// Method		: LoadOption_All
// Access		: public  
// Returns		: BOOL
// Parameter	: stLT_Option & stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 16:00
// Desc.		:
//=============================================================================
BOOL CLT_Option::LoadOption_All( stLT_Option& stOption )
{
	BOOL bReturn = TRUE;

	bReturn &= LoadOption_Inspector		(stOption.Inspector);
	bReturn &= LoadOption_MES			(stOption.MES);
	if (Sys_Focusing == m_InsptrType)
	{
		bReturn &= LoadOption_BCR			(stOption.BCR);
	}
	bReturn &= LoadOption_PCB			(stOption.PCB);
	//bReturn &= LoadOption_PLC			(stOption.PLC);
	bReturn &= LoadOption_Misc			(stOption.Misc);
	bReturn &= LoadOption_Torque		(stOption.Torque);
	bReturn &= LoadOption_VisionCam		(stOption.VisionCam);
	bReturn &= LoadOption_TCP_IP		(stOption.TCPIP);

	//bReturn &= LoadOption_HT			(stOption.HandlerTester);

	return bReturn;
}

//=============================================================================
// Method		: SetInspectorType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nInsptrType
// Qualifier	:
// Last Update	: 2016/9/26 - 18:01
// Desc.		:
//=============================================================================
void CLT_Option::SetInspectorType(__in enInsptrSysType nInsptrType)
{
	m_InsptrType = nInsptrType;
}
