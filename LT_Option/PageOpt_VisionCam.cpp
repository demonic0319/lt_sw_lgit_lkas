//*****************************************************************************
// Filename	: PageOpt_VisionCam.cpp
// Created	: 2010/9/16
// Modified	: 2010/9/16 - 15:33
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_VisionCam.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"
#include <memory>
#include "MFCPropertyGridProperties.h"

#define ID_PROPGRID_IPADDR (21)

IMPLEMENT_DYNAMIC(CPageOpt_VisionCam, CPageOption)

//=============================================================================
// 생성자
//=============================================================================
CPageOpt_VisionCam::CPageOpt_VisionCam(void)
{
}

CPageOpt_VisionCam::CPageOpt_VisionCam(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{

}

//=============================================================================
// 소멸자
//=============================================================================
CPageOpt_VisionCam::~CPageOpt_VisionCam(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_VisionCam, CPageOption)	
END_MESSAGE_MAP()

//=============================================================================
// CPageOpt_VisionCam 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CPageOpt_VisionCam::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_VisionCam::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_VisionCam::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_VisionCam::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_VisionCam::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_VisionCam::InitPropList()
{
	CPageOption::InitPropList();

	
	//--------------------------------------------------------
	// 통신 설정
	//--------------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Comm(new CMFCPropertyGridProperty(_T("Vision Camera 통신 설정")));

	CMFCPropertyGridProperty* pProp = NULL;

	// Vision Camera 1 IP Address	
	in_addr addr;
	addr.s_addr=inet_addr("192.168.101.10");
	pProp = new CMFCPropertyGridIPAdressProperty(_T("Vision Camera 1 IP Address"), addr, OPT_DESC_IP_ADDRESS, ID_PROPGRID_IPADDR);	
	apGroup_Comm->AddSubItem(pProp);
	
	// Vision Camera 2 IP Address	
// 	addr.s_addr = inet_addr("192.168.101.10");
// 	pProp = new CMFCPropertyGridIPAdressProperty(_T("Vision Camera 2 IP Address"), addr, OPT_DESC_IP_ADDRESS, ID_PROPGRID_IPADDR);
// 	apGroup_Comm->AddSubItem(pProp);

	// Vision Cam 이미지 저장 경로
//	apGroup_Comm->AddSubItem(new CMFCPropertyGridFileProperty(_T("Image 저장 경로"), _T("C:\\VImage\\")));

	// 2번째 Vision Cam 사용여부
// 	pProp = new CMFCPropertyGridProperty(_T("Vision Cam 2 사용여부"), lpszUsableTable[1], OPT_DESC_USE_AUTO_CONN);
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_Comm->AddSubItem(pProp);

	m_wndPropList.AddProperty(apGroup_Comm.release());
}

//=============================================================================
// Method		: CPageOpt_VisionCam::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_VisionCam::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_VisionCam(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_VisionCam::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_VisionCam::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_VisionCam(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: GetOption
// Access		: protected  
// Returns		: Luritech_Option::stOpt_Ethernet
// Qualifier	:
// Last Update	: 2016/5/18 - 16:22
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_Ethernet CPageOpt_VisionCam::GetOption()
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	COleVariant rVariant;
	VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 그룹 1 
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();

	// Vision Camera 1 IP Address ---------------------------
	m_stOption.dwAddress = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue().ulVal;

	// 이미지 저장 경로
// 	USES_CONVERSION;
// 
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	m_stOption.szPath_VImage = strValue;

	// Vision Cam 2 사용여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}

	return m_stOption;
}

//=============================================================================
// Method		: SetOption
// Access		: protected  
// Returns		: void
// Parameter	: stOpt_Ethernet stOption
// Qualifier	:
// Last Update	: 2016/5/18 - 16:23
// Desc.		:
//=============================================================================
void CPageOpt_VisionCam::SetOption( stOpt_Ethernet stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	//---------------------------------------------------------------
	// 그룹 1 통신 종류
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	int iSubItemCount = pPropertyGroup->GetSubItemsCount();

	// Vision Camera 1 IP Address ----------------------
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(ULONG_VARIANT(m_stOption.dwAddress));

	// 이미지 저장 경로
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_VImage);

}
