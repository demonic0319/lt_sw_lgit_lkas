﻿//*****************************************************************************
// Filename	: PageOpt_Insp.cpp
// Created	: 2010/9/6
// Modified	: 2010/9/6 - 15:51
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "PageOpt_Insp.h"
#include "Define_OptionItem.h"
#include "Define_OptionDescription.h"

#include <memory>
#include "CustomProperties.h"

IMPLEMENT_DYNAMIC(CPageOpt_Insp, CPageOption)

//=============================================================================
//
//=============================================================================
CPageOpt_Insp::CPageOpt_Insp(LPCTSTR lpszCaption /*= NULL*/) : CPageOption (lpszCaption)
{
	
}

CPageOpt_Insp::CPageOpt_Insp(UINT nIDTemplate, UINT nIDCaption /*= 0*/) : CPageOption(nIDTemplate, nIDCaption)
{
	
}

//=============================================================================
//
//=============================================================================
CPageOpt_Insp::~CPageOpt_Insp(void)
{
}

BEGIN_MESSAGE_MAP(CPageOpt_Insp, CPageOption)	
END_MESSAGE_MAP()

// CPageOpt_Insp 메시지 처리기입니다.

//=============================================================================
// Method		: CPageOpt_Insp::AdjustLayout
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::AdjustLayout()
{
	CPageOption::AdjustLayout();
}

//=============================================================================
// Method		: CPageOpt_Insp::SetPropListFont
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SetPropListFont()
{
	CPageOption::SetPropListFont();
}

//=============================================================================
// Method		: CPageOpt_Insp::InitPropList
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/8/30 - 11:12
// Desc.		:
//=============================================================================
void CPageOpt_Insp::InitPropList()
{
	CPageOption::InitPropList();

	CMFCPropertyGridProperty* pProp = NULL;

	//-----------------------------------------------------
	// 검사기 설정
	//-----------------------------------------------------
 	std::auto_ptr<CMFCPropertyGridProperty> apGroup_System(new CMFCPropertyGridProperty(_T("System Setting")));
	
	// Area Sensor 사용 여부
// 	pProp = new CMFCPropertyGridProperty(_T("Use Area Sensor"), lpszUsableTable[0], _T("Use Area Sensor"));
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_System->AddSubItem(pProp);

	// Door Sensor 사용 여부
// 	pProp = new CMFCPropertyGridProperty(_T("Use Door Sensor"), lpszUsableTable[0], _T("Use Door Sensor"));
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_System->AddSubItem(pProp);

	// MES 사용 여부
// 	pProp = new CMFCPropertyGridProperty(_T("Use MES"), lpszUsableTable[0], _T("Use MES"));
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_System->AddSubItem(pProp);

	if (Sys_2D_Cal == m_InsptrType)
	{
		// 영상 View 윈도우 사용 여부
		pProp = new CMFCPropertyGridProperty(_T("Use Image View Window"), lpszUsableTable[0], _T("Use Image View Window"));
		pProp->AddOption(lpszUsableTable[0]);
		pProp->AddOption(lpszUsableTable[1]);
		pProp->AllowEdit(FALSE);
		apGroup_System->AddSubItem(pProp);

	}

	if (Sys_2D_Cal == m_InsptrType || SYS_IR_IMAGE_TEST == m_InsptrType || Sys_Image_Test == m_InsptrType)
	{
		// 2D CAL 암막 셔터 모터 제어 기능 사용 여부
// 		pProp = new CMFCPropertyGridProperty(_T("Use Blind Shutter"), lpszUsableTable[0], _T("Use Blind Shutter"));
// 		pProp->AddOption(lpszUsableTable[0]);
// 		pProp->AddOption(lpszUsableTable[1]);
// 		pProp->AllowEdit(FALSE);
// 		apGroup_System->AddSubItem(pProp);
	}

	// RAW 이미지 저장
	pProp = new CMFCPropertyGridProperty(_T("Use Save RAW Image"), lpszUsableTable[0], _T("Use Save RAW Image"));
	pProp->AddOption(lpszUsableTable[0]);
	pProp->AddOption(lpszUsableTable[1]);
	pProp->AllowEdit(FALSE);
	apGroup_System->AddSubItem(pProp);
	
	// Gray12 이미지 저장
// 	pProp = new CMFCPropertyGridProperty(_T("Use Save Gray12 Image"), lpszUsableTable[0], _T("Use Save Gray12 Image"));
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_System->AddSubItem(pProp);
	
	// RGB 이미지 저장
	pProp = new CMFCPropertyGridProperty(_T("Use Save RGB Image"), lpszUsableTable[0], _T("Use Save RGB Image"));
	pProp->AddOption(lpszUsableTable[0]);
	pProp->AddOption(lpszUsableTable[1]);
	pProp->AllowEdit(FALSE);
	apGroup_System->AddSubItem(pProp);


 	m_wndPropList.AddProperty(apGroup_System.release());

	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Path(new CMFCPropertyGridProperty(_T("Path Setting")));

	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Log Path"),			_T("C:\\Log"			)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Report Path"),		_T("C:\\Report"			)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Recipe Path"),		_T("C:\\Recipe"			)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Consumables Path"),	_T("C:\\Consumables"	)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Motor Path"),			_T("C:\\Motor"			)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Maintenance Path"),	_T("C:\\Maintenance"	)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Image Path"),			_T("C:\\Image"			)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("IIc Path"),			_T("C:\\IIc"			)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("IIc Image Path"),		_T("C:\\IIcImage"		)));
	apGroup_Path->AddSubItem(new CMFCPropertyGridFileProperty(_T("Vision Image Path"),	_T("C:\\VisionImage"	)));

	m_wndPropList.AddProperty(apGroup_Path.release());
	
// #ifdef _DEBUG
// 	//-----------------------------------------------------
// 	// 프로그램 운영 설정
// 	//-----------------------------------------------------
// 	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Oper(new CMFCPropertyGridProperty(_T("Operation Setting")));
// 
// 	// 자동 재 실행 사용 여부
// 	pProp = new CMFCPropertyGridProperty(_T("비정상 종료 시 자동 재 실행 사용 여부"), lpszUsableTable[0], OPT_DESC_USE_AUTO_RESTART);
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_Oper->AddSubItem(pProp);
// 
// 	// 장치 연결상태 표시 윈도우 사용여부
// 	pProp = new CMFCPropertyGridProperty(_T("장치 연결상태 표시 윈도우 사용여부"), lpszUsableTable[1], OPT_DESC_USE_DEVICE_INFO_PANE);
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_Oper->AddSubItem(pProp);
// 
// 	// 자동 통신 연결 사용 여부
// 	pProp = new CMFCPropertyGridProperty(_T("자동 통신 연결 사용 여부"), lpszUsableTable[0], OPT_DESC_USE_AUTO_CONN);
// 	pProp->AddOption(lpszUsableTable[0]);
// 	pProp->AddOption(lpszUsableTable[1]);
// 	pProp->AllowEdit(FALSE);
// 	apGroup_Oper->AddSubItem(pProp);
// 	
// 	m_wndPropList.AddProperty(apGroup_Oper.release());
// #endif

	//-----------------------------------------------------
	// 기타 설정
	//-----------------------------------------------------
	std::auto_ptr<CMFCPropertyGridProperty> apGroup_Etc(new CMFCPropertyGridProperty(_T("Etc Setting")));
	apGroup_Etc->AddSubItem(new CPasswordProp(_T("Password Administrator"), _T(""), _T("Settings Editable administrator password")));
	m_wndPropList.AddProperty(apGroup_Etc.release());
}

//=============================================================================
// Method		: CPageOpt_Insp::SaveOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SaveOption()
{
	CPageOption::SaveOption();

	m_stOption	= GetOption ();

	m_pLT_Option->SaveOption_Inspector(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_Insp::LoadOption
// Access		: virtual protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/9/6 - 15:20
// Desc.		:
//=============================================================================
void CPageOpt_Insp::LoadOption()
{
	CPageOption::LoadOption();

	if (m_pLT_Option->LoadOption_Inspector(m_stOption))
		SetOption(m_stOption);
}

//=============================================================================
// Method		: CPageOpt_Insp::GetOption
// Access		: protected 
// Returns		: Luritech_Option::stOption_Inspector
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
Luritech_Option::stOpt_Insp CPageOpt_Insp::GetOption()
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	COleVariant rVariant;
	VARIANT		varData;
	CString		strValue;

	//---------------------------------------------------------------
	// 기본 검사 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

	USES_CONVERSION;

 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
 	nSubItemIndex = 0;

	// Area Sensor 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.bUseAreaSensor_Err = (BOOL)nIndex;

	// Door Sensor 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.bUseDoorOpen_Err = (BOOL)nIndex;

	// MES 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.bUseMES = (BOOL)nIndex;

	if (Sys_2D_Cal == m_InsptrType)
	{
		// 영상 View 윈도우 사용 여부
		rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
		varData = rVariant.Detach();
		ASSERT(varData.vt == VT_BSTR);
		strValue = OLE2A(varData.bstrVal);

		for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
		{
			if (lpszUsableTable[nIndex] == strValue)
				break;
		}
		m_stOption.bUseImageViewWnd = (BOOL)nIndex;

	}

	if (Sys_2D_Cal == m_InsptrType || Sys_IR_Image_Test == m_InsptrType || Sys_Image_Test == m_InsptrType)
	{
		// 2D CAL 암막 셔터 모터 제어 기능 사용 여부
// 		rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 		varData = rVariant.Detach();
// 		ASSERT(varData.vt == VT_BSTR);
// 		strValue = OLE2A(varData.bstrVal);
// 
// 		for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 		{
// 			if (lpszUsableTable[nIndex] == strValue)
// 				break;
// 		}
// 		m_stOption.bUseBlindShutter = (BOOL)nIndex;
	}

	// RAW 이미지 저장
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);

	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	{
		if (lpszUsableTable[nIndex] == strValue)
			break;
	}
	m_stOption.bSaveImage_RAW = (BOOL)nIndex;
	
	// Gray12 이미지 저장
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.bSaveImage_Gray12 = (BOOL)nIndex;
	
	// RGB 이미지 저장
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);

	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
	{
		if (lpszUsableTable[nIndex] == strValue)
			break;
	}
	m_stOption.bSaveImage_RGB = (BOOL)nIndex;

	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Log = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Report = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Recipe = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Consumables = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Motor = strValue;
	
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Maintenance = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_Image = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_I2c = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_I2cImage = strValue;

	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData = rVariant.Detach();
	ASSERT(varData.vt == VT_BSTR);
	strValue = OLE2A(varData.bstrVal);
	m_stOption.szPath_VisionImage = strValue;

// #ifdef _DEBUG
// 	//---------------------------------------------------------------
// 	// 프로그램 운영 설정
// 	//---------------------------------------------------------------
// 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
// 	nSubItemIndex = 0;
// 
// 	// 자동 재 실행 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseAutoRestart = (BOOL)nIndex;
// 
// 	// 장치 연결상태 표시 윈도우 사용여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseDeviceInfoPane = (BOOL)nIndex;
// 
// 	// 자동 통신 연결 사용 여부
// 	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
// 	varData = rVariant.Detach();
// 	ASSERT(varData.vt == VT_BSTR);
// 	strValue = OLE2A(varData.bstrVal);
// 	for (nIndex = 0; NULL != lpszUsableTable[nIndex]; nIndex++)
// 	{
// 		if (lpszUsableTable[nIndex] == strValue)
// 			break;
// 	}
// 	m_stOption.UseAutoConnection = (BOOL)nIndex;
// 
// #endif

	//---------------------------------------------------------------
	// 기타 설정
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Password -----------------------------
	rVariant = (pPropertyGroup->GetSubItem(nSubItemIndex++))->GetValue();
	varData	 = rVariant.Detach();
	ASSERT (varData.vt == VT_BSTR);	
	strValue = OLE2A(varData.bstrVal);

	m_stOption.Password_Admin = strValue;

	return m_stOption;
}

//=============================================================================
// Method		: CPageOpt_Insp::SetOption
// Access		: protected 
// Returns		: void
// Parameter	: stOption_Inspector stOption
// Qualifier	:
// Last Update	: 2010/9/10 - 16:07
// Desc.		:
//=============================================================================
void CPageOpt_Insp::SetOption( stOpt_Insp stOption )
{
	UINT nGroupIndex	= 0;
	UINT nSubItemIndex	= 0;
	UINT nIndex			= 0;

	//---------------------------------------------------------------
	// 기본 검사 설정
	//---------------------------------------------------------------
	int iCount = m_wndPropList.GetPropertyCount();
	CMFCPropertyGridProperty* pPropertyGroup = NULL;
	int iSubItemCount = 0;

 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
 	nSubItemIndex = 0;

	// Area Sensor 사용 여부
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseAreaSensor_Err]);

	// Door Sensor 사용 여부
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseDoorOpen_Err]);

	// MES 사용 여부
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseMES]);

	if (Sys_2D_Cal == m_InsptrType)
	{
		// 영상 View 윈도우 사용 여부
		(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseImageViewWnd]);
	}

	if (Sys_2D_Cal == m_InsptrType || Sys_IR_Image_Test == m_InsptrType || Sys_Image_Test == m_InsptrType)
	{
		// 2D CAL 암막 셔터 모터 제어 기능 사용 여부
		//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bUseBlindShutter]);
	}

	// RAW 이미지 저장
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bSaveImage_RAW]);
	
	// Gray12 이미지 저장
	//(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bSaveImage_Gray12]);
	
	// RGB 이미지 저장
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.bSaveImage_RGB]);

	//-----------------------------------------------------
	// 경로 설정
	//-----------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Log);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Report);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Recipe);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Consumables);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Motor);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Maintenance);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_Image);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_I2c);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_I2cImage);
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.szPath_VisionImage);

// #ifdef _DEBUG
// 	//---------------------------------------------------------------
// 	// 프로그램 운영 설정
// 	//---------------------------------------------------------------
// 	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
// 	iSubItemCount = pPropertyGroup->GetSubItemsCount();
// 	nSubItemIndex = 0;
// 
// 	// 자동 재 실행 사용 여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseAutoRestart]);
// 	// 장치 연결상태 표시 윈도우 사용여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseDeviceInfoPane]);
// 	// 자동 통신 연결 사용 여부
// 	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(lpszUsableTable[m_stOption.UseAutoConnection]);
// #endif
	//---------------------------------------------------------------
	// 기타 설정
	//---------------------------------------------------------------
	pPropertyGroup = m_wndPropList.GetProperty(nGroupIndex++);
	iSubItemCount = pPropertyGroup->GetSubItemsCount();
	nSubItemIndex = 0;

	// Password -----------------------------
	(pPropertyGroup->GetSubItem(nSubItemIndex++))->SetValue(m_stOption.Password_Admin);
}