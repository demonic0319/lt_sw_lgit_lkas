﻿//*****************************************************************************
// Filename	: Define_Option.h
// Created	: 2015/12/16
// Modified	: 2016/05/18
//
// Author	: PiRing
//	
// Purpose	: 옵션 관련 설정값 선언 파일
//*****************************************************************************
#ifndef Define_Option_h__
#define Define_Option_h__

#pragma pack (push,1)

#include "Define_OptionItem.h"
#include "Def_Enum_Cm.h"
#include <afxwin.h>

namespace Luritech_Option
{
	//---------------------------------
	// Option Base
	//---------------------------------
	typedef struct _stOption_Base
	{
		_stOption_Base()
		{
			Reset();
		};

		virtual void Reset()
		{
		};

		_stOption_Base& operator= (_stOption_Base& ref)
		{
			return *this;
		};
	}stOption_Base;

	//---------------------------------
	// Serial 통신
	//---------------------------------
	typedef struct _stOption_Serial : public stOption_Base
	{
		BYTE	Port;
		DWORD	BaudRate;	/* Baudrate at which running       */
		BYTE	ByteSize;	/* Number of bits/byte, 4-8        */
		BYTE	Parity;		/* 0-4=None,Odd,Even,Mark,Space    */
		BYTE	StopBits;	/* 0,1,2 = 1, 1.5, 2               */

		_stOption_Serial ()
		{
			Reset ();
		};

		void Reset ()
		{
			Port		= 1;
			BaudRate	= CBR_115200;
			ByteSize	= 8;
			Parity		= NOPARITY;
			StopBits	= ONESTOPBIT;
		};

		_stOption_Serial& operator= (_stOption_Serial& ref)
		{
			Port		= ref.Port;
			BaudRate	= ref.BaudRate;
			ByteSize	= ref.ByteSize;
			Parity		= ref.Parity;
			StopBits	= ref.StopBits;

			return *this;
		};
	}stOpt_Serial;

	//---------------------------------
	// 이더넷 통신
	//---------------------------------
	typedef struct _stOpt_Ethernet : public stOption_Base
	{
		DWORD	dwAddress;
		DWORD	dwPort;

		_stOpt_Ethernet ()
		{
			Reset ();
		};

		void Reset ()
		{
			dwAddress		= 0;
			dwPort			= 0;
		};

		_stOpt_Ethernet& operator= (_stOpt_Ethernet& ref)
		{
			dwAddress		= ref.dwAddress;
			dwPort			= ref.dwPort;

			return *this;
		};
	}stOpt_Ethernet;
	
	//---------------------------------
	// 기타
	//---------------------------------
	typedef struct _stOpt_Misc : public stOption_Base
	{
		_stOpt_Misc ()
		{
			Reset ();
		};

		void Reset ()
		{
			
		};

		_stOpt_Misc& operator= (_stOpt_Misc& ref)
		{
			return *this;
		};
	}stOpt_Misc;

	//---------------------------------
	// PLC
	//---------------------------------	
	typedef struct _stOpt_PLC
	{	
		BOOL				UsePLC;

		stOpt_Ethernet		Address;
		DWORD				MonitorCycle;
		CString				DeviceCode;
		DWORD				HeadDevice_Read;
		DWORD				HeadDevice_Write;

		DWORD				dwNIC_Address;

		_stOpt_PLC ()
		{
			Reset ();
		};

		void Reset ()
		{
			UsePLC				= FALSE;

			Address.Reset ();
			Address.dwPort		= 2004;
			MonitorCycle		= 500;
			DeviceCode			= _T("D");
			HeadDevice_Read		= 50;
			HeadDevice_Write	= 700;

			dwNIC_Address		= 0;
		};

		_stOpt_PLC& operator= (_stOpt_PLC& ref)
		{
			UsePLC				= ref.UsePLC;
			Address				= ref.Address;
			MonitorCycle		= ref.MonitorCycle;
			DeviceCode			= ref.DeviceCode;
			HeadDevice_Read		= ref.HeadDevice_Read;
			HeadDevice_Write	= ref.HeadDevice_Write;
			dwNIC_Address		= ref.dwNIC_Address;

			return *this;
		};
	}stOpt_PLC;

	//---------------------------------
	// MES
	//---------------------------------	
	typedef struct _stOpt_MES : public stOption_Base
	{
		CString			szEquipmentID;
		stOpt_Ethernet	Address;
		CString			szPath_MESLog;		// MES Log 경로

		DWORD	dwNIC_Address;

		_stOpt_MES()
		{
			Address.Reset();
			szPath_MESLog = _T("C:\\MES\\");
		};

		void Reset()
		{
			szEquipmentID.Empty();
			Address.Reset();
			szPath_MESLog.Empty();

			dwNIC_Address	= 0;
		};

		_stOpt_MES& operator= (_stOpt_MES& ref)
		{
			szEquipmentID	= ref.szEquipmentID;
			Address			= ref.Address;
			szPath_MESLog	= ref.szPath_MESLog;

			dwNIC_Address	= ref.dwNIC_Address;

			return *this;
		};
	}stOpt_MES;
	
	//---------------------------------
	// Barcode Reader & Motor
	//---------------------------------	
	typedef struct _stOpt_BCR : public stOption_Base
	{	
		stOpt_Serial	Handy_ComPort;
		stOpt_Serial	Fixed_ComPort;

		_stOpt_BCR()
		{
			Reset ();
		};

		void Reset ()
		{
			Handy_ComPort.Reset();
			Fixed_ComPort.Reset();
		};

		_stOpt_BCR& operator= (_stOpt_BCR& ref)
		{
			Handy_ComPort	= ref.Handy_ComPort;
			Fixed_ComPort	= ref.Fixed_ComPort;

			return *this;
		};
	}stOpt_BCR;

	//---------------------------------
	// 보드
	//---------------------------------
	typedef struct _stOpt_PCB : public stOption_Base
	{
		stOpt_Serial	ComPort_CtrlBrd[MAX_MODULE_CNT];
		stOpt_Serial	ComPort_LightBrd[MAX_LIGHT_BRD_CNT];
		stOpt_Serial	ComPort_LightPSU[MAX_LIGHT_BRD_CNT];
		
		stOpt_Serial	ComPort_Displace;

		_stOpt_PCB()
		{
			Reset();
		};

		void Reset()
		{
			for (UINT nIdx = 0; nIdx < MAX_MODULE_CNT; nIdx++)
			{
				ComPort_CtrlBrd[nIdx].Reset();
			}

			for (UINT nIdx = 0; nIdx < MAX_LIGHT_BRD_CNT; nIdx++)
			{
				ComPort_LightBrd[nIdx].Reset();
				ComPort_LightPSU[nIdx].Reset();
			}

			ComPort_Displace.Reset();
		};

		_stOpt_PCB& operator= (_stOpt_PCB& ref)
		{
			for (UINT nIdx = 0; nIdx < MAX_MODULE_CNT; nIdx++)
			{
				ComPort_CtrlBrd[nIdx]	= ref.ComPort_CtrlBrd[nIdx];
			}

			for (UINT nIdx = 0; nIdx < MAX_LIGHT_BRD_CNT; nIdx++)
			{
				ComPort_LightBrd[nIdx]	= ref.ComPort_LightBrd[nIdx];
				ComPort_LightPSU[nIdx]	= ref.ComPort_LightPSU[nIdx];
			}

			ComPort_Displace = ref.ComPort_Displace;

			return *this;
		};
	}stOpt_PCB;

	//---------------------------------
	// 토크 드라이버
	//---------------------------------
	typedef struct _stOpt_Torque : public stOption_Base
	{
		stOpt_Serial	ComPort_Torque;

		_stOpt_Torque()
		{
			Reset();
		};

		void Reset()
		{
			ComPort_Torque.Reset();
		};

		_stOpt_Torque& operator= (_stOpt_Torque& ref)
		{
			ComPort_Torque = ref.ComPort_Torque;

			return *this;
		};
	}stOpt_Torque;

	//---------------------------------
	// 검사기 설정
	//---------------------------------
	typedef struct _stOption_Inspector : public stOption_Base
	{
		BOOL		bUseAreaSensor_Err;	// Area 센서 체크 기능 사용여부
		BOOL		bUseDoorOpen_Err;	// Door 센서 체크 기능 사용여부
		BOOL		bUseMES;			// MES 사용여부
		BOOL		bUseImageViewWnd;	// 2D CAL 메인화면 영상 출력 기능 사용 여부
		BOOL		bUseBlindShutter;	// 2D CAL 암막 셔터 모터 제어 기능 사용 여부

		BOOL		bUseCreateDumpFile;	// StereoCAL EEPROM Dump File 생성 여부
		
		BOOL		bSaveImage_RAW;		// RAW 이미지 저장
		BOOL		bSaveImage_Gray12;	// Gray12 이미지 저장
		BOOL		bSaveImage_RGB;		// RGB 이미지 저장

		// 저장 경로
		CString		szPath_Log;
		CString		szPath_Report;
		CString		szPath_Recipe;
		CString		szPath_Consumables;
		CString		szPath_Motor;
		CString		szPath_Maintenance;
		CString		szPath_Image;
		CString		szPath_I2c;
		CString		szPath_I2cImage;
		CString		szPath_VisionImage;
		
		// 자동 재 실행 사용 여부
		BOOL		UseAutoRestart;
		// 모델 설정 화면 잠금 기능
		BOOL		UseDeviceInfoPane;
		// 통신 자동 연결
		BOOL		UseAutoConnection;

		//*** 기타 ***//
		CString		Password_Admin;
		//CString		Password_ReadOnly;

		_stOption_Inspector ()
		{
			bUseAreaSensor_Err	= TRUE;
			bUseDoorOpen_Err	= TRUE;
			bUseMES				= FALSE;
			bUseImageViewWnd	= TRUE;
			bUseBlindShutter	= TRUE;
			bUseCreateDumpFile	= TRUE;

			bSaveImage_RAW		= FALSE;
			bSaveImage_Gray12	= FALSE;
			bSaveImage_RGB		= FALSE;

			szPath_Log			= _T("C:\\Log");
			szPath_Report		= _T("C:\\Report");
			szPath_Recipe		= _T("C:\\Recipe");
			szPath_Consumables	= _T("C:\\Consumables");
			szPath_Motor		= _T("C:\\Motor");
			szPath_Maintenance	= _T("C:\\Maintenance");
			szPath_Image		= _T("C:\\Image");
			szPath_I2c			= _T("C:\\I2c");
			szPath_I2cImage		= _T("C:\\I2cImage");
			szPath_VisionImage	= _T("C:\\VisionImage");
			
			UseDeviceInfoPane	= TRUE;
			UseAutoConnection	= FALSE;
			UseAutoRestart		= FALSE;
		};

		void Reset ()
		{
			bUseAreaSensor_Err	= TRUE;
			bUseDoorOpen_Err	= TRUE;
			bUseMES				= FALSE;
			bUseImageViewWnd	= TRUE;
			bUseBlindShutter	= TRUE;
			bUseCreateDumpFile	= TRUE;

			bSaveImage_RAW		= FALSE;
			bSaveImage_Gray12	= FALSE;
			bSaveImage_RGB		= FALSE;

			szPath_Log.Empty();
			szPath_Report.Empty();
			szPath_Recipe.Empty();
			szPath_Consumables.Empty();
			szPath_Motor.Empty();
			szPath_Maintenance.Empty();
			szPath_Image.Empty();
			szPath_I2c.Empty();
			szPath_I2cImage.Empty();
			szPath_VisionImage.Empty();

			UseDeviceInfoPane	= TRUE;
			UseAutoConnection	= FALSE;
			UseAutoRestart		= FALSE;

			Password_Admin.Empty();
			//Password_ReadOnly.Empty();
		};

		_stOption_Inspector& operator= (_stOption_Inspector& ref)
		{
			bUseAreaSensor_Err		= ref.bUseAreaSensor_Err;
			bUseDoorOpen_Err		= ref.bUseDoorOpen_Err;
			bUseMES					= ref.bUseMES;
			bUseImageViewWnd		= ref.bUseImageViewWnd;
			bUseBlindShutter		= ref.bUseBlindShutter;
			bUseCreateDumpFile		= ref.bUseCreateDumpFile;

			bSaveImage_RAW			= ref.bSaveImage_RAW;
			bSaveImage_Gray12		= ref.bSaveImage_Gray12;
			bSaveImage_RGB			= ref.bSaveImage_RGB;

			szPath_Log				= ref.szPath_Log;
			szPath_Report			= ref.szPath_Report;
			szPath_Recipe			= ref.szPath_Recipe;
			szPath_Consumables		= ref.szPath_Consumables;
			szPath_Motor			= ref.szPath_Motor;
			szPath_Maintenance		= ref.szPath_Maintenance;
			szPath_Image			= ref.szPath_Image;
			szPath_I2c				= ref.szPath_I2c;
			szPath_I2cImage			= ref.szPath_I2cImage;
			szPath_VisionImage		= ref.szPath_VisionImage;

			UseAutoRestart			= ref.UseAutoRestart;
			UseDeviceInfoPane		= ref.UseDeviceInfoPane;
			UseAutoConnection		= ref.UseAutoConnection;

			Password_Admin			= ref.Password_Admin;
			//Password_ReadOnly		= ref.Password_ReadOnly;

			return *this;
		};
	}stOpt_Insp;
	
	typedef struct _stOpt_HT : public stOption_Base
	{
		// 서버 구동 설정 (Tester server)
		stOpt_Ethernet	ServerAddr;
		
		stOpt_Ethernet	TesterAddr;

		// 클라이언트 주소
		stOpt_Ethernet	HandlerAddr;


		DWORD	dwNIC_Address;


		// Ping 주기 ms (1초)
		DWORD	dwPingCycle;
		// Ping Ack Timeout 시간 (초)(기본 : 3분간 응답 대기)
		DWORD	dwPinigAckTimeout;
		// Ack Timeout (초)
		DWORD	dwAckTimeout;

		_stOpt_HT()
		{
			ServerAddr.Reset();
			TesterAddr.Reset();

			HandlerAddr.Reset();

			dwPingCycle			= 1000;
			dwPinigAckTimeout	= 180;
			dwAckTimeout		= 180000;
		};

		void Reset()
		{
			ServerAddr.Reset();
			TesterAddr.Reset();
			HandlerAddr.Reset();

			dwPingCycle			= 1000;
			dwPinigAckTimeout	= 180;
			dwAckTimeout		= 180;


			dwNIC_Address = 0;
		};

		_stOpt_HT& operator= (_stOpt_HT& ref)
		{
			ServerAddr			= ref.ServerAddr;
			TesterAddr			= ref.TesterAddr;
			HandlerAddr			= ref.HandlerAddr;

			dwPingCycle			= ref.dwPingCycle;
			dwPinigAckTimeout	= ref.dwPinigAckTimeout;
			dwAckTimeout		= ref.dwAckTimeout;

			dwNIC_Address		= ref.dwNIC_Address;
			return *this;
		};
	}stOpt_HT;

	//-----------------------------------------------------
	// 통합 검사기용
	//-----------------------------------------------------
	typedef struct _stLT_Option
	{
		stOpt_Insp			Inspector;
		stOpt_HT			FCM30;
		stOpt_BCR			BCR;
		stOpt_MES			MES;
		stOpt_PCB			PCB;
		stOpt_Misc			Misc;
		stOpt_Torque		Torque;
		stOpt_Ethernet		VisionCam;
		stOpt_Ethernet		TCPIP;

		_stLT_Option ()
		{
			Reset ();
		};

		void Reset ()
		{
			Inspector.Reset();
			BCR.Reset();
			MES.Reset();
			PCB.Reset();
			Misc.Reset();
			Torque.Reset();
			VisionCam.Reset();
			TCPIP.Reset();
			FCM30.Reset();
		};

		_stLT_Option& operator= (_stLT_Option& ref)
		{
			Inspector		= ref.Inspector;
			BCR				= ref.BCR;
			MES				= ref.MES;
			PCB				= ref.PCB;
			Misc			= ref.Misc;
			Torque			= ref.Torque;
			VisionCam		= ref.VisionCam;
			TCPIP			= ref.TCPIP;
			FCM30			= ref.FCM30;

			return *this;
		}
	}stLT_Option;

	
	typedef enum
	{
		OPT_INSPECTOR		= 0,
		OPT_BCR,
		OPT_MES,
		OPT_PCB,
		OPT_MISC,
		OPT_TORQUE,
		OPT_VISION,
		OPT_FCM30,
		OPT_TYPE_MAX,
	}enumOptionCategory;

	static LPCTSTR lpszOptionCategory [] = 
	{
		_T("System"),
		_T("Barcode Scanner"),
		_T("MES"),
		_T("Device"),
		_T("Misc"),
		_T("Torque"),
		_T("Vision Camera"),
		_T("FCM30 TCP/IP"),
		NULL
	};
}

#pragma pack (pop)

#endif // Define_Option_h__

