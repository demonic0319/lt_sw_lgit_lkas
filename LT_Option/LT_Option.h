﻿//*****************************************************************************
// Filename	: LT_Option.h
// Created	: 2015/12/16
// Modified	: 2015/12/16
//
// Author	: PiRing
//	
// Purpose	: 옵션 처리를 위한 클래스
//*****************************************************************************
#ifndef LT_Option_h__
#define LT_Option_h__

#pragma once

#include "Define_Option.h"
#include "Def_Enum_Cm.h"

using namespace Luritech_Option;

//=============================================================================
//
//=============================================================================
class CLT_Option
{
public:
	CLT_Option();
	~CLT_Option();

	CLT_Option& operator= (CLT_Option& ref)
	{
		m_stOption			= ref.m_stOption;
		m_strRegPath_Base	= ref.m_strRegPath_Base;
		m_InsptrType		= ref.m_InsptrType;

		return *this;
	};

protected:
	// 옵션 구조체
	stLT_Option			m_stOption;

	// 레지스트리 저장 경로
	CString				m_strRegPath_Base;

	// 검사기 설정
	enInsptrSysType		m_InsptrType;

public:
	void		SetRegistryPath			(__in LPCTSTR lpszRegPath);

	// 기본 검사 설정
	void		SaveOption_Inspector	(__in stOpt_Insp stOption);
	BOOL		LoadOption_Inspector	(__out stOpt_Insp& stOption);

	// 바코드 리더기 설정
	void		SaveOption_BCR			(__in stOpt_BCR stOption);
	BOOL		LoadOption_BCR			(__out stOpt_BCR& stOption);

	// MES 설정
	void		SaveOption_MES			(__in stOpt_MES stOption);
	BOOL		LoadOption_MES			(__out stOpt_MES& stOption);

	// 광원 보드 설정
	void		SaveOption_PCB			(__in stOpt_PCB stOption);
	BOOL		LoadOption_PCB			(__out stOpt_PCB& stOption);
	
	// 토크 드라이버 설정
	void		SaveOption_Torque		(__in stOpt_Torque stOption);
	BOOL		LoadOption_Torque		(__out stOpt_Torque& stOption);

	// PLC
	void		SaveOption_PLC			(__in stOpt_PLC stOption);
	BOOL		LoadOption_PLC			(__out stOpt_PLC& stOption);

	// 기타 설정
	void		SaveOption_Misc			(__in stOpt_Misc stOption);
	BOOL		LoadOption_Misc			(__out stOpt_Misc& stOption);

	void		SaveOption_HT			(stOpt_HT stOption);
	BOOL		LoadOption_HT			(stOpt_HT& stOption);

	// 비전 카메라
	void		SaveOption_VisionCam	(__in stOpt_Ethernet stOption);
	BOOL		LoadOption_VisionCam	(__out stOpt_Ethernet& stOption);

	// TCPIP
	void		SaveOption_TCP_IP		(__in stOpt_Ethernet stOption);
	BOOL		LoadOption_TCP_IP		(__out stOpt_Ethernet& stOption);


	// 전체 옵션 저장/불러오기
	void		SaveOption_All			(__in stLT_Option stOption);
	BOOL		LoadOption_All			(__out stLT_Option& stOption);

	// 검사기 구분
	void		SetInspectorType		(__in enInsptrSysType nInsptrType);

};

#endif // LT_Option_h__

