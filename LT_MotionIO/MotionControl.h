﻿//*****************************************************************************
// Filename	: 	MotionControl
// Created	:	2016/9/23 - 14:43
// Modified	:	2016/9/23 - 14:43
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#pragma once

#include "AXM.h"
#include "Def_Motor.h"
#include "math.h"

#include <TimeAPI.h>

class CMotionControl
{
public:
	CMotionControl();
	~CMotionControl();

	ST_MotionParam	*m_pstMotorParam;

	void	SetPtrMotorParam			(ST_MotionParam *pstMotorParam);

	// 원점 동작 함수 
	HANDLE  m_hThrOrigin;

	static  UINT WINAPI	ThreadOrigin	(__in LPVOID lParam);

	void	DoEvents					();
	void	DoEvents					(DWORD dwMiliSeconds);

	// 원점 수행 
	BOOL	OriginStartAxis				();

	// 원점 수행 정지
	BOOL	OriginStopAxis				();

	// 원점 수행 프로세스 
	void	OriginFunction				();

	// 모터 급정지
	BOOL	MotorAxisEStop				();

	// 모터 천천히 정지
	BOOL	MotorAxisSStop				();

	// 모터 조그 이동, nDir 0 -> - 방향, 1 -> + 방향
	BOOL	MotorAxisJogMove			(UINT nDir, UINT nJogVel);

	// 모터 이동, nAbsRelMode 0 -> Abs, 1 -> Rel
	BOOL	MotorAxisMove				(UINT nAbsRelMode, double dbPos);

	// 모터 이동 속도&가감속 직접 입력 제어, nAbsRelMode 0 -> Abs, 1 -> Rel
	BOOL	MotorAxisDitalMove			(UINT nAbsRelMode, PST_AxisMoveParam pAxisParam);

	// 축 사용 여부 확인
	BOOL	GetAxisUseStatus			();

	// 알람 상태 확인
	DWORD	GetAlarmStatus				();

	// 원점 상태 
	BOOL	SetOriginReset				();

	// 원점 상태 확인
	BOOL	GetOriginStatus				();

	// 모터 이동 상태 확인
	DWORD	GetMoveingStatus			();

	// 모터 엠프 상태 확인
	DWORD	GetAmpStatus				();

	// 모터 모션 상태 확인
	DWORD	GetMotionStatus				();

	// 플러스 리미트 센서 상태 확인 
	DWORD	GetPosSenStatus				();

	// 마이너스 리미트 센서 상태 확인 
	DWORD	GetNegSenStatus				();

	// 홈 센서 상태 확인
	DWORD	GetHomeSenStatus			();

	// 원점 수행 강제 정지 신호
	DWORD	GetOriginEStopSenStatus		();

	// 펄스값 확인
	double	GetCurrentPos				();

	// 결과 Message
	LPCTSTR	GetMotorResultMsg			();

	// 초기 모터 셋팅
	BOOL	SetMotorOpen				();

	// 모터 동작이 끝날 경우
	BOOL	SetMotorClose				();

	// 모터 셋팅 값 변환
	void	SetMotorSetting				();
	
	// 모터 엠프 상태 셋팅, bOnOff 0 -> OFF, 1 -> ON
	BOOL	SetMotorAmpCtr				(BOOL bOnOff);

	// 모터 알람 초기화 
	BOOL	SetMotorAlarmClear			();

	// 모터 펄스값 초기화
	BOOL	SetMotorPulseClear			();

	BOOL	m_bMoveStop;
private:

	LPCTSTR AlarmMessage				(DWORD dwAlarm);
	TCHAR	m_szAlarm_message[MAX_PATH];

	BOOL	m_bOriginStop;
	DWORD	m_dwErrCode;
	CString m_strResultMeg;
};
