﻿//*****************************************************************************
// Filename	: 	Define_Motor
// Created	:	2016/9/23 - 13:01
// Modified	:	2016/9/23 - 13:01
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Motor_h__
#define Def_Motor_h__

//#define		UI_TEST_DEBUG					// 개발용 PC에서 테스트 할경우

#define UP						TRUE
#define DN						FALSE
#define ON						TRUE
#define OFF						FALSE
#define AXL_DEFAULT_IRQNO		7

//=============================================================================
// Motor Param 구조체
//=============================================================================
typedef struct _tag_MotionParam
{
	double dbMaxVel;
	double dbVel;
	double dbAcc;
	double dbOriginAccFirst;
	double dbOriginAccSecond;
	double dbDec;
	double dbCoarseVel;
	double dbFineVel;
	double dbOriginVelFirst;
	double dbOriginVelSecond;
	double dbOriginVelThird;
	double dbOriginVelLast;
	double dbNegLimit;
	double dbPosLimit;
	double dbBufNegLimit;
	double dbBufPosLimit;
	double dbNumerator;
	double dbDenominator;
	double dbUnit;
	double dbPulse;
	double dbGearRatio;
	double dbOriginOffset;
	double dbGantryOffset;
	double dbOffsetRange;
	double dbEncoderMethod;
	double dbCoordDirection;
	double dbMotorType;
	double dbAxisStatus;
	double dbEncoderType;
	double dbStepMotorSmoothing;
	double dbVelPulse;
	double dbMotorAmpEnableLevel;
	double dbMotorAmpResetLevel;
	double dbMotorAmpFaultLevel;
	double dbMotorNegaLimitLevel;
	double dbMotorPosiLimitLevel;
	double dbGantrySelect;
	double dbStartSpeed;
	double dbStopLevel;

	BOOL bAxisUse;
	int	iAxisNum;
	int iAxisServoLevel;
	int iAxisInpositionLevel;
	int iAxisAlarmLevel;
	int iAxisHomeSenLevel;
	int iAxisHomeSig;
	int iAxisOutputMethod;
	int iAxisNegLimitSenLevel;
	int iAxisPosLimitSenLevel;
	int iAxisHomeDir;
	int iAxisEncInputMethod;
	int iAxisEmergencyLevel;

	CString szAxisName;

	_tag_MotionParam()
	{
		dbMaxVel				= 0.0;
		dbVel					= 0.0;
		dbAcc					= 0.0;
		dbOriginAccFirst		= 0.0;
		dbOriginAccSecond		= 0.0;
		dbDec					= 0.0;
		dbCoarseVel				= 0.0;
		dbFineVel				= 0.0;
		dbOriginVelFirst		= 0.0;
		dbOriginVelSecond		= 0.0;
		dbOriginVelThird		= 0.0;
		dbOriginVelLast			= 0.0;
		dbNegLimit				= 0.0;
		dbPosLimit				= 0.0;
		dbBufNegLimit			= 0.0;
		dbBufPosLimit			= 0.0;
		dbNumerator				= 0.0;
		dbDenominator			= 0.0;
		dbUnit					= 10000.0;
		dbPulse					= 10000.0;
		dbGearRatio				= 0.0;
		dbOriginOffset			= 0.0;
		dbGantryOffset			= 0.0;
		dbOffsetRange			= 0.0;
		dbEncoderMethod			= 0.0;
		dbCoordDirection		= 0.0;
		dbMotorType				= 0.0;
		dbAxisStatus			= 0.0;
		dbEncoderType			= 0.0;
		dbStepMotorSmoothing	= 0.0;
		dbVelPulse				= 0.0;
		dbMotorAmpEnableLevel	= 0.0;
		dbMotorAmpResetLevel	= 0.0;
		dbMotorAmpFaultLevel	= 0.0;
		dbMotorNegaLimitLevel	= 0.0;
		dbMotorPosiLimitLevel	= 0.0;
		dbGantrySelect			= 0.0;
		dbStartSpeed			= 0.0;
		dbStopLevel				= 0.0;

		iAxisServoLevel			= 0;
		iAxisInpositionLevel	= 0;
		iAxisNum				= 0;
		iAxisAlarmLevel			= 0;
		iAxisHomeSenLevel		= 0;
		iAxisHomeSig			= 0;
		iAxisOutputMethod		= 0;
		iAxisPosLimitSenLevel	= 0;
		iAxisNegLimitSenLevel	= 0;
		iAxisHomeDir			= 0;
		iAxisEncInputMethod		= 0;
		iAxisEmergencyLevel		= 0;
		bAxisUse				= TRUE;

		szAxisName.Empty();
	}

	_tag_MotionParam& operator= (_tag_MotionParam& ref)
	{
		dbMaxVel				= ref.dbMaxVel;
		dbVel					= ref.dbVel;
		dbAcc					= ref.dbAcc;
		dbOriginAccFirst		= ref.dbOriginAccFirst;
		dbOriginAccSecond		= ref.dbOriginAccSecond;
		dbDec					= ref.dbDec;
		dbCoarseVel				= ref.dbCoarseVel;
		dbFineVel				= ref.dbFineVel;
		dbOriginVelFirst		= ref.dbOriginVelFirst;
		dbOriginVelSecond		= ref.dbOriginVelSecond;
		dbOriginVelThird		= ref.dbOriginVelThird;
		dbOriginVelLast			= ref.dbOriginVelLast;
		dbNegLimit				= ref.dbNegLimit;
		dbPosLimit				= ref.dbPosLimit;
		dbBufNegLimit			= ref.dbBufNegLimit;
		dbBufPosLimit			= ref.dbBufPosLimit;
		dbNumerator				= ref.dbNumerator;
		dbDenominator			= ref.dbDenominator;
		dbUnit					= ref.dbUnit;
		dbPulse					= ref.dbPulse;
		dbGearRatio				= ref.dbGearRatio;
		dbOriginOffset			= ref.dbOriginOffset;
		dbGantryOffset			= ref.dbGantryOffset;
		dbOffsetRange			= ref.dbOffsetRange;
		dbEncoderMethod			= ref.dbEncoderMethod;
		dbCoordDirection		= ref.dbCoordDirection;
		dbMotorType				= ref.dbMotorType;
		dbAxisStatus			= ref.dbAxisStatus;
		dbEncoderType			= ref.dbEncoderType;
		dbStepMotorSmoothing	= ref.dbStepMotorSmoothing;
		dbVelPulse				= ref.dbVelPulse;
		iAxisInpositionLevel	= ref.iAxisInpositionLevel;
		dbMotorAmpEnableLevel	= ref.dbMotorAmpEnableLevel;
		dbMotorAmpResetLevel	= ref.dbMotorAmpResetLevel;
		dbMotorAmpFaultLevel	= ref.dbMotorAmpFaultLevel;
		dbMotorNegaLimitLevel	= ref.dbMotorNegaLimitLevel;
		dbMotorPosiLimitLevel	= ref.dbMotorPosiLimitLevel;
		dbGantrySelect			= ref.dbGantrySelect;
		dbStartSpeed			= ref.dbStartSpeed;
		dbStopLevel				= ref.dbStopLevel;

		iAxisNum				= ref.iAxisNum;
		iAxisServoLevel			= ref.iAxisServoLevel;
		iAxisAlarmLevel			= ref.iAxisAlarmLevel;
		iAxisHomeSenLevel		= ref.iAxisHomeSenLevel;
		iAxisHomeSig			= ref.iAxisHomeSig;
		iAxisOutputMethod		= ref.iAxisOutputMethod;
		iAxisPosLimitSenLevel	= ref.iAxisPosLimitSenLevel;
		iAxisNegLimitSenLevel	= ref.iAxisNegLimitSenLevel;
		iAxisHomeDir			= ref.iAxisHomeDir;
		iAxisEncInputMethod		= ref.iAxisEncInputMethod;
		iAxisEmergencyLevel		= ref.iAxisEmergencyLevel;
		bAxisUse				= ref.bAxisUse;

		szAxisName				= ref.szAxisName;

		return *this;
	};
}ST_MotionParam, *PST_MotionParam;

//=============================================================================
// Motor Param temp Info
//=============================================================================
typedef struct _tagAxisMoveParam
{
	long		lAxisNum;
	double		dbPos;
	double		dbVel;
	double		dbAcc;
	double		dbDec;

	_tagAxisMoveParam()
	{
		lAxisNum = 0;
		dbPos = 0.0;
		dbVel = 0.0;
		dbAcc = 0.0;
		dbDec = 0.0;
	}
}ST_AxisMoveParam, *PST_AxisMoveParam;

//=============================================================================
// Motor Status Info
//=============================================================================
typedef struct _tag_MotorInfo
{
	BOOL	bMotionOpened;
	DWORD	dwModulStatus;
	long	lMaxAxisCnt;

	_tag_MotorInfo()
	{
		bMotionOpened	= FALSE;
		dwModulStatus	= 0;
		lMaxAxisCnt		= 0;
	}

}ST_MotorInfo, *PST_MotorInfo;

//=============================================================================
// Motor Total Info
//=============================================================================
typedef struct _tag_AllMotorData
{
	ST_MotionParam*		pMotionParam;
	ST_MotorInfo		MotorInfo;

	_tag_AllMotorData()
	{
		pMotionParam	= NULL;
	}

}ST_AllMotorData, *PST_AllMotorData;

#endif // Def_Motor_h__