﻿//*****************************************************************************
// Filename	: 	MotionManager
// Created	:	2016/9/23 - 15:56
// Modified	:	2016/9/23 - 15:56
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "MotionManager.h"

#include "AXL.h"
#include "AXHS.h"
#include "AXDev.h"

#define MASTER_MOTOR_FILE		_T("Axis.mot")

#if defined(_WIN64)
#pragma	comment(lib,"..\\Lib\\64bit\\AXL_x64.lib")
#else
#pragma	comment(lib,"..\\Lib\\32bit\\AXL.lib")
#endif

CMotionManager::CMotionManager()
{
	m_pMotionCtr = NULL;
}

CMotionManager::~CMotionManager()
{
 	SetAllMotorClose();
}

//=============================================================================
// Method		: DoEvents
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
//=============================================================================
void CMotionManager::DoEvents()
{
	MSG msg;

	if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
	{
		if (!AfxGetApp()->PreTranslateMessage(&msg))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	::Sleep(0);
}

//=============================================================================
// Method		: DoEvents
// Access		: public  
// Returns		: void
// Parameter	: DWORD dwMiliSeconds
// Qualifier	:
// Last Update	: 2017/2/3 - 17:33
// Desc.		:
//=============================================================================
void CMotionManager::DoEvents(DWORD dwMiliSeconds)
{
	timeBeginPeriod(1);

	clock_t start_tm = clock();
	do
	{
		DoEvents();
		::Sleep(1);
	} while ((DWORD)(clock() - start_tm) < dwMiliSeconds);
}

//=============================================================================
// Method		: SetPrtMotorPath
// Access		: public  
// Returns		: void
// Parameter	: CString * pszMotorpath
// Qualifier	:
// Last Update	: 2017/4/4 - 15:42
// Desc.		:
//=============================================================================
void CMotionManager::SetPrtMotorPath(CString *pszMotorpath)
{
	if (pszMotorpath == NULL)
		return;

	m_szMotorFile = *pszMotorpath;

	m_szMotorFile.Format(_T("%s%s"), m_szMotorFile, MASTER_MOTOR_FILE);
}

//=============================================================================
// Method		: SetPrtMotorPath
// Access		: public  
// Returns		: void
// Parameter	: CString * pszMotorpath
// Parameter	: CString szMotor
// Qualifier	:
// Last Update	: 2017/4/4 - 15:42
// Desc.		:
//=============================================================================
void CMotionManager::SetPrtMotorPath(CString *pszMotorpath, LPCTSTR szMotor)
{
	if (pszMotorpath == NULL)
		return;

	m_szMotorFile = *pszMotorpath;

	m_szMotorFile.Format(_T("%s%s.mot"), m_szMotorFile, szMotor);
}

//=============================================================================
// Method		: LoadMotionInfo
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/5 - 18:37
// Desc.		:
//=============================================================================
BOOL CMotionManager::LoadMotionInfo()
{
#ifdef UI_TEST_DEBUG
	m_AllMotorData.MotorInfo.lMaxAxisCnt = 6;
#endif

	if (!m_File_Motor.LoadMotorInfoFiles(m_szMotorFile, m_AllMotorData))
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		// LOAD 데이터
		m_pMotionCtr[nAxis].SetPtrMotorParam(&m_AllMotorData.pMotionParam[nAxis]);

		// 모터 셋팅
		m_pMotionCtr[nAxis].SetMotorOpen();
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveMotionInfo
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/9 - 15:19
// Desc.		:
//=============================================================================
BOOL CMotionManager::SaveMotionInfo()
{
	if (!m_File_Motor.SaveMotorInfoFiles(m_szMotorFile, &m_AllMotorData))
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: Initialize
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nMaxAxis
// Qualifier	:
// Last Update	: 2017/2/5 - 18:37
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllMotorOpen()
{
#ifdef UI_TEST_DEBUG
	m_AllMotorData.MotorInfo.lMaxAxisCnt = 6;

	// Motion Cnt 초기화, 생성
	if (m_pMotionCtr)
	{
		delete[] m_pMotionCtr;
		m_pMotionCtr = NULL;
	}
	m_pMotionCtr = new CMotionControl[m_AllMotorData.MotorInfo.lMaxAxisCnt];

	// Motion 데이터 초기화, 생성
	if (m_AllMotorData.pMotionParam)
	{
		delete[] m_AllMotorData.pMotionParam;
		m_AllMotorData.pMotionParam = NULL;
	}

	m_AllMotorData.pMotionParam = new ST_MotionParam[m_AllMotorData.MotorInfo.lMaxAxisCnt];

	// 데이터 불러오기
	// 	if (!LoadMotionInfo())
	// 		return	FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		// LOAD 데이터
		m_pMotionCtr[nAxis].SetPtrMotorParam(&m_AllMotorData.pMotionParam[nAxis]);

		// 모터 셋팅
		m_pMotionCtr[nAxis].SetMotorOpen();
	}

	// Motion 보드 init 성공여부
	m_AllMotorData.MotorInfo.bMotionOpened = TRUE;
#else
	long lAxisCnt = 0;

	m_AllMotorData.MotorInfo.bMotionOpened = FALSE;

	// Initialize library 
	// 7은 IRQ를 뜻한다. PCI에서는 자동으로 IRQ가 설정된다.
	if (!AxlIsOpened())
	{
		if (AxlOpenNoReset(AXL_DEFAULT_IRQNO) != AXT_RT_SUCCESS)
			return FALSE;
	}

	// 모션 제어 모듈이 있는지 확인
	AxmInfoIsMotionModule(&m_AllMotorData.MotorInfo.dwModulStatus);

	if (m_AllMotorData.MotorInfo.dwModulStatus == STATUS_NOTEXIST)
		return FALSE;

	// 모터 MAX 축 수량 확인
	AxmInfoGetAxisCount(&m_AllMotorData.MotorInfo.lMaxAxisCnt);

	if (m_AllMotorData.MotorInfo.lMaxAxisCnt < 0)
		return FALSE;

	// Motion Cnt 초기화, 생성
	if (m_pMotionCtr)
	{
		delete[] m_pMotionCtr;
		m_pMotionCtr = NULL;
	}
	m_pMotionCtr = new CMotionControl[m_AllMotorData.MotorInfo.lMaxAxisCnt];

	// Motion 데이터 초기화, 생성
	if (m_AllMotorData.pMotionParam)
	{
		delete[] m_AllMotorData.pMotionParam;
		m_AllMotorData.pMotionParam = NULL;
	}

	m_AllMotorData.pMotionParam = new ST_MotionParam[m_AllMotorData.MotorInfo.lMaxAxisCnt];

	// 데이터 불러오기
	// 	if (!LoadMotionInfo())
	// 		return	FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		// LOAD 데이터
		m_pMotionCtr[nAxis].SetPtrMotorParam(&m_AllMotorData.pMotionParam[nAxis]);

		// 모터 셋팅
		m_pMotionCtr[nAxis].SetMotorOpen();
	}

	// Motion 보드 init 성공여부
	m_AllMotorData.MotorInfo.bMotionOpened = TRUE;
#endif
	return TRUE;
}

//=============================================================================
// Method		: CloseDevice
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 15:59
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllMotorClose()
{
// 	if (m_AllMotorData.MotorInfo.bMotionOpened == TRUE)
// 	{
// 		for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
// 		{
// 			if (m_pMotionCtr[nAxis].SetMotorClose())
// 				return FALSE;
// 		}
// 	}
	
	// Motion Cnt 제거
	if (m_pMotionCtr)
	{
		delete[] m_pMotionCtr;
		m_pMotionCtr = NULL;
	}

	// Motion 데이터 배열 제거
	if (m_AllMotorData.pMotionParam)
	{
		delete[] m_AllMotorData.pMotionParam;
		m_AllMotorData.pMotionParam = NULL;
	}

	return TRUE;
}

//=============================================================================
// Method		: SetAllOriginStop
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 16:00
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllOriginStop()
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (!m_pMotionCtr[nAxis].OriginStopAxis())
			return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: SetAllAmpCtr
// Access		: public  
// Returns		: BOOL
// Parameter	: BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/3/28 - 15:17
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllAmpCtr(BOOL bOnOff)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (!m_pMotionCtr[nAxis].SetMotorAmpCtr(bOnOff))
			return FALSE;

		DoEvents(5);
	}

	return TRUE;
}

//=============================================================================
// Method		: SetAllAlarmClear
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/2 - 13:08
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllAlarmClear()
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if(!m_pMotionCtr[nAxis].SetMotorAlarmClear())
			return FALSE;

		DoEvents(5);
	}

	return TRUE;
}

//=============================================================================
// Method		: SetAllMotorSStop
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/28 - 14:14
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllMotorSStop()
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if(m_pMotionCtr[nAxis].MotorAxisSStop())
			return FALSE;

		DoEvents(5);
	}

	return TRUE;
}

//=============================================================================
// Method		: SetAllMotorEStop
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/3/28 - 14:14
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllMotorEStop()
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pMotionCtr[nAxis].MotorAxisEStop())
			return FALSE;

		DoEvents(5);
	}

	return TRUE;
}

//=============================================================================
// Method		: SetAllMotionSetting
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 16:01
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAllMotionSetting()
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (TRUE == m_pMotionCtr[nAxis].m_pstMotorParam->bAxisUse)
		{
			m_pMotionCtr[nAxis].SetMotorSetting();
		}
		DoEvents(5);
	}

	return TRUE;
}

//=============================================================================
// Method		: SetOriginStop
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 16:06
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetOriginStop(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].OriginStopAxis())
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetAmpCtr
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Parameter	: BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/3/28 - 16:07
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAmpCtr(UINT nAxis, BOOL bOnOff)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].SetMotorAmpCtr(bOnOff))
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetAlarmClear
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 16:08
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetAlarmClear(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].SetMotorAlarmClear())
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetMotorSStop
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 16:08
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetMotorSStop(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].MotorAxisSStop())
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetMotorPosClear
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/31 - 15:54
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetMotorPosClear(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].SetMotorPulseClear())
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetMotorEStop
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 16:08
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetMotorEStop(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].MotorAxisEStop())
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetMotionOrigin
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 20:17
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetMotorOrigin(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].OriginStartAxis())
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: SetMotionSetting
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 16:08
// Desc.		:
//=============================================================================
void CMotionManager::SetMotionSetting(UINT nAxis)
{
	m_pMotionCtr[nAxis].SetMotorSetting();
}

//=============================================================================
// Method		: GetBoardOpen
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/10/14 - 19:20
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetBoardOpen()
{
	return m_AllMotorData.MotorInfo.bMotionOpened;
}

//=============================================================================
// Method		: GetAllOriginStatus
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/23 - 16:01
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetAllOriginStatus()
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	for (UINT nAxis = 0; nAxis < (UINT)m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (!m_pMotionCtr[nAxis].GetOriginStatus())
			return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: GetOriginStopStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 15:35
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetOriginStopStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetOriginEStopSenStatus();
}

//=============================================================================
// Method		: GetAllAmpStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:29
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetAmpStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetAmpStatus();
}

//=============================================================================
// Method		: SetOriginReset
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2018/2/13 - 13:49
// Desc.		:
//=============================================================================
BOOL CMotionManager::SetOriginReset(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].SetOriginReset();
}

//=============================================================================
// Method		: GetOriginSenStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:20
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetMotionStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetMotionStatus();
}

//=============================================================================
// Method		: GetOriginSenStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:20
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetOriginStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetOriginStatus();
}

//=============================================================================
// Method		: GetPosSenserStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:32
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetPosSensorStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetPosSenStatus();
}

//=============================================================================
// Method		: GetNegSensorStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:32
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetNegSensorStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetNegSenStatus();
}

//=============================================================================
// Method		: GetHomeSensorStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:32
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetHomeSensorStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetHomeSenStatus();
}

//=============================================================================
// Method		: GetAlarmSenorStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:33
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetAlarmSenorStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetAlarmStatus();
}

//=============================================================================
// Method		: GetAxisNumber
// Access		: public  
// Returns		: UINT
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 20:36
// Desc.		:
//=============================================================================
UINT CMotionManager::GetAxisNumber(UINT nAxis)
{
	return m_AllMotorData.pMotionParam[nAxis].iAxisNum;
}

//=============================================================================
// Method		: GetAxisUseStatus
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 20:57
// Desc.		:
//=============================================================================
BOOL CMotionManager::GetAxisUseStatus(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	return m_pMotionCtr[nAxis].GetAxisUseStatus();
}

//=============================================================================
// Method		: GetCurrentPos
// Access		: public  
// Returns		: double
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/28 - 14:28
// Desc.		:
//=============================================================================
double CMotionManager::GetCurrentPos(UINT nAxis)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	return m_pMotionCtr[nAxis].GetCurrentPos();
}

//=============================================================================
// Method		: GetPosSensorLevel
// Access		: public  
// Returns		: int
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/29 - 9:33
// Desc.		:
//=============================================================================
int CMotionManager::GetPosSensorLevel(UINT nAxis)
{
	return m_AllMotorData.pMotionParam[nAxis].iAxisPosLimitSenLevel;
}

//=============================================================================
// Method		: GetNegSensorLevel
// Access		: public  
// Returns		: int
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/29 - 9:34
// Desc.		:
//=============================================================================
int CMotionManager::GetNegSensorLevel(UINT nAxis)
{
	return m_AllMotorData.pMotionParam[nAxis].iAxisNegLimitSenLevel;
}

//=============================================================================
// Method		: GetHomeSensorLevel
// Access		: public  
// Returns		: int
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/29 - 9:34
// Desc.		:
//=============================================================================
int CMotionManager::GetHomeSensorLevel(UINT nAxis)
{
	return m_AllMotorData.pMotionParam[nAxis].iAxisHomeSenLevel;
}

//=============================================================================
// Method		: GetAlramSensorLevel
// Access		: public  
// Returns		: int
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/29 - 9:34
// Desc.		:
//=============================================================================
int CMotionManager::GetAlramSensorLevel(UINT nAxis)
{
	return m_AllMotorData.pMotionParam[nAxis].iAxisAlarmLevel;
}

//=============================================================================
// Method		: GetResultMessage
// Access		: public  
// Returns		: CString
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/3/30 - 9:16
// Desc.		:
//=============================================================================
CString CMotionManager::GetResultMessage(UINT nAxis)
{
	return m_pMotionCtr[nAxis].GetMotorResultMsg();
}

//=============================================================================
// Method		: MotorJogMove
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAxis
// Parameter	: UINT nDir
// Parameter	: UINT nJogSpeed
// Qualifier	:
// Last Update	: 2017/3/28 - 14:39
// Desc.		:
//=============================================================================
BOOL CMotionManager::MotorJogMove(UINT nAxis, UINT nDir, UINT nJogSpeed)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].MotorAxisJogMove(nDir, nJogSpeed))
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: MotorMultiMove
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAbsRelMode
// Parameter	: long lAxisSize
// Parameter	: PST_AxisMoveParam pAxisParam
// Qualifier	:
// Last Update	: 2017/3/28 - 13:28
// Desc.		:
//=============================================================================
BOOL CMotionManager::MotorMultiMove(UINT nAbsRelMode, long lAxisSize, PST_AxisMoveParam pAxisParam)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	BOOL	bResult = TRUE;
	DWORD	dwStatus = 0;

	long*	lNo		 = new long[lAxisSize];
	double* dbPos 	 = new double[lAxisSize];
	double* dbVel	 = new double[lAxisSize];
	double* dbAcc	 = new double[lAxisSize];
	double* dbDec	 = new double[lAxisSize];
	double* dbGetPos = new double[lAxisSize];
	double* dbGetRelPos		= new double[lAxisSize];

	for (long lAxis = 0; lAxis < lAxisSize; lAxis++)
	{
		lNo[lAxis]		= pAxisParam[lAxis].lAxisNum;
		dbPos[lAxis]	= pAxisParam[lAxis].dbPos;
		dbVel[lAxis]	= pAxisParam[lAxis].dbVel;
		dbAcc[lAxis]	= pAxisParam[lAxis].dbAcc;
		dbDec[lAxis]	= pAxisParam[lAxis].dbDec;
		dbGetPos[lAxis] = 0.0;

		AxmMotSetAbsRelMode(lNo[lAxis], nAbsRelMode);
	}

	for (UINT nAxis = 0; nAxis < (UINT)lAxisSize; nAxis++)
	{
		m_pMotionCtr[lNo[nAxis]].m_bMoveStop = FALSE;
		dbGetRelPos[nAxis] = m_pMotionCtr[lNo[nAxis]].GetCurrentPos();
	}
		
	dwStatus = AxmMoveStartMultiPos(lAxisSize, lNo, dbPos, dbVel, dbAcc, dbDec);

	if (dwStatus != AXT_RT_SUCCESS)
		bResult = FALSE;

	if (bResult)
	{
		for (UINT nAxis = 0; nAxis < (UINT)lAxisSize; nAxis++)
		{
			// 동작 완료 여부를 확인하자
			if (!m_pMotionCtr[lNo[nAxis]].GetMoveingStatus())
			{
				bResult = FALSE;
				break;
			}
		}
	}

	if (bResult)
	{
		// 현재 위치 인지 확인
		for (UINT nAxis = 0; nAxis < (UINT)lAxisSize; nAxis++)
			dbGetPos[nAxis] = m_pMotionCtr[lNo[nAxis]].GetCurrentPos();
	}

	if (bResult)
	{
		for (UINT nAxis = 0; nAxis < (UINT)lAxisSize; nAxis++)
		{
			if (POS_REL_MODE == nAbsRelMode)
			{
				dbPos[nAxis] += dbGetRelPos[nAxis];
			}

			double dbTemp = abs(dbPos[nAxis] - dbGetPos[nAxis]);

			if (dbTemp > 5.0)
			{
				bResult = FALSE;
				break;
			}
		}
	}

	if (bResult)
	{
		for (UINT nAxis = 0; nAxis < (UINT)lAxisSize; nAxis++)
		{
			if (m_AllMotorData.pMotionParam[lNo[nAxis]].iAxisAlarmLevel != 2)
			{
				dwStatus = m_pMotionCtr[lNo[nAxis]].GetAlarmStatus();

				if (dwStatus > 0)
				{
					bResult = FALSE;
					break;
				}
			}
		}
	}

	delete[] lNo;
	delete[] dbPos;
	delete[] dbVel;
	delete[] dbAcc;
	delete[] dbDec;
	delete[] dbGetPos;
	delete[] dbGetRelPos;


	return bResult;
}

//=============================================================================
// Method		: MotorAxisMove
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAbsRelMode
// Parameter	: double dbPos
// Qualifier	:
// Last Update	: 2017/3/29 - 16:31
// Desc.		:
//=============================================================================
BOOL CMotionManager::MotorAxisMove(UINT nAbsRelMode, UINT nAxis, double dbPos)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	if (!m_pMotionCtr[nAxis].MotorAxisMove(nAbsRelMode, dbPos))
		return FALSE;

	return TRUE;
}

//=============================================================================
// Method		: MotorAxisDitailMove
// Access		: public  
// Returns		: BOOL
// Parameter	: UINT nAbsRelMode
// Parameter	: UINT nAxis
// Parameter	: double dbPos
// Parameter	: double dbVel
// Parameter	: double dbAcc
// Qualifier	:
// Last Update	: 2017/4/27 - 20:51
// Desc.		:
//=============================================================================
BOOL CMotionManager::MotorAxisDitailMove(UINT nAbsRelMode, UINT nAxis, double dbPos, double dbVel, double dbAcc)
{
	if (m_pMotionCtr == NULL)
		return FALSE;

	if (m_AllMotorData.MotorInfo.bMotionOpened == FALSE)
		return FALSE;

	ST_AxisMoveParam AxisParam;

	AxisParam.dbPos = dbPos;
	AxisParam.dbVel = dbVel;
	AxisParam.dbDec = dbAcc;
	AxisParam.dbAcc = dbAcc;

	if (!m_pMotionCtr[nAxis].MotorAxisDitalMove(nAbsRelMode, &AxisParam))
		return FALSE;

	return TRUE;
}
