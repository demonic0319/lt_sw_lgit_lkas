﻿//*****************************************************************************
// Filename	: 	Def_DigitiIO_h__
// Created	:	2017/2/05 - 13:01
// Modified	:	2017/2/05 - 13:01
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_DigitiIO_h__
#define Def_DigitiIO_h__

#include <wtypes.h>

#define AXL_DEFAULT_IRQNO	7
#define AXL_MAX_MODULE_NO	4		// BPFR 기종은 최대 4개의 모듈 장착

typedef enum enum_IO_SignalType
{
	IO_SignalT_SetOff = 0,
	IO_SignalT_SetOn,
	IO_SignalT_PulseOff,
	IO_SignalT_PulseOn,
	IO_SignalT_ToggleStart,
	IO_SignalT_ToggleStop,
};

// 베이스 보드 및 보드에 장착된 모듈 정보
typedef struct _tag_AxdBoardInfo
{
	BOOL	bAxlIsOpened;	// AXL 라이브러리 초기화 상태 (1, 0)
	DWORD	dwStatus;		// 모듈 상태 (STATUS_EXIST, STATUS_NOTEXIST)
	long	lModuleCounts;	// DIO 모듈 개수	

	_tag_AxdBoardInfo()
	{
		bAxlIsOpened	= FALSE;
		dwStatus		= 0;
		lModuleCounts	= 0;
	};

}ST_AxdBoardInfo, *PST_AxdBoardInfo;



#endif // Def_DigitiIO_h__