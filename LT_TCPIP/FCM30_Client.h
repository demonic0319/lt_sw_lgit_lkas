﻿//*****************************************************************************
// Filename	: 	FCM30_Client.h
// Created	:	2016/5/10 - 15:31
// Modified	:	2016/5/10 - 15:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef FCM30_Client_h__
#define FCM30_Client_h__

#pragma once

#include "BSocketClient.h"
#include "Def_FCM30.h"
#include "SerQueue.h"

using namespace TCP_IP;

#define MAX_FCM30_Client_BUFFER	1024

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
class CFCM30_Client : public CBSocketClient
{
public:
	CFCM30_Client();
	virtual ~CFCM30_Client();

	//-----------------------------------------------------
	// SocketClient Interface handler
	//-----------------------------------------------------
	virtual void	OnThreadBegin			(CSocketHandle* pSH);
	virtual void	OnThreadExit			(CSocketHandle* pSH);
	virtual void	OnConnectionDropped		(CSocketHandle* pSH);
	virtual void	OnConnectionError		(CSocketHandle* pSH, DWORD dwError);

	TCP_IP::ST_Protocol GetSendProtocol();
	TCP_IP::ST_Protocol GetRecvProtocol();

protected:
	
	virtual BOOL	PasingRecvAck			(const char* lpBuffer, DWORD dwReaded);
	virtual BOOL	OnRecvACK				(const char* lpBuffer, DWORD dwReaded);

	CSerQueue		m_SerQueue;
	ST_Protocol		m_stSendProtocol;
	ST_Protocol		m_stRecvProtocol;

	char			m_szACKBuf[MAX_FCM30_Client_BUFFER];
	DWORD_PTR		m_dwACKBufSize;	

	// Ping Ack 처리용 타이머 ------------------------------
	HANDLE			m_hTimerQueue;
	HANDLE			m_hTimer_PingCheck;

	void			CreateTimerQueue_Mon();
	void			DeleteTimerQueue_Mon();

	void			CreateTimer_PingCheck();
	void			DeleteTimer_PingCheck();

	static VOID CALLBACK TimerRoutine_PingCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);
	void			OnMonitor_PingCheck();

	BOOL			m_bRecvedPing;
	DWORD			m_dwPingCheckTime;

	BOOL			m_bPingEable;
	// Ping 주기 (1초)
	DWORD			m_dwPingCycle;

	// Ping Ack Timeout 시간 (3분간 응답 대기)
	DWORD			m_dwPinigAckTimeout;

	// Ack Timeout
	DWORD			m_dwAckTimeout;	

public:
	
	virtual BOOL	SendData			(const char* lpBuffer, DWORD dwCount);

	void		ResetAck();

	void		SetEnablePing			(__in BOOL bEnable)
	{
		m_bPingEable = bEnable;
	};

	void		SetPingCycle			(__in DWORD dwMiliSec)
	{
		m_dwPingCycle = dwMiliSec;
	};

	void		SetPingTimeout			(__in DWORD dwSecond)
	{
		m_dwPinigAckTimeout = dwSecond * 1000;
	};

	void		SetAckTimeout			(__in DWORD dwSecond)
	{
		m_dwAckTimeout = dwSecond * 1000;
	};

	// 메세지 전송 (Handler -> Tester) -----------------------------------------
	BOOL		Send_Ping				();
	BOOL		Send_GetData			(__in CString szBarcode);

	// 메세지 수신 -------------------------------------------------------------
	BOOL		Recv_Ping_Ack			();
	BOOL		Recv_Set				();
	BOOL		Recv_Get				();
};

#endif // FCM30_Client_h__

