﻿//*****************************************************************************
// Filename	: 	BSocketClient.h
//
// Created	:	
// Modified	:	2010/02/24 - 14:03
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef BSocketClient_h__
#define BSocketClient_h__


#pragma once

#include "SocketHandle.h"
#include "SocketClientImpl.h"

typedef enum
{
	COMM_NOTCONNECT	= 0,
	COMM_CONNECTED,	
	COMM_CONNECTED_SYNC_OK,
	COMM_DISCONNECT,
	COMM_CONNECT_DROP,
	COMM_CONNECT_ERROR,
	COMM_PING_TIMEOUT,
	COMM_PING_ERROR,
}enTCPIPConnectStatus;

// TCP/IP 통신 결과 코드
typedef enum enEthernetResultCode
{
	ERC_Err_Unknown,
	ERC_OK,
	ERC_Err_NotOpen,
	ERC_Err_Semaphore,
	ERC_Err_Transfer,
	ERC_Err_Parameter,
	ERC_Err_Ack,
	ERC_Err_Ack_Timeout,
	ERC_Err_Ack_RecvFail,
	ERC_Err_Ack_Protocol,
	ERC_Err_Protocol,
	ERC_Err_Protocol_CheckSum,
};

//=============================================================================
//
//=============================================================================
class CBSocketClient :	public ISocketClientHandler
{
	typedef SocketClientImpl<ISocketClientHandler> CSocketClient;
	friend	CSocketClient;

public:
	CBSocketClient();
	virtual ~CBSocketClient();

protected:

	//-----------------------------------------------------
	// 소켓 처리 관련
	//-----------------------------------------------------
	CSocketClient	m_SocketClient;			// 소켓 통신용 클래스
	int				m_nMode;				// AF_IPV4, AF_INET6
	CString			m_strAddr;				// 서버 IP
	CString			m_strPort;				// 서버 포트
	CString			m_strNIC_Addr;			// NIC IP

	//-----------------------------------------------------
	// 윈도우 핸들 / 윈도우 메세지
	//-----------------------------------------------------
	HWND			m_hOwnerWnd;			// 오너의 윈도우 핸들
	UINT			m_nWM_LOG;				// 로그 출력용 윈도우 메세지		
	UINT			m_WM_Recv;
	UINT			m_nDeviceType;			// 장치 구분용

	//-----------------------------------------------------
	// 통신 상태 알림 윈도우 메세지
	//-----------------------------------------------------
	UINT			m_nWM_COMM_STATUS;

	//-----------------------------------------------------
	// 에러 처리 / 동기화 처리
	//-----------------------------------------------------
	DWORD			m_dwErrCode;			// 오류 코드

	HANDLE			m_hSemaphore;
	DWORD			m_dwSemaphoreWaitTime;
	
	DWORD			LockSemaphore			();
	BOOL			UnlockSemaphore			();
	BOOL			WaitSemaphore			(DWORD dwTimeout = 2000);

	//-----------------------------------------------------
	// 이벤트, 쓰레드 처리
	//-----------------------------------------------------
	HANDLE			m_hACKEvent;			// 응답데이터 수신시 이벤트
	HANDLE			m_hAbortACKEvent;
	HANDLE			m_hInternalExitEvent;	// 내부의 통신 접속 쓰레드 종료 이벤트
	HANDLE			m_hExternalExitEvent;	// 외부의 통신 접속 쓰레드 종료 이벤트

	BOOL			m_bUseConnect;
	HANDLE			m_hThreadTryConnect;	// 통신 끊김시 자동 재연결을 위한 쓰레드 핸들
	HANDLE			m_hThreadConnect;

	DWORD			m_dwTimeReconnect;		// 통신 재접속 딜레이
	DWORD			m_dwAckTimeout;			// 커맨드 전송후, 응답 올때까지의 타임아웃 시간
	BOOL			m_bFlag_WaitAck;
	//-----------------------------------------------------
	// 지속적으로 ping 체크하여 연결 시도
	// TCP/IP의 SO_KEEPALIVE 옵션을 사용
	UINT			m_bExitProgramFlag;	// 접속 끊김과 접속 종료 처리를 구분하기 위해서 플래그 처리
	static UINT WINAPI	ThreadTryConnect	(LPVOID lParam);
	static UINT WINAPI	ThreadConnect		(LPVOID lParam);

	virtual BOOL	PasingRecvAck			(const char* lpBuffer, DWORD dwReaded);

	enum _WAIT_ACK_RESULT
	{
		ACK_TIMEOUT		= 0,
		ACK_READ_PASS,
		ACK_READ_ERROR,
		ACK_WRITE_PASS,
		ACK_WRITE_ERROR,
	};
	// 응답을 기다림
	BOOL			WaitACK					(DWORD dwWaitTime = 1000, LPDWORD  dwOutBufSize = NULL);
	
	void			GetAddress				(const SockAddrIn& addrIn, CString& rString) const;

#define		MAX_LOG_BUFFER_SOCK_CLI		5
#define		MAX_LOG_SIZE_SOCK_CLI		2048

	TCHAR			m_szLogMsg[MAX_LOG_BUFFER_SOCK_CLI][MAX_LOG_SIZE_SOCK_CLI];
	//LPTSTR			m_lpLogMsg[MAX_LOG_BUFFER_SOCK_CLI];	
	BYTE			m_byLogIndex;	
	LPTSTR			GetLogBuffer			(UINT_PTR nBufferSize);
	void			ReleaseLogBuffer		();

	void _cdecl		SendLogMsg				(LPCTSTR lpszFormat, ...);
	void _cdecl		SendErrorMsg			(LPCTSTR lpszFormat, ...);
	
	DWORD			_tinet_addr				(__in const TCHAR *cp);

public:

	virtual void	MakeSemaphore			(LPCTSTR lpName, DWORD dwWaitDelay = 20);
	void			ResetSemaphore			();

	virtual void	SetDeviceType			(UINT nDeviceType)
	{
		m_nDeviceType = nDeviceType;
	};
	bool			IsConnected				() { return m_SocketClient->IsOpen();};
	int				GetLastErrorCode		() { return m_dwErrCode; };

	//-----------------------------------------------------
	// 오너 윈도우, 모니터링 윈도우 메세지
	//-----------------------------------------------------
	BOOL			SetOwnerHWND			(HWND hOwnerWnd);	
	void			SetWmLog				(UINT nWindowsMessage);	
	void			SetExitEvent			(HANDLE hExitEvent);
	void			SetWmRecv				(UINT nWindowMessage);

	//-----------------------------------------------------
	// 통신 상태 알림 윈도우 메세지
	//-----------------------------------------------------
	void			SetWmCommStatus			(UINT nWindowsMessage){m_nWM_COMM_STATUS = nWindowsMessage;};

	//-----------------------------------------------------
	// SocketClient Interface handler
	//-----------------------------------------------------
	virtual void	OnThreadBegin			(CSocketHandle* pSH);
	virtual void	OnThreadExit			(CSocketHandle* pSH);
	virtual void	OnDataReceived			(CSocketHandle* pSH, const BYTE* pbData, DWORD dwCount, const SockAddrIn& addr);
	virtual void	OnConnectionDropped		(CSocketHandle* pSH);
	virtual void	OnConnectionError		(CSocketHandle* pSH, DWORD dwError);

	//-----------------------------------------------------
	// 연결, 해제, 전송, 수신
	//-----------------------------------------------------
	virtual BOOL	ConnectServer			(LPCTSTR pszAddr, LPCTSTR pszPort, LPCTSTR pzNIC_Addr = NULL);
	virtual BOOL	ConnectServer			();
	virtual BOOL	ConnectServerThread		();
	virtual BOOL	DisconnectSever			();
	BOOL			SendString				(LPCTSTR lpszString);
	void			SetAddress				(LPCTSTR pszAddr, LPCTSTR pszPort, LPCTSTR pzNIC_Addr = NULL);
	void			SetAddress				(DWORD dwIPAddr, UINT nPort, DWORD dwNIC_IPAddr = 0);

	// 접속될때까지 계속 접속시도
	void			ContinuousConnectServer	();
	// 접속 될때까지 대기
	BOOL			WaitConnection			(DWORD dwTimeOut = INFINITE);
	//-----------------------------------------------------
	// 모니터링 설정 관련
	//-----------------------------------------------------
	void			SetReconnectCycle		(DWORD dwMiliseconds);	
	void			SetAckTimeout			(DWORD dwMiliseconds);

	//-----------------------------------------------------
	// 데이터 전송
	//-----------------------------------------------------
	virtual BOOL	SendData				(const char* lpBuffer, DWORD dwCount);

	void			StopWaitACK				();

	void			SetExitProgramFlag		();
};
#endif // BSocketClient_h__