﻿//*****************************************************************************
// Filename	: 	ClientInfo.h
// Created	:	2014/7/28 - 18:36
// Modified	:	2014/7/28 - 18:36
//
// Author	:	PiRing
//	
// Purpose	:	서버에 접속된 클라이언트 정보 처리용 클래스 
//*****************************************************************************
#ifndef ClientInfo_h__
#define ClientInfo_h__

#pragma once

#include "afxwin.h"

typedef struct _stClientUnit
{	
	SOCKET		Sock;
	UINT		ClientType;	// 고유 ID
	DWORD		Address;
	UINT		Port;

	_stClientUnit ()
	{
		Reset ();
	};

	void Reset ()
	{
		Sock		= NULL;
		ClientType	= 0;
		Address		= 0;
		Port		= 0;
	};

}ST_ClientUnit;

//=============================================================================
//
//=============================================================================
class CClientInfo
{
public:
	CClientInfo(void);
	~CClientInfo(void);

protected:	
	CArray<ST_ClientUnit, ST_ClientUnit&> m_arClients;
	
	UINT			m_nConnectedClientCount;
	UINT			m_nDefinedClientCount;
		
public:

	UINT		GetConnectedClientCount	()
	{
		return m_nConnectedClientCount;
	};

	UINT		GetDefinedClientCount ()
	{
		return m_nDefinedClientCount;
	};

	BOOL		AddDefineClient			(DWORD dwAddress, UINT nClientType);

	BOOL		AddConnectedClient		(SOCKET newSocket, DWORD dwAddress);
	BOOL		RemoveConnectedClient	(SOCKET newSocket, DWORD dwAddress);
	void		RemoveAllClient			();
	
	int			GetClientIndex_ByAddr	(DWORD dwAddr);
	int			GetClientIndex_BySocket	(SOCKET Socket);

	int			GetClientType_ByAddr	(DWORD dwAddr);
	int			GetClientType_BySocket	(SOCKET Socket);

	SOCKET		GetClientSocket_ByAddr	(DWORD dwAddr);
	SOCKET		GetClientSocket_ByType	(UINT nClientType);

	BOOL		GetClientInfo_ByAddr	(DWORD dwAddr, __out ST_ClientUnit& stClient);
	BOOL		GetClientInfo_BySocket	(SOCKET Socket, __out ST_ClientUnit& stClient);
	BOOL		GetClientInfo_ByType	(UINT nClientType, __out ST_ClientUnit& stClient);
	
};

#endif // ClientInfo_h__

