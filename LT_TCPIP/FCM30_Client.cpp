﻿//*****************************************************************************
// Filename	: 	FCM30_Client.cpp
// Created	:	2016/5/10 - 15:31
// Modified	:	2016/05/31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "FCM30_Client.h"
#include <strsafe.h>

CFCM30_Client::CFCM30_Client()
{
	m_hTimerQueue		= NULL;
	m_hTimer_PingCheck	= NULL;

	m_dwPingCycle		= 1000;
	m_dwPinigAckTimeout = 180000;
	m_dwAckTimeout		= 3000;

	m_bRecvedPing		= TRUE;
	m_dwPingCheckTime	= 0;
	m_bPingEable		= TRUE;

	CreateTimerQueue_Mon();
	CreateTimer_PingCheck();
}

CFCM30_Client::~CFCM30_Client()
{
	DeleteTimer_PingCheck();
	DeleteTimerQueue_Mon();
}

void CFCM30_Client::OnThreadBegin(CSocketHandle* pSH)
{
	__super::OnThreadBegin(pSH);
	//CreateTimer_PingCheck();	
}

void CFCM30_Client::OnThreadExit(CSocketHandle* pSH)
{
	__super::OnThreadExit(pSH);
	//DeleteTimer_PingCheck();
}

void CFCM30_Client::OnConnectionDropped(CSocketHandle* pSH)
{
	__super::OnConnectionDropped(pSH);
	//DeleteTimer_PingCheck();
}

void CFCM30_Client::OnConnectionError(CSocketHandle* pSH, DWORD dwError)
{
	__super::OnConnectionError(pSH, dwError);
	//DeleteTimer_PingCheck();
}


//=============================================================================
// Method		: GetSendProtocol
// Access		: public  
// Returns		: TCP_IP::ST_Protocol
// Qualifier	:
// Last Update	: 2018/9/19 - 17:18
// Desc.		:
//=============================================================================
TCP_IP::ST_Protocol CFCM30_Client::GetSendProtocol()
{
	return m_stSendProtocol;
}

//=============================================================================
// Method		: GetRecvProtocol
// Access		: public  
// Returns		: TCP_IP::ST_Protocol
// Qualifier	:
// Last Update	: 2018/9/19 - 17:18
// Desc.		:
//=============================================================================
TCP_IP::ST_Protocol CFCM30_Client::GetRecvProtocol()
{
	return m_stRecvProtocol;
}

//=============================================================================
// Method		: PasingRecvAck
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwReaded
// Qualifier	:
// Last Update	: 2016/5/31 - 18:39
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::PasingRecvAck(const char* lpBuffer, DWORD dwReaded)
{
	ASSERT(NULL != lpBuffer);
	BOOL	bReturn = TRUE;

	__try
	{
		int			nProtocolSize = 0;
		DWORD		dwMinProtocolSize = FCM_MinProtocolSize;	//최소
		DWORD		dwMaxProtocolSize = FCM_MaxProtocolSize;	//최대 (25000 ~ 35000 ??)
		DWORD_PTR	dwQueueSize = 0;
		int			iFindEtxIndex = 0;
		//char	szBuf[4096] = {0,};
		dwQueueSize = m_SerQueue.GetSize();
		//char*	pszBuf = new char[dwQueueSize + dwReaded + 1];
		//memset(pszBuf, 0, dwQueueSize + dwReaded + 1);

		//-----------------------------------------------------
		// Data가 들어오면 우선 큐에 집어 넣는다?
		//-----------------------------------------------------
		m_SerQueue.Push(lpBuffer, dwReaded);

		//TRACE (_T("QueueSize : %d\n"), m_SerQueue.GetSize());		
		// ************* 루프 Start ***************************
		BOOL bLoop = TRUE;
		while (bLoop)
		{
			dwQueueSize = m_SerQueue.EraseUntilFindDelimiter((char)FCM_STX);

			// 데이터 크기가 프로토콜 크기보다 작으면 리턴.
			if (dwQueueSize < dwMinProtocolSize)
			{
				bReturn = FALSE;
				break;
			}
			else //if (dwQueueSize >= dwMinProtocolSize) // ETX 찾고 리턴
			{
				// ETX 찾을건가?
				iFindEtxIndex = (int)m_SerQueue.FindDelimiter((char)FCM_ETX, 0);

				if ((int)(dwMinProtocolSize - 1) <= iFindEtxIndex) // 정상
				{
					nProtocolSize = iFindEtxIndex + 1;

					//m_SerQueue.PopData(pszBuf, (DWORD)nProtocolSize);
					m_SerQueue.PopData(m_szACKBuf, (DWORD)nProtocolSize);

					// 데이터 전송하고					
					OnRecvACK(m_szACKBuf, nProtocolSize);
					bReturn = TRUE;
					//break;
					continue;
				}
				else if (iFindEtxIndex < 0) // ETX 못 찾음
				{
					if (dwMaxProtocolSize < dwQueueSize)
					{
						m_SerQueue.Pop(); // 앞에 STX 삭제
						continue;// 루프 계속
					}
					else // dwMinProtocolSize <= dwQueueSize <= dwMaxProtocolSize
					{
						bReturn = FALSE;
						break;
					}
				}
				else // 프로토콜 오류 (0 ~ dwMinProtocolSize 에서 ETX 찾음)
				{
					m_SerQueue.Empty();
					bReturn = FALSE;
					break;
				}
			}
		}// While ()

		//delete[] pszBuf;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		bReturn = FALSE;
		SendErrorMsg(_T("*** Exception Error : CRemote_Ctrl::PasingRecvAck()"));
	}

	return bReturn;
}

//=============================================================================
// Method		: OnRecvACK
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwReaded
// Qualifier	:
// Last Update	: 2017/4/13 - 18:47
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::OnRecvACK(const char* lpBuffer, DWORD dwReaded)
{
	m_dwACKBufSize = dwReaded;
	m_szACKBuf[m_dwACKBufSize] = 0x00;

	if (sizeof(ST_Protocol) <= dwReaded)
		memcpy(&m_stRecvProtocol, lpBuffer, sizeof(ST_Protocol));

	if (FCM_MinProtocolSize <= dwReaded)
		m_stRecvProtocol.SetRecvProtocol(lpBuffer, dwReaded); //재분리 적용

	if (NULL != m_hOwnerWnd)
	{
		//::SendNotifyMessage(m_hOwnerWnd, m_WM_Recv, (WPARAM)m_szACKBuf, (LPARAM)MAKELONG((WORD)m_dwACKBufSize, (WORD)m_nDeviceType));
		::SendMessage(m_hOwnerWnd, m_WM_Recv, (WPARAM)m_szACKBuf, (LPARAM)MAKELONG((WORD)m_dwACKBufSize, (WORD)m_nDeviceType));

		//Log
		CString strText;
		USES_CONVERSION;

		LPTSTR pszText = strText.GetBuffer((int)(m_dwACKBufSize + 1));
		::StringCchCopyN(pszText, m_dwACKBufSize + 1, A2CT(reinterpret_cast<LPCSTR>(lpBuffer)), m_dwACKBufSize);
		strText.ReleaseBuffer();
	}

	// 이벤트 핸들이 설정되어있다면 이벤트를 발생시킨다.
	if (NULL != m_hACKEvent)
		SetEvent(m_hACKEvent);

	return TRUE;
}

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/8 - 14:30
// Desc.		:
//=============================================================================
void CFCM30_Client::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/8 - 14:36
// Desc.		:
//=============================================================================
void CFCM30_Client::DeleteTimerQueue_Mon()
{
	__try
	{
		//DeleteTimer_TimeCheck();
		//DeleteTimer_UpdateUI();

		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CTestManager_Base::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimer_PingCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/8 - 14:36
// Desc.		:
//=============================================================================
void CFCM30_Client::CreateTimer_PingCheck()
{
	__try
	{
		// Time Check Timer
		m_bRecvedPing = TRUE;

		if (NULL == m_hTimer_PingCheck)
			if (!CreateTimerQueueTimer(&m_hTimer_PingCheck, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine_PingCheck, (PVOID)this, m_dwPingCycle, m_dwPingCycle, WT_EXECUTEDEFAULT))
			{
				TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
			}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::CreateTimer_TimeCheck()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimer_PingCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/8 - 14:36
// Desc.		:
//=============================================================================
void CFCM30_Client::DeleteTimer_PingCheck()
{
	__try
	{
		if (NULL != m_hTimer_PingCheck)
		{
			if (DeleteTimerQueueTimer(m_hTimerQueue, m_hTimer_PingCheck, NULL))
			{
				m_hTimer_PingCheck = NULL;
			}
			else
			{
				TRACE(_T("DeleteTimerQueueTimer : m_hTimer_TimeCheck_Mon failed (%d)\n"), GetLastError());
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CTestManager_Base::DeleteTimer_TimeCheck()\n"));
	}

	TRACE(_T("타이머 종료 : CTestManager_Base::DeleteTimer_TimeCheck()\n"));
}

//=============================================================================
// Method		: TimerRoutine_PingCheck
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2016/10/8 - 14:36
// Desc.		:
//=============================================================================
VOID CALLBACK CFCM30_Client::TimerRoutine_PingCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CFCM30_Client* pThis = (CFCM30_Client*)lpParam;

	pThis->OnMonitor_PingCheck();
}

//=============================================================================
// Method		: OnMonitor_PingCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/8 - 14:36
// Desc.		:
//=============================================================================
void CFCM30_Client::OnMonitor_PingCheck()
{
	// 연결 상태 체크
	if (this->IsConnected())
	{
		// Ping 전송
		if (m_bPingEable)
		{
			//Send_Ping((enParaIndex)(Para_A + m_nDeviceType));
		}

		// Ping 회신 & 타임 체크
		if (m_bRecvedPing)
		{
			m_dwPingCheckTime = 0;
		}
		else
		{
			// Time 체크
			m_dwPingCheckTime += m_dwPingCycle;

			if (m_dwPinigAckTimeout < m_dwPingCheckTime)
			{
				m_bRecvedPing = TRUE;
				// Ping ACK Timeout
				::SendNotifyMessage (m_hOwnerWnd, m_nWM_COMM_STATUS, (WPARAM)m_nDeviceType, (LPARAM)COMM_PING_TIMEOUT);
			}
		}
	}
}

//=============================================================================
// Method		: SendData
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwCount
// Qualifier	:
// Last Update	: 2016/10/24 - 11:31
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::SendData(const char* lpBuffer, DWORD dwCount)
{
	if (__super::SendData(lpBuffer, dwCount))
	{
		//Log
 		CString strText;
		USES_CONVERSION;

 		LPTSTR pszText = strText.GetBuffer(dwCount + 1);
 		::StringCchCopyN(pszText, dwCount + 1, A2CT(reinterpret_cast<LPCSTR>(lpBuffer)), dwCount);
 		strText.ReleaseBuffer();
// 
 		TRACE(_T("[H->T:%c] %s\n"), (char)(m_nDeviceType), strText);
// 		SendLogMsg(_T("[H->T:%c] %s"), (char)(Para_A + m_nDeviceType), strText);

		return TRUE;
	}
	else
	{
		return FALSE;
	}

}

//=============================================================================
// Method		: ResetAck
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/8 - 14:35
// Desc.		:
//=============================================================================
void CFCM30_Client::ResetAck()
{
// 	m_dwACKBufSize = 0;
// 	ZeroMemory(m_szACKBuf, 1024);
// 
// 	m_SerQueue.Empty();
// 
// 	m_stRecvProtocol.szProtocol.Empty();
// 	m_stRecvProtocol.szLotID.Empty();
// 	m_stRecvProtocol.szLotTryCount.Empty();
// 
// 	ResetEvent(m_hACKEvent);
// 	ResetEvent(m_hAbortACKEvent);
}

//=============================================================================
// Method		: Send_Ping
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/10/6 - 10:00
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::Send_Ping()
{
	BOOL bResult = TRUE;

	// 동기화 처리 ----------------------------------------
// 	if (!WaitSemaphore())
// 	{
// 		SendLogMsg(_T("동기화 대기 실패 (Error Code : %d "), m_dwErrCode);
// 		return FALSE;
// 	}

	// 전송
	if (SendData(g_szSend_Ping, FCM_ProtocolSize))
	{
		m_bRecvedPing = FALSE;
	}
	else
	{
		SendLogMsg(_T("Send_Ping Error : Transfer failed!!"));
		bResult = FALSE;
	}

//	UnlockSemaphore();

	return bResult;
}

//=============================================================================
// Method		: Send_GetData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in CString szBarcode
// Qualifier	:
// Last Update	: 2018/9/19 - 14:52
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::Send_GetData(__in CString szBarcode)
{
	BOOL bResult = TRUE;

	// 동기화 처리 ----------------------------------------
	if (!WaitSemaphore())
	{
		SendLogMsg(_T("동기화 대기 실패 (Error Code : %d "), m_dwErrCode);
		return FALSE;
	}

	m_stSendProtocol.Init();
	m_stSendProtocol.MakeProtocol(szBarcode);

	// 전송
	if (SendData(m_stSendProtocol.Protocol.GetBuffer(), m_stSendProtocol.Protocol.GetLength()))
	{

	}
	else
	{
		SendLogMsg(_T("Send_Get_TesterStatus Error : Transfer failed!!"));
		bResult = FALSE;
	}

	UnlockSemaphore();

	return bResult;
}

//=============================================================================
// Method		: Recv_Ping_Ack
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/10/8 - 19:26
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::Recv_Ping_Ack()
{
	m_bRecvedPing = TRUE;

	return TRUE;
}

//=============================================================================
// Method		: Recv_Set
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/4/13 - 18:44
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::Recv_Set()
{
	return TRUE;
}

//=============================================================================
// Method		: Recv_Get
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/4/13 - 18:44
// Desc.		:
//=============================================================================
BOOL CFCM30_Client::Recv_Get()
{
	return TRUE;
}
