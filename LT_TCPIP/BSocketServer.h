﻿//*****************************************************************************
// Filename	: BSocketServer.h
// Created	: 2010/12/7
// Modified	: 2010/12/7 - 11:53
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef BSocketServer_h__
#define BSocketServer_h__

#pragma once
#include "SocketHandle.h"
#include "Socketserverimpl.h"
#if defined(SOCKHANDLE_USE_OVERLAPPED)
#include "AsyncSocketServerImpl.h"
#endif

typedef enum
{
	SERV_NOTCONNECT	= 0,
	SERV_STARTED,
	SERV_STOPED,
	SERV_CONNECT_DROP,
	SERV_CONNECT_ERROR,
	SERV_ADD_CONNECTION,
	SERV_DEL_CONNECTION,
	SERV_CONNECTION_FAIL,
}enumServerState;

typedef std::list<SOCKET> CSocketList;

//=============================================================================
// CBSocketServer
//=============================================================================
class CBSocketServer : public ISocketServerHandler
{
#if defined(SOCKHANDLE_USE_OVERLAPPED)
	typedef ASocketServerImpl<ISocketServerHandler> CSocketServer;
#else
	typedef SocketServerImpl<ISocketServerHandler> CSocketServer;
#endif

	friend CSocketServer;

public:
	CBSocketServer(void);
	virtual ~CBSocketServer(void);


protected:

	CSocketServer	m_SocketServer;

	//-----------------------------------------------------
	// 소켓 처리 관련
	//-----------------------------------------------------
	int				m_nMode;				// AF_IPV4, AF_INET6
	int				m_nSockType;
	CString			m_strServerIP;
	CString			m_strPort;				// 서버 포트

	//-----------------------------------------------------
	// 윈도우 핸들 / 윈도우 메세지
	//-----------------------------------------------------
	HWND			m_hOwnerWnd;			// 오너의 윈도우 핸들
	UINT			m_nWM_LOG;				// 로그 출력용 윈도우 메세지	
	UINT			m_nWM_Recv;
	UINT			m_nWM_ServerStatus;
	BYTE			m_byDeviceType;
	//-----------------------------------------------------
	// 에러 처리 / 동기화 처리
	//-----------------------------------------------------
	DWORD			m_dwErrCode;			// 오류 코드
	HANDLE			m_hSemaphore;
	DWORD			m_dwSemaphoreWaitTime;

	DWORD			LockSemaphore			();
	BOOL			UnlockSemaphore			();
	BOOL			WaitSemaphore			(DWORD dwTimeout = 2000);

	//-----------------------------------------------------
	// 이벤트, 쓰레드 처리
	//-----------------------------------------------------
	HANDLE			m_hExternalExitEvent;	// 외부의 통신 접속 쓰레드 종료 이벤트
	HANDLE			m_hACKEvent;			// 응답데이터 수신시 이벤트
	DWORD			m_dwAckTimeout;			// 커맨드 전송후, 응답 올때까지의 타임아웃 시간

	enum _WAIT_ACK_RESULT
	{
		ACK_TIMEOUT		= 0,
		ACK_READ_PASS,
		ACK_READ_ERROR,
		ACK_WRITE_PASS,
		ACK_WRITE_ERROR,
	};
	// 응답을 기다림
	BOOL			WaitACK				(DWORD dwWaitTime = 1000, LPDWORD  dwOutBufSize = NULL);
	virtual BOOL	PasingRecvAck		(const char* lpBuffer, DWORD dwReaded, const SockAddrIn& addr, SOCKET sock);

	void			GetAddress			(const SockAddrIn& addrIn, CString& rString) const;	
	void			GetAddress			(const SockAddrIn& addrIn, LPTSTR lpOutBuff, UINT nBuffSize) const;
	bool			GetDestination		(SockAddrIn& addrIn) const;

#define		MAX_LOG_BUFFER_SOCK_SRV		3
#define		MAX_LOG_SIZE_SOCK_SRV		2048

	TCHAR			m_szLogMsg[MAX_LOG_BUFFER_SOCK_SRV][MAX_LOG_SIZE_SOCK_SRV];
	//LPTSTR			m_lpLogMsg[MAX_LOG_BUFFER_SOCK_SRV];
	BYTE			m_byLogIndex;	
	LPTSTR			GetLogBuffer		(UINT_PTR nBufferSize);
	void			ReleaseLogBuffer	();

	void _cdecl		SendLogMsg			(LPCTSTR lpszFormat, ...);
	void _cdecl		SendErrorMsg		(LPCTSTR lpszFormat, ...);

	DWORD			_tinet_addr			(__in const TCHAR *cp);

public:

	//-----------------------------------------------------
	// ISocketServerHandler
	//-----------------------------------------------------
	virtual void OnThreadBegin			(CSocketHandle* pSH);
	virtual void OnThreadExit			(CSocketHandle* pSH);
	virtual void OnAddConnection		(CSocketHandle* pSH, SOCKET newSocket);
	virtual void OnRemoveConnection		(CSocketHandle* pSH, SOCKET oldSocket);
	virtual void OnDataReceived			(CSocketHandle* pSH, const BYTE* pbData, DWORD dwCount, const SockAddrIn& addr, SOCKET sock);
	virtual void OnConnectionFailure	(CSocketHandle* pSH, SOCKET newSocket);
	virtual void OnConnectionDropped	(CSocketHandle* pSH);
	virtual void OnConnectionError		(CSocketHandle* pSH, DWORD dwError);


	bool			IsOpened			() { return m_SocketServer->IsOpen();};
	int				GetLastErrorCode	() { return m_dwErrCode; };
	
	void			SetExitEvent		(HANDLE hExitEvent);
	
	void			SetServerIP			( LPCTSTR szAddr)
	{
		m_strServerIP = szAddr;
	};
	void			SetPort				( UINT nPort );
	//-----------------------------------------------------
	// 오너 윈도우, 모니터링 윈도우 메세지
	//-----------------------------------------------------
	BOOL			SetOwnerHWND		(HWND hOwnerWnd);	
	void			SetWmLog			(UINT nWindowsMessage);
	void			SetWmRecv			(UINT nWindowMessage);
	//void			SetExitEvent		(HANDLE hExitEvent);
	// 통신 상태 알림 윈도우 메세지	
	void			SetWmCommStatus		(UINT nWindowsMessage){m_nWM_ServerStatus = nWindowsMessage;};
	void			SetDeviceType		(BYTE byDeviceType){m_byDeviceType = byDeviceType;};

	//-----------------------------------------------------
	// 연결, 해제, 전송, 수신
	//-----------------------------------------------------
	virtual BOOL	StartServer			(LPCTSTR pszHost, LPCTSTR pszPort);

	virtual BOOL	StartServer			(LPCTSTR pszPort);
	virtual BOOL	StartServer			();
	virtual BOOL	StopServer			();
	BOOL			SendStringToAll		(LPCTSTR lpszString);
	BOOL			SendDataToAll		(const char* lpBuffer, DWORD dwCount);
	BOOL			SendString			(SOCKET sock, LPCTSTR lpszString);
	BOOL			SendData			(SOCKET sock, const char* lpBuffer, DWORD dwCount);
	//-----------------------------------------------------
	// 모니터링 설정 관련
	//-----------------------------------------------------		
	void			SetAckTimeout		(DWORD dwMiliseconds);
	
};

#endif // BSocketServer_h__
