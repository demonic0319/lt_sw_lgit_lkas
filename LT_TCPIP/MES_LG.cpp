﻿//*****************************************************************************
// Filename	: 	MES_LG.cpp
// Created	:	2016/5/10 - 15:31
// Modified	:	2016/05/31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "StdAfx.h"
#include "MES_LG.h"

CMES_LG::CMES_LG()
{
}


CMES_LG::~CMES_LG()
{
}

//=============================================================================
// Method		: PasingRecvAck
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwReaded
// Qualifier	:
// Last Update	: 2016/5/31 - 18:39
// Desc.		:
//=============================================================================
BOOL CMES_LG::PasingRecvAck(const char* lpBuffer, DWORD dwReaded)
{
	if (NULL == lpBuffer)
	{
		ASSERT(_T("NULL == szACK"));
		m_dwErrCode = ERROR_INVALID_PARAMETER;
		TRACE(_T("FilterAckData() : %d\n"), __LINE__);
		return FALSE;
	}

	//ASSERT(0 != dwReaded);
	if (0 == dwReaded)
		return FALSE;


	DWORD_PTR	dwQueueSize = 0;
	DWORD_PTR	dwDataLength = 0;

	INT_PTR		iFindSTXIndex = 0;
	INT_PTR		iFindETXIndex = 0;

	//ResetAckBuffer();
	m_dwACKBufSize = 0;

	//-----------------------------------------------------
	// Data가 들어오면 우선 큐에 집어 넣는다?
	//-----------------------------------------------------
	m_SerQueue.Push(lpBuffer, dwReaded);

	//-----------------------------------------------------
	// STX를 찾는다.
	//-----------------------------------------------------
	if (0 <= (iFindSTXIndex = m_SerQueue.EraseUntilFindDelimiter(LG_MES_STX)))
	{
		//-----------------------------------------------------
		// ETX를 찾는다.
		//-----------------------------------------------------
		dwQueueSize = m_SerQueue.GetSize();

		// 프로토콜 사이즈보다 작으면 데이터가 들어오길 기다린다.
		if (dwQueueSize < (DWORD)LG_MES_MinProtocolSize)
		{
			TRACE(_T("FilterAckData() : %d\n"), __LINE__);
			return FALSE;
		}

		iFindETXIndex = 0;
		while (0 < (iFindETXIndex = m_SerQueue.FindDelimiter(LG_MES_ETX, (UINT)iFindETXIndex)))
		{
			// ETX 위치가 프로토콜 마지막
			if (LG_MES_DELIMETER_CNT == m_SerQueue.FindDelimiterCounter(LG_MES_DELIMETER, 0, (UINT)iFindETXIndex))
			{
				dwDataLength = (DWORD)iFindETXIndex + 1;
				m_SerQueue.PopData(m_szACKBuf, (DWORD)iFindETXIndex + 1);
				break;
			}
			else
			{
				++iFindETXIndex; // ETX 찾은 위치 다음부터 찾는다.(무한 루프 방지)
				continue;
			}
		}

		if (iFindETXIndex <= 0) // STX를 찾고 ETX를 못찾으면 데이터가 중간에 나뉘어 들어온걸로 판단함
		{
			// STX ~> ETX 까지 검색한 데이터 사이즈가 프로토콜 사이즈보다 넘어가면 다음 데이터가 들어오면 처리
			if (dwQueueSize < (DWORD)LG_MES_MaxProtocolSize)
			{
				//TRACE(_T("FilterAckData() : %d\n"), __LINE__);
				return FALSE;
			}
			else // 아니면 에러 처리
			{
				m_SerQueue.Empty();
				TRACE(_T("FilterAckData() : %d\n"), __LINE__);
				return FALSE;
			}
		}
	}
	else // STX가 없으면 쓰레기 데이터로 간주하고 처리	
	{
		// 오류 : 쓰레기 데이터
		m_SerQueue.Empty();
		TRACE(_T("FilterAckData() : %d\n"), __LINE__);
		return FALSE;
	}

	m_dwACKBufSize = dwDataLength;
	m_szACKBuf[m_dwACKBufSize] = 0x00;
	m_stRecvProtocol.SetRecvProtocol(m_szACKBuf, dwDataLength);

	
	TRACE("CMES_LG::PasingRecvAck -> %s\n", lpBuffer);
	TRACE("CMES_LG::PasingRecvAck -> %s\n", m_szACKBuf);
	TRACE("CMES_LG::PasingRecvAck -> %s\n", m_stRecvProtocol.szLotID);

	// 이벤트 핸들이 설정되어있다면 이벤트를 발생시킨다.
	if (NULL != m_hACKEvent)
		SetEvent(m_hACKEvent);

	TRACE(_T("FilterAckData : Complete Ack Protocol\n"));
	if (NULL != m_hOwnerWnd)
	{
		::SendNotifyMessage(m_hOwnerWnd, m_WM_Recv, (WPARAM)m_szACKBuf, (LPARAM)m_dwACKBufSize);
	}

	//-----------------------------------------------------
	// 큐 비우기
	//-----------------------------------------------------
	m_SerQueue.Empty();
	m_dwErrCode = NO_ERROR;

	return TRUE;
}

//=============================================================================
// Method		: GetLotID
// Access		: public  
// Returns		: BOOL
// Parameter	: CString & szLotID
// Parameter	: CString & szOvlCount
// Parameter	: DWORD dwTimeOut
// Qualifier	:
// Last Update	: 2016/5/31 - 18:40
// Desc.		:
//=============================================================================
BOOL CMES_LG::GetLotID(CString& szLotID, CString& szOvlCount, DWORD dwTimeOut /*= 5000*/)
{
	if (WaitACK(dwTimeOut))
	{
		USES_CONVERSION;
		szLotID.Format(_T("%s"), A2T(m_stRecvProtocol.szLotID.GetBuffer()));
		m_stRecvProtocol.szLotID.ReleaseBuffer();
		szOvlCount.Format(_T("%s"), A2T(m_stRecvProtocol.szLotTryCount.GetBuffer()));
		m_stRecvProtocol.szLotTryCount.ReleaseBuffer();

		TRACE("CMES_LG::GetLotID -> %s\n", m_stRecvProtocol.szLotID);

		TRACE(_T("CMES_LG::GetLotID -> %s\n"), szLotID);

		return TRUE;
	}

	return FALSE;
}

void CMES_LG::ResetAck()
{
	m_dwACKBufSize = 0;
	ZeroMemory(m_szACKBuf, 1024);

	m_SerQueue.Empty();

	m_stRecvProtocol.szProtocol.Empty();
	m_stRecvProtocol.szLotID.Empty();
	m_stRecvProtocol.szLotTryCount.Empty();

	ResetEvent(m_hACKEvent);
	ResetEvent(m_hAbortACKEvent);
}