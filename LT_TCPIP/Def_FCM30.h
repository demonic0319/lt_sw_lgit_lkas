﻿//*****************************************************************************
// Filename	: 	Def_FCM30.h
// Created	:	2016/9/6 - 10:10
// Modified	:	2016/9/6 - 10:10
//
// Author	:	PiRing
//	
// Purpose	:	LG Electronics
//*****************************************************************************
#ifndef Def_FCM30_h__
#define Def_FCM30_h__

namespace TCP_IP
{	
	typedef struct _tag_TCPPacket
	{
		int		nPingStatus;	// Ready, AckError, NoAck
		bool	bSendInfo;		// Send Information
		int		nAckStatus;		// timeout
	}ST_TCPPacket;

	typedef enum ConstVar
	{
		FCM_STX					= '#',
		FCM_ETX					= '@',
		FCM_Delimiter			= '|',
		FCM_Blank				= 0x20,	// ' '
		FCM_DataLength			= 41,
		FCM_MinProtocolSize		= 70,
		FCM_MaxProtocolSize		= 70,
		FCM_ProtocolSize		= 25,
	};	

	typedef struct _tag_Protocol
	{
		char			STX;					
		CStringA		Command;
		CStringA		Barcode;
		
		CStringA		Data;
		CStringA		Result;			// 결과

		double 			AlignX;			// 데이터
		double 			AlignY;			// 데이터
		double 			AlignTh;		// 데이터
		double 			InputTx;		// 데이터
		double 			InputTy;		// 데이터
		double 			OutputTx;		// 데이터
		double 			OutputTy;		// 데이터

		char			ETX;

		CStringA		Protocol;

		_tag_Protocol()
		{
			Init();
		};

		void Init()
		{
			STX			= FCM_STX;
			Command.Empty();
			
			Barcode.Empty();
			Result.Empty();		
			
			AlignX		= 0.0;
			AlignX		= 0.0;
			AlignY		= 0.0;
			AlignTh		= 0.0;
			InputTx		= 0.0;
			InputTy		= 0.0;
			OutputTx	= 0.0;
			OutputTy	= 0.0;

			Data.Empty();
			Protocol.Empty();

			ETX			= FCM_ETX;
		};

		_tag_Protocol& operator= (_tag_Protocol& ref)
		{
			STX			= ref.STX;
			Command		= ref.Command;
			Barcode		= ref.Barcode;
			Data		= ref.Data;
			Result		= ref.Result;
			AlignX		= ref.AlignX;
			AlignY		= ref.AlignY;
			AlignTh		= ref.AlignTh;
			InputTx		= ref.InputTx;
			InputTy		= ref.InputTy;
			OutputTx	= ref.OutputTx;
			OutputTy	= ref.OutputTy;
			ETX			= ref.ETX;
			Protocol	= ref.Protocol;

			return *this;
		};

		//=============================================================================
		// Method		: GetTokenizeValue
		// Access		: public  
		// Returns		: INT_PTR
		// Parameter	: __out CStringArray & szarValue
		// Qualifier	:
		// Last Update	: 2017/7/20 - 16:15
		// Desc.		:
		//=============================================================================
		INT_PTR GetTokenizeValue(__out CStringArray& szarValue)
		{
			szarValue.RemoveAll();

			CStringA szData = Data;
			CStringA szResToken;

			int curPos = 0;
			szResToken = szData.Tokenize(",\0", curPos);

			USES_CONVERSION;
			while (szResToken != "")
			{
				szarValue.Add(A2T(szResToken));
				szResToken = szData.Tokenize(",\0", curPos);
			};

			return szarValue.GetCount();
		};

		//=============================================================================
		// Method		: MakeProtocol
		// Access		: public  
		// Returns		: void
		// Parameter	: __in CString szBarcode
		// Qualifier	:
		// Last Update	: 2018/9/19 - 14:20
		// Desc.		:
		//=============================================================================
		void MakeProtocol(__in CString szBarcode)
		{
			Command = "GET_DATA";
			Barcode = szBarcode;

			Protocol.Empty();

			Protocol.AppendChar(STX);
			Protocol.AppendChar(FCM_Delimiter);
			Protocol.Append(Command);
			Protocol.AppendChar(FCM_Delimiter);
			Protocol.Append(Barcode);
			Protocol.AppendChar(FCM_Delimiter);
			Protocol.AppendChar(ETX);
		};

		//=============================================================================
		// Method		: AnalyzeData_Insp
		// Access		: public  
		// Returns		: void
		// Qualifier	:
		// Last Update	: 2018/9/19 - 14:35
		// Desc.		:
		//=============================================================================
		void AnalyzeData_Data()
		{
			// AlignX,AlignY,AlignTh,내부변위Tx,내부변위Ty,외부변위Tx,외부변위Ty
			CStringArray szarDataz;

			if (6 <= GetTokenizeValue(szarDataz))
			{
				AlignX		= _ttof(szarDataz.GetAt(0));
				AlignY		= _ttof(szarDataz.GetAt(1));
				AlignTh		= _ttof(szarDataz.GetAt(2));
				InputTx		= _ttof(szarDataz.GetAt(3));
				InputTy		= _ttof(szarDataz.GetAt(4));
				OutputTx	= _ttof(szarDataz.GetAt(5));
				OutputTy	= _ttof(szarDataz.GetAt(6));

				// 몰라...ㅈㅅ
				AlignX		+= 0.0001;
				AlignY		+= 0.0001;
				AlignTh		+= 0.0001;
				InputTx		+= 0.0001;
				InputTy		+= 0.0001;
				OutputTx	+= 0.0001;
				OutputTy	+= 0.0001;
			}
			else
			{
				AlignX		= 0.0;
				AlignY		= 0.0;
				AlignTh		= 0.0;
				InputTx		= 0.0;
				InputTy		= 0.0;
				OutputTx	= 0.0;
				OutputTy	= 0.0;
			}
		};

		//=============================================================================
		// Method		: SetRecvProtocol
		// Access		: public  
		// Returns		: BOOL
		// Parameter	: __in const char * pszProtocol
		// Parameter	: __in UINT_PTR nLength
		// Qualifier	:
		// Last Update	: 2017/7/20 - 16:15
		// Desc.		:
		//=============================================================================
		BOOL SetRecvProtocol(__in const char* pszProtocol, __in UINT_PTR nLength)
		{
			if (NULL == pszProtocol)
			{
				TRACE(_T("Protocol is NULL! \n"));
				return FALSE;
			}

			if (nLength < FCM_MinProtocolSize)
			{
				TRACE(_T("Minimum Protocol Size Failed! \n"));
				return FALSE;
			}

			Init();

			// 아스키 형태일 경우
			CStringA strProtocol = pszProtocol;
			INT_PTR nOffset		 = 0;
			
			CStringA resToken;
			CStringA szToken;
			szToken.AppendChar(FCM_Delimiter);
			int curPos = 0;

			resToken = strProtocol.Tokenize(szToken, curPos);
			STX = resToken.GetAt(0);	// #
			nOffset++;							// | 구분자 제거

			resToken = strProtocol.Tokenize(szToken, curPos);
			Command = resToken;

			resToken = strProtocol.Tokenize(szToken, curPos);
			Barcode = resToken;

			resToken = strProtocol.Tokenize(szToken, curPos);
			Result = resToken;

			resToken = strProtocol.Tokenize(szToken, curPos);
			Data = resToken;

			ETX = strProtocol.GetAt(curPos);


			// 데이터 분활
			AnalyzeData_Data();
			
			// Check STX
			if ((char)FCM_STX != STX)
			{
				TRACE(_T("Error STX! \n"));
				return FALSE;
			}

			// Check ETX
			if ((char)FCM_ETX != ETX)
			{
				TRACE(_T("Error ETX! \n"));
				return FALSE;
			}

			return TRUE;
		};

	}ST_Protocol, *PST_Protocol;

	static LPCSTR g_szSend_Ping		= "$NHP00                                                  \r";
	static LPCSTR g_szResp_Ping		= "$HNA00                                                  \r";
	static LPCSTR g_szResp_PingErr	= "$HNA01                                                  \r";
}

#endif // Def_LGE_Nectarine_h__
