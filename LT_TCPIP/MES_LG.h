﻿//*****************************************************************************
// Filename	: 	MES_LG.h
// Created	:	2016/5/10 - 15:31
// Modified	:	2016/5/10 - 15:31
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef MES_LG_h__
#define MES_LG_h__

#pragma once

#include "BSocketClient.h"
#include "Define_MES_LG.h"
#include "SerQueue.h"

#define MAX_MES_LG_BUFFER	1024

class CMES_LG : public CBSocketClient
{
public:
	CMES_LG();
	virtual ~CMES_LG();

protected:
	virtual BOOL	PasingRecvAck			(const char* lpBuffer, DWORD dwReaded);

	CSerQueue			m_SerQueue;
	ST_LG_MES_Protocol	m_stProtocol;
	ST_LG_MES_Protocol	m_stRecvProtocol;

	char			m_szACKBuf[MAX_MES_LG_BUFFER];
	DWORD_PTR		m_dwACKBufSize;

public:

	void		Test(){};
	BOOL		GetLotID		(CString& szLotID, CString& szOvlCount, DWORD dwTimeOut = 5000);
	CStringA	GetRecvProtocol()
	{
		return m_stRecvProtocol.szProtocol;
	};

	void ResetAck();

};

#endif // MES_LG_h__

