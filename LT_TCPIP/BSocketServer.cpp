﻿//*****************************************************************************
// Filename	: BSocketServer.cpp
// Created	: 2010/12/7
// Modified	: 2010/12/7 - 14:34
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#include "StdAfx.h"
#include "BSocketServer.h"
#include "ThreadPool.hpp"
#include "ErrorCode.h"
#include <strsafe.h>

// CBSocketServer
using namespace PR_ErrorCode;

const int AF_IPV4   = 0;
const int AF_IPV6   = 1;

const int SOCK_TCP  = SOCK_STREAM-1;
const int SOCK_UDP  = SOCK_DGRAM-1;

//=============================================================================
// 생성자
//=============================================================================
CBSocketServer::CBSocketServer()
{
	m_nMode					= AF_IPV4;
	m_nSockType				= SOCK_TCP;
	m_SocketServer.SetInterface(this);

	m_strPort				= _T("5001");				// 서버 포트

	m_dwAckTimeout			= 400;

	m_hOwnerWnd				= NULL;
	m_nWM_LOG				= NULL;	
	m_nWM_ServerStatus		= NULL;
	m_byDeviceType			= 0;

	m_hExternalExitEvent	= NULL;
	m_hACKEvent				= NULL;
	m_hACKEvent				= CreateEvent( NULL, FALSE, FALSE, NULL);

	m_dwSemaphoreWaitTime	= 10;
	m_hSemaphore			= NULL;	
	m_hSemaphore			= CreateSemaphore (NULL, 1, 1, _T("BSocket Server"));

	m_byLogIndex			= 0;
	memset (m_szLogMsg, 0, sizeof(m_szLogMsg));
	//for (int iIndex = 0; iIndex < MAX_LOG_BUFFER_SOCK_SRV; iIndex++)
	//	m_lpLogMsg[iIndex] = NULL;
}

//=============================================================================
// 소멸자
//=============================================================================
CBSocketServer::~CBSocketServer()
{
	TRACE(_T("<<< Start ~CBSocketServer >>> \n"));

	StopServer();

	if (NULL != m_hACKEvent)
		CloseHandle (m_hACKEvent);

	if (NULL != m_hSemaphore)
		CloseHandle(m_hSemaphore);

	ReleaseLogBuffer();

	TRACE(_T("<<< End ~CBSocketServer >>> \n"));
}

// CBSocketServer 메시지 처리기입니다.
//#############################################################################
// Category		: 재정의 함수들
// Pupose		: CWnd, ISocketServerHandler 재정의 함수
// Last Update	: 2010/12/7 - 14:35
//#############################################################################
//=============================================================================
// Method		: CBSocketServer::OnThreadBegin
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Qualifier	:
// Last Update	: 2010/12/7 - 13:17
// Desc.		: 서버 시작 쓰레드가 시작되면 이 함수가 호출된다.
//=============================================================================
void CBSocketServer::OnThreadBegin(CSocketHandle* pSH)
{
	ASSERT( pSH == m_SocketServer );
	(pSH);
	CString strAddr;
	SockAddrIn sockAddr;
	m_SocketServer->GetSockName(sockAddr);
	GetAddress( sockAddr, strAddr );
	SendLogMsg( _T("Server Running on: %s"), strAddr);

	::SendNotifyMessage (m_hOwnerWnd, m_nWM_ServerStatus, MAKEWORD(m_byDeviceType, SERV_STARTED), (LPARAM)0);
}

//=============================================================================
// Method		: CBSocketServer::OnThreadExit
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Qualifier	:
// Last Update	: 2010/12/7 - 13:18
// Desc.		: 통신 쓰레드가 중단되면 이 함수가 호출된다.
//=============================================================================
void CBSocketServer::OnThreadExit(CSocketHandle* pSH)
{
	ASSERT( pSH == m_SocketServer );
	(pSH);
	SendLogMsg( _T("Server Down!"));
	
	::SendNotifyMessage (m_hOwnerWnd, m_nWM_ServerStatus, MAKEWORD(m_byDeviceType, SERV_STOPED), (LPARAM)0);
}

//=============================================================================
// Method		: CBSocketServer::OnConnectionFailure
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Parameter	: SOCKET newSocket
// Qualifier	:
// Last Update	: 2010/12/7 - 13:18
// Desc.		:
//=============================================================================
void CBSocketServer::OnConnectionFailure(CSocketHandle* pSH, SOCKET newSocket)
{
	ASSERT(pSH == m_SocketServer);
	(pSH);

	CSocketHandle sockHandle;
	SockAddrIn	sockAddr;
	DWORD		dwAddress = 0;
	UINT		nPort = 0;
	if (newSocket != INVALID_SOCKET)
	{
		sockHandle.Attach(newSocket);
		sockHandle.GetPeerName(sockAddr);

		TCHAR szIPAddr[MAX_PATH] = { 0 };
		CSocketHandle::FormatIP(szIPAddr, MAX_PATH, sockAddr);

		dwAddress = ntohl(_tinet_addr(szIPAddr));// String to DWORD
		nPort = static_cast<UINT>(ntohs(sockAddr.GetPort()));

		SendLogMsg(_T("Connection abandoned: %d:%d"), dwAddress, nPort);
	}
	else
	{
		SendLogMsg(_T("Connection abandoned. Not a valid socket."));
	}

	::SendNotifyMessage (m_hOwnerWnd, m_nWM_ServerStatus, MAKEWORD(m_byDeviceType, SERV_CONNECTION_FAIL), (LPARAM)dwAddress);	
}

//=============================================================================
// Method		: CBSocketServer::OnAddConnection
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Parameter	: SOCKET newSocket
// Qualifier	:
// Last Update	: 2010/12/7 - 13:18
// Desc.		:
//=============================================================================
void CBSocketServer::OnAddConnection(CSocketHandle* pSH, SOCKET newSocket)
{
	ASSERT( pSH == m_SocketServer );
	(pSH);

	CString strAddr;
	CSocketHandle sockHandle;
	SockAddrIn sockAddr;
	sockHandle.Attach( newSocket );
	sockHandle.GetPeerName( sockAddr );
	GetAddress( sockAddr, strAddr );
	sockHandle.Detach();

	SendLogMsg( _T("Connection established: %s"), strAddr );

	TCHAR szIPAddr[MAX_PATH] = { 0 };
	CSocketHandle::FormatIP(szIPAddr, MAX_PATH, sockAddr);
	DWORD	dwAddress = ntohl(_tinet_addr(szIPAddr));

	// 서버에 접속된 클라이언트의 목록에 추가한다.
	::SendNotifyMessage (m_hOwnerWnd, m_nWM_ServerStatus, MAKEWORD(m_byDeviceType, SERV_ADD_CONNECTION), (LPARAM)dwAddress);
}

//=============================================================================
// Method		: CBSocketServer::OnRemoveConnection
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Parameter	: SOCKET oldSocket
// Qualifier	:
// Last Update	: 2010/12/7 - 16:31
// Desc.		:
//=============================================================================
void CBSocketServer::OnRemoveConnection( CSocketHandle* pSH, SOCKET oldSocket )
{
	ASSERT( pSH == m_SocketServer );
	(pSH);

	CString strAddr;
	CSocketHandle sockHandle;
	SockAddrIn sockAddr;
	sockHandle.Attach( oldSocket );
	sockHandle.GetPeerName( sockAddr );
	GetAddress( sockAddr, strAddr );
	sockHandle.Detach();

	SendLogMsg( _T("Connection Removed: %s"), strAddr );

	TCHAR szIPAddr[MAX_PATH] = { 0 };
	CSocketHandle::FormatIP(szIPAddr, MAX_PATH, sockAddr);
	DWORD	dwAddress = ntohl(_tinet_addr(szIPAddr));

	// 서버에 접속된 클라이언트의 목록에서 삭제한다.	
	::SendNotifyMessage (m_hOwnerWnd, m_nWM_ServerStatus, MAKEWORD(m_byDeviceType, SERV_DEL_CONNECTION), (LPARAM)dwAddress);
}

//=============================================================================
// Method		: CBSocketServer::OnDataReceived
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Parameter	: const BYTE * pbData
// Parameter	: DWORD dwCount
// Parameter	: const SockAddrIn & addr
// Parameter	: SOCKET sock
// Qualifier	:
// Last Update	: 2010/12/8 - 10:32
// Desc.		:
//=============================================================================
void CBSocketServer::OnDataReceived(CSocketHandle* pSH, const BYTE* pbData, DWORD dwCount, const SockAddrIn& addr, SOCKET sock)
{
	ASSERT( pSH == m_SocketServer );
	(pSH);

	CString strAddr, strText;
	USES_CONVERSION;

	LPTSTR pszText = strText.GetBuffer(dwCount+1);
	::StringCchCopyN(pszText, dwCount+1, A2CT(reinterpret_cast<LPCSTR>(pbData)), dwCount);
	strText.ReleaseBuffer();
	GetAddress( addr, strAddr );
	SendLogMsg( _T("Recv [%s] : %s"), strAddr, strText);


	// 수신된 데이터를 파싱 (쓰레드로 처리한다.)
	char* pbRecvData = new char[dwCount + 1];
	pbRecvData[dwCount] = 0x0;
	memcpy (pbRecvData, pbData, dwCount);

	PasingRecvAck ((const char*)pbRecvData, dwCount, addr, sock);

	delete [] pbRecvData;
	strText.ReleaseBuffer();

	// 수신 이벤트 발생
// 	if (NULL != m_hACKEvent)
// 		SetEvent (m_hACKEvent);
}

//=============================================================================
// Method		: CBSocketServer::OnConnectionDropped
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Qualifier	:
// Last Update	: 2010/12/7 - 13:18
// Desc.		: 연결이 끊기면 이 함수가 호출된다.
//=============================================================================
void CBSocketServer::OnConnectionDropped(CSocketHandle* pSH)
{
	ASSERT( pSH == m_SocketServer );
	(pSH);

	TRACE ( _T("Connection lost with client.\n"));
	SendLogMsg ( _T("Connection lost with client."));

	// 서버에 접속된 클라이언트의 목록에서 삭제한다.	
	::SendNotifyMessage(m_hOwnerWnd, m_nWM_ServerStatus, MAKEWORD(m_byDeviceType, SERV_CONNECT_DROP), (LPARAM)0);
}

//=============================================================================
// Method		: CBSocketServer::OnConnectionError
// Access		: public 
// Returns		: void
// Parameter	: CSocketHandle * pSH
// Parameter	: DWORD dwError
// Qualifier	:
// Last Update	: 2010/12/7 - 13:18
// Desc.		: 접속 오류 발생하면 이 함수가 호출된다.
//=============================================================================
void CBSocketServer::OnConnectionError(CSocketHandle* pSH, DWORD dwError)
{
	ASSERT( pSH == m_SocketServer );
	(pSH);
	_com_error err(dwError);

	SendLogMsg ( _T("Communication Error:%s"), err.ErrorMessage() );

	TRACE ( _T("Communication Error:\r\n%s\n"), err.ErrorMessage() );
	
	::SendNotifyMessage(m_hOwnerWnd, m_nWM_ServerStatus, MAKEWORD(m_byDeviceType, SERV_CONNECT_ERROR), (LPARAM)0);
}

//#############################################################################
// Category		: 동기화 관련 함수들
// Pupose		: 
// Last Update	: 2010/12/7 - 14:35
//#############################################################################
//=============================================================================
// Method		: CBSocketServer::LockSemaphore
// Access		: protected 
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2010/12/7 - 14:35
// Desc.		: 
//=============================================================================
DWORD CBSocketServer::LockSemaphore()
{
	DWORD	dwResult = WaitForSingleObject( m_hSemaphore, m_dwSemaphoreWaitTime);
	if (WAIT_OBJECT_0 != dwResult)
	{
		//TRACE (_T("------------** Error : Semaphore is Alive **------------\n"));
		m_dwErrCode	= ERR_SEMAPHORE;		
	}
	else
	{
		//TRACE (_T("------------** Enter Semaphore **------------\n"));
	}

	return dwResult;
}

//=============================================================================
// Method		: CBSocketServer::UnlockSemaphore
// Access		: protected 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/12/7 - 14:35
// Desc.		: 
//=============================================================================
BOOL CBSocketServer::UnlockSemaphore()
{
	BOOL bResult = ReleaseSemaphore (m_hSemaphore, 1, NULL);

	/*if (!bResult) 
	{ 
		TRACE (_T("------------** Error : ReleaseSemaphore() **------------\n"));
	} 
	TRACE (_T("------------** ReleaseSemaphore() **------------\n"));*/

	return bResult;
}

//=============================================================================
// Method		: CBSocketServer::WaitSemaphore
// Access		: protected 
// Returns		: BOOL
// Parameter	: DWORD dwTimeout
// Qualifier	:
// Last Update	: 2010/12/7 - 14:35
// Desc.		: 
//=============================================================================
BOOL CBSocketServer::WaitSemaphore( DWORD dwTimeout )
{
	BOOL bResult = FALSE;
	DWORD	dwResult = WaitForSingleObject( m_hSemaphore, dwTimeout);

	switch (dwResult)
	{
	case WAIT_OBJECT_0:
		//TRACE (_T("------------** WaitSemaphore() : Success **------------\n"));
		bResult = TRUE;
		m_dwErrCode = NO_ERROR;
		break;

	case WAIT_TIMEOUT:
		//TRACE (_T("------------** WaitSemaphore() : Timeout **------------\n"));
		bResult = FALSE;
		m_dwErrCode = ERR_WAIT_TIMEOUT;
		break;

	case WAIT_ABANDONED:
		//TRACE (_T("------------** WaitSemaphore() : Abandoned **------------\n"));
		bResult = FALSE;
		m_dwErrCode = ERR_WAIT_ABANDONED;
		break;
	}	

	return bResult;
}

//#############################################################################
// Category		: 이더넷 통신 함수들
// Pupose		: 연결, 전송, 해제
// Last Update	: 2010/12/7 - 14:36
//#############################################################################
BOOL CBSocketServer::StartServer( LPCTSTR pszHost, LPCTSTR pszPort )
{
	BOOL bReturn = TRUE;
	__try
	{
		int nFamily = (m_nMode == AF_IPV4) ? AF_INET : AF_INET6;
		if (!m_SocketServer.StartServer(pszHost, pszPort, nFamily, SOCK_STREAM))
		{							
			SendErrorMsg(_T("Failed to start server. (Port : %s)"), pszPort);

			bReturn = FALSE;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketServer::StartServer()"));
		bReturn = FALSE;
	}

	return bReturn;
}

//=============================================================================
// Method		: CBSocketServer::StartServer
// Access		: public 
// Returns		: BOOL
// Parameter	: LPCTSTR pszPort
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 서버로 시작 시도
//=============================================================================
BOOL CBSocketServer::StartServer( LPCTSTR pszPort )
{
	BOOL bReturn = TRUE;
	__try
	{
		int nFamily = (m_nMode == AF_IPV4) ? AF_INET : AF_INET6;
		if (!m_SocketServer.StartServer(NULL, pszPort, nFamily, SOCK_STREAM))
		{							
			SendErrorMsg(_T("Failed to start server. (Port : %s)"), pszPort);

			bReturn = FALSE;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketServer::StartServer()"));
		bReturn = FALSE;
	}
		
	return bReturn;
}

//=============================================================================
// Method		: CBSocketServer::StartServer
// Access		: public 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 멤버 변수를 이용하여 서버 시작 시도
//=============================================================================
BOOL CBSocketServer::StartServer()
{	
	if (m_strPort.IsEmpty())
		return FALSE;

	if (m_strServerIP.IsEmpty())
		return StartServer (m_strPort);
	else
		return StartServer (m_strServerIP, m_strPort);
}

//=============================================================================
// Method		: CBSocketServer::StopServer
// Access		: public 
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 통신 연결해제
//=============================================================================
BOOL CBSocketServer::StopServer()
{
	BOOL bReturn = TRUE;
	__try
	{
		if (m_SocketServer.IsOpen())
		{
			m_SocketServer.CloseAllConnections();
			m_SocketServer.Terminate(2000);

			TRACE (_T("Terminate CBSocketServer!! \n"));
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketServer::StopServer()"));
		bReturn = FALSE;
	}

	return bReturn;
}

//=============================================================================
// Method		: CBSocketServer::SendStringToAll
// Access		: public 
// Returns		: BOOL
// Parameter	: LPCTSTR lpszString
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 문자열 전송
//=============================================================================
BOOL CBSocketServer::SendStringToAll ( LPCTSTR lpszString )
{
	if ( m_SocketServer.IsOpen() )
	{
		DWORD dwResult = 0;

		USES_CONVERSION;
		if (m_nSockType == SOCK_TCP)
		{			
			// unsafe access to Socket list!
#ifdef SOCKHANDLE_USE_OVERLAPPED
			const SocketBufferList& sl = m_SocketServer.GetSocketList();
			for(SocketBufferList::const_iterator citer = sl.begin(); citer != sl.end(); ++citer)
#else
			const SocketList& sl = m_SocketServer.GetSocketList();
			for(SocketList::const_iterator citer = sl.begin(); citer != sl.end(); ++citer)
#endif
			{
				CSocketHandle sockHandle;
				sockHandle.Attach( (*citer) );

				TRACE (_T("String : %s \n"), lpszString);
				TRACE (_T("Length : %d \n"), strlen((LPSTR)lpszString));

				dwResult = sockHandle.Write((const LPBYTE)(T2CA(lpszString)), (DWORD)_tcslen(lpszString), NULL);
				sockHandle.Detach();	
			}
		}
		else
		{
			SockAddrIn servAddr, sockAddr;
			m_SocketServer->GetSockName(servAddr);
			GetDestination(sockAddr);
			if ( servAddr != sockAddr ) 
			{
				dwResult = m_SocketServer.Write((const LPBYTE)(T2CA(lpszString)), (DWORD)_tcslen(lpszString), sockAddr);
			}
			else
			{
				SendLogMsg(_T("Please change the port number to send message to a client.") );
			}
		}
	}
	else
	{
		//::MessageBox(_T("Socket is not connected"));
		SendLogMsg(_T("Socket is not connected"));
		m_dwErrCode = ERR_COMM_NOTCONNECTED;
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CBSocketServer::SendString
// Access		: public 
// Returns		: BOOL
// Parameter	: SOCKET sock
// Parameter	: LPCTSTR lpszString
// Qualifier	:
// Last Update	: 2010/12/8 - 9:15
// Desc.		:
//=============================================================================
BOOL CBSocketServer::SendString( SOCKET sock, LPCTSTR lpszString )
{
	if ( m_SocketServer.IsOpen() )
	{
		DWORD dwResult = 0;

		USES_CONVERSION;
		if (m_nSockType == SOCK_TCP)
		{			
			CSocketHandle sockHandle;
			//sockHandle.CreateSocket ();
			sockHandle.Attach( sock );

			TRACE (_T("String : %s \n"), lpszString);
			TRACE (_T("Length : %d \n"), strlen((LPSTR)lpszString));

			dwResult = sockHandle.Write((const LPBYTE)(T2CA(lpszString)), (DWORD)_tcslen(lpszString), NULL);
			sockHandle.Detach();
		}
		else
		{
			SockAddrIn servAddr, sockAddr;
			m_SocketServer->GetSockName(servAddr);
			GetDestination(sockAddr);
			if ( servAddr != sockAddr ) 
			{
				dwResult = m_SocketServer.Write((const LPBYTE)(T2CA(lpszString)), (DWORD)_tcslen(lpszString), sockAddr);
			}
			else
			{
				SendLogMsg(_T("Please change the port number to send message to a client.") );
			}
		}
	}
	else
	{
		//::MessageBox(_T("Socket is not connected"));
		SendLogMsg(_T("Socket is not connected"));
		m_dwErrCode = ERR_COMM_NOTCONNECTED;
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CBSocketServer::SendDataToAll
// Access		: protected 
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwCount
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 데이터 전송
//=============================================================================
BOOL CBSocketServer::SendDataToAll( const char* lpBuffer, DWORD dwCount )
{
	if ( m_SocketServer.IsOpen() )
	{
		DWORD dwResult = 0;

		//USES_CONVERSION;
		if (m_nSockType == SOCK_TCP)
		{
			// unsafe access to Socket list!
#ifdef SOCKHANDLE_USE_OVERLAPPED
			const SocketBufferList& sl = m_SocketServer.GetSocketList();
			for(SocketBufferList::const_iterator citer = sl.begin(); citer != sl.end(); ++citer)
#else
			const SocketList& sl = m_SocketServer.GetSocketList();
			for(SocketList::const_iterator citer = sl.begin(); citer != sl.end(); ++citer)
#endif
			{
				CSocketHandle sockHandle;
				sockHandle.Attach( (*citer) );

				TRACE ("String : %s \n", lpBuffer);
				TRACE (_T("Length : %d - %d \n"), dwCount, strlen((LPSTR)lpBuffer));

				dwResult = sockHandle.Write((const LPBYTE)lpBuffer, dwCount, NULL);
				sockHandle.Detach();
			}
		}
		else
		{
			SockAddrIn servAddr, sockAddr;
			m_SocketServer->GetSockName(servAddr);
			GetDestination(sockAddr);
			if ( servAddr != sockAddr ) 
			{
				dwResult = m_SocketServer.Write((const LPBYTE)lpBuffer, dwCount, sockAddr);
			}
			else
			{
				SendLogMsg(_T("Please change the port number to send message to a client.") );
			}
		}
	}
	else
	{
		//::MessageBox(_T("Socket is not connected"));
		SendLogMsg(_T("Socket is not connected"));
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: CBSocketServer::SendData
// Access		: public 
// Returns		: BOOL
// Parameter	: SOCKET sock
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwCount
// Qualifier	:
// Last Update	: 2010/12/8 - 9:15
// Desc.		:
//=============================================================================
BOOL CBSocketServer::SendData( SOCKET sock, const char* lpBuffer, DWORD dwCount )
{
	if ( m_SocketServer.IsOpen() )
	{
		DWORD dwResult = 0;

		//USES_CONVERSION;
		if (m_nSockType == SOCK_TCP)
		{
			CSocketHandle sockHandle;
			sockHandle.Attach( sock );

			TRACE ("SendData String[%d] : %s \n", m_byDeviceType, lpBuffer);
			TRACE (_T("SendData Length : %d - %d \n"), dwCount, strlen((LPSTR)lpBuffer));

			dwResult = sockHandle.Write((const LPBYTE)lpBuffer, dwCount, NULL);
			sockHandle.Detach();
		}
		else
		{
			SockAddrIn servAddr, sockAddr;
			m_SocketServer->GetSockName(servAddr);
			GetDestination(sockAddr);
			if ( servAddr != sockAddr ) 
			{
				dwResult = m_SocketServer.Write((const LPBYTE)lpBuffer, dwCount, sockAddr);
			}
			else
			{
				SendLogMsg(_T("Please change the port number to send message to a client.") );
			}
		}
	}
	else
	{
		//::MessageBox(_T("Socket is not connected"));
		SendLogMsg(_T("Socket is not connected"));
		return FALSE;
	}	

	return TRUE;
}

//#############################################################################
// Category		: 여러가지 설정 함수들
// Pupose		: 오너 윈도우와 메세지 처리하기 위한 함수들
// Last Update	: 2010/12/7 - 14:36
//#############################################################################
//=============================================================================
// Method		: CBSocketServer::SetOwnerHWND
// Access		: public 
// Returns		: BOOL
// Parameter	: HWND hOwnerWnd
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 오너 윈도우 핸들 설정
//=============================================================================
BOOL CBSocketServer::SetOwnerHWND( HWND hOwnerWnd )
{
	ASSERT(NULL != hOwnerWnd);

	m_hOwnerWnd = hOwnerWnd;

	return TRUE;
}

//=============================================================================
// Method		: CBSocketServer::SetWmLog
// Access		: public 
// Returns		: void
// Parameter	: UINT nWindowsMessage
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 로그용 윈도 메세지 설정
//=============================================================================
void CBSocketServer::SetWmLog( UINT nWindowsMessage )
{
	m_nWM_LOG = nWindowsMessage;
}

//=============================================================================
// Method		: CBSocketServer::SetWmRecv
// Access		: public 
// Returns		: void
// Parameter	: UINT nWindowMessage
// Qualifier	:
// Last Update	: 2010/12/14 - 13:28
// Desc.		: 수신된 데이터 처리용 윈도우 메세지
//=============================================================================
void CBSocketServer::SetWmRecv( UINT nWindowMessage )
{
	m_nWM_Recv = nWindowMessage;
}

//=============================================================================
// Method		: CBSocketServer::SendLogMsg
// Access		: protected 
// Returns		: void _cdecl
// Parameter	: LPCTSTR lpszFormat
// Parameter	: ...
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 로그 메세지을 윈도 메세지로 전송
//=============================================================================
void _cdecl CBSocketServer::SendLogMsg( LPCTSTR lpszFormat, ... )
{
	ASSERT(NULL != lpszFormat);
	ASSERT(NULL != m_hOwnerWnd);
	ASSERT(NULL != m_nWM_LOG);

	__try
	{
		UINT_PTR nLogSize = _tcslen(lpszFormat) + 1024;
		LPTSTR lpszLog	= GetLogBuffer(nLogSize);
		size_t cb		= 0;

		HRESULT hR = 0;
		va_list args;
		va_start(args, lpszFormat);
		hR = ::StringCchVPrintfEx(lpszLog, nLogSize, NULL, &cb, 0, lpszFormat, args);
		va_end(args);

		if (S_OK == hR)
			::SendNotifyMessage (m_hOwnerWnd, m_nWM_LOG, (WPARAM)lpszLog, (LPARAM)MAKELONG((WORD)m_byDeviceType, (WORD)FALSE));
		else
			SendErrorMsg(_T("*** CBSocketServer::SendLogMsg() -> StringCchVPrintfEx Error (%d)"), hR);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketServer::SendLogMsg()"));
	}
}

//=============================================================================
// Method		: CBSocketServer::SendErrorMsg
// Access		: protected 
// Returns		: void _cdecl
// Parameter	: LPCTSTR lpszFormat
// Parameter	: ...
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 에러 메세지을 윈도 메세지로 전송
//=============================================================================
void _cdecl CBSocketServer::SendErrorMsg( LPCTSTR lpszFormat, ... )
{
	ASSERT(NULL != lpszFormat);
	ASSERT(NULL != m_hOwnerWnd);

	__try
	{
		UINT_PTR nLogSize = _tcslen(lpszFormat) + 1024;
		LPTSTR lpszLog	= GetLogBuffer(nLogSize);
		size_t cb		= 0;

		HRESULT hR = 0;
		va_list args;
		va_start(args, lpszFormat);
		hR = ::StringCchVPrintfEx(lpszLog, nLogSize, NULL, &cb, 0, lpszFormat, args);
		va_end(args);

		if (S_OK == hR)
			::SendNotifyMessage (m_hOwnerWnd, m_nWM_LOG, (WPARAM)lpszLog, (LPARAM)MAKELONG((WORD)m_byDeviceType, (WORD)TRUE));
		else
			TRACE (_T("*** CBSocketServer::SendErrorMsg() -> StringCchVPrintfEx Error (%d)"), hR);
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE (_T("*** Exception Error : CBSocketServer::SendErrorMsg()"));
	}
}

//=============================================================================
// Method		: _tinet_addr
// Access		: protected  
// Returns		: DWORD
// Parameter	: __in const TCHAR * cp
// Qualifier	:
// Last Update	: 2016/9/28 - 11:38
// Desc.		:
//=============================================================================
DWORD CBSocketServer::_tinet_addr(__in const TCHAR *cp)
{
#ifdef UNICODE
	char IP[16];
	int Ret = 0;
	Ret = WideCharToMultiByte(CP_ACP, 0, cp, (int)_tcslen(cp), IP, 15, NULL, NULL);
	IP[Ret] = 0;
	return inet_addr(IP);
#else
	return inet_addr(cp);
#endif
}

//=============================================================================
// Method		: CBSocketServer::GetLogBuffer
// Access		: protected 
// Returns		: LPTSTR
// Parameter	: UINT_PTR dwBufferSize
// Qualifier	:
// Last Update	: 2011/1/17 - 16:27
// Desc.		:
//=============================================================================
LPTSTR CBSocketServer::GetLogBuffer(UINT_PTR nBufferSize)
{
	LPTSTR pReturn = NULL;

	if (m_byLogIndex < (MAX_LOG_BUFFER_SOCK_SRV - 1))
		++m_byLogIndex;
	else
		m_byLogIndex = 0;

	//ZeroMemory (m_szLogMsg[m_byLogIndex], nBufferSize);
	pReturn = m_szLogMsg[m_byLogIndex];

	//if (NULL != m_lpLogMsg[m_byLogIndex])
	//{
	//	delete [] m_lpLogMsg[m_byLogIndex];
	//	m_lpLogMsg[m_byLogIndex] = NULL;
	//}

	//m_lpLogMsg[m_byLogIndex] = new TCHAR[nBufferSize];
	//ZeroMemory (m_lpLogMsg[m_byLogIndex], nBufferSize);
	//pReturn = m_lpLogMsg[m_byLogIndex];

	return pReturn;
}

//=============================================================================
// Method		: CBSocketServer::ReleaseBuffer
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2011/1/17 - 16:13
// Desc.		:
//=============================================================================
void CBSocketServer::ReleaseLogBuffer()
{
	//for (int iIndex = 0; iIndex < MAX_LOG_BUFFER_SOCK_SRV; iIndex++)
	//{
	//	if (NULL != m_lpLogMsg[iIndex])
	//		delete [] m_lpLogMsg[iIndex];
	//}
}

//=============================================================================
// Method		: CBSocketServer::SetAckTimeout
// Access		: public 
// Returns		: void
// Parameter	: DWORD dwMiliseconds
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 
//=============================================================================
void CBSocketServer::SetAckTimeout( DWORD dwMiliseconds )
{
	ASSERT (10 <= dwMiliseconds && dwMiliseconds <= 5000);

	m_dwAckTimeout = dwMiliseconds;
}

//=============================================================================
// Method		: SetExitEvent
// Access		: public  
// Returns		: void
// Parameter	: HANDLE hExitEvent
// Qualifier	:
// Last Update	: 2016/10/11 - 21:39
// Desc.		:
//=============================================================================
void CBSocketServer::SetExitEvent(HANDLE hExitEvent)
{
	m_hExternalExitEvent = hExitEvent;
}

//=============================================================================
// Method		: CBSocketServer::SetPort
// Access		: public 
// Returns		: void
// Parameter	: LPCTSTR pszAddr
// Parameter	: LPCTSTR pszPort
// Qualifier	:
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 서버 포트를 설정함
//=============================================================================
void CBSocketServer::SetPort( UINT nPort )
{
	m_strPort.Format(_T("%d"), nPort);
}

//=============================================================================
// Method		: CBSocketServer::GetAddress
// Access		: protected 
// Returns		: void
// Parameter	: const SockAddrIn & addrIn
// Parameter	: CString & rString
// Qualifier	: const
// Last Update	: 2010/12/7 - 14:36
// Desc.		: 
//=============================================================================
void CBSocketServer::GetAddress( const SockAddrIn& addrIn, CString& rString ) const
{
	TCHAR szIPAddr[MAX_PATH] = { 0 };
	CSocketHandle::FormatIP(szIPAddr, MAX_PATH, addrIn);
	rString.Format(_T("%s : %d"), szIPAddr, static_cast<int>(static_cast<UINT>(ntohs(addrIn.GetPort()))) );
}

void CBSocketServer::GetAddress( const SockAddrIn& addrIn, LPTSTR lpOutBuff, UINT nBuffSize ) const
{
	TCHAR szIPAddr[MAX_PATH] = { 0 };
	CSocketHandle::FormatIP(szIPAddr, MAX_PATH, addrIn);	
	StringCbPrintf (lpOutBuff, nBuffSize, _T("%s : %d"), szIPAddr, static_cast<int>(static_cast<UINT>(ntohs(addrIn.GetPort()))));
}

//=============================================================================
// Method		: CBSocketServer::GetDestination
// Access		: protected 
// Returns		: bool
// Parameter	: SockAddrIn & addrIn
// Qualifier	: const
// Last Update	: 2010/12/7 - 14:36
// Desc.		:
//=============================================================================
bool CBSocketServer::GetDestination(SockAddrIn& addrIn) const
{
	int nFamily = (m_nMode == AF_IPV4) ? AF_INET : AF_INET6;
	return addrIn.CreateFrom(NULL, m_strPort, nFamily);
}

//#############################################################################
// Category		: READ/WRITE 처리
// Pupose		: Read / Write 통신 처리 함수들
// Last Update	: 2010/12/7 - 14:36
//#############################################################################
//=============================================================================
// Method		: CBSocketServer::PasingRecvAck
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: const char * lpBuffer
// Parameter	: DWORD dwReaded
// Parameter	: const SockAddrIn & addr
// Parameter	: SOCKET sock
// Qualifier	:
// Last Update	: 2010/12/8 - 10:33
// Desc.		: 수신된 응답 처리 (정상 / 오류)
//=============================================================================
BOOL CBSocketServer::PasingRecvAck( const char* lpBuffer, DWORD dwReaded, const SockAddrIn& addr, SOCKET sock)
{
	// STX, ETX 체크
	// Command 체크

	

	return TRUE;
}

//=============================================================================
// Method		: CBSocketServer::WaitACK
// Access		: protected 
// Returns		: BOOL
// Parameter	: DWORD dwWaitTime
// Parameter	: LPDWORD dwOutBufSize
// Qualifier	:
// Last Update	: 2010/12/7 - 14:37
// Desc.		: TCP/IP 포트로 응답이 들어오길 기다림
//=============================================================================
BOOL CBSocketServer::WaitACK( DWORD dwWaitTime /*= 1000*/, LPDWORD  dwOutBufSize /*= NULL*/ )
{
	BOOL	bRet		= FALSE;
	DWORD	dwBufSize	= 0;
	BOOL	fDone		= TRUE;
	DWORD	dwMaxTick	= GetTickCount() + dwWaitTime;

	HANDLE     hArray[2];
	hArray[0] = m_hACKEvent;	// Ack 이벤트가
	hArray[1] = m_hExternalExitEvent;	// 쓰레드 종료 이벤트

	__try
	{
		while (fDone)
		{		
			DWORD dwResult = ::MsgWaitForMultipleObjects(2, hArray, FALSE, dwMaxTick - GetTickCount(), QS_ALLINPUT);
			switch (dwResult)
			{
			case WAIT_OBJECT_0:		// 포트에 데이터 수신됨
				ResetEvent (m_hACKEvent);
				//TRACE (_T("WaitACK is Passed!! \n"));
				m_dwErrCode = NO_ERROR;
				bRet		= TRUE;
				fDone		= FALSE;
				break;

			case WAIT_OBJECT_0 + 1:	// 프로그램 종료 이벤트 처리
				fDone = FALSE;
				break;

			case WAIT_OBJECT_0 + 2:
				{
					MSG msg;
					while (::PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))         
					{
						if (msg.message == WM_QUIT) 
							exit(0);

						::TranslateMessage(&msg);            
						::DispatchMessage(&msg);         
					}  
				}
				break;
			
			case WAIT_TIMEOUT:
				TRACE (_T("WaitACK is TimeOut\n"));			
				m_dwErrCode = ERR_TCPIP_ACK_TIMEOUT;
				bRet		= FALSE;
				fDone		= FALSE;
				break;
			}

			if (FALSE == m_SocketServer.IsOpen())
			{
				TRACE (_T("WaitACK is Disconnected\n"));
				m_dwErrCode = ERR_COMM_DISCONNECT;
				bRet		= FALSE;
				fDone		= FALSE;
				break;
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		SendErrorMsg(_T("*** Exception Error : CBSocketServer::WaitACK()"));
	}

	return bRet;
}
