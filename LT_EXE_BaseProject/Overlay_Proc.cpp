﻿//*****************************************************************************
// Filename	: 	Overlay_Proc.cpp
// Created	:	2017/3/23 - 08:14
// Modified	:	2017/3/23 - 08:14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "Overlay_Proc.h"
#include "CommonFunction.h"

COverlay_Proc::COverlay_Proc()
{
}


COverlay_Proc::~COverlay_Proc()
{
}

//=============================================================================
// Method		: Overlay_SFR
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_SFR stOption
// Parameter	: __in ST_Result_SFR stResult
// Qualifier	:
// Last Update	: 2018/8/7 - 1:11
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_SFR(__inout IplImage* lpImage, __in ST_UI_SFR stOption, __in ST_Result_SFR stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;
	
	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		if (TRUE == stOption.stInput[nIdx].bEnable)
		{
			int iLineSize		= 2;
			int iMarkSize		= 10;
			double dbFontSize	= 1.0;

			if (TRUE == m_bTest)
			{
				rtRect = stResult.rtROI[nIdx];
			}
			else
			{
				rtRect = stOption.stInput[nIdx].rtROI;
			}
	
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
	
			rtRectText.SetRectEmpty();

			switch (stOption.stInput[nIdx].nOverlayDir)
			{
			case 0:	// OverlayDir_Left
				rtRectText.left = rtRect.left - 70;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 1: // OverlayDir_Right
				rtRectText.left = rtRect.right + 10;
				rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
				break;
			case 2: // OverlayDir_Top
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.top - 40;
				break;
			case 3: // OverlayDir_Bottom
				rtRectText.left = rtRect.left;
				rtRectText.top = rtRect.bottom + 40;
				break;
			default:
				break;
			}
	
			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("%.2f"), stResult.dbValue[nIdx]);

			iLineSize = 4;

			if (TRUE == stResult.bResult[nIdx])
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
			}
		}
	}
}


//=============================================================================
// Method		: Overlay_Chart
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Chart stOption
// Parameter	: __in ST_Result_Chart stResult
// Qualifier	:
// Last Update	: 2018/8/23 - 16:11
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Chart(__inout IplImage* lpImage, __in ST_UI_Chart stOption, __in ST_Result_Chart stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 2.0;

	if (TRUE == m_bTest)
	{
		rtRect = stResult.rtROI;
	}
	else
	{
		rtRect = stOption.rtROI;

		stResult.bDetect = FALSE;
	}

	if (TRUE == stResult.bDetect)
	{
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(0, 84, 255), iLineSize);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
	}

	iLineSize = 3;
	iMarkSize = 30;

	rtRectText.left = lpImage->width / 3;
	rtRectText.top  = lpImage->height - 50;

	if (TRUE == m_bTest)
	{
		if (TRUE == stResult.bResult)
		{
			szText = _T("Chart Matching OK");
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
		}
		else
		{
			szText = _T("Chart Matching NG");
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
		}
	}
}

//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Current(__inout IplImage* lpImage, __in ST_UI_Current stOption, __in ST_Result_Current stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	//!SH _180809: 출력위치 변수화 해야됨
	rtRect.left =	100;
	rtRect.top	=	700;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	szText.Format(_T("%.2f"), 
		stResult.dbValue[Current_CH2]
		);

	iLineSize = 4;

	if (TRUE == stResult.GetFinalResult())
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRect, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_DynamicBW
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_DynamicBW stOption
// Parameter	: __in ST_Result_DynamicBW stResult
// Qualifier	:
// Last Update	: 2018/8/8 - 16:07
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_DynamicBW(__inout IplImage* lpImage, __in ST_UI_DynamicBW stOption, __in ST_Result_DynamicBW stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CPoint	ptCenter;
	CRect	rtRectText;
	CRect	rtRectResultText_DR;
	CRect	rtRectResultText_SNR;
	CString szText_DR;
	CString szText_SNR;
	CString szText;

	int iLineSize = 2;
	int iMarkSize = 10;
	double dbFontSize = 1.0;

	for (UINT nIdx = 0; nIdx < ROI_DnyBW_Max; nIdx++)
	{
		if (TRUE == stOption.stInput[nIdx].bEnable)
		{
			

			if (TRUE == m_bTest)
			{
				ptCenter = stResult.ptROI[nIdx];
			}
			else
			{
				ptCenter = stOption.stInput[nIdx].ptROI;
			}

			rtRect.left = ptCenter.x - stOption.iMaxROIWidth / 2;
			rtRect.right = ptCenter.x + stOption.iMaxROIWidth / 2;
			rtRect.top = ptCenter.y - stOption.iMaxROIHeight / 2;
			rtRect.bottom = ptCenter.y + stOption.iMaxROIHeight / 2;

			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

			switch (stOption.stInput[nIdx].nOverlayDir)
			{
			case 0:	// OverlayDir_Left
				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2 - 70;
				rtRectText.top = ptCenter.y;
				break;
			case 1: // OverlayDir_Right

				rtRectText.left = ptCenter.x + stOption.iMaxROIWidth / 2 + 10;
				rtRectText.top = ptCenter.y;
				break;
			case 2: // OverlayDir_Top

				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2;
				rtRectText.top = ptCenter.y - stOption.iMaxROIHeight / 2 - 40;
				break;
			case 3: // OverlayDir_Bottom

				rtRectText.left = ptCenter.x - stOption.iMaxROIWidth / 2;
				rtRectText.top = ptCenter.y + stOption.iMaxROIHeight / 2 + 40;
				break;
			default:
				break;
			}

			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("AVG : %.2f"), stResult.dbAverageValue[nIdx]);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			rtRectText.top += 30;
			szText.Format(_T("VAR : %.2f"), stResult.dbVarianceValue[nIdx]);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);
// 
			iLineSize = 4;

			rtRectResultText_DR.left = lpImage->width / 2 - 50;
			rtRectResultText_DR.top = lpImage->height * 2 / 3 - 100;

			rtRectResultText_SNR.left = lpImage->width / 2 - 50;
			rtRectResultText_SNR.top = lpImage->height * 2 / 3 - 100;


			szText_DR.Format(_T("DR : %2.3f"), stResult.dbDynamic);
			szText_SNR.Format(_T("SNR : %2.3f"), stResult.dbSNR_BW);
		

			if (TRUE == stResult.bDynamic && TRUE == stResult.bSNR_BW)
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_DR, RGB(0, 0, 255), iLineSize, dbFontSize, szText_DR);
				rtRectResultText_SNR.top = rtRectResultText_DR.top + 30;
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_SNR, RGB(0, 0, 255), iLineSize, dbFontSize, szText_SNR);


			}
			else
			{
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_DR, RGB(255, 0, 0), iLineSize, dbFontSize, szText_DR);
				rtRectResultText_SNR.top = rtRectResultText_DR.top + 30;
				Overlay_Process(lpImage, OvrMode_TXT, rtRectResultText_SNR, RGB(255, 0, 0), iLineSize, dbFontSize, szText_SNR);

			}
		}
	}
}

//=============================================================================
// Method		: Overlay_Shading
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Shading stOption
// Qualifier	:
// Last Update	: 2018/8/4 - 17:50
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Shading(__inout IplImage* lpImage, __in ST_UI_Shading stOption, __in ST_Result_Shading stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	//!SH _180809: 출력위치 변수화 해야됨
	rtRectText.left = 200;
	rtRectText.top = 700;

	int		iLineSize = 4;
	double	dbFontSize = 1.0;

	szText.Format(_T("%.2fF : %.2f / %.2f, %.2fF : %.2f / %.2f"), 
		stOption.dTestField_1, stResult.dValue[enUI_ShadingField_1], stResult.dValue[enUI_ShadingField_1_Dev],
		stOption.dTestField_2, stResult.dValue[enUI_ShadingField_2], stResult.dValue[enUI_ShadingField_2_Dev]
		);

	CvPoint LeftTop;
	CvPoint LeftBottom;
	CvPoint RightTop;
	CvPoint RightBottom;
	int iBlockSzW = stOption.iMaxROIWidth / 2;
	int iBlockSzH = stOption.iMaxROIHeight / 2;
	int iImageW = 1280;
	int iImageH = 1080;
	int ileft = 0;
	int iright = 0;
	int itop = 0;
	int ibottom = 0;

	// 센터
	rtRect.left		= ileft		= iImageW / 2 - iBlockSzW;
	rtRect.right	= iright	= iImageW / 2  + iBlockSzW;
	rtRect.top		= itop		= iImageH / 2  - iBlockSzH;
	rtRect.bottom	= ibottom	= iImageH / 2 + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	// Field 1
	GetField_4Point(iImageW, iImageH, stOption.dTestField_1, LeftTop, LeftBottom, RightTop, RightBottom);

	// LeftTop
	rtRect.left		= ileft		= LeftTop.x - iBlockSzW;
	rtRect.right	= iright	= LeftTop.x + iBlockSzW;
	rtRect.top		= itop		= LeftTop.y - iBlockSzH;
	rtRect.bottom	= ibottom	= LeftTop.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	// LeftBottom
	rtRect.left		= ileft		= LeftBottom.x - iBlockSzW;
	rtRect.right	= iright	= LeftBottom.x + iBlockSzW;
	rtRect.top		= itop		= LeftBottom.y - iBlockSzH;
	rtRect.bottom	= ibottom	= LeftBottom.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	// RightTop
	rtRect.left		= ileft		= RightTop.x - iBlockSzW;
	rtRect.right	= iright	= RightTop.x + iBlockSzW;
	rtRect.top		= itop		= RightTop.y - iBlockSzH;
	rtRect.bottom	= ibottom	= RightTop.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	// RightBottom
	rtRect.left		= ileft		= RightBottom.x - iBlockSzW;
	rtRect.right	= iright	= RightBottom.x + iBlockSzW;
	rtRect.top		= itop		= RightBottom.y - iBlockSzH;
	rtRect.bottom	= ibottom	= RightBottom.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);


	// Field 2
	GetField_4Point(iImageW, iImageH, stOption.dTestField_2, LeftTop, LeftBottom, RightTop, RightBottom);

	// LeftTop
	rtRect.left = ileft = LeftTop.x - iBlockSzW;
	rtRect.right = iright = LeftTop.x + iBlockSzW;
	rtRect.top = itop = LeftTop.y - iBlockSzH;
	rtRect.bottom = ibottom = LeftTop.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	// LeftBottom
	rtRect.left = ileft = LeftBottom.x - iBlockSzW;
	rtRect.right = iright = LeftBottom.x + iBlockSzW;
	rtRect.top = itop = LeftBottom.y - iBlockSzH;
	rtRect.bottom = ibottom = LeftBottom.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	// RightTop
	rtRect.left = ileft = RightTop.x - iBlockSzW;
	rtRect.right = iright = RightTop.x + iBlockSzW;
	rtRect.top = itop = RightTop.y - iBlockSzH;
	rtRect.bottom = ibottom = RightTop.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

	// RightBottom
	rtRect.left = ileft = RightBottom.x - iBlockSzW;
	rtRect.right = iright = RightBottom.x + iBlockSzW;
	rtRect.top = itop = RightBottom.y - iBlockSzH;
	rtRect.bottom = ibottom = RightBottom.y + iBlockSzH;
	Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);


	if (TRUE == stResult.GetFinalResult())
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}

}

//=============================================================================
// Method		: Overlay_Process
// Access		: protected  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in enOverlayMode enMode
// Parameter	: __in CRect rtROI
// Parameter	: __in COLORREF clrLineColor
// Parameter	: __in int iLineSize
// Parameter	: __in double dbFontSize
// Parameter	: __in CString szText
// Qualifier	:
// Last Update	: 2018/3/2 - 10:56
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Process(__inout IplImage* lpImage, __in enOverlayMode enMode, __in CRect rtROI, __in COLORREF clrLineColor /*= RGB(255, 200, 0)*/, __in int iLineSize /*= 1*/, __in double dbFontSize /*= 1.0*/, __in CString szText /*= _T("")*/)
{
	CvPoint nStartPt;
	nStartPt.x = rtROI.left;
	nStartPt.y = rtROI.top;

	CvPoint nEndPt;
	nEndPt.x = rtROI.right;
	nEndPt.y = rtROI.bottom;

	CvPoint nCenterPt;
	nCenterPt.x = nStartPt.x + ((nEndPt.x - nStartPt.x) / 2);
	nCenterPt.y = nStartPt.y + ((nEndPt.y - nStartPt.y) / 2);

	CvSize Ellipse_Axis;
	Ellipse_Axis.width  = (nEndPt.x - nStartPt.x)/2;
	Ellipse_Axis.height = (nEndPt.y - nStartPt.y)/2;

	switch (enMode)
	{
	case OvrMode_LINE:
		cvLine(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_RECTANGLE:
		cvRectangle(lpImage, nStartPt, nEndPt, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_CIRCLE:
		//cvCircle(lpImage, nStartPt, abs(nEndPt.x - nStartPt.x) / 2, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		cvEllipse(lpImage, nCenterPt, Ellipse_Axis, 0, 0, 360, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor)), iLineSize);
		break;
	case OvrMode_TXT:
	{
		CvFont* cvFont = new CvFont;
		cvInitFont(cvFont, CV_FONT_VECTOR0, dbFontSize, dbFontSize, 3, iLineSize);
		cvPutText(lpImage, CT2A(szText), nStartPt, cvFont, CV_RGB(GetRValue(clrLineColor), GetGValue(clrLineColor), GetBValue(clrLineColor))); // cvPoint 위치에 설정 색상으로 글씨 넣기

		delete cvFont; // 해제
	}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Ymean(__inout IplImage* lpImage, __in ST_UI_Ymean		stOption, __in ST_Result_Ymean stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}


	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 600;
	rtRectText.top = 700;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bYmeanResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_LCB(__inout IplImage* lpImage, __in ST_UI_LCB stOption, __in ST_Result_LCB stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}


	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 600;
	rtRectText.top = 700;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bLCBResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_BlackSpot(__inout IplImage* lpImage, __in ST_UI_BlackSpot stOption, __in ST_Result_BlackSpot stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefectCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefectCount; nIdx++)
		{
			rtRect = stResult.rtROI[nIdx];
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}


	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 600;
	rtRectText.top = 700;

	szText.Format(_T("FailCount : %d"), stResult.nDefectCount);

	iLineSize = 4;

	if (TRUE == stResult.bBlackSpotResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Defect_Black(__inout IplImage* lpImage, __in ST_UI_Defect_Black		stOption, __in ST_Result_Defect_Black stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	//CPoint	ptPoint;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefect_BlackCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefect_BlackCount; nIdx++)
		{
			rtRect.left = stResult.ptROI[nIdx].x;
			rtRect.right = stResult.ptROI[nIdx].x + 2;
			rtRect.top = stResult.ptROI[nIdx].y;
			rtRect.bottom = stResult.ptROI[nIdx].y + 2;
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 600;
	rtRectText.top = 700;

	szText.Format(_T("FailCount : %d"), stResult.nDefect_BlackCount);

	iLineSize = 4;

	if (TRUE == stResult.bDefect_BlackResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}


//=============================================================================
// Method		: Overlay_Current
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_Defect_White(__inout IplImage* lpImage, __in ST_UI_Defect_White		stOption, __in ST_Result_Defect_White stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	int		iLineSize = 2;
	double	dbFontSize = 1.0;

	if (0 < stResult.nDefect_WhiteCount)
	{
		for (int nIdx = 0; nIdx < stResult.nDefect_WhiteCount; nIdx++)
		{
			//!SH _180908: Point 버전 필요
			rtRect.left = stResult.ptROI[nIdx].x;
			rtRect.right = stResult.ptROI[nIdx].x + 2;
			rtRect.top = stResult.ptROI[nIdx].y;
			rtRect.bottom = stResult.ptROI[nIdx].y + 2;
			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);
		}

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectText.left = 600;
	rtRectText.top = 700;

	szText.Format(_T("FailCount : %d"), stResult.nDefect_WhiteCount);

	iLineSize = 4;

	if (TRUE == stResult.bDefect_WhiteResult)
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	}
}

void COverlay_Proc::Overlay_Distortion(__inout IplImage* lpImage, __in ST_UI_Rotation stOption, __in ST_Result_Rotation stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;

	for (UINT nIdx = 0; nIdx < ROI_Rotation_Max; nIdx++)
	{
		/*if (TRUE == stOption.stInput[nIdx].bEnable)
		{*/
			int iLineSize = 2;
			int iMarkSize = 10;
			double dbFontSize = 1.0;

			if (TRUE == m_bTest)
			{
				rtRect = stResult.rtROI[nIdx];
			}
			else
			{
				rtRect = stOption.stInput[nIdx].rtROI;
			}

			Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

			rtRectText.SetRectEmpty();

	//		switch (stOption.stInput[nIdx].nOverlayDir)
	//		{
	//		case 0:	// OverlayDir_Left
	//			rtRectText.left = rtRect.left - 70;
	//			rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
	//			break;
	//		case 1: // OverlayDir_Right
	//			rtRectText.left = rtRect.right + 10;
	//			rtRectText.top = (rtRect.top + rtRect.bottom) / 2;
	//			break;
	//		case 2: // OverlayDir_Top
	//			rtRectText.left = rtRect.left;
	//			rtRectText.top = rtRect.top - 40;
	//			break;
	//		case 3: // OverlayDir_Bottom
	//			rtRectText.left = rtRect.left;
	//			rtRectText.top = rtRect.bottom + 40;
	//			break;
	//		default:
	//			break;
	//		}

			szText.Format(_T("%d"), nIdx);
			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

			/*	rtRectText.top += 30;
				szText.Format(_T("%.2f"), stResult.);*/

			iLineSize = 4;

	//		if (TRUE == stResult.bResult[nIdx])
	//		{
	//			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(0, 84, 255), iLineSize, dbFontSize, szText);
	//		}
	//		else
	//		{
	//			Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 0, 0), iLineSize, dbFontSize, szText);
	//		}
	//	}
	}
}

//=============================================================================
// Method		: Overlay_RI
// Access		: public  
// Returns		: void
// Parameter	: __inout IplImage * lpImage
// Parameter	: __in ST_UI_Current stOption
// Parameter	: __in ST_Result_Current stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 10:19
// Desc.		:
//=============================================================================
void COverlay_Proc::Overlay_RI(__inout IplImage* lpImage, __in ST_UI_Rllumination		stOption, __in ST_Result_Rllumination stResult)
{
	if (NULL == lpImage)
		return;

	CRect	rtRect;
	CRect	rtRectText;
	CPoint	ptCenter;
	CString szText;
	CString szTextResult;
	CRect	rtRectTextResult;
	int		iLineSize = 2;
	double	dbFontSize = 1.0;


	for (int nIdx = 0; nIdx <ROI_Rllumination_Max; nIdx++)
	{
		rtRectText.SetRectEmpty();
		rtRect = stResult.ptRIROI[nIdx];
		Overlay_Process(lpImage, OvrMode_RECTANGLE, rtRect, RGB(255, 200, 0), iLineSize);

		switch (nIdx)
		{
		case 0:	
		case 1: 
		case 2: 
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.bottom + 40;
			break;
		case 3: // OverlayDir_Bottom
		case 4:
			rtRectText.left = rtRect.left;
			rtRectText.top = rtRect.top - 40;
			break;
		default:
			break;
		}

		szText.Format(_T("%.3f"), stResult.dRIValue[nIdx]);
		Overlay_Process(lpImage, OvrMode_TXT, rtRectText, RGB(255, 200, 0), iLineSize, dbFontSize, szText);

	}

	//!SH _180809 : 출력위치 변수화 해야됨
	rtRectTextResult.SetRectEmpty();
	rtRectTextResult.left = 400;
	rtRectTextResult.top = 700;

	szTextResult.Format(_T("RI Corner : %.3f , RI Min : %.3f"), stResult.dRICorner, stResult.dRIMin);

	iLineSize = 4;

	if (TRUE == stResult.dRIResult[0] && TRUE == stResult.dRIResult[1])
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectTextResult, RGB(0, 84, 255), iLineSize, dbFontSize, szTextResult);
	}
	else
	{
		Overlay_Process(lpImage, OvrMode_TXT, rtRectTextResult, RGB(255, 0, 0), iLineSize, dbFontSize, szTextResult);
	}
}


void COverlay_Proc::GetField_4Point(__in int iWidth, int iHeight, double dbField, __out CvPoint& Left_Top, CvPoint& Left_Bottom, CvPoint& Right_Top, CvPoint& Right_Bottom)
{
	double FieldDistance = (dbField / 1.0) * sqrt(((iWidth / 2.0)*(iWidth / 2.0)) + ((iHeight / 2.0)*(iHeight / 2.0)));

	//Theta
	double Theta_RB = atan((iHeight / 2.0) / (iWidth / 2.0)) * (180 / 3.141592);  // 기준 Theta

	double Theta_LT = (180 + Theta_RB);
	double Theta_LB = (180 - Theta_RB);
	double Theta_RT = (360 - Theta_RB);


	// RB_Field Point
	Right_Bottom.x = FieldDistance * cos(Theta_RB*(3.141592 / 180)) + iWidth / 2.0;
	Right_Bottom.y = FieldDistance * sin(Theta_RB*(3.141592 / 180)) + iHeight / 2.0;

	// RT_Field Point
	Right_Top.x = FieldDistance * cos(Theta_RT* 3.141592 / 180) + iWidth / 2.0;
	Right_Top.y = FieldDistance * sin(Theta_RT* 3.141592 / 180) + iHeight / 2.0;

	// LT_Field Point
	Left_Top.x = FieldDistance * cos(Theta_LT* 3.141592 / 180) + iWidth / 2.0;
	Left_Top.y = FieldDistance * sin(Theta_LT* 3.141592 / 180) + iHeight / 2.0;

	// LB_Field Point
	Left_Bottom.x = FieldDistance * cos(Theta_LB* 3.141592 / 180) + iWidth / 2.0;
	Left_Bottom.y = FieldDistance * sin(Theta_LB* 3.141592 / 180) + iHeight / 2.0;

}