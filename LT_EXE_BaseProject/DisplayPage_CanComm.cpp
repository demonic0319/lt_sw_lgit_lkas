#include "stdafx.h"
#include "DisplayPage_CanComm.h"


CDisplayPage_CanComm::CDisplayPage_CanComm()
{
	m_wnd_MT_PCBComm = NULL;
}

CDisplayPage_CanComm::~CDisplayPage_CanComm()
{
}

//=============================================================================
// Method		: OnCanPg_TextSetSendProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommTestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:27
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg_TextSetSendProtocol(__in UINT nParaIdx, __in enMTCtrl_TestItem enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_PCBComm)
		return;

	m_wnd_MT_PCBComm->SetTextSendProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg_TextSetRecvProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommTestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:27
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg_TextSetRecvProtocol(__in UINT nParaIdx, __in enMTCtrl_TestItem enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_PCBComm)
		return;

	m_wnd_MT_PCBComm->SetTextRecvProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg_TextSetResetInfo
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/18 - 16:27
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg_TextSetResetInfo()
{
	if (NULL == m_wnd_MT_PCBComm)
		return;

	m_wnd_MT_PCBComm->ResetInfo();
}

//=============================================================================
// Method		: OnCanPg2_TextGetTestValue
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg2TestItem enItem
// Parameter	: __out CString & szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg2_TextGetTestValue(__in UINT nParaIdx, __in enMTCtrl_Calibration enItem, __out CString &szValue)
{
	if (NULL == m_wnd_MT_Calibration)
		return;

	m_wnd_MT_Calibration->GetTextValue(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg2_TextSetSendProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg2TestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg2_TextSetSendProtocol(__in UINT nParaIdx, __in enMTCtrl_Calibration enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_Calibration)
		return;

	m_wnd_MT_Calibration->SetTextSendProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg2_TextSetRecvProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg2TestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg2_TextSetRecvProtocol(__in UINT nParaIdx, __in enMTCtrl_Calibration enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_Calibration)
		return;

	m_wnd_MT_Calibration->SetTextRecvProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg2_TextSetResetInfo
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg2_TextSetResetInfo()
{
	if (NULL == m_wnd_MT_Calibration)
		return;

	m_wnd_MT_Calibration->ResetInfo();
}

//=============================================================================
// Method		: OnCanPg3_TextSetSendProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg3TestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg3_TextSetSendProtocol(__in UINT nParaIdx, __in enMTCtrl_VsAlgo enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_ImageTest)
		return;

	m_wnd_MT_ImageTest->SetTextSendProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg3_TextSetRecvProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg3TestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg3_TextSetRecvProtocol(__in UINT nParaIdx, __in enMTCtrl_VsAlgo enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_ImageTest)
		return;

	m_wnd_MT_ImageTest->SetTextRecvProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg3_TextSetResetInfo
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg3_TextSetResetInfo()
{
	if (NULL == m_wnd_MT_ImageTest)
		return;

	m_wnd_MT_ImageTest->ResetInfo();
}

//=============================================================================
// Method		: OnCanPg4_TextGetTestValue
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg4TestItem enItem
// Parameter	: __out CString & szValue
// Qualifier	:
// Last Update	: 2017/12/10 - 10:30
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg4_TextGetTestValue(__in UINT nParaIdx, __in enMTCtrl_Particle enItem, __out CString &szValue)
{
	if (NULL == m_wnd_MT_Particle)
		return;

	m_wnd_MT_Particle->GetTextValue(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg4_TextSetSendProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg4TestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg4_TextSetSendProtocol(__in UINT nParaIdx, __in enMTCtrl_Particle enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_Particle)
		return;

	m_wnd_MT_Particle->SetTextSendProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg4_TextSetRecvProtocol
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in enCanCommPg4TestItem enItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg4_TextSetRecvProtocol(__in UINT nParaIdx, __in enMTCtrl_Particle enItem, __in CString szValue)
{
	if (NULL == m_wnd_MT_Particle)
		return;

	m_wnd_MT_Particle->SetTextRecvProtocol(enItem, szValue);
}

//=============================================================================
// Method		: OnCanPg4_TextSetResetInfo
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/18 - 16:28
// Desc.		:
//=============================================================================
void CDisplayPage_CanComm::OnCanPg4_TextSetResetInfo()
{
	if (NULL == m_wnd_MT_Particle)
		return;

	m_wnd_MT_Particle->ResetInfo();
}