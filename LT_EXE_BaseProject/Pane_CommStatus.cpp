﻿//*****************************************************************************
// Filename	: 	Pane_CommStatus.cpp
// Created	:	2014/7/5 - 10:24
// Modified	:	2015/12/12 - 0:07
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "resource.h"
#include "Pane_CommStatus.h"
#include "CommonFunction.h"
#include "BSocketClient.h"
#include "BSocketServer.h"
#include "Dlg_AcessMode.h"
#include "Dlg_MES_Online.h"
#include "Dlg_OperMode.h"
#include "VisionCam.h"

#define		IDC_BN_TEST_S		1001
#define		IDC_BN_TEST_E		IDC_BN_TEST_S + MAX_TEST_BUTTON_CNT - 1

#define		IDC_PRG_TESTING		1020

#define		IDC_BN_DEVICE_S		1100
#define		IDC_BN_DEVICE_E		IDC_BN_DEVICE_S + DEV_BN_MaxCount

static LPCTSTR m_szDeviceName[] =
{
	_T(""),					//DevSI_PermissionMode
	_T(""),					//DevSI_PermissionMode
	_T("FCM30"),			//DevSI_FCM30
	_T("MES"),				//DevSI_MES
	//_T("Online Mode"),		//DevSI_MES_Online
	_T("Handy BCR"),		//DevSI_HandyBCR
	//_T("Fixed BCR"),		//DevSI_FixedBCR
	_T("PCB Camera 1"),		//DevSI_PCB_1
	_T("PCB Camera 2"),		//DevSI_PCB_2
	_T("Light Brd 1"),		//DevSI_Light_PCB_1
	_T("Light Brd 2"),		//DevSI_Light_PCB_2
	_T("Light Brd 3"),		//DevSI_Light_PCB_3
	_T("Light PSU C"),		//DevSI_LightPSU_1,
	_T("Light PSU L"),		//DevSI_LightPSU_2,
	_T("Light PSU R"),		//DevSI_LightPSU_3,
	_T("Grabber 1"),		//DevSI_Grabber_1
	_T("Grabber 2"),		//DevSI_Grabber_2,
	_T("Video Signal 1"),	//DevSI_VideoSignal_1
	_T("Video Signal 2"),	//DevSI_VideoSignal_2
	_T("PLC"),				//DevSI_PLC
	_T("Digital IO"),		//DevSI_DIO
	_T("Motion"),			//DevSI_Motion
	_T("Touque"),
	_T("Displace Sensor"),
	_T("Vision Camera"),
	NULL
};

static LPCTSTR m_szBnDeviceName[] =
{
	_T("Access Mode		"),		// DEV_BN_PermissionChange
	_T("Operate Mode	"),		// DEV_BN_OperateModeChange
	//_T("Change Online	"),		// DEV_BN_MES_Online
	_T("Keyboard		"),		// DEV_BN_Keyboard
	_T("Manual Barcode	"),		// DEV_BN_Barcode
	_T("Equipment Init	"),		// DEV_BN_EquipmentInit
	_T("Chart Change	"),		// DEV_BN_Chart
	_T("EEPROM Verify	"),		// DEV_BN_EEPROM_Verify
// 	_T("Light CAM		"),		// DEV_BN_Para_Light
// 	_T("Right CAN		"),		// DEV_BN_Para_Right
	NULL
};

static LPCTSTR m_szBnTestName[] =
{
	_T("Camera On"),
	_T("Camera Off"),
	_T("Test 03"),
	_T("Test 04"),
	_T("Test 05"),
	_T("Test 06"),
	_T("Test 07"),
	_T("Test 08"),
	_T("Test 09"),
	_T("Test 10"),
	_T("Test 11"),
	_T("Test 12"),
	NULL
};

//=============================================================================
// CPane_CommStatus
//=============================================================================
IMPLEMENT_DYNAMIC(CPane_CommStatus, CMFCTasksPane)

//=============================================================================
//
//=============================================================================
CPane_CommStatus::CPane_CommStatus()
{
	VERIFY(m_Font.CreateFont(
		16,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

}

//=============================================================================
//
//=============================================================================
CPane_CommStatus::~CPane_CommStatus()
{
	TRACE(_T("<<< Start ~CPane_CommStatus >>> \n"));

	m_Font.DeleteObject();

	TRACE(_T("<<< End ~CPane_CommStatus >>> \n"));
}


BEGIN_MESSAGE_MAP(CPane_CommStatus, CMFCTasksPane)
	ON_WM_CREATE()	
	ON_COMMAND_RANGE			(IDC_BN_TEST_S,		IDC_BN_TEST_E,		OnBnClickedTest)
	ON_UPDATE_COMMAND_UI_RANGE	(IDC_BN_TEST_S,		IDC_BN_TEST_E,		OnUpdateCmdUI_Test)
	ON_COMMAND_RANGE			(IDC_BN_DEVICE_S,	IDC_BN_DEVICE_E,	OnBnClicked_Dev)
	ON_UPDATE_COMMAND_UI_RANGE	(IDC_BN_DEVICE_S,	IDC_BN_DEVICE_E,	OnUpdateCmdUI_Dev)
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

//=============================================================================
// CPane_CommStatus 메시지 처리기입니다.
//=============================================================================

//=============================================================================
// Method		: CPane_CommStatus::OnCreate
// Access		: public 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2015/12/12 - 0:07
// Desc.		:
//=============================================================================
int CPane_CommStatus::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CMFCTasksPane::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < DevSI_MaxCount; nIdx++)
	{
		m_st_Device[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Device[nIdx].SetFont_Gdip(L"Arial", 8.0F);
		m_st_Device[nIdx].Create(m_szDeviceName[nIdx], dwStyle, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < DEV_BN_MaxCount; nIdx++)
	{
		m_bn_Device[nIdx].Create(m_szBnDeviceName[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_DEVICE_S + nIdx);
		m_bn_Device[nIdx].SetMouseCursorHand();
		m_bn_Device[nIdx].SetFont(&m_Font);
	}

#ifdef USE_LOG_WND
	m_st_WarningEvent.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_WarningEvent.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_WarningEvent.SetFont_Gdip(L"Arial", 8.0F);

	m_st_WarningEvent.Create (_T("Connection"), WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN, CRect(0,0,0,0), this, IDC_STATIC);

	if (!m_ed_WarningLog.Create(WS_CHILD | WS_VISIBLE, CRect(0,0,0,0), this, 121) )
	{
		TRACE0("출력 창을 만들지 못했습니다.\n");
		return -1;
	}
	m_ed_WarningLog.SetFont(&m_Font);
	m_ed_WarningLog.HideCaret();
	m_ed_WarningLog.SetBackColor(RGB(0xFF, 0xFF, 0xFF));
#endif
	
	// 테스트용 버튼 만들기
	MakeTestButtons();

	//HINSTANCE hInstance = AfxFindResourceHandle (MAKEINTRESOURCE (nID), RT_GROUP_ICON);
	//HICON hIcon = (HICON)::LoadImage (hInstance, MAKEINTRESOURCE (nID), IMAGE_ICON, 0, 0, LR_DEFAULTCOLOR);
	
	// Pane 옵션 설정
	EnableGroupCollapse (FALSE);
	EnableWrapLabels (TRUE);
	EnableOffsetCustomControls (FALSE);

	// 그룹 추가
	AddPermissionMode();

// 	Add_OperateMode();

#ifndef MES_NOT_USE
	Add_MES();
#endif

	AddTCPIP();
	AddPCISlot();
	AddSerialComm();
	Add_Video();
	Add_VisionVideo();

	AddTorqueCtrl();
	//Add_EquipmentCtrl();
	//Add_ChartCtrl();
	Add_EEPROM();

	//Add_CamSelect();

#ifdef _DEBUG
	//AddTestCtrl();
#ifdef USE_LOG_WND
	AddWarningStatus();
#endif
#endif

	AddUtilities();
	AddSystemInfo ();	

	return 0;
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: protected  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2017/9/21 - 13:12
// Desc.		:
//=============================================================================
BOOL CPane_CommStatus::OnEraseBkgnd(CDC* pDC)
{
	for (UINT nIdx = 0; nIdx < DevSI_MaxCount; nIdx++)
	{
		m_st_Device[nIdx].Invalidate();
	}

// 	for (UINT nIdx = 0; nIdx < DEV_BN_MaxCount; nIdx++)
// 	{
// 		CRect rc;
// 		m_bn_Device[nIdx].GetClientRect(rc);
// 		m_bn_Device[nIdx].SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
// 	}
// 
// 	for (UINT nIdx = 0; nIdx < MAX_TEST_BUTTON_CNT; nIdx++)
// 	{
// 		CRect rc;
// 		m_bn_Test[nIdx].GetClientRect(rc);
// 		m_bn_Test[nIdx].SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
// 	}

	return CMFCTasksPane::OnEraseBkgnd(pDC);
}

//=============================================================================
// Method		: CalcFixedLayout
// Access		: virtual public  
// Returns		: CSize
// Parameter	: __in BOOL bStretch
// Parameter	: __in BOOL bHorz
// Qualifier	:
// Last Update	: 2017/9/13 - 14:54
// Desc.		: Pane의 가로 크기 변경
//=============================================================================
CSize CPane_CommStatus::CalcFixedLayout (__in BOOL bStretch, __in BOOL bHorz)
{
	ASSERT_VALID(this);

	return CSize (m_nPaneWidth, 32767);	
}

//=============================================================================
// Method		: CPane_CommStatus::OnUpdateCmdUI_Test
// Access		: virtual protected 
// Returns		: void
// Parameter	: CCmdUI * pCmdUI
// Qualifier	:
// Last Update	: 2013/6/11 - 19:53
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnUpdateCmdUI_Test( CCmdUI* pCmdUI )
{
	if ((IDC_BN_TEST_S <= pCmdUI->m_nID) && (pCmdUI->m_nID <= IDC_BN_TEST_E))
	{
		UINT nBnIndex = pCmdUI->m_nID - IDC_BN_TEST_S;

		m_bn_Test[nBnIndex].EnableWindow(TRUE);
	}
}

//=============================================================================
// Method		: CPane_CommStatus::OnBnClickedTest
// Access		: protected 
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2013/6/11 - 19:42
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnBnClickedTest(UINT nID)
{
	if ((IDC_BN_TEST_S <= nID) && (nID <= IDC_BN_TEST_E))
	{
		UINT nTestNo = nID - IDC_BN_TEST_S;

		//(AfxGetApp()->GetMainWnd())->SendMessage(WM_TEST_FUNCTION, (WPARAM)nTestNo, 0);
		GetParent()->SendMessage(WM_TEST_FUNCTION, (WPARAM)nTestNo, 0);
	}
}

//=============================================================================
// Method		: OnUpdateCmdUI_Dev
// Access		: protected  
// Returns		: void
// Parameter	: CCmdUI * pCmdUI
// Qualifier	:
// Last Update	: 2016/9/21 - 18:10
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnUpdateCmdUI_Dev(CCmdUI* pCmdUI)
{
	if ((IDC_BN_DEVICE_S <= pCmdUI->m_nID) && (pCmdUI->m_nID <= IDC_BN_DEVICE_E))
	{
		UINT nBnIndex = pCmdUI->m_nID - IDC_BN_DEVICE_S;

		m_bn_Device[nBnIndex].EnableWindow(TRUE);
	}
}

//=============================================================================
// Method		: OnBnClicked_Dev
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/9/21 - 18:10
// Desc.		:
//=============================================================================
void CPane_CommStatus::OnBnClicked_Dev(UINT nID)
{
	if ((IDC_BN_DEVICE_S <= nID) && (nID <= IDC_BN_DEVICE_E))
	{
		UINT nTestNo = nID - IDC_BN_DEVICE_S;

		switch (nTestNo)
		{
		case DEV_BN_PermissionChange:
		{
			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_PERMISSION_MODE, 0, 0);
 			CDlg_AcessMode dlgMode;
			if (IDCANCEL != dlgMode.DoModal())
			{
				SetStatus_PermissionMode(dlgMode.GetAcessMode());
			}
		}
			break;

		case DEV_BN_OperateModeChange:
		{
			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_PERMISSION_MODE, 0, 0);
			CDlg_OperMode dlgMode;
			dlgMode.SetSystemType(m_InsptrType);
			if (IDCANCEL != dlgMode.DoModal())
			{
				//SetStatus_OperateMode(dlgMode.Get_OperateMode());
			}
		}
		break;

// 		case DEV_BN_MES_Online:
// 		{
// 			// MES Online <-> Offline
// 			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_MES_ONLINE_MODE, 0, 0); //MES_Online, MES_Offline
// 			CDlg_MES_Online	dlgGMES;
// 			if (IDCANCEL != dlgGMES.DoModal())
// 			{
// 				//SetStatus_GMES_Online(dlgGMES.Get_MES_OnlineMode());
// 			}
// 
// 		}
// 			break;

		case DEV_BN_Keyboard:
		{
			RunTouchKeyboard();
		}
			break;

		case DEV_BN_Barcode:
		{
			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_MANUAL_BARCODE, 0, 0);
			GetParent()->SendMessage(WM_MANUAL_BARCODE, 0, 0);
		}
			break;

		case DEV_BN_EquipmentInit:
		{
			//(AfxGetApp()->GetMainWnd())->SendMessage(WM_EQP_INIT, 0, 0);
			GetParent()->SendMessage(WM_EQP_INIT, 0, 0);
		}
			break;

		case DEV_BN_Chart:
		{
			GetParent()->SendMessage(WM_CHART_CTRL, 0, 0);
		}
			break;

// 		case DEV_BN_Para_Light:
// 		{
// 			GetParent()->SendMessage(WM_CAMERA_CTRL, Para_Left, 0);
// 		}
// 			break;
// 
// 		case DEV_BN_Para_Right:
// 		{
// 			GetParent()->SendMessage(WM_CAMERA_CTRL, Para_Right, 0);
// 		}
// 			break;

		default:
			break;
		} // End of switch
	}
}

//=============================================================================
// Method		: MakeTestButtons
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/21 - 14:45
// Desc.		:
//=============================================================================
void CPane_CommStatus::MakeTestButtons()
{
	for (int iCnt = 0; iCnt < MAX_TEST_BUTTON_CNT; iCnt++)
	{
		m_bn_Test[iCnt].Create(m_szBnTestName[iCnt], WS_VISIBLE | WS_CHILD, CRect(0, 0, 0, 0), this, IDC_BN_TEST_S + iCnt);
		m_bn_Test[iCnt].SetMouseCursorHand();
		m_bn_Test[iCnt].SetFont(&m_Font);
	}
}

//=============================================================================
// Method		: CPane_CommStatus::AddSystemInfo
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/11/25 - 15:49
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddSystemInfo()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle (), MAKEINTRESOURCE (IDI_ICON_Luritech), IMAGE_ICON, 16, 16, LR_SHARED); 

	int nGroupSysInfo = AddGroup (_T("Program Info"), TRUE, FALSE, hIcon);
	AddLabel (nGroupSysInfo, GetVersionInfo(_T("CompanyName")), -1, TRUE);
	
	CString strText;
	strText.Format(_T("%s (%s)"), GetVersionInfo(_T("ProductVersion")),  GetVersionInfo(_T("FileVersion")));
	AddLabel (nGroupSysInfo, _T("Program Version "));
	AddLabel (nGroupSysInfo, strText, -1, TRUE);	

	COleDateTime now = COleDateTime::GetCurrentTime ();	
	CString strDate = now.Format (_T("Exe :%Y/%m/%d %H:%M:%S"));
	AddLabel (nGroupSysInfo, _T("Execution Time"));
	AddLabel (nGroupSysInfo, strDate, -1, TRUE);	
}

//=============================================================================
// Method		: AddPermissionMode
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/9/21 - 17:12
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddPermissionMode()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Permission"), FALSE, TRUE, hIcon);

	//AddLabel(nGroupCommStatus, _T(""), -1, TRUE);
	AddWindow(nGroupCommStatus, m_st_Device[DevSI_PermissionMode].GetSafeHwnd(), 20);
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_PermissionChange].GetSafeHwnd(), 35);

	SetStatus_PermissionMode(enPermissionMode::Permission_Operator);
}

//=============================================================================
// Method		: Add_OperateMode
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/12 - 20:06
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_OperateMode()
{
	//if (Sys_2D_Cal == m_InsptrType)
	{
		HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

		int nGroupCommStatus = AddGroup(_T("Operate Mode"), FALSE, TRUE, hIcon);

		//AddLabel(nGroupCommStatus, _T(""), -1, TRUE);
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_OperateMode].GetSafeHwnd(), 20);
		AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_OperateModeChange].GetSafeHwnd(), 30);

		SetStatus_OperateMode(enOperateMode::OpMode_Production);
	}
}

//=============================================================================
// Method		: AddPCISlot
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/25 - 10:36
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddPCISlot()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = 0;

	nGroupCommStatus = AddGroup(_T("Motion"), FALSE, TRUE, hIcon);

	if (g_InspectorTable[m_InsptrType].UseDIO)
	{
		//AddWindow(nGroupCommStatus, m_st_Device[DevSI_DIO].GetSafeHwnd(), 20);
	}

	if (g_InspectorTable[m_InsptrType].UseMotion)
	{
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_Motion].GetSafeHwnd(), 20);
	}
}

//=============================================================================
// Method		: AddSerialComm
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2015/12/17 - 13:33
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddSerialComm()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Devices"), FALSE, TRUE, hIcon);

#ifdef USE_BARCODE_SCANNER
	if ((Sys_Focusing == m_InsptrType) || (Sys_Image_Test == m_InsptrType))
	{
		AddLabel(nGroupCommStatus, _T("Barcode Reader"), -1, TRUE);
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_HandyBCR].GetSafeHwnd(), 20);
		//AddWindow(nGroupCommStatus, m_st_Device[DevSI_FixedBCR].GetSafeHwnd(), 20);
	}
#endif

	AddLabel(nGroupCommStatus, _T("Control Board"), -1, TRUE);
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].Grabber_Cnt; nIdx++)
	{
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_PCB_1 + nIdx].GetSafeHwnd(), 20);
	}

	// 변위 센서
	AddLabel(nGroupCommStatus, _T("Keyence LT-9030"), -1, TRUE);
	AddWindow(nGroupCommStatus, m_st_Device[DevSI_Displace].GetSafeHwnd(), 20);
	
	AddLabel(nGroupCommStatus, _T("Light Control"), -1, TRUE);
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightBrd_Cnt; nIdx++)
	{
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_Light_PCB_1 + nIdx].GetSafeHwnd(), 20);
	}
	
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].LightPSU_Cnt; nIdx++)
	{
		//AddLabel(nGroupCommStatus, _T("Power Supply"), -1, TRUE);
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_LightPSU_1 + nIdx].GetSafeHwnd(), 20);
	}
}

//=============================================================================
// Method		: AddTCPIP
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/13 - 17:05
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddTCPIP()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("TCP / IP"), FALSE, TRUE, hIcon);

	//AddLabel(nGroupCommStatus, _T("PLC"), -1, TRUE);
	AddWindow(nGroupCommStatus, m_st_Device[DevSI_FCM30].GetSafeHwnd(), 20);
}

//=============================================================================
// Method		: Add_MES
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/12 - 14:36
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_MES()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("MES"), FALSE, TRUE, hIcon);

	//AddLabel(nGroupCommStatus, _T("MES"), -1, TRUE);
	AddWindow(nGroupCommStatus, m_st_Device[DevSI_MES].GetSafeHwnd(), 20);
//	AddWindow(nGroupCommStatus, m_st_Device[DevSI_MES_Online].GetSafeHwnd(), 20);
//	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_MES_Online].GetSafeHwnd(), 30);
}

//=============================================================================
// Method		: Add_Video
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/1/18 - 11:48
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_Video()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupCommStatus = AddGroup(_T("Video"), FALSE, TRUE, hIcon);

	//AddLabel(nGroupCommStatus, _T("Video"), -1, TRUE);
	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InsptrType].Grabber_Cnt; nIdx++)
	{
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_Grabber_1 + nIdx].GetSafeHwnd(), 20);
		AddWindow(nGroupCommStatus, m_st_Device[DevSI_VideoSignal_1 + nIdx].GetSafeHwnd(), 20);
	}
}

void CPane_CommStatus::Add_VisionVideo()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);
	int nGroupCommStatus = AddGroup(_T("Vision"), FALSE, TRUE, hIcon);
	AddWindow(nGroupCommStatus, m_st_Device[DevSI_Vision].GetSafeHwnd(), 20);
	
}

//=============================================================================
// Method		: Add_EquipmentCtrl
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/12 - 9:23
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_EquipmentCtrl()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);
	int nGroupCommStatus = AddGroup(_T("Equipment Ctrl"), TRUE, FALSE, hIcon);

	//AddLabel(nGroupCommStatus, _T("Utilities"), -1, TRUE);	
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_EquipmentInit].GetSafeHwnd(), 30);
}

//=============================================================================
// Method		: Add_ChartCtrl
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/5 - 14:23
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_ChartCtrl()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);
	int nGroupCommStatus = AddGroup(_T("Chart Ctrl"), TRUE, FALSE, hIcon);

	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_Chart].GetSafeHwnd(), 30);
}

//=============================================================================
// Method		: AddUtilities
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/27 - 10:08
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddUtilities()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);
	int nGroupCommStatus = AddGroup(_T("Utilities"), TRUE, FALSE, hIcon);

	//AddLabel(nGroupCommStatus, _T("Utilities"), -1, TRUE);	
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_Barcode].GetSafeHwnd(), 30);
	AddWindow(nGroupCommStatus, m_bn_Device[DEV_BN_Keyboard].GetSafeHwnd(), 30);
}

//=============================================================================
// Method		: CPane_CommStatus::AddMarkingStatus
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/2/4 - 11:14
// Desc.		:
//=============================================================================
#ifdef USE_LOG_WND
void CPane_CommStatus::AddWarningStatus()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle (), MAKEINTRESOURCE (IDI_ICON_CONNECT), IMAGE_ICON, 16, 16, LR_SHARED);

	int nLogMessage = AddGroup (_T("Log 메세지"), FALSE, TRUE, hIcon);
	
	//AddWindow (nGroupPLCStatus, m_st_WarningEvent.GetSafeHwnd (), 20);
	AddWindow (nLogMessage, m_ed_WarningLog.GetSafeHwnd (), 100/*250*/);
}
#endif


//=============================================================================
// Method		: AddTorqueCtrl
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/8 - 8:31
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddTorqueCtrl()
{
	if (SYS_FOCUSING == m_InsptrType)
	{
		HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);

		int nGroupCommStatus = AddGroup(_T("Torque"), FALSE, TRUE, hIcon);

		AddWindow(nGroupCommStatus, m_st_Device[DevSI_Torque].GetSafeHwnd(), 20);
	}
}

//=============================================================================
// Method		: Add_EEPROM
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/9 - 21:26
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_EEPROM()
{

}

//=============================================================================
// Method		: Add_CamSelect
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/16 - 17:06
// Desc.		:
//=============================================================================
void CPane_CommStatus::Add_CamSelect()
{
// 	if (Sys_2D_Cal != m_InsptrType)
// 	{
// 		HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_ICON_TSP), IMAGE_ICON, 16, 16, LR_SHARED);
// 
// 		int nGroupCamSelect = AddGroup(_T("Camera Select"), FALSE, TRUE, hIcon);
// 
// 		AddWindow(nGroupCamSelect, m_bn_Device[DEV_BN_Para_Light].GetSafeHwnd(), 35);
// 		AddWindow(nGroupCamSelect, m_bn_Device[DEV_BN_Para_Right].GetSafeHwnd(), 35);
// 	}
}

//=============================================================================
// Method		: CPane_CommStatus::AddTestCtrl
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/6/11 - 19:43
// Desc.		:
//=============================================================================
void CPane_CommStatus::AddTestCtrl()
{
	HICON hIcon = (HICON)LoadImage(AfxGetInstanceHandle (), MAKEINTRESOURCE (IDI_ICON_CONNECT), IMAGE_ICON, 16, 16, LR_SHARED);

	int nGroupTestCtrl = AddGroup (_T("TEST"), FALSE, TRUE, hIcon);

	m_nTestButtonCount = (m_nTestButtonCount <= MAX_TEST_BUTTON_CNT) ? m_nTestButtonCount : MAX_TEST_BUTTON_CNT;

	for (UINT iCnt = 0; iCnt < m_nTestButtonCount; iCnt++)
	{
		AddWindow (nGroupTestCtrl, m_bn_Test[iCnt].GetSafeHwnd (), 25);
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2016/9/21 - 20:55
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InsptrType = nSysType;
}

//=============================================================================
// Method		: SetPaneWidth
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nWidth
// Qualifier	:
// Last Update	: 2017/9/21 - 11:51
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetPaneWidth(__in UINT nWidth)
{
	if (m_nPaneWidth != nWidth)
	{
		m_nPaneWidth = nWidth;

		if (GetSafeHwnd())
		{
			// 다시 그리기
			// 부모 윈도우로 다시 그리기 메세지 보냄
			CRect rc;
			//AfxGetApp()->GetMainWnd()->GetClientRect(rc);
			GetParent()->GetClientRect(rc);
			GetParent()->SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
		}
	}
}

//=============================================================================
// Method		: SetTestButtonCount
// Access		: public  
// Returns		: void
// Parameter	: __in BYTE nCount
// Qualifier	:
// Last Update	: 2017/9/21 - 11:51
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetTestButtonCount(__in BYTE nCount)
{
	if (m_nTestButtonCount != nCount)
	{
		m_nTestButtonCount = nCount;
	}
}

//=============================================================================
// Method		: SetStatus_PermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode InspMode
// Qualifier	:
// Last Update	: 2016/9/21 - 20:56
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_PermissionMode(__in enPermissionMode InspMode)
{
	m_st_Device[DevSI_PermissionMode].SetText(g_szPermissionMode[InspMode]);

	switch (InspMode)
	{
	case Permission_Operator:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case Permission_Manager:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case Permission_Administrator:
		m_st_Device[DevSI_PermissionMode].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_OperateMode
// Access		: public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/12 - 20:05
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_OperateMode(__in enOperateMode nOperMode)
{
	m_st_Device[DevSI_OperateMode].SetText(g_szOperateMode[nOperMode]);

	switch (nOperMode)
	{
	case OpMode_Production:
		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case OpMode_Master:
		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case OpMode_StartUp_Check:
		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case OpMode_DryRun:
		m_st_Device[DevSI_OperateMode].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_HandyBCR
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bConStatus
// Qualifier	:
// Last Update	: 2016/10/6 - 20:48
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_HandyBCR(__in BOOL bConStatus)
{
	if (bConStatus)
	{
		m_st_Device[DevSI_HandyBCR].SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_Device[DevSI_HandyBCR].SetText(_T("Connected"));
	}
	else
	{
		m_st_Device[DevSI_HandyBCR].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Device[DevSI_HandyBCR].SetText(_T("Disconnected"));
	}
}

//=============================================================================
// Method		: SetStatus_FixedBCR
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bConStatus
// Qualifier	:
// Last Update	: 2017/9/12 - 14:31
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_FixedBCR(__in BOOL bConStatus)
{
	if (bConStatus)
	{
		//m_st_Device[DevSI_FixedBCR].SetColorStyle(CVGStatic::ColorStyle_Green);
		//m_st_Device[DevSI_FixedBCR].SetText(_T("Connected"));
	}
	else
	{
		//m_st_Device[DevSI_FixedBCR].SetColorStyle(CVGStatic::ColorStyle_Red);
		//m_st_Device[DevSI_FixedBCR].SetText(_T("Disconnected"));
	}
}

//=============================================================================
// Method		: SetStatus_CameraBoard
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2017/1/15 - 15:28
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_CameraBoard(__in UINT nConStatus, __in UINT nIndex /*= 0*/)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;
	UINT nIdx = DevSI_PCB_1 + nIndex;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		//m_st_Device[nIdx].SetText(_T("Opened"));
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_DeepDarkGray);
		//m_st_Device[nIdx].SetText(_T("Closed"));
		break;

	case COMM_STATUS_ERROR:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		//m_st_Device[nIdx].SetText(_T("Error"));
		break;

	case COMM_STATUS_SYNC_OK:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		//m_st_Device[nIdx].SetText(_T("Sync OK"));
		break;

	case COMM_STATUS_NO_ACK:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Orange);
		//m_st_Device[nIdx].SetText(_T("No ACK"));
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_LightBoard
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2017/9/12 - 14:31
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_LightBoard(__in UINT nConStatus, __in UINT nIndex /*= 0*/)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;
	UINT nIdx = DevSI_Light_PCB_1 + nIndex;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		//m_st_Device[nIdx].SetText(_T("Opened"));
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_DeepDarkGray);
		//m_st_Device[nIdx].SetText(_T("Closed"));
		break;

	case COMM_STATUS_ERROR:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		//m_st_Device[nIdx].SetText(_T("Error"));
		break;

	case COMM_STATUS_SYNC_OK:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		//m_st_Device[nIdx].SetText(_T("Sync OK"));
		break;

	case COMM_STATUS_NO_ACK:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Orange);
		//m_st_Device[nIdx].SetText(_T("No ACK"));
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_LightPSU
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2017/11/8 - 11:00
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_LightPSU(__in UINT nConStatus, __in UINT nIndex /*= 0*/)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;
	UINT nIdx = DevSI_LightPSU_1 + nIndex;
	//UINT nIdx = DevSI_PowerSupply;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		//m_st_Device[nIdx].SetText(_T("Opened"));
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_DeepDarkGray);
		//m_st_Device[nIdx].SetText(_T("Closed"));
		break;

	case COMM_STATUS_ERROR:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		//m_st_Device[nIdx].SetText(_T("Error"));
		break;

	case COMM_STATUS_SYNC_OK:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		//m_st_Device[nIdx].SetText(_T("Sync OK"));
		break;

	case COMM_STATUS_NO_ACK:
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Orange);
		//m_st_Device[nIdx].SetText(_T("No ACK"));
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_MES
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nStatus
// Qualifier	:
// Last Update	: 2016/12/28 - 17:45
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_MES(__in UINT nStatus)
{
	switch (nStatus)
	{
	case enTCPIPConnectStatus::COMM_CONNECTED:
		m_st_Device[DevSI_MES].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case enTCPIPConnectStatus::COMM_CONNECTED_SYNC_OK:
		m_st_Device[DevSI_MES].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case enTCPIPConnectStatus::COMM_DISCONNECT:
		m_st_Device[DevSI_MES].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		break;

	case enTCPIPConnectStatus::COMM_CONNECT_DROP:
		m_st_Device[DevSI_MES].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case enTCPIPConnectStatus::COMM_CONNECT_ERROR:
		m_st_Device[DevSI_MES].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;
	}
}

//=============================================================================
// Method		: SetStatus_MES_Online
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nStatus
// Qualifier	:
// Last Update	: 2018/3/1 - 10:17
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_MES_Online(__in UINT nStatus)
{
	switch (nStatus)
	{
	case MES_Offline:
		//m_st_Device[DevSI_MES_Online].SetColorStyle(CVGStatic::ColorStyle_Red);
		//m_st_Device[DevSI_MES_Online].SetText(L"Offline");
		break;

	case MES_Online:
		//m_st_Device[DevSI_MES_Online].SetColorStyle(CVGStatic::ColorStyle_Green);
		//m_st_Device[DevSI_MES_Online].SetText(L"Online");
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_DIO
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nStatus
// Qualifier	:
// Last Update	: 2017/9/12 - 14:32
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_DIO(__in UINT nStatus)
{
	switch (nStatus)
	{
	case 0:	// Fail
		m_st_Device[DevSI_DIO].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		break;

	case 1: // OK
		m_st_Device[DevSI_DIO].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case 2:	// Error
		m_st_Device[DevSI_DIO].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;
	}
}

//=============================================================================
// Method		: SetStatus_Motion
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nStatus
// Qualifier	:
// Last Update	: 2017/9/12 - 14:32
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_Motion(__in UINT nStatus)
{
	switch (nStatus)
	{
	case 0:	// Fail
		m_st_Device[DevSI_Motion].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		break;

	case 1: // OK
		m_st_Device[DevSI_Motion].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case 2:	// Error
		m_st_Device[DevSI_Motion].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;
	}
}

//=============================================================================
// Method		: SetStatus_GrabBoard
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bConStatus
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2018/2/1 - 10:39
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_GrabBoard(__in BOOL bConStatus, __in UINT nIndex /*= 0*/)
{
	UINT nIdx = DevSI_Grabber_1 + nIndex;

	if (bConStatus)
	{
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_Device[nIdx].SetText(_T("Connected"));
	}
	else
	{
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Device[nIdx].SetText(_T("Disconnected"));
	}
}

//=============================================================================
// Method		: SetStatus_VideoSignal
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bConStatus
// Parameter	: __in UINT nIndex
// Qualifier	:
// Last Update	: 2018/2/1 - 10:39
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_VideoSignal(__in BOOL bConStatus, __in UINT nIndex /*= 0*/)
{
	UINT nIdx = DevSI_VideoSignal_1 + nIndex;

	if (bConStatus)
	{
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_Device[nIdx].SetText(_T("On"));
	}
	else
	{
		m_st_Device[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Device[nIdx].SetText(_T("Off"));
	}
}


void CPane_CommStatus::SetStatus_DisplaceSensor(__in BOOL bConStatus)
{
	if (bConStatus)
	{
		m_st_Device[DevSI_Displace].SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_Device[DevSI_Displace].SetText(_T("Connected"));
	}
	else
	{
		m_st_Device[DevSI_Displace].SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Device[DevSI_Displace].SetText(_T("Disconnected"));
	}
}


void CPane_CommStatus::SetStatus_VisionCam(__in BOOL bConnect, __in UINT nCamIdx /*= 0*/)
{
	CVGStatic* pStatic = NULL;

	pStatic = &m_st_Device[DevSI_Vision];

	switch (bConnect)
	{
	case CVisionCam::COMM_CONNECTED:
		pStatic->SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case CVisionCam::COMM_DISCONNECT:
	case CVisionCam::COMM_CONNECT_DROP:
		pStatic->SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case CVisionCam::COMM_CONNECT_ERROR:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_Torque
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nConStatus
// Qualifier	:
// Last Update	: 2018/3/8 - 8:34
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_Torque(__in UINT nConStatus)
{
	enCommStatusType nStatus = (enCommStatusType)nConStatus;

	switch (nStatus)
	{
	case COMM_STATUS_CONNECT:
		m_st_Device[DevSI_Torque].SetColorStyle(CVGStatic::ColorStyle_Yellow);
		break;

	case COMM_STATUS_NOTCONNECTED:
	case COMM_STATUS_DISCONNECT:
		m_st_Device[DevSI_Torque].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case COMM_STATUS_SYNC_OK:
		m_st_Device[DevSI_Torque].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetStatus_FCM30
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nStatus
// Qualifier	:
// Last Update	: 2018/9/19 - 15:21
// Desc.		:
//=============================================================================
void CPane_CommStatus::SetStatus_FCM30(__in UINT nStatus)
{
	switch (nStatus)
	{
	case enTCPIPConnectStatus::COMM_CONNECTED:
		m_st_Device[DevSI_FCM30].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case enTCPIPConnectStatus::COMM_CONNECTED_SYNC_OK:
		m_st_Device[DevSI_FCM30].SetColorStyle(CVGStatic::ColorStyle_Green);
		break;

	case enTCPIPConnectStatus::COMM_DISCONNECT:
		m_st_Device[DevSI_FCM30].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		break;

	case enTCPIPConnectStatus::COMM_CONNECT_DROP:
		m_st_Device[DevSI_FCM30].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;

	case enTCPIPConnectStatus::COMM_CONNECT_ERROR:
		m_st_Device[DevSI_FCM30].SetColorStyle(CVGStatic::ColorStyle_Red);
		break;
	}
}
