﻿//*****************************************************************************
// Filename	: 	Def_Motion.h
// Created	:	2016/10/03 - 16:40
// Modified	:	2016/10/03 - 16:40
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Motion_h__
#define Def_Motion_h__

#include <afxwin.h>

#define IR_MotorX_Crash 168000
#define IR_Chart_Crash	50000

//=============================================================================
// Motor Axis Name
//=============================================================================
// 2D CAL 검사 -------------------------------------
typedef enum enAxis_2D_CAL 
{
	AX_2D_StageX = 0,
	AX_2D_StageY,
	AX_2D_StageZ,
	AX_2D_StageR,
	AX_2D_ChartR,
	AX_2D_Shutter,
	AX_2D_MAX
};

typedef enum enTeach_2D_CAL
{
	TC_2D_Load_X = 0,
	TC_2D_Load_Y,
	TC_2D_Load_Z,
	TC_2D_Load_R,
	TC_2D_ShutterOpen,
	TC_2D_ShutterClose,
	TC_2D_Center_X,
	TC_2D_Center_Y,
	TC_2D_Center_R,
	TC_2D_Distance,	
	TC_2D_MAX,
};

static LPCTSTR g_szTeachItem_2D_CAL[] =
{
	_T("Load X "),
	_T("Load Y "),
	_T("Load Z "),
	_T("Load R "),
	_T("Load Shutter Open "),
	_T("Load Shutter Close "),
	_T("Center X "),
	_T("Center Y "),
	_T("Center R "),
	_T("Distance "),
	NULL
};

// Image 검사 -------------------------------------
typedef enum enAxis_Image
{
	AX_Image_StageX = 0,
	AX_Image_StageY,
	AX_Image_StageZ,
	AX_Image_StageR,
	AX_Image_Shutter,
	AX_Image_MAX
};

typedef enum enTeach_Image
{
	TC_Imgt_Load_X	= 0,				
	TC_Imgt_Load_Y,					
	TC_Imgt_Load_Z,					
	TC_Imgt_Load_R,					
	TC_Imgt_Test_R,					
	TC_Imgt_Test_Front_Z,			
	TC_Imgt_Chart_Front_X,			
	TC_Imgt_Par_Front_X,			
	TC_Imgt_Par_Y,					
	TC_Imgt_Distance,				
	TC_Imgt_Crash_X,				
	TC_Imgt_Shutter,							
	TC_Imgt_MAX
};

static LPCTSTR g_szTeachItem_Image[] =
{
	_T("Loding X (Puls)		"),			// TC_Imgt_Load_X	= 0,	
	_T("Loding Y (Puls)		"),			// TC_Imgt_Load_Y,			
	_T("Loding Z (Puls)		"),			// TC_Imgt_Load_Z,			
	_T("Loding R (Puls)		"),			// TC_Imgt_Load_R,			
	_T("TEST R (Puls)		"),			// TC_Imgt_Test_R,			
	_T("TEST Z (Puls)		"),			// TC_Imgt_Test_Front_Z,	
	_T("TEST X (Puls)		"),			// TC_Imgt_Chart_Front_X,	
	_T("PAR X (Puls)		"),			// TC_Imgt_Par_Front_X,	
	_T("PAR Y (Puls)		"),			// TC_Imgt_Par_Y,			
	_T("Distance			"),			// TC_Imgt_Distance,		
	_T("Crash X (Puls)		"),			// TC_Imgt_Crash_X,		
	_T("Shutter (Puls)		"),			// TC_Imgt_Shutter,		
	NULL	  
};

// IR_Image 검사 -------------------------------------
typedef enum enAxis_IR_Image
{
	AX_IR_Image_LoadingY = 0,
	AX_IR_Image_LoadingX,
	AX_IR_Image_StageZ,
	AX_IR_Image_StageR,
	AX_IR_Image_StageTX,
	AX_IR_Image_StageTY,
	AX_IR_Image_MAX
};

typedef enum enTeach_IR_Image
{
	// Loading / Unloading
	TC_IR_Imgt_Load_Y,
	TC_IR_Imgt_Load_X,
	TC_IR_Imgt_Load_Z,
	TC_IR_Imgt_Load_R,
	TC_IR_Imgt_Load_TX,
	TC_IR_Imgt_Load_TY,

	// Vision
	TC_IR_Imgt_Vision_Y,
	TC_IR_Imgt_Vision_X,
	TC_IR_Imgt_Vision_Z,
	TC_IR_Imgt_Vision_R,
	TC_IR_Imgt_Vision_TX,
	TC_IR_Imgt_Vision_TY,

	// Displace A
	TC_IR_Imgt_DisplaceA_Y,
	TC_IR_Imgt_DisplaceA_X,
	TC_IR_Imgt_DisplaceA_Z,
	TC_IR_Imgt_DisplaceA_R,
	TC_IR_Imgt_DisplaceA_TX,
	TC_IR_Imgt_DisplaceA_TY,

	// Displace B
	TC_IR_Imgt_DisplaceB_Y,
	TC_IR_Imgt_DisplaceB_X,
	TC_IR_Imgt_DisplaceB_Z,
	TC_IR_Imgt_DisplaceB_R,
	TC_IR_Imgt_DisplaceB_TX,
	TC_IR_Imgt_DisplaceB_TY,

	// Displace C
	TC_IR_Imgt_DisplaceC_Y,
	TC_IR_Imgt_DisplaceC_X,
	TC_IR_Imgt_DisplaceC_Z,
	TC_IR_Imgt_DisplaceC_R,
	TC_IR_Imgt_DisplaceC_TX,
	TC_IR_Imgt_DisplaceC_TY,

	// Displace D
	TC_IR_Imgt_DisplaceD_Y,
	TC_IR_Imgt_DisplaceD_X,
	TC_IR_Imgt_DisplaceD_Z,
	TC_IR_Imgt_DisplaceD_R,
	TC_IR_Imgt_DisplaceD_TX,
	TC_IR_Imgt_DisplaceD_TY,

	// Chart
	TC_IR_Imgt_Chart_Y,
	TC_IR_Imgt_Chart_X,
	TC_IR_Imgt_Chart_Z,
	TC_IR_Imgt_Chart_R,
	TC_IR_Imgt_Chart_TX,
	TC_IR_Imgt_Chart_TY,

	// Blemish
	TC_IR_Imgt_Blemish_Y,
	TC_IR_Imgt_Blemish_X,
	TC_IR_Imgt_Blemish_Z,
	TC_IR_Imgt_Blemish_R,
	TC_IR_Imgt_Blemish_TX,
	TC_IR_Imgt_Blemish_TY,

	TC_IR_Imgt_MAX
};

static LPCTSTR g_szTeachItem_IR_Image[] =
{
	// Loading / Unloading
	_T("Y  [Puls]"),
	_T("X  [Puls]"),			
	_T("Z  [Puls]"),			
	_T("R  [Puls]"),			
	_T("TX [Puls]"),			
	_T("TY [Puls]"),			

	// Vision
	_T("Y  [Puls]"),
	_T("X  [Puls]"),			
	_T("Z  [Puls]"),			
	_T("R  [Puls]"),			
	_T("TX [Puls]"),			
	_T("TY [Puls]"),	

	// Displace A
	_T("Y  [Puls]"),
	_T("X  [Puls]"),
	_T("Z  [Puls]"),
	_T("R  [Puls]"),
	_T("TX [Puls]"),
	_T("TY [Puls]"),

	// Displace B
	_T("Y  [Puls]"),
	_T("X  [Puls]"),
	_T("Z  [Puls]"),
	_T("R  [Puls]"),
	_T("TX [Puls]"),
	_T("TY [Puls]"),

	// Displace C
	_T("Y  [Puls]"),
	_T("X  [Puls]"),
	_T("Z  [Puls]"),
	_T("R  [Puls]"),
	_T("TX [Puls]"),
	_T("TY [Puls]"),

	// Displace D
	_T("Y  [Puls]"),
	_T("X  [Puls]"),
	_T("Z  [Puls]"),
	_T("R  [Puls]"),
	_T("TX [Puls]"),
	_T("TY [Puls]"),

	// Chart
	_T("Y  [Puls]"),
	_T("X  [Puls]"),
	_T("Z  [Puls]"),
	_T("R  [Puls]"),
	_T("TX [Puls]"),
	_T("TY [Puls]"),

	// Blemish
	_T("Y  [Puls]"),
	_T("X  [Puls]"),
	_T("Z  [Puls]"),
	_T("R  [Puls]"),
	_T("TX [Puls]"),
	_T("TY [Puls]"),

	NULL
};

// Focus 검사 -------------------------------------
typedef enum enAxis_Focus
{
	AX_Focus_StageX = 0,
	AX_Focus_StageY,
	AX_Focus_StageR,
	AX_Focus_MAX
};

typedef enum enTeach_Focus
{
	TC_Focus_CenterX = 0,
	TC_Focus_CenterY,
	TC_Focus_CenterR,
	TC_Focus_MAX,
};

static LPCTSTR g_szTeachItem_Focus[] =
{
	_T("Center X (Puls)"),
	_T("Center Y (Puls)"),
	_T("Center R (Puls)"),
	NULL
};

#if (SET_INSPECTOR == SYS_2D_CAL)
	#define		TC_ALL_MAX		TC_2D_MAX
#elif (SET_INSPECTOR == SYS_FOCUSING)
	#define		TC_ALL_MAX		TC_Focus_MAX
#elif (SET_INSPECTOR == SYS_IMAGE_TEST)
	#define 	TC_ALL_MAX		TC_Imgt_MAX
#elif (SET_INSPECTOR == SYS_IR_IMAGE_TEST)
	#define 	TC_ALL_MAX		TC_IR_Imgt_MAX
#else
	#define		TC_ALL_MAX		TC_2D_MAX
#endif

//------------------------------------------------------
// TEACH 위치 정보
//------------------------------------------------------
typedef struct _tag_TeachInfo
{
	double dbTeachData[TC_ALL_MAX];

	_tag_TeachInfo()
	{
		for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
			dbTeachData[nIdx] = 0.0;
	}

	_tag_TeachInfo& operator= (_tag_TeachInfo& ref)
	{
		for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
			dbTeachData[nIdx] = ref.dbTeachData[nIdx];

		return *this;
	};
}ST_TeachInfo, *LPST_TeachInfo;

#endif // Def_Motion_h__
