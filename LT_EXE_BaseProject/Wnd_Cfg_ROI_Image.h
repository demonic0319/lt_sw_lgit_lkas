﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_ROI_Image.h
// Created	:	2018/2/8 - 16:34
// Modified	:	2018/2/8 - 16:34
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef Wnd_Cfg_ROI_Image_h__
#define Wnd_Cfg_ROI_Image_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_ROI_Image
//-----------------------------------------------------------------------------
class CWnd_Cfg_ROI_Image : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_ROI_Image)

public:
	CWnd_Cfg_ROI_Image();
	virtual ~CWnd_Cfg_ROI_Image();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	CFont			m_font_Default;
	CFont			m_font_Data;

	// Image View Title
	// Load File
	// Capture On
	// Capture Off

	// 밝기 조절

	// 영상 뷰어

	// 오버레이 그리기

	

	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_2D_Cal;

	// UI에 세팅 된 데이터 -> 구조체
	void		GetUIData			(__out ST_RecipeInfo& stRecipeInfo);
	// 구조체 -> UI에 세팅
	void		SetUIData			(__in const ST_RecipeInfo* pRecipeInfo);

public:
	
	void		SetSystemType		(__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	}

	// 모델 데이터를 UI에 표시
	void		SetRecipeInfo		(__in const ST_RecipeInfo* pRecipeInfo);
	// UI에 표시된 데이터의 구조체 반환	
	void		GetRecipeInfo		(__out ST_RecipeInfo& stRecipeInfo);


	// 영상 뷰어 출력 용도
	void		ShowVideo			(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void		NoSignal_Ch			(__in INT iChIdx);
};

#endif // Wnd_Cfg_ROI_Image_h__


