// Wnd_LayoutFrame.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_LayoutFrame.h"

// CWnd_LayoutFrame

IMPLEMENT_DYNAMIC(CWnd_LayoutFrame, CWnd_BaseView)

CWnd_LayoutFrame::CWnd_LayoutFrame()
{
}

CWnd_LayoutFrame::~CWnd_LayoutFrame()
{
}

BEGIN_MESSAGE_MAP(CWnd_LayoutFrame, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
END_MESSAGE_MAP()

// CWnd_LayoutFrame 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/12/13 - 18:40
// Desc.		:
//=============================================================================
int CWnd_LayoutFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	// Layout image

	// para control button
	for (UINT nIdx = 0; nIdx < Para_MaxEnum; nIdx++)
	{
		m_wndParaCtrl[nIdx].SetBackgroundColor(Gdiplus::Color::White);
		m_wndParaCtrl[nIdx].SetTitle((WCHAR*)g_szParaName[nIdx]);
		m_wndParaCtrl[nIdx].SetParaIdx(nIdx);
		m_wndParaCtrl[nIdx].Create(g_szParaName[nIdx], dwStyle, rectDummy, this, 100 + nIdx);
		m_wndParaCtrl[nIdx].SetOwner(GetParent());
	}
	
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/12/13 - 18:40
// Desc.		:
//=============================================================================
void CWnd_LayoutFrame::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin = 5;

	int iLeft = iMargin;
	int iTop = iMargin;
	
	int iWidth = cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int iFrameWidth = iWidth * 2 / 5;
	int iFrameHeight = iHeight;

//	int iLeftIdx = iLeft;
//	int iFrameIdx[] = { 0, 2 };
// 	for (UINT nIdx = 0; nIdx < Para_MaxEnum; nIdx++)
// 	{
// 		iLeftIdx = (iLeft + (iFrameIdx[nIdx] * iFrameWidth));
// 		m_wndParaCtrl[nIdx].MoveWindow(iLeftIdx, iTop, iFrameWidth, iFrameHeight);
// 	}

	m_wndParaCtrl[Para_Left].MoveWindow(iLeft, iTop, iFrameWidth, iFrameHeight);

	if (1 < m_nParaCount)
	{
		iLeft = cx - iFrameWidth - iMargin;
		m_wndParaCtrl[Para_Right].MoveWindow(iLeft, iTop, iFrameWidth, iFrameHeight);
	}
	else
	{
		m_wndParaCtrl[Para_Right].MoveWindow(0, 0, 0, 0);
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/5/29 - 19:39
// Desc.		:
//=============================================================================
BOOL CWnd_LayoutFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Add your specialized code here and/or call the base class
	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetParaCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/12/14 - 9:37
// Desc.		:
//=============================================================================
void CWnd_LayoutFrame::SetParaCount(__in UINT nCount)
{
	m_nParaCount = (nCount <= Para_MaxEnum) ? nCount : Para_MaxEnum;
}
