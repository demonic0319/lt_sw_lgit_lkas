#pragma once

// CWnd_MT_PCBComm
#ifndef Wnd_MT_PCBComm_h__
#define Wnd_MT_PCBComm_h__

#include "VGStatic.h"

#include "Def_DataStruct.h"
#include "Def_MTCtrl_Item.h"

class CWnd_MT_PCBComm : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MT_PCBComm)

public:
	CWnd_MT_PCBComm();
	virtual ~CWnd_MT_PCBComm();

	enum 
	{
		Title_CANComm_Name,
		Title_CANComm_Send,
		Title_CANComm_Recv,
		Title_CANComm_Func,
		Title_CANComm_MaxNum,
	};

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	void			OnRangeCmds				(__in UINT nID);
	void			OnRangePort				(__in UINT nID);

	void			SetTextSendProtocol		(__in UINT nTestItem, __in CString szValue);
	void			SetTextRecvProtocol		(__in UINT nTestItem, __in CString szValue);
	void			ResetInfo				();
	void			SetButtonEnable			(__in BOOL bEnable);

	CVGStatic		m_st_Name;
	CVGStatic		m_stTitle[Title_CANComm_MaxNum];

	CMFCButton		m_bn_Item[Para_MaxEnum];

	CVGStatic		m_stTestName[MTCtrl_MaxEnum];
	CVGStatic		m_stSendProtol[MTCtrl_MaxEnum];
	CVGStatic		m_stRecvProtol[MTCtrl_MaxEnum];
	CMFCButton		m_btTestFunc[MTCtrl_MaxEnum];

	CFont			m_font;
	UINT			m_nPort;
		
protected:
	DECLARE_MESSAGE_MAP()
};


#endif // Wnd_MT_PCBComm_h__