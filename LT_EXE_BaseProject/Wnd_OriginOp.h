﻿//*****************************************************************************
// Filename	: 	Wnd_OriginOp.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "resource.h"
#include "Def_Motion.h"
#include "MotionManager.h"

#define	 OR_WIDTH_OFFSET 1.0

typedef enum OriginOp_ID
{
	IDC_ET_VEL1_DATA = 1000,
	IDC_ET_VEL4_DATA,
	IDC_ET_VEL2_DATA,
	IDC_ET_ACC1_DATA,
	IDC_ET_OFFSET_DATA,
	IDC_ET_VEL3_DATA,
	IDC_ET_ACC2_DATA,
	IDC_BN_SETTING,
	IDC_OP_BN_SAVE,
	IDC_BN_ETC_START,
	IDC_BN_ALL_START,
	IDC_BN_STOP,
	IDC_CB_DIR_DATA,
	IDC_CB_SIG_DATA,
	IDC_MAXNUM,
};

enum enOriginOpStatic
{
	ST_OR_DIR = 0,
	ST_OR_VEL1,
	ST_OR_VEL4,
	ST_OR_SIG,
	ST_OR_VEL2,
	ST_OR_ACC1,
	ST_OR_OFFSET,
	ST_OR_VEL3,
	ST_OR_ACC2,
	ST_OR_MAXNUM,
};

static LPCTSTR g_szOriginOpName[] =
{
	_T("Direction"),
	_T("Vel First"),
	_T("Vel Last"),
	_T("Signal"),
	_T("Vel Second"),
	_T("Acc First"),
	_T("Offset"),
	_T("Vel Third"),
	_T("Acc Second"),
	NULL
};

enum enOriginOpEditItem
{
	ED_OR_VEL1 = 0,
	ED_OR_VEL4,
	ED_OR_VEL2,
	ED_OR_ACC1,
	ED_OR_OFFSET,
	ED_OR_VEL3,
	ED_OR_ACC2,
	ED_OR_MAXNUM,
};

enum enOriginOpButton
{
	BN_OR_SETTING = 0,
	BN_OR_SAVE,
	BN_OR_ETC_START,
	BN_OR_ALL_START,
	BN_OR_STOP,
	BN_OR_MAXNUM,
};

static LPCTSTR g_szOriginOpButton[] =
{
	_T("SETTING"),
	_T("SAVE"),
	_T("ETC START"),
	_T("ALL START"),
	_T("STOP"),
	NULL
};

enum enOriginOpCommbo
{
	CB_OR_DIR = 0,
	CB_OR_SIGNAL,
	CB_OR_MAXNUM,
};

static LPCTSTR g_szOriginComboDir[] =
{
	_T(" 0 : Dir Ccw(-)"),
	_T(" 1 : Dir Cw(+)"),
	NULL
};

static LPCTSTR g_szOriginComboSignal[] =
{
	_T(" 0 : PosEndLimit"),
	_T(" 1 : NegEndLimit"),
	_T(" 4 : Home Sensor"),
	_T(" 5 : Encod Zphas"),
	NULL
};

// CWnd_OriginOp
class CWnd_OriginOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_OriginOp)

public:
	CWnd_OriginOp();
	virtual ~CWnd_OriginOp();

	CMotionManager*	m_pstDevice;

	void		SetPtr_Device	(__in CMotionManager* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	void		SetUpdateData	();
	void		SetSelectAxis	(UINT nAxis);

protected:

	CFont			m_font;
	UINT			m_nAxis;
	CComboBox		m_cb_Item[CB_OR_MAXNUM];
	CVGStatic		m_st_Item[ST_OR_MAXNUM];
	CMFCButton		m_bn_Item[BN_OR_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[ED_OR_MAXNUM];
	CVGStatic		m_st_Name;

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnEtcStart();
	afx_msg void	OnBnClickedBnAllStart();
	afx_msg void	OnBnClickedBnStop();
	afx_msg void	OnBnClickedBnSave();
	afx_msg void	OnBnClickedBnSetting();
	virtual BOOL	PreTranslateMessage(MSG* pMsg);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	void			GetUpdateData();

	DECLARE_MESSAGE_MAP()
};