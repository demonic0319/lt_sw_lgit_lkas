#ifndef Wnd_CameraView_Cv_h__
#define Wnd_CameraView_Cv_h__

#pragma once
#include "Wnd_BaseView.h"
#include "cv.h"
#include "highgui.h"
#include "VGStatic.h"
// CWnd_CameraView_Cv

class CWnd_CameraView_Cv : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_CameraView_Cv)

public:
	CWnd_CameraView_Cv();
	virtual ~CWnd_CameraView_Cv();

	CVGStatic			m_CameraFrame;

	BITMAPINFO * m_pBitmapInfo;

protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnSize(UINT nType, int cx, int cy);

	void Display_NoImage();
	void CameraMode(UINT nMode);
	void ShowVideo(__in INT iChIdx, __in  IplImage *TestImage, __in DWORD dwWidth, __in DWORD dwHeight);
};


#endif // Wnd_CameraView_Cv_h__
