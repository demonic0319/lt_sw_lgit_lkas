﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_DAQ.h
// Created	:	2016/3/14 - 10:56
// Modified	:	2016/3/14 - 10:56
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_DAQ_h__
#define Wnd_Cfg_DAQ_h__

#pragma once
#include "Wnd_BaseView.h"
#include "Def_Enum.h"
#include "VGStatic.h"
#include "Def_DataStruct.h"
#include "Def_DAQWrapper.h"


enum enDAQ_Static
{
	STI_DAQ_Cam_BoardNum = 0,
	STI_DAQ_Cam_Data_Type,
	STI_DAQ_Cam_Signal_Type,
	STI_DAQ_Cam_MIPI_Lane,
	STI_DAQ_Cam_Sensor_Type,
	STI_DAQ_Cam_Color_Array,
	STI_DAQ_Cam_Width_Multiple,
	STI_DAQ_Cam_Hsync_Polarity,
	STI_DAQ_Cam_Pclk_Polarity,
	STI_DAQ_Cam_DVal_Use,
	STI_DAQ_Cam_Clock_Select,
	STI_DAQ_Cam_Clock_Hz,
	STI_DAQ_Cam_Clock_Use,
	//STI_DAQ_Cam_I2c_Enable,
	STI_DAQ_Cam_Height_Multiple,
	STI_DAQ_MAXNUM,
};

enum enDAQ_Combobox
{
	CBO_DAQ_BoardNumber = 0,
	CBO_DAQ_DataType,
	CBO_DAQ_SignalType,
	CBO_DAQ_MIPILane,
	CBO_DAQ_SensorType,
	CBO_DAQ_ColorArray,
	CBO_DAQ_HsyncPolarity,
	CBO_DAQ_PClkPolarity,
	CBO_DAQ_DValUse,
	CBO_DAQ_ClockSelect,
	CBO_DAQ_ClockUse,
	//CBO_DAQ_I2CEnable,
	//CBO_DAQ_I2CFile,
	CBO_DAQ_MAXNUM,
};

enum enDAQ_Edit
{
	EDT_DAQ_WidthMultiple = 0,
	EDT_DAQ_HeightMultiple,
	EDT_DAQ_ClockHz,
	EDT_DAQ_MAXNUM,
};

enum enDAQ_I2CFile
{
	FILESET_DAQ_I2CFile = 0,
	FILESET_DAQ_I2CFile_2,
	FILESET_DAQ_MAXNUM,
};

static LPCTSTR g_szI2CFile[] =
{
	_T("I2C File"),
	_T("I2C File_이물"),
};

//-----------------------------------------------------------------------------
// CWnd_Cfg_DAQ
//-----------------------------------------------------------------------------
class CWnd_Cfg_DAQ : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_DAQ)

public:
	CWnd_Cfg_DAQ();
	virtual ~CWnd_Cfg_DAQ();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);
	afx_msg void	OnBnClickedBnI2CLoad	();		// Button : I2C File Load
	afx_msg void	OnBnClickedBnI2CClear	();		// Button : I2C File Clear
	afx_msg void	OnBnClickedBnI2CLoad2();		// Button : I2C File Load
	afx_msg void	OnBnClickedBnI2CClear2();

	CFont			m_font_Data;

	CVGStatic		m_st_Item[STI_DAQ_MAXNUM];
	CComboBox		m_cb_Item[CBO_DAQ_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[EDT_DAQ_MAXNUM];

	CVGStatic		m_st_I2C_File[FILESET_DAQ_MAXNUM];
	CVGStatic		m_st_I2C_File_Data[FILESET_DAQ_MAXNUM];
	CMFCButton		m_bn_I2C_File_Load[FILESET_DAQ_MAXNUM];
	CMFCButton		m_bn_I2C_File_Clear[FILESET_DAQ_MAXNUM];
	CString			m_szI2CFilePath[FILESET_DAQ_MAXNUM];
	enInsptrSysType m_InspectionType;

public:

	void		SetSystemType(__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};


	void		InitUI();

	void		GetUIData				(__out ST_LVDSInfo& stLVDSInfo);
	void		SetUIData				(__in const ST_LVDSInfo* pstLVDSInfo);

	void		Enable_Setting			(__in BOOL bEnable);

	void		SetUIData_ByModel		(__in enModelType nModelType, __in ST_LVDSInfo* pstLVDSInfo);
	void		SetUIData_ByModel		(__in enModelType nModelType);
	
};

#endif // Wnd_Cfg_DAQ_h__


