//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_EachTest.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestItem_EachTest.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestItem_EachTest.h"

// CWnd_Cfg_TestItem_EachTest
#define IDC_LC_EACHSTEPLIST		1000
#define	IDC_BN_START			1002


IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem_EachTest, CWnd_BaseView)

CWnd_Cfg_TestItem_EachTest::CWnd_Cfg_TestItem_EachTest()
{
	m_InspectionType = enInsptrSysType::Sys_Focusing;
}

CWnd_Cfg_TestItem_EachTest::~CWnd_Cfg_TestItem_EachTest()
{
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem_EachTest, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BN_START, OnBnClickedStart)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem_EachTest::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	UINT nID_Index = 0;
	m_st_ManualTest.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_ManualTest.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_ManualTest.SetFont_Gdip(L"Arial", 15.0f);
	m_st_ManualTest.Create(_T("Manual Test"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_List_EachTestStep.Create(WS_CHILD | WS_VISIBLE, rectDummy, this, IDC_LC_EACHSTEPLIST);
	
	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	m_st_Result.Create(_T("Result"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_bn_Start.Create(_T("Each Run"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_START);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_EachTest::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 0;
	int iLeft	= iMargin;
	int iTop	= iMargin;
	int iWidth	= cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	iMargin = 5;

	int iListH = iHeight / 12 *9;
	int stH = (iHeight - iListH - (iMargin * 4)) / 5;

	iListH += (stH * 2) + (iMargin * 2);
	m_st_ManualTest.MoveWindow(iLeft, iTop, iWidth, stH);
	
	iTop += stH + iMargin;
	m_List_EachTestStep.MoveWindow(iLeft, iTop, iWidth, iListH);
	
	iTop += iListH + iMargin;
	m_st_Result.MoveWindow(iLeft, iTop, iWidth, stH);
	
	iTop += stH + iMargin;
	m_bn_Start.MoveWindow(iLeft, iTop, iWidth, stH);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_EachTest::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Option.SetActiveTab(0);
	}
}

//=============================================================================
// Method		: OnNMClickTestItem
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/9/28 - 18:36
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_EachTest::OnNMClickTestItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: SetSysAddTabCreate
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_EachTest::SetSysAddTabCreate()
{
	UINT nItemCnt = 0;
}

// CWnd_Cfg_TestItem_EachTest message handlers
//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_EachTest::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_List_EachTestStep.SetSystemType(nSysType);
}

//=============================================================================
// Method		: Set_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_EachTest::Set_RecipeInfo(__in ST_RecipeInfo* pstRecipeInfo)
{
	m_List_EachTestStep.Set_StepInfo(&pstRecipeInfo->StepInfo);
}

//=============================================================================
// Method		: OnBnClickedStart
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/25 - 11:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_EachTest::OnBnClickedStart()
{
	if (NULL == m_pnCamParaIdx)
		return;

	int nStepIdx = m_List_EachTestStep.GetSelectItem();
	int nParaIdx = *m_pnCamParaIdx;

	m_bn_Start.EnableWindow(FALSE);

	AfxGetApp()->GetMainWnd()->SendMessage(WM_MANUAL_ONEITEM_TEST, nStepIdx, nParaIdx);

	Sleep(300);

	m_bn_Start.EnableWindow(TRUE);
}

