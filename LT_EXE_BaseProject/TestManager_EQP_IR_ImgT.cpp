//*****************************************************************************
// Filename	: 	TestManager_EQP_IR_ImgT.cpp
// Created	:	2018/1/27 - 13:48
// Modified	:	2018/1/27 - 13:48
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP_IR_ImgT.h"
#include "CRC16.h"
#include "ColorSpace.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

enum { RCCC, CRCC, CCRC, CCCR };


CTestManager_EQP_IR_ImgT::CTestManager_EQP_IR_ImgT()
{
	OnInitialize();
}


CTestManager_EQP_IR_ImgT::~CTestManager_EQP_IR_ImgT()
{
	TRACE(_T("<<< Start ~CTestManager_EQP_IR_ImgT >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP_IR_ImgT >>> \n"));
}

//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_IR_ImgT::OnLoadOption()
{
	BOOL bReturn = CTestManager_EQP::OnLoadOption();



	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	CTestManager_EQP::InitDevicez(hWndOwner);

	if (RC_OK != OnLightBrd_PowerOn(Slot_A, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].wStep))
	{

	}
	Sleep(200);

}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartOperation_LoadUnload(__in BOOL bLoad)
{
	// 이부분은 CTestManager_EQP껄로 처리 했을 때 무방한지 TEST
	// TEST 중간에 안전센서 감지 하였을때 모든 TEST 끝난 후에 정상적으로 TEST 종료 되는 지 확인해야함.

	LRESULT lReturn = RC_OK;
	lReturn = CTestManager_EQP::StartOperation_LoadUnload(bLoad);
// 
// 	LRESULT lReturn = RC_OK;
// 
// 	if (FALSE == m_bFlag_ReadyTest)
// 	{
// 		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
// 		OnLog_Err(_T("검사가 진행 가능한 상태가 아닙니다."));
// 		return RC_NotReadyTest;
// 	}
// 	//20180326 원복
//  	lReturn = OnCheck_SaftyJIG();
//  	if (RC_OK != lReturn)
//  	{
//  		if (bLoad == Product_Unload)
//  		{
//  			OnSet_TestProgress(TP_Unloading);
//  
//  			OnFinally_LoadUnload(FALSE);
//  			OnLightBrd_PowerOff();
//  
//  			m_stInspInfo.Set_ResultCode(RC_OK);
//  
//  		}
//  		//	OnFinally_LoadUnload(bLoad);
//  		return lReturn;
//  	}
// 
// 	if (TRUE == bLoad)
// 	{
// 		lReturn = MES_CheckBarcodeValidation();
// 		if (RC_OK != lReturn)
// 		{
// 			if (RC_NoBarcode == lReturn)
// 			{
// 				AfxMessageBox(_T("No Barcode"), MB_SYSTEMMODAL);
// 			}
// 			else if (RC_MES_Err_BarcodeValidation == lReturn)
// 			{
// 				AfxMessageBox(_T("MES : Barcode is not Validation"), MB_SYSTEMMODAL);
// 			}
// 
// 			return lReturn;
// 		}
// 	}
// 
// 	// 테스트 가능 여부 판단
// 	if (TRUE == bLoad)
// 	{
// 		if (IsTesting())
// 		{
// 			TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
// 			OnLog_Err(_T("Inspection is in progress."));
// 			return RC_AlreadyTesting;
// 		}
// 	}
// 
// 	// 쓰레드 핸들 리셋
// 	if (NULL != m_hThr_LoadUnload)
// 	{
// 		CloseHandle(m_hThr_LoadUnload);
// 		m_hThr_LoadUnload = NULL;
// 	}
// 
// 	stThreadParam* pParam = new stThreadParam;
// 	pParam->pOwner = this;
// 	pParam->nIndex = 0;
// 	pParam->nCondition = bLoad;
// 	pParam->nArg_2 = 0;
// 
// 	m_hThr_LoadUnload = HANDLE(_beginthreadex(NULL, 0, ThreadLoadUnload, pParam, 0, NULL));
// 
// 	::SetThreadPriority(m_hThr_LoadUnload, THREAD_PRIORITY_HIGHEST);
// 

	return lReturn;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Inspection(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
	lResult = CTestManager_EQP::StartOperation_Manual(nStepIdx, nParaIdx);

	return lResult;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lResult = RC_OK;
//	lResult = CTestManager_EQP::StartOperation_InspManual(nParaIdx);



	return lResult;
}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::StopProcess_LoadUnload()
{
	CTestManager_EQP::StopProcess_LoadUnload();

}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::StopProcess_Test_All()
{
	//CTestManager_EQP::StopProcess_Test_All();
	//for (UINT nItem = 0; nItem < TI_IR_ImgT_MaxEnum; nItem++)
	//{
	//	SetLoopItem
	//}

	m_bFlag_UserStop = TRUE;

	//m_mortion

	OnSet_TestResult_Unit(TR_Stop, Para_Left);
	OnSet_TestResult_Unit(TR_Stop, Para_Right);
}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::StopProcess_InspManual(nParaIdx);

}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_IR_ImgT::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	BOOL bResult = TRUE;
	bResult = CTestManager_EQP::AutomaticProcess_LoadUnload(bLoad);



	return bResult;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	//CTestManager_EQP::AutomaticProcess_Test_All(nParaIdx);
	TRACE(_T("=-=-= [%d] Start Test operation =-=-=\n"), nParaIdx + 1);
	OnLog(_T("=-=-= [%d] Start Test operation =-=-="), nParaIdx + 1);

	LRESULT lReturn = RC_OK;

	__try
	{
		// 초기화
	//	OnInitial_Test_All(nParaIdx);

		lReturn = OnStart_Test_All(nParaIdx);
	}
	__finally
	{
		//for (int t = 0; t < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; t++)
		{
			// 작업 종료 처리
			if (RC_OK == lReturn)
			{
				OnFinally_Test_All(TRUE, nParaIdx);
				TRACE(_T("=-=-= [%d] Complete the entire test =-=-=\n"), nParaIdx + 1);
				OnLog(_T("=-=-= [%d] Complete the entire test =-=-="), nParaIdx + 1);
			}
			else
			{
				m_stInspInfo.CamInfo[nParaIdx].ResultCode = lReturn;

				OnFinally_Test_All(FALSE, nParaIdx);
				TRACE(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-=\n"), nParaIdx + 1, lReturn);
				OnLog(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-="), nParaIdx + 1), lReturn;

// 
// 				// * 검사 결과 판정
// 				OnSet_TestResult_Unit(TR_Check, nParaIdx);
// 				OnUpdate_TestReport(nParaIdx);

			
			}
		}

		if (RC_OK != lReturn)
		{
			// * Unload Product
			lReturn = StartOperation_LoadUnload(Product_Unload);
			if (RC_OK != lReturn)
			{
				// 에러 상황
			}
		}
	}

}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::AutomaticProcess_Test_InspManual(nParaIdx);


}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnInitial_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnInitial_LoadUnload(bLoad);

	if (bLoad)
	{
		OnDOut_StartLamp(FALSE);
		OnDOut_StopLamp(TRUE);
	}
}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnFinally_LoadUnload(__in BOOL bLoad)
{
	CTestManager_EQP::OnFinally_LoadUnload(bLoad);

	if (FALSE == bLoad)
	{
		OnDOut_StartLamp(TRUE);
		OnDOut_StopLamp(FALSE);
	}
}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_LoadUnload(bLoad);



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
//	CTestManager_EQP::OnInitial_Test_All(nParaIdx);

	m_stInspInfo.nTestPara = nParaIdx;
	//m_bFlag_CameraChange = FALSE;

	OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	//OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	OnSet_TestProgress_Unit(TP_Testing, nParaIdx);

	// 검사 시작 시간 설정
	OnSet_BeginTestTime(nParaIdx);

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();
}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_All(lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnStart_Test_All(nParaIdx);

	// * 안전 기능 체크
	if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgeLoading)
	{
		if (RC_OK == m_stInspInfo.CamInfo[nParaIdx].ResultCode)
			lReturn = RC_LoadingFailed;
		else
			lReturn = m_stInspInfo.CamInfo[nParaIdx].ResultCode;

		// * 임시 : 안전하게 배출하기 위하여
		// KHO  이거 지워도 되는 건지? (A:지우면 언로드가 안될때 있음 )
		Sleep(3000);

		return lReturn;
	}
	//
	// * 검사 시작 위치로 제품 이동
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		lReturn = OnMotion_MoveToTestPos(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황

			// 알람
			OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nParaIdx]);
			//return lReturn;
		}
	}

	// * Step별로 검사 진행
	enTestResult nResult = TR_Pass;

	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{
		OnInitial_Test_All(nCamPara);
	
		if (m_stInspInfo.CamInfo[Para_Left].nJudgment != TR_Pass && nCamPara == Para_Right)
		{

// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stECurrentData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stOpticalCenterData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stRotateData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stDistortionData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stFovData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stDynamicData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stDefectPixelData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stSFRData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stSNR_LightData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stIntensityData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stShadingData.FailData();
// 			m_stInspInfo.CamInfo[nCamPara].stImageQ.stParticleData.FailData();
			m_stInspInfo.CamInfo[Para_Right].nJudgment = TR_Fail;
		}
		else
		{

			m_Test_ResultDataView.TestResultView_AllReset(nCamPara);
			OnSetCamerParaSelect(nCamPara);

			//for (UINT nTryCnt = 0; nTryCnt <= 500; nTryCnt++)
			for (UINT nTryCnt = 0; nTryCnt <= m_stInspInfo.RecipeInfo.nTestRetryCount; nTryCnt++)
			{
				// 검사기에 맞추어 검사 시작
				lReturn = StartTest_Equipment(nCamPara);

				// * 검사 결과 판정
				nResult = Judgment_Inspection(nCamPara);

				if (TRUE == m_bFlag_UserStop)
				{
					nResult = TR_Stop;
					break;
				}

// 				if ((TR_Pass == nResult))
// 				{
// 					break;
// 				}
			}
		}
		
		if (TRUE == m_bFlag_UserStop)
		{
			nResult = TR_Stop;
			//break;
		}

		OnSet_TestResult_Unit(nResult, nCamPara);



		// * 검사 결과 판정 정보 세팅 및 UI표시
		if ((m_stInspInfo.RecipeInfo.ModelType == Model_MRA2) && (nCamPara == Para_Left))
		{
			OnSet_TestProgress_Unit(TP_Fin, nCamPara);
		}
		else
		{
			OnUpdate_TestReport(nParaIdx);
		}

		switch (lReturn)
		{
		case TR_Check:
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Check;
			break;
		case TR_Stop:
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Stop;
			break;
		case TR_Fail:
		case TR_Ready:
		case TR_Testing:
		case TR_Empty:
		case TR_Skip:
		case TR_Rework:
		case TR_Timeout:
			m_stInspInfo.CamInfo[nCamPara].nJudgment = TR_Fail;
		default:
			break;
		}
	}

	Judgment_Inspection_All();


	// 전체 결과
	TotalTest_ResultDataSave(nParaIdx);


	// * MES 검사 결과 보고
	if (MES_Online == m_stInspInfo.MESOnlineMode)
	{
		lReturn = MES_SendInspectionData(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황
		}
	}
	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{

		OnSet_TestProgress_Unit(TP_Fin, nCamPara);
	}

	// * Unload Product
	lReturn = StartOperation_LoadUnload(Product_Unload);
	if (RC_OK != lReturn)
	{
		// 에러 상황
	}



	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnInitial_Test_InspManual(nTestItemID, nParaIdx);


}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	CTestManager_EQP::OnFinally_Test_InspManual(nTestItemID, lResult, nParaIdx);


}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnStart_Test_InspManual(nTestItemID, nParaIdx);


	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::Load_Product()
{
	LRESULT lReturn = RC_OK;

	//20180326 원복
	//lReturn = CTestManager_EQP::Load_Product();

	// * 안전 기능 체크 (Main Power, EMO, Area Sensor, Door Sensor)

	lReturn = OnDIn_CheckSafetyIO();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : Safety Error [%s]"), g_szResultCode[lReturn]);
		return lReturn;
	}

	// * JIG Cover 체크 ()
	
	lReturn = OnDIn_CheckJIGCoverStatus();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : JIG Cover Opened"));
		return lReturn;
	}
	

	// * 모터 제어	
	lReturn = OnMotion_LoadProduct();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : Error Loading Product"));
		return lReturn;
	}

	// * POGO PIN 컨택 확인,  POGO PIN 사용 횟수 증가
	Increase_ConsumCount();

	// * 통신 초기화, 전원 On, 광원 On

	// * 최종 정상이 아니면 CHECK 판정 후 배출한다.
	if (RC_OK == lReturn)
	{
		// * 검사 시작 (쓰레드)
		lReturn = StartOperation_Inspection(0);
		if (RC_OK != lReturn) // 에러 상황
		{
			OnAddAlarm_F(_T("===== Can't Start Inspection ====="));
			return lReturn;
		}
	}
	else	// 에러 상황
	{
		// * 검사 판정 NG or CHECK
		m_stInspInfo.CamInfo[Para_Left].nJudgeLoading = TR_Fail;
		m_stInspInfo.CamInfo[Para_Right].nJudgeLoading = TR_Fail;
		m_stInspInfo.CamInfo[Para_Left].ResultCode = lReturn;
		m_stInspInfo.CamInfo[Para_Right].ResultCode = lReturn;

		// * 검사 결과 판정 정보 세팅 및 UI표시
		OnSet_TestResult_Unit(TR_Check, Para_Left);
		OnSet_TestResult_Unit(TR_Check, Para_Right);
		OnUpdate_TestReport(Para_Left);
		OnUpdate_TestReport(Para_Right);
		Judgment_Inspection_All();

		AutomaticProcess_LoadUnload(Product_Unload);
		return lReturn;//RC_LoadingFailed
	}


	// * 검사 시작 (쓰레드)
//	lReturn = StartOperation_Inspection(0);

	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::Unload_Product();

	return lReturn;
}

//=============================================================================
// Method		: StartTest_Equipment
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = StartTest_ImageTest(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: StartTest_ImageTest
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/14 - 22:09
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartTest_ImageTest(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;

	m_stInspInfo.CamInfo[nParaIdx].szBarcode = m_stInspInfo.szBarcodeBuf;
	m_bFlag_UserStop = FALSE;

	BOOL bError = FALSE;

	// * 설정된 스텝 진행
	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
	{
		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStart();

		// UI 스텝 선택
		OnSet_TestStepSelect(nStepIdx, nParaIdx);

		// * 검사 시작
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
			{
				// MRA2 모델의 경우 언로딩 동작을 하지 안도록 한다, 전체 루틴에서만 동작 될 수 있도록 한다. KHO 확인 필요
				if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType && TI_IR_ImgT_Motion_Load == pStepInfo->StepList[nStepIdx].nTestItem && nParaIdx == Para_Left)
				{
					lReturn = RC_OK;
				}
				else
				{
					lReturn = StartTest_ImageTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaIdx);
				}
			}
		}

		// 스텝 진행 시간 체크
		dwElapTime = Check_TimeStepStop(dwElapTime);

		// 진행 시간 설정
		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);

		// 스텝 결과 UI에 표시
		OnSet_TestStepResult(nStepIdx, nParaIdx);

		// * 검사 프로그레스		
		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);

		// -- Option 처리
		if (m_stInspInfo.RecipeInfo.nTestMode == TestMode_NGSkip)
		{
			// * 검사 중단 여부 판단
			if (RC_OK != lReturn)
			{
				// 에러 상황
				switch (pStepInfo->StepList[nStepIdx].nTestItem)
				{
				case TI_IR_ImgT_CaptureImage:
				case TI_IR_ImgT_Motion_Load:
				case TI_IR_ImgT_Motion_Vision:
				case TI_IR_ImgT_Motion_Displace:
				case TI_IR_ImgT_Motion_Chart:
				case TI_IR_ImgT_Motion_Particle:
				case TI_IR_ImgT_Initialize_Test:
				case TI_IR_ImgT_Finalize_Test:
				case TI_IR_ImgT_Fn_Current:
				case TI_IR_ImgT_Fn_SFR:
				case TI_IR_ImgT_Fn_Ymean:
				case TI_IR_ImgT_Fn_LCB:
				case TI_IR_ImgT_Fn_BlackSpot:
				case TI_IR_ImgT_Fn_Distortion:
				case TI_IR_ImgT_Fn_DefectBlack:
				case TI_IR_ImgT_Fn_DefectWhite:
				case TI_IR_ImgT_Fn_OpticalCenter:
				case TI_IR_ImgT_Fn_Shading:
				case TI_IR_ImgT_Fn_Displace:
				case TI_IR_ImgT_Fn_Vision:
				case TI_IR_ImgT_Fn_IIC:
					nStepIdx = (INT)iStepCnt - 1;
					bError = TRUE;
					break;
				default:
					break;
				}
			}
		}

		// UI 갱신 딜레이
		Sleep(100);

		// 스텝 딜레이
		if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
		{
			Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
		}

		if (TRUE == m_bFlag_UserStop)
		{
			m_stInspInfo.CamInfo[nParaIdx].nJudgment = TR_Stop;
			bError = TRUE;
			break;
		}
	}

	// 검사 중단시 Finalize 수행
	if (bError)
	{
		if (Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
		{
			_TI_Cm_Finalize_Error(Para_Left);
			_TI_Cm_Finalize_Error(Para_Right);
		}
		else
		{
			_TI_Cm_Finalize_Error(Para_Left);
		}

		lReturn = TR_Check;

		// * 최종 판정 CHECK 처리
		OnSet_TestResult_Unit(TR_Check, Para_Left);

		
	}

	// ...
// 	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
// 	{
// 		if (TI_IR_ImgT_Fn_SFR == pStepInfo->StepList[nStepIdx].nTestItem)
// 		{
// 			TestResultView(nStepIdx, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
// 		}
// 	}

	return lReturn;
}


//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
	// 	if (TI_2D_Detect_CornerExt == nTestItemID)
	// 	{
	// 		LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
	// 	}
}


//=============================================================================
// Method		: StartTest_ImageTest_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 11:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartTest_ImageTest_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	
	// Overlay Draw
	OnSetOverlayInfo_IR_ImgT(nTestItemID);
	// 
	TestResultView_Reset(nTestItemID, nParaIdx);

	switch (nTestItemID)
	{
	case TI_IR_ImgT_CaptureImage:
		lReturn = _TI_Cm_CaptureImage(nStepIdx, nParaIdx, TRUE);
		break;
	case TI_IR_ImgT_Motion_Load:
		lReturn = _TI_IR_ImgT_MoveStage_Load(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Motion_Vision:
		lReturn = _TI_IR_ImgT_MoveStage_Vision(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Motion_Displace:
		lReturn = _TI_IR_ImgT_MoveStage_Displace(nStepIdx, nParaIdx);
		break;
// 	case TI_IR_ImgT_Motion_TiltModify:
// 		lReturn = _TI_IR_ImgT_MoveStage_TiltModify(nStepIdx, nParaIdx);
// 		break;
	case TI_IR_ImgT_Motion_Chart:
		lReturn = _TI_IR_ImgT_MoveStage_Chart(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Motion_Particle:
		lReturn = _TI_IR_ImgT_MoveStage_Particle(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Initialize_Test:
		lReturn = _TI_Cm_Initialize(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Finalize_Test:
		lReturn = _TI_Cm_Finalize(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_Current:
		lReturn = _TI_IR_ImgT_Pro_Current(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_IIC:
		lReturn = _TI_IR_ImgT_Pro_IIC(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_Vision:
		lReturn = _TI_IR_ImgT_Pro_Vision(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_Displace:
		lReturn = _TI_IR_ImgT_Pro_Displace(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_SFR:
		lReturn = _TI_IR_ImgT_Pro_SFR(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_Ymean:
		lReturn = _TI_IR_ImgT_Pro_Ymean(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_LCB:
		lReturn = _TI_IR_ImgT_Pro_LCB(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_BlackSpot:
		lReturn = _TI_IR_ImgT_Pro_BlackSpot(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_Distortion:
		lReturn = _TI_IR_ImgT_Pro_Distortion(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_DefectBlack:
		lReturn = _TI_IR_ImgT_Pro_DefectBlack(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_DefectWhite:
		lReturn = _TI_IR_ImgT_Pro_DefectWhite(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_Shading:
		lReturn = _TI_IR_ImgT_Pro_Shading(nStepIdx, nParaIdx);
		break;
	case TI_IR_ImgT_Fn_OpticalCenter:
		lReturn = _TI_IR_ImgT_Pro_OpticalCenter(nStepIdx, nParaIdx);
		break;
// 	case TI_IR_ImgT_Re_Current:
// 	case TI_IR_ImgT_Re_SFR:
// 	case TI_IR_ImgT_Re_Ymean:
// 	case TI_IR_ImgT_Re_DefectBlack:
// 	case TI_IR_ImgT_Re_DefectWhite:
// 	case TI_IR_ImgT_Re_Distortion:
// 	case TI_IR_ImgT_Re_Shading_F1:
// 	case TI_IR_ImgT_Re_Shading_F2:
// 		lReturn = _TI_IR_ImgT_JudgeResult(nStepIdx, nTestItemID, nParaIdx);
// 		break;
	default:
		break;
	}

	switch (nTestItemID)
	{
	case TI_IR_ImgT_Fn_Current:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_IIC:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	//case TI_IR_ImgT_Fn_:
	//	TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
	//	break;
	case TI_IR_ImgT_Fn_Displace:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_Vision:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_SFR:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_Ymean:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_LCB:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_BlackSpot:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_Shading:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_Distortion:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_DefectBlack:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_DefectWhite:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	case TI_IR_ImgT_Fn_OpticalCenter:
		TestResultView(nTestItemID, nParaIdx, &m_stInspInfo.CamInfo[nParaIdx].stIRImage);
		break;
	default:
		break;
	}

	//-이미지 저장
	if (m_stOption.Inspector.bSaveImage_RGB)
	{
		switch (nTestItemID)
		{
		case TI_IR_ImgT_Fn_SFR:
		case TI_IR_ImgT_Fn_Ymean:
		case TI_IR_ImgT_Fn_Distortion:
		case TI_IR_ImgT_Fn_DefectBlack:
		case TI_IR_ImgT_Fn_DefectWhite:
// 			m_bPicCaptureMode = TRUE;
// 			for (int t = 0; t < 100; t++)
// 			{
// 				Sleep(50);
// 				if (m_bPicCaptureMode == FALSE)
// 				{
// 					break;
// 				}
// 			}
			break;
		default:
			break;
		}
	}

	switch (nTestItemID)
	{
	case TI_IR_ImgT_Fn_IIC:
	case TI_IR_ImgT_Fn_Vision:
	case TI_IR_ImgT_Fn_Displace:
	case TI_IR_ImgT_Fn_Current:
	case TI_IR_ImgT_Fn_SFR:
	case TI_IR_ImgT_Fn_Ymean:
	case TI_IR_ImgT_Fn_LCB:
	case TI_IR_ImgT_Fn_BlackSpot:
	case TI_IR_ImgT_Fn_Distortion:
	case TI_IR_ImgT_Fn_Shading:
	case TI_IR_ImgT_Fn_DefectBlack:
	case TI_IR_ImgT_Fn_DefectWhite:
	case TI_IR_ImgT_Fn_OpticalCenter:
		EachTest_ResultDataSave(nTestItemID, nParaIdx);
		break;

	default:
		break;
	}
	return lReturn;
}

//=============================================================================
// Method		: StartTest_Particle_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::StartTest_Particle_TestItem(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

// 	switch (nTestItemID)
// 	{
// 	default:
// 		break;
// 	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_08_AutoSocket_Back, IO_SignalT_SetOff);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_09_AutoSocket_Go, IO_SignalT_SetOff);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_08_AutoSocket_Back, IO_SignalT_SetOn);
	Sleep(500);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_10_AutoSocket_Dn, IO_SignalT_SetOn);
	Sleep(500);

	lReturn = CTestManager_EQP::_TI_Cm_Initialize(nStepIdx, nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Initialize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_nRegisterChangeMode = Reg_Chg_Normal;
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize(nStepIdx, nParaIdx);

	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Finalize_Test].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_10_AutoSocket_Dn, IO_SignalT_SetOff);
	Sleep(500);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_08_AutoSocket_Back, IO_SignalT_SetOff);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_09_AutoSocket_Go, IO_SignalT_SetOff);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_09_AutoSocket_Go, IO_SignalT_SetOn);
	Sleep(500);

	return lReturn;
}


//=============================================================================
// Method		: _TI_Cm_Finalize_Error
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/10 - 10:50
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_Finalize_Error(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_Finalize_Error(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurVoltage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_MeasurVoltage(nStepIdx, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CaptureImage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/2/23 - 16:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	int iWidth = 0;
	int iHeight = 0;
	DWORD dwRawSize = 0;

// 	ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nParaIdx);
// 
// 	iWidth	= pstVideo->m_dwWidth;
// 	iHeight = pstVideo->m_dwHeight;
// 
// 	// raw 데이터를 가져왔다.
// 	DWORD dwRawSize = 0;
// 	LPBYTE lpbyRawImage = m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx, dwRawSize);
// 	LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nParaIdx);
// 	LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx);

	LPBYTE lpbyRGBImage = (LPBYTE)m_Device.MIUCtrl.GetDisplayImage()->imageData;
	LPBYTE lpbyRawImage = (LPBYTE)m_Device.MIUCtrl.GetRaw12Image()->imageData;
	LPWORD lpwImage = (LPWORD)m_Device.MIUCtrl.GetRaw16Image()->imageData;

	iWidth = m_Device.MIUCtrl.GetDisplayImage()->width;
	iHeight = m_Device.MIUCtrl.GetDisplayImage()->height;
	dwRawSize = m_Device.MIUCtrl.GetRaw12Image()->imageSize;

	m_stRawBuf[nParaIdx].AssignMem(dwRawSize);

	// 이미지 버퍼에 복사
	memcpy(m_stRawBuf[nParaIdx].lpbyImage_Raw,		lpbyRawImage,	dwRawSize						);
	memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit,	lpbyRGBImage,	iWidth * iHeight * 3			);
	memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit,	lpwImage,		iWidth * iHeight * sizeof(WORD)	);
	
	// 이미지 저장
	if (bSaveImage)
	{
		CString szPath;
		CString szImgFileName;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);

		CString strDate;
		strDate.Format(_T("_%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

		szImgFileName = (_T("Capture") + strDate);

		CString strPath;
		//-바코드
		if (FALSE == m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
		{
			strPath.Format(_T("%s/%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[0].szBarcode);
		}
		else{
			strPath.Format(_T("%s/No Barcode"), m_stInspInfo.Path.szImage);
		}
		MakeDirectory(strPath);

		if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
			szPath.Format(_T("%s/Capatue_%s_%s"), strPath, g_szParaName[nParaIdx], szImgFileName);
		else
			szPath.Format(_T("%s/Capatue_%s"), strPath, szImgFileName);

		CString szTemp;

		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			szTemp = szPath + _T(".raw");
			OnSaveImage_Raw(szTemp.GetBuffer(0), m_stRawBuf[nParaIdx].lpbyImage_Raw, m_stRawBuf[nParaIdx].dwSize);
		}

		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			szTemp = szPath + _T("12bit.png");
			OnSaveImage_Gray16(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, iWidth, iHeight);

			szTemp = szPath + _T("8bit.png");
			OnSaveImage_Gray8(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, iWidth, iHeight);
		}

		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			szTemp = szPath + _T(".bmp");
			OnSaveImage_RGB(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpbyImage_8bit, iWidth, iHeight);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraRegisterSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nRegisterType
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraRegisterSet(nStepIdx, nRegisterType, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraAlphaSet
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nAlpha
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraAlphaSet(nStepIdx, nAlpha, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraPowerOnOff
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in enPowerOnOff nOnOff
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::_TI_Cm_CameraPowerOnOff(nStepIdx, nOnOff, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Get_ImageBuffer_CaptureImage
// Access		: protected  
// Returns		: BOOL
// Parameter	: enImageMode eImageMode
// Parameter	: __in UINT nParaIdx
// Parameter	: __out int & iImgW
// Parameter	: __out int & iImgH
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/3/1 - 14:16
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_IR_ImgT::_TI_Get_ImageBuffer_CaptureImage(enImageMode eImageMode, __in UINT nParaIdx, __out int &iImgW, __out int &iImgH, __out CString &strTestFileName, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	int iWidth = 0;
	int iHeight = 0;
	int iHalfImageW = iWidth / 2;
	int iHalfImageH = iHeight / 2;

	DWORD dwRawSize = 0;

	// 이미지 캡쳐 (Request Capture)
	if (ImageMode_LiveCam == eImageMode)
	{
		BOOL bSignal = FALSE;
		for (int i = 0; i < 30; i++)
		{
			if (m_Device.MIUCtrl.GetVideoSignalStatus())
			{
				bSignal = TRUE;
				break;
			}

			Sleep(33);
		}
		
		if (bSignal == FALSE)
		{
			return FALSE;
		}

		LPBYTE lpbyRGBImage = (LPBYTE)m_Device.MIUCtrl.GetDisplayImage()->imageData;
		LPBYTE lpbyRawImage = (LPBYTE)m_Device.MIUCtrl.GetRaw12Image()->imageData;
		LPWORD lpwImage = (LPWORD)m_Device.MIUCtrl.GetRaw16Image()->imageData;

		iWidth = m_Device.MIUCtrl.GetDisplayImage()->width;
		iHeight = m_Device.MIUCtrl.GetDisplayImage()->height;
		dwRawSize = m_Device.MIUCtrl.GetRaw12Image()->imageSize;

		m_stRawBuf[nParaIdx].AssignMem(dwRawSize);

// 		// 쿼터 이미지 버퍼
// 		IplImage *image12bitRaw = cvCreateImage(cvSize(iWidth, iHeight), IPL_DEPTH_8U, 2);
// 		IplImage *image12bitRaw_Quarter = cvCreateImage(cvSize(iWidth / 2, iHeight / 2), IPL_DEPTH_8U, 2);
// 		IplImage *image16bitRaw_Quarter = cvCreateImage(cvSize(iWidth / 2, iHeight / 2), IPL_DEPTH_8U, 2);
// 
// 		//- 기존 원본사이즈의 12bitRaw를 Copy한다.
// 		memcpy(image12bitRaw->imageData, lpbyRawImage, iWidth*iHeight * 2);
// 
// 		// 12bit raw을 Quard로 바꾸면서 16bit 변환 , 마지막 매개 인자를 바꾸면 odd,even이 바뀐다.
// 		m_tm_Test.Shift12to16Bit_QuarterMode((unsigned char*)image12bitRaw->imageData, (unsigned char*)image16bitRaw_Quarter->imageData, iWidth, iHeight, CCCR);
// 		// 16bit을 12bit 변환
// 		m_tm_Test.Shift16to12BitMode((unsigned char*)image16bitRaw_Quarter->imageData, (unsigned char*)image12bitRaw_Quarter->imageData, iHalfImageW, iHalfImageH);
// 
// 		// raw가 잘 변환 되었는지 보기 위한 용도로 아래 함수를 사용.
// 		IplImage *imageQuarter_8bitDebug = cvCreateImage(cvSize(iWidth / 2, iHeight / 2), IPL_DEPTH_8U, 1);
// 		m_tm_Test.Shift12BitMode((unsigned char*)image12bitRaw_Quarter->imageData, (unsigned char*)imageQuarter_8bitDebug->imageData, iHalfImageW, iHalfImageH);

		{
		memcpy(m_stRawBuf[nParaIdx].lpbyImage_Raw, lpbyRawImage, dwRawSize);
		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, iWidth * iHeight * 3);
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, iWidth * iHeight * sizeof(WORD));
	}
		
	}
	else
	{
		if (m_stImageMode.szImagePath.IsEmpty())
		{
			iImgW = 0;
			iImgH = 0;
			return FALSE;
		}

		IplImage *LoadImage = cvLoadImage((CStringA)m_stImageMode.szImagePath,-1);

		iWidth = LoadImage->width;
		iHeight = LoadImage->height;

		if (iWidth <1 || iHeight < 1)
		{
			iImgW = 0;
			iImgH = 0;
			return FALSE;
		}
		m_stImageBuf[nParaIdx].AssignMem(iWidth, iHeight);
		memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, LoadImage->imageData, LoadImage->width*LoadImage->height * 2);

		IplImage *LoadImage_8bit = cvCreateImage(cvSize(iWidth, iHeight),IPL_DEPTH_8U, 3);


		cv::Mat Bit16Mat(LoadImage->height, LoadImage->width, CV_16UC1, LoadImage->imageData);
		Bit16Mat.convertTo(Bit16Mat, CV_8UC1, 0.2, 0);

		cv::Mat rgb8BitMat(LoadImage_8bit->height, LoadImage_8bit->width, CV_8UC3, LoadImage_8bit->imageData);
		cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);


		memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, LoadImage_8bit->imageData, LoadImage->width*LoadImage->height * 3);

		cvReleaseImage(&LoadImage);
		cvReleaseImage(&LoadImage_8bit);
	}

	// 이미지 저장
	if (bSaveImage)
	{
		CString szPath;
		CString szImgFileName;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);
		if (szFileName)
		{
			CString strDate;
			strDate.Format(_T("_%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

			szImgFileName = (szFileName + strDate);
		}
		else
		{
			szImgFileName.Format(_T("%02d%02d_%02d%02d%02d"), tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);

		}


		CString strPath;
		//-바코드
		if (FALSE==m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
		{
			strPath.Format(_T("%s%s\\"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[0].szBarcode);
		}
		else{
			strPath.Format(_T("%sNo Barcode\\"), m_stInspInfo.Path.szImage);
		}
		MakeDirectory(strPath);

		if (1 <  g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
			szPath.Format(_T("%sCapatue_%s_%s"), strPath, g_szParaName[nParaIdx], szImgFileName);
		else
			szPath.Format(_T("%sCapatue_%s"), strPath, szImgFileName);

		strTestFileName = szPath;

		CString szTemp;

		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			szTemp = szPath + _T(".raw");
			OnSaveImage_Raw(szTemp.GetBuffer(0), m_stRawBuf[nParaIdx].lpbyImage_Raw, m_stRawBuf[nParaIdx].dwSize);
		}

		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			szTemp = szPath + _T("_12bit.png");
			OnSaveImage_Gray16(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, iWidth, iHeight);

			szTemp = szPath + _T("_8bit.png");
			OnSaveImage_Gray8(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, iWidth, iHeight);
		}

		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			szTemp = szPath + _T(".bmp");
			OnSaveImage_RGB(szTemp.GetBuffer(0), m_stImageBuf[nParaIdx].lpbyImage_8bit, iWidth, iHeight);
		}
	}

	iImgW = iWidth;
	iImgH = iHeight;

	m_szImageFileName = strTestFileName;
	return TRUE;

}

//=============================================================================
// Method		: _TI_IR_ImgT_MoveStage_Load
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/7 - 13:46
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_MoveStage_Load(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_tm_Motion.OnActionLoad();

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Motion_Load].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_MoveStage_Chart
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 9:34
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_MoveStage_Chart(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

// 	if (RC_OK != OnLightBrd_PowerOn(Slot_A, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].wStep))
// 	{
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		return RC_Light_Brd_Err_Ack;
// 	}
// 	Sleep(200);

	lReturn = m_tm_Motion.OnActionStandby_IR_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	lReturn = OnMotion_ChartTestProduct(nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Motion_Chart].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Chart [Para -> %s]"), g_szParaName[nParaIdx]);

	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_MoveStage_Chart
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 9:34
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_MoveChart_Index(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	bflagChartest = FALSE;
	// 1. Y축을 Loading 위초로 이동
	if (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY)
	{
		// TEST 중이니 Shutter는 동작 하지 않는다.
		lReturn = m_tm_Motion.OnActionLoad_IR_IgmT(FALSE);
	}

	if (RC_OK != lReturn)
	{
		OnAddAlarm_F(_T("Motion : Move to Chart Index, Y Axis Init Err"));
	}

	// 2. 차트 이동
	if (RC_OK == lReturn)
	{
		if (TRUE == pStepInfo->StepList[nStepIdx].bUseChart_Rot)
		{
			lReturn = m_tm_Motion.OnActionTesting_IR_ImgT_Index(pStepInfo->StepList[nStepIdx].nChart_Idx);
		}

		if (RC_OK != lReturn)
		{
			OnAddAlarm_F(_T("Motion : Move to Chart [Index -> %s]"), pStepInfo->StepList[nStepIdx].nChart_Idx);
		}
	}

	// 3. Y축 요청 위치로 이동
	if (RC_OK == lReturn)
	{
		if (TRUE == pStepInfo->StepList[nStepIdx].bUseMoveY)
		{
			lReturn = m_tm_Motion.OnActionTesting_IR_ImgT_Y(pStepInfo->StepList[nStepIdx].nMoveY, m_stOption.Inspector.bUseBlindShutter);
		}

		if (RC_OK != lReturn)
		{
			OnAddAlarm_F(_T("Motion : Move to Chart Index, Y Axis [Move : %d mm] Err"), pStepInfo->StepList[nStepIdx].nMoveY);
		}
	}
	if (pStepInfo->StepList[nStepIdx].bChart_Check)
	{
		bflagChartest = TRUE;
		int iImageW = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Width;
		int iImageH = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Height;

		// 이미지 가져오기
		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("Chart")))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			lReturn = RC_NoImage;
		}

		if (RC_NoImage != lReturn)
		{
			m_tm_Test.LGIT_Func_Chart(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stChart, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stChart);
		}
	}
	else
	{
		
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stChart.bDetect = TRUE;
	}
	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Motion_Chart].vt);

	// 차트 매칭 확인 
	if (TRUE == m_stInspInfo.CamInfo[nParaIdx].stIRImage.stChart.bDetect && RC_OK == lReturn)
	{
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stChart.bResult = TRUE;

		// * 검사 항목별 결과
		varResult.SetString(_T("OK"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stChart.bResult = FALSE;

		// * 에러 상황
		varResult.SetString(_T("NG"), VT_BSTR);
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_Vision(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	LPBYTE lpRecvImg = NULL;
	DWORD dwWidth = 0;
	DWORD dwHeight = 0;
	DWORD dwSize = 0;

	if (!m_Device.Vision.GetAcquiredImage(lpRecvImg, dwWidth, dwHeight))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
	}

	CString szMasterPath = m_stInspInfo.Path.szVisionImage + _T("\\") + m_stInspInfo.RecipeInfo.stIR_ImageQ.stVision.szPatternName + _T(".bmp");
	lReturn = m_tm_Test.Luri_Func_Vision(lpRecvImg, dwWidth, dwHeight, szMasterPath, m_stInspInfo.RecipeInfo.stIR_ImageQ.stVision, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stVision, NULL);

	//// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_Vision].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_Displace(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// 틸트 보정하기
// 	CString szAATiltx;
// 	CString szAATilty;
// 	
// 	double dAATiltx = 0;
// 	double dAATilty = 0;
// 
// 	BOOL bFind = TRUE;
// 
// 	bFind = m_tm_Test.GetTextFileData_FindBarcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, 6, szAATiltx);
// 	bFind &= m_tm_Test.GetTextFileData_FindBarcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, 5, szAATilty);
// 
// 	dAATiltx = _ttof(szAATiltx);
// 	dAATilty = _ttof(szAATilty);
// 
// 	// 틸트 계산
//  	ST_TeachInfo stTeachInfo = m_stInspInfo.MaintenanceInfo.stTeachInfo;
// 
// 	double dPoint_W = abs(stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_X + (0 * AX_IR_Image_MAX)] - stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_X + (1 * AX_IR_Image_MAX)]);
// 	double dPoint_H = abs(stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_Y + (0 * AX_IR_Image_MAX)] - stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_Y + (2 * AX_IR_Image_MAX)]);
// 	dPoint_W *= 0.001;
// 	dPoint_H *= 0.001;
// 
// 	double dPoint_x[4] = { 0, dPoint_W, dPoint_W, 0			};
// 	double dPoint_y[4] = { 0, 0,		dPoint_H, dPoint_H	};
// 	double dHeight[4] = { 0, };
// 
// 	for (int i = 0; i < 4; i++)
// 		dHeight[i] = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[i] * 100;
// 
// 	double dCurrTiltx = 0;
// 	double dCurrTilty = 0;
// 	m_tm_Test.GetTiltXY_4PointInput(dPoint_x, dPoint_y, dHeight, dCurrTiltx, dCurrTilty);
// 
// 	double dModifyTiltx = 0;
// 	double dModifyTilty = 0;
// 
// 	if (bFind)
// 	{
// 		dModifyTiltx = dAATiltx - dCurrTiltx;
// 		dModifyTilty = dAATilty - dCurrTilty;
// 
// 		// step1
// 		//dAATiltx = -0.348;
// 		//dAATilty = -0.16;
// 		//dModifyTiltx = dCurrTiltx - dAATiltx;
// 		//dModifyTilty = dCurrTilty - dAATilty;
// 
// 		// step2
// 		//dModifyTiltx = dCurrTiltx + dAATiltx;
// 		//dModifyTilty = dCurrTilty + dAATilty;
// 
// 		// step3
// 		//dModifyTiltx = dCurrTiltx + dAATiltx;
// 		//dModifyTilty = dCurrTilty + dAATilty;
// 	}
// 	else
// 	{
// 		dModifyTiltx = dCurrTiltx;
// 		dModifyTilty = dCurrTilty;
// 
// // 		dAATiltx = -0.348;
// // 		dAATilty = -0.16;
// // 		dModifyTiltx = dCurrTiltx - dAATiltx;
// // 		dModifyTilty = dCurrTilty - dAATilty;
// 	}
// 	
// 	m_tm_Motion.OnActionTesting_IR_ImgT_TiltModify(dModifyTiltx, dModifyTilty, nParaIdx);
// 	m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[4] = dModifyTiltx;
// 	m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[5] = dModifyTilty;

	for (UINT nCH = 0; nCH < 2; nCH++)
	{
		// 모션에서 측정함.
		//m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[nCH] = dValue[nCH] * m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.dbOffset[nCH];

		// 판정
		BOOL	bMinUse = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMin[nCH].bEnable;
		BOOL	bMaxUse = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMax[nCH].bEnable;
		double  dbMinDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMin[nCH].dbValue;
		double  dbMaxDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMax[nCH].dbValue;

		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.bResult[nCH] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[nCH + 4]);
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_Displace].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_MoveStage_Particle
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 9:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_MoveStage_Particle(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_tm_Motion.OnActionStandby_IR_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	lReturn = OnMotion_ParticleTestProduct(nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Motion_Particle].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Particle Test Stage [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_MoveStage_Vision(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{ 
	LRESULT lReturn = RC_OK;

	lReturn = m_tm_Motion.OnActionStandby_IR_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	lReturn = OnMotion_VisionTestProduct(nParaIdx);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Motion_Vision].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Vision Test Stage [Para -> %s]"), g_szParaName[nParaIdx]);
	}


	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_MoveStage_Displace(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	OnMotion_DisplaceTestProduct(0, 0, 0, nParaIdx);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	m_bRecvTilt = FALSE;
	BOOL bPopup = FALSE;
	///// Loop /////

	for (int i = 0; i < 10; i++)
	{
		if (m_bFlag_UserStop == TRUE)
		{
			break;
		}

		//BOOL bFind = TRUE;
		//CString szAATiltx;
		//CString szAATilty;
		m_dRecvTiltX = 0;
		m_dRecvTiltY = 0;

		if (m_stInspInfo.CamInfo[nParaIdx].szBarcode.IsEmpty())
		{
			
		}
		else
		{
			//bFind = m_tm_Test.GetTextFileData_FindBarcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, 6, szAATiltx);
			//bFind &= m_tm_Test.GetTextFileData_FindBarcode(m_stInspInfo.CamInfo[nParaIdx].szBarcode, 5, szAATilty);

			if (!m_Device.FCM30.Send_GetData(m_stInspInfo.CamInfo[nParaIdx].szBarcode))
			{
				m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
				return lReturn = RC_DeviceComm_Err;
			}
		}
	


		double dDisplaceVal[4] = { 0, };
		for (int iSeq = 0; iSeq < 4; iSeq++)
		{
				lReturn = OnMotion_DisplaceTestProduct(1, i, iSeq, nParaIdx);
				Sleep(300);

				//Sleep(100);

			if (RC_OK == lReturn)
			{
				OnDisplace_Read_Value(nParaIdx, dDisplaceVal[iSeq]);
				m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[iSeq] = dDisplaceVal[iSeq];
			}
		}

		// 틸트 보정하기
		double dAATiltx = 0;
		double dAATilty = 0;

		dAATiltx = m_dRecvTiltX;
		dAATilty = m_dRecvTiltY;

		//////// 틸트 계산
		ST_TeachInfo stTeachInfo = m_stInspInfo.MaintenanceInfo.stTeachInfo;

		double dPoint_W = abs(stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_X + (0 * AX_IR_Image_MAX)] - stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_X + (1 * AX_IR_Image_MAX)]);
		double dPoint_H = abs(stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_Y + (0 * AX_IR_Image_MAX)] - stTeachInfo.dbTeachData[TC_IR_Imgt_DisplaceA_Y + (2 * AX_IR_Image_MAX)]);
		//dPoint_W *= 0.001;
		//dPoint_H *= 0.001;

		double dPoint_x[4] = { 0, dPoint_W, dPoint_W, 0 };
		double dPoint_y[4] = { 0, 0, dPoint_H, dPoint_H };
		double dHeight[4] = { 0, };

		for (int i = 0; i < 4; i++)
			dHeight[i] = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[i];

		double dCurrTiltx = 0;
		double dCurrTilty = 0;
		// 변위 4Point 로 Tx, Ty 계산.
		m_tm_Test.GetTiltXY_4PointInput(dPoint_x, dPoint_y, dHeight, dCurrTiltx, dCurrTilty);



		double dModifyTiltx = 0;
		double dModifyTilty = 0;

		if (m_bRecvTilt)
		{
			// 			dModifyTiltx = dCurrTiltx - dAATiltx;
			// 			dModifyTilty = dCurrTilty - dAATilty;
		}
		else
		{
			if (bPopup == FALSE)
			{
				bPopup = TRUE;
				if (AfxMessageBox(_T("틸트 정보가 존재하지 않습니다. 기본 평탄도로 진행하시겠습니까?"), MB_YESNO) == IDYES)
				{
					dAATiltx = 0;
					dAATilty = 0;
				}
				else
				{
					m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
					return lReturn = RC_NoBarcode;
				}
			} 
			else
			{
				dAATiltx = 0;
				dAATilty = 0;
			}
		
		}

		//dModifyTiltx = (-1 * dAATiltx) - dCurrTiltx;
		//dModifyTilty = (-1 * dAATilty) - dCurrTilty;

		// 보정할 Tx, Ty
		dModifyTiltx = (dAATiltx) - dCurrTiltx;
		dModifyTilty = (dAATilty) - dCurrTilty;

		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[4] = dModifyTiltx;
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[5] = dModifyTilty;

		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[6] = dAATiltx;
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[7] = dAATilty;

		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[8] = dCurrTiltx;
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[9] = dCurrTilty;

		for (UINT nCH = 0; nCH < 2; nCH++)
		{
			// 모션에서 측정함.
			//m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[nCH] = dValue[nCH] * m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.dbOffset[nCH];

			// 판정
			BOOL	bMinUse = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMin[nCH].bEnable;
			BOOL	bMaxUse = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMax[nCH].bEnable;
			double  dbMinDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMin[nCH].dbValue;
			double  dbMaxDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.stSpecMax[nCH].dbValue;

			m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.bResult[nCH] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[nCH + 4]);
		}

		if (m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.bResult[0] && m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.bResult[1])
			break;

// 		if (dModifyTiltx >= -0.01 && dModifyTiltx <= 0.01 && dModifyTilty >= -0.01 && dModifyTilty <= 0.01)
// 		{
// 			break;
// 		}
// 		else
		{
			// Tx, Ty 보정 하는 함수 (모션)
			m_tm_Motion.OnActionTesting_IR_ImgT_TiltModify(dModifyTiltx * 0.3, dModifyTilty * 0.9, nParaIdx);

			Sleep(500);
		}
	}
	



	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Motion_Displace].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Displace Test Stage [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_MoveStage_TiltModify(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//lReturn = m_tm_Motion.OnActionStandby_IR_ImgT(m_stOption.Inspector.bUseBlindShutter);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}

	double dDisplaceVal[4] = { 0, };
	for (int iSeq = 0; iSeq < 4; iSeq++)
	{
		lReturn = OnMotion_TiltModifyProduct(iSeq, nParaIdx);

		if (RC_OK == lReturn)
		{
			OnDisplace_Read_Value(nParaIdx, dDisplaceVal[iSeq]);
			m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[iSeq] = dDisplaceVal[iSeq] * m_stInspInfo.RecipeInfo.stIR_ImageQ.stDisplace.dbOffset[iSeq];
		}
	}

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Motion_Displace].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		OnAddAlarm_F(_T("Motion : Move to Displace Test Stage [Para -> %s]"), g_szParaName[nParaIdx]);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_Pro_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/9 - 9:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_Current(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	double fCurrent[5] = { 0.0, };

	lReturn = OnMIU_DynamicCurrent(nParaIdx, fCurrent);

	for (UINT nCH = 0; nCH < Current_Max; nCH++)
	{
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[nCH] = fCurrent[nCH] * m_stInspInfo.RecipeInfo.stIR_ImageQ.stCurrent.dbOffset[nCH];

		// 판정
		BOOL	bMinUse	 = m_stInspInfo.RecipeInfo.stIR_ImageQ.stCurrent.stSpecMin[nCH].bEnable;
		BOOL	bMaxUse	 = m_stInspInfo.RecipeInfo.stIR_ImageQ.stCurrent.stSpecMax[nCH].bEnable;
		double  dbMinDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stCurrent.stSpecMin[nCH].dbValue;
		double  dbMaxDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stCurrent.stSpecMax[nCH].dbValue;

		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stCurrent.bResult[nCH] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[nCH]);
	}

// 	// video off
// 	OnMIU_CaptureStop(nParaIdx);
// 
// 	// video on
// 	lReturn = OnMIU_SensorInit(nParaIdx);
// 
// 	if (RC_OK == lReturn)
// 	{
// 		// * Register Write (Alpha ??)
// 		lReturn = OnMIU_CaptureStart(nParaIdx);
// 	}


	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_Current].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_IIC(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// Capture Cnt = 1 이상이면 작업시작
	//-----------------------------------------
	int iImageW = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Width;
	int iImageH = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Height;


	m_Device.MIUCtrl.ChangeMono();
	Sleep(500);

	// iic 이미지로 변경
	if (m_stInspInfo.RecipeInfo.ModelType == Model_OV10642)
	{
		m_Device.MIUCtrl.RegisterWrite(1, 0x3129, 0x0080);
		Sleep(500);
	}
	else if(m_stInspInfo.RecipeInfo.ModelType == Model_AR0220)
	{
		Sleep(500);
	}

	//cvSaveImage("D:\\MonoPattern.bmp", m_Device.MIUCtrl.GetDisplayImage());

// 	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("IIC")))
// 	{
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		lReturn = RC_NoImage;
// 		return lReturn;
// 	}
	IplImage* pLoadMasterImage = NULL;
	if (m_stInspInfo.RecipeInfo.stIR_ImageQ.stIIC.szPatternName.IsEmpty() == FALSE)
	{
		CString szMasterPath = m_stInspInfo.Path.szI2cImage + _T("\\") + m_stInspInfo.RecipeInfo.stIR_ImageQ.stIIC.szPatternName + _T(".bmp");
		pLoadMasterImage = cvLoadImage((CStringA)szMasterPath);
	}
	

	lReturn = m_tm_Test.Luri_Func_IIC(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, 
		(LPBYTE)pLoadMasterImage->imageData, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stIIC);

	// * 측정값 입력 (카메라 초기화)
// 	COleVariant varResult;
// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_IIC].vt);
// 	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	// 영상 이미지로 변경
	if (m_stInspInfo.RecipeInfo.ModelType == Model_OV10642)
	{
		m_Device.MIUCtrl.RegisterWrite(1, 0x3129, 0x0000);
		Sleep(500);
	}
	else if (m_stInspInfo.RecipeInfo.ModelType == Model_AR0220)
	{
		Sleep(500);
	}


	m_Device.MIUCtrl.ChangeRaw();
	Sleep(500);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_IIC].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	if (pLoadMasterImage != NULL)
		cvReleaseImage(&pLoadMasterImage);

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_Pro_SFR
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_SFR(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

// 	if (RC_OK != OnLightBrd_PowerOn(Slot_A, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].wStep))
// 	{
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		return RC_Light_Brd_Err_Ack;
// 	}
// 	Sleep(500);
	//*** 추가 재시도 횟수
	UINT nRetryCnt = m_stInspInfo.RecipeInfo.StepInfo.StepList[nStepIdx].nRetryCnt;

	// Capture Cnt = 1 이상이면 작업시작
	//-----------------------------------------
	int iImageW = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Width;
	int iImageH = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Img_Height;

	double dAverageSFR[ROI_SFR_Max] = { 0 };
	UINT nTestCount = m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR.nAverageCount;

	if (nTestCount == 0)
	{
		nTestCount = 1;
	}
	for (int nTestNum = 0; nTestNum < nTestCount; nTestNum++)
	{

		if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("SFR")))
		{
			m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
			lReturn = RC_NoImage;
			return lReturn;
		}

		lReturn = m_tm_Test.LGIT_Func_SFR(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR);
		//lReturn = m_tm_Test.LGIT_Func_SFR(m_stImageBuf[nParaIdx].lpbyImage_8bit, (LPBYTE)m_stImageBuf[nParaIdx].lpwImage_16bit, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR);

		for (int nROINum = 0; nROINum < ROI_SFR_Max; nROINum++)
		{
			dAverageSFR[nROINum] += m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.dbValue[nROINum];
		}
	}


	for (int nROINum = 0; nROINum < ROI_SFR_Max; nROINum++)
	{
		BOOL	bMinUse = m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR.stInput[nROINum].stSpecMin.bEnable;
		BOOL	bMaxUse = m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR.stInput[nROINum].stSpecMax.bEnable;
		double dbMinDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR.stInput[nROINum].stSpecMin.dbValue;
		double dbMaxDev = m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR.stInput[nROINum].stSpecMax.dbValue;

		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.dbValue[nROINum] = dAverageSFR[nROINum] / nTestCount ;
		m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.bResult[nROINum] = m_tm_Test.Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.dbValue[nROINum]);

// 		if (m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.bResult[nROINum] == FALSE)
// 		{
// 			lReturn = RC_TestSkip;
// 		}

		if (m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.bResult[nROINum]==FALSE)
		{
			lReturn = RC_Err_TestFail;

		}
	
	}
	
// 	delete[]lpwRaw_Std;
// 	delete[]lpwRaw_Sum;

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_SFR].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


//=============================================================================
// Method		: _TI_IR_ImgT_Pro_Ymean
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/7 - 1:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_Ymean(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	int iImageW = 0;
	int iImageH = 0;
	
	// 추후 루틴 관련하여 고민할 것 
	//lReturn = OnCameraBrd_LEDOnOff(enPowerOnOff::Power_Off, nParaIdx);

	//if (RC_OK != lReturn)
	//{
	//	m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	//	return lReturn;
	//}

	if (RC_OK != OnLightBrd_PowerOn(Slot_C, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean].wStep))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return RC_Light_Brd_Err_Ack;
	}

	Sleep(500);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("Ymean")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_Ymean(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stYmean, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stYmean);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_Ymean].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_LCB(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	int iImageW = 0;
	int iImageH = 0;

	// 추후 루틴 관련하여 고민할 것 
	//lReturn = OnCameraBrd_LEDOnOff(enPowerOnOff::Power_Off, nParaIdx);

	//if (RC_OK != lReturn)
	//{
	//	m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	//	return lReturn;
	//}

	if (RC_OK != OnLightBrd_PowerOn(Slot_C, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean].wStep))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return RC_Light_Brd_Err_Ack;
	}

	Sleep(500);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("LCB")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_LCB(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stLCB, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stLCB);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_LCB].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_BlackSpot(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	int iImageW = 0;
	int iImageH = 0;

	// 추후 루틴 관련하여 고민할 것 
	//lReturn = OnCameraBrd_LEDOnOff(enPowerOnOff::Power_Off, nParaIdx);

	//if (RC_OK != lReturn)
	//{
	//	m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	//	return lReturn;
	//}

	if (RC_OK != OnLightBrd_PowerOn(Slot_C, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Ymean].wStep))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return RC_Light_Brd_Err_Ack;
	}

	Sleep(500);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("BlackSpot")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_BlackSpot(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stBlackSpot, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stBlackSpot);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_BlackSpot].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_Pro_Ymean
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/7 - 1:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_DefectBlack(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	int iImageW = 0;
	int iImageH = 0;

	// 추후 루틴 관련하여 고민할 것 
	/*lReturn = OnCameraBrd_LEDOnOff(enPowerOnOff::Power_Off, nParaIdx);

	if (RC_OK != lReturn)
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return lReturn;
	}*/

	if (RC_OK != OnLightBrd_PowerOn(Slot_C, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_PixelDefectB].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_PixelDefectB].wStep))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return RC_Light_Brd_Err_Ack;
	}

	Sleep(500);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("Defect_Black")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_DefectBlack(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stDefect_Black, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDefect_Black);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_DefectBlack].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}


//=============================================================================
// Method		: _TI_IR_ImgT_Pro_Ymean
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/7 - 1:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_DefectWhite(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	int iImageW = 0;
	int iImageH = 0;

	// 추후 루틴 관련하여 고민할 것 
	//lReturn = OnCameraBrd_LEDOnOff(enPowerOnOff::Power_Off, nParaIdx);

	//if (RC_OK != lReturn)
	//{
	//	m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	//	return lReturn;
	//}

	if (RC_OK != OnLightBrd_PowerOn(Slot_C, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_PixelDefectW].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_PixelDefectW].wStep))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		return RC_Light_Brd_Err_Ack;
	}

	Sleep(500);

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("Defect_White")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_DefectWhite(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stDefect_White, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDefect_White);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_DefectWhite].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_OpticalCenter(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	int iImageW = 0;
	int iImageH = 0;

	//!SH _180811: 임시
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("OpticalCenter")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.Luri_Func_OpticalCenter(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stOpticalCenter, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stOpticalCenter);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_OpticalCenter].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_Pro_Shading
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:38
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_Shading(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

 	if (RC_OK != OnLightBrd_PowerOn(Slot_C, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Shading].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_Shading].wStep))
 	{
 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
 		return RC_Light_Brd_Err_Ack;
 	}
 	Sleep(500);

 	int iImageW = 0;
 	int iImageH = 0;
 
 	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("Shading")))
 	{
 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
 		lReturn = RC_NoImage;
 		//return lReturn;
 	}

// 	IplImage* pLoadImage = cvLoadImage("C:\\project\\LKAS\\20181106\\Capatue_Stain_1017_232356.bmp");
// 	int iImageW = pLoadImage->width;
// 	int iImageH = pLoadImage->height;
// 
// 	for (int y = 0; y < pLoadImage->height; y++)
// 	{
// 		for (int x = 0; x < pLoadImage->width; x++)
// 		{
// 			m_stImageBuf[nParaIdx].lpbyImage_8bit[y * pLoadImage->width * 3 + x * 3 + 0] = pLoadImage->imageData[y * pLoadImage->widthStep + x * 3 + 0];
// 			m_stImageBuf[nParaIdx].lpbyImage_8bit[y * pLoadImage->width * 3 + x * 3 + 1] = pLoadImage->imageData[y * pLoadImage->widthStep + x * 3 + 1];
// 			m_stImageBuf[nParaIdx].lpbyImage_8bit[y * pLoadImage->width * 3 + x * 3 + 2] = pLoadImage->imageData[y * pLoadImage->widthStep + x * 3 + 2];
// 		}
// 	}
// 
// 	cvReleaseImage(&pLoadImage);

	lReturn = m_tm_Test.Luri_Func_Shading(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stShading, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stShading);

	// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_Shading].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_Pro_DynamicBW
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/11 - 15:06
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_DynamicBW(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

// 	int iImageW = 0;
// 	int iImageH = 0;
// 
// 	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("DynamicBW")))
// 	{
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		lReturn = RC_NoImage;
// 		//return lReturn;
// 	}
// 
// 	lReturn = m_tm_Test.LGIT_Func_DynamicBW(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stDynamicBW, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDynamicBW);
// 
// 	// * 측정값 입력 (카메라 초기화)
// 	COleVariant varResult;
// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_DynamicBW].vt);
// 	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
// 
// 	if (RC_OK == lReturn)
// 	{
// 		// * 검사 항목별 결과 
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
// 	}
// 	else
// 	{
// 		// * 에러 상황
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_ImgT_JudgeResult
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nTestItem
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/11 - 15:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_JudgeResult(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	CArray <COleVariant, COleVariant&> VarArray;
	COleVariant varResult;

	for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList.GetAt(nTestItem).nResultCount; nIdx++)
	{
		switch (nTestItem)
		{
// 		case TI_IR_ImgT_Re_Vision:
// 			//varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[nIdx];
// 			//VarArray.Add(varResult);
// 			break;
// 		case TI_IR_ImgT_Re_Displace:
// 			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[nIdx];
// 			VarArray.Add(varResult);
// 			break;
// 		case TI_IR_ImgT_Re_Current:
			//varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[nIdx];
			//VarArray.Add(varResult);
// 			break;
// 		case TI_IR_ImgT_Re_SFR:
// 			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.dbValue[nIdx];
// 			VarArray.Add(varResult);
// 			break;
// 		case TI_IR_ImgT_Re_Ymean:
// 			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stYmean.nDefectCount;
// 			VarArray.Add(varResult);
// 			break;
// 		case TI_IR_ImgT_Re_Distortion:
// 			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDistortion.bRotation;
// 			VarArray.Add(varResult);
// 			break;
// 		case TI_IR_ImgT_Re_DefectBlack:
// 			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDefect_Black.bDefect_BlackResult;
// 			VarArray.Add(varResult);
// 			break;
// 		case TI_IR_ImgT_Re_DefectWhite:
// 			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDefect_White.nDefect_WhiteCount;
// 			VarArray.Add(varResult);
// 			break;
// 
// 		case TI_IR_ImgT_Re_OpticalCenterX:
// 			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stOpticalCenter.iValue[0];
// 			VarArray.Add(varResult);
// 			break;
// 
// 		case TI_IR_ImgT_Re_OpticalCenterY:
// 			varResult.intVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stOpticalCenter.iValue[1];
// 			VarArray.Add(varResult);
// 			break;
// 
// 		case TI_IR_ImgT_Re_Shading_F1:
// 			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stShading.dValue[0];
// 			VarArray.Add(varResult);
// 			break;
// 
// 		case TI_IR_ImgT_Re_Shading_F2:
// 			varResult.dblVal = m_stInspInfo.CamInfo[nParaIdx].stIRImage.stShading.dValue[1];
// 			VarArray.Add(varResult);
// 			break;

		default:
			break;
		}
	}

	// 측정값 입력 및 판정
	m_stInspInfo.Set_Measurement(nParaIdx, nStepIdx, (UINT)VarArray.GetCount(), VarArray.GetData());

	return lReturn;
}

//=============================================================================
// Method		: Judgment_Inspection_All
// Access		: protected  
// Returns		: enTestResult
// Qualifier	:
// Last Update	: 2018/4/24 - 10:00
// Desc.		:
//=============================================================================
enTestResult CTestManager_EQP_IR_ImgT::Judgment_Inspection_All()
{
	enTestResult lReturn = TR_Pass;
	UINT nTestCount = 1;

	if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
	{
		if ((Sys_Focusing == m_InspectionType) && (Model_OMS_Front == m_stInspInfo.RecipeInfo.ModelType))
		{
			nTestCount = 1;
		}
		else
		{
			nTestCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; // 2
		}
	}

	for (UINT nIdx = 0; nIdx < nTestCount; nIdx++)
	{
		if (TR_Pass != m_stInspInfo.CamInfo[nIdx].nJudgment)
		{
			lReturn = m_stInspInfo.CamInfo[nIdx].nJudgment;
			break;
		}
	}

	// 검사 최종 판정
	OnSet_TestResult(lReturn);
	OnSet_TestResult_Total(lReturn);//이 결과가 최종으로 UI에 Display 됨.
	return lReturn;
}

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::MES_CheckBarcodeValidation(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		lReturn = MES_MakeInspecData_LKAS(nParaIdx);

		// MES 로그 남기기
		BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

		SYSTEMTIME tmLocal;
		GetLocalTime(&tmLocal);
		// MES 로그 남기기

		//m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

		// MES
		m_FileMES.SaveMESFile(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &tmLocal);

		// 로그
		m_FileTestLog.SaveLOGFile(m_stInspInfo.Path.szReport, m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &tmLocal);
	}

	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_LKAS
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/9/20 - 9:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::MES_MakeInspecData_LKAS(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
	DWORD dwElapTime = 0;
	int iCamCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt;

	// * 설정된 스텝 진행
	int	nSFRCount = 0;
	for (int camCnt = 0; camCnt < iCamCount; camCnt++)
	{
		m_stMesData_LKAS.Reset(camCnt);
		for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
		{
			switch (pStepInfo->StepList[nStepIdx].nTestItem)
			{
			case TI_IR_ImgT_Fn_Current:
				m_TestMgr_TestMes.SaveData_Current(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_IIC:
				m_TestMgr_TestMes.SaveData_IIC(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_SFR:
				m_TestMgr_TestMes.SaveData_SFR(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_Distortion:
				m_TestMgr_TestMes.SaveData_Distortion(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_DefectBlack:
				m_TestMgr_TestMes.SaveData_DefectBlock(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_DefectWhite:
				m_TestMgr_TestMes.SaveData_DefectWhite(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_Ymean:
				m_TestMgr_TestMes.SaveData_Ymean(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_LCB:
				m_TestMgr_TestMes.SaveData_LCB(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_BlackSpot:
				m_TestMgr_TestMes.SaveData_BlackSpot(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_Shading:
				m_TestMgr_TestMes.SaveData_Shading(camCnt, &m_stMesData_LKAS);
				break;
			case TI_IR_ImgT_Fn_OpticalCenter:
				m_TestMgr_TestMes.SaveData_OpticalCenter(camCnt, &m_stMesData_LKAS);
				break;
			default:
				break;
			}
		}
	}

	m_FileMES.Reset();
	m_FileTestLog.Reset();

	BOOL bJudge = (TR_Pass == m_stInspInfo.Judgment_All) ? TRUE : FALSE;

#ifdef USE_BARCODE_SCANNER
	//m_FileMES.Set_EquipmnetID(m_stInspInfo.CamInfo[0].szBarcode);
	m_FileMES.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
#else
	m_FileMES.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
#endif
	m_FileMES.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileMES.Set_Barcode(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

	m_FileTestLog.Set_EquipmnetID(m_stInspInfo.szEquipmentID);
	m_FileTestLog.Set_Path(m_stOption.MES.szPath_MESLog);
	m_FileTestLog.Set_Barcode(m_stInspInfo.CamInfo[0].szBarcode, m_stInspInfo.CamInfo[0].nMES_TryCnt, bJudge, &m_stInspInfo.CamInfo[0].TestTime.tmStart_Loading);

	CString		NAME;				// 항목 명칭	
	CString		VALUE;				// 데이터
	BOOL		JUDGE = TRUE;		// 판정

	int nHeaderCnt = 0;

	for (int camCnt = 0; camCnt < iCamCount; camCnt++)
	{
		nHeaderCnt = 0;
		for (int t = 0; t < MesDataIdx_LKAS_MAX; t++)
		{
			if (!m_stMesData_LKAS.szMesTestData[camCnt][t].IsEmpty())
			{
				CString strData;
				CString szBlockData;
				CString szValueData;
				CString szResultData;

				strData = m_stMesData_LKAS.szMesTestData[camCnt][t];

				strData.Remove(_T(' '));
				strData.Remove(_T('\t'));
				strData.Remove(_T('\r'));
				strData.Remove(_T('\n'));
				//- ,를 찾고
				int nNum = 0;
				int nStartNum = 0;
				int nDataNum = 0;

				int nDataCnt = 0;
				while (true)
				{
					nNum = strData.Find(',', nStartNum);
					if (nNum != -1)
					{
						szBlockData = strData.Mid(nStartNum, nNum);

						nDataNum = szBlockData.Find(':', 0);
						szValueData = szBlockData.Mid(0, nDataNum);
						szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());

						m_FileMES.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], szValueData, _ttoi(szResultData));
						m_FileTestLog.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], szValueData, _ttoi(szResultData));

						nHeaderCnt++;
						nStartNum = nNum + 1;
						nDataCnt++;
					}
					else{
						break;
					}
				}

				szBlockData = strData.Mid(nStartNum, strData.GetLength());
				nDataNum = szBlockData.Find(':', 0);
				szValueData = szBlockData.Mid(0, nDataNum);
				szResultData = szBlockData.Mid(nDataNum + 1, szBlockData.GetLength());
				m_FileMES.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], szValueData, _ttoi(szResultData));
				m_FileTestLog.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], szValueData, _ttoi(szResultData));

				nHeaderCnt++;
				nDataCnt++;

				for (int k = nDataCnt; k < m_stMesData_LKAS.nMesDataNum[t]; k++)
				{
					m_FileMES.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], _T(""), 1);
					m_FileTestLog.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], _T(""), 1);
					nHeaderCnt++;
				}
			}
			else
			{
				CString strHeader;
				for (int k = 0; k < m_stMesData_LKAS.nMesDataNum[t]; k++)
				{

					m_FileMES.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], _T(""), 1);

					strHeader.Format(_T("%s"), g_szMESHeader_LKAS[nHeaderCnt]);
					if (strHeader == _T("Reserved"))
					{
						m_FileTestLog.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], _T(""), 1);
					}
					else{
						m_FileTestLog.Add_ItemData(g_szMESHeader_LKAS[nHeaderCnt], _T("X"), 1);
					}
					nHeaderCnt++;
				}
			}
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	CTestManager_EQP::OnPopupMessageTest_All(nResultCode);
}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

//	lReturn = CTestManager_EQP::OnMotion_MoveToTestPos(nParaIdx);
	lReturn = m_tm_Motion.OnActionStandby_IR_ImgT(m_stOption.Inspector.bUseBlindShutter);


	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToStandbyPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToUnloadPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:40
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_MoveToUnloadPos(nParaIdx);



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_Origin();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_LoadProduct();



	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnMotion_UnloadProduct();

	OnMIU_CaptureStop(0);

	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_10_AutoSocket_Dn, IO_SignalT_SetOff);
	Sleep(500);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_08_AutoSocket_Back, IO_SignalT_SetOff);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_09_AutoSocket_Go, IO_SignalT_SetOff);
	m_Device.DigitalIOCtrl.Set_DO_Status(DI_IR_ImgT_09_AutoSocket_Go, IO_SignalT_SetOn);
	Sleep(500);

	return lReturn;
}


LRESULT CTestManager_EQP_IR_ImgT::OnMotion_VisionTestProduct(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_tm_Motion.OnActionTesting_IR_ImgT_Vision(nParaIdx);

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnMotion_DisplaceTestProduct(__in int iLoopmode, __in int iStep, __in UINT nSeq, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_tm_Motion.OnActionTesting_IR_ImgT_Displace(iLoopmode, iStep, nSeq, nParaIdx);

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnMotion_TiltModifyProduct(__in double dTiltx, __in double dTilty, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = m_tm_Motion.OnActionTesting_IR_ImgT_TiltModify(dTiltx, dTilty, nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_ChartTestProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_ChartTestProduct(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_tm_Motion.OnActionTesting_IR_ImgT_Chart(nParaIdx);
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnMotion_ParticleTestProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnMotion_ParticleTestProduct(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_tm_Motion.OnActionTesting_IR_ImgT_Blemish(nParaIdx);
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	//CTestManager_EQP::OnDIn_DetectSignal(byBitOffset, bOnOff);
	OnDIn_DetectSignal_IR_ImgT(byBitOffset, bOnOff);
}

//=============================================================================
// Method		: OnDIn_DetectSignal_SteCal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/3/3 - 20:41
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnDIn_DetectSignal_IR_ImgT(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	enDI_ImageTest nDI_Signal = (enDI_ImageTest)byBitOffset;

	switch (nDI_Signal)
	{
	case DI_IR_ImgT_00_MainPower:
		if (FALSE == bOnOff)
		{
			OnDIn_MainPower();
		}
		break;

	case DI_IR_ImgT_01_EMO:
		if (FALSE == bOnOff)
		{
			OnDIn_EMO();
		}
		break;

	case DI_IR_ImgT_02_Start:
		if (bOnOff)
		{
			StartOperation_LoadUnload(Product_Load);
		}
		break;

	case DI_IR_ImgT_03_Stop:
		if (bOnOff)
		{
			StopProcess_Test_All();
		}
		break;

	case DI_IR_ImgT_04_Init:
		break;

	case DI_IR_ImgT_05_Pause:
		break;
		
	default:
		break;
	}
}

//=============================================================================
// Method		: OnDIO_InitialSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/8 - 20:30
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnDIO_InitialSignal()
{
	CTestManager_EQP::OnDIO_InitialSignal();


}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_MainPower();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_EMO();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_AreaSensor();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_DoorSensor();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_OMS_Entry:
	case Model_OMS_Front:
	case Model_OMS_Front_Set:
	case Model_IKC:
		return RC_OK;
		break;

	case Model_MRA2:
		break;

	default:
		break;
	}

	// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
	// * 검사 진행 중 커버 열리면 오류 처리
	if (IsTesting())
	{
		if (!m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
		{
			lReturn = RC_DIO_Err_JIGCorverCheck;

			OnLog_Err(_T("I/O : JIG Cover is Opened !!"));
			OnAddAlarm_F(_T("I/O : JIG Cover is Opened !!"));

			StopProcess_Test_All();

			AfxMessageBox(_T("I/O : Opened JIG Cover!!"), MB_SYSTEMMODAL);
		}
	}




	lReturn = CTestManager_EQP::OnDIn_JIGCoverCheck();


	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Start();



	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDIn_Stop();



	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDIn_CheckSafetyIO()
{
	LRESULT lReturn = RC_OK;

	//	lReturn = CTestManager_EQP::OnDIn_CheckSafetyIO();

	if (FALSE == m_stInspInfo.byDIO_DI[DI_IR_ImgT_00_MainPower])
	{
		lReturn = RC_DIO_Err_MainPower;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_IR_ImgT_01_EMO])
	{
		lReturn = RC_DIO_Err_EMO;
	}

// 	if (FALSE == m_stInspInfo.byDIO_DI[DI_IR_ImgT_03_AreaSensor] && TRUE == m_stOption.Inspector.bUseAreaSensor_Err)
// 	{
// 		lReturn = RC_DIO_Err_AreaSensor;
// 	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_IR_ImgT_15_DoorOpen] && TRUE == m_stOption.Inspector.bUseDoorOpen_Err)
	{
		lReturn = RC_DIO_Err_DoorSensor;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckJIGCoverStatus
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/19 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDIn_CheckJIGCoverStatus();

	// IKC 커버 센서 없음
	if (Model_IKC == m_stInspInfo.RecipeInfo.ModelType)
		return lReturn;

	// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
// 	if (!m_stInspInfo.byDIO_DI[DI_IR_ImgT_02_CoverSensor])
// 	{
// 		lReturn = RC_DIO_Err_JIGCorverCheck;
// 	}


	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_BoardPower(bOn, nParaIdx);



	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_FluorescentLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StartLamp(bOn);



	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_StopLamp(bOn);



	return lReturn;
}

//=============================================================================
// Method		: OnDOut_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enLampColor nLampColor
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/31 - 11:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	//lReturn = CTestManager_EQP::OnDOut_TowerLamp(nLampColor, bOn);

	enum_IO_SignalType enSignalType;

	if (ON == bOn)
	{
		enSignalType = IO_SignalT_SetOn;
	}
	else
	{
		enSignalType = IO_SignalT_SetOff;
	}

	switch (nLampColor)
	{
	case Lamp_Red:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_IR_ImgT_12_TowerLampRed, enSignalType);
		break;
	case Lamp_Yellow:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_IR_ImgT_13_TowerLampYellow, enSignalType);
		break;
	case Lamp_Green:
		lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_IR_ImgT_14_TowerLampGreen, enSignalType);
		break;
	case Lamp_All:
		for (UINT nIdx = 0; nIdx < 3; nIdx++)
		{
			lReturn = m_Device.DigitalIOCtrl.Set_DO_Status(DO_IR_ImgT_12_TowerLampRed + nIdx, enSignalType);
		}
		break;
	default:
		break;
	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnDOut_Buzzer(bOn);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_stInspInfo.RecipeInfo.ModelType)
	{
	case Model_OMS_Entry:
		lReturn = OnDAQ_EEPROM_OMS_Entry_IR_ImgT(nIdxBrd);
		break;

	case Model_OMS_Front:
	case Model_OMS_Front_Set:
		lReturn = OnDAQ_EEPROM_OMS_Front_IR_ImgT(nIdxBrd);
		break;

	case Model_MRA2:
		lReturn = OnDAQ_EEPROM_MRA2_IR_ImgT(nIdxBrd);
		break;

	case Model_IKC:
		lReturn = OnDAQ_EEPROM_IKC_IR_ImgT(nIdxBrd);
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Entry_IR_ImgT
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/13 - 17:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnDAQ_EEPROM_OMS_Entry_IR_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDAQ_EEPROM_OMS_Front_IR_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDAQ_EEPROM_MRA2_IR_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::OnDAQ_EEPROM_IKC_IR_ImgT(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnManualSequence(nParaID, enFunc);



	return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();

	// * 검사 시작
	if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
	{
		if (OpMode_DryRun != m_stInspInfo.OperateMode)
		{
			StartTest_ImageTest_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nParaID);
		}
	}

	return lReturn;
}
//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnCheck_DeviceComm()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_DeviceComm();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_SaftyJIG();



	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	lReturn = CTestManager_EQP::OnCheck_RecipeInfo();



	return lReturn;
}

//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;


	//fileReport.SaveFinalizeResult2DCal(nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st2D_Result);


	
}

void CTestManager_EQP_IR_ImgT::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP_IR_ImgT::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
	// 		__super::CreateTimer_UpdateUI(5000, 2500);
}

void CTestManager_EQP_IR_ImgT::DeleteTimer_UpdateUI()
{
	// 		__super::DeleteTimer_UpdateUI();
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnMonitor_TimeCheck()
{
	CTestManager_EQP::OnMonitor_TimeCheck();

}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnMonitor_UpdateUI()
{
	CTestManager_EQP::OnMonitor_UpdateUI();

}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnSaveWorklist()
{
	// 	m_stInspInfo.WorklistInfo.Reset();
	// 
	// 	ST_CamInfo* pstCam = NULL;
	// 
	// 	CString szText;
	// 	CString szTime;
	// 	SYSTEMTIME lcTime;
	// 
	// 	GetLocalTime(&lcTime);
	// 	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
	// 		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);
	// 
	// 	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
	// 	{
	// 		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
	// 			continue;
	// 
	// 		pstCam = &m_stInspInfo.CamInfo[nChIdx];
	// 
	// 		m_stInspInfo.WorklistInfo.Time = szTime;
	// 		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
	// 		m_stInspInfo.WorklistInfo.SWVersion;
	// 		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
	// 		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
	// 		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;
	// 
	// 		szText.Format(_T("%d"), nChIdx + 1);
	// 		m_stInspInfo.WorklistInfo.Socket = szText;
	// 
	// 
	// 		m_stInspInfo.WorklistInfo.MakeItemz();
	// 
	// 		// 파일 저장
	// 		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);
	// 
	// 		// UI 표시
	// 		OnInsertWorklist();
	// 
	// 	}// End of for
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnInitialize()
{
	CTestManager_EQP::OnInitialize();

}

//============================================================================= 
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnFinalize()
{
	CTestManager_EQP::OnFinalize();

}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	CTestManager_EQP::SetPermissionMode(nAcessMode);


}


void CTestManager_EQP_IR_ImgT::TestResultView(UINT nTestID, UINT nPara, LPVOID pParam){
	
	m_Test_ResultDataView.TestResultView(nTestID, nPara, pParam);
}
void CTestManager_EQP_IR_ImgT::TestResultView_Reset(UINT nTestID, UINT nPara){

	m_Test_ResultDataView.TestResultView_Reset(nTestID, nPara);
}

// void CTestManager_EQP_IR_ImgT::Mes_EachTest_Save(UINT nTestID, UINT nPara){
//	m_Test_ResultDataView.TestResultView_Reset(nTestID, nPara);
//}

void CTestManager_EQP_IR_ImgT::EachTest_ResultDataSave(UINT nTestID, UINT nPara)
{
	ST_MES_TestItemLog m_MesData;
	CString str;
	SYSTEMTIME systime;

	GetLocalTime(&systime);

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&systime);

 	//- Model
	m_MesData.Model = m_stInspInfo.RecipeInfo.szRecipeFile;
 	//- SW Ver
	m_MesData.SWVersion = m_Worklist.GetSWVersion(g_szProgramName[m_InspectionType]);

	if (m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
	{
		m_MesData.Barcode =_T("No Barcode");
	}
	else
	{
		m_MesData.Barcode = m_stInspInfo.CamInfo[0].szBarcode;

	}
 
	m_MesData.Equipment = m_stInspInfo.szEquipmentID;

 	//- Channel
	str.Format(_T("%d"), nPara);
 	m_MesData.Socket = str;
 
 	//- Result + Data
 	UINT DataNum = 0;
 	CString Data[100];
 
	CString strResult;
	UINT nResult = 0;
 
	CString strTestName;
	switch (nTestID)
	{
	case TI_IR_ImgT_Fn_SFR:
		m_TestMgr_TestMes.SFR_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("SFR");
		break;

	case TI_IR_ImgT_Fn_Current:
		m_TestMgr_TestMes.Current_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Current");
		break;

	case TI_IR_ImgT_Fn_Shading:
		m_TestMgr_TestMes.Shading_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Shading");
		break;

	case TI_IR_ImgT_Fn_Ymean:
		m_TestMgr_TestMes.Ymean_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Ymean");
		break;

	case TI_IR_ImgT_Fn_LCB:
		m_TestMgr_TestMes.LCB_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("LCB");
		break;

	case TI_IR_ImgT_Fn_BlackSpot:
		m_TestMgr_TestMes.BlackSpot_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("BlackSpot");
		break;

	case TI_IR_ImgT_Fn_Vision:
		m_TestMgr_TestMes.Vision_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Vision");
		break;

	case TI_IR_ImgT_Fn_Displace:
		m_TestMgr_TestMes.Displace_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Displace");
		break;

	case TI_IR_ImgT_Fn_IIC:
		m_TestMgr_TestMes.IIC_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("IIC");
		break;

	case TI_IR_ImgT_Fn_Distortion:
		m_TestMgr_TestMes.Distortion_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Distortion");
		break;

	case TI_IR_ImgT_Fn_DefectBlack:
		m_TestMgr_TestMes.DefectBlack_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Defect_Black");
		break;

	case TI_IR_ImgT_Fn_DefectWhite:
		m_TestMgr_TestMes.DefectWhite_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Defect_White");
		break;

	case TI_IR_ImgT_Fn_OpticalCenter:
		m_TestMgr_TestMes.OpicalCenter_Each_DataSave(m_InspectionType, nPara, &m_MesData.ItemHeaderz, &m_MesData.Itemz, nResult);
		strTestName = _T("Optical_Center");
		break;



	default:
		break;
	}
	m_MesData.Result.Format(_T("%d"), nResult);

	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &systime, &m_MesData, strTestName);

 	//  	// 	/*결과*/
//  		m_MesData.Itemz.Add(Data[Distortion_W_FullArea + t]);
//  		m_MesData.ItemHeaderz.Add(g_lpszHeader_Distortion_Worklist[Distortion_W_FullArea + t]);
}

void CTestManager_EQP_IR_ImgT::TotalTest_ResultDataSave(UINT nPara)
{
	ST_MES_TestItemLog m_MesData;
	CString str;
	SYSTEMTIME systime;

	GetLocalTime(&systime);

	//- 시간
	m_MesData.Time = m_Worklist.Conv_SYSTEMTIME2String(&systime);

	//- Model
	m_MesData.Model = m_stInspInfo.RecipeInfo.szRecipeFile;
	//- SW Ver
	m_MesData.SWVersion = m_Worklist.GetSWVersion(g_szProgramName[m_InspectionType]);

	if (m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
	{
		m_MesData.Barcode = _T("No Barcode");
	}
	else
	{
		m_MesData.Barcode = m_stInspInfo.CamInfo[0].szBarcode;

	}

	m_MesData.Equipment = m_stInspInfo.szEquipmentID;

	//- Channel
	str.Format(_T("%d"), nPara);
	m_MesData.Socket = str;

	CString strResult;
	strResult.Format(_T("%d"), m_stInspInfo.CamInfo[nPara].nJudgment);

	m_MesData.Result = strResult;

	//- Result + Data
	UINT DataNum = 0;
	CString Data[100];

	UINT nResult = 0;

	CString strData;

	// Current
	for (int t = 0; t < MESDataNum_IR_ECurrent_IKC; t++)
	{
		m_MesData.ItemHeaderz.Add(g_szMESHeader_Current[t]);
	}

	for (int t = 0; t < MESDataNum_IR_ECurrent_IKC; t++)
	{
		strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stCurrent.dbValue[1]);
		m_MesData.Itemz.Add(strData);
	}

	// IIC
	m_MesData.ItemHeaderz.Add(_T("IIC Pattern"));

	if (m_stInspInfo.CamInfo[nPara].stIRImage.stIIC.bResult[0] == TRUE)
		strData = _T("Pass");
	else
		strData = _T("Fail");
	m_MesData.Itemz.Add(strData);

	// Tilt
	for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		m_MesData.ItemHeaderz.Add(g_szMESHeader_Displace[t]);
	}

	for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stDisplace.dbValue[t + 4]);
		m_MesData.Itemz.Add(strData);
	}

	// Shading
	for (int t = 0; t < MESDataNum_IR_Shading; t++)
	{
		m_MesData.ItemHeaderz.Add(g_szMESHeader_Shading[t]);
	}

	for (int t = 0; t < MESDataNum_IR_Shading; t++)
	{
		//strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stShading.dValue[t]);

		switch (t)
		{
		case 0:
			strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stShading.dValue[0]);
			break;
		case 1:
			strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stShading.dValue[2]);
			break;
		case 2:
			strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stShading.dValue[1]);
			break;
		case 3:
			strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stShading.dValue[3]);
			break;
		default:
			break;
		}

		m_MesData.Itemz.Add(strData);
	}

	// Stain
	m_MesData.ItemHeaderz.Add(g_szMESHeader_Stain[0]); //Ymean Count만
	strData.Format(_T("%d"), m_stInspInfo.CamInfo[nPara].stIRImage.stYmean.nDefectCount);
	m_MesData.Itemz.Add(strData);

	// Stain
	m_MesData.ItemHeaderz.Add(g_szMESHeader_Stain[1]); //Ymean Count만
	strData.Format(_T("%d"), m_stInspInfo.CamInfo[nPara].stIRImage.stLCB.nDefectCount);
	m_MesData.Itemz.Add(strData);

	// Stain
	m_MesData.ItemHeaderz.Add(g_szMESHeader_Stain[2]); //Ymean Count만
	strData.Format(_T("%d"), m_stInspInfo.CamInfo[nPara].stIRImage.stBlackSpot.nDefectCount);
	m_MesData.Itemz.Add(strData);

	// Optical Center
	for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{
		m_MesData.ItemHeaderz.Add(g_szMESHeader_OpticalCenter[t]);
	}

	for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{

		strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stOpticalCenter.dValue[t]);
		m_MesData.Itemz.Add(strData);
	}

	// Distortion
	m_MesData.ItemHeaderz.Add(g_szMESHeader_Distortion[0]);

	strData.Format(_T("%.3f"), m_stInspInfo.CamInfo[nPara].stIRImage.stDistortion.dbRotation);

	m_MesData.Itemz.Add(strData);

	// SFR
	for (int t = 0; t < MESDataNum_IR_SFR_IKC; t++)
	{
		m_MesData.ItemHeaderz.Add(g_szMESHeader_SFR[t]);
	}

	for (int t = 0; t < ROI_SFR_Max; t++)
	{
		if (m_stInspInfo.RecipeInfo.stIR_ImageQ.stSFR.stInput[t].bEnable)
			strData.Format(_T("%.2f"), m_stInspInfo.CamInfo[nPara].stIRImage.stSFR.dbValue[t]);
		else
			strData.Format(_T("X"));

		m_MesData.Itemz.Add(strData);
	}

	m_Worklist.Save_TestItemLog_List(m_stInspInfo.Path.szReport, &systime, &m_MesData, _T("Total Item"));
}

//=============================================================================
// Method		: OnReset_CamInfo
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/30 - 19:25
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::OnReset_CamInfo(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.ResetCamInfo(nParaIdx);
}

//=============================================================================
// Method		: _TI_IR_ImgT_SFR_LGAl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/11 - 16:47
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_SFR_LGAl()
{
	//Option 변수(공통)
	const int nMaxROINum = 12; //사용할 ROI 개수
	int nEdgeDir[nMaxROINum] = { 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0 }; // Edge direction: 0(Vertical), 1(Horizontal), 괄호 안에 있는 것들은 UI로 선택 가능하게

	const int nMaxROIWidth = 70; // ROI영역 최대 크기
	const int nMaxROIHeight = 70;

	const double dMaxEdgeAngle = 45; // Angle영역 최대 크기
	const double dPixelSize = 0;

	POINT ptROICenter[nMaxROINum] = { { 628, 472 }, { 642, 600 }, { 572, 544 }, { 698, 528 }, { 266, 282 }, { 322, 212 }, { 1002, 282 }, { 946, 212 }, { 266, 792 }, { 322, 862 }, { 1002, 790 }, { 948, 862 } }; // Center point of ROI region UI에서 Option 빼야됨
	int TxtPositon[nMaxROINum] = { ROI_TOP, ROI_BOTTOM, ROI_LEFT, ROI_RIGHT, ROI_BOTTOM, ROI_RIGHT, ROI_BOTTOM, ROI_LEFT, ROI_TOP, ROI_RIGHT, ROI_TOP, ROI_LEFT };  //Text 위치
	int offset = 0;

	//Option 변수 Freq2sfr
	const int nMaxFreqeuncyNum = 3; //SFR을 구하고자 하는 공간 주파수 개수(SFR을 어디서 부분에서 구할것인지)
	double dFrequency[nMaxFreqeuncyNum] = { 0.25, 0.125, 0.0625 }; // Normalized Spatial frequency for SFR calculation : 0.0 ~ 1.0 (SFR을 구하고자 하는 공간 주파수 값들 => 공간 주파수 개수 만큼) 

	//Option 변수 sfr2freq
	double dSFR = 0.5;

	int imode = 0;

	RawImgInfo stImgInfo;
	TDATASPEC tDataSpec;

	m_LGAlgorithm.GetImageData(MODEL_NIO, tDataSpec, stImgInfo);//Model 변수 만들어서 넣어야 함

	offset = m_LGAlgorithm.GetImageOffset(tDataSpec, stImgInfo); //이미지에 대한 픽셀당 바이트 보정 (이미지 데이터 크기 보정)
	//tDataSpec.eOutMode = OUTMODE_BAYER_GBRG;


	// Image buffers
	vector<BYTE> vFrameBuffer(stImgInfo.nSensorWidth * stImgInfo.nSensorHeight * 2, 0); //BYTE 형 vFrameBuffer 선언 0으로 초기화

	// Image loading
	m_LGAlgorithm.LoadImageData(vFrameBuffer.data(), "D:\\IKC\\NiO_Main.raw"); //vFrameBuffer에 저장

	string pathname = "C:\\project\\LGIT_IR_Front\\LT_EXE_BaseProject\\IKC\\NiO_Main.raw";
	m_LGAlgorithm.SpiltFileName(pathname); //파일 이름만 가져옴

	// Inspection Mode 변환시 사용할 알고리즘
	if (imode == 0)
	{
		m_LGAlgorithm.CalulateSFR(tDataSpec, vFrameBuffer.data() + offset, stImgInfo.nDisplaySizeX, stImgInfo.nDisplaySizeY, nMaxROINum, nMaxFreqeuncyNum, nEdgeDir, ptROICenter, nMaxROIWidth, nMaxROIHeight, dFrequency, TxtPositon, true, dPixelSize, dMaxEdgeAngle, pathname.c_str(), false);

	}
	else
	{
		m_LGAlgorithm.CalulateFreq(tDataSpec, vFrameBuffer.data() + offset, stImgInfo.nDisplaySizeX, stImgInfo.nDisplaySizeY, nMaxROINum, nEdgeDir, ptROICenter, nMaxROIWidth, nMaxROIHeight, dSFR, TxtPositon, true, dPixelSize, dMaxEdgeAngle, pathname.c_str(), false);

	}
}

//=============================================================================
// Method		: _TI_IR_ImgT_Ymean_LGAl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/11 - 16:47
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Ymean_LGAl()
{

	int offset = 0;

	// Image information
	RawImgInfo stImgInfo;
	TDATASPEC tDataSpec;

	// Spec information
	TStainSpec tStainSpec;

	m_LGAlgorithm.GetImageData(MODEL_NIO, tDataSpec, stImgInfo); //Model 변수 만들어서 넣어야 함
	offset = m_LGAlgorithm.GetImageOffset(tDataSpec, stImgInfo); //이미지에 대한 픽셀당 바이트 보정 (이미지 데이터 크기 보정)

	// Image buffers
	vector<BYTE> vFrameBuffer(stImgInfo.nSensorHeight * stImgInfo.nSensorWidth * 2);//BYTE 형 vFrameBuffer 선언 0으로 초기화
	memset(&tStainSpec, 0x00, sizeof(TStainSpec));//초기화
	//----------------------------------------------------------------------
	// Spec - BlackSpot //Option UI로 뺄것들
	//----------------------------------------------------------------------
	tStainSpec.stSpecBlackSpot.nBlockWidth = 32;
	tStainSpec.stSpecBlackSpot.nBlockHeight = 32;
	tStainSpec.stSpecBlackSpot.nClusterSize = 5;
	tStainSpec.stSpecBlackSpot.nDefectInCluster = 5;
	tStainSpec.stSpecBlackSpot.dDefectRatio = 0.45000;
	tStainSpec.stSpecBlackSpot.nMaxSingleDefectNum = 10000;	// noise image
	tStainSpec.stSpecBlackSpot.tCircleSpec.bEnableCircle = false;
	tStainSpec.stSpecBlackSpot.tCircleSpec.nPosOffsetX = 5;
	tStainSpec.stSpecBlackSpot.tCircleSpec.nPosOffsetY = 5;
	tStainSpec.stSpecBlackSpot.tCircleSpec.dRadiusRatioX = 0.4;
	tStainSpec.stSpecBlackSpot.tCircleSpec.dRadiusRatioY = 0.4;

	// Image loading
	m_LGAlgorithm.LoadImageData(vFrameBuffer.data(), "C:\\project\\LGIT_IR_Front\\LT_EXE_BaseProject\\IKC\\NiO_Main_Stain.raw"); //vFrameBuffer에 저장

	string pathname = "C:\\project\\LGIT_IR_Front\\LT_EXE_BaseProject\\IKC\\NiO_Main_Stain.raw";
	m_LGAlgorithm.SpiltFileName(pathname); //파일 이름만 가져옴

	m_LGAlgorithm.InspectStain(tDataSpec, tStainSpec, vFrameBuffer.data() + offset, stImgInfo.nDisplaySizeX, stImgInfo.nDisplaySizeY, true, pathname.c_str(), true, false);
}

//=============================================================================
// Method		: _TI_IR_ImgT_Shading_LGAl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/8/11 - 16:47
// Desc.		:
//=============================================================================
void CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Shading_LGAl()
{
	RawImgInfo stImgInfo;
	TDATASPEC tDataSpec;

	TShadingUniformitySpec m_stShadingUniformitySpec;

	std::cout << std::endl;
	std::cout << __FUNCTION__ << std::endl;

	m_LGAlgorithm.GetImageData(MODEL_IKC, tDataSpec, stImgInfo);
	int offset = m_LGAlgorithm.GetImageOffset(tDataSpec, stImgInfo); //이미지에 대한 픽셀당 바이트 보정 (이미지 데이터 크기 보정)

	// Image buffers
	std::vector<BYTE> vFrameBuffer(stImgInfo.nSensorHeight * stImgInfo.nSensorWidth * 2);

	memset(&m_stShadingUniformitySpec, 0x00, sizeof(TShadingUniformitySpec));


	const int nMaxROINum = 19;

	POINT ptROICenterHor[nMaxROINum] = { { 32, 240 }, { 64, 240 }, { 96, 240 }, { 128, 240 }, { 160, 240 }, { 192, 240 }, { 224, 240 }, { 256, 240 }, { 288, 240 }, { 320, 240 }, { 352, 240 }, { 384, 240 }, { 416, 240 }, { 448, 240 }, { 480, 240 }, { 512, 240 }, { 544, 240 }, { 576, 240 }, { 608, 240 } }; // Center point of ROI region
	POINT ptROICenterVer[nMaxROINum] = { { 320, 24 }, { 320, 48 }, { 320, 72 }, { 320, 96 }, { 320, 120 }, { 320, 144 }, { 320, 168 }, { 320, 192 }, { 320, 216 }, { 320, 240 }, { 320, 264 }, { 320, 288 }, { 320, 312 }, { 320, 336 }, { 320, 360 }, { 320, 384 }, { 320, 408 }, { 320, 432 }, { 320, 456 } };
	POINT ptROICenterDiaA[nMaxROINum] = { { 32, 24 }, { 64, 48 }, { 96, 72 }, { 128, 96 }, { 160, 120 }, { 192, 144 }, { 224, 168 }, { 256, 192 }, { 288, 216 }, { 320, 240 }, { 352, 264 }, { 384, 288 }, { 416, 312 }, { 448, 336 }, { 480, 360 }, { 512, 384 }, { 544, 408 }, { 576, 432 }, { 608, 456 } }; // Center point of ROI region
	POINT ptROICenterDiaB[nMaxROINum] = { { 608, 24 }, { 576, 48 }, { 544, 72 }, { 512, 96 }, { 480, 120 }, { 448, 144 }, { 416, 168 }, { 384, 192 }, { 352, 216 }, { 320, 240 }, { 288, 264 }, { 256, 288 }, { 224, 312 }, { 192, 336 }, { 160, 360 }, { 128, 384 }, { 96, 408 }, { 64, 432 }, { 32, 456 } }; // Center point of ROI region
	double dHorThd[nMaxROINum] = { 35.0, 55.0, 70.0, 80.0, 85.0, 90.0, 100.0, 100.0, 105.0, 105.0, 105.0, 100.0, 100.0, 90.0, 85.0, 80.0, 70.0, 55.0, 35.0 };
	double dVerThd[nMaxROINum] = { 45.0, 60.0, 75.0, 80.0, 85.0, 95.0, 100.0, 100.0, 105.0, 105.0, 105.0, 100.0, 100.0, 95.0, 85.0, 80.0, 75.0, 60.0, 45.0 };
	double dDiaThd[nMaxROINum] = { 1.0, 15.0, 40.0, 70.0, 85.0, 90.0, 100.0, 105.0, 110.0, 110.0, 110.0, 105.0, 100.0, 90.0, 85.0, 70.0, 40.0, 15.0, 1.0 };

	//----------------------------------------------------------------------
	// Spec - Shading Uniformity
	//----------------------------------------------------------------------
	m_stShadingUniformitySpec.nROIWidth = 32;
	m_stShadingUniformitySpec.nROIHeight = 24;
	m_stShadingUniformitySpec.nMaxROICount = 19;
	m_stShadingUniformitySpec.nNormalizeIndex = 7;
	m_stShadingUniformitySpec.dHorThdMin = 9.5;
	m_stShadingUniformitySpec.dHorThdMax = 9.5;
	m_stShadingUniformitySpec.dVerThdMin = 9.5;
	m_stShadingUniformitySpec.dVerThdMax = 9.5;
	m_stShadingUniformitySpec.dDiaThdMin = 9.5;
	m_stShadingUniformitySpec.dDiaThdMax = 9.5;
	m_stShadingUniformitySpec.ptHorROI = ptROICenterHor;
	m_stShadingUniformitySpec.ptVerROI = ptROICenterVer;
	m_stShadingUniformitySpec.ptDiaAROI = ptROICenterDiaA;
	m_stShadingUniformitySpec.ptDiaBROI = ptROICenterDiaB;
	m_stShadingUniformitySpec.dHorThreshold = dHorThd;
	m_stShadingUniformitySpec.dVerThreshold = dVerThd;
	m_stShadingUniformitySpec.dDiaThreshold = dDiaThd;

	// Load image "D:\\Project\\LT_Front\\LT_EXE_BaseProject\\IKC\\20180711_175140.raw"
	//D:\System\Image\No Barcode
	m_LGAlgorithm.LoadImageData(vFrameBuffer.data(), "D:\\System\\Image\\No Barcode\\Capatue_Ymean_0809_211152.raw");

	// Inspect
	m_LGAlgorithm.Inspect_ShaingUniformity(vFrameBuffer.data() + offset, stImgInfo.nDisplaySizeX, stImgInfo.nDisplaySizeY, m_stShadingUniformitySpec, tDataSpec.eDataFormat, tDataSpec.eOutMode, tDataSpec.eSensorType, tDataSpec.nBlackLevel);

}

//=============================================================================
// Method		: _TI_IR_ImgT_SNR_LGAl
// Access		: protected  
// Returns		: int
// Qualifier	:
// Last Update	: 2018/8/11 - 16:47
// Desc.		:
//=============================================================================
int CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_SNR_LGAl()
{
	// Image information
	RawImgInfo stImgInfo;
	TDATASPEC tDataSpec;

	// Spec information
	TSNRBWSpec tSNRSpec;

	// Patch Center Point
	POINT ptCenter[3];

	std::cout << std::endl;
	std::cout << __FUNCTION__ << std::endl;

	m_LGAlgorithm.GetImageData(MODEL_IKC, tDataSpec, stImgInfo);
	int offset = m_LGAlgorithm.GetImageOffset(tDataSpec, stImgInfo); //이미지에 대한 픽셀당 바이트 보정 (이미지 데이터 크기 보정)
	// Image buffers
	std::vector<BYTE> vFrameBuffer(stImgInfo.nSensorWidth * stImgInfo.nSensorHeight * 2);

	memset(&tSNRSpec, 0x00, sizeof(tSNRSpec));

	// Spec
	tSNRSpec.dSNRThreshold = 100;
	tSNRSpec.dDRThreshold = 100;
	tSNRSpec.nROIWidth = (int)(stImgInfo.nDisplaySizeX / 2 * 0.4);
	tSNRSpec.nROIHeight = (int)(stImgInfo.nDisplaySizeY / 2 * 0.4);
	tSNRSpec.nLscBlockSize = 0;
	tSNRSpec.nMaxROICount = 3;
	tSNRSpec.ptROICenter = ptCenter;

	/* White */
	ptCenter[SNR_BW_IMAGE_BLACK].x = (int)((1 - 0.666) * stImgInfo.nDisplaySizeX / 2);
	ptCenter[SNR_BW_IMAGE_BLACK].y = stImgInfo.nDisplaySizeY / 2;
	/* Black */
	ptCenter[SNR_BW_IMAGE_WHITE].x = stImgInfo.nDisplaySizeX / 2;
	ptCenter[SNR_BW_IMAGE_WHITE].y = stImgInfo.nDisplaySizeY / 2;
	/* Gray */
	ptCenter[SNR_BW_IMAGE_GRAY].x = (int)((1 + 0.666) * stImgInfo.nDisplaySizeX / 2);
	ptCenter[SNR_BW_IMAGE_GRAY].y = stImgInfo.nDisplaySizeY / 2;

	// Load image
	m_LGAlgorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front\\LT_EXE_BaseProject\\IKC\\20180711_175140.raw");

	// Inspect
	return m_LGAlgorithm.InspectSNRBW2(tDataSpec, tSNRSpec, vFrameBuffer.data() + offset, stImgInfo.nDisplaySizeX, stImgInfo.nDisplaySizeY, true, "D:\\Project\\LT_Front\\LT_EXE_BaseProject\\IKC\\20180711_175140.raw", false);
}

bool CTestManager_EQP_IR_ImgT::RCCC2CCCC(LPBYTE src, LPBYTE dst, int iW, int iH, int iDepth, int nType)
{
	int nDepth = iDepth / 8;
	double sum = 0;
	int Cnt = 0;
	int Index = 0;
	int Data = 0, DataH = 0, DataL = 0;
	int Ref_X = 0, Ref_Y = 0;

	switch (nType){
	case RCCC:
		Ref_X = 0; Ref_Y = 0;  // R의 위치는 ==> 짝수, 짝수		
		break;
	case CRCC:
		Ref_X = 1; Ref_Y = 0;  // R의 위치는 ==> 홀수, 짝수
		break;
	case CCRC:
		Ref_X = 0; Ref_Y = 1;  // R의 위치는 ==> 짝수, 홀수
		break;
	case CCCR:
		Ref_X = 1; Ref_Y = 1;  // R의 위치는 ==> 홀수, 홀수
		break;
	}
	for (int y = 0; y < iH; y++)
	{
		for (int x = 0; x < iW; x++)
		{
			if (x % 2 == Ref_X && y % 2 == Ref_Y)
			{
				sum = 0.0; Cnt = 0;
				if (x != 0)
				{
					DataH = src[y*(iW * nDepth) + (x - 1) * nDepth + 1] << 8;
					DataL = src[y*(iW * nDepth) + (x - 1) * nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}
				if (y != 0)
				{
					DataH = src[(y - 1)*(iW * nDepth) + (x)* nDepth + 1] << 8;
					DataL = src[(y - 1)*(iW * nDepth) + (x)* nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}
				if (x != (iW - 1))
				{
					DataH = src[y*(iW * nDepth) + (x + 1) * nDepth + 1] << 8;
					DataL = src[y*(iW * nDepth) + (x + 1) * nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}
				if (y != (iH - 1))
				{
					DataH = src[(y + 1)*(iW * nDepth) + (x)* nDepth + 1] << 8;
					DataL = src[(y + 1)*(iW * nDepth) + (x)* nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}

				if (Cnt != 0) 
				{
					Data = sum / (double)Cnt;
					dst[y*(iW * nDepth) + x * nDepth] = (unsigned char)(Data & 0x00ff);
					dst[y*(iW * nDepth) + x * nDepth + 1] = (unsigned char)((Data >> 8) & 0x00ff);
				}
				else 
				{
					dst[y*(iW * nDepth) + x * nDepth] = 0;
					dst[y*(iW * nDepth) + x * nDepth + 1] = 0;
				}

			}
			else
			{
				for (int k = 0; k < nDepth; k++)
				{
					dst[y*(iW * nDepth) + x*nDepth + k] = src[y*(iW * nDepth) + x * nDepth + k];
				}
			}
		}
	}
	return true;
}

//=============================================================================
// Method		: StartTest_Particle_TestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/23 - 16:32
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP_IR_ImgT::Save_ResultData(UINT nTestID, UINT nParaIdx)
{
	BOOL bReturn = FALSE;

	CString sLogPath, filePath ;
	CTime sTime;

	sTime = CTime::GetCurrentTime();
	sLogPath.Format(_T("D://System/Log/%d/%d/%d"), sTime.GetYear(), sTime.GetMonth(), sTime.GetDay());

	CreateFolder(sLogPath);

	CFile processData;
	filePath.Format(_T("%d%d%d.txt"), sTime.GetYear(), sTime.GetMonth(), sTime.GetDay());

	if (!processData.Open(sLogPath + filePath, CFile::modeCreate | CFile::modeWrite | CFile::typeBinary, NULL))
	{
		//AfxMessageBox("ProcessData exception!");
		processData.Close();

		return bReturn;
	}
	CString SLable;
	CString SData, sTemp;

	switch (nTestID)
	{
	case TI_IR_ImgT_Fn_SFR:
		SLable = _T("시간 / Barcode / Total / ROI / ROI Data");
		processData.Write(SLable, SLable.GetLength() * sizeof(TCHAR));

		SData.Format(_T("%d:%d:%d, %d, %d, "), sTime.GetHour(), sTime.GetMinute(), sTime.GetSecond(), 123456, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.GetFinalResult());

		for (int iNum = 0; iNum < ROI_SFR_Max; iNum++)
		{
			sTemp.Format(_T("%d , %d, %d, "), iNum, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.bResult[iNum], m_stInspInfo.CamInfo[nParaIdx].stIRImage.stSFR.dbValue[iNum]);
			SData += sTemp;

		}
		processData.Write(SData, SData.GetLength() * sizeof(TCHAR));

		break;
	default:
		break;
	}


	processData.Close();

	return bReturn;
}

//=============================================================================
// Method		: _TI_IR_ImgT_Pro_DynamicBW
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/11 - 15:06
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_RI(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

// 	int iImageW = 0;
// 	int iImageH = 0;
// 
// 	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("Rllumination")))
// 	{
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		lReturn = RC_NoImage;
// 		//return lReturn;
// 	}
// 
// 	lReturn = m_tm_Test.LGIT_Func_Rllumination(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stRllumination, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stRllumination);
// 
// 	//// * 측정값 입력 (카메라 초기화)
// 	COleVariant varResult;
// 	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_RI].vt);
// 	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
// 	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);
// 
// 	if (RC_OK == lReturn)
// 	{
// 		// * 검사 항목별 결과 
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
// 	}
// 	else
// 	{
// 		// * 에러 상황
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 	}

	return lReturn;
}

LRESULT CTestManager_EQP_IR_ImgT::_TI_IR_ImgT_Pro_Distortion(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	int iImageW = 0;
	int iImageH = 0;

// 	if (RC_OK != OnLightBrd_PowerOn(Slot_A, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].fVolt, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[Light_I_ChartTest].wStep))
// 	{
// 		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
// 		return RC_Light_Brd_Err_Ack;
// 	}
// 	Sleep(500);
	if (!_TI_Get_ImageBuffer_CaptureImage(m_stImageMode.eImageMode, nParaIdx, iImageW, iImageH, m_stInspInfo.CamInfo[nParaIdx].stIRImage.szFileNmae, TRUE, _T("Distortion")))
	{
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
		lReturn = RC_NoImage;
		//return lReturn;
	}

	lReturn = m_tm_Test.LGIT_Func_Distortion(m_stImageBuf[nParaIdx].lpbyImage_8bit, m_stRawBuf[nParaIdx].lpbyImage_Raw, iImageW, iImageH, m_stInspInfo.RecipeInfo.stIR_ImageQ.stDistortion, m_stInspInfo.CamInfo[nParaIdx].stIRImage.stDistortion);

	//// * 측정값 입력 (카메라 초기화)
	COleVariant varResult;
	varResult.ChangeType(m_stInspInfo.RecipeInfo.TestItemInfo.TestItemList[TI_IR_ImgT_Fn_Distortion].vt);
	varResult.SetString(g_szResultCode[lReturn], VT_BSTR);
	m_stInspInfo.Set_Measurement_NoJudge(nParaIdx, nStepIdx, 1, &varResult);

	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과 
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}
