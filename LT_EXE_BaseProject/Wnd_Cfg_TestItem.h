//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestItem_h__
#define Wnd_Cfg_TestItem_h__

#pragma once


#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_TestItem_Cm.h"
#include "Def_DataStruct.h"

#include "Wnd_Cfg_2DCal.h"
#include "Wnd_Cfg_2DCal_Spec.h"

#include "Wnd_Cfg_WipID.h"

#include "Wnd_Cfg_TestItem_EachTest.h"

//-----------------------------------------------------------------------------
// CWnd_Cfg_TestItem
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestItem : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestItem)

public:
	CWnd_Cfg_TestItem();
	virtual ~CWnd_Cfg_TestItem();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void	OnNMClickTestItem	(NMHDR *pNMHDR, LRESULT *pResult);

	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_Focusing;

	CMFCTabCtrl				m_tc_Option;
	
	CWnd_Cfg_2DCal			m_wnd_Cfg2DCal;
	CWnd_Cfg_2DCal_Spec		m_wnd_Cfg2DCal_Spec;

	CWnd_Cfg_WipID			m_wnd_CfgWipID;

	void		SetSysAddTabCreate		();
	void		SetInitListCtrl			();

	CWnd_Cfg_TestItem_EachTest			m_wnd_TestItem_EachTest;
	UINT*								m_pnCamParaIdx				= NULL;
	
public:
	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	void		Set_ModelType			(__in enModelType nModelType);

	// 저장된 Test Item Info 데이터 불러오기
	void		Set_RecipeInfo			(__in  ST_RecipeInfo* pstRecipeInfo);
	void		Get_RecipeInfo			(__out ST_RecipeInfo& stOutRecipInfo);

	//void		Set_TestItemInfo		(__in const ST_TestItemInfo* pstTestItemInfo, __in const ST_TestItemOpt* pstInTestItemOpt);
	//void		Get_TestItemInfo		(__out ST_TestItemInfo& stOutTestItemInfo, __out ST_TestItemOpt& stOutTestItemOpt);
	

	void	SetCameraParaIdx(__in UINT *pstCamParaIdx)
	{
		if (NULL == pstCamParaIdx)
			return;

		m_pnCamParaIdx = pstCamParaIdx;
	};
};
#endif // Wnd_Cfg_TestItem_h__


