﻿//*****************************************************************************
// Filename	: 	TestManager_Test.cpp
// Created	:	2017/10/14 - 18:01
// Modified	:	2017/10/14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_Test.h"
#include "CommonFunction.h"

enum { RCCC, CRCC, CCRC, CCCR };

#define		Center_Auto_Detect	1				// (기본 : 사용)

#define		QUARTER_MODE		1

CTestManager_Test::CTestManager_Test()
{
}

CTestManager_Test::~CTestManager_Test()
{
}

//=============================================================================
// Method		: LGIT_Func_Chart
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_Chart stOption
// Parameter	: __out ST_Result_Chart & stResult
// Qualifier	:
// Last Update	: 2018/8/23 - 16:53
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Chart(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Chart stOption, __out ST_Result_Chart& stResult)
{
	LRESULT	lReturn = RC_OK;

	int iBright		= stOption.nBrightness;
	int iColor		= stOption.nMarkColor;
	int nSharpness	= 0;

	int iCenterX	= 0;
	int iCenterY	= 0;

	lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.rtROI, stResult.rtROI);

	if (RC_OK == lReturn)
	{
		stResult.bDetect = m_LT_CircleDetect.CircleDetect_Test(pImage8bit, iImageW, iImageH, stResult.rtROI, iBright, iCenterX, iCenterY, iColor, nSharpness);
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_SFR
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_SFR stOption
// Parameter	: __out ST_Result_SFR & stResult
// Qualifier	:
// Last Update	: 2018/8/7 - 0:03
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_SFR(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_SFR stOption, __out ST_Result_SFR& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 자동 ROI에 사용될 영상 데이터
	IplImage *lpAutoImage = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 3);
	memcpy(lpAutoImage->imageData, pImage8bit, lpAutoImage->widthStep * lpAutoImage->height);
	
	// 나중에 꼭 바꾸자...
	OnTestProcess_SFRROI(lpAutoImage, stOption, stResult);

//  	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
//  	{
//  		if (TRUE == stOption.stInput[nROI].bEnable)
//  		{
//  			// 광축 대비
//  			lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, stResult.rtROI[nROI]);
//  
//  			// 광축 대비 + 객체 중심
//  			//lReturn = LURI_Func_DetectROI_Ver2(lpAutoImage, stOption.stInput[nROI].rtROI, stResult.rtROI[nROI]);
//  		}
//  	}




	// 자동 ROI에 사용된 영상 데이터 해제
	cvReleaseImage(&lpAutoImage);

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 4 분할 리사이징

#if QUARTER_MODE
	IplImage *image12bitRaw			= cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 2);
	IplImage *image12bitRaw_Quarter = cvCreateImage(cvSize(iImageW / 2, iImageH / 2), IPL_DEPTH_8U, 2);
	IplImage *image16bitRaw_Quarter = cvCreateImage(cvSize(iImageW / 2, iImageH / 2), IPL_DEPTH_8U, 2);

	//- 기존 원본사이즈의 12bitRaw를 Copy한다.
	memcpy(image12bitRaw->imageData, pImageRaw, iImageW*iImageH * 2);

	int iHalfImageW = iImageW / 2;
	int iHalfImageH = iImageH / 2;

	// 12bit raw을 Quard로 바꾸면서 16bit 변환 , 마지막 매개 인자를 바꾸면 odd,even이 바뀐다.
	Shift12to16Bit_QuarterMode((unsigned char*)image12bitRaw->imageData, (unsigned char*)image16bitRaw_Quarter->imageData, iImageW, iImageH, CCCR);
	// 16bit을 12bit 변환
	Shift16to12BitMode((unsigned char*)image16bitRaw_Quarter->imageData, (unsigned char*)image12bitRaw_Quarter->imageData, iHalfImageW, iHalfImageH);


	// raw가 잘 변환 되었는지 보기 위한 용도로 아래 함수를 사용.
	IplImage *imageQuarter_8bitDebug = cvCreateImage(cvSize(iImageW / 2, iImageH / 2), IPL_DEPTH_8U, 1);
	Shift12BitMode((unsigned char*)image12bitRaw_Quarter->imageData, (unsigned char*)imageQuarter_8bitDebug->imageData, iHalfImageW, iHalfImageH);

	cvSaveImage("D:\\ImageOrg.bmp", imageQuarter_8bitDebug);
#endif

	CRect QuarterRect;
	CvPoint nStartPt;
	CvPoint nEndPt;

	for (UINT nROI = 0; nROI < ROI_SFR_Max; nROI++)
	{
		if (TRUE == stOption.stInput[nROI].bEnable)
		{

#if QUARTER_MODE
			// 1/4 영역 위치로 변경하기
			QuarterRect.left = stResult.rtROI[nROI].left * iHalfImageW / iImageW;
			QuarterRect.right = stResult.rtROI[nROI].right *iHalfImageW / iImageW;
			QuarterRect.top = stResult.rtROI[nROI].top * iHalfImageH / iImageH;
			QuarterRect.bottom = stResult.rtROI[nROI].bottom * iHalfImageH / iImageH;

			nStartPt.x = QuarterRect.left;
			nStartPt.y = QuarterRect.top;

			nEndPt.x = QuarterRect.right;
			nEndPt.y = QuarterRect.bottom;
			
			cvRectangle(imageQuarter_8bitDebug, nStartPt, nEndPt, CV_RGB(150, 150, 150), 1); // 정상적으로 Rect가 그려지는 보기 위한 용도
			// 2. SFR Config
			m_LG_Algorithm.LGIT_SFR_Config(QuarterRect.Width(), QuarterRect.Height(), stOption.dbMaxEdgeAngle, stOption.dbPixelSize, (ESFRAlgorithmType)stOption.eType, (ESFRAlgorithmMethod)stOption.eMethod, stOption.bEnableLog, (ESFRFrequencyUnit)stOption.eConverse);

			// 3. SFR DATA
			m_LG_Algorithm.LGIT_SFR_Func((unsigned char*)image12bitRaw_Quarter->imageData, iHalfImageW, iHalfImageH, QuarterRect, stOption.stInput[nROI].iEdgeDir, stOption.stInput[nROI].dbFrequencyLists, stOption.stInput[nROI].dbSFR, stOption.stInput[nROI].iFrequencyNum, stResult.dbValue[nROI]);
			
#else
			m_LG_Algorithm.LGIT_SFR_Config(stOption.stInput[nROI].rtROI.Width(), stOption.stInput[nROI].rtROI.Height(), stOption.dbMaxEdgeAngle, stOption.dbPixelSize, (ESFRAlgorithmType)stOption.eType, (ESFRAlgorithmMethod)stOption.eMethod, stOption.bEnableLog, (ESFRFrequencyUnit)stOption.eConverse);

			// 3. SFR DATA
			m_LG_Algorithm.LGIT_SFR_Func(pImageRaw, iImageW, iImageH, stResult.rtROI[nROI], stOption.stInput[nROI].iEdgeDir, stOption.stInput[nROI].dbFrequencyLists, stOption.stInput[nROI].dbSFR, stOption.stInput[nROI].iFrequencyNum, stResult.dbValue[nROI]);

#endif
			stResult.dbValue[nROI] += stOption.stInput[nROI].dbOffset;

			// 판정
			BOOL	bMinUse = stOption.stInput[nROI].stSpecMin.bEnable;
			BOOL	bMaxUse = stOption.stInput[nROI].stSpecMax.bEnable;
			double dbMinDev = stOption.stInput[nROI].stSpecMin.dbValue;
			double dbMaxDev = stOption.stInput[nROI].stSpecMax.dbValue;
			 
			stResult.bResult[nROI] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbValue[nROI]);
		}
	}

#if QUARTER_MODE

	cvSaveImage("D:\\ImageRect.bmp", imageQuarter_8bitDebug);
	cvReleaseImage(&imageQuarter_8bitDebug);
	cvReleaseImage(&image12bitRaw);
	cvReleaseImage(&image12bitRaw_Quarter);
	cvReleaseImage(&image16bitRaw_Quarter);	
		
#endif
	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_DynamicBW
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_DynamicBW stOption
// Parameter	: __out ST_Result_DynamicBW & stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 9:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_DynamicBW(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_DynamicBW stOption, __out ST_Result_DynamicBW& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	// 1. 자동ROI
	for (UINT nROI = 0; nROI < ROI_DnyBW_Max; nROI++)
	{
		if (TRUE == stOption.stInput[nROI].bEnable)
		{
			//lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].ptROI, stResult.rtROI[nROI]);//상훈씨
		}
	}
	
	//@SH _180809: Raw Data로 데이터 볼떄 씀, 최종 빌드시 삭제 예정
	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	POINT pPoitnTemp[ROI_DnyBW_Max];

	//!SH _180809: Data Log 필요할 수 있을것 같아서 만들어만 둠. 현재는 쓰지는 않음
	
	Set_Dinamic_ROI_Array(stOption, pPoitnTemp);

	// 2. DynamicBW Config
	m_LG_Algorithm.LGIT_DynamicBW_Config(stOption.iMaxROIWidth, stOption.iMaxROIHeight, stOption.iLscBlockSize, stOption.iMaxROICount, stOption.dbSNRThreshold, stOption.dbDRThreshold, pPoitnTemp);// , stOption.bEnableLog);
	//
	//// 3. DynamicBW DATA
	m_LG_Algorithm.LGIT_DynamicBW_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.dbVarianceValue, stResult.dbAverageValue, stResult.dbSNR_BW, stResult.dbDynamic);

	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_Dynamic].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_Dynamic].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_Dynamic].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_Dynamic].dbValue;
	 
	stResult.dbDynamic *= stOption.dbOffsetDR;

	stResult.bDynamic = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDynamic);

	bMinUse  = stOption.stSpecMin[Spec_SNR_BW].bEnable;
	bMaxUse  = stOption.stSpecMax[Spec_SNR_BW].bEnable;
	dbMinDev = stOption.stSpecMin[Spec_SNR_BW].dbValue;
	dbMaxDev = stOption.stSpecMax[Spec_SNR_BW].dbValue;

	stResult.dbSNR_BW *= stOption.dbOffsetSNR;

	stResult.bSNR_BW = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbSNR_BW);

	if (stResult.bDynamic == FALSE || stResult.bSNR_BW == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_Shading
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_Shading stOption
// Parameter	: __out ST_Result_Shading & stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 9:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Shading(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Shading stOption, __out ST_Result_Shading& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;




// 
// 	int iOffsetX = 0;
// 	int iOffsetY = 0;
// 
// 	// 1. 자동ROI
// // 	for (UINT nROI = 0; nROI < ROI_Shading_Max; nROI++)
// 	// 	{
// // 		if (TRUE == stOption.stInput[nROI].bEnable)
// // 		{
// // 			lReturn = LURI_Func_DetectROI_Ver1(pImage8bit, iImageW, iImageH, stOption.stInput[nROI].rtROI, stResult.rtROI[nROI]);
// // 		}
// 	// 	}
// 	//vector<BYTE> vFrameBuffer(iImageW * (iImageH+4) * 2, 0);
// 	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\System\\Image\\No Barcode\\Capatue_Ymean_0809_211152.raw");
// 	
// 	POINT ptROICenterHor[19];// = { { 32, 240 }, { 64, 240 }, { 96, 240 }, { 128, 240 }, { 160, 240 }, { 192, 240 }, { 224, 240 }, { 256, 240 }, { 288, 240 }, { 320, 240 }, { 352, 240 }, { 384, 240 }, { 416, 240 }, { 448, 240 }, { 480, 240 }, { 512, 240 }, { 544, 240 }, { 576, 240 }, { 608, 240 } }; // Center point of ROI region
// 	POINT ptROICenterVer[19];// = { { 320, 24 }, { 320, 48 }, { 320, 72 }, { 320, 96 }, { 320, 120 }, { 320, 144 }, { 320, 168 }, { 320, 192 }, { 320, 216 }, { 320, 240 }, { 320, 264 }, { 320, 288 }, { 320, 312 }, { 320, 336 }, { 320, 360 }, { 320, 384 }, { 320, 408 }, { 320, 432 }, { 320, 456 } };
// 	POINT ptROICenterDiaA[19];// = { { 32, 24 }, { 64, 48 }, { 96, 72 }, { 128, 96 }, { 160, 120 }, { 192, 144 }, { 224, 168 }, { 256, 192 }, { 288, 216 }, { 320, 240 }, { 352, 264 }, { 384, 288 }, { 416, 312 }, { 448, 336 }, { 480, 360 }, { 512, 384 }, { 544, 408 }, { 576, 432 }, { 608, 456 } }; // Center point of ROI region
// 	POINT ptROICenterDiaB[19];// = { { 608, 24 }, { 576, 48 }, { 544, 72 }, { 512, 96 }, { 480, 120 }, { 448, 144 }, { 416, 168 }, { 384, 192 }, { 352, 216 }, { 320, 240 }, { 288, 264 }, { 256, 288 }, { 224, 312 }, { 192, 336 }, { 160, 360 }, { 128, 384 }, { 96, 408 }, { 64, 432 }, { 32, 456 } }; // Center point of ROI region
// 	double dHorThd[19]; //= { 35.0, 55.0, 70.0, 80.0, 85.0, 90.0, 100.0, 100.0, 105.0, 105.0, 105.0, 100.0, 100.0, 90.0, 85.0, 80.0, 70.0, 55.0, 35.0 };
// 	double dVerThd[19];//= { 45.0, 60.0, 75.0, 80.0, 85.0, 95.0, 100.0, 100.0, 105.0, 105.0, 105.0, 100.0, 100.0, 95.0, 85.0, 80.0, 75.0, 60.0, 45.0 };
// 	double dDiaThd[19]; //= { 1.0, 15.0, 40.0, 70.0, 85.0, 90.0, 100.0, 105.0, 110.0, 110.0, 110.0, 105.0, 100.0, 90.0, 85.0, 70.0, 40.0, 15.0, 1.0 };
// 
// 	// 1. 알고리즘 공통 셋팅
// 	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);
// 
// 	//// 2. Shading Config
// 	m_LG_Algorithm.LGIT_Shading_Config(stOption.iMaxROIWidth, stOption.iMaxROIHeight, stOption.iMaxROICount, stOption.iNormalizeIndex, 
// 		stOption.stInputHor[0].stSpecMin.dbValue, stOption.stInputHor[0].stSpecMax.dbValue, 
// 		stOption.stInputVer[0].stSpecMin.dbValue, stOption.stInputVer[0].stSpecMax.dbValue, 
// 		stOption.stInputDirA[0].stSpecMin.dbValue, stOption.stInputDirA[0].stSpecMax.dbValue);// , stOption.bEnableLog);
// 	////m_LG_Algorithm.LGIT_Shading_Config_Array(stOption.dbHorThreshold, stOption.dbVerThreshold, stOption.dbDiaThreshold, stOption.ptHorROI, stOption.ptVerROI, stOption.ptDiaAROI, stOption.ptDiaBROI, stOption.iMaxROICount);
// 	Set_Shading_ROI_Array(stOption, dHorThd, dVerThd, dDiaThd, ptROICenterHor, ptROICenterVer, ptROICenterDiaA, ptROICenterDiaB);
// 
// 	m_LG_Algorithm.LGIT_Shading_Config_Array(dHorThd, dVerThd, dDiaThd, ptROICenterHor, ptROICenterVer, ptROICenterDiaA, ptROICenterDiaB, 19);
// 	//
// 	//// 3. Shading DATA
// 	////@SH _180808: CenterPoint, bLGITShadingResult는 값은 받아오지만 쓰지는 않음.
// 	double CenterResult;
// 	bool   bLGITShadingResult[4][ROI_Shading_Max];
// 
// 
// 
// 	m_LG_Algorithm.LGIT_Shading_Func(pImageRaw, iImageW, iImageH, CenterResult,
// 		stResult.ptHorROI, stResult.ptVerROI, stResult.ptDiaAROI, stResult.ptDiaBROI,
// 		stResult.dbHorizonValue, stResult.dbVerticalValue, stResult.dbDiaAValue, stResult.dbDiaBValue,
// 		bLGITShadingResult[0], bLGITShadingResult[1], bLGITShadingResult[2], bLGITShadingResult[3]);
// 
// 	// 판정
// 	BOOL	bMinUse;
// 	BOOL	bMaxUse;
// 	double dbMinDev;
// 	double dbMaxDev;
// 
// 
// 	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
// 	{
// 		bMinUse = stOption.stInputHor[nldx].stSpecMin.bEnable;
// 		bMaxUse = stOption.stInputHor[nldx].stSpecMax.bEnable;
// 		dbMinDev = stOption.stInputHor[nldx].stSpecMin.dbValue;
// 		dbMaxDev = stOption.stInputHor[nldx].stSpecMax.dbValue;
// 		stResult.bHorizonResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbHorizonValue[nldx]);
// 	}
// 	
// 	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
// 	{
// 		bMinUse = stOption.stInputVer[nldx].stSpecMin.bEnable;
// 		bMaxUse = stOption.stInputVer[nldx].stSpecMax.bEnable;
// 		dbMinDev = stOption.stInputVer[nldx].stSpecMin.dbValue;
// 		dbMaxDev = stOption.stInputVer[nldx].stSpecMax.dbValue;
// 		stResult.bVerticalResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbVerticalValue[nldx]);
// 	}
// 	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
// 	{
// 		bMinUse = stOption.stInputDirA[nldx].stSpecMin.bEnable;
// 		bMaxUse = stOption.stInputDirA[nldx].stSpecMax.bEnable;
// 		dbMinDev = stOption.stInputDirA[nldx].stSpecMin.dbValue;
// 		dbMaxDev = stOption.stInputDirA[nldx].stSpecMax.dbValue;
// 		stResult.bDiaAResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDiaAValue[nldx]);
// 	}
// 	for (int nldx = 0; nldx < ROI_Shading_Max; nldx++)
// 	{
// 		bMinUse = stOption.stInputDirB[nldx].stSpecMin.bEnable;
// 		bMaxUse = stOption.stInputDirB[nldx].stSpecMax.bEnable;
// 		dbMinDev = stOption.stInputDirB[nldx].stSpecMin.dbValue;
// 		dbMaxDev = stOption.stInputDirB[nldx].stSpecMax.dbValue;
// 		stResult.bDiaBResult[nldx] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbDiaBValue[nldx]);
// 	}


	return lReturn;
}



//=============================================================================
// Method		: LGIT_Func_Shading
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImage8bit
// Parameter	: __in LPBYTE pImageRaw
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in ST_UI_Shading stOption
// Parameter	: __out ST_Result_Shading & stResult
// Qualifier	:
// Last Update	: 2018/8/9 - 9:57
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Rllumination(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rllumination stOption, __out ST_Result_Rllumination& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	int iOffsetX = 0;
	int iOffsetY = 0;

	// 1. 자동ROI
	//vector<BYTE> vFrameBuffer(iImageW * (iImageH+4) * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\System\\Image\\No Barcode\\Capatue_Ymean_0809_211152.raw");

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	//// 2. Shading Config
	m_LG_Algorithm.LGIT_RI_Config(stOption.dBoxField, stOption.nBoxSize, stOption.dSpecRIcornerMax, stOption.dSpecRIcornerMin, stOption.dSpecRIminMax, stOption.dSpecRIminMin, stOption.b8BitUse);// , stOption.bEnableLog);
	
	//// 3. Shading DATA
	////@SH _180808: CenterPoint, bLGITShadingResult는 값은 받아오지만 쓰지는 않음.
	//double CenterResult;
	//bool   bLGITShadingResult[4][ROI_Shading_Max];

	m_LG_Algorithm.LGIT_RelativeIllumination_Func(pImageRaw, iImageW, iImageH, stResult.ptRIROI, stResult.dRICorner, stResult.dRIMin, stResult.dRIValue, stOption.dbCorneroffset, stOption.dbMinoffset);
	
	// 판정
	BOOL	bMinUse = stOption.stSpecMin[Spec_RI_Coner].bEnable;
	BOOL	bMaxUse = stOption.stSpecMax[Spec_RI_Coner].bEnable;
	double dbMinDev = stOption.stSpecMin[Spec_RI_Coner].dbValue;
	double dbMaxDev = stOption.stSpecMax[Spec_RI_Coner].dbValue;

/*	stResult.dbDynamic *= stOption.dbOffsetDR;*/

	stResult.dRIResult[0] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dRICorner);



	bMinUse = stOption.stSpecMin[Spec_RI_Min].bEnable;
	bMaxUse = stOption.stSpecMax[Spec_RI_Min].bEnable;
	dbMinDev = stOption.stSpecMin[Spec_RI_Min].dbValue;
	dbMaxDev = stOption.stSpecMax[Spec_RI_Min].dbValue;

	//stResult.dbSNR_BW *= stOption.dbOffsetSNR;

	stResult.dRIResult[1] = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dRIMin);

	if (stResult.dRIResult[1] == FALSE ||stResult.dRIResult[0] == FALSE)
	{
		lReturn = RC_TestSkip;
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Distortion(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Rotation& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//!SH _180908: ROI 잡는 부분 필요
	/*
	
	*/

	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config__in int nROIBoxSize, __in int nMaxROIBoxSize, __in double dRadius, __in double dRealGapX, __in double dRealGapY, __in int nFiducialMarkNum, __in int nFiducialMarkType, __in double dModuleChartDistance, __in int nDistortionAlrotithmType
	m_LG_Algorithm.LGIT_Rotation_Config(stOption.nROIBoxSize, stOption.nMaxROIBoxSize, stOption.dRadius, stOption.dRealGapX, stOption.dRealGapY, stOption.nFiducialMarkNum, stOption.nFiducialMarkType, stOption.dModuleChartDistance, stOption.nDistortionAlrotithmType);// , stOption.bEnableLog);

	for (int nidx = 0; nidx < ROI_Rotation_Max; nidx++)
	{
		stResult.rtROI[nidx] = stOption.stInput[nidx].rtROI;
	}


	// 3. Ymean Wide DATA
	m_LG_Algorithm.LGIT_Rotation_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.dbRotation);


	// 판정
	for (int nidx = 0; nidx < Spec_Rotation_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		double		dbMinDev = stOption.stSpecMin[nidx].dbValue;
		double		dbMaxDev = stOption.stSpecMax[nidx].dbValue;
		stResult.bRotation = Get_MeasurmentData(bMinUse, bMaxUse, dbMinDev, dbMaxDev, stResult.dbRotation);
	}

	if (stResult.bRotation == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_DefectBlack(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_Black stOption, __out ST_Result_Defect_Black& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_DefectBlack_Config(stOption.nBlockSize, stOption.dbDefectRatio, stOption.nMaxSingleDefectNum);// , stOption.bEnableLog);

	// 3. Ymean Wide DATA
	m_LG_Algorithm.LGIT_DefectBlack_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.nDefect_BlackCount);
	

	// 판정
	for (int nidx = 0; nidx < Spec_Defect_Black_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bDefect_BlackResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefect_BlackCount);
	}

	if (stResult.bDefect_BlackResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_DefectWhite(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_White stOption, __out ST_Result_Defect_White& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_DefectWhite_Config(stOption.nBlockSize, stOption.dbDefectRatio, stOption.nMaxSingleDefectNum);// , stOption.bEnableLog);

	// 3. Ymean Wide DATA
	m_LG_Algorithm.LGIT_DefectWhite_Func(pImageRaw, iImageW, iImageH, stResult.ptROI, stResult.nDefect_WhiteCount);


	// 판정
	for (int nidx = 0; nidx < Spec_Defect_Black_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bDefect_WhiteResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefect_WhiteCount);
	}

	if (stResult.bDefect_WhiteResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}
//=============================================================================
// Method		: LGIT_Func_Ymean
// Access		: public  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LGIT_Func_Ymean(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Ymean stOption, __out ST_Result_Ymean& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기
	
	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_Ymean_Config(stOption.iDefectBlockSize, stOption.iEdgeSize, stOption.fCenterThreshold, stOption.fEdgeThreshold, stOption.fCornerThreshold, stOption.iLscBlockSize, 
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		double pCircleMaxValue;

		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_Ymean_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.nCircleCount, pCircleMaxValue, stResult.pCircleMaxPoint,
			stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
		/* __out int& nCircleCount, __out double& pCircleMaxValue, __out CPoint& pCircleMaxPoint,
	__out int& ocx, __out int& ocy, __out int& radx, __out int& rady)*/
	}
	else
	{	
		double CenterValue;
		double EdgeValue;
		double CornerValue;

		// 3. Ymean DATA
		m_LG_Algorithm.LGIT_Ymean_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, 
			stResult.nCenterCount, CenterValue, stResult.pCenterMaxPoint,
			stResult.nEdgeCount, EdgeValue, stResult.pEdgeMaxPoint,
			stResult.nCornerCount, CornerValue, stResult.pCornerMaxPoint);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_Ymean_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bYmeanResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bYmeanResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}


LRESULT CTestManager_Test::LGIT_Func_BlackSpot(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_BlackSpot stOption, __out ST_Result_BlackSpot& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_BlackSpot_Config(stOption.nBlockWidth, stOption.nBlockHeight, stOption.nClusterSize, stOption.nDefectInCluster, stOption.dDefectRatio, stOption.nMaxSingleDefectNum,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		double pCircleMaxValue = 0.0;

		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_BlackSpot_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
	}
	else
	{
		// 3. Ymean DATA
		m_LG_Algorithm.LGIT_BlackSpot_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_BlackSpot_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bBlackSpotResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bBlackSpotResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::LGIT_Func_LCB(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_LCB stOption, __out ST_Result_LCB& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	//vector<BYTE> vFrameBuffer(iImageW * iImageH * 2, 0);
	//m_LG_Algorithm.LoadImageData(vFrameBuffer.data(), "D:\\Project\\LT_Front_180808\\System\\Image\\1\\Capatue_SFR_0808_094405.raw");

	stResult.Reset(); //20180809 상훈씨 데이터 초기화 수정하기

	// 1. 알고리즘 공통 셋팅
	m_LG_Algorithm.LGIT_SetDataSpec((EDATAFORMAT)stOption.stInspect.eDataForamt, (EOUTMODE)stOption.stInspect.eOutMode, (ESENSORTYPE)stOption.stInspect.eSensorType, stOption.stInspect.iBlackLevel);

	// 2. Ymean Config
	m_LG_Algorithm.LGIT_LCB_Config(stOption.dCenterThreshold, stOption.dEdgeThreshold, stOption.dCornerThreshold, stOption.nMaxSingleDefectNum, stOption.nMinDefectWidthHeight,
		stOption.bEnableCircle, stOption.iPosOffsetX, stOption.iPosOffsetY, stOption.dbRadiusRatioX, stOption.dbRadiusRatioY);// , stOption.bEnableLog);

	if (stOption.bEnableCircle == true)
	{
		// 3. Ymean Wide DATA
		m_LG_Algorithm.LGIT_LCB_Wide_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount, stResult.ocx, stResult.ocy, stResult.radx, stResult.rady);
	}
	else
	{
		// 3. Ymean DATA
		m_LG_Algorithm.LGIT_LCB_Func(pImageRaw, iImageW, iImageH, stResult.rtROI, stResult.nDefectCount);
	}

	// 판정
	for (int nidx = 0; nidx < Spec_LCB_MAX; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		int		iMinDev = stOption.stSpecMin[nidx].iValue;
		int		iMaxDev = stOption.stSpecMax[nidx].iValue;
		stResult.bLCBResult = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.nDefectCount);
	}

	if (stResult.bLCBResult == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}

LRESULT CTestManager_Test::Luri_Func_OpticalCenter(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_OpticalCenter stOption, __out ST_Result_OpticalCenter& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	int icx = iImageW / 2;
	int icy = iImageH / 2;

	double ptCenterX;
	double ptCenterY;
	CRect rtROI;
	rtROI.left = icx - 150;
	rtROI.right = icx + 150;
	rtROI.top = icy - 150;
	rtROI.bottom = icy + 150;

	// 초기값
	ptCenterX = -1;
	ptCenterY = -1;

	IplImage *srcImage = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 3);
	IplImage *OriginImage = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 1);
	IplImage *DilateImage = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 1);
	IplImage *BinImage = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 1);

	cvSetZero(OriginImage);
	cvSetZero(DilateImage);

	// ROI Image Cut
	//cvSetImageROI(pImageBuf, cvRect(rtROI.left, rtROI.top, rtROI.Width(), rtROI.Height()));

	for (int y = 0; y < iImageH; y++)
	{
		for (int x = 0; x < iImageW; x++)
		{
			srcImage->imageData[y * srcImage->widthStep + x * 3 + 0] = pImage8bit[y * iImageW * 3 + x * 3 + 0];
			srcImage->imageData[y * srcImage->widthStep + x * 3 + 1] = pImage8bit[y * iImageW * 3 + x * 3 + 1];
			srcImage->imageData[y * srcImage->widthStep + x * 3 + 2] = pImage8bit[y * iImageW * 3 + x * 3 + 2];
		}
	}

	// 3채널 -> 1채널로 변환
	cvCvtColor(srcImage, OriginImage, CV_RGB2GRAY);

	// 2진화
	cvThreshold(OriginImage, DilateImage, 100, 255, CV_THRESH_BINARY);

	// 반전
	cvNot(DilateImage, DilateImage);
	cvErode(DilateImage, DilateImage);

	cvCopy(DilateImage, BinImage);
	CvSeq *Contour = 0;

	//cvSaveImage("C:\\CenterPoint.bmp", DilateImage);

	CvMemStorage* CvStorage = cvCreateMemStorage(0);
	cvFindContours(DilateImage, CvStorage, &Contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	double dbOldDistance = 999999;

	int iCenterX = 0;
	int iCenterY = 0;

	CvRect rtTemp;
	CvRect rtFinal;

	for (; Contour != 0; Contour = Contour->h_next)
	{
		rtTemp = cvContourBoundingRect(Contour, 1);

		double dbCircularity = (4.0 * M_PI * cvContourArea(Contour, CV_WHOLE_SEQ)) / (cvArcLength(Contour, CV_WHOLE_SEQ, -1) * cvArcLength(Contour, CV_WHOLE_SEQ, -1));

		// 값이 작을 수록 찌그러진 것도 잡는다.
		if (dbCircularity > 0.5)
		{
			if (rtROI.left < rtTemp.x && rtROI.top < rtTemp.y && rtROI.right > rtTemp.x + rtTemp.width && rtROI.bottom > rtTemp.y + rtTemp.height)
			{
				iCenterX = rtTemp.x + rtTemp.width / 2;
				iCenterY = rtTemp.y + rtTemp.height / 2;

				if (rtTemp.width < iImageW / 2 && rtTemp.height < iImageH / 2 && rtTemp.width > 10 && rtTemp.height > 10)
				{
					double dbDistance = GetDistance(rtTemp.x + rtTemp.width / 2, rtTemp.y + rtTemp.height / 2, OriginImage->width / 2, OriginImage->height / 2);

					if (dbDistance < dbOldDistance)
					{
						ptCenterX = iCenterX;
						ptCenterY = iCenterY;
						dbOldDistance = dbDistance;
						rtFinal = rtTemp;
					}
				}
			}
		}
	}

	cvReleaseImage(&srcImage);
	cvReleaseImage(&OriginImage);
	cvReleaseImage(&DilateImage);
	cvReleaseMemStorage(&CvStorage);


	BYTE YTmp = 0;
	double AvgX = 0, AvgY = 0;
	int _cnt = 0;
	for (int j = rtFinal.y; j < rtFinal.y + rtFinal.height; j++)
	{
		for (int i = rtFinal.x; j < rtFinal.x + rtFinal.width; j++)
		{
			YTmp = BinImage->imageData[j*BinImage->widthStep + i];
			if (YTmp > 100)
			{
				AvgX += i;
				AvgY += j;
				_cnt++;
			}
		}
	}
	if (_cnt != 0)
	{
		AvgX /= _cnt;
		AvgY /= _cnt;
	}
	else
	{
		AvgX = 0;
		AvgY = 0;
	}
	ptCenterX = AvgX;
	ptCenterY = AvgY;


	double iOffset_x = ((int)ptCenterX - stOption.dCenterX) + stOption.dOffset[0];
	double iOffset_y = ((int)ptCenterY - stOption.dCenterY) + stOption.dOffset[1];
	double iResultOffset_x = 0;
	double iResultOffset_y = 0;

	// 광축 역상상태 - 광축 부호 변경.
	SetCamState_OC(stOption.nCamState, iOffset_x, iOffset_y, iResultOffset_x, iResultOffset_y);
	
	// 광축 결과 Convert
	iResultOffset_x *= 0.6;
	iResultOffset_y *= 0.6;

	// 0.25 단위로 반내림.
	int ibufOffsetX = (int)iResultOffset_x;
	int ibufOffsetY = (int)iResultOffset_y;

	double dbufOffsetX = iResultOffset_x - ibufOffsetX;
	double dbufOffsetY = iResultOffset_y - ibufOffsetY;

	if (dbufOffsetX < 0.25)	dbufOffsetX = 0;
	else if (dbufOffsetX > 0.25 && dbufOffsetX < 0.5) dbufOffsetX = 0.25;
	else if (dbufOffsetX > 0.5 && dbufOffsetX < 0.75) dbufOffsetX = 0.5;
	else if (dbufOffsetX > 0.75 && dbufOffsetX < 1) dbufOffsetX = 0.75;

	if (dbufOffsetY < 0.25)	dbufOffsetY = 0;
	else if (dbufOffsetY > 0.25 && dbufOffsetY < 0.5) dbufOffsetY = 0.25;
	else if (dbufOffsetY > 0.5 && dbufOffsetY < 0.75) dbufOffsetY = 0.5;
	else if (dbufOffsetY > 0.75 && dbufOffsetY < 1) dbufOffsetY = 0.75;

	stResult.dValue[0] = (double)ibufOffsetX + dbufOffsetX;
	stResult.dValue[1] = (double)ibufOffsetY + dbufOffsetY;

	// 판정
	for (int nidx = 0; nidx < OpticalCenter_Max; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		double	iMinDev = stOption.stSpecMin[nidx].dbValue;
		double	iMaxDev = stOption.stSpecMax[nidx].dbValue;
		stResult.bResult[nidx] = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.dValue[nidx]);

		if (stResult.bResult[nidx] == FALSE)
		{
			lReturn = RC_Err_TestFail;
		}
	}

	return lReturn;
}


LRESULT CTestManager_Test::Luri_Func_Shading(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Shading stOption, __out ST_Result_Shading& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	IplImage* srcImg = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 3);
	IplImage* GrayScaleImg = cvCreateImage(cvSize(iImageW, iImageH), IPL_DEPTH_8U, 1);

	for (int y = 0; y < iImageH; y++)
	{
		for (int x = 0; x < iImageW; x++)
		{
			srcImg->imageData[y * srcImg->widthStep + x * 3 + 0] = pImage8bit[y * iImageW * 3 + x * 3 + 0];
			srcImg->imageData[y * srcImg->widthStep + x * 3 + 1] = pImage8bit[y * iImageW * 3 + x * 3 + 1];
			srcImg->imageData[y * srcImg->widthStep + x * 3 + 2] = pImage8bit[y * iImageW * 3 + x * 3 + 2];
		}
	}

	cvCvtColor(srcImg, GrayScaleImg, CV_BGR2GRAY);

 	int iBlockSzW = stOption.iMaxROIWidth / 2;
 	int iBlockSzH = stOption.iMaxROIHeight / 2;
	int ileft = 0;
	int iright = 0;
	int itop = 0;
	int ibottom = 0;
	ileft = (iImageW / 2) - iBlockSzW;
	iright = (iImageW / 2) + iBlockSzW;
	itop = (iImageH / 2) - iBlockSzH;
	ibottom = (iImageH / 2) + iBlockSzH;
	BYTE val = 0;

 	// Center Y
	double dCenterY = 0;
	int iCount = 0;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dCenterY += (double)val;
			iCount++;
 		}
 	}
	dCenterY /= iCount;


	double dEdgeY_1 = 0;
	double dEdgeY_1_Dev = 0;
	double dEdgeY_2 = 0;
	double dEdgeY_2_Dev = 0;

	CvPoint LeftTop;
	CvPoint LeftBottom;
	CvPoint RightTop;
	CvPoint RightBottom;
	double dEdgeFieldY_1[4] = { 0, };

	// field 1 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	GetField_4Point(iImageW, iImageH, stOption.dTestField_1, LeftTop, LeftBottom, RightTop, RightBottom);

	// left top
	ileft = LeftTop.x - iBlockSzW;
	iright = LeftTop.x + iBlockSzW;
	itop = LeftTop.y - iBlockSzH;
	ibottom = LeftTop.y + iBlockSzH;
	iCount = 0;
	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[0] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[0] /= iCount;


	// left bottom
	ileft = LeftBottom.x - iBlockSzW;
	iright = LeftBottom.x + iBlockSzW;
	itop = LeftBottom.y - iBlockSzH;
	ibottom = LeftBottom.y + iBlockSzH;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[1] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[1] /= iCount;


	// right top
	ileft = RightTop.x - iBlockSzW;
	iright = RightTop.x + iBlockSzW;
	itop = RightTop.y - iBlockSzH;
	ibottom = RightTop.y + iBlockSzH;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[2] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[2] /= iCount;


	// right bottom
	ileft = RightTop.x - iBlockSzW;
	iright = RightTop.x + iBlockSzW;
	itop = RightTop.y - iBlockSzH;
	ibottom = RightTop.y + iBlockSzH;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[3] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[3] /= iCount;

	// 엣지 결과 1
	dEdgeY_1 = (dEdgeFieldY_1[0] + dEdgeFieldY_1[1] + dEdgeFieldY_1[2] + dEdgeFieldY_1[3]) / 4;

	// 표준 편차
	dEdgeY_1_Dev = 0;
	double dBuf = 0;
	for (int i = 0; i < 4; i++)
	{
		dBuf += pow(abs(dEdgeFieldY_1[i] - dEdgeY_1), 2);
	}
	dBuf /= 3;
	dEdgeY_1_Dev = sqrt(dBuf);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




	// field 2 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	GetField_4Point(iImageW, iImageH, stOption.dTestField_2, LeftTop, LeftBottom, RightTop, RightBottom);

	// left top
	ileft = LeftTop.x - iBlockSzW;
	iright = LeftTop.x + iBlockSzW;
	itop = LeftTop.y - iBlockSzH;
	ibottom = LeftTop.y + iBlockSzH;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[0] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[0] /= iCount;


	// left bottom
	ileft = LeftBottom.x - iBlockSzW;
	iright = LeftBottom.x + iBlockSzW;
	itop = LeftBottom.y - iBlockSzH;
	ibottom = LeftBottom.y + iBlockSzH;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[1] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[1] /= iCount;


	// right top
	ileft = RightTop.x - iBlockSzW;
	iright = RightTop.x + iBlockSzW;
	itop = RightTop.y - iBlockSzH;
	ibottom = RightTop.y + iBlockSzH;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[2] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[2] /= iCount;


	// right bottom
	ileft = RightTop.x - iBlockSzW;
	iright = RightTop.x + iBlockSzW;
	itop = RightTop.y - iBlockSzH;
	ibottom = RightTop.y + iBlockSzH;
	iCount = 0;

	for (int y = itop; y < ibottom; y++)
	{
		for (int x = ileft; x < iright; x++)
		{
			val = GrayScaleImg->imageData[y * GrayScaleImg->widthStep + x + 0];
			dEdgeFieldY_1[3] += (double)val;
			iCount++;
		}
	}
	dEdgeFieldY_1[3] /= iCount;

	// 엣지 결과 1
	dEdgeY_2 = (dEdgeFieldY_1[0] + dEdgeFieldY_1[1] + dEdgeFieldY_1[2] + dEdgeFieldY_1[3]) / 4;

	// 표준 편차
	dEdgeY_2_Dev = 0;
	dBuf = 0;
	for (int i = 0; i < 4; i++)
	{
		dBuf += pow(abs(dEdgeFieldY_1[i] - dEdgeY_2), 2);
	}
	dBuf /= 3;
	dEdgeY_2_Dev = sqrt(dBuf);
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// 중심 대비 퍼센트 계산.
	stResult.dValue[0] = 100.0 - (dEdgeY_1 / dCenterY * 100);
	stResult.dValue[1] = 100.0 - (dEdgeY_2 / dCenterY * 100);
	stResult.dValue[2] = (dEdgeY_1_Dev / dEdgeY_1 * 100);
	stResult.dValue[3] = (dEdgeY_2_Dev / dEdgeY_2 * 100);

//	stResult.dValue[0] = dCenterY - dEdgeY_1;
//	stResult.dValue[1] = dCenterY - dEdgeY_2;
//	stResult.dValue[2] = dEdgeY_1_Dev;
//	stResult.dValue[3] = dEdgeY_2_Dev;


	// 판정
	for (int nidx = 0; nidx < enUI_ShadingField_Max; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		double	iMinDev = stOption.stSpecMin[nidx].dbValue;
		double	iMaxDev = stOption.stSpecMax[nidx].dbValue;
		stResult.bResult[nidx] = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.dValue[nidx]);

		if (stResult.bResult[nidx] == FALSE)
		{
			lReturn = RC_Err_TestFail;
		}
	}

	cvReleaseImage(&srcImg);
	cvReleaseImage(&GrayScaleImg);

	return lReturn;
}

LRESULT CTestManager_Test::Luri_Func_Vision(__in LPBYTE pImageBuf, __in UINT nImageW, __in UINT nImageH, __in CString szMasterPath, __in ST_UI_Vision stOption, __out ST_Result_Vision& stResult, __out LPBYTE pDstImageBuf)
{
	if (1 > nImageW || 1 > nImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

	BOOL bResult = FALSE;

	// 광축 탐색에 필요한 옵션
	CvPoint ResultPoint;
	ResultPoint.x = 0;
	ResultPoint.y = 0;
	double Percent_min;
	double Percent_max;

	IplImage* srcImage = cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, 3);
	IplImage* GrayImage = cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, 1);
	IplImage* DstImage = cvCreateImage(cvSize(nImageW, nImageH), IPL_DEPTH_8U, 3);

	// LPByte -> IplImage
	CvtByte_IplImage(pImageBuf, srcImage);

	cvSaveImage("D:\\srcImage.bmp", srcImage);

	// Color -> Glay Cvt Color
	cvCvtColor(srcImage, GrayImage, CV_RGB2GRAY);
	cvCopy(srcImage, DstImage);

	// Master image Load : 여기다가 경로 맘대로 집어넣엉 
	IplImage* MasterIMG = cvLoadImage((CStringA)szMasterPath, CV_LOAD_IMAGE_GRAYSCALE);

	//cvSaveImage("D:\\MasterIMG.bmp", MasterIMG);


	if (MasterIMG->imageData != NULL)
	{

		IplImage* Matching_Result = cvCreateImage(cvSize(GrayImage->width - MasterIMG->width + 1, GrayImage->height - MasterIMG->height + 1), IPL_DEPTH_32F, 1);

		cvMatchTemplate(GrayImage, MasterIMG, Matching_Result, CV_TM_CCOEFF_NORMED);
		cvMinMaxLoc(Matching_Result, &Percent_min, &Percent_max, NULL, &ResultPoint);

		stResult.iCenterX[0] = ResultPoint.x + MasterIMG->width / 2;
		stResult.iCenterY[0] = ResultPoint.y + MasterIMG->height / 2;

		cvLine(DstImage, cvPoint(stResult.iCenterX[0], ResultPoint.y), cvPoint(stResult.iCenterX[0], ResultPoint.y + MasterIMG->width), CV_RGB(255, 150, 0), 3);
		cvLine(DstImage, cvPoint(ResultPoint.x, stResult.iCenterY[0]), cvPoint(ResultPoint.x + MasterIMG->height, stResult.iCenterY[0]), CV_RGB(255, 150, 0), 3);

		cvRectangle(DstImage, cvPoint(ResultPoint.x, ResultPoint.y), cvPoint(ResultPoint.x + MasterIMG->width, ResultPoint.y + MasterIMG->height), CV_RGB(0, 255, 0), 3);

		stResult.dbValue[0] = Percent_max * 100.0;
		
		cvReleaseImage(&Matching_Result);
		bResult = TRUE;

	}
	else
	{
		stResult.iCenterX[0] = 0;
		stResult.iCenterY[0] = 0;
		bResult = FALSE;
	}

	//cvSaveImage("D:\\DstImage.bmp", DstImage);

	// 결과 이미지 포워딩 용
// 	if (pDstImageBuf != NULL)
// 		CvtIplImage_Byte(DstImage, pDstImageBuf);


	// 판정
	for (int nidx = 0; nidx < Vision_Max; nidx++)
	{
		BOOL	bMinUse = stOption.stSpecMin[nidx].bEnable;
		BOOL	bMaxUse = stOption.stSpecMax[nidx].bEnable;
		double	iMinDev = stOption.stSpecMin[nidx].dbValue;
		double	iMaxDev = stOption.stSpecMax[nidx].dbValue;
		stResult.bResult[nidx] = Get_MeasurmentData(bMinUse, bMaxUse, iMinDev, iMaxDev, stResult.dbValue[nidx]);

		if (stResult.bResult[nidx] == FALSE)
		{
			lReturn = RC_Err_TestFail;
		}
	}

	cvReleaseImage(&MasterIMG);
	cvReleaseImage(&srcImage);
	cvReleaseImage(&GrayImage);
	cvReleaseImage(&DstImage);

	return bResult;
}

LRESULT CTestManager_Test::Luri_Func_IIC(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in LPBYTE pMasterImage, __out ST_Result_IIC& stResult)
{
	if (1 > iImageW || 1 > iImageH)
		return RC_Invalid_Handle;

	LRESULT	lReturn = RC_OK;

// 	int iPixelMatchCount = 0;
// 	for (int y = 0; y < iImageH; y++)
// 	{
// 		for (int x = 0; x < iImageW; x++)
// 		{
// 			if (pImage8bit[y * iImageW * 3 + x * 3 + 0] == pMasterImage[y * iImageW * 3 + x * 3 + 0]
// 				&& pImage8bit[y * iImageW * 3 + x * 3 + 1] == pMasterImage[y * iImageW * 3 + x * 3 + 1]
// 				&& pImage8bit[y * iImageW * 3 + x * 3 + 2] == pMasterImage[y * iImageW * 3 + x * 3 + 2])
// 			{
// 				iPixelMatchCount++;
// 			}
// 		}
// 	}

//	if (iPixelMatchCount == iImageH * iImageW)
	{
		stResult.bResult[0] = TRUE;
		stResult.dbValue[0] = 100;
	}
// 	else
// 	{
// 		stResult.bResult[0] = FALSE;
// 		stResult.dbValue[0] = 0;
// 	}

	if (stResult.bResult[0] == FALSE)
	{
		lReturn = RC_Err_TestFail;
	}

	return lReturn;
}


//LRESULT CTestManager_Test::Luri_Func_Displace(__in CString szBarcode, __in ST_UI_Displace stOption, __out ST_Result_Displace& stResult)
//{
//	LRESULT	lReturn = RC_OK;
//
//	// text 파일에서 데이터 읽어오기 (외부 변위 x,y)
//	CString szTiltx;
//	CString szTilty;
//	GetTextFileData_FindBarcode(szBarcode, 5, szTiltx);
//	GetTextFileData_FindBarcode(szBarcode, 6, szTilty);
//
//	double dAATiltx = _ttof(szTiltx);
//	double dAATilty = _ttof(szTilty);
//
//	// 조정 성공이면..
//	{
//		stResult.dbValue[0] = dAATiltx;
//		stResult.dbValue[1] = dAATilty;
//	}
//
//
//	return lReturn;
//}

//=============================================================================
// Method		: Get_MeasurmentData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bMinUse
// Parameter	: __in BOOL bMaxUse
// Parameter	: __in double dbMinSpec
// Parameter	: __in double dbMaxSpec
// Parameter	: __in double dbValue
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
BOOL CTestManager_Test::Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in double dbMinSpec, __in double dbMaxSpec, __in double dbValue)
{
	BOOL bMinResult = TRUE;
	BOOL bMaxResult = TRUE;

	if (TRUE == bMinUse)
	{
		bMinResult = (dbMinSpec <= dbValue) ? TRUE : FALSE;
	}

	if (TRUE == bMaxUse)
	{
		bMaxResult = (dbMaxSpec >= dbValue) ? TRUE : FALSE;
	}

	return bMinResult & bMaxResult;
}

//=============================================================================
// Method		: Get_MeasurmentData
// Access		: public  
// Returns		: BOOL
// Parameter	: __in BOOL bMinUse
// Parameter	: __in BOOL bMaxUse
// Parameter	: __in int iMinSpec
// Parameter	: __in int iMaxSpec
// Parameter	: __in int iValue
// Qualifier	:
// Last Update	: 2018/7/30 - 10:25
// Desc.		:
//=============================================================================
BOOL CTestManager_Test::Get_MeasurmentData(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec, __in int iValue)
{
	BOOL bMinResult = TRUE;
	BOOL bMaxResult = TRUE;

	if (TRUE == bMinUse)
	{
		bMinResult = (iMinSpec <= iValue) ? TRUE : FALSE;
	}

	if (TRUE == bMaxUse)
	{
		bMaxResult = (iMaxSpec >= iValue) ? TRUE : FALSE;
	}

	return bMinResult & bMaxResult;
}

//=============================================================================
// Method		: LURI_Func_DetectROI_Ver1
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in LPBYTE pImageBuf
// Parameter	: __in int iImageW
// Parameter	: __in int iImageH
// Parameter	: __in CRect rtOption
// Parameter	: __out CRect & rtResult 
// Qualifier	:
// Last Update	: 2018/8/9 - 9:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_Test::LURI_Func_DetectROI_Ver1(__in LPBYTE pImageBuf, __in int iImageW, __in int iImageH, __in CRect rtOption, __out CRect &rtResult)
{
	int iHulfW = iImageW / 2;
	int iHulfH = iImageH / 2;

	// 영상 사이즈에 따라 영역 변경하여 사용 (전체 이미지에서 선정)
	int iFindW = iImageW / 5;
	int iFindH = iImageH / 5;

	// 영역 설정
	CRect rtROI;

	rtROI.left		= iHulfW - iFindW;
	rtROI.right		= iHulfW + iFindW;
	rtROI.top		= iHulfH - iFindH;
	rtROI.bottom	= iHulfH + iFindH;

	int iBright		= 0;
	int iColor		= MarkCol_Black;
	int nSharpness	= 0;

	int iCenterX	= 0;
	int iCenterY	= 0;

	if (FALSE == m_LT_CircleDetect.CircleDetect_Test(pImageBuf, iImageW, iImageH, rtROI, iBright, iCenterX, iCenterY, iColor, nSharpness))
	{
		return RC_Detect;
	}

	// ROI Offset
	int iOffsetX = iCenterX - iHulfW;
	int iOffsetY = iCenterY - iHulfH;

	rtResult.left	= rtOption.left		+ iOffsetX;
	rtResult.right	= rtOption.right	+ iOffsetX;
	rtResult.top	= rtOption.top		+ iOffsetY;
	rtResult.bottom = rtOption.bottom	+ iOffsetY;

	if (rtResult.left < 0 || rtResult.left > iImageW)
		return RC_Detect;

	if (rtResult.right < 0 || rtResult.right > iImageW)
		return RC_Detect;

	if (rtResult.top < 0 || rtResult.top > iImageW)
		return RC_Detect;

	if (rtResult.bottom < 0 || rtResult.bottom > iImageW)
		return RC_Detect;

	return RC_OK;
}

void CTestManager_Test::OnTestProcess_SFRROI(__in IplImage* pFrameImage, __in ST_UI_SFR stOpt, __out ST_Result_SFR &stTestData)
{
//	int AutoROI_Thres = stOpt.nDetectThr;
	int AutoROI_Thres = 0;
	if (pFrameImage == NULL)
		return;

	CvMemStorage* pContourStorage = cvCreateMemStorage(0);
	CvSeq *pContour = 0;
	CvSeq *pContour_Center = 0;
	IplImage* pSrcImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pBinaryImage = cvCreateImage(cvGetSize(pFrameImage), IPL_DEPTH_8U, 1);
	IplImage* pContoresImage = NULL;
	IplImage* eig_image = NULL;
	IplImage* temp_image = NULL;

	// 광축을 구해서 Offset 추가
	CvPoint Offset_for_Auto;
	Offset_for_Auto.x = 0;
	Offset_for_Auto.y = 0;

	cvCvtColor(pFrameImage, pSrcImage, CV_RGB2GRAY);

	//cvThreshold(pSrcImage, pBinaryImage, 0, 255, CV_THRESH_OTSU); // 20180305 HTH Modify
	cvAdaptiveThreshold(pSrcImage, pBinaryImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY, 201, 5);

	cvNot(pBinaryImage, pBinaryImage);
	cvRectangle(pBinaryImage, cvPoint(0, 0), cvPoint(pBinaryImage->width - 3, pBinaryImage->height - 3), CV_RGB(0, 0, 0), 6);
	cvFindContours(pBinaryImage, pContourStorage, &pContour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	//cvFindContours(pBinaryImage, pContourStorage, &pContour_Center, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);

	pContour_Center = pContour;

	//광축 구하기
	CvRect rect;
	double area = 0, arcCount = 0;
	CvPoint nCenterPoint;

	nCenterPoint.x = 0;
	nCenterPoint.y = 0;
	double dbMin_Dist = 99999;
	double dbDist = 0;
	double circularity = 0;
	int rtCenterX = 0;
	int rtCenterY = 0;
	CvPoint StartPoint;
	CvPoint EndPoint;

	int iStartX = pSrcImage->width / 4;
	int iStartY = pSrcImage->height / 4;

	int iEndX = pSrcImage->width * 3 / 4;
	int iEndY = pSrcImage->height * 3 / 4;

	int Conc_c = 0;

	for (; pContour_Center != 0; pContour_Center = pContour_Center->h_next)
	{
		rect = cvContourBoundingRect(pContour_Center, 1);
		area = cvContourArea(pContour_Center, CV_WHOLE_SEQ);
		arcCount = cvArcLength(pContour_Center, CV_WHOLE_SEQ, -1);
		circularity = (4.0 * 3.14 * area) / (arcCount*arcCount);

		rtCenterX = rect.x + rect.width / 2;
		rtCenterY = rect.y + rect.height / 2;

		StartPoint.x = rect.x;
		StartPoint.y = rect.y;

		EndPoint.x = rect.x + rect.width;
		EndPoint.y = rect.y + rect.height;

		if (rect.x > iStartX && rect.x + rect.width < iEndX
			&& rect.y > iStartY && rect.y + rect.height < iEndY)
		{
			dbDist = GetDistance(pSrcImage->width / 2, pSrcImage->height / 2, rtCenterX, rtCenterY);

			//원에 가깝고
			if (circularity > 0.3)
			{
				// 이미지 중심에서 제일 가까운 오브젝트 탐색
				if (dbMin_Dist > dbDist)
				{
					//cvRectangleR(pSrcImage, rect, CV_RGB(255, 255, 255), 3);

					dbMin_Dist = dbDist;

					nCenterPoint.x = rtCenterX;
					nCenterPoint.y = rtCenterY;

					StartPoint.x = nCenterPoint.x - rect.width / 2;
					StartPoint.y = nCenterPoint.y - rect.height / 2;

					EndPoint.x = nCenterPoint.x + rect.width / 2;
					EndPoint.y = nCenterPoint.y + rect.height / 2;

				}
			}
		}
		Conc_c++;
	}

	if (pSrcImage->width < nCenterPoint.x || 0 >= nCenterPoint.x)
	{
		nCenterPoint.x = pSrcImage->width / 2;
	}

	if (pSrcImage->height < nCenterPoint.y || 0 >= nCenterPoint.y)
	{
		nCenterPoint.y = pSrcImage->height / 2;
	}

	Offset_for_Auto.x = nCenterPoint.x - (pSrcImage->width / 2);
	Offset_for_Auto.y = nCenterPoint.y - (pSrcImage->height / 2);

	// 샤프니스 확인 후 자동 ROI 진입
	BOOL bAutoROI = FALSE;

	IplImage* LP_in = cvCreateImage(cvGetSize(pSrcImage), IPL_DEPTH_8U, 1);
	IplImage* LP_OUT = cvCreateImage(cvGetSize(pSrcImage), IPL_DEPTH_16S, 1);

	cvCopy(pSrcImage, LP_in);
	cvLaplace(LP_in, LP_OUT, 1);

	int MaxLPVal = -999;
	CvScalar Tmp;
	for (int j = 0; j < LP_OUT->height; j++)
	{
		for (int i = 0; i < LP_OUT->width; i++)
		{
			Tmp = cvGet2D(LP_OUT, j, i);
			if (MaxLPVal < Tmp.val[0])
			{
				MaxLPVal = (int)Tmp.val[0];
			}
		}
	}





	cvReleaseImage(&LP_in);
	cvReleaseImage(&LP_OUT);
	if (MaxLPVal >= AutoROI_Thres)
		bAutoROI = TRUE;
	else
		bAutoROI = FALSE;



	if (bAutoROI == FALSE || Conc_c == 0)
	{
		for (int i = 0; i < ROI_SFR_Max; i++)
		{
			stTestData.rtROI[i].left = stOpt.stInput[i].rtROI.left + Offset_for_Auto.x;
			stTestData.rtROI[i].top = stOpt.stInput[i].rtROI.top + Offset_for_Auto.y;
			stTestData.rtROI[i].right = stOpt.stInput[i].rtROI.right + Offset_for_Auto.x;
			stTestData.rtROI[i].bottom = stOpt.stInput[i].rtROI.bottom + Offset_for_Auto.y;
		}

		stTestData.cCenterPoint.x = 0;
		stTestData.cCenterPoint.y = 0;
	}
	else
	{
		stTestData.cCenterPoint.x = nCenterPoint.x;
		stTestData.cCenterPoint.y = nCenterPoint.y;
		// ROI 마다 자동으로 찾아버리기
		CvRect rcContour;
		CRect rtROI[ROI_SFR_Max];
		
		for (int i = 0; i < ROI_SFR_Max; i++)
		{
			CvPoint Corr_Center;
			if (stOpt.stInput[i].bEnable)
			{
				int _W = stOpt.stInput[i].rtROI.right - stOpt.stInput[i].rtROI.left;
				int _H = stOpt.stInput[i].rtROI.bottom - stOpt.stInput[i].rtROI.top;

				rtROI[i] = stOpt.stInput[i].rtROI;
				CvPoint ROICenterPoint;
				ROICenterPoint.x = (rtROI[i].left + rtROI[i].right) / 2 + Offset_for_Auto.x;
				ROICenterPoint.y = (rtROI[i].top + rtROI[i].bottom) / 2 + Offset_for_Auto.y;
				// ROI에 근접한 Contours 구하기
				double Old_Dist = 99999;
				double Max_Size = 0;
				CvSeq *pContour_Tmp = pContour;
				CvRect Near_ContP;
				BOOL TRUE_Flag = FALSE;
				for (; pContour_Tmp != 0; pContour_Tmp = pContour_Tmp->h_next)
				{
					rcContour = cvBoundingRect(pContour_Tmp, 1);
					double Size_Rect = rcContour.width * rcContour.height;
					double Dist_Contour2ROI = GetDistance(rcContour.x + (rcContour.width / 2), rcContour.y + (rcContour.height / 2), ROICenterPoint.x, ROICenterPoint.y);
					if (rcContour.height > _H && rcContour.width > _W)
					{
						if (Old_Dist > Dist_Contour2ROI && rcContour.width < pFrameImage->width / 2 && rcContour.height < pFrameImage->height / 2)
						{

							Old_Dist = Dist_Contour2ROI;
							Near_ContP.x = rcContour.x - 5; // 여유 +-5 pixel
							Near_ContP.y = rcContour.y - 5; // 여유 +-5 pixel
							Near_ContP.width = rcContour.width + 10;
							Near_ContP.height = rcContour.height + 10;
							TRUE_Flag = TRUE;
						}
					}

				}
				if (TRUE_Flag == FALSE)
				{
					TRACE(_T("Near_Point가 없습니다.\n"));
					cvReleaseMemStorage(&pContourStorage);
					cvReleaseImage(&pSrcImage);
					cvReleaseImage(&pBinaryImage);

					stTestData.rtROI[i].left = stOpt.stInput[i].rtROI.left;
					stTestData.rtROI[i].top = stOpt.stInput[i].rtROI.top;
					stTestData.rtROI[i].right = stOpt.stInput[i].rtROI.right;
					stTestData.rtROI[i].bottom = stOpt.stInput[i].rtROI.bottom;


				}
				else{
					// 근접한 Contours 이미지 따기
					pContoresImage = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_8U, 1);
					eig_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
					temp_image = cvCreateImage(cvSize(Near_ContP.width, Near_ContP.height), IPL_DEPTH_32F, 1);
					if (Near_ContP.x < 0)
						Near_ContP.x = 0;
					if (Near_ContP.y < 0)
						Near_ContP.y = 0;
					if (Near_ContP.x + Near_ContP.width > pFrameImage->width - 1)
						Near_ContP.x = pFrameImage->width - Near_ContP.width - 1;
					if (Near_ContP.y + Near_ContP.height > pFrameImage->height - 1)
						Near_ContP.y = pFrameImage->height - Near_ContP.height - 1;
					cvSetImageROI(pSrcImage, Near_ContP);
					cvCopy(pSrcImage, pContoresImage);
					cvResetImageROI(pSrcImage);
					// 스무딩 처리
					cvDilate(pContoresImage, pContoresImage, 0, 3);
					cvErode(pContoresImage, pContoresImage, 0, 3);
					//cvSmooth(pContoresImage, pContoresImage);
					cvNormalize(pContoresImage, pContoresImage, 0, 255, CV_MINMAX);
					cvDilate(pContoresImage, pContoresImage, 0, 3);
					cvErode(pContoresImage, pContoresImage, 0, 3);

					int MAX_CORNERS = 2000;
					CvPoint2D32f* corners = new CvPoint2D32f[MAX_CORNERS];
					CvPoint2D32f* corners_Final = new CvPoint2D32f[MAX_CORNERS];

					int Corners_Cnt = 0;
					// 코너 찾기
					cvGoodFeaturesToTrack(pContoresImage, eig_image, temp_image, corners, &MAX_CORNERS, 0.14, 20.0, 0, 5, 0, 0.04);
					for (int j = 0; j < MAX_CORNERS; j++)
					{
						if (!(corners[j].x > pContoresImage->width *(1.0 / 3) && corners[j].x < pContoresImage->width*(2.0 / 3) &&
							corners[j].y > pContoresImage->height*(1.0 / 3) && corners[j].y < pContoresImage->height*(2.0 / 3) ))
						{
							corners_Final[Corners_Cnt] = corners[j];
							cvCircle(pContoresImage, cvPoint((int)corners[j].x, (int)corners[j].y), 4, CV_RGB(255, 255, 255), 1);
							Corners_Cnt++;
						}
					}

					// 월드좌표 변환
					for (int j = 0; j < Corners_Cnt; j++)
					{
						corners_Final[j].x += Near_ContP.x;
						corners_Final[j].y += Near_ContP.y;
					}
					double Dist_1st = 0.0;
					CvPoint Dist_1stPoint;
					double Dist_2rd = 0.0;
					CvPoint Dist_2rdPoint;

					// W가 큰 ROI 일때 (가로SFR) => X축, Y축에 대한 거리 추출 가중치가 달라짐
					if (rtROI[i].Width() > rtROI[i].Height())
					{
						// 1st
						double Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
							Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y /*- (float)Pointy_Mul*/));
							if (Max_Tmp > Dist_1st)
							{
								Max_Tmp = Dist_1st;
								Dist_1stPoint.x = (int)corners_Final[k].x;
								Dist_1stPoint.y = (int)corners_Final[k].y;
							}
						}
						// 2rd
						Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							// 1st Point 를 제외한 가장 가까운 Point = 2rd
							if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].y < Dist_1stPoint.y + _H && corners_Final[k].y > Dist_1stPoint.y - _H))
							{
								// 가중치를 적용한 거리 계산
								int Pointy_Mul = (int)((corners_Final[k].y - (float)ROICenterPoint.y) * 20);
								Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)corners_Final[k].x, (int)(corners_Final[k].y) /*- (float)Pointy_Mul)*/);
								if (Max_Tmp > Dist_2rd)
								{
									Max_Tmp = Dist_2rd;
									Dist_2rdPoint.x = (int)corners_Final[k].x;
									Dist_2rdPoint.y = (int)corners_Final[k].y;
								}
							}
						}
					}
					// H가 큰 ROI 일때 (세로SFR)
					else if (rtROI[i].Width() < rtROI[i].Height())
					{
						// 1st
						double Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
							Dist_1st = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x)/*+ (float)Pointy_Mul)*/, (int)corners_Final[k].y);
							if (Max_Tmp > Dist_1st)
							{
								Max_Tmp = Dist_1st;
								Dist_1stPoint.x = (int)corners_Final[k].x;
								Dist_1stPoint.y = (int)corners_Final[k].y;
							}
						}
						// 2rd
						Max_Tmp = 99999;
						for (int k = 0; k < Corners_Cnt; k++)
						{
							// 1st Point 를 제외한 가장 가까운 Point = 2rd
							if (corners_Final[k].x != Dist_1stPoint.x && corners_Final[k].y != Dist_1stPoint.y && (corners_Final[k].x <= Dist_1stPoint.x + _W && corners_Final[k].x >= Dist_1stPoint.x - _W))
							{
								// 가중치를 적용한 거리 계산
								int Pointy_Mul = (int)((corners_Final[k].x - (float)ROICenterPoint.x) * 20);
								Dist_2rd = GetDistance(ROICenterPoint.x, ROICenterPoint.y, (int)(corners_Final[k].x /*+ Pointy_Mul*/), (int)corners_Final[k].y);
								if (Max_Tmp > Dist_2rd)
								{
									Max_Tmp = Dist_2rd;
									Dist_2rdPoint.x = (int)corners_Final[k].x;
									Dist_2rdPoint.y = (int)corners_Final[k].y;
								}
							}
						}
					}
					Corr_Center.x = (int)((Dist_1stPoint.x + Dist_2rdPoint.x) / 2.0);
					Corr_Center.y = (int)((Dist_1stPoint.y + Dist_2rdPoint.y) / 2.0);
					//cvCircle(pContoresImage, Corr_Center, 4, CV_RGB(255, 255, 255), 1);

					// 보정값 넣기
					stTestData.rtROI[i].left = Corr_Center.x - (_W / 2);
					stTestData.rtROI[i].top = Corr_Center.y - (_H / 2);
					stTestData.rtROI[i].right = stTestData.rtROI[i].left + _W;
					stTestData.rtROI[i].bottom = stTestData.rtROI[i].top + _H;

					delete[] corners;
					delete[] corners_Final;

					cvReleaseImage(&eig_image);
					cvReleaseImage(&temp_image);
					cvReleaseImage(&pContoresImage);
					//cvRectangle(pSrcImage, cvPoint(stTestData.rtTestPicROI[i].left, stTestData.rtTestPicROI[i].top), cvPoint(stTestData.rtTestPicROI[i].right, stTestData.rtTestPicROI[i].bottom),CV_RGB(200,200,200),2 );

				}

			}
		}
	}

	cvReleaseMemStorage(&pContourStorage);
	cvReleaseImage(&pSrcImage);
	cvReleaseImage(&pBinaryImage);
}

//=============================================================================
// Method		: GetDistance
// Access		: protected  
// Returns		: double
// Parameter	: __in int x1
// Parameter	: __in int y1
// Parameter	: __in int x2
// Parameter	: __in int y2
// Qualifier	:
// Last Update	: 2018/8/11 - 14:50
// Desc.		:
//=============================================================================
double CTestManager_Test::GetDistance(__in int x1, __in int y1, __in int x2, __in int y2)
{
	double result;

	result = sqrt((double)((x2 - x1)*(x2 - x1)) + ((y2 - y1)*(y2 - y1)));

	return result;
}

//=============================================================================
// Method		: Set_Shading_ROI_Array
// Access		: protected  
// Returns		: void
// Parameter	: __in ST_UI_Shading & stOption
// Parameter	: __out double * pdHorThres
// Parameter	: __out double * pdVerThres
// Parameter	: __out double * pdDiaThres
// Parameter	: __out POINT * ppHorizonPoint
// Parameter	: __out POINT * ppVerticalPoint
// Parameter	: __out POINT * ppDiaAPoint
// Parameter	: __out POINT * ppDiaBPoint
// Qualifier	:
// Last Update	: 2018/8/11 - 14:48
// Desc.		:
//=============================================================================
void CTestManager_Test::Set_Shading_ROI_Array(__in ST_UI_Shading & stOption, __out double * pdHorThres, __out double * pdVerThres, __out double * pdDiaThres, __out POINT * ppHorizonPoint, __out POINT * ppVerticalPoint, __out POINT * ppDiaAPoint, __out POINT * ppDiaBPoint)
{
// 	for (int iROI = 0; iROI < stOption.iMaxROICount; iROI++)
// 	{
// 		ppHorizonPoint[iROI].x = stOption.stInputHor[iROI].ptROI.x;
// 		ppHorizonPoint[iROI].y = stOption.stInputHor[iROI].ptROI.y;
// 
// 		ppVerticalPoint[iROI].x = stOption.stInputVer[iROI].ptROI.x;
// 		ppVerticalPoint[iROI].y = stOption.stInputVer[iROI].ptROI.y;
// 
// 		ppDiaAPoint[iROI].x = stOption.stInputDirA[iROI].ptROI.x;
// 		ppDiaAPoint[iROI].y = stOption.stInputDirA[iROI].ptROI.y;
// 
// 		ppDiaBPoint[iROI].x = stOption.stInputDirB[iROI].ptROI.x;
// 		ppDiaBPoint[iROI].y = stOption.stInputDirB[iROI].ptROI.y;
// 
// 		pdHorThres[iROI] = stOption.stInputHor[iROI].dbThreshold;
// 		pdVerThres[iROI] = stOption.stInputVer[iROI].dbThreshold;
// 		pdDiaThres[iROI] = stOption.stInputDirA[iROI].dbThreshold;
// 	}
}

//=============================================================================
// Method		: Set_Dinamic_ROI_Array
// Access		: protected  
// Returns		: void
// Parameter	: __in ST_UI_DynamicBW & stOption
// Parameter	: __out POINT * ppDinamicBWPoint
// Qualifier	:
// Last Update	: 2018/8/11 - 14:47
// Desc.		:
//=============================================================================
void CTestManager_Test::Set_Dinamic_ROI_Array(__in ST_UI_DynamicBW & stOption, __out POINT * ppDinamicBWPoint)
{
	for (int iROI = 0; iROI < stOption.iMaxROICount; iROI++)
	{
		ppDinamicBWPoint[iROI].x = stOption.stInput[iROI].ptROI.x;    //상훈씨
		ppDinamicBWPoint[iROI].y = stOption.stInput[iROI].ptROI.y;
	}
}


void CTestManager_Test::CvtByte_IplImage(__in LPBYTE pImageBuf, __out IplImage* dstImage)
{
	for (int j = 0; j < dstImage->height; j++)
	{
		for (int i = 0; i < dstImage->width; i++)
		{
			dstImage->imageData[j*dstImage->widthStep + (i * 3) + 0] = pImageBuf[(j*dstImage->width * 4) + (i * 4) + 0];
			dstImage->imageData[j*dstImage->widthStep + (i * 3) + 1] = pImageBuf[(j*dstImage->width * 4) + (i * 4) + 1];
			dstImage->imageData[j*dstImage->widthStep + (i * 3) + 2] = pImageBuf[(j*dstImage->width * 4) + (i * 4) + 2];
		}
	}
}


void CTestManager_Test::CvtIplImage_Byte(__in IplImage* srcImage, __out LPBYTE& pImageBuf)
{
	for (int j = 0; j < srcImage->height; j++)
	{
		for (int i = 0; i < srcImage->width; i++)
		{
			pImageBuf[(j*srcImage->width * 4) + (i * 4) + 0] = srcImage->imageData[j*srcImage->widthStep + (i * 3) + 0];
			pImageBuf[(j*srcImage->width * 4) + (i * 4) + 1] = srcImage->imageData[j*srcImage->widthStep + (i * 3) + 1];
			pImageBuf[(j*srcImage->width * 4) + (i * 4) + 2] = srcImage->imageData[j*srcImage->widthStep + (i * 3) + 2];
		}
	}
}



bool CTestManager_Test::RCCC2CCCC(IplImage *src, IplImage *dst, int nType) //
{
	int nDepth = src->depth / 8;
	double sum = 0;
	int Cnt = 0;
	int Index = 0;
	int Data = 0, DataH = 0, DataL = 0;
	int Ref_X = 0, Ref_Y = 0;

	switch (nType){
	case RCCC:
		Ref_X = 0; Ref_Y = 0;  // R의 위치는 ==> 짝수, 짝수		
		break;
	case CRCC:
		Ref_X = 1; Ref_Y = 0;  // R의 위치는 ==> 홀수, 짝수
		break;
	case CCRC:
		Ref_X = 0; Ref_Y = 1;  // R의 위치는 ==> 짝수, 홀수
		break;
	case CCCR:
		Ref_X = 1; Ref_Y = 1;  // R의 위치는 ==> 홀수, 홀수
		break;
	}
	for (int y = 0; y < src->height; y++){
		for (int x = 0; x < src->width; x++){
			if (x % 2 == Ref_X && y % 2 == Ref_Y){
				sum = 0.0; Cnt = 0;
				if (x != 0){
					DataH = src->imageData[y*src->widthStep + (x - 1) * nDepth + 1] << 8;
					DataL = src->imageData[y*src->widthStep + (x - 1) * nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}
				if (y != 0){
					DataH = src->imageData[(y - 1)*src->widthStep + (x)* nDepth + 1] << 8;
					DataL = src->imageData[(y - 1)*src->widthStep + (x)* nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}
				if (x != (src->width - 1)){
					DataH = src->imageData[y*src->widthStep + (x + 1) * nDepth + 1] << 8;
					DataL = src->imageData[y*src->widthStep + (x + 1) * nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}
				if (y != (src->height - 1)){
					DataH = src->imageData[(y + 1)*src->widthStep + (x)* nDepth + 1] << 8;
					DataL = src->imageData[(y + 1)*src->widthStep + (x)* nDepth + 0] & 0xff;
					sum += (DataH + DataL);
					Cnt++;
				}

				if (Cnt != 0) {
					Data = sum / (double)Cnt;
					dst->imageData[y*src->widthStep + x * nDepth] = (unsigned char)(Data & 0x00ff);
					dst->imageData[y*src->widthStep + x * nDepth + 1] = (unsigned char)((Data >> 8) & 0x00ff);
				}
				else {
					dst->imageData[y*src->widthStep + x * nDepth] = 0;
					dst->imageData[y*src->widthStep + x * nDepth + 1] = 0;
				}

			}
			else{
				for (int k = 0; k < nDepth; k++){
					dst->imageData[y*dst->widthStep + x*nDepth + k] = src->imageData[y*src->widthStep + x * nDepth + k];
				}
			}
		}
	}
	return true;
}


void CTestManager_Test::GetField_4Point(__in int iWidth, int iHeight, double dbField, __out CvPoint& Left_Top, CvPoint& Left_Bottom, CvPoint& Right_Top, CvPoint& Right_Bottom)
{
	double FieldDistance = (dbField / 1.0) * sqrt(((iWidth / 2.0)*(iWidth / 2.0)) + ((iHeight / 2.0)*(iHeight / 2.0)));

	//Theta
	double Theta_RB = atan((iHeight / 2.0) / (iWidth / 2.0)) * (180 / 3.141592);  // 기준 Theta

	double Theta_LT = (180 + Theta_RB);
	double Theta_LB = (180 - Theta_RB);
	double Theta_RT = (360 - Theta_RB);


	// RB_Field Point
	Right_Bottom.x = FieldDistance * cos(Theta_RB*(3.141592 / 180)) + iWidth / 2.0;
	Right_Bottom.y = FieldDistance * sin(Theta_RB*(3.141592 / 180)) + iHeight / 2.0;

	// RT_Field Point
	Right_Top.x = FieldDistance * cos(Theta_RT* 3.141592 / 180) + iWidth / 2.0;
	Right_Top.y = FieldDistance * sin(Theta_RT* 3.141592 / 180) + iHeight / 2.0;

	// LT_Field Point
	Left_Top.x = FieldDistance * cos(Theta_LT* 3.141592 / 180) + iWidth / 2.0;
	Left_Top.y = FieldDistance * sin(Theta_LT* 3.141592 / 180) + iHeight / 2.0;

	// LB_Field Point
	Left_Bottom.x = FieldDistance * cos(Theta_LB* 3.141592 / 180) + iWidth / 2.0;
	Left_Bottom.y = FieldDistance * sin(Theta_LB* 3.141592 / 180) + iHeight / 2.0;

}

void CTestManager_Test::GetTiltXY_4PointInput(__in double* x, __in double* y, __in double* height, __out double& tiltx, __out double& tilty)
{
	// tilt x
 	//double Dist_x = (double)(x[1] - x[0]);// *1000;
 	// 	double dheight = height[1] - height[0];
 	// 	double AngleX[2] = { 0, };
 	// 
 	// 	AngleX[0] = atan(dheight / Dist_x);
 	// 
 	//	Dist_x = (double)(x[2] - x[3]);// *1000;
 	// 	dheight = height[2] - height[3];
 	// 	AngleX[1] = atan(dheight / Dist_x);
 	// 
 	// 	double AngleTotal_X = (AngleX[0] + AngleX[1]) / 2.0;
 	// 
 	// 	// tilt y
 	//	double Dist_y = (double)(y[3] - y[0]);// *1000;
 	// 	dheight = height[3] - height[0];
 	// 	double AngleY[2];
 	// 	AngleY[0] = atan(dheight / Dist_y);
 	// 
 	//	Dist_y = (double)(y[2] - y[1]);// *1000;
 	// 	dheight = height[2] - height[1];
 	// 	AngleY[1] = atan(dheight / Dist_y);
 	// 
 	// 	double AngleTotal_Y = (AngleY[0] + AngleY[1]) / 2.0;
 	// 
 	// 	//tiltx = AngleTotal_X;
 	//tiltx = (AngleTotal_X * 180 / M_PI);
 	// 	//tilty = AngleTotal_Y;
 	//tilty = (AngleTotal_Y * 180 / M_PI);


  	double dLength[4] = { 0, };
  	dLength[0] = abs(y[0] - y[3]);
  	dLength[1] = abs(x[0] - x[1]);
  	dLength[2] = abs(y[1] - y[2]);
  	dLength[3] = abs(x[2] - x[3]);
  
  	double dAvgTop = 0;
  	double dAvgBottom = 0;
  	double dAvgLeft = 0;
  	double dAvgRight = 0;
  
  	dAvgTop = (height[0] + height[1]) / 2;
  	dAvgBottom = (height[2] + height[3]) / 2;
  	dAvgLeft = (height[3] + height[0]) / 2;
  	dAvgRight = (height[1] + height[2]) / 2;
  
  	double dWidth = 0;
  	double dHeight = 0;
  	double dRadian = 0;
  
  	CString szBuf;
  
  	// Tx
  	dWidth = dLength[1];
  	dHeight = dAvgRight - dAvgLeft;
  	dRadian = atan(dHeight / dWidth);
  	tiltx = (dRadian * 180 / M_PI);
  	//szBuf.Format(_T("%.2f"), dBufftiltx);
  
  	//tiltx = _ttof(szBuf);
  
  	// Ty
  	dWidth = dLength[0];
  	dHeight = dAvgBottom - dAvgTop;
  	dRadian = atan(dHeight / dWidth);
  	tilty = (dRadian * 180 / M_PI);
 	//szBuf.Format(_T("%.2f"), dBufftilty);
 
 	//tilty = _ttof(szBuf);

}

BOOL CTestManager_Test::GetTextFileData_FindBarcode(__in CString szBarcode, __in int iColumn, __out CString& szOutData)
{
	CStdioFile file;
	CFileException e;

	CString szFilePath = _T("D:\\AABonder.txt");

	if (!file.Open(szFilePath, CFile::modeRead, &e))
	{
		return FALSE;
	}

	CString szText;
	CString szSubText;
	BOOL bFind = FALSE;
	while (file.ReadString(szText))
	{
		if (szText.Find(szBarcode) >= 0)
		{
			AfxExtractSubString(szSubText, szText, iColumn, _T(','));
			szOutData = szSubText;
			bFind = TRUE;
			break;
		}
	}

	file.Close();

	return bFind;
}


void CTestManager_Test::PRI_12BitToRaw8(unsigned char *p12Bit, unsigned char *pRawBit8, int w, int h)	// 1280 x 964 12bit -> 1280 x 960 8bit
{
	int x, y, m_iWidth = w, m_iHeight = h;

	for (y = 0; y < m_iHeight; y++)
	{
		for (x = 0; x < m_iWidth / 2; x = x++)
		{
			pRawBit8[y*m_iWidth + x * 2] = (p12Bit[(y + 2)*m_iWidth * 2 + 4 * x + 0]);
			pRawBit8[y*m_iWidth + x * 2 + 1] = (p12Bit[(y + 2)*m_iWidth * 2 + 4 * x + 2]);
		}
	}
}

void CTestManager_Test::RawToBmpQuater(unsigned char *pRaw8, unsigned char *pBMP, int image_width, int image_height, int ch)
{
	int i, j, sx = 0, sy = 0;

	if (ch == 0)		sx = 0, sy = 0;
	else if (ch == 1)		sx = 1, sy = 0;
	else if (ch == 2)		sx = 0, sy = 1;
	else if (ch == 3)		sx = 1, sy = 1;

	for (j = sy; j < image_height; j += 2)
	{
		for (i = sx; i < image_width; i += 2)
		{
			*pBMP++ = pRaw8[(image_height - j - 1)*image_width + i];
			*pBMP++ = pRaw8[(image_height - j - 1)*image_width + i];
			*pBMP++ = pRaw8[(image_height - j - 1)*image_width + i];
		}
	}
}


void CTestManager_Test::Shift12to16Bit_QuarterMode(unsigned char* p12bitRaw, unsigned char* p16bitRaw, int nWidth, int nHeight, int Mode)
{

	int k = 0;
	//int n12bitRawSize = nWidth * nHeight * 1.5;
	int nFullWidth = nWidth*1.5;

	int n12bitRawSize = nHeight * nFullWidth;

	unsigned char dummy[2], UseLineX = 0, UseLineY = 0;

	switch (Mode){
	case RCCC:
		UseLineX = 1;
		UseLineY = 1;
		break;
	case CRCC:
		UseLineX = 0;
		UseLineY = 1;
		break;
	case CCRC:
		UseLineX = 1;
		UseLineY = 0;
		break;
	case CCCR:
		UseLineX = 0;
		UseLineY = 0;
		break;
	}


	for (int i = 0; i < n12bitRawSize; i += 3)
	{
		if (((int)(i / nFullWidth) % 2) == UseLineY) {
			i += nFullWidth - 3;
			continue;
		}
		if (UseLineX == 0){
			p16bitRaw[k + 1] = (p12bitRaw[i] & 0xF0) >> 4;
			p16bitRaw[k + 0] = ((p12bitRaw[i] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0x0F));
			//dummy[1] = (p12bitRaw[i + 1] & 0xF0) >> 4;
			//dummy[0] = ((p12bitRaw[i + 1] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0xF0) >> 4);
		}
		else{
			//dummy[1] = (p12bitRaw[i] & 0xF0) >> 4;
			//dummy[0] = ((p12bitRaw[i] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0x0F));
			p16bitRaw[k + 1] = (p12bitRaw[i + 1] & 0xF0) >> 4;
			p16bitRaw[k + 0] = ((p12bitRaw[i + 1] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0xF0) >> 4);

		}
		k += 2;
		//p16bitRaw[k + 3] = (p12bitRaw[i + 1] & 0xF0) >> 4;
		//p16bitRaw[k + 2] = ((p12bitRaw[i + 1] & 0x0F) << 4) + ((p12bitRaw[i + 2] & 0xF0) >> 4);
		//k += 4;
	}
}


void CTestManager_Test::Shift16to12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight)
{
	unsigned int i, j, k;
	unsigned int nScrWidth, nDestWidth;
	unsigned char *pcDest;

	nDestWidth = nWidth * 3 / 2;
	nScrWidth = nWidth * 2;

	pcDest = (unsigned char *)pDest;

	for (i = 0; i < (nHeight); i++)
	{
		for (j = 0, k = 0; j < nDestWidth; j += 6, k += 8)
		{
			pcDest[j + 0] = ((pImage[k] & 0xF0) >> 4) + ((pImage[k + 1] & 0x0f) << 4);
			pcDest[j + 1] = ((pImage[k + 2] & 0xF0) >> 4) + ((pImage[k + 3] & 0x0f) << 4);
			pcDest[j + 2] = (pImage[k] & 0x0F) + ((pImage[k + 2] & 0x0f) << 4);

			pcDest[j + 3] = ((pImage[k + 4] & 0xF0) >> 4) + ((pImage[k + 5] & 0x0f) << 4);
			pcDest[j + 4] = ((pImage[k + 6] & 0xF0) >> 4) + ((pImage[k + 7] & 0x0f) << 4);
			pcDest[j + 5] = (pImage[k + 4] & 0x0F) + ((pImage[k + 6] & 0x0f) << 4);
		}
		pcDest += nDestWidth;
		pImage += nScrWidth;
	}
}

void CTestManager_Test::Shift12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight)
{
	unsigned int	i, j, k;
	unsigned int	nByteWidth;
	unsigned short *psDest;

	nByteWidth = nWidth * 3 / 2;
	psDest = (unsigned short *)pDest;
	nWidth >>= 1;
	for (i = 0; i < nHeight; i++)
	{
		for (j = 0, k = 0; j < nWidth; j++, k += 3)
		{
			psDest[j] = (pImage[k + 1] << 8) + (pImage[k]);
		}
		psDest += nWidth;
		pImage += nByteWidth;
	}
}


void CTestManager_Test::SetCamState_OC(__in UINT nCamState, __in double iIn_X, __in double iIn_Y, __out double& iOut_X, __out double& iOut_Y)
{
	switch (nCamState)
	{
	case OC_CamState_Original:	// 오리지날
		iOut_X = iIn_X;
		iOut_Y = iIn_Y;
		break;
	case OC_CamState_UpsideDown:// 상하반전
		iOut_X = iIn_X;
		iOut_Y = iIn_Y * -1;
		break;
	case OC_CamState_Reverse:	// 좌우반전
		iOut_X = iIn_X * -1;
		iOut_Y = iIn_Y;
		break;
	case OC_CamState_Rotate:	// 로테이트
		iOut_X = iIn_X * -1;
		iOut_Y = iIn_Y * -1;
		break;
	default:
		break;
	}
}
