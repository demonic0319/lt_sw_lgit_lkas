﻿#include <afxwin.h>
#include "stdafx.h"
#include "TestManager_TestMES_LKAS.h"

CTestManager_TestMES_LKAS::CTestManager_TestMES_LKAS()
{
}

CTestManager_TestMES_LKAS::~CTestManager_TestMES_LKAS()
{
}

//--------------------------------------------------------------------------------------------개별 검사 Report
void CTestManager_TestMES_LKAS::Current_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{

	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_ECurrent_IKC; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Current[t]);
	}

	for (int t = 0; t < MESDataNum_IR_ECurrent_IKC; t++)
	{
		
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[1]);
		
		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_LKAS::SFR_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_SFR_IKC; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_SFR[t]);
	}

	for (int t = 0; t < ROI_SFR_Max; t++)
	{
		if (m_pInspInfo->RecipeInfo.stIR_ImageQ.stSFR.stInput[t].bEnable)
		{
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.dbValue[t]);
		}
		else
		{
			strData.Format(_T("X"));
		}

		pAddData->Add(strData);
	}
}

void CTestManager_TestMES_LKAS::DynamicRange_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bDynamic;

	for (int t = 0; t < MesDataNUM_IR_DynamicBW_MRA2; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Dynamic_BW[t]);
	}


	strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbDynamic);
	pAddData->Add(strData);


	strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbSNR_BW);
	pAddData->Add(strData);
}


void CTestManager_TestMES_LKAS::Ymean_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.bYmeanResult;

	pAddHeaderz->Add(g_szMESHeader_Stain[0]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.nDefectCount);

	pAddData->Add(strData);
	
}

void CTestManager_TestMES_LKAS::LCB_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stLCB.bLCBResult;

	pAddHeaderz->Add(g_szMESHeader_Stain[1]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stLCB.nDefectCount);

	pAddData->Add(strData);
}

void CTestManager_TestMES_LKAS::BlackSpot_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stBlackSpot.bBlackSpotResult;

	pAddHeaderz->Add(g_szMESHeader_Stain[2]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stBlackSpot.nDefectCount);

	pAddData->Add(strData);
}

void CTestManager_TestMES_LKAS::Distortion_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDistortion.bRotation;

	pAddHeaderz->Add(g_szMESHeader_Distortion[0]); //Ymean Count만

	strData.Format(_T("%.3f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDistortion.dbRotation);

	pAddData->Add(strData);

}

void CTestManager_TestMES_LKAS::DefectBlack_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_Black.bDefect_BlackResult;

	pAddHeaderz->Add(g_szMESHeader_Defectpixel[1]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_Black.nDefect_BlackCount);

	pAddData->Add(strData);

}

void CTestManager_TestMES_LKAS::DefectWhite_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_White.bDefect_WhiteResult;

	pAddHeaderz->Add(g_szMESHeader_Defectpixel[3]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_White.nDefect_WhiteCount);

	pAddData->Add(strData);

}


void CTestManager_TestMES_LKAS::Shading_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_Shading; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Shading[t]);
	}

	for (int t = 0; t < MESDataNum_IR_Shading; t++)
	{
		switch (t)
		{
		case 0:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[0]);
			break;
		case 1:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[2]);
			break;
		case 2:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[1]);
			break;
		case 3:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[3]);
			break;
		default:
			break;
		}

		pAddData->Add(strData);
	}
}

void CTestManager_TestMES_LKAS::OpicalCenter_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stOpticalCenter.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_OpticalCenter[t]);
	}

	for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stOpticalCenter.dValue[t]);

		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_LKAS::Vision_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stVision.GetFinalResult();

	//for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{
		pAddHeaderz->Add(_T("Match Rate"));
	}

	//for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stVision.dbValue[0]);

		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_LKAS::Displace_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDisplace.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Displace[t]);
	}

	for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[t + 4]);
		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_LKAS::IIC_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stIIC.GetFinalResult();

	//for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		pAddHeaderz->Add(_T("IIC Pattern"));
	}

	//for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stIRImage.stIIC.bResult[0] == TRUE)
		{
			strData = _T("Pass");
		} 
		else
		{
			strData = _T("Fail");
		}

	//	strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stIIC.bResult[0]);
		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_LKAS::RI_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_RI_IKC; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_RI[t]);
	}

	
	strData.Format(_T("%.3f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRICorner);
	pAddData->Add(strData);


	strData.Format(_T("%.3f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIMin);
	pAddData->Add(strData);


	// 	switch (nSysType)
	// 	{
	// 	case Sys_2D_Cal:
	// 		break;
	// 	case Sys_3D_Cal:
	// 		break;
	// 	case Sys_Stereo_Cal:
	// 		break;
	// 	case Sys_Focusing:
	// 	{
	// 						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nResult;
	// 						 for (int t = 0; t < MESDataNum_Foc_SFR; t++)
	// 						 {
	// 							 pAddHeaderz->Add(g_szMESHeader_SFR[t]);
	// 						 }
	// 
	// 						 for (int t = 0; t < GRP_SFR_Max; t++)
	// 						 {
	// 							 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[t]);
	// 							 pAddData->Add(strData);
	// 						 }
	// 
	// 						 for (int t = 0; t < ROI_SFR_Max; t++)
	// 						 {
	// 							 if (m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.bUse)
	// 							 {
	// 								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbValue[t]);
	// 							 }
	// 							 else
	// 							 {
	// 								 strData.Format(_T("X"));
	// 							 }
	// 
	// 							 pAddData->Add(strData);
	// 						 }
	// 
	// 	}
	// 		break;
	// 	case Sys_Image_Test:
	// 	{
	// 						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.nResult;
	// 						   for (int t = 0; t < MESDataNum_SFR; t++)
	// 						   {
	// 							   pAddHeaderz->Add(g_szMESHeader_SFR[t]);
	// 						   }
	// 
	// 						   for (int t = 0; t < GRP_SFR_Max; t++)
	// 						   {
	// 							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.dbMinValue[t]);
	// 							   pAddData->Add(strData);
	// 						   }
	// 
	// 						   for (int t = 0; t < ROI_SFR_Max; t++)
	// 						   {
	// 
	// 							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.bUse)
	// 							   {
	// 								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.dbValue[t]);
	// 							   }
	// 							   else
	// 							   {
	// 								   strData.Format(_T("X"));
	// 							   }
	// 
	// 							   pAddData->Add(strData);
	// 						   }
	// 	}
	// 		break;
	// 	default:
	// 		break;
	// 	}

	//	pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR] = strFullData;
}

//=============================================================================
// Method		: SaveData_Current
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:02
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_Current(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	// 적용할 채널 적용
	UINT nCH = 1;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stCurrent.dbValue[1], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stCurrent.bResult[1]);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_Current] = strFullData;
}

//=============================================================================
// Method		: SaveData_IIC
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:04
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_IIC(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	// 적용할 채널 적용
	UINT nCH = 0;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stIIC.dbValue[nCH], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stIIC.bResult[nCH]);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_I2C] = strFullData;
}

//=============================================================================
// Method		: SaveData_SFR
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:04
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_SFR(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (UINT nCH = 0; nCH < ROI_SFR_Max; nCH++)
	{
		switch (nCH)
		{
		case 0:		// CL_00F_V MTF(67lp)_Center
		case 1:		// CR_OOF_V MTF(67lp)_Center
		case 2:		// CT_00F_H MTF(67lp)_Center
		case 3:		// CB_00F_H MTF(67lp)_Center
		case 4:		// LT_04F_V MTF(67lp)_0.4F
		case 5:		// LT_04F_H MTF(67lp)_0.4F
		case 6:		// RT_04F_V MTF(67lp)_0.4F
		case 7:		// RT_04F_H MTF(67lp)_0.4F
		case 8:		// RB_04F_V MTF(67lp)_0.4F
		case 9:		// RB_04F_H MTF(67lp)_0.4F
		case 10:	// LB_04F_V MTF(67lp)_0.4F
		case 11:	// LB_04F_H MTF(67lp)_0.4F
		case 12:	// L_04F_V  MTF(67lp)_0.4F
		case 13:	// L_04F_H  MTF(67lp)_0.4F
		case 14:	// R_04F_V  MTF(67lp)_0.4F
		case 15:	// R_04F_H  MTF(67lp)_0.4F
		case 16:	// LT_07F_V MTF(67lp)_0.7F
		case 17:	// LT_07F_H MTF(67lp)_0.7F
		case 18:	// RT_07F_V MTF(67lp)_0.7F
		case 19:	// RT_07F_H MTF(67lp)_0.7F
		case 20:	// RB_07F_V MTF(67lp)_0.7F
		case 21:	// RB_07F_H MTF(67lp)_0.7F
		case 22:	// LB_07F_V MTF(67lp)_0.7F
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stSFR.dbValue[nCH], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stSFR.bResult[nCH]);
			strData		+= _T(",");
			strFullData += strData;
			break;

			// 마지막 항목 넣어 마지막거는 ',' 하면 안되 다른 곳에서 함
		case 23:	// LB_07F_H MTF(67lp)_0.7F
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stSFR.dbValue[nCH], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stSFR.bResult[nCH]);
			strFullData += strData;
			break;
		default:
			break;
		}
	}

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_SFR] = strFullData;
}

//=============================================================================
// Method		: SaveData_Distortion
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:10
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_Distortion(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stDistortion.dbRotation, m_pInspInfo->CamInfo[iParaIdx].stIRImage.stDistortion.bRotation);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_Distortion] = strFullData;
}

//=============================================================================
// Method		: SaveData_DefectBlock
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:11
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_DefectBlock(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stDefect_Black.nDefect_BlackCount, m_pInspInfo->CamInfo[iParaIdx].stIRImage.stDefect_Black.bDefect_BlackResult);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_DefectBlack] = strFullData;

}

//=============================================================================
// Method		: SaveData_DefectWhite
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:12
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_DefectWhite(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stDefect_White.nDefect_WhiteCount, m_pInspInfo->CamInfo[iParaIdx].stIRImage.stDefect_White.bDefect_WhiteResult);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_DefectWhite] = strFullData;
}

//=============================================================================
// Method		: SaveData_Ymean
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:12
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_Ymean(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stYmean.nDefectCount, m_pInspInfo->CamInfo[iParaIdx].stIRImage.stYmean.bYmeanResult);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_Ymean] = strFullData;
}

void CTestManager_TestMES_LKAS::SaveData_LCB(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stLCB.nDefectCount, m_pInspInfo->CamInfo[iParaIdx].stIRImage.stLCB.bLCBResult);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_LCB] = strFullData;
}

void CTestManager_TestMES_LKAS::SaveData_BlackSpot(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%d:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stBlackSpot.nDefectCount, m_pInspInfo->CamInfo[iParaIdx].stIRImage.stBlackSpot.bBlackSpotResult);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_BlackSpot] = strFullData;
}

//=============================================================================
// Method		: SaveData_Shading
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:13
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_Shading(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	// 데이터 이상해 형이 준 로그랑 달러 확인해보고 다시 넣어

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.dValue[enUI_ShadingField_1], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.bResult[enUI_ShadingField_1]);
	strData		+= _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.dValue[enUI_ShadingField_1_Dev], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.bResult[enUI_ShadingField_1_Dev]);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.dValue[enUI_ShadingField_2], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.bResult[enUI_ShadingField_2]);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.dValue[enUI_ShadingField_2_Dev], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stShading.bResult[enUI_ShadingField_2_Dev]);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_Shading] = strFullData;
}

//=============================================================================
// Method		: SaveData_OpticalCenter
// Access		: public  
// Returns		: void
// Parameter	: __in int iParaIdx
// Parameter	: __in ST_MES_Data_LKAS * pMesData
// Qualifier	:
// Last Update	: 2018/9/20 - 10:14
// Desc.		:
//=============================================================================
void CTestManager_TestMES_LKAS::SaveData_OpticalCenter(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	// Optical X
	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stOpticalCenter.dValue[OpticalCenter_Offset_X], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stOpticalCenter.bResult[OpticalCenter_Offset_X]);
	strData		+= _T(",");
	strFullData += strData;

	// Optical Y
	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[iParaIdx].stIRImage.stOpticalCenter.dValue[OpticalCenter_Offset_Y], m_pInspInfo->CamInfo[iParaIdx].stIRImage.stOpticalCenter.bResult[OpticalCenter_Offset_Y]);
	strFullData += strData;

	pMesData->szMesTestData[iParaIdx][MesDataIdx_LKAS_OpticalCenter] = strFullData;
}
