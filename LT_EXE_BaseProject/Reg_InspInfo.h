﻿//*****************************************************************************
// Filename	: 	Reg_InspInfo.h
// Created	:	2016/3/31 - 16:33
// Modified	:	2016/3/31 - 16:33
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Reg_InspInfo_h__
#define Reg_InspInfo_h__

#pragma once
#include "Registry.h"
#include "Def_DataStruct.h"

class CReg_InspInfo
{
public:
	CReg_InspInfo();
	CReg_InspInfo(__in LPCTSTR lpszRegPath);
	~CReg_InspInfo();

protected:
	CRegistry	m_Reg;

	CString		m_strRegPath;

public:
	void		SetRegitryPath			(__in LPCTSTR lpszRegPath)
	{
		m_strRegPath = lpszRegPath;
	}

 	BOOL	SaveSelectedModel			(__in CString szRecipeFile, __in CString szRecipe);
 	BOOL	LoadSelectedModel			(__out CString& szRecipeFile, __out CString& szRecipe);

	BOOL	SaveSelectedModel			(__in enModelType nModel, __in CString szRecipeFile, __in CString szRecipe);
 	BOOL	LoadSelectedModel			(__in enModelType nModel, __out CString& szRecipeFile, __out CString& szRecipe);

	BOOL	SaveYield					(__in const ST_Yield* pstYield);
	BOOL	LoadYield					(__out ST_Yield& stYield);

	// Password 2차
	BOOL	SavePassword				(__in UINT nIndex, __in CString szPassword);
	CString LoadPassword				(__in UINT nIndex);

	BOOL	SavePogoInfo				(__in const ST_ConsumablesInfo* pstConsumInfo);
	BOOL	LoadPogoInfo				(__out ST_ConsumablesInfo& stConsumInfo);

	// 카메라 검사 여부 
	BOOL	SaveSelectedCam				(__in DWORD dwSelection);
	BOOL	LoadSelectedCam				(__out DWORD& dwSelection);

	// 모터
	BOOL	SaveSelectedMotor			(__in const CString szMotorFile);
	BOOL	LoadSelectedMotor			(__out CString& szMotorFile);

	// 유지 보수
	BOOL	SaveSelectMaintenance		(__in CString szMaintenanceFile);
	BOOL	LoadSelectMaintenance		(__out CString& szMaintenanceFile);

};

#endif // Reg_InspInfo_h__
