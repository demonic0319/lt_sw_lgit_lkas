﻿//*****************************************************************************
// Filename	: 	Def_Test.h
// Created	:	2017/09/08
// Modified	:	2017/09/08
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Test_h__
#define Def_Test_h__

#include "Def_Enum.h"
#include "Def_Test_Cm.h"
#include "Def_TestItem_Cm.h"
#include "Def_Camera_Cm.h"
#include "Def_T_2DCal.h"
#include "Def_T_IR.h"
#include "Def_T_IQ.h"
#include "Def_T_Foc.h"

//-----------------------------------------------------------------------------
// 쓰레드로 동시에 구동하는 검사 구분
//-----------------------------------------------------------------------------
typedef enum enThreadTestType
{
	TTT_Initialize,
	TTT_Fialize,
	TTT_CameraReboot,
	TTT_EEPROM_Write,
};

static LPCTSTR g_szThreadTestType[] =
{
	_T("Camera Initialize"),
	_T("Camera Fialize"),
	_T("Camera Reboot"),
	_T("EEPROM Write"),
	NULL
};

#define MAX_CURRENT_CNT		5

//-----------------------------------------------------------------------------
// 카메라 검사 정보 구조체 (검사기 별)
//-----------------------------------------------------------------------------
typedef struct _tag_CamInfo : public ST_CamInfo_Base
{
	ST_StepMeasInfo		TestInfo;					// 검사 스텝 정보

	long				lCurrent[MAX_CURRENT_CNT];	// 전류 측정 값
	UINT				nInitialize;				// * 영상무감 (전원 보드, 그래버 보드, 영상)
	UINT				nFinalize;					// * 전원 보드, 그래버 보드, 영상
	UINT				nEEPROM_Result;				// * EEPROM Write 결과 코드
	UINT				nErr_CornerPtStepNo;		// * 코너점 추출 오류 단계
	UINT				nCAL_ResultCode;			// * CAL 라이브러리 반환 코드

	//CStringA			szADDI0933_ID;
	//CStringA			szSensorID;

	// 2D CAL 측정 값
	ST_2DCal_CornerPt	st2D_CornerPt[MAX_2DCAL_ROI];	// 13 / 15개	
	ST_2DCal_Result		st2D_Result;

	// Image Test;
 	ST_IQ_Result		stImageQ;

	// Focusing
	ST_Foc_Result		stFocus;

	ST_IR_Result		stIRImage;

	_tag_CamInfo()
	{
		for (UINT nIdx = 0; nIdx < MAX_CURRENT_CNT; nIdx++)
		{
			lCurrent[nIdx] = 0;
		}
		nInitialize			= RC_OK;
		nEEPROM_Result		= RCRom_OK;
		nErr_CornerPtStepNo = 0;
		nCAL_ResultCode		= 0;
	};

	virtual void Reset(__in BOOL bWithoutBarcode = FALSE)
	{
		__super::Reset(bWithoutBarcode);

		TestInfo.ResetData();
		for (UINT nIdx = 0; nIdx < MAX_CURRENT_CNT; nIdx++)
		{
			lCurrent[nIdx] = 0;
		}
		nInitialize			= RC_OK;
		nFinalize			= RC_OK;
		nEEPROM_Result		= RCRom_OK;
		nErr_CornerPtStepNo = 0;
		nCAL_ResultCode		= 0;

		for (UINT nIdx = 0; nIdx < MAX_2DCAL_ROI; nIdx++)
		{
			st2D_CornerPt[nIdx].Reset();
		}

		st2D_Result.Reset();
		stImageQ.Reset();
		stFocus.Reset();
	}

	_tag_CamInfo& operator= (_tag_CamInfo& ref)
	{
		//__super::operator =(ref);

		TestInfo		= ref.TestInfo;
		for (UINT nIdx = 0; nIdx < MAX_CURRENT_CNT; nIdx++)
		{
			lCurrent[nIdx] = ref.lCurrent[nIdx];
		}
		nInitialize			= ref.nInitialize;
		nFinalize			= ref.nFinalize;
		nEEPROM_Result		= ref.nEEPROM_Result;
		nErr_CornerPtStepNo = ref.nErr_CornerPtStepNo;
		nCAL_ResultCode		= ref.nCAL_ResultCode;
		
		for (UINT nIdx = 0; nIdx < MAX_2DCAL_ROI; nIdx++)
		{
			st2D_CornerPt[nIdx] = ref.st2D_CornerPt[nIdx];
		}
 		st2D_Result		= ref.st2D_Result;
		stImageQ		= ref.stImageQ;
		stFocus			= ref.stFocus;

		return *this;
	};

	virtual void Reset_Measurment()
	{
		__super::Reset();

		TestInfo.ResetData();
		for (UINT nIdx = 0; nIdx < MAX_CURRENT_CNT; nIdx++)
		{
			lCurrent[nIdx] = 0;
		}
		nInitialize			= RC_OK;
		nFinalize			= RC_OK;
		nEEPROM_Result		= RCRom_OK;
		nErr_CornerPtStepNo = 0;
		nCAL_ResultCode		= 0;

		for (UINT nIdx = 0; nIdx < MAX_2DCAL_ROI; nIdx++)
		{
			st2D_CornerPt[nIdx].Reset();
		}

		st2D_Result.Reset();
		stImageQ.Reset();
		stFocus.Reset();

	};

	// Current 측정 데이터
	void Set_CurrentMeas(__in long* lInCurrent, __in UINT nCount)
	{
		UINT nLoop = (nCount <= MAX_CURRENT_CNT) ? nCount : MAX_CURRENT_CNT;

		for (UINT nIdx = 0; nIdx < nLoop; nIdx++)
		{
			lCurrent[nIdx] = lInCurrent[nIdx];
		}
	};

}ST_CamInfo, *PST_CamInfo;


#endif // Def_Test_h__
