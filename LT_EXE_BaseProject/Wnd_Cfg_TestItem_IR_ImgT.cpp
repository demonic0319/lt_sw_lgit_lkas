//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_IR_ImgT.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestItem_IR_ImgT.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestItem_IR_ImgT.h"
#include "Def_WindowMessage.h"

// CWnd_Cfg_TestItem_IR_ImgT

#define IDC_TAB 1000
IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem_IR_ImgT, CWnd_BaseView)

CWnd_Cfg_TestItem_IR_ImgT::CWnd_Cfg_TestItem_IR_ImgT()
{
	m_InspectionType = enInsptrSysType::Sys_Image_Test;
}

CWnd_Cfg_TestItem_IR_ImgT::~CWnd_Cfg_TestItem_IR_ImgT()
{
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem_IR_ImgT, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()

//	ON_EN_SETFOCUS(IDC_TAB, &CWnd_Cfg_TestItem_IR_ImgT::OnEnSetfocusTabFocus)

END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem_IR_ImgT::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	UINT nID_Index = 0;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, IDC_TAB, CMFCTabCtrl::LOCATION_BOTTOM);

	m_Wnd_SFR.SetOwner(GetOwner());
	m_Wnd_SFR.SetOverlayID(WM_SELECT_OVERLAY, Ovr_SFR);
	m_Wnd_SFR.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_SFR.Create(NULL, _T("SFR"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_DynamicBW.SetOwner(GetOwner());
	m_Wnd_DynamicBW.SetOverlayID(WM_SELECT_OVERLAY, Ovr_DynamicBW);
	m_Wnd_DynamicBW.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_DynamicBW.Create(NULL, _T("DynamicBW"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Current.SetOwner(GetOwner());
	m_Wnd_Current.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Current);
	m_Wnd_Current.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Current.Create(NULL, _T("Current"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Shading.SetOwner(GetOwner());
	m_Wnd_Shading.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Shading);
	m_Wnd_Shading.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Shading.Create(NULL, _T("Shading"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Ymean.SetOwner(GetOwner());
	m_Wnd_Ymean.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Ymean);
	m_Wnd_Ymean.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Ymean.Create(NULL, _T("Ymean"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_LCB.SetOwner(GetOwner());
	m_Wnd_LCB.SetOverlayID(WM_SELECT_OVERLAY, Ovr_LCB);
	m_Wnd_LCB.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_LCB.Create(NULL, _T("LCB"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_BlackSpot.SetOwner(GetOwner());
	m_Wnd_BlackSpot.SetOverlayID(WM_SELECT_OVERLAY, Ovr_BlackSpot);
	m_Wnd_BlackSpot.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_BlackSpot.Create(NULL, _T("Black Spot"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Rllumination.SetOwner(GetOwner());
	m_Wnd_Rllumination.SetOverlayID(WM_SELECT_OVERLAY, Ovr_RI);
	m_Wnd_Rllumination.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Rllumination.Create(NULL, _T("Rllumination"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Chart.SetOwner(GetOwner());
	m_Wnd_Chart.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Chart);
	m_Wnd_Chart.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Chart.Create(NULL, _T("Chart"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Displace.SetOwner(GetOwner());
	m_Wnd_Displace.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Displace);
	m_Wnd_Displace.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Displace.Create(NULL, _T("Displace"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Vision.SetOwner(GetOwner());
	m_Wnd_Vision.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Vision);
	m_Wnd_Vision.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Vision.Create(NULL, _T("Vision"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_IIC.SetOwner(GetOwner());
	m_Wnd_IIC.SetOverlayID(WM_SELECT_OVERLAY, Ovr_IIC);
	m_Wnd_IIC.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_IIC.Create(NULL, _T("IIC"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);
	m_Wnd_Distortion.SetOwner(GetOwner());
	m_Wnd_Distortion.SetOverlayID(WM_SELECT_OVERLAY, Ovr_Distortion);
	m_Wnd_Distortion.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Distortion.Create(NULL, _T("Distortion"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Defect_Black.SetOwner(GetOwner());
	m_Wnd_Defect_Black.SetOverlayID(WM_SELECT_OVERLAY, Ovr_DefectBlack);
	m_Wnd_Defect_Black.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Defect_Black.Create(NULL, _T("Defect_Black"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_Defect_White.SetOwner(GetOwner());
	m_Wnd_Defect_White.SetOverlayID(WM_SELECT_OVERLAY, Ovr_DefectWhite);
	m_Wnd_Defect_White.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_Defect_White.Create(NULL, _T("Defect_White"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_Wnd_OpticalCenter.SetOwner(GetOwner());
	m_Wnd_OpticalCenter.SetOverlayID(WM_SELECT_OVERLAY, Ovr_OpticalCenter);
	m_Wnd_OpticalCenter.SeChangeOption(WM_CHANGE_OPTIONPIC);
	m_Wnd_OpticalCenter.Create(NULL, _T("Optical Center"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

	m_wnd_EachTest.SetCameraParaIdx(m_pnCamParaIdx);
	m_wnd_EachTest.Create(NULL, _T("개별"), dwStyle /*| WS_BORDER*/, rectDummy, this, nID_Index++);

	// 검사기 별로 탭 컨트롤 정의
	SetSysAddTabCreate();

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iLeft	= iMargin;
	int iTop	= iMargin;
	int iWidth	= cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int nTabW = iWidth * 4 / 5;
	m_tc_Option.MoveWindow(iLeft, iTop, nTabW, iHeight);

	iLeft += iMargin + nTabW;
	
	m_wnd_EachTest.MoveWindow(iLeft, iTop, iWidth - (iMargin + nTabW), iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Option.SetActiveTab(0);

		GetOwner()->SendMessage(WM_SELECT_OVERLAY, 0, Ovr_Current);
	}
}

//=============================================================================
// Method		: OnNMClickTestItem
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/9/28 - 18:36
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::OnNMClickTestItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: SetSysAddTabCreate
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::SetSysAddTabCreate()
{
	UINT nItemCnt = 0;

	m_tc_Option.AddTab(&m_Wnd_Displace,			_T("Displace		"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Vision,			_T("Vision			"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Current,			_T("Current			"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_IIC,				_T("IIC				"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_SFR,				_T("SFR				"),		nItemCnt++,		TRUE);
//	m_tc_Option.AddTab(&m_Wnd_DynamicBW,		_T("DynamicBW		"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Shading,			_T("Shading			"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Ymean,			_T("Ymean			"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_LCB,				_T("LCB				"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_BlackSpot,		_T("BlackSpot		"),		nItemCnt++,		TRUE);
	m_tc_Option.AddTab(&m_Wnd_Distortion,		_T("Distortion		"),		nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Defect_Black,		_T("Defect_Black	"),		 nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_Defect_White,		_T("Defect_White	"),		nItemCnt++, TRUE);
	m_tc_Option.AddTab(&m_Wnd_OpticalCenter,	_T("Optical Center	"),		nItemCnt++,		TRUE);
//	m_tc_Option.AddTab(&m_Wnd_Chart,			_T("Chart			"),		nItemCnt++,		TRUE);
}

//=============================================================================
// Method		: SetInitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 18:20
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::SetInitListCtrl()
{

}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_wnd_EachTest.SetSystemType(nSysType);
}

//=============================================================================
// Method		: Set_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::Set_RecipeInfo(__in ST_RecipeInfo* pstRecipeInfo)
{
	m_Wnd_SFR.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stSFR);
	m_Wnd_SFR.SetUpdateData();

	m_Wnd_DynamicBW.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stDynamicBW);
	m_Wnd_DynamicBW.SetUpdateData();

 	m_Wnd_Shading.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stShading);
 	m_Wnd_Shading.SetUpdateData();

	m_Wnd_Current.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stCurrent);
	m_Wnd_Current.SetUpdateData();

	m_Wnd_Ymean.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stYmean);
	m_Wnd_Ymean.SetUpdateData();

	m_Wnd_LCB.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stLCB);
	m_Wnd_LCB.SetUpdateData();

	m_Wnd_BlackSpot.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stBlackSpot);
	m_Wnd_BlackSpot.SetUpdateData();

	m_Wnd_Rllumination.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stRllumination);
	m_Wnd_Rllumination.SetUpdateData();

	m_Wnd_OpticalCenter.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stOpticalCenter);
	m_Wnd_OpticalCenter.SetUpdateData();

	m_Wnd_Chart.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stChart);
	m_Wnd_Chart.SetUpdateData();

	m_Wnd_Displace.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stDisplace);
	m_Wnd_Displace.SetUpdateData();

	m_Wnd_Vision.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stVision);
	m_Wnd_Vision.SetUpdateData();

	m_Wnd_IIC.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stIIC);
	m_Wnd_IIC.SetUpdateData();
	
	m_Wnd_Distortion.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stDistortion);
	m_Wnd_Distortion.SetUpdateData();

	m_Wnd_Defect_Black.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stDefect_Black);
	m_Wnd_Defect_Black.SetUpdateData();
	
	m_Wnd_Defect_White.SetPtr_RecipeInfo(&pstRecipeInfo->stIR_ImageQ.stDefect_White);
	m_Wnd_Defect_White.SetUpdateData();

	m_wnd_EachTest.Set_RecipeInfo(pstRecipeInfo);
}

//=============================================================================
// Method		: Get_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stOutRecipInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::Get_RecipeInfo(__out ST_RecipeInfo& stOutRecipInfo)
{
	m_Wnd_SFR.GetUpdateData();
	m_Wnd_DynamicBW.GetUpdateData();
	m_Wnd_Shading.GetUpdateData();
	m_Wnd_Current.GetUpdateData();
	m_Wnd_Ymean.GetUpdateData();
	m_Wnd_BlackSpot.GetUpdateData();
	m_Wnd_LCB.GetUpdateData();
	m_Wnd_OpticalCenter.GetUpdateData();
	m_Wnd_Rllumination.GetUpdateData();
	m_Wnd_Chart.GetUpdateData();
	m_Wnd_Displace.GetUpdateData();
	m_Wnd_Vision.GetUpdateData();
	m_Wnd_IIC.GetUpdateData();
m_Wnd_Distortion.GetUpdateData();
	m_Wnd_Defect_Black.GetUpdateData();
	m_Wnd_Defect_White.GetUpdateData();
}

//=============================================================================
// Method		: Get_TestItemInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __out ST_TestItemInfo & stOutTestItemInfo
// Qualifier	:
// Last Update	: 2018/2/25 - 17:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::Get_TestItemInfo(__in ST_RecipeInfo& stRecipeInfo, __out ST_TestItemInfo& stOutTestItemInfo)
{
	ST_TestItemSpec* pSpec = NULL;
	CString szText;
	UINT nCtrlIdx = 0;

// 	for (UINT nIdx = enTestItem_IR_ImgT::TI_IR_ImgT_Re_Current; nIdx <= enTestItem_IR_ImgT::TI_IR_ImgT_Re_DefectWhite; nIdx++)
// 	{
// 		pSpec = &stOutTestItemInfo.TestItemList.GetAt(nIdx);
// 		ASSERT(NULL != pSpec);
// 
// 		// 결과 데이터가 1개 초과이고, 개별 Min Max 스펙을 가지는 경우
// 		if ((1 < pSpec->nResultCount) && (pSpec->bUseMultiSpec))
// 		{
// 			pSpec->bUseMinMaxSpec = FALSE;
// 
// 			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
// 			{
// 				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_IR_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);
// 
// 				if (pSpec->Spec[nArIdx].bUseSpecMin)
// 				{
// 					pSpec->bUseMinMaxSpec = TRUE;
// 
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
// 					}
// 				}
// 				else
// 				{
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
// 					}
// 				}
// 
// 				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_IR_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);
// 
// 				if (pSpec->Spec[nArIdx].bUseSpecMax)
// 				{
// 					pSpec->bUseMinMaxSpec = TRUE;
// 
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
// 					}
// 				}
// 				else
// 				{
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
// 					}
// 				}
// 
// 				// 항목 인덱스 증가
// 				++nCtrlIdx;
// 			}
// 		}
// 		else // 결과 데이터가 동일한 MinMax 스펙을 가지는 경우
// 		{
// 			pSpec->bUseMinMaxSpec = FALSE;
// 
// 			for (UINT nArIdx = 0; nArIdx < pSpec->nResultCount; nArIdx++)
// 			{
// 				Get_TestItemMinSpec(stRecipeInfo, (enTestItem_IR_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMin, szText);
// 
// 				if (pSpec->Spec[nArIdx].bUseSpecMin)
// 				{
// 					pSpec->bUseMinMaxSpec = TRUE;
// 
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.intVal = _ttoi(szText.GetBuffer(0));
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.dblVal = _ttof(szText.GetBuffer(0));
// 					}
// 				}
// 				else
// 				{
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.intVal = 0;
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Min.dblVal = 0.0f;
// 					}
// 				}
// 
// 				Get_TestItemMaxSpec(stRecipeInfo, (enTestItem_IR_ImgT)nIdx, nArIdx, pSpec->Spec[nArIdx].bUseSpecMax, szText);
// 
// 				if (pSpec->Spec[nArIdx].bUseSpecMax)
// 				{
// 					pSpec->bUseMinMaxSpec = TRUE;
// 
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.intVal = _ttoi(szText.GetBuffer(0));
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.dblVal = _ttof(szText.GetBuffer(0));
// 					}
// 				}
// 				else
// 				{
// 					if ((VT_I2 == pSpec->vt) || (VT_I4 == pSpec->vt) || ((VT_I1 <= pSpec->vt) && (pSpec->vt <= VT_UINT)))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.intVal = 0;
// 					}
// 					else if ((VT_R4 == pSpec->vt) || (VT_R8 == pSpec->vt))
// 					{
// 						pSpec->Spec[nArIdx].Spec_Max.dblVal = 0.0f;
// 					}
// 				}
// 			}
// 
// 			// 항목 인덱스 증가
// 			++nCtrlIdx;
// 		}
// 	}
}

//=============================================================================
// Method		: Get_TestItemMinSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_IR_ImgT enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL &bUseSpec
// Parameter	: __out CString &szSpec
// Qualifier	:
// Last Update	: 2018/2/25 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::Get_TestItemMinSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_IR_ImgT enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	// KHO
	switch (enTestItem)
	{
// 	case TI_IR_ImgT_Re_Vision:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stVision.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stVision.stSpecMin[nArIdx].dbValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Displace:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDisplace.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stDisplace.stSpecMin[nArIdx].dbValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Current:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stCurrent.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stCurrent.stSpecMin[nArIdx].dbValue);
// 		break;
// 	case TI_IR_ImgT_Re_SFR:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stSFR.stInput[nArIdx].stSpecMin.bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stSFR.stInput[nArIdx].stSpecMin.dbValue);
// 		break;
// 	case TI_IR_ImgT_Re_Ymean:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stYmean.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stYmean.stSpecMin[nArIdx].iValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Shading_F1:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stShading.stSpecMin[0].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stShading.stSpecMin[0].dbValue);
// 		break;
// 	case TI_IR_ImgT_Re_Shading_F2:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stShading.stSpecMin[1].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stShading.stSpecMin[1].dbValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Distortion:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDistortion.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stDistortion.stSpecMin[nArIdx].iValue);
// 		break;
// 	case TI_IR_ImgT_Re_DefectBlack:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDefect_Black.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stDefect_Black.stSpecMin[nArIdx].iValue);
// 		break;
// 	case TI_IR_ImgT_Re_DefectWhite:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDefect_White.stSpecMin[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stDefect_White.stSpecMin[nArIdx].iValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_OpticalCenterX:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMin[0].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMin[0].iValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_OpticalCenterY:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMin[1].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMin[1].iValue);
// 		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: Get_TestItemMaxSpec
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo & stRecipeInfo
// Parameter	: __in enTestItem_IR_ImgT enTestItem
// Parameter	: __in UINT nArIdx
// Parameter	: __out BOOL &bUseSpec
// Parameter	: __out CString &szSpec
// Qualifier	:
// Last Update	: 2018/2/25 - 17:22
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::Get_TestItemMaxSpec(__in ST_RecipeInfo& stRecipeInfo, __in enTestItem_IR_ImgT enTestItem, __in UINT nArIdx, __out BOOL &bUseSpec, __out CString &szSpec)
{
	// KHO
	switch (enTestItem)
	{
// 	case TI_IR_ImgT_Re_Vision:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stVision.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stVision.stSpecMax[nArIdx].dbValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Displace:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDisplace.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stDisplace.stSpecMax[nArIdx].dbValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Current:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stCurrent.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stCurrent.stSpecMax[nArIdx].dbValue);
// 		break;
// 	case TI_IR_ImgT_Re_SFR:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stSFR.stInput[nArIdx].stSpecMax.bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stSFR.stInput[nArIdx].stSpecMax.dbValue);
// 		break;
// 	case TI_IR_ImgT_Re_Ymean:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stYmean.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stYmean.stSpecMax[nArIdx].iValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Shading_F1:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stShading.stSpecMax[0].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stShading.stSpecMax[0].dbValue);
// 		break;
// 	case TI_IR_ImgT_Re_Shading_F2:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stShading.stSpecMax[1].bEnable;
// 		szSpec.Format(_T("%.2f"), stRecipeInfo.stIR_ImageQ.stShading.stSpecMax[1].dbValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_Distortion:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDistortion.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stDistortion.stSpecMax[nArIdx].iValue);
// 		break;
// 	case TI_IR_ImgT_Re_DefectBlack:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDefect_Black.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stDefect_Black.stSpecMax[nArIdx].iValue);
// 		break;
// 	case TI_IR_ImgT_Re_DefectWhite:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stDefect_White.stSpecMax[nArIdx].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stDefect_White.stSpecMax[nArIdx].iValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_OpticalCenterX:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMax[0].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMax[0].iValue);
// 		break;
// 
// 	case TI_IR_ImgT_Re_OpticalCenterY:
// 		bUseSpec = stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMax[1].bEnable;
// 		szSpec.Format(_T("%d"), stRecipeInfo.stIR_ImageQ.stOpticalCenter.stSpecMax[1].iValue);
// 		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: OnEnSetfocusTabFocus
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/7 - 19:16
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem_IR_ImgT::OnEnSetfocusTabFocus()
{
	GetOwner()->SendNotifyMessage(WM_CHANGE_OPTIONPIC,0, 0);
}