﻿//*****************************************************************************
// Filename	: View_MainCtrl.cpp
// Created	: 2010/11/26
// Modified	: 2016/07/21
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// View_MainCtrl.cpp : CView_MainCtrl 클래스의 구현
//

#include "stdafx.h"
#include "resource.h"

#include "View_MainCtrl.h"
#include "CommonFunction.h"
#include "Pane_CommStatus.h"
#include "File_Recipe.h"
#include "File_Report.h"
#include "File_Maintenance.h"

#include <strsafe.h>
#include <iphlpapi.h>
#include <icmpapi.h>

#pragma comment(lib, "iphlpapi.lib")

//msec 측정 라이브러리 추가
#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


//=============================================================================
// CView_MainCtrl 생성자
//=============================================================================
CView_MainCtrl::CView_MainCtrl()
{
	// 윈도우 포인터	
	m_nWndIndex				= -1;
		
	for (int iCnt = 0; iCnt < SUBVIEW_MAX; iCnt++)
 		m_pWndPtr[iCnt]		= NULL;
	
	m_pwndCommPane			= NULL;
	m_pdlgBarcode			= NULL;

	InitConstructionSetting();
}

//=============================================================================
// CView_MainCtrl 소멸자
//=============================================================================
CView_MainCtrl::~CView_MainCtrl()
{
	TRACE(_T("<<< Start ~CView_MainCtrl >>> \n"));

	if (NULL != m_pdlgBarcode)
	{
		m_pdlgBarcode->DestroyWindow();
		delete m_pdlgBarcode;
		m_pdlgBarcode = NULL;
	}

	//DeleteSplashScreen();

	TRACE(_T("<<< End ~CView_MainCtrl >>> \n"));
}


BEGIN_MESSAGE_MAP(CView_MainCtrl, CWnd)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
END_MESSAGE_MAP()


//=============================================================================
// CView_MainCtrl 메시지 처리기
//=============================================================================
//=============================================================================
// Method		: CView_MainCtrl::PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2010/11/26 - 13:59
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::PreCreateWindow(CREATESTRUCT& cs)
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::OnPaint
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/11/26 - 14:00
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnPaint() 
{
	CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트
	
	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
	
	// 그리기 메시지에 대해서는 CWnd::OnPaint()를 호출하지 마십시오.
}

//=============================================================================
// Method		: CView_MainCtrl::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
int CView_MainCtrl::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	//m_wnd_MainView.SetBackgroundColor(RGB(0xFF, 0xFF, 0xFF));
	//m_wnd_MainView.SetBackgroundColor(RGB(0, 0, 0));

 	CRect rectDummy;
 	rectDummy.SetRectEmpty();
 
 	UINT nViewIndex = 1;
 
 	//m_wnd_MainView.SetSystemType(m_InspectionType);
 	if (!m_wnd_MainView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + nViewIndex++, NULL))
 	{
 		TRACE0("m_wnd_MainView 뷰 창을 만들지 못했습니다.\n");
 		return -1;
 	}
 
 	//m_wnd_ManualView.SetSystemType(m_InspectionType);
 	//m_wnd_ManualView.SetPtrInspectionInfo(&m_stInspInfo);
//  	if (!m_wnd_ManualView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + nViewIndex++, NULL))
//  	{
//  		TRACE0("m_wnd_ManualView 뷰 창을 만들지 못했습니다.\n");
//  		return -1;
//  	}
 
 	if (!m_wnd_IOView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + nViewIndex++, NULL))
 	{
 		TRACE0("m_wnd_IOView 뷰 창을 만들지 못했습니다.\n");
 		return -1;
 	}
 
 	if (!m_wnd_MaintenanceView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + nViewIndex++, NULL))
 	{
 		TRACE0("m_wnd_MaintenanceView 뷰 창을 만들지 못했습니다.\n");
 		return -1;
 	}
 
 	//m_wnd_RecipeView.SetPtr_ImageMode(&m_stImageMode);
 	if (!m_wnd_RecipeView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + nViewIndex++, NULL))
 	{
 		TRACE0("m_wnd_RecipeView 뷰 창을 만들지 못했습니다.\n");
 		return -1;
 	}			
 
 // 	if (!m_wnd_AlarmView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + nViewIndex++, NULL))
 // 	{
 // 		TRACE0("m_wnd_AlarmView 뷰 창을 만들지 못했습니다.\n");
 // 		return -1;
 // 	}
 
 	if (!m_wnd_LogView.Create(NULL, NULL, WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, this, AFX_IDW_PANE_FIRST + nViewIndex++, NULL))
 	{
 		TRACE0("m_wnd_LogView 뷰 창을 만들지 못했습니다.\n");
 		return -1;
 	}	
 
 	m_wnd_MainView.ShowWindow(SW_SHOW);
 
 	m_pdlgBarcode = new CDlg_Barcode;
 	m_pdlgBarcode->SetBarcodeLength(Barcode_Leng, FALSE);
 	m_pdlgBarcode->Create(CDlg_Barcode::IDD, GetDesktopWindow());
 
 	UINT nIdx = 0;
 
 	m_pWndPtr[nIdx++] = (CWnd*)&m_wnd_MainView;			// Main 화면
  	//m_pWndPtr[nIdx++] = (CWnd*)&m_wnd_ManualView;		//
 	m_pWndPtr[nIdx++] = (CWnd*)&m_wnd_IOView;			// 
 	m_pWndPtr[nIdx++] = (CWnd*)&m_wnd_MaintenanceView;	// 
 	m_pWndPtr[nIdx++] = (CWnd*)&m_wnd_RecipeView;		// 모듈 설정
 	//m_pWndPtr[nIdx++] = (CWnd*)&m_wnd_AlarmView;		// 
 	m_pWndPtr[nIdx++] = (CWnd*)&m_wnd_LogView;			// 로그

 	//SetBackgroundColor (RGB(0x0, 0x0, 0x0));
 
 	// 초기 세팅
 	//CreateSplashScreen (this, IDB_BITMAP_Luritech);
 	InitUISetting ();
 	InitDeviceSetting();

	return 0;
}

//=============================================================================
// Method		: CView_MainCtrl::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

 	m_wnd_MainView.MoveWindow(0, 0, cx, cy);
 	//m_wnd_ManualView.MoveWindow(0, 0, cx, cy);
 	m_wnd_IOView.MoveWindow(0, 0, cx, cy);
 	m_wnd_MaintenanceView.MoveWindow(0, 0, cx, cy);
 	m_wnd_RecipeView.MoveWindow(0, 0, cx, cy);
 	//m_wnd_AlarmView.MoveWindow(0, 0, cx, cy);
 	m_wnd_LogView.MoveWindow(0, 0, cx, cy);
}

//=============================================================================
// Method		: CView_MainCtrl::OnEraseBkgnd
// Access		: protected 
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2010/10/13 - 10:40
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::OnEraseBkgnd(CDC* pDC)
{
	if (m_brBkgr.GetSafeHandle() == NULL)
	{
		return CWnd::OnEraseBkgnd(pDC);
	}

	ASSERT_VALID(pDC);

	CRect rectClient;
	GetClientRect(rectClient);

	if (m_brBkgr.GetSafeHandle() != NULL)
	{
		pDC->FillRect(rectClient, &m_brBkgr);
	}
	else
	{
		CWnd::OnEraseBkgnd(pDC);
	}

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::OnCtlColor
// Access		: protected 
// Returns		: HBRUSH
// Parameter	: CDC * pDC
// Parameter	: CWnd * pWnd
// Parameter	: UINT nCtlColor
// Qualifier	:
// Last Update	: 2010/10/12 - 17:35
// Desc.		:
//=============================================================================
HBRUSH CView_MainCtrl::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
#define AFX_MAX_CLASS_NAME 255
#define AFX_STATIC_CLASS _T("Static")
#define AFX_BUTTON_CLASS _T("Button")

		if (nCtlColor == CTLCOLOR_STATIC)
		{
			TCHAR lpszClassName [AFX_MAX_CLASS_NAME + 1];

			::GetClassName(pWnd->GetSafeHwnd(), lpszClassName, AFX_MAX_CLASS_NAME);
			CString strClass = lpszClassName;

			if (strClass == AFX_STATIC_CLASS)
			{
				pDC->SetBkMode(TRANSPARENT);
				return(HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
			}

			if (strClass == AFX_BUTTON_CLASS)
			{
				if ((pWnd->GetStyle() & BS_GROUPBOX) == 0)
				{
					pDC->SetBkMode(TRANSPARENT);
				}

				return(HBRUSH) ::GetStockObject(HOLLOW_BRUSH);
			}
		}
	}

	return CWnd::OnCtlColor(pDC, pWnd, nCtlColor);
}

//=============================================================================
// Method		: CView_MainCtrl::OnTimer
// Access		: protected 
// Returns		: void
// Parameter	: UINT_PTR nIDEvent
// Qualifier	:
// Last Update	: 2010/12/9 - 15:57
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnTimer(UINT_PTR nIDEvent)
{
	// 타이머 처리
	

	CWnd::OnTimer(nIDEvent);
}

//=============================================================================
// Method		: OnInitLogFolder
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/19 - 15:04
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnInitLogFolder()
{
	// 로그 처리
}

//=============================================================================
// Method		: CView_MainCtrl::InitConstructionSetting
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2010/12/13 - 15:13
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitConstructionSetting()
{

}

//=============================================================================
// Method		: CView_MainCtrl::InitUISetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2013/1/2 - 16:23
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitUISetting()
{

}

//=============================================================================
// Method		: CView_MainCtrl::InitDeviceSetting
// Access		: protected 
// Returns		: void
// Qualifier	:
// Last Update	: 2012/12/17 - 17:53
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitDeviceSetting()
{

}

//=============================================================================
// Method		: DisplayVideo
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in LPBYTE lpbyRGB
// Parameter	: __in DWORD dwRGBSize
// Parameter	: __in UINT nWidth
// Parameter	: __in UINT nHeight
// Qualifier	:
// Last Update	: 2018/2/1 - 13:15
// Desc.		:
//=============================================================================
void CView_MainCtrl::DisplayVideo(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight)
{

}

void CView_MainCtrl::DisplayVideo_LastImage(__in UINT nChIdx)
{

}

void CView_MainCtrl::DisplayVideo_NoSignal(__in UINT nChIdx)
{
	m_wnd_MainView.NoSignal_Ch(nChIdx);
	m_wnd_RecipeView.NoSignal_Ch(nChIdx);
	m_wnd_MaintenanceView.NoSignal_Ch(nChIdx);
}

//=============================================================================
// Method		: DisplayVideo_Overlay
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in enOverlayItem enItem
// Parameter	: __inout IplImage * TestImage
// Qualifier	:
// Last Update	: 2018/2/23 - 10:08
// Desc.		:
//=============================================================================
void CView_MainCtrl::DisplayVideo_Overlay(__in UINT nChIdx, __in enOverlayItem enItem, __inout IplImage *TestImage)
{

}

//=============================================================================
// Method		: LoadRecipeInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szRecipe
// Parameter	: BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2016/5/28 - 14:50
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::LoadRecipeInfo(__in LPCTSTR szRecipe, BOOL bNotifyModelWnd /*= TRUE*/)
{

	return TRUE;
}

//=============================================================================
// Method		: InitLoadRecipeInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/11 - 15:09
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitLoadRecipeInfo()
{

}

//=============================================================================
// Method		: LoadMotorInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMotor
// Parameter	: __in BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2017/10/2 - 14:04
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::LoadMotorInfo(__in LPCTSTR szMotor, __in BOOL bNotifyModelWnd /*= TRUE*/)
{
	// 모델 파일에서 모델 정보 불러오기

	return TRUE;
}


//=============================================================================
// Method		: InitLoaMotorInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/2 - 14:04
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitLoadMotorInfo()
{

}

//=============================================================================
// Method		: LoadMaintenanceInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szMaintenance
// Parameter	: __in BOOL bNotifyModelWnd
// Qualifier	:
// Last Update	: 2017/9/29 - 17:04
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::LoadMaintenanceInfo(__in LPCTSTR szMaintenance, __in BOOL bNotifyModelWnd /*= TRUE*/)
{
	// 모델 파일에서 모델 정보 불러오기

	return TRUE;
}

//=============================================================================
// Method		: InitLoadMaintenanceInfo
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/29 - 16:50
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitLoadMaintenanceInfo()
{

}

//=============================================================================
// Method		: Manual_DeviceControl
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nChIdx
// Parameter	: __in UINT nBnIdx
// Qualifier	:
// Last Update	: 2017/1/22 - 13:36
// Desc.		:
//=============================================================================
void CView_MainCtrl::Manual_DeviceControl(__in UINT nChIdx, __in UINT nBnIdx)
{
	
}

//=============================================================================
// Method		: OnSetCamerParaSelect
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/13 - 9:51
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnSetCamerParaSelect(__in UINT nParaIdx /*= 0*/)
{
	m_wnd_MainView.Set_CameraSelect(nParaIdx);
}

//=============================================================================
// Method		: CView_MainCtrl::SetBackgroundColor
// Access		: protected 
// Returns		: void
// Parameter	: COLORREF color
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2010/10/13 - 10:40
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetBackgroundColor(COLORREF color, BOOL bRepaint /*= TRUE*/)
{
	if (m_brBkgr.GetSafeHandle() != NULL)
	{
		m_brBkgr.DeleteObject();
	}

	if (color != (COLORREF)-1)
	{
		m_brBkgr.CreateSolidBrush(color);
	}

	if (bRepaint && GetSafeHwnd() != NULL)
	{
		Invalidate();
		UpdateWindow();
	}
}

//=============================================================================
// Method		: CView_MainCtrl::SwitchWindow
// Access		: public 
// Returns		: UINT
// Parameter	: UINT nIndex
// Qualifier	:
// Last Update	: 2010/11/26 - 14:06
// Desc.		: 자식 윈도우 전환하는 함수
// MainView에서 선택된 검사기 번호를 다른 윈도우로 넘긴다.
//=============================================================================
UINT CView_MainCtrl::SwitchWindow(UINT nIndex)
{
	if (m_nWndIndex == nIndex)
		return m_nWndIndex;

	UINT nOldView = m_nWndIndex;
	m_nWndIndex = nIndex;

	if (nOldView != -1)
	{
		if (m_pWndPtr[nOldView]->GetSafeHwnd())
		{
			m_pWndPtr[nOldView]->ShowWindow(SW_HIDE);
		}
	}

	if (m_pWndPtr[m_nWndIndex]->GetSafeHwnd())
	{
		m_pWndPtr[m_nWndIndex]->ShowWindow(SW_SHOW);
	}

	// 모델뷰 -> 메인뷰로 전환시 모델 데이터 갱신
	if ((SUBVIEW_RECIPE == nOldView) && (SUBVIEW_AUTO == m_nWndIndex))
	{
		m_wnd_RecipeView.InitOptionView();
	}

	if ((SUBVIEW_RECIPE == m_nWndIndex))
	{
		m_wnd_RecipeView.InitOptionView();
		//m_wnd_RecipeView.SetRecipeFile(m_stInspInfo.RecipeInfo.szRecipeFile); //??
	}

	return m_nWndIndex;
}

//=============================================================================
// Method		: CView_MainCtrl::SetCommPanePtr
// Access		: public 
// Returns		: void
// Parameter	: CWnd * pwndCommPane
// Qualifier	:
// Last Update	: 2013/7/16 - 16:51
// Desc.		:
//=============================================================================
void CView_MainCtrl::SetCommPanePtr(CWnd* pwndCommPane)
{
	m_pwndCommPane = pwndCommPane;
}

//=============================================================================
// Method		: ReloadOption
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2014/8/3 - 22:27
// Desc.		:
//=============================================================================
void CView_MainCtrl::ReloadOption()
{

}

//=============================================================================
// Method		: CView_MainCtrl::InitStartProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2014/7/5 - 10:49
// Desc.		:
//=============================================================================
void CView_MainCtrl::InitStartProgress()
{

}

//=============================================================================
// Method		: InitStartDeviceProgress
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/12 - 22:11
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::InitStartDeviceProgress()
{

	return TRUE;
}

//=============================================================================
// Method		: CView_MainCtrl::FinalExitProgress
// Access		: public 
// Returns		: void
// Qualifier	:
// Last Update	: 2016/06/13
// Desc.		: 프로그램 종료시 처리해야 할 코드들..
//=============================================================================
void CView_MainCtrl::FinalExitProgress()
{

}

//=============================================================================
// Method		: ManualBarcode
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/11/10 - 13:28
// Desc.		:
//=============================================================================
void CView_MainCtrl::ManualBarcode()
{

}

//=============================================================================
// Method		: MotorOrigin
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/11/7 - 21:55
// Desc.		:
//=============================================================================
BOOL CView_MainCtrl::MotorOrigin()
{
	BOOL  bReslut = FALSE;


	return bReslut;
}

//=============================================================================
// Method		: ChangeMESOnlineMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enMES_Online nOnlineMode
// Qualifier	:
// Last Update	: 2018/3/1 - 10:31
// Desc.		:
//=============================================================================
void CView_MainCtrl::ChangeMESOnlineMode(__in enMES_Online nOnlineMode)
{

}

//=============================================================================
// Method		: OnManual_OneItemTest
// Access		: virtual public  
// Returns		: void
// Parameter	: UINT nStepIdx
// Parameter	: UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/25 - 11:17
// Desc.		:
//=============================================================================
void CView_MainCtrl::OnManual_OneItemTest(UINT nStepIdx, UINT nParaIdx)
{

}

//=============================================================================
// Method		: CView_MainCtrl::Test_Process
// Access		: public 
// Returns		: void
// Parameter	: UINT nTestNo
// Qualifier	:
// Last Update	: 2014/7/10 - 9:54
// Desc.		:
//=============================================================================
void CView_MainCtrl::Test_Process( UINT nTestNo )
{
	CFile_Report fileReport;

	switch (nTestNo)
	{
	case 0:
	{
		//UINT nParaIdx = Para_Left;
// 		  UINT nParaIdx = Para_Left;
// 
// 		m_wnd_MainView.Set_CameraSelect(nParaIdx);
// 		
// 		OnCameraBrd_PowerOnOff(enPowerOnOff::Power_On, nParaIdx);
// 
// 		// 레지스터 세팅
// 		m_Device.DAQ_LVDS.Set_UseAutoI2CWrite(nParaIdx, FALSE);
// 	//	m_Device.DAQ_LVDS.SetI2CFilePath(nParaIdx, _T("C:/_EQP/I2C/LG_OMS_Entry_Camera.set"));
// 		//m_Device.DAQ_LVDS.SetI2CFilePath(nParaIdx, _T("C:/Temp/LG_OMS_Entry_C1_reg_data_ALPHA_1600.set"));
// 		//m_Device.DAQ_LVDS.SetI2CFilePath(nParaIdx, _T("C:/_EQP/I2C/LGIT_MRA2_Camera.set"));
// 		//m_Device.DAQ_LVDS.Set_RGB_Exposure(nParaIdx, 0.0625f);
// 		//m_Device.DAQ_LVDS.Set_RGB_ContrastBrightness(nParaIdx, 100, 150);	// Contrast : -50 < 0 < 100	// Brightness : -150 < 0 < 150
// 		
// 		OnDAQ_SensorInit_Unit(nParaIdx);
// 
// 		OnDAQ_CaptureStart(nParaIdx);

	}
		break;

	case 1:
	{
// 		UINT nParaIdx = Para_Left;
// 		//UINT nParaIdx = Para_Right;
// 
// 		OnDAQ_CaptureStop(nParaIdx);
// 
// 		OnCameraBrd_PowerOnOff(enPowerOnOff::Power_Off);
		//AutomaticProcess_LoadUnload(TRUE, Para_Left);
	}
		break;

	case 2:
	{
//			  StartOperation_LoadUnload(TRUE);
	}
		break;

	case 3:
	{
// 		ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(0);
// 		LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(0);
// 
// 		// 이미지 버퍼에 복사
// 		memcpy(m_stImageBuf[0].lpwImage_16bit, lpwImage, pstVideo->m_dwWidth * pstVideo->m_dwHeight * sizeof(WORD));
// 
// 		CString szPath;
// 		if (1 < g_InspectorTable[m_InspectionType].Grabber_Cnt)
// 			szPath.Format(_T("%s/Cap_%s_Step%02d.png"), m_stInspInfo.Path.szImage, g_szParaName[0], 0);
// 		else
// 			szPath.Format(_T("%s/Cap_Step%02d.png"), m_stInspInfo.Path.szImage, 0);
// 
// 		OnSaveImage_Gray16(szPath.GetBuffer(0), m_stImageBuf[0].lpwImage_16bit, m_stImageBuf[0].dwWidth, m_stImageBuf[0].dwHeight);
	}
		break;

	case 4:
	{
//		m_wnd_MainView.Clear_ImageHistory(0);
//		_TI_Cm_Initialize(0);
	}
		break;

	case 5:
	{
// 		m_stImageBuf.ResetData();
// 
// 		CString szLabel;
// 		for (UINT nIdx = 0; nIdx < 11; nIdx++)
// 		{
// 			szLabel.Format(_T("Step_%02d"), nIdx);
// 			OnImage_AddHistory(0, szLabel);
// 			  
// 			Delay(500);
// 		}
		//_TI_Cm_Finalize(0);
//		OnDAQ_EEPROM_OMS_Entry_2D();
	}
		break;

	case 6:
	{

	}
		break;

	case 7:
	{

	}
		break;
	case 8:
	{

	}
		break;
	case 9:
	{

	}
		break;
	case 10:
	{
		TRACE(_T("sizeof(ST_2DCal_Para): %d (112)\n"), sizeof(ST_2DCal_Para));
		TRACE(_T("sizeof(ST_2DCal_CornerPt): %d (321)\n"), sizeof(ST_2DCal_CornerPt));
		TRACE(_T("sizeof(ST_2DCal_Result): %d (74)\n"), sizeof(ST_2DCal_Result));
		TRACE(_T("sizeof(ST_3DCal_Depth_Para): %d(88)\n"), sizeof(ST_3DCal_Depth_Para));
		TRACE(_T("sizeof(ST_3DCal_Depth_Result): %d(156)\n"), sizeof(ST_3DCal_Depth_Result));
		TRACE(_T("sizeof(ST_3DCal_Eval_Para): %d(84)\n"), sizeof(ST_3DCal_Eval_Para));
		TRACE(_T("sizeof(ST_3DCal_Eval_Result): %d(392)\n"), sizeof(ST_3DCal_Eval_Result));
	}
		break;
	default:
		break;
	}
}

void CView_MainCtrl::Test_SNR()
{
// 	CFileFind finder;
// 	CStringArray	szarFiles;
// 
// 	// build a string with wildcards
// 	CString strWildcard(_T("C:/Temp/Image/SNR"));
// 	strWildcard += _T("/*.bmp");
// 
// 	// start working for files
// 	BOOL bWorking = finder.FindFile(strWildcard);
// 
// 	while (bWorking)
// 	{
// 		bWorking = finder.FindNextFile();
// 
// 		if (finder.IsDots())
// 			continue;
// 
// 		if (finder.IsDirectory())
// 			continue;
// 
// 
// 		CString szFile = finder.GetFilePath();
// 		szarFiles.Add(szFile);
// 	}
// 
// 	finder.Close();
// 
// 	CStringA szImagePath;
// 	CTI_SNR	 ti_SNR;
// 	// * 알고리즘 구동
// 	for (UINT nIdx = 0; nIdx < szarFiles.GetCount(); nIdx++)
// 	{
// 		// mra2, ikc
// 		USES_CONVERSION;
// 		szImagePath = CT2A(szarFiles[nIdx].GetBuffer(0));
// 
// 		// 패스의 파일 열기
// 		IplImage* pImage = cvLoadImage(szImagePath.GetBuffer(0));
// 		LPWORD	lpwGray12 = NULL;
// 		if (NULL != pImage)
// 		{
//  			lpwGray12 = new WORD[pImage->width * pImage->height];
//  			memset(lpwGray12, 0, pImage->width * pImage->height * 2);
//  
// 			register LPBYTE src_start = (LPBYTE)pImage->imageData;
// 			register const LPBYTE src_stop = src_start + (pImage->width * pImage->height * 3);
// 			register LPWORD pDest = lpwGray12;
// 
// 			unsigned short value16 = 0;
// 			for (; src_start < src_stop; )
// 			{
// 				value16 = (WORD)(*src_start);
// 
// 				*(pDest++) = value16;
// 
// 				src_start++;
// 				src_start++;
// 				src_start++;
// 			}
// 
// 			// 알고리즘 roi
// 			// 알고리즘 구동
// 			ST_CAN_ROI	stROI;
// 			unsigned short sSignal = 0;
// 			float fNoice = 0;
// 
// 			if (800 < pImage->height)
// 			{
// 				stROI.sLeft		= 320;
// 				stROI.sTop		= 242;
// 				stROI.sWidth	= 640;
// 				stROI.sHeight	= 480;
// 			}
// 			else
// 			{
// 				stROI.sLeft		= 320;
// 				stROI.sTop		= 122;
// 				stROI.sWidth	= 640;
// 				stROI.sHeight	= 480;
// 			}
// 			
// 
// 			ti_SNR.SNR_Test(stROI, sSignal, fNoice, lpwGray12, pImage->width, pImage->height);
// 
// 			// 결과 저장
// 			// 파일명, db 값
// 			//TRACE(_T("File : %s,%d,%d\n"), szarFiles[nIdx].GetBuffer(0), pImage->width, pImage->height);
// 			//TRACE(_T("File : %s,%d,%.4f\n"), szarFiles[nIdx].GetBuffer(0), sSignal, fNoice);
// 
// 			 double dbValue = 20.0 * log10(((float)sSignal) / fNoice);
// 
// 			 TRACE(_T("File : %s,%d,%.6f,%.6f,%d,%d,%d,%d \n"), szarFiles[nIdx].GetBuffer(0), sSignal, fNoice, dbValue, stROI.sLeft, stROI.sTop, stROI.sWidth, stROI.sHeight);
// 			
// 		}
// 
// 		if (NULL != lpwGray12)
// 		{
// 			delete[] lpwGray12;
// 		}
// 
// 		cvReleaseImage(&pImage);
// 	}
}
