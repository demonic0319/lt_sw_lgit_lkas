//*****************************************************************************
// Filename	: 	TestManager_EQP_IR_ImgT.h
// Created	:	2018/1/27 - 13:44
// Modified	:	2018/1/27 - 13:44
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
#ifndef TestManager_EQP_IR_ImgT_h__
#define TestManager_EQP_IR_ImgT_h__

#pragma once

#include "TestManager_EQP.h"
#include "Def_Mes_IR_ImgT.h"
#include "TestManager_TestMES_LKAS.h"
//=============================================================================
// CTestManager_EQP_IR_ImgT
//=============================================================================
class CTestManager_EQP_IR_ImgT : public CTestManager_EQP
{
public:
	CTestManager_EQP_IR_ImgT();
	virtual ~CTestManager_EQP_IR_ImgT();

protected:

	//-----------------------------------------------------
	// 옵션
	//-----------------------------------------------------
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();	
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);


	//-----------------------------------------------------
	// 쓰레드 작업 처리
	//-----------------------------------------------------

	// Load/Unload 쓰레드 시작
	virtual LRESULT	StartOperation_LoadUnload			(__in BOOL bLoad);
	// 검사 쓰레드 시작
	virtual LRESULT	StartOperation_Inspection			(__in UINT nParaIdx = 0);
	// 수동 모드일때 자동화 처리용 함수
	virtual LRESULT	StartOperation_Manual				(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 수동 모드일때 검사항목 처리용 함수
	virtual LRESULT	StartOperation_InspManual			(__in UINT nParaIdx = 0);

	// 자동으로 처리되고 있는 쓰레드 강제 종료
	virtual void	StopProcess_LoadUnload				();
	virtual void	StopProcess_Test_All				();
	virtual void	StopProcess_InspManual				(__in UINT nParaIdx = 0);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual BOOL	AutomaticProcess_LoadUnload			(__in BOOL bLoad);
	virtual void	AutomaticProcess_Test_All			(__in UINT nParaIdx = 0);
	virtual void	AutomaticProcess_Test_InspManual	(__in UINT nParaIdx = 0);

	// 팔레트 로딩/언로딩 초기 세팅
	virtual void	OnInitial_LoadUnload				(__in BOOL bLoad);
	// 팔레트 로딩/언로딩 종료 세팅
	virtual void	OnFinally_LoadUnload				(__in BOOL bLoad);
	// 개별 검사 작업 시작
	virtual LRESULT	OnStart_LoadUnload					(__in BOOL bLoad);

	// 전체 테스트 초기 세팅
	virtual void	OnInitial_Test_All					(__in UINT nParaIdx = 0);
	// 전체 테스트 종료 세팅
	virtual void	OnFinally_Test_All					(__in LRESULT lResult, __in UINT nParaIdx = 0);
	// 전체 검사 작업 시작
	virtual LRESULT	OnStart_Test_All					(__in UINT nParaIdx = 0);

	// 개별 테스트 동작항목
	virtual void	OnInitial_Test_InspManual			(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	virtual void	OnFinally_Test_InspManual			(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx = 0);
	virtual LRESULT	OnStart_Test_InspManual				(__in UINT nTestItemID, __in UINT nParaIdx = 0);
	
	// 검사
	virtual LRESULT Load_Product						();
	virtual LRESULT Unload_Product						();

	// 검사기별 검사
	virtual LRESULT	StartTest_Equipment					(__in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_ImageTest					(__in UINT nParaIdx = 0);

	// 쓰레드 내에서 처리될 자동 작업 함수
	virtual void	AutomaticProcess_TestItem			(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep = 0);

	virtual LRESULT	StartTest_ImageTest_TestItem		(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	StartTest_Particle_TestItem			(__in UINT nTestItemID, __in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 공용 검사항목
	virtual	LRESULT _TI_Cm_Initialize					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize						(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_Finalize_Error				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurCurrent				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_MeasurVoltage				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual	LRESULT _TI_Cm_CaptureImage					(__in UINT nStepIdx, __in UINT nParaIdx = 0, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);
	virtual	LRESULT _TI_Cm_CameraRegisterSet			(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx = 0);
	virtual LRESULT _TI_Cm_CameraAlphaSet				(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx = 0);
	virtual LRESULT _TI_Cm_CameraPowerOnOff				(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx = 0);
	
	BOOL _TI_Get_ImageBuffer_CaptureImage				(__in enImageMode eImageMode , __in UINT nParaIdx , __out int &iImgW, __out int &iImgH, __out CString &strTestFileName, __in BOOL bSaveImage = FALSE, __in LPCTSTR szFileName = NULL);

	// IQ 검사 항목 ( Manual )
	virtual LRESULT	_TI_IR_ImgT_MoveStage_Load			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_MoveStage_Chart			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_MoveStage_Particle		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_MoveStage_Vision		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_MoveStage_Displace		(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_MoveStage_TiltModify	(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT _TI_IR_ImgT_MoveChart_Index			(__in UINT nStepIdx, __in UINT nParaIdx = 0);	
	virtual LRESULT	_TI_IR_ImgT_Pro_Vision				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_Displace			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_Current				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_IIC					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_SFR					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_Ymean				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_LCB					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_BlackSpot			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_Shading				(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_DynamicBW			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_RI					(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_Distortion			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_DefectBlack			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_DefectWhite			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	virtual LRESULT	_TI_IR_ImgT_Pro_OpticalCenter		(__in UINT nStepIdx, __in UINT nParaIdx = 0);

	// 검사 결과 표시
	virtual LRESULT _TI_IR_ImgT_JudgeResult				(__in UINT nStepIdx, __in UINT nTestItem, __in UINT nParaIdx = 0);

	// 검사 최종 판정
	enTestResult	Judgment_Inspection_All				();

	// MES 통신
	virtual LRESULT	MES_CheckBarcodeValidation			(__in UINT nParaIdx = 0);
	virtual LRESULT MES_SendInspectionData				(__in UINT nParaIdx = 0);
	virtual LRESULT MES_MakeInspecData_LKAS				(__in UINT nParaIdx = 0);

	// 검사 결과에 따른 메세지 팝업
	virtual void	OnPopupMessageTest_All				(__in enResultCode nResultCode);

	//-----------------------------------------------------
	// Motion
	//-----------------------------------------------------	
	virtual LRESULT	OnMotion_MoveToTestPos				(__in UINT nParaIdx = 0);
	virtual	LRESULT OnMotion_MoveToStandbyPos			(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_MoveToUnloadPos			(__in UINT nParaIdx = 0);

	virtual LRESULT	OnMotion_Origin						();
	
	virtual LRESULT	OnMotion_LoadProduct				();
	virtual LRESULT	OnMotion_UnloadProduct				();
	virtual LRESULT	OnMotion_VisionTestProduct			(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_DisplaceTestProduct		(__in int iLoopmode, __in int iStep, __in UINT nSeq, __in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_TiltModifyProduct			(__in double dTiltx, __in double dTilty, __in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_ParticleTestProduct		(__in UINT nParaIdx = 0);
	virtual LRESULT	OnMotion_ChartTestProduct			(__in UINT nParaIdx = 0);

	
	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	// Digital I/O 신호 UI 처리
	virtual void	OnDIO_UpdateDInSignal				(__in BYTE byBitOffset, __in BOOL bOnOff){};
	virtual void	OnDIO_UpdateDOutSignal				(__in BYTE byBitOffset, __in BOOL bOnOff){};
	// I/O 신호 감지 및 신호별 기능 처리
	virtual void	OnDIn_DetectSignal					(__in BYTE byBitOffset, __in BOOL bOnOff);
	virtual void	OnDIn_DetectSignal_IR_ImgT			(__in BYTE byBitOffset, __in BOOL bOnOff);
	// Digital I/O 초기화
	virtual void	OnDIO_InitialSignal					();

	virtual LRESULT	OnDIn_MainPower						();
	virtual LRESULT	OnDIn_EMO							();
	virtual LRESULT	OnDIn_AreaSensor					();
	virtual LRESULT	OnDIn_DoorSensor					();
	virtual LRESULT	OnDIn_JIGCoverCheck					();
	virtual LRESULT	OnDIn_Start							();
	virtual LRESULT	OnDIn_Stop							();
	virtual LRESULT	OnDIn_CheckSafetyIO					();
	virtual LRESULT	OnDIn_CheckJIGCoverStatus			();
		
	virtual LRESULT	OnDOut_BoardPower					(__in BOOL bOn, __in UINT nParaIdx = 0);
	virtual LRESULT	OnDOut_FluorescentLamp				(__in BOOL bOn);
	virtual LRESULT	OnDOut_StartLamp					(__in BOOL bOn);
	virtual LRESULT	OnDOut_StopLamp						(__in BOOL bOn);
	virtual LRESULT	OnDOut_TowerLamp					(__in enLampColor nLampColor, __in BOOL bOn);
	virtual LRESULT	OnDOut_Buzzer						(__in BOOL bOn);

	// DAQ 보드
	virtual LRESULT	OnDAQ_EEPROM_Write					(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Entry_IR_ImgT		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_OMS_Front_IR_ImgT		(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_MRA2_IR_ImgT			(__in UINT nIdxBrd = 0);
	virtual LRESULT	OnDAQ_EEPROM_IKC_IR_ImgT			(__in UINT nIdxBrd = 0);

	//-----------------------------------------------------
	// 카메라 보드 / 광원 제어 보드
	//-----------------------------------------------------
 	

	//-----------------------------------------------------
	// 메뉴얼 테스트
	//-----------------------------------------------------
	virtual LRESULT OnManualSequence				(__in UINT nParaID, __in enParaManual enFunc);
	virtual	LRESULT OnManualTestItem				(__in UINT nParaID, __in UINT nStepIdx);

	//-----------------------------------------------------
	// 테스트
	//-----------------------------------------------------
	// 주변기기 통신 상태 체크
	virtual LRESULT	OnCheck_DeviceComm				();
	// 기구물 안전 체크
	virtual LRESULT	OnCheck_SaftyJIG				();	
	// 모델 설정 데이터가 정상인가 판단
	virtual LRESULT	OnCheck_RecipeInfo				();	
	
	// 제품 결과 판정 및 리포트 처리
	virtual void	OnJugdement_And_Report			(__in UINT nParaIdx = 0);
	virtual void	OnJugdement_And_Report_All		();
	
	//-----------------------------------------------------
	// 타이머 
	//-----------------------------------------------------
	virtual void	CreateTimer_UpdateUI			(__in DWORD DueTime = 5000, __in DWORD Period = 250);
	virtual void	DeleteTimer_UpdateUI			();
	virtual void	OnMonitor_TimeCheck				();
	virtual void	OnMonitor_UpdateUI				();
	
	//-----------------------------------------------------
	// Worklist 처리
	//-----------------------------------------------------
	virtual void	OnSaveWorklist					();
	
	//-----------------------------------------------------
	// Overlay Info
	//-----------------------------------------------------
	virtual void	OnSetOverlayInfo_IR_ImgT			(__in UINT enTestItem){};

	//-----------------------------------------------------
	// 멤버 변수 & 클래스
	//-----------------------------------------------------
	int m_iSelectTest[12];

	// 유저 Stop 처리 변수
	BOOL				m_bFlag_UserStop = FALSE;
	CLG_LGIT_Algorithm     m_LGAlgorithm;
	void _TI_IR_ImgT_SFR_LGAl();
	void _TI_IR_ImgT_Ymean_LGAl();
	void _TI_IR_ImgT_Shading_LGAl();
	int _TI_IR_ImgT_SNR_LGAl();

	bool RCCC2CCCC(LPBYTE src, LPBYTE dst, int iW, int iH, int iDepth, int nType);


public:
	// 생성자 처리용 코드
	virtual void		OnInitialize				();
	// 소멸자 처리용 코드
	virtual	void		OnFinalize					();

	// 제어 권한 상태 설정
	virtual void		SetPermissionMode			(__in enPermissionMode nAcessMode);
	
	
	void TestResultView(UINT nTestID, UINT nPara, LPVOID pParam);
	void TestResultView_Reset(UINT nTestID, UINT nPara);


	virtual void   OnSet_TestResult_Total(__in enTestResult nResult){};


	void EachTest_ResultDataSave(UINT nTestID, UINT nPara);
	void TotalTest_ResultDataSave(UINT nPara);

	BOOL Save_ResultData(UINT nTestID, UINT nPara);
	virtual void OnReset_CamInfo(__in UINT nParaIdx = 0);

	ST_MES_Data_IR_IKC m_stMesData_IKC;
	ST_MES_Data_IR_MRA2 m_stMesData_MRA2;
	ST_MES_Data_LKAS m_stMesData_LKAS;

	CTestManager_TestMES_LKAS m_TestMgr_TestMes;

	BOOL bflagChartest = FALSE;
	BOOL bflagDryRun = FALSE;

	UINT m_nRegisterChangeMode;
	
	// tiltx,y
	BOOL	m_bRecvTilt;
	double	m_dRecvTiltX;
	double	m_dRecvTiltY;

};

#endif // TestManager_EQP_IR_ImgT_h__
