﻿// Wnd_MotionCtr.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotionCtr.h"

// CWnd_MotionCtr

IMPLEMENT_DYNAMIC(CWnd_MotionCtr, CWnd)

typedef enum MotionCtr_ID
{
	IDC_MC_ED_JOG_SPEED = 1000,
	IDC_MC_ED_STEP_POS,
	IDC_MC_ED_REPEAT_COUNT,
	IDC_MC_ED_REPEAT_READ_COUNT,
	IDC_MC_ED_REPEAT_DELAY,
	IDC_MC_ED_REPEAT_POINT1,
	IDC_MC_ED_REPEAT_POINT2,
	IDC_MC_ED_STEP_SEMPLE_POS,
	IDC_MC_BN_JOG_MINUS,
	IDC_MC_BN_JOG_PLUS,
	IDC_MC_BN_ABS_MOVE,
	IDC_MC_BN_REL_MOVE,
	IDC_MC_BN_REPEAT_MOVE,
	IDC_MC_BN_REPEAT_STOP,
	IDC_MC_BN_MANUAL_ESTOP,
	IDC_MC_BN_MANUAL_SSTOP,
	IDC_MC_BN_MANUAL_CLEAR,
	IDC_MC_MC_MAXNUM,
};

CWnd_MotionCtr::CWnd_MotionCtr()
{
	m_pstDevice				= NULL;
	m_nAxisNum				= 0;
	m_bRepeatStop			= FALSE;
	m_bRepeatStop_Flag		= TRUE;
	m_bEStop_Flag			= TRUE;
	m_bSStop_Flag			= TRUE;

	VERIFY(m_font.CreateFont(
		14,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MotionCtr::~CWnd_MotionCtr()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_MotionCtr, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_MC_BN_ABS_MOVE, OnBnClickedBnAbsMove)
	ON_BN_CLICKED(IDC_MC_BN_REL_MOVE, OnBnClickedBnRelMove)
	ON_BN_CLICKED(IDC_MC_BN_REPEAT_MOVE, OnBnClickedBnRepeatStart)
	ON_BN_CLICKED(IDC_MC_BN_MANUAL_CLEAR, OnBnClickedBnClear)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_MotionCtr::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Name.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Name.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Name.Create(_T("MOTOR MANUL CONTROL"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < ST_MC_JOG_SPEED; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 11.5F);
		m_st_Item[nIdx].Create(g_szMotionCtrTitleName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = ST_MC_JOG_SPEED; nIdx < ST_MC_MAXNUM; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].Create(g_szMotionCtrTitleName[nIdx], dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < ED_MC_MAXNUM; nIdx++)
	{
		DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_CHILD;
		m_ed_Item[nIdx].Create(dwEtStyle, CRect(0, 0, 0, 0), this, IDC_MC_ED_JOG_SPEED + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
	}

	for (UINT nIdx = 0; nIdx < ED_MC_MAXNUM; nIdx++)
		m_ed_Item[nIdx].SetValidChars(_T("0123456789-"));

	m_ed_Item[ED_MC_JOG_SPEED].SetWindowText(_T("1"));
	m_ed_Item[ED_MC_JOG_SPEED].SetValidChars(_T("01234567"));
	m_ed_Item[ED_MC_REPEAT_DLY].SetValidChars(_T("0123456789"));
	m_ed_Item[ED_MC_REPEAT_CNT].SetValidChars(_T("0123456789"));

	for (UINT nIdx = 0; nIdx < BN_MC_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szMotionCtrButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_MC_BN_JOG_MINUS + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iSpacing = 5;
	int iLeft	 = 0;
	int iTop	 = 0;
	int iHeight  = cy;
	int iTitleW  = (cx - iSpacing * 2) / 3;
	int iTitleH = 35;
	int iCtrlHeight = (iHeight - iTitleH - (iSpacing * 6)) / 6;
	int iLeft_2nd = iLeft + iSpacing + iTitleW;
	int iLeft_3rd = iLeft_2nd + iSpacing + iTitleW;
	int iWidth_MMBn = cx - (iTitleW * 2) - (iSpacing * 2);

	//m_gb_Item.MoveWindow(0, 0, cx, cy);
	m_st_Name.MoveWindow(0, iTop, cx, iTitleH);

	iTop += iTitleH + iSpacing;
	iLeft = 0;
	m_st_Item[ST_MC_JOG_NAME].MoveWindow(iLeft, iTop, iTitleW, iCtrlHeight);

	iLeft += iTitleW + iSpacing;
	m_st_Item[ST_MC_REPEAT_NAME].MoveWindow(iLeft_2nd, iTop, iTitleW, iCtrlHeight);

	iLeft += iTitleW + iSpacing;
	m_st_Item[ST_MC_MANUAL_CTR].MoveWindow(iLeft_3rd, iTop, iWidth_MMBn, iCtrlHeight);
	
	iLeft = 0;
	iTop += iCtrlHeight + iSpacing;	
	int iTitleSubW = iTitleW / 2;
	m_st_Item[ST_MC_JOG_SPEED].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_ed_Item[ED_MC_JOG_SPEED].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft = iLeft_2nd;
	m_st_Item[ST_MC_REPEAT_CNT].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_ed_Item[ED_MC_REPEAT_CNT].MoveWindow(iLeft, iTop, iTitleSubW / 2 - 1, iCtrlHeight);

	iLeft += iTitleSubW / 2;
	m_st_Item[ST_MC_REPEAT_READ_CNT].MoveWindow(iLeft, iTop, iTitleSubW / 2, iCtrlHeight);

	m_bn_Item[BN_MC_MANUAL_ESTOP].MoveWindow(iLeft_3rd, iTop, iWidth_MMBn, iCtrlHeight * 2 + iSpacing);

	iLeft = 0;
	iTop += iCtrlHeight + iSpacing;
	m_bn_Item[BN_MC_MINUS_MOVE].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_bn_Item[BN_MC_PLUS_MOVE].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft = iLeft_2nd;
	m_st_Item[ST_MC_REPEAT_DLY].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_ed_Item[ED_MC_REPEAT_DLY].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft = 0;
	iTop += iCtrlHeight + iSpacing;
	m_st_Item[ST_MC_STEP_NAME].MoveWindow(iLeft, iTop, iTitleW, iCtrlHeight);

	iLeft = iLeft_2nd;
	m_st_Item[ST_MC_REPEAT_1P].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);
	
	iLeft += iTitleSubW + 2;
	m_ed_Item[ED_MC_REPEAT_P1].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	m_bn_Item[BN_MC_MANUAL_SSTOP].MoveWindow(iLeft_3rd, iTop, iWidth_MMBn, iCtrlHeight * 2 + iSpacing);

	iLeft = 0;
	iTop += iCtrlHeight + iSpacing;
	m_st_Item[ST_MC_STEP_POS].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_ed_Item[ED_MC_STEP_POS].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft = iLeft_2nd;
	m_st_Item[ST_MC_REPEAT_2P].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_ed_Item[ED_MC_REPEAT_P2].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft = 0;
	iTop += iCtrlHeight + iSpacing;
	m_bn_Item[BN_MC_ABS_MOVE].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_bn_Item[BN_MC_REL_MOVE].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft = iLeft_2nd;
	m_bn_Item[BN_MC_REPEAT_START].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	iLeft += iTitleSubW + 2;
	m_bn_Item[BN_MC_REPEAT_STOP].MoveWindow(iLeft, iTop, iTitleSubW - 1, iCtrlHeight);

	m_bn_Item[BN_MC_MANUAL_CLEAR].MoveWindow(iLeft_3rd, iTop, iWidth_MMBn, iCtrlHeight);
}

//=============================================================================
// Method		: OnBnClickedBnAbsMove
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 16:27
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnAbsMove()
{
	if (m_pstDevice == NULL)
		return;

	CString str;
	double dbPos = 0.0;

	m_ed_Item[ED_MC_STEP_POS].GetWindowText(str);
	dbPos = _ttof(str);

	m_pstDevice->MotorAxisMove(POS_ABS_MODE, m_nAxisNum, dbPos);
}

//=============================================================================
// Method		: OnBnClickedBnRelMove
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 16:27
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnRelMove()
{
	CString str;
	double dbPos = 0.0;

	m_ed_Item[ED_MC_STEP_POS].GetWindowText(str);
	dbPos = _ttof(str);

	m_pstDevice->MotorAxisMove(POS_REL_MODE, m_nAxisNum, dbPos);
}

//=============================================================================
// Method		: OnBnClickedBnRepeatStart
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/29 - 16:27
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnRepeatStart()
{
	if (m_pstDevice == NULL)
		return;

	CString str;
	UINT nCount		= 0;
	UINT nDelay		= 0;
	double dbAPointPos = 0.0;
	double dbBPointPos = 0.0;
	m_bRepeatStop = FALSE;

	m_ed_Item[ED_MC_REPEAT_CNT].GetWindowText(str);
	nCount = _ttoi(str);

	m_ed_Item[ED_MC_REPEAT_DLY].GetWindowText(str);
	nDelay = _ttoi(str);

	m_ed_Item[ED_MC_REPEAT_P1].GetWindowText(str);
	dbAPointPos = _ttof(str);

	m_ed_Item[ED_MC_REPEAT_P2].GetWindowText(str);
	dbBPointPos = _ttof(str);

	m_st_Item[ST_MC_REPEAT_READ_CNT].SetWindowText(_T("0"));

	for (UINT nCnt = 0; nCnt < nCount; nCnt++)
	{
		// 시작 POS
		if (!m_pstDevice->MotorAxisMove(POS_ABS_MODE, m_nAxisNum, dbAPointPos))
			return;

		// 대기 시간
		m_pstDevice->DoEvents(nDelay);

		// STOP 을 누를 경우 
		if (m_bRepeatStop)
			break;

		// 종료 POS
		if (!m_pstDevice->MotorAxisMove(POS_ABS_MODE, m_nAxisNum, dbBPointPos))
			return;

		// 대기 시간
		m_pstDevice->DoEvents(nDelay);

		str.Format(_T("%d"), nCnt + 1);
		m_st_Item[ST_MC_REPEAT_READ_CNT].SetWindowText(str);

		// STOP 을 누를 경우 
		if (m_bRepeatStop)
			break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/31 - 15:53
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::OnBnClickedBnClear()
{
	if (m_pstDevice == NULL)
		return;

	m_bRepeatStop = TRUE;
	m_pstDevice->SetMotorPosClear(m_nAxisNum);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_MotionCtr::PreTranslateMessage(MSG* pMsg)
{
	CString str;
	UINT nSpeed = 0;

	m_ed_Item[ED_MC_JOG_SPEED].GetWindowText(str);
	nSpeed = _ttoi(str);

	switch (pMsg->message)
	{
	case WM_KEYDOWN:
		if ((::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 86)) // Ctrl + V
		{
			CWnd* pWnd = CWnd::FromHandle(pMsg->hwnd);
			pWnd->SetWindowText(_T(""));
			pMsg->message = WM_PASTE;
		}

		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 67)  // Ctrl + C
			pMsg->message = WM_COPY; 

// 		if (::GetKeyState(VK_CONTROL) < 0 && pMsg->wParam == 90)  // Ctrl + Z
// 			pMsg->message = WM_UNDO;

		break;

	case WM_LBUTTONDOWN:
		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_MINUS)->GetSafeHwnd())
			m_pstDevice->MotorJogMove(m_nAxisNum, 0, nSpeed);

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_PLUS)->GetSafeHwnd())
			m_pstDevice->MotorJogMove(m_nAxisNum, 1, nSpeed);

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_REPEAT_STOP)->GetSafeHwnd())
		{
			if (m_bRepeatStop_Flag == TRUE)
			{
				m_bRepeatStop_Flag = FALSE;
				m_bRepeatStop = TRUE;
			}
		}

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_ESTOP)->GetSafeHwnd())
		{
			if (m_bEStop_Flag == TRUE)
			{
				m_bEStop_Flag = FALSE;

				if (m_pstDevice != NULL)
				{
					m_bRepeatStop = TRUE;
					m_pstDevice->SetMotorEStop(m_nAxisNum);
				}
			}
		}

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_SSTOP)->GetSafeHwnd())
		{
			if (m_bSStop_Flag == TRUE)
			{
				m_bSStop_Flag = FALSE;

				if (m_pstDevice != NULL)
				{
					m_bRepeatStop = TRUE;
					m_pstDevice->SetMotorSStop(m_nAxisNum);
				}
			}
		}

		break;

	case WM_LBUTTONUP:
		if ((pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_MINUS)->GetSafeHwnd()) || (pMsg->hwnd == GetDlgItem(IDC_MC_BN_JOG_PLUS)->GetSafeHwnd()))
			m_pstDevice->SetMotorSStop(m_nAxisNum);

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_REPEAT_STOP)->GetSafeHwnd())
			m_bRepeatStop_Flag = TRUE;

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_ESTOP)->GetSafeHwnd())
			m_bEStop_Flag = TRUE;

		if (pMsg->hwnd == GetDlgItem(IDC_MC_BN_MANUAL_SSTOP)->GetSafeHwnd())
			m_bSStop_Flag = TRUE;

		break;
	}

	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_MotionCtr::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetSelectAxis
// Access		: public  
// Returns		: void
// Parameter	: UINT nAxis
// Qualifier	:
// Last Update	: 2017/4/3 - 11:49
// Desc.		:
//=============================================================================
void CWnd_MotionCtr::SetSelectAxis(UINT nAxis)
{
 	m_nAxisNum = nAxis;
}
