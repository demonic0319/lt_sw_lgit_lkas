﻿//*****************************************************************************
// Filename	: 	Wnd_CylinderTable.c
// Created	:	2017/02/05 - 13:47
// Modified	:	2017/02/05 - 13:47
//
// Author	:	
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_CylinderTable_h__
#define Wnd_CylinderTable_h__

#pragma once

#include <afxwin.h>
#include "VGStatic.h"
#include "Def_Digital_IO.h"

//-----------------------------------------------------------------------------
// CWnd_CylinderTable
//-----------------------------------------------------------------------------
class CWnd_CylinderTable : public CWnd
{
	DECLARE_DYNAMIC(CWnd_CylinderTable)

public:
	CWnd_CylinderTable();
	virtual ~CWnd_CylinderTable();

protected:
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);	
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	afx_msg void	OnRangeCmds				(UINT nID);

	DECLARE_MESSAGE_MAP()

	CFont			m_font_Data;

	// 컨트롤
	CVGStatic		m_st_CYL;
	CVGStatic		m_st_CYL_Name[CYL_Fo_MaxEnum];
	CMFCButton		m_bn_CYL_Status[CYL_Fo_MaxEnum];

	UINT			m_WM_Ctrl;
	UINT			m_nParaID;

	void	Set_CylinderResult				(__in UINT nItem, __in BOOL bResult);
	void	Set_CylinderReset				(__in UINT nItem);

public:

	void	SetControlMsgID	(UINT nWM_ID)
	{
		m_WM_Ctrl = nWM_ID;
	}

	void	SetControlParaID (UINT nParaID)
	{
		m_nParaID = nParaID;
	}

};

#endif // Wnd_CylinderTable_h__
