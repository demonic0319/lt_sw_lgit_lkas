﻿#ifndef Def_Mes_IR_ImgT_h__
#define Def_Mes_IR_ImgT_h__

#include <afxwin.h>

#pragma pack(push,1)
static LPCTSTR g_szMESHeader_LKAS[] =
{
	_T("Current"),
	_T("I2C"),
	_T("CL_00F_V MTF(67lp)_Center"),
	_T("CR_OOF_V MTF(67lp)_Center"),
	_T("CT_00F_H MTF(67lp)_Center"),
	_T("CB_00F_H MTF(67lp)_Center"),
	_T("LT_04F_V MTF(67lp)_0.4F"),
	_T("LT_04F_H MTF(67lp)_0.4F"),
	_T("RT_04F_V MTF(67lp)_0.4F"),
	_T("RT_04F_H MTF(67lp)_0.4F"),
	_T("RB_04F_V MTF(67lp)_0.4F"),
	_T("RB_04F_H MTF(67lp)_0.4F"),
	_T("LB_04F_V MTF(67lp)_0.4F"),
	_T("LB_04F_H MTF(67lp)_0.4F"),
	_T("L_04F_V  MTF(67lp)_0.4F"),
	_T("L_04F_H  MTF(67lp)_0.4F"),
	_T("R_04F_V  MTF(67lp)_0.4F"),
	_T("R_04F_H  MTF(67lp)_0.4F"),
	_T("LT_07F_V MTF(67lp)_0.7F"),
	_T("LT_07F_H MTF(67lp)_0.7F"),
	_T("RT_07F_V MTF(67lp)_0.7F"),
	_T("RT_07F_H MTF(67lp)_0.7F"),
	_T("RB_07F_V MTF(67lp)_0.7F"),
	_T("RB_07F_H MTF(67lp)_0.7F"),
	_T("LB_07F_V MTF(67lp)_0.7F"),
	_T("LB_07F_H MTF(67lp)_0.7F"),
	_T("Distortion(%)"),
	_T("Defect(Black)"),
	_T("Defect(White)"),
	_T("Blotch(Stain)"),
	_T("Blotch(LCB)"),
	_T("Blotch(BlackSpot)"),
	_T("Shading(%)_Center_Corner_0.65F"),
	_T("Shading(%)_Corner_Dev."),
	_T("Shading(%)_Center_Corner_0.85F"),
	_T("Shading(%)_Corner_Dev."),
	_T("Optical Center X"),
	_T("Optical Center Y"),
};

static LPCTSTR g_szMESHeader_IR_IKC[] =
{
	_T("Current_1.8V"),
	_T("Current_2.8V"),
	_T("Current_LED"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	//_T("Dynamic_Range"),
	//_T("SNR_BW"),
	_T("RI_Corner"),
	_T("RI_Min"),
	//_T("RI_UL"),
	//_T("RI_UR"),
	//_T("RI_LL"),
	//_T("RI_LR"),
	//_T("Ymean"),
};

static LPCTSTR g_szMESHeader_IR_MRA2[] =
{
	_T("Current_1.8V"),
	_T("Current_2.8V"),
	_T("Current_LED"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("Dynamic_Range"),
	_T("SNR_BW"),
	_T("RI_Corner"),
	_T("RI_Min"),
	//_T("RI_UL"),
	//_T("RI_UR"),
	//_T("RI_LL"),
	//_T("RI_LR"),
	//_T("Ymean"),
};

//항목 순서
typedef enum enMesDataIndex_LKAS
{
	MesDataIdx_LKAS_Current = 0,
	MesDataIdx_LKAS_I2C,
	MesDataIdx_LKAS_SFR,
	MesDataIdx_LKAS_Distortion,
	MesDataIdx_LKAS_DefectBlack,
	MesDataIdx_LKAS_DefectWhite,
	MesDataIdx_LKAS_Ymean,
	MesDataIdx_LKAS_LCB,
	MesDataIdx_LKAS_BlackSpot,
	MesDataIdx_LKAS_Shading,
	MesDataIdx_LKAS_OpticalCenter,
	MesDataIdx_LKAS_MAX,
};

typedef enum enMesDataIndex_IR_IKC
{
	MesDataIdx_ECurrent_IR_IKC = 0,
	MesDataIdx_SFR_IR_IKC_600,
	MesDataIdx_SFR_IR_IKC_680,
	MesDataIdx_SFR_IR_IKC_900,
	//MesDataIdx_DyanamicRange_IR_IKC,		//dynamic Range
	//MesDataIdx_SNR_BW_IR_IKC,
	MesDataIdx_RI_IR_IKC,
	MesDataIdx_Ymean_IR_IKC,
	MesDataIdx_MAX_IR_IKC,
};

typedef enum enMesDataIndex_IR_MRA2
{
	MesDataIdx_ECurrent_IR_MRA2 = 0,
	MesDataIdx_SFR_IR_MRA2_600,
	MesDataIdx_SFR_IR_MRA2_680, 
	MesDataIdx_SFR_IR_MRA2_900,
	MesDataIdx_DyanamicBW_IR_MRA2,		//dynamic Range
	//MesDataIdx_SNR_BW_IR_MRA2,
	MesDataIdx_RI_IR_MRA2,
	MesDataIdx_Ymean_IR_MRA2,
	MesDataIdx_MAX_IR_MRA2,
};


//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM_LKAS
{
	MESDataNum_LKAS_Current			= 1,
	MESDataNum_LKAS_I2C				= 1,
	MesDataNUM_LKAS_SFR				= 24,
	MESDataNum_LKAS_Distortion		= 1,
	MESDataNum_LKAS_DefectBlack		= 1,
	MESDataNum_LKAS_DefectWhite		= 1,
	MESDataNum_LKAS_Ymean			= 1,
	MESDataNum_LKAS_LCB				= 1,
	MESDataNum_LKAS_BlackSpot		= 1,
	MESDataNum_LKAS_Shading			= 4,
	MESDataNum_LKAS_OpticalCenter	= 2,

} MesData_DataNUM;

//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM_IR_IKC 
{
	MESDataNum_IR_ECurrent_IKC = 1,
	MESDataNum_IR_SFR_IKC = 30,
	//MesDataNUM_IR_DynamicBW_IKC  = 2,
	MESDataNum_IR_RI_IKC = 2,
	//MESDataNum_IR_Ymean_IKC  = 1,

	MESDataNum_IR_OpticalCenter = 2,
	MESDataNum_IR_Displace = 6,
	MESDataNum_IR_Shading = 4,

} MesData_DataNUM_IR_IKC;

//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM_IR_MRA2
{
	MESDataNum_IR_ECurrent_MRA2 = 1,
	MESDataNum_IR_SFR_MRA2 = 30,
	MesDataNUM_IR_DynamicBW_MRA2 = 2,
	MESDataNum_IR_RI_MRA2 = 2,
	//MESDataNum_IR_Ymean_MRA2 = 1,

	MESDataNum_IR_OpticalCenter_MRA2 = 2,
	MESDataNum_IR_Displace_MRA2 = 6,
	MESDataNum_IR_Shading_MRA2 = 4,

} MesData_DataNUM_IR_MRA2;

typedef struct _tag_MesData_LKAS
{
	CString szMesTestData[2][MesDataIdx_LKAS_MAX];
	CString szMesTestLogData[2][MesDataIdx_LKAS_MAX];

	int		nMesDataNum[MesDataIdx_LKAS_MAX];
	CString szDataName[MesDataIdx_LKAS_MAX];

	_tag_MesData_LKAS()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_LKAS_Current]		= MESDataNum_LKAS_Current;
		nMesDataNum[MesDataIdx_LKAS_I2C]			= MESDataNum_LKAS_I2C;
		nMesDataNum[MesDataIdx_LKAS_SFR]			= MesDataNUM_LKAS_SFR;
		nMesDataNum[MesDataIdx_LKAS_Distortion]		= MESDataNum_LKAS_Distortion;
		nMesDataNum[MesDataIdx_LKAS_DefectBlack]	= MESDataNum_LKAS_DefectBlack;
		nMesDataNum[MesDataIdx_LKAS_DefectWhite]	= MESDataNum_LKAS_DefectWhite;
		nMesDataNum[MesDataIdx_LKAS_Ymean]			= MESDataNum_LKAS_Ymean;
		nMesDataNum[MesDataIdx_LKAS_LCB]			= MESDataNum_LKAS_LCB;
		nMesDataNum[MesDataIdx_LKAS_BlackSpot]		= MESDataNum_LKAS_BlackSpot;
		nMesDataNum[MesDataIdx_LKAS_Shading]		= MESDataNum_LKAS_Shading;
		nMesDataNum[MesDataIdx_LKAS_OpticalCenter]	= MESDataNum_LKAS_OpticalCenter;

		szDataName[MesDataIdx_LKAS_Current]			= _T("Current");
		szDataName[MesDataIdx_LKAS_I2C]				= _T("I2C");
		szDataName[MesDataIdx_LKAS_SFR]				= _T("SFR");
		szDataName[MesDataIdx_LKAS_Distortion]		= _T("Distortion");
		szDataName[MesDataIdx_LKAS_DefectBlack]		= _T("DefectBlack");
		szDataName[MesDataIdx_LKAS_DefectWhite]		= _T("DefectWhite");
		szDataName[MesDataIdx_LKAS_Ymean]			= _T("Ymean");
		szDataName[MesDataIdx_LKAS_LCB]				= _T("LCB");
		szDataName[MesDataIdx_LKAS_BlackSpot]		= _T("BlackSpot");
		szDataName[MesDataIdx_LKAS_Shading]			= _T("Shading");
		szDataName[MesDataIdx_LKAS_OpticalCenter]	= _T("OpticalCenter");
	};

	void Reset(int Num)
	{
		for (int t = 0; t < MesDataIdx_LKAS_MAX; t++)
		{
			szMesTestData[Num][t].Empty();
		}
	};

	_tag_MesData_LKAS& operator= (_tag_MesData_LKAS& ref)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int t = 0; t < MesDataIdx_LKAS_MAX; t++)
			{
				szMesTestData[k][t] = ref.szMesTestData[k][t];
			}
		}

		return *this;
	};

}ST_MES_Data_LKAS, *PST_MES_Data_LKAS;

typedef struct _tag_MesData_IR_IKC
{
	CString szMesTestData[2][MesDataIdx_MAX_IR_IKC];
	CString szMesTestLogData[2][MesDataIdx_MAX_IR_IKC];

	int		nMesDataNum[MesDataIdx_MAX_IR_IKC];
	CString szDataName[MesDataIdx_MAX_IR_IKC];

	_tag_MesData_IR_IKC()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_ECurrent_IR_IKC] = MESDataNum_IR_ECurrent_IKC;
		nMesDataNum[MesDataIdx_SFR_IR_IKC_600] = MESDataNum_IR_SFR_IKC;
		nMesDataNum[MesDataIdx_SFR_IR_IKC_680] = MESDataNum_IR_SFR_IKC;
		nMesDataNum[MesDataIdx_SFR_IR_IKC_900] = MESDataNum_IR_SFR_IKC;
		//nMesDataNum[MesDataIdx_DyanamicRange_IR_IKC] = MesDataNUM_DynamicBW;
		//nMesDataNum[MesDataIdx_SNR_BW_IR_IKC] = MESDataNum_SNR_Light;
		nMesDataNum[MesDataIdx_RI_IR_IKC] = MESDataNum_IR_RI_IKC;
		//nMesDataNum[MesDataIdx_Ymean_IR_IKC] = MESDataNum_Stain;

		szDataName[MesDataIdx_ECurrent_IR_IKC] = _T("Current");
		szDataName[MesDataIdx_SFR_IR_IKC_600] = _T("SFR_600");
		szDataName[MesDataIdx_SFR_IR_IKC_680] = _T("SFR_680");
		szDataName[MesDataIdx_SFR_IR_IKC_900] = _T("SFR_900");
		//szDataName[MesDataIdx_DyanamicRange_IR_IKC] = _T("DyanamicRange");
		//szDataName[MesDataIdx_SNR_BW_IR_IKC] = _T("SNR_BW");
		szDataName[MesDataIdx_RI_IR_IKC] = _T("Relative_Illumination");
		//szDataName[MesDataIdx_Ymean_IR_IKC] = _T("Ymean");

	};

	void Reset(int Num)
	{
		for (int t = 0; t < MesDataIdx_MAX_IR_IKC; t++)
		{
			szMesTestData[Num][t].Empty();
		}
	};

	_tag_MesData_IR_IKC& operator= (_tag_MesData_IR_IKC& ref)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int t = 0; t < MesDataIdx_MAX_IR_IKC; t++)
			{
				szMesTestData[k][t] = ref.szMesTestData[k][t];
			}
		}

		return *this;
	};

}ST_MES_Data_IR_IKC, *PST_MES_Data_IR_IKC;

typedef struct _tag_MesData_IR_MRA2
{
	CString szMesTestData[2][MesDataIdx_MAX_IR_MRA2];
	CString szMesTestLogData[2][MesDataIdx_MAX_IR_MRA2];

	int		nMesDataNum[MesDataIdx_MAX_IR_MRA2];
	CString szDataName[MesDataIdx_MAX_IR_MRA2];

	_tag_MesData_IR_MRA2()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_ECurrent_IR_MRA2] = MESDataNum_IR_ECurrent_MRA2;
		nMesDataNum[MesDataIdx_SFR_IR_MRA2_600] = MESDataNum_IR_SFR_MRA2;
		nMesDataNum[MesDataIdx_SFR_IR_MRA2_680] = MESDataNum_IR_SFR_MRA2;
		nMesDataNum[MesDataIdx_SFR_IR_MRA2_900] = MESDataNum_IR_SFR_MRA2;
		nMesDataNum[MesDataIdx_DyanamicBW_IR_MRA2] = MesDataNUM_IR_DynamicBW_MRA2;
		//nMesDataNum[MesDataIdx_SNR_BW_IR_MRA2] = MesDataNUM_IR_DynamicBW_MRA2;
		nMesDataNum[MesDataIdx_RI_IR_MRA2] = MESDataNum_IR_RI_MRA2;
		//nMesDataNum[MesDataIdx_Ymean_IR_MRA2] = MESDataNum_Stain;

		szDataName[MesDataIdx_ECurrent_IR_MRA2] = _T("Current");
		szDataName[MesDataIdx_SFR_IR_MRA2_600] = _T("SFR_600");
		szDataName[MesDataIdx_SFR_IR_MRA2_680] = _T("SFR_680");
		szDataName[MesDataIdx_SFR_IR_MRA2_900] = _T("SFR_900");
		szDataName[MesDataIdx_DyanamicBW_IR_MRA2] = _T("Dyanamic BW");
		//szDataName[MesDataIdx_SNR_BW_IR_MRA2] = _T("SNR_BW");
		szDataName[MesDataIdx_RI_IR_MRA2] = _T("Relative_Illumination");
		//szDataName[MesDataIdx_Ymean_IR_MRA2] = _T("Ymean");

	};

	void Reset(int Num)
	{
		for (int t = 0; t < MesDataIdx_MAX_IR_MRA2; t++)
		{
			szMesTestData[Num][t].Empty();
		}
	};

	_tag_MesData_IR_MRA2& operator= (_tag_MesData_IR_MRA2& ref)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int t = 0; t < MesDataIdx_MAX_IR_MRA2; t++)
			{
				szMesTestData[k][t] = ref.szMesTestData[k][t];
			}
		}

		return *this;
	};

}ST_MES_Data_IR_MRA2, *PST_MES_Data_IR_MRA2;
#pragma pack(pop)

#endif // Def_FOV_h__