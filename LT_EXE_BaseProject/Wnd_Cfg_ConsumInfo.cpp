﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_ConsumInfo.cpp
// Created	:	2016/10/31 - 21:02
// Modified	:	2016/10/31 - 21:02
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************

#include "stdafx.h"
#include "Wnd_Cfg_ConsumInfo.h"
#include "Reg_InspInfo.h"

#define		IDC_CB_FILE				1001
#define		IDC_BN_SAVE				1002
#define		IDC_ED_CNT_MAX			1100
#define		IDC_ED_COUNT			1200
#define		IDC_BN_RESET_CNT		1300
#define		IDC_BN_RESET_CNT_E		1300 + USE_CHANNEL_CNT

#define		IDC_BN_APPLY			1404
#define		IDC_BN_CANCEL			1405

#define		WM_REFRESH_POGO			WM_USER + 201

//-----------------------------------------------------------------------------
// CWnd_Cfg_ConsumInfo
//-----------------------------------------------------------------------------
IMPLEMENT_DYNAMIC(CWnd_Cfg_ConsumInfo, CWnd_BaseView)

CWnd_Cfg_ConsumInfo::CWnd_Cfg_ConsumInfo()
{
	VERIFY(m_font_Default.CreateFont(
		16,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename

	VERIFY(m_font_Data.CreateFont(
		16,							// nHeight
		0,							// nWidth
		0,							// nEscapement
		0,							// nOrientation
		FW_BOLD,					// nWeight
		FALSE,						// bItalic
		FALSE,						// bUnderline
		0,							// cStrikeOut
		ANSI_CHARSET,				// nCharSet
		OUT_DEFAULT_PRECIS,			// nOutPrecision
		CLIP_DEFAULT_PRECIS,		// nClipPrecision
		DEFAULT_QUALITY,			// nQuality
		VARIABLE_PITCH,				// nPitchAndFamily
		_T("Arial")));		// lpszFacename
}

CWnd_Cfg_ConsumInfo::~CWnd_Cfg_ConsumInfo()
{
	m_font_Default.DeleteObject();
	m_font_Data.DeleteObject();

}

BEGIN_MESSAGE_MAP(CWnd_Cfg_ConsumInfo, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_CBN_SELENDOK	(IDC_CB_FILE, OnCbnSelendokFile)
	ON_BN_CLICKED	(IDC_BN_SAVE, OnBnClickedBnSave)
	ON_COMMAND_RANGE(IDC_BN_RESET_CNT, IDC_BN_RESET_CNT_E, OnBnClickedReset)
	ON_MESSAGE		(WM_REFRESH_POGO,	OnRefreshFileList)
END_MESSAGE_MAP()


// CWnd_Cfg_ConsumInfo message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
int CWnd_Cfg_ConsumInfo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_File.SetFont_Gdip(L"Arial", 9.0F);

	m_st_File.Create(_T("Socket File"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_File.Create(dwStyle | CBS_DROPDOWNLIST | CBS_SORT, rectDummy, this, IDC_CB_FILE);
	m_bn_NewFile.Create(_T("Add New File"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);


	CString szText;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_stCount_Max[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stCount_Max[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stCount_Max[nIdx].SetTextAlignment(StringAlignmentNear);

		m_stCount[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stCount[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stCount[nIdx].SetTextAlignment(StringAlignmentNear);

		szText.Format(_T("Socket %d: Max Count"), nIdx + 1);
		m_stCount_Max[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);
		szText.Format(_T("Socket %d: Count"), nIdx + 1);
		m_stCount[nIdx].Create(szText, dwStyle, rectDummy, this, IDC_STATIC);

		m_ed_Count_Max[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_NUMBER, rectDummy, this, IDC_ED_CNT_MAX + nIdx);
		m_ed_Count[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER | ES_NUMBER, rectDummy, this, IDC_ED_COUNT + nIdx);

		m_bn_ResetCount[nIdx].Create(_T("Reset"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_RESET_CNT + nIdx);

		m_ed_Count_Max[nIdx].SetFont(&m_font_Data);
		m_ed_Count[nIdx].SetFont(&m_font_Data);
	}

	m_bn_Apply.Create(_T("Apply"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_APPLY);
	m_bn_Cancel.Create(_T("Cancel"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_CANCEL);

	m_cb_File.SetFont(&m_font_Data);


	// 파일 감시 쓰레드 설정
	//	m_IniWatch.SetOwner(GetSafeHwnd(), WM_REFRESH_POGO);
	if (!m_strConsumInfoPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_strConsumInfoPath, CONSUM_FILE_EXT);
		//m_IniWatch.BeginWatchThrFunc();

		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());

		if (m_szConsumInfoFile.IsEmpty())
		{
			m_cb_File.SetCurSel(0);
		}
	}

	// 기본 데이터
	CString strFile;
	m_cb_File.GetWindowText(strFile);
	SetConsumInfoFile(strFile);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin = 10;
	int iSpacing = 5;
	int iCateSpacing = 10;

	int iLeft = iMagrin;
	int iTop = iMagrin;
	int iWidth = cx - iMagrin - iMagrin;
	int iHeight = cy - iMagrin;

	int iCtrlWidth	= iWidth / 4;
	int iCtrlHeight = 25;
	int iGrpHeight	= iCtrlHeight + iSpacing + iCtrlHeight;

	int iSubLeft = iLeft;
	int iSubTop = iTop;

	//int iCapWidth	= 120;
	int iTempWidth	= 0;

	//iTop += iCateSpacing;
	m_st_File.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
	iLeft += iCtrlWidth + iSpacing;
	iTempWidth = iCtrlWidth + iCtrlWidth + iSpacing;
	m_cb_File.MoveWindow(iLeft, iTop, iTempWidth, iCtrlHeight);
	iLeft += iTempWidth + iSpacing;
	m_bn_NewFile.MoveWindow(iLeft, iTop, iCtrlWidth - iMagrin, iCtrlHeight);

	iLeft = iMagrin;
	iTop += iCtrlHeight + iCateSpacing;

	for (UINT nIdx = 0; nIdx < m_stConsumInfo.ItemCount; nIdx++)
	{	
		iSubTop = iTop + iCtrlHeight + iSpacing;
		m_stCount_Max[nIdx].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		m_stCount[nIdx].MoveWindow(iLeft, iSubTop, iCtrlWidth, iCtrlHeight);

		iSubLeft = iLeft + iCtrlWidth + iSpacing;
		m_ed_Count_Max[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);
		m_ed_Count[nIdx].MoveWindow(iSubLeft, iSubTop, iCtrlWidth, iCtrlHeight);

		iSubLeft += iCtrlWidth + iSpacing;
		m_bn_ResetCount[nIdx].MoveWindow(iSubLeft, iTop, iCtrlWidth, iGrpHeight);

		iTop += iGrpHeight + iCateSpacing;
	}

	//iTop += iGrpHeight + iCateSpacing;
	m_bn_Apply.MoveWindow(iLeft, iTop, iCtrlWidth + iSpacing + iCtrlWidth, iCtrlHeight);
	iSubLeft = cx - iMagrin - iWidth;
	//m_bn_Cancel.MoveWindow(iSubLeft, iTop, iCtrlWidth, iCtrlHeight);

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ConsumInfo::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 10:04
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ConsumInfo::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2016/10/31 - 22:38
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd_BaseView::OnShowWindow(bShow, nStatus);

	if (bShow)
	{
		LoadIniFile(m_szConsumInfoFile);
	}
}

//=============================================================================
// Method		: OnCbnSelendokFile
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::OnCbnSelendokFile()
{
	// 선택된 파일 로드
	CString szFile;

	int iSel = m_cb_File.GetCurSel();
	if (0 <= iSel)
	{
		m_cb_File.GetWindowText(szFile);

		SelectEndConsumInfoFile(szFile);
	}
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::OnBnClickedBnSave()
{
	// UI에서 데이터 구함
	CString szText;
	for (UINT nIdx = 0; nIdx < m_stConsumInfo.ItemCount; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_stConsumInfo.Item[nIdx].dwCount_Max = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_stConsumInfo.Item[nIdx].dwCount = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	// 파일 다른이름으로 저장
	CString strFullPath;
	CString strFullTitle;
	CString strFileExt;
	strFileExt.Format(_T("Ini File (*.%s)| *.%s||"), CONSUM_FILE_EXT, CONSUM_FILE_EXT);

	CFileDialog fileDlg(FALSE, CONSUM_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strConsumInfoPath;

	if (fileDlg.DoModal() == IDOK)
	{
		strFullPath = fileDlg.GetPathName();
		strFullTitle = fileDlg.GetFileTitle();

		if (m_fileRecipe.Save_ConsumInfoFile(strFullPath, &m_stConsumInfo))
		{
			// 리스트 모델 갱신
			m_szConsumInfoFile = strFullTitle;
			m_IniWatch.RefreshList();
			RefreshFileList(m_IniWatch.GetFileList());
		}
	}
}

//=============================================================================
// Method		: OnBnClickedReset
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/10/31 - 21:45
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::OnBnClickedReset(UINT nID)
{
	UINT nIndex = nID - IDC_BN_RESET_CNT;
	
	m_ed_Count[nIndex].SetWindowText(_T("0"));
}

//=============================================================================
// Method		: OnBnClickedBnApply
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/10/31 - 22:34
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::OnBnClickedBnApply()
{
	if (FALSE == SaveIniFile(m_szConsumInfoFile))
	{
		AfxMessageBox(_T("Save Failed! : Consumable Info!"), MB_SYSTEMMODAL);
	}
}

//=============================================================================
// Method		: OnRefreshFileList
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/1/6 - 11:01
// Desc.		:
//=============================================================================
LRESULT CWnd_Cfg_ConsumInfo::OnRefreshFileList(WPARAM wParam, LPARAM lParam)
{
	RefreshFileList(m_IniWatch.GetFileList());

	return 0;
}

//=============================================================================
// Method		: LoadIniFile
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ConsumInfo::LoadIniFile(__in LPCTSTR szFile)
{
	CString strFilePath;

	strFilePath.Format(_T("%s%s.%s"), m_strConsumInfoPath, szFile, CONSUM_FILE_EXT);

	if (m_fileRecipe.Load_ConsumInfoFile(strFilePath, m_stConsumInfo))
	{
		// 화면에 표시
		CString szText;
		for (UINT nIdx = 0; nIdx < m_stConsumInfo.ItemCount; nIdx++)
		{
			szText.Format(_T("%d"), m_stConsumInfo.Item[nIdx].dwCount_Max);
			m_ed_Count_Max[nIdx].SetWindowText(szText);
			szText.Format(_T("%d"), m_stConsumInfo.Item[nIdx].dwCount);
			m_ed_Count[nIdx].SetWindowText(szText);
		}

		return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: SaveIniFile
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ConsumInfo::SaveIniFile(__in LPCTSTR szFile)
{
	CString strFilePath;

	strFilePath.Format(_T("%s%s.%s"), m_strConsumInfoPath, szFile, CONSUM_FILE_EXT);

	// UI에서 데이터 구함
	CString szText;
	for (UINT nIdx = 0; nIdx < m_stConsumInfo.ItemCount; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_stConsumInfo.Item[nIdx].dwCount_Max = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_stConsumInfo.Item[nIdx].dwCount = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	return m_fileRecipe.Save_ConsumInfoFile(strFilePath, &m_stConsumInfo);
}

//=============================================================================
// Method		: SelectEndConsumInfoFile
// Access		: protected  
// Returns		: void
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::SelectEndConsumInfoFile(__in LPCTSTR szFile)
{
	// 파일 로드
	if (LoadIniFile(szFile))
	{
		m_szConsumInfoFile = szFile;
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/10/10 - 16:57
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	m_fileRecipe.SetSystemType(m_InspectionType);

	m_stConsumInfo.ItemCount = g_InspectorTable[m_InspectionType].Grabber_Cnt;	
}

//=============================================================================
// Method		: RefreshFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_File.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_File.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_szConsumInfoFile.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_szConsumInfoFile);

		if (0 <= iSel)
		{
			m_cb_File.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: SetConsumInfoFile
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szFile
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_Cfg_ConsumInfo::SetConsumInfoFile(__in LPCTSTR szFile)
{
	int iSel = m_cb_File.FindStringExact(0, szFile);

	if (0 <= iSel)
	{
		m_cb_File.SetCurSel(iSel);

		SelectEndConsumInfoFile(szFile);
	}
}

//=============================================================================
// Method		: GetConsumInfo
// Access		: public  
// Returns		: ST_ConsumablesInfo
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
ST_ConsumablesInfo CWnd_Cfg_ConsumInfo::GetConsumInfo()
{
	CString szText;
	for (UINT nIdx = 0; nIdx < m_stConsumInfo.ItemCount; nIdx++)
	{
		m_ed_Count_Max[nIdx].GetWindowText(szText);
		m_stConsumInfo.Item[nIdx].dwCount_Max = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();

		m_ed_Count[nIdx].GetWindowText(szText);
		m_stConsumInfo.Item[nIdx].dwCount = _ttol(szText.GetBuffer());
		szText.ReleaseBuffer();
	}

	m_stConsumInfo.szFilename = m_szConsumInfoFile;

	return m_stConsumInfo;
}

//=============================================================================
// Method		: GetConsumInfoFile
// Access		: public  
// Returns		: CString
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
CString CWnd_Cfg_ConsumInfo::GetConsumInfoFile()
{
	return m_szConsumInfoFile;
}

//=============================================================================
// Method		: SaveConsumInfoSetting
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_ConsumInfo::SaveConsumInfoSetting()
{
	CString szFile;
	m_cb_File.GetWindowText(szFile);

	return SaveIniFile(szFile);
}
