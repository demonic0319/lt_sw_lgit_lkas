//*****************************************************************************
// Filename	: Wnd_MotorView.cpp
// Created	: 2017/04/03
// Modified	: 2017/04/03
//
// Author	: KHO
//	
// Purpose	: 
//*****************************************************************************
// Wnd_MotorView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MotorView.h"

#define		IDC_CB_FILE				1001
#define		IDC_BN_SAVE				1002

//=============================================================================
// CWnd_MotorView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_MotorView, CWnd_BaseView)

CWnd_MotorView::CWnd_MotorView()
{
	m_pstDevice = NULL;
	m_szMotorFile.Empty();
	m_szMotorPath.Empty();

	VERIFY(m_font.CreateFont(
		19,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MotorView::~CWnd_MotorView()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_MotorView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_MESSAGE(WM_SELECT_AXIS,	OnMotorSelect)
	ON_MESSAGE(WM_UPDATA_AXIS,	OnMotorUpdata)
	ON_MESSAGE(WM_MOTOR_ORIGIN, OnMotorOrigin)
	ON_BN_CLICKED(IDC_BN_SAVE, OnBnClickedBnNew)
	ON_CBN_SELENDOK(IDC_CB_FILE, OnCbnSelendokFile)
END_MESSAGE_MAP()

//=============================================================================
// CWnd_MotorView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_MotorView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
int CWnd_MotorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_tc_Item.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);
	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	m_st_File.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_File.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_File.SetFont_Gdip(L"Arial", 10.5F);

	m_st_File.Create(_T("Motor File"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_cb_File.Create(dwStyle | CBS_DROPDOWNLIST | CBS_SORT, rectDummy, this, IDC_CB_FILE);
	m_cb_File.SetFont(&m_font);

	m_bn_NewFile.Create(_T("Add New File"), dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_BN_SAVE);

	if (!m_szMotorPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szMotorPath, MOTOR_FILE_EXT);
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
		m_cb_File.SetCurSel(0);
	}

	m_Wnd_MotionTable.SetOwner(this);
	m_Wnd_MotionTable.Create(NULL, _T(""), dwStyle, rectDummy, this, 11);

	m_Wnd_MotionCtr.SetOwner(this);
	m_Wnd_MotionCtr.Create(NULL, _T(""), dwStyle, rectDummy, this, 12);

	m_Wnd_OriginOp.SetOwner(this);
	m_Wnd_OriginOp.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Option, 13);

	m_Wnd_MotionOp.SetOwner(this);
	m_Wnd_MotionOp.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Option, 14);

	m_Wnd_TeachOp.SetOwner(this);
	m_Wnd_TeachOp.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Option, 15);
	
	m_tc_Option.AddTab(&m_Wnd_OriginOp, _T("Origin"), 0, FALSE);
	m_tc_Option.AddTab(&m_Wnd_MotionOp, _T("Motion"), 1, FALSE);
	m_tc_Option.AddTab(&m_Wnd_TeachOp,  _T("Teach"),  2, FALSE);

	m_tc_Option.SetActiveTab(0);
	m_tc_Option.EnableTabSwap(FALSE);

	m_st_Filename.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Filename.SetColorStyle(CVGStatic::ColorStyle_Default);
	m_st_Filename.SetFont_Gdip(L"Arial", 11.0F);
	m_st_Filename.Create(_T("파일 경로"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	// 센서 모니터링 시작
	m_Wnd_MotionTable.StartThread_SensorMon();

	return 0;
}

//=============================================================================
// Method		: CWnd_MotorView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
void CWnd_MotorView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 10;
	int iLeft			= iMagrin;
	int iTop			= iMagrin;
	int iWidth			= cx - iMagrin - iMagrin;
	int iHeight			= cy - iMagrin - iMagrin;
	int iCtrlWidth		= (iWidth - (iSpacing * 3)) / 9;
	int iCtrlHeight		= 28;

	m_st_Filename.MoveWindow(iLeft, iTop, iCtrlWidth * 4, iCtrlHeight);
	
	iLeft += iCtrlWidth * 4 + iSpacing;
	m_st_File.MoveWindow(iLeft, iTop, iCtrlWidth * 2, iCtrlHeight);

	iLeft += iCtrlWidth * 2 + iSpacing;
	m_cb_File.MoveWindow(iLeft, iTop, iCtrlWidth * 2, iCtrlHeight);

	iLeft += iCtrlWidth * 2 + iSpacing;
	iCtrlWidth = cx - iLeft - iMagrin;
	m_bn_NewFile.MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	iLeft	 = iMagrin;
	iTop	+= iCtrlHeight + iCateSpacing;
	iHeight  = (cy - iTop - iMagrin - iCateSpacing * 2) / 12;
	iCtrlHeight = iHeight * 5;
	m_Wnd_MotionTable.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	
	iTop += iCtrlHeight + iCateSpacing;
	iCtrlHeight = iHeight * 3;
	m_Wnd_MotionCtr.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
	
	iTop += iCtrlHeight + iCateSpacing;
	iCtrlHeight = cy - iTop - iMagrin;
	m_tc_Option.MoveWindow(iLeft, iTop, iWidth, iCtrlHeight);
}

//=============================================================================
// Method		: OnBnClickedBnNew
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/4 - 16:52
// Desc.		:
//=============================================================================
void CWnd_MotorView::OnBnClickedBnNew()
{
	if (IDYES == AfxMessageBox(_T("새로 작성 하시겠습니까?"), MB_YESNO))
	{
		CString strFileTitle;
		CString strFileExt;
		strFileExt.Format(_T("Motor File (*.%s)| *.%s||"), MOTOR_FILE_EXT, MOTOR_FILE_EXT);

		CFileDialog fileDlg(FALSE, MOTOR_FILE_EXT, NULL, OFN_OVERWRITEPROMPT, strFileExt);
		fileDlg.m_ofn.lpstrInitialDir = m_szMotorPath;

		if (fileDlg.DoModal() == IDOK)
		{
			m_szMotorFile = fileDlg.GetFileTitle();

			m_pstDevice->SetPrtMotorPath(&m_szMotorPath, m_szMotorFile);

			if (m_pstDevice->SaveMotionInfo())
			{
				;
			}

			GetOwner()->SendNotifyMessage(WM_CHANGED_MOTOR, (WPARAM)m_szMotorFile.GetBuffer(), 0);
		}
	}
}

//=============================================================================
// Method		: OnCbnSelendokFile
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/4/4 - 16:53
// Desc.		:
//=============================================================================
void CWnd_MotorView::OnCbnSelendokFile()
{
	// 선택된 파일 로드
	CString szFile;
	CString szMotorFileFull;

	int iSel = m_cb_File.GetCurSel();
	
	if (0 <= iSel)
	{
		m_cb_File.GetWindowText(szFile);
	}

	szMotorFileFull = m_szMotorPath + szFile;
	m_st_Filename.SetWindowText(szMotorFileFull + _T(".") + MOTOR_FILE_EXT);

	m_szMotorFile = szFile;
	GetOwner()->SendNotifyMessage(WM_CHANGED_MOTOR, (WPARAM)szFile.GetBuffer(), 0);

	szFile.ReleaseBuffer();
}

//=============================================================================
// Method		: OnMotorSelect
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/4/3 - 11:32
// Desc.		:
//=============================================================================
LRESULT CWnd_MotorView::OnMotorSelect(WPARAM wParam, LPARAM lParam)
{
	UINT nAxis = (UINT)wParam;

	m_Wnd_MotionOp.SetSelectAxis(nAxis);
	m_Wnd_OriginOp.SetSelectAxis(nAxis);
	m_Wnd_MotionCtr.SetSelectAxis(nAxis);

	return TRUE;
}

//=============================================================================
// Method		: OnMotorOrigin
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/11/7 - 21:30
// Desc.		:
//=============================================================================
LRESULT CWnd_MotorView::OnMotorOrigin(WPARAM wParam, LPARAM lParam)
{
	GetOwner()->SendNotifyMessage(WM_MOTOR_ORIGIN, 0, 0);
	return 0;
}

//=============================================================================
// Method		: OnMotorUpdata
// Access		: protected  
// Returns		: LRESULT
// Parameter	: WPARAM wParam
// Parameter	: LPARAM lParam
// Qualifier	:
// Last Update	: 2017/4/4 - 10:45
// Desc.		:
//=============================================================================
LRESULT CWnd_MotorView::OnMotorUpdata(WPARAM wParam, LPARAM lParam)
{
	m_Wnd_MotionTable.SetUpdataAxisName();

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_MotorView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetDeleteTimer
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/6/28 - 12:36
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetDeleteTimer()
{
	m_Wnd_MotionTable.SetDeleteTimer();
}

//=============================================================================
// Method		: SetOriginEnable
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/8/12 - 17:29
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetOriginEnable()
{
	m_Wnd_OriginOp.EnableWindow(TRUE);
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: enum_Inspection_Mode InspMode
// Qualifier	:
// Last Update	: 2016/1/13 - 14:18
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetPermissionMode(enPermissionMode InspMode)
{
	switch (InspMode)
	{
	case Permission_Operator:
		break;

	case Permission_Manager:
	case Permission_Administrator:
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: RefreshFileList
// Access		: public  
// Returns		: void
// Parameter	: __in const CStringList * pFileList
// Qualifier	:
// Last Update	: 2017/1/6 - 11:27
// Desc.		:
//=============================================================================
void CWnd_MotorView::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_File.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_File.AddString(pFileList->GetNext(pos));
	}

	// 이전에 선택되어있는 파일 다시 선택
	if (!m_szMotorPath.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_szMotorPath);

		if (0 <= iSel)
		{
			m_cb_File.SetCurSel(iSel);
		}
	}
}

//=============================================================================
// Method		: UpdateMotorInfo
// Access		: public  
// Returns		: void
// Parameter	: __in CString szMotorFile
// Qualifier	:
// Last Update	: 2017/10/2 - 15:26
// Desc.		:
//=============================================================================
void CWnd_MotorView::UpdateMotorInfo(__in CString szMotorFile)
{
	if (m_pstDevice == NULL)
		return;

	if (!szMotorFile.IsEmpty())
		m_szMotorFile = szMotorFile;

	UINT nSelectAxis = 0;

	for (UINT nAxis = 0; nAxis < (UINT)m_pstDevice->m_AllMotorData.MotorInfo.lMaxAxisCnt; nAxis++)
	{
		if (m_pstDevice->GetAxisUseStatus(nAxis))
		{
			nSelectAxis = nAxis;
			break;
		}
	}

	if (!m_szMotorPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szMotorPath, MOTOR_FILE_EXT);
		m_IniWatch.RefreshList();
		RefreshFileList(m_IniWatch.GetFileList());
	}

	if (!m_szMotorFile.IsEmpty())
	{
		int iSel = m_cb_File.FindStringExact(0, m_szMotorFile);

		if (0 <= iSel)
			m_cb_File.SetCurSel(iSel);

		if (!m_szMotorPath.IsEmpty())
			m_st_Filename.SetWindowText(m_szMotorPath + m_szMotorFile + _T(".") + MOTOR_FILE_EXT);
	}

	//UI 업데이트 
	m_Wnd_TeachOp.SetUpdateData();
	m_Wnd_OriginOp.SetUpdateData();
	m_Wnd_MotionOp.SetUpdateData();

	m_pstDevice->SetAllMotionSetting();

	m_Wnd_MotionTable.SetUpdateDataReset(nSelectAxis);
	m_Wnd_MotionCtr.SetSelectAxis(nSelectAxis);
	m_Wnd_MotionOp.SetSelectAxis(nSelectAxis);
	m_Wnd_OriginOp.SetSelectAxis(nSelectAxis);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 13:58
// Desc.		:
//=============================================================================
void CWnd_MotorView::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;

	m_Wnd_MotionTable.SetSystemType(m_InspectionType);
	m_Wnd_TeachOp.SetSystemType(m_InspectionType);
}
