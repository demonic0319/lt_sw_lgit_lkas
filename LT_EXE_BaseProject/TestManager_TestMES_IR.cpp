﻿#include <afxwin.h>
#include "stdafx.h"
#include "TestManager_TestMES_IR.h"



CTestManager_TestMES_IR::CTestManager_TestMES_IR()
{
}

CTestManager_TestMES_IR::~CTestManager_TestMES_IR()
{
}

void CTestManager_TestMES_IR::Current_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData)
{
	//-전류 1.8V, 2.8V
	CString strData;
	CString strFullData;

	strFullData.Empty();

	int nCurrentCnt = 3;

	//if ((m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Entry))
	//	nCurrentCnt = Spec_ECurrent_Max;

	for (UINT t = 0; t < nCurrentCnt; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.bResult[t]);

		if (t < (nCurrentCnt - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}
	pMesData->szMesTestData[nParaIdx][MesDataIdx_ECurrent_IR_IKC] = strFullData;
}

void CTestManager_TestMES_IR::Current_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData)
{
	//-전류 1.8V, 2.8V
	CString strData;
	CString strFullData;

	strFullData.Empty();

	int nCurrentCnt = 3;

	//if ((m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Front) || (m_pInspInfo->RecipeInfo.ModelType == Model_OMS_Entry))
	//	nCurrentCnt = Spec_ECurrent_Max;

	for (UINT t = 0; t < nCurrentCnt; t++)
	{
		strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.bResult[t]);

		if (t < (nCurrentCnt - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}
	pMesData->szMesTestData[nParaIdx][MesDataIdx_ECurrent_IR_MRA2] = strFullData;
}

void CTestManager_TestMES_IR::SFR_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData, int nDistance)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (int t = 0; t < ROI_SFR_Max; t++)
	{
		if (m_pInspInfo->RecipeInfo.stIR_ImageQ.stSFR.stInput[t].bEnable)
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.bResult[t]);
		}
		else{
			strData.Format(_T("Ｘ:1"));
		}
		if (t < (ROI_SFR_Max - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	if (nDistance == 0)
	{
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_IR_IKC_600] = strFullData;
	}
	if (nDistance == 1)
	{
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_IR_IKC_680] = strFullData;
	}
	if (nDistance == 2)
	{
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_IR_IKC_900] = strFullData;
	}

}

void CTestManager_TestMES_IR::SFR_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData, int nDistance)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	for (int t = 0; t < ROI_SFR_Max; t++)
	{
		if (m_pInspInfo->RecipeInfo.stIR_ImageQ.stSFR.stInput[t].bEnable)
		{
			strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.dbValue[t], m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.bResult[t]);
		}
		else{
			strData.Format(_T("Ｘ:1"));
		}
		if (t < (ROI_SFR_Max - 1))
		{
			strData += _T(",");
		}
		strFullData += strData;
	}

	if (nDistance == 0)
	{
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_IR_MRA2_600] = strFullData;
	}
	if (nDistance == 1)
	{
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_IR_MRA2_680] = strFullData;
	}
	if (nDistance == 2)
	{
		pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR_IR_MRA2_900] = strFullData;
	}

}

void CTestManager_TestMES_IR::DynamicBW_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData)
{

	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbDynamic, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bDynamic);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbSNR_BW, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bSNR_BW);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_DyanamicBW_IR_MRA2] = strFullData;

}


void CTestManager_TestMES_IR::Ymean_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.bYmeanResult);
	strFullData += strData;
	
	pMesData->szMesTestData[nParaIdx][MesDataIdx_Ymean_IR_IKC] = strFullData;
}

void CTestManager_TestMES_IR::Ymean_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.nDefectCount, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.bYmeanResult);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_Ymean_IR_MRA2] = strFullData;
}

void CTestManager_TestMES_IR::RI_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRICorner, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIResult[0]);
	strData += _T(",");
	strFullData += strData;
	
	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIMin, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIResult[1]);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_RI_IR_IKC] = strFullData;
}

void CTestManager_TestMES_IR::RI_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData)
{
	CString strData;
	CString strFullData;

	strFullData.Empty();

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRICorner, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIResult[0]);
	strData += _T(",");
	strFullData += strData;

	strData.Format(_T("%.2f:%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIMin, m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIResult[1]);
	strFullData += strData;

	pMesData->szMesTestData[nParaIdx][MesDataIdx_RI_IR_MRA2] = strFullData;
}

//--------------------------------------------------------------------------------------------개별 검사 Report

void CTestManager_TestMES_IR::Current_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{

	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_ECurrent_IKC; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Current[t]);
	}

	for (int t = 0; t < MESDataNum_IR_ECurrent_IKC; t++)
	{
		
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stCurrent.dbValue[t]);
		
		pAddData->Add(strData);
	}

	////-전류 1.8V, 2.8V
	//CString strData;
	//switch (nSysType)
	//{
	//case Sys_2D_Cal:
	//	break;
	//case Sys_3D_Cal:
	//	break;
	//case Sys_Stereo_Cal:
	//	break;
	//case Sys_Focusing:
	//{
	//					 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.nResult;
	//					

	//					 if (m_pInspInfo->RecipeInfo.ModelType == Model_IKC || m_pInspInfo->RecipeInfo.ModelType == Model_MRA2){

	//						 for (int t = 0; t < MESDataNum_Foc_ECurrent; t++)
	//						 {
	//							 pAddHeaderz->Add(g_szMESHeader_Current[t]);
	//						 }


	//						 for (int t = 0; t < 2; t++)
	//						 {
	//							 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[t]);
	//							 pAddData->Add(strData);
	//						 }
	//						 for (int t = 0; t < 3; t++)
	//						 {
	//							 pAddData->Add(_T("X"));
	//						 }
	//					 }
	//					 else{
	//						 for (int t = 0; t < MESDataNum_Foc_ECurrent; t++)
	//						 {
	//							 pAddHeaderz->Add(g_szMESHeader_CurrentEntry[t]);
	//						 }


	//						 for (int t = 0; t <5; t++)
	//						 {
	//							 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stECurrentData.dbValue[t]);
	//							 pAddData->Add(strData);
	//						 }
	//					 }
	//					
	//					 
	//}
	//	break;
	//case Sys_Image_Test:
	//{
	//					   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.nResult;
	//					   if (m_pInspInfo->RecipeInfo.ModelType == Model_IKC || m_pInspInfo->RecipeInfo.ModelType == Model_MRA2){
	//						   for (int t = 0; t < MESDataNum_ECurrent; t++)
	//						   {
	//							   pAddHeaderz->Add(g_szMESHeader_Current[t]);
	//						   }


	//						   for (int t = 0; t < 2; t++)
	//						   {
	//							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[t]);
	//							   pAddData->Add(strData);
	//						   }
	//						   for (int t = 0; t < 3; t++)
	//						   {
	//							   pAddData->Add(_T("X"));
	//						   }
	//					   }else{
	//						   for (int t = 0; t < MESDataNum_ECurrent; t++)
	//						   {
	//							   pAddHeaderz->Add(g_szMESHeader_CurrentEntry[t]);
	//						   }


	//						   for (int t = 0; t < 5; t++)
	//						   {
	//							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stECurrentData.dbValue[t]);
	//							   pAddData->Add(strData);
	//						   }
	//					   }
	//}
	//	break;
	//default:
	//	break;
	//}
}


void CTestManager_TestMES_IR::SFR_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_SFR_IKC; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_SFR[t]);
	}

	for (int t = 0; t < ROI_SFR_Max; t++)
	{
		if (m_pInspInfo->RecipeInfo.stIR_ImageQ.stSFR.stInput[t].bEnable)
		{
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stSFR.dbValue[t]);
		}
		else
		{
			strData.Format(_T("X"));
		}

		pAddData->Add(strData);
	}

// 	switch (nSysType)
// 	{
// 	case Sys_2D_Cal:
// 		break;
// 	case Sys_3D_Cal:
// 		break;
// 	case Sys_Stereo_Cal:
// 		break;
// 	case Sys_Focusing:
// 	{
// 						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nResult;
// 						 for (int t = 0; t < MESDataNum_Foc_SFR; t++)
// 						 {
// 							 pAddHeaderz->Add(g_szMESHeader_SFR[t]);
// 						 }
// 
// 						 for (int t = 0; t < GRP_SFR_Max; t++)
// 						 {
// 							 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[t]);
// 							 pAddData->Add(strData);
// 						 }
// 
// 						 for (int t = 0; t < ROI_SFR_Max; t++)
// 						 {
// 							 if (m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.bUse)
// 							 {
// 								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbValue[t]);
// 							 }
// 							 else
// 							 {
// 								 strData.Format(_T("X"));
// 							 }
// 
// 							 pAddData->Add(strData);
// 						 }
// 
// 	}
// 		break;
// 	case Sys_Image_Test:
// 	{
// 						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.nResult;
// 						   for (int t = 0; t < MESDataNum_SFR; t++)
// 						   {
// 							   pAddHeaderz->Add(g_szMESHeader_SFR[t]);
// 						   }
// 
// 						   for (int t = 0; t < GRP_SFR_Max; t++)
// 						   {
// 							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.dbMinValue[t]);
// 							   pAddData->Add(strData);
// 						   }
// 
// 						   for (int t = 0; t < ROI_SFR_Max; t++)
// 						   {
// 
// 							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.bUse)
// 							   {
// 								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.dbValue[t]);
// 							   }
// 							   else
// 							   {
// 								   strData.Format(_T("X"));
// 							   }
// 
// 							   pAddData->Add(strData);
// 						   }
// 	}
// 		break;
// 	default:
// 		break;
// 	}



//	pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR] = strFullData;
}


void CTestManager_TestMES_IR::DynamicRange_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.bDynamic;

	for (int t = 0; t < MesDataNUM_IR_DynamicBW_MRA2; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Dynamic_BW[t]);
	}


	strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbDynamic);
	pAddData->Add(strData);


	strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDynamicBW.dbSNR_BW);
	pAddData->Add(strData);

// 	switch (nSysType)
// 	{
// 	case Sys_2D_Cal:
// 		break;
// 	case Sys_3D_Cal:
// 		break;
// 	case Sys_Stereo_Cal:
// 		break;
// 	case Sys_Focusing:
// 	{
// 
// 	}
// 		break;
// 	case Sys_Image_Test:
// 	{
// 						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nResultDR&m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.nResultBW;
// 						   for (int t = 0; t < MESDataNum_Particle_SNR; t++)
// 						   {
// 							   pAddHeaderz->Add(g_szMESHeader_Dynamic[t]);
// 						   }
// 						   int nCnt = 0;
// 
// 						   for (int k = 0; k < ROI_Par_SNR_Max; k++)
// 						   {
// 							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.bUse[k])
// 							   {
// 								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
// 								   {
// 									   strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.sSignal[t][k]);
// 									   pAddData->Add(strData);
// 									   nCnt++;
// 								   }
// 								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
// 								   {
// 									   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.fNoise[t][k]);
// 									   pAddData->Add(strData);
// 									   nCnt++;
// 								   }
// 
// 								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueBW[k]);
// 								   pAddData->Add(strData);
// 								   nCnt++;
// 								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stDynamicData.dbValueDR[k]);
// 								   pAddData->Add(strData);
// 								   nCnt++;
// 
// 
// 
// 							   }
// 							   else{
// 								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
// 								   {
// 									   pAddData->Add(_T("X"));
// 									   nCnt++;
// 								   }
// 								   for (int t = 0; t < Light_Par_SNR_MAX; t++)
// 								   {
// 									   pAddData->Add(_T("X"));
// 									   nCnt++;
// 								   }
// 
// 								   pAddData->Add(_T("X"));
// 								   nCnt++;
// 								   pAddData->Add(_T("X"));
// 								   nCnt++;
// 								   pAddData->Add(_T("X"));
// 								   nCnt++;
// 
// 							   }
// 						   }
// 
// 						   for (int t = nCnt; t < MESDataNum_Particle_SNR; t++)
// 						   {
// 							   pAddData->Add(_T("X"));
// 						   }
// 						   //pMesData->szMesTestData[nParaIdx][MesDataIdx_Particle_SNR] = strFullData;
// 	}
// 		break;
// 	default:
// 		break;
// 	}


}


void CTestManager_TestMES_IR::Ymean_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.bYmeanResult;

	pAddHeaderz->Add(g_szMESHeader_Stain[1]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stYmean.nDefectCount);

	pAddData->Add(strData);
	
}

void CTestManager_TestMES_IR::Distortion_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDistortion.bRotation;

	pAddHeaderz->Add(g_szMESHeader_Distortion[0]); //Ymean Count만

	strData.Format(_T("%.3f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDistortion.dbRotation);

	pAddData->Add(strData);

}

void CTestManager_TestMES_IR::DefectBlack_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_Black.bDefect_BlackResult;

	pAddHeaderz->Add(g_szMESHeader_Defectpixel[1]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_Black.nDefect_BlackCount);

	pAddData->Add(strData);

}

void CTestManager_TestMES_IR::DefectWhite_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_White.bDefect_WhiteResult;

	pAddHeaderz->Add(g_szMESHeader_Defectpixel[3]); //Ymean Count만

	strData.Format(_T("%d"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDefect_White.nDefect_WhiteCount);

	pAddData->Add(strData);

}


void CTestManager_TestMES_IR::Shading_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_Shading; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Shading[t]);
	}

	for (int t = 0; t < MESDataNum_IR_Shading; t++)
	{
		//strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[t]);

		switch (t)
		{
		case 0:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[0]);
			break;
		case 1:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[2]);
			break;
		case 2:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[1]);
			break;
		case 3:
			strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stShading.dValue[3]);
			break;
		default:
			break;
		}

		pAddData->Add(strData);
	}
}

void CTestManager_TestMES_IR::OpicalCenter_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stOpticalCenter.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_OpticalCenter[t]);
	}

	for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stOpticalCenter.dValue[t]);

		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_IR::Vision_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stVision.GetFinalResult();

	//for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{
		pAddHeaderz->Add(_T("Match Rate"));
	}

	//for (int t = 0; t < MESDataNum_IR_OpticalCenter; t++)
	{

		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stVision.dbValue[0]);

		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_IR::Displace_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDisplace.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_Displace[t]);
	}

	for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stDisplace.dbValue[t + 4]);
		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_IR::IIC_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stIIC.GetFinalResult();

	//for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		pAddHeaderz->Add(_T("IIC Pattern"));
	}

	//for (int t = 0; t < MESDataNum_IR_Displace; t++)
	{
		if (m_pInspInfo->CamInfo[nParaIdx].stIRImage.stIIC.bResult[0] == TRUE)
		{
			strData = _T("Pass");
		} 
		else
		{
			strData = _T("Fail");
		}

	//	strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stIIC.bResult[0]);
		pAddData->Add(strData);
	}
}


void CTestManager_TestMES_IR::RI_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult)
{
	CString strData;

	nResult = m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.GetFinalResult();

	for (int t = 0; t < MESDataNum_IR_RI_IKC; t++)
	{
		pAddHeaderz->Add(g_szMESHeader_RI[t]);
	}

	
	strData.Format(_T("%.3f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRICorner);
	pAddData->Add(strData);


	strData.Format(_T("%.3f"), m_pInspInfo->CamInfo[nParaIdx].stIRImage.stRllumination.dRIMin);
	pAddData->Add(strData);


	// 	switch (nSysType)
	// 	{
	// 	case Sys_2D_Cal:
	// 		break;
	// 	case Sys_3D_Cal:
	// 		break;
	// 	case Sys_Stereo_Cal:
	// 		break;
	// 	case Sys_Focusing:
	// 	{
	// 						 nResult = m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.nResult;
	// 						 for (int t = 0; t < MESDataNum_Foc_SFR; t++)
	// 						 {
	// 							 pAddHeaderz->Add(g_szMESHeader_SFR[t]);
	// 						 }
	// 
	// 						 for (int t = 0; t < GRP_SFR_Max; t++)
	// 						 {
	// 							 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbMinValue[t]);
	// 							 pAddData->Add(strData);
	// 						 }
	// 
	// 						 for (int t = 0; t < ROI_SFR_Max; t++)
	// 						 {
	// 							 if (m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.bUse)
	// 							 {
	// 								 strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stFocus.stSFRData.dbValue[t]);
	// 							 }
	// 							 else
	// 							 {
	// 								 strData.Format(_T("X"));
	// 							 }
	// 
	// 							 pAddData->Add(strData);
	// 						 }
	// 
	// 	}
	// 		break;
	// 	case Sys_Image_Test:
	// 	{
	// 						   nResult = m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.nResult;
	// 						   for (int t = 0; t < MESDataNum_SFR; t++)
	// 						   {
	// 							   pAddHeaderz->Add(g_szMESHeader_SFR[t]);
	// 						   }
	// 
	// 						   for (int t = 0; t < GRP_SFR_Max; t++)
	// 						   {
	// 							   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.dbMinValue[t]);
	// 							   pAddData->Add(strData);
	// 						   }
	// 
	// 						   for (int t = 0; t < ROI_SFR_Max; t++)
	// 						   {
	// 
	// 							   if (m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.bUse)
	// 							   {
	// 								   strData.Format(_T("%.2f"), m_pInspInfo->CamInfo[nParaIdx].stImageQ.stSFRData.dbValue[t]);
	// 							   }
	// 							   else
	// 							   {
	// 								   strData.Format(_T("X"));
	// 							   }
	// 
	// 							   pAddData->Add(strData);
	// 						   }
	// 	}
	// 		break;
	// 	default:
	// 		break;
	// 	}

	//	pMesData->szMesTestData[nParaIdx][MesDataIdx_SFR] = strFullData;
}