﻿//*****************************************************************************
// Filename	: 	List_Worklist.cpp
// Created	:	2016/3/18 - 18:24
// Modified	:	2016/3/18 - 18:24
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// List_Worklist.cpp : implementation file
//

#include "stdafx.h"
#include "List_Worklist.h"
#include "CommonFunction.h"

// CList_Worklist

typedef enum enWorklistHeader
{
	WLH_No,
	WLH_Time,
	WLH_Equipment,
	WLH_Model,
	WLH_SWVersion,
	//WLH_LOTName,
	WLH_Barcode,
	WLH_Result,
	WLH_Socket,

	WLH_MaxCol,
};

// 헤더
static const TCHAR*	g_lpszHeader[] = 
{
	_T("No"),				// WLH_No
	_T("Time"),				// WLH_Time
	_T("Equipment"),		// WLH_Equipment
	_T("Model"),			// WLH_Model
	_T("SWVersion"),		// WLH_SWVersion
	//_T("LOTName"),		// WLH_LOTName
	_T("Barcode"),			// WLH_Barcode
	_T("Result"),			// WLH_Result
	_T("Socket"),			// WLH_Socket
	NULL 
};

const int	iListAglin[] = 
{ 
	LVCFMT_LEFT,	 // WLH_No
	LVCFMT_CENTER,	 // WLH_Time
	LVCFMT_CENTER, 	 // WLH_Equipment
	LVCFMT_CENTER,	 // WLH_Model
	LVCFMT_CENTER,	 // WLH_SWVersion
	//LVCFMT_CENTER,	 // WLH_LOTName
	LVCFMT_CENTER,	 // WLH_Barcode
	LVCFMT_CENTER,	 // WLH_Result
	LVCFMT_CENTER,	 // WLH_Socket

};

const int	iHeaderWidth[] = 
{
	50,		// WLH_No
	160,	// WLH_Time
	120,	// WLH_Equipment
	120,	// WLH_Model
	120,	// WLH_SWVersion
	//100,	// WLH_LOTName
	100,	// WLH_Barcode
	80,		// WLH_Result
	80,		// WLH_Socket
};


IMPLEMENT_DYNAMIC(CList_Worklist, CListCtrl)

CList_Worklist::CList_Worklist()
{
	m_Font.CreateStockObject(DEFAULT_GUI_FONT);
}

CList_Worklist::~CList_Worklist()
{
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CList_Worklist, CListCtrl)
	ON_WM_CREATE()
	ON_WM_SIZE()
	//ON_NOTIFY_REFLECT(NM_CLICK,	&CList_Worklist::OnNMClick)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


// CList_Worklist message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
int CList_Worklist::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CListCtrl::OnCreate(lpCreateStruct) == -1)
		return -1;

	SetFont(&m_Font);

	SetExtendedStyle(LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT | LVS_EX_DOUBLEBUFFER);

	InitHeader();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_Worklist::OnSize(UINT nType, int cx, int cy)
{
	CListCtrl::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

/*	int iColWidth[WLH_MaxCol] = { 0, };
	int iColDivide = 0;
	int iUnitWidth = 0;
	int iMisc = 0;

	for (int nCol = 0; nCol < WLH_MaxCol; nCol++)
	{
		iColDivide += iHeaderWidth[nCol];
	}

	CRect rectClient;
	GetClientRect(rectClient);

	for (int nCol = 0; nCol < WLH_MaxCol; nCol++)
	{
		iUnitWidth = (rectClient.Width() * iHeaderWidth[nCol]) / iColDivide;
		iMisc += iUnitWidth;
		SetColumnWidth(nCol, iUnitWidth);
	}

	iUnitWidth = ((rectClient.Width() * iHeaderWidth[WLH_No]) / iColDivide) + (rectClient.Width() - iMisc);
	SetColumnWidth(WLH_No, iUnitWidth);
*/
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
BOOL CList_Worklist::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style |= LVS_REPORT | LVS_SHOWSELALWAYS | LVS_SINGLESEL | WS_BORDER | WS_TABSTOP;
	cs.dwExStyle &= LVS_EX_GRIDLINES | LVS_EX_FULLROWSELECT;

	return CListCtrl::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnNMClick
// Access		: public  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2016/3/21 - 9:52
// Desc.		:
//=============================================================================
void CList_Worklist::OnNMClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	NM_LISTVIEW* pNMView = (NM_LISTVIEW*)pNMHDR;
	int index = pNMView->iItem;

	*pResult = 0;
}

//=============================================================================
// Method		: InitHeader
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/21 - 16:11
// Desc.		:
//=============================================================================
void CList_Worklist::InitHeader()
{
	for (int nCol = 0; nCol < WLH_MaxCol; nCol++)
	{
		InsertColumn(nCol, g_lpszHeader[nCol], iListAglin[nCol], iHeaderWidth[nCol]);
	}
}

//=============================================================================
// Method		: InsertRegion
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in const ST_Worklist * pstWorklist
// Qualifier	:
// Last Update	: 2016/8/12 - 13:59
// Desc.		:
//=============================================================================
void CList_Worklist::InsertRowItemz(__in const ST_Worklist* pstWorklist)
{
	ASSERT(GetSafeHwnd());

	int iNewCount = GetItemCount();

	CString strText;

	InsertItem(iNewCount, _T(""));
	
	SetRowItemz(iNewCount, pstWorklist);

 	strText.Format(_T("%d"), iNewCount + 1);
 	SetItemText(iNewCount, WLH_No, strText);
 
}

//=============================================================================
// Method		: SetRectRow
// Access		: protected  
// Returns		: void
// Parameter	: UINT nRow
// Parameter	: __in const ST_Worklist * pstWorklist
// Qualifier	:
// Last Update	: 2016/3/24 - 16:32
// Desc.		:
//=============================================================================
void CList_Worklist::SetRowItemz(UINT nRow, __in const ST_Worklist* pstWorklist)
{
	CString szItem;

	szItem = pstWorklist->Time;
	SetItemText(nRow, WLH_Time, szItem);
	szItem = pstWorklist->EqpID;
	SetItemText(nRow, WLH_Equipment, szItem);
	szItem = pstWorklist->SWVersion;
	SetItemText(nRow, WLH_SWVersion, szItem);
	szItem = pstWorklist->Model;
	SetItemText(nRow, WLH_Model, szItem);
	//szItem = pstWorklist->LOTName;
	//SetItemText(nRow, WLH_LOTName, szItem);
	szItem = pstWorklist->Barcode;
	SetItemText(nRow, WLH_Barcode, szItem);	
	szItem = pstWorklist->Result;
	SetItemText(nRow, WLH_Result, szItem);
	szItem = pstWorklist->Socket;
	SetItemText(nRow, WLH_Socket, szItem);
}

//=============================================================================
// Method		: InsertWorklist
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_Worklist * pstWorklist
// Qualifier	:
// Last Update	: 2016/8/12 - 17:13
// Desc.		:
//=============================================================================
void CList_Worklist::InsertWorklist(__in const ST_Worklist* pstWorklist)
{
 	m_Worklist.Add(*(ST_Worklist*)pstWorklist);
 
 	InsertRowItemz(pstWorklist);
 
 	// 500개가 되면 300개 삭제??
 	int iCount = GetItemCount();
 	if (500 < iCount)
 	{
 		for (int iCnt = 0; iCnt < 300; iCnt++)
 		{
 			DeleteItem(0);
 		}
 		m_Worklist.RemoveAt(0, 300);
 
 		iCount = GetItemCount();
 		CString strText;
 		for (int iIdx = 0; iIdx < iCount; iIdx++)
 		{
 			strText.Format(_T("%d"), iIdx + 1);
 			SetItemText(iIdx, WLH_No, strText);
 		}
 	}
}

//=============================================================================
// Method		: ClearWorklist
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/8/12 - 17:13
// Desc.		:
//=============================================================================
void CList_Worklist::ClearWorklist()
{
	m_Worklist.RemoveAll();

	this->DeleteAllItems();
}

//=============================================================================
// Method		: GetWorklist
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nRowIndex
// Parameter	: __out ST_Worklist & stWorklist
// Qualifier	:
// Last Update	: 2016/8/12 - 17:13
// Desc.		:
//=============================================================================
BOOL CList_Worklist::GetWorklist(__in UINT nRowIndex, __out ST_Worklist& stWorklist)
{
	INT_PTR iCount = m_Worklist.GetCount();

	if (iCount <= (int)nRowIndex)
		return FALSE;

	stWorklist = m_Worklist.GetAt(nRowIndex);

	return TRUE;
}
