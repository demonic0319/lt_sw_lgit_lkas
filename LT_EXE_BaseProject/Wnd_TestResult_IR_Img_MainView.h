﻿#ifndef Wnd_TestResult_IR_Img_MainView_h__
#define Wnd_TestResult_IR_Img_MainView_h__

#pragma once

#include "VGStatic.h"
#include "Def_DataStruct_Cm.h"
#include "Def_DataStruct.h"

#include "Wnd_Rst_Dynamic.h"
#include "Wnd_Rst_SFR.h"
#include "Wnd_Rst_Shading.h"
#include "Wnd_Rst_Current.h"
#include "Wnd_Rst_Ymean.h"
#include "Wnd_Rst_LCB.h"
#include "Wnd_Rst_BlackSpot.h"
#include "Wnd_Rst_Rllumination.h"
#include "Wnd_Rst_Rotate.h"
#include "Wnd_Rst_Defect_Black.h"
#include "Wnd_Rst_Defect_White.h"
#include "Wnd_Rst_Displace.h"
#include "Wnd_Rst_Vision.h"
#include "Wnd_Rst_OpticalCenter.h"
#include "Wnd_Rst_IIC.h"

// CWnd_TestResult_IR_Img_MainView
enum enTestResult_IR_Img_Main_Static
{
	STI_TR_IR_Img_Main_Title,
	STI_TR_IR_Img_Main_MAX,
};

static LPCTSTR	g_szTestResult_IR_Img_Main_Static[] =
{
	_T("TEST RESULT"),
	NULL
};

class CWnd_TestResult_IR_Img_MainView : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TestResult_IR_Img_MainView)

public:
	CWnd_TestResult_IR_Img_MainView();
	virtual ~CWnd_TestResult_IR_Img_MainView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate		(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize			(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow	(BOOL bShow, UINT nStatus);
	afx_msg void	OnRangeBtnCtrl	(UINT nID);
	virtual BOOL	PreCreateWindow	(CREATESTRUCT& cs);

	enInsptrSysType				m_InspectionType;

	CFont						m_font;
	
	CVGStatic					m_st_default;
	CVGStatic					m_st_Item[STI_TR_IR_Img_Main_MAX];

	CComboBox					m_cb_TestItem;

	int							m_iSelectTest[TI_IR_ImgT_MaxEnum];

	CWnd_Rst_Dynamic			m_Wnd_Rst_Dynamic;
	CWnd_Rst_SFR				m_Wnd_Rst_SFR;
	CWnd_Rst_Shading			m_Wnd_Rst_Shading;
	CWnd_Rst_Current			m_Wnd_Rst_Current;
	CWnd_Rst_Ymean				m_Wnd_Rst_Ymean;
	CWnd_Rst_LCB				m_Wnd_Rst_LCB;
	CWnd_Rst_BlackSpot			m_Wnd_Rst_BlackSpot;
	CWnd_Rst_Rotate				m_Wnd_Rst_Distortion;
	CWnd_Rst_Defect_Black		m_Wnd_Rst_DefectBlack;
	CWnd_Rst_Defect_White		m_Wnd_Rst_DefectWhite;
	CWnd_Rst_Rllumination		m_Wnd_Rst_RI;

	CWnd_Rst_Displace			m_Wnd_Rst_Displace;
	CWnd_Rst_Vision				m_Wnd_Rst_Vision;
	CWnd_Rst_OpticalCenter		m_Wnd_Rst_OpticalCenter;
	CWnd_Rst_IIC				m_Wnd_Rst_IIC;


public:
	afx_msg void OnLbnSelChangeTest();

	void SelectNum(UINT nSelect);

	enModelType		m_ModelType;

	void SetModelType(__in enModelType nModelType)
	{
		m_ModelType = nModelType;
		SelectNum(0);
	}

	// 검사기 종류 설정
	void	SetSystemType (__in enInsptrSysType nSysType)
	{
		m_InspectionType = nSysType;
	};

	void AllDataReset		();
	void SetUI_Reset		(int nIdx);
	void SetUIData			(int nIdx, LPVOID pParam);
	void SetClearTab		();
	void SetAddTab			(int nIdx, int &iItemCnt);
};
#endif // Wnd_TestResult_IR_Img_MainView_h__
