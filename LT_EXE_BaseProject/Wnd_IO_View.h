﻿//*****************************************************************************
// Filename	: Wnd_IO_View.h
// Created	: 2016/05/29
// Modified	: 2016/09/22
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************

#ifndef Wnd_IO_View_h__
#define Wnd_IO_View_h__

#pragma once

#include "Def_TestDevice.h"
#include "VGStatic.h"
#include "Wnd_BaseView.h"
#include "Wnd_IOTable.h"

//=============================================================================
// CWnd_IO_View
//=============================================================================
class CWnd_IO_View : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_IO_View)

public:
	CWnd_IO_View();
	virtual ~CWnd_IO_View();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	
	CWnd_IOTable	m_Wnd_IOTable;
	ST_Device*		m_pDevice				= NULL;

	// 검사기 설정
	enInsptrSysType		m_InspectionType	= enInsptrSysType::Sys_Focusing;

public:
	
	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);
	// 주변 장치 제어 구조체 포인터 설정
	void		SetPtr_Device			(__in ST_Device* pstDevice);

	// DI Bit 상태 UI에 표시
	void	Set_IO_DI_OffsetData		(__in BYTE byBitOffset, __in BOOL bOnOff);
	void	Set_IO_DI_Data				(__in LPBYTE lpbyDIData, __in UINT nCount);
	// DO Bit 상태 UI에 표시
	void	Set_IO_DO_OffsetData		(__in BYTE byBitOffset, __in BOOL bOnOff);
	void	Set_IO_DO_Data				(__in LPBYTE lpbyDOData, __in UINT nCount);
};

#endif // Wnd_IO_View_h__


