#ifndef Test_ResultDataView_h__
#define Test_ResultDataView_h__

#pragma once

#include "Wnd_MainView.h"
#include "Wnd_RecipeView.h"

class CTest_ResultDataView
{
public:
	CTest_ResultDataView();
	~CTest_ResultDataView();

	void TestResultView(UINT nTestID, UINT nPara, LPVOID pParam);
	void TestResultView_Foc			(__in UINT nTestID, __in UINT nPara, __in LPVOID pParam, __in enFocus_AAView enItem);
	void TestResultView_Reset(UINT nTestID, UINT nPara);
	void TestResultView_AllReset(UINT nPara);
	void TestResultView_AllReset_Foc(UINT nPara);
	void TestResultView_Reset_Foc(__in UINT nTestID, __in UINT nPara);
protected:
	CWnd_MainView *m_pMainView;
	CWnd_RecipeView *m_pRecipeView;


public:
	void SetPtr_MainView(CWnd_MainView *pView)
	{
		m_pMainView = pView;
	};
	void SetPtr_RecipeView(CWnd_RecipeView *pView)
	{
		m_pRecipeView = pView;
	};

};

#endif // Test_ResultDataView_h__
