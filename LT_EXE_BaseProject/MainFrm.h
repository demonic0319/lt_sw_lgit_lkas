﻿//*****************************************************************************
// Filename	: MainFrm.h
// Created	: 2010/11/22
// Modified	: 2010/11/22 - 17:59
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// MainFrm.h : CMainFrame 클래스의 인터페이스
//

#pragma once

#if (SET_INSPECTOR == SYS_2D_CAL)
#include "View_MainCtrl_2DCal.h"
#elif (SET_INSPECTOR == SYS_IR_IMAGE_TEST)
#include "View_MainCtrl_IR_ImgT.h"
#elif (SET_INSPECTOR == SYS_FOCUSING)
#include "View_MainCtrl_Foc.h"
#elif (SET_INSPECTOR == SYS_IMAGE_TEST)
#include "View_MainCtrl_ImgT.h"
#elif (SET_INSPECTOR == SYS_3D_CAL)
#include "View_MainCtrl_3DCal.h"
#else
#include "View_MainCtrl.h"
#endif

#include "NTCaptionBar.h"
#include "NTBannerBar.h"
#include "NTTabViewBar.h"
#include "NTLinksBar.h"
#include "Pane_CommStatus.h"
#include "Define_MonitorInfoFunc.h"

#ifndef CFrameWnd
#define CFrameWnd CFrameWndEx
#endif

//=============================================================================
//
//=============================================================================
class CMainFrame : public CFrameWndEx
{

public:
	CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

	// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);

	// 구현입니다.
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.

#if (SET_INSPECTOR == SYS_2D_CAL)
	CView_MainCtrl_2DCal	m_wndView_MainCtrl;
#elif (SET_INSPECTOR == SYS_IR_IMAGE_TEST)
	CView_MainCtrl_IR_ImgT		m_wndView_MainCtrl;
#elif (SET_INSPECTOR == SYS_FOCUSING)
	CView_MainCtrl_Foc		m_wndView_MainCtrl;
#elif (SET_INSPECTOR == SYS_IMAGE_TEST)
	CView_MainCtrl_ImgT		m_wndView_MainCtrl;
#elif (SET_INSPECTOR == SYS_3D_CAL)
	CView_MainCtrl_3DCal	m_wndView_MainCtrl;
#else
	CView_MainCtrl		m_wndView_MainCtrl;
#endif

	ST_MONITORINFO		m_infoMonitor;

	// 생성된 메시지 맵 함수
protected:
	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSetFocus			(CWnd *pOldWnd);

	afx_msg void	OnClose				();
	afx_msg void	OnActivate			(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnGetMinMaxInfo		(MINMAXINFO FAR* lpMMI);
	afx_msg void	OnSettingChange		(UINT uFlags, LPCTSTR lpszSection);

	afx_msg void	OnTabView			(UINT nID);
	afx_msg void	OnUpdateTabView		(CCmdUI* pCmdUI);

	afx_msg	LRESULT	OnLoadComplete		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnOption			(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnLogMsg			(WPARAM wParam, LPARAM lParam);

	afx_msg	LRESULT	OnWatchExtProcess	(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnOptionChanged		(WPARAM wParam, LPARAM lParam);

	afx_msg	LRESULT	OnTestFunction		(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnChangeView		(WPARAM wParam, LPARAM lParam);

	afx_msg	LRESULT	OnPermissionMode	(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnMESOnlineMode		(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnOperateMode		(WPARAM wParam, LPARAM lParam);

	afx_msg	LRESULT	OnManualBarcode		(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnManualOneItemTest	(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnEquipmentInit		(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnChart_Ctrl		(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnEEPROM_Ctrl		(WPARAM wParam, LPARAM lParam);
	afx_msg	LRESULT	OnCamera_Ctrl		(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()

	int				MakeTabViewBar		();	
	void			AddLog				(LPCTSTR lpszLog, BOOL bError = FALSE, BYTE bySubLog = 0);
	void			AddLogProgramInfo	();


	HANDLE			m_hThreadStartSetting;
	static UINT WINAPI	ThreadStartSetting	(LPVOID lParam);
	void			LoadStartSetting	();

	BOOL			m_bShowDeviceInfoPane;
	void			ShowPaneByTabIndex	(UINT nTabIndex);

private:

	CNTCaptionBar		m_wndCaptionBar;
	CNTBannerBar		m_wndBannerBar;	
	CNTTabViewBar		m_wndTabViewBar;
	CNTLinksBar			m_wndLinksBar;
	CPane_CommStatus	m_wndDeviceStatus;

	void			SetupMemoryBitmapSize	(const CSize& sz);

	UINT				m_nTabView;

	CString				m_strExecutedAppTime;

	// 프로그램 시작 할 때
	void			InitProgram				();

	// 프로그램 종료 할 때
	void			ExitProgram				();

	// 외부의 비정상 종료 감시 프로그램을 실행 시킴.	
	PROCESS_INFORMATION	m_ProcessInfo;
	BOOL			RunWatchProgram			();
	void			CloseWatchProgram		();


	void			ShowWindow_2ndView		(__in BOOL bShow);

	// 검사기 종류
	enInsptrSysType		m_InspectionType	= (enInsptrSysType)SET_INSPECTOR;

public:
	
	// 검사기 종류 설정
	virtual void	SetSystemType			(__in enInsptrSysType nSysType);
	
};


