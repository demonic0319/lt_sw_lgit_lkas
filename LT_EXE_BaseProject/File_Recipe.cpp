﻿#include "stdafx.h"
#include "File_Recipe.h"
#include "Def_Enum.h"
#include "CommonFunction.h"


#define		LVDS_AppName			_T("LVDS")
#define		BoardNumber_KeyName		_T("Board Number")
#define		SensorType_KeyName		_T("Sensor Type")
#define		ConvFormat_KeyName		_T("Conv Format")
#define		ClockSelect_KeyName		_T("Clock Select")
#define		ClockUse_KeyName		_T("Clock Use")
#define		DataMode_KeyName		_T("Data Mode")
#define		DvalUse_KeyName			_T("Dval Use")
#define		HsyncPolarity_KeyName	_T("Hsync Polarity")
#define		MIPILane_KeyName		_T("MIPI Lane")
#define		PClockPolarity_KeyName	_T("PClock Polarity")
#define		VideoMode_KeyName		_T("Video Mode")
#define		Clock_KeyName			_T("Clock")
#define		WidthMultiple_KeyName	_T("Width Multiple")
#define		HeightMultiple_KeyName	_T("Height Multiple")

#define		AppName_SteCal_Para		_T("Para_SteCal")

CFile_Recipe::CFile_Recipe()
{
}


CFile_Recipe::~CFile_Recipe()
{
}

//=============================================================================
// Method		: Load_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_Common(__in LPCTSTR szPath, __out ST_RecipeInfo& stRecipeInfo)
{
	BOOL bReturn = TRUE;

	bReturn = __super::Load_Common(szPath, stRecipeInfo);

	//bReturn &= Load_LVDS(szPath, stRecipeInfo.stLVDSInfo);


	return bReturn;
}

//=============================================================================
// Method		: Save_Default
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/1/3 - 15:55
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_Common(__in LPCTSTR szPath, __in const ST_RecipeInfo* pstRecipeInfo)
{
	BOOL bReturn = TRUE;

	bReturn = __super::Save_Common(szPath, pstRecipeInfo);

	//bReturn &= Save_LVDS(szPath, &pstRecipeInfo->stLVDSInfo);

	return bReturn;
}

//=============================================================================
// Method		: Load_RecipeFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_RecipeInfo & stRecipeInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:18
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_RecipeFile(__in LPCTSTR szPath, __out ST_RecipeInfo& stRecipeInfo)
{
	BOOL bReturn = TRUE;
	CFile_VI_Config Vl_Config;

	bReturn = __super::Load_RecipeFile(szPath, stRecipeInfo);
	bReturn &= Load_LVDS(szPath, stRecipeInfo.stLVDSInfo);
	
	switch (stRecipeInfo.nInspectionType)
	{
	case Sys_Focusing:
	{
// 		bReturn &= Vl_Config.Load_SFRFile			(szPath, stRecipeInfo.stFocus.stSFROpt);
	}
	break;

	case Sys_2D_Cal:
	{
		//bReturn &= Load_2DCal_Para(szPath, stRecipeInfo.st2D_CAL.Parameter);
		bReturn &= Load_2DCal_Info(szPath, stRecipeInfo.st2D_CAL);
	}
	break;

	case Sys_Image_Test:
	{
		
	}
	break;

	case Sys_IR_Image_Test:
	{
		bReturn &= Vl_Config.Load_DynamicBWFile		(szPath, stRecipeInfo.stIR_ImageQ.stDynamicBW		);
		bReturn &= Vl_Config.Load_SFRFile			(szPath, stRecipeInfo.stIR_ImageQ.stSFR				);
		bReturn &= Vl_Config.Load_ShadingFile		(szPath, stRecipeInfo.stIR_ImageQ.stShading			);
		bReturn &= Vl_Config.Load_CurrentFile		(szPath, stRecipeInfo.stIR_ImageQ.stCurrent			);
		bReturn &= Vl_Config.Load_YmeanFile			(szPath, stRecipeInfo.stIR_ImageQ.stYmean			);
		bReturn &= Vl_Config.Load_LCBFile			(szPath, stRecipeInfo.stIR_ImageQ.stLCB				);
		bReturn &= Vl_Config.Load_BlackSpotFile		(szPath, stRecipeInfo.stIR_ImageQ.stBlackSpot		);
		bReturn &= Vl_Config.Load_RlluminationFile	(szPath, stRecipeInfo.stIR_ImageQ.stRllumination	);
		bReturn &= Vl_Config.Load_ChartFile			(szPath, stRecipeInfo.stIR_ImageQ.stChart			);
		bReturn &= Vl_Config.Load_RotateFile		(szPath, stRecipeInfo.stIR_ImageQ.stDistortion		);
		bReturn &= Vl_Config.Load_Defect_BlackFile	(szPath, stRecipeInfo.stIR_ImageQ.stDefect_Black	);
		bReturn &= Vl_Config.Load_Defect_WhiteFile	(szPath, stRecipeInfo.stIR_ImageQ.stDefect_White	);
		bReturn &= Vl_Config.Load_DisplaceFile		(szPath, stRecipeInfo.stIR_ImageQ.stDisplace		);
		bReturn &= Vl_Config.Load_VisionFile		(szPath, stRecipeInfo.stIR_ImageQ.stVision			);
		bReturn &= Vl_Config.Load_IICFile			(szPath, stRecipeInfo.stIR_ImageQ.stIIC				);
		bReturn &= Vl_Config.Load_OpticalCenterFile	(szPath, stRecipeInfo.stIR_ImageQ.stOpticalCenter	);

	}
	break;

	case Sys_3D_Cal:
	{
		//bReturn &= Load_3DCal_Info(szPath, stRecipeInfo.st3D_CAL);
	}
	break;

	default:
		break;
	}

	return bReturn;
}

//=============================================================================
// Method		: Save_RecipeFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 11:26
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_RecipeFile(__in LPCTSTR szPath, __in const ST_RecipeInfo* pstRecipeInfo)
{
	BOOL bReturn = TRUE;
	CFile_VI_Config Vl_Config;

	bReturn = __super::Save_RecipeFile(szPath, pstRecipeInfo);
	bReturn &= Save_LVDS(szPath, &pstRecipeInfo->stLVDSInfo);

	switch (pstRecipeInfo->nInspectionType)
	{
	case Sys_Focusing:
	{
	;
	}
	break;

	case Sys_2D_Cal:
	{
		bReturn &= Save_2DCal_Info(szPath, &pstRecipeInfo->st2D_CAL);
	}
	break;

	case Sys_Image_Test:
	{
	
	}
	break;

	case Sys_IR_Image_Test:
	{
		bReturn &= Vl_Config.Save_DynamicBWFile		(szPath, &pstRecipeInfo->stIR_ImageQ.stDynamicBW	);
		bReturn &= Vl_Config.Save_SFRFile			(szPath, &pstRecipeInfo->stIR_ImageQ.stSFR			);
		bReturn &= Vl_Config.Save_ShadingFile		(szPath, &pstRecipeInfo->stIR_ImageQ.stShading		);
		bReturn &= Vl_Config.Save_CurrentFile		(szPath, &pstRecipeInfo->stIR_ImageQ.stCurrent		);
		bReturn &= Vl_Config.Save_YmeanFile			(szPath, &pstRecipeInfo->stIR_ImageQ.stYmean		);
		bReturn &= Vl_Config.Save_LCBFile			(szPath, &pstRecipeInfo->stIR_ImageQ.stLCB			);
		bReturn &= Vl_Config.Save_BlackSpotFile		(szPath, &pstRecipeInfo->stIR_ImageQ.stBlackSpot	);
		bReturn &= Vl_Config.Save_RlluminationFile	(szPath, &pstRecipeInfo->stIR_ImageQ.stRllumination	);
		bReturn &= Vl_Config.Save_ChartFile			(szPath, &pstRecipeInfo->stIR_ImageQ.stChart		);
		bReturn &= Vl_Config.Save_RotateFile		(szPath, &pstRecipeInfo->stIR_ImageQ.stDistortion);
		bReturn &= Vl_Config.Save_DefectBlackFile	(szPath, &pstRecipeInfo->stIR_ImageQ.stDefect_Black);
		bReturn &= Vl_Config.Save_DefectWhiteFile	(szPath, &pstRecipeInfo->stIR_ImageQ.stDefect_White);
		bReturn &= Vl_Config.Save_DisplaceFile		(szPath, &pstRecipeInfo->stIR_ImageQ.stDisplace);
		bReturn &= Vl_Config.Save_VisionFile		(szPath, &pstRecipeInfo->stIR_ImageQ.stVision);
		bReturn &= Vl_Config.Save_IICFile			(szPath, &pstRecipeInfo->stIR_ImageQ.stIIC);
		bReturn &= Vl_Config.Save_OpticalCenterFile	(szPath, &pstRecipeInfo->stIR_ImageQ.stOpticalCenter);

	}
	break;

	case Sys_3D_Cal:
	{
	}
	break;

	default:
		break;
	}

	return bReturn;
}

//=============================================================================
// Method		: Load_LVDS
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LVDSInfo & stLVDSInfo
// Qualifier	:
// Last Update	: 2017/8/24 - 16:45
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Load_LVDS(__in LPCTSTR szPath, __out ST_LVDSInfo& stLVDSInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR   inBuff[255] = { 0, };
	int		nData = 0;

	// Board Number
// 	nData = GetPrivateProfileInt(LVDS_AppName, BoardNumber_KeyName, 0, szPath);
// 	stLVDSInfo.nBoardNo[0] = nData;
// 
// 	// Sensor Type
// 	nData = GetPrivateProfileInt(LVDS_AppName, SensorType_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nSensorType = (enImgSensorType)nData;
// 
// 	// ConvFormat
// 	nData = GetPrivateProfileInt(LVDS_AppName, ConvFormat_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nConvFormat = (enConvFormat)nData;
// 
// 	// ClockSelect
// 	nData = GetPrivateProfileInt(LVDS_AppName, ClockSelect_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nClockSelect = (enDAQClockSelect)nData;
// 
// 	// ClockUse
// 	nData = GetPrivateProfileInt(LVDS_AppName, ClockUse_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nClockUse = (enDAQClockUse)nData;
// 
// 	// DataMode
// 	nData = GetPrivateProfileInt(LVDS_AppName, DataMode_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nDataMode = (enDAQDataMode)nData;
// 
// 	// DvalUse
// 	nData = GetPrivateProfileInt(LVDS_AppName, DvalUse_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nDvalUse = (enDAQDvalUse)nData;
// 
// 	// HsyncPolarity
// 	nData = GetPrivateProfileInt(LVDS_AppName, HsyncPolarity_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nHsyncPolarity = (enDAQHsyncPolarity)nData;
// 
// 	// MIPILane
// 	nData = GetPrivateProfileInt(LVDS_AppName, MIPILane_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nMIPILane = (enDAQMIPILane)nData;
// 
// 	// PClockPolarity
// 	nData = GetPrivateProfileInt(LVDS_AppName, PClockPolarity_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nPClockPolarity = (enDAQPclkPolarity)nData;
// 
// 	// VideoMode
// 	nData = GetPrivateProfileInt(LVDS_AppName, VideoMode_KeyName, 0, szPath);
// 	stLVDSInfo.stLVDSOption.nVideoMode = (enDAQVideoMode)nData;
// 
// 	// Clock
// 	GetPrivateProfileString(LVDS_AppName, Clock_KeyName, _T("10000"), inBuff, 255, szPath);
// 	stLVDSInfo.stLVDSOption.dwClock = (DWORD)_ttol(inBuff);
// 
// 	// WidthMultiple
// 	GetPrivateProfileString(LVDS_AppName, WidthMultiple_KeyName, _T("1.0"), inBuff, 255, szPath);
// 	stLVDSInfo.stLVDSOption.dWidthMultiple = _ttof(inBuff);
// 
// 	// HeightMultiple
// 	GetPrivateProfileString(LVDS_AppName, HeightMultiple_KeyName, _T("1.0"), inBuff, 255, szPath);
// 	stLVDSInfo.stLVDSOption.dHeightMultiple = _ttof(inBuff);

	// I2C File
	GetPrivateProfileString(LVDS_AppName, _T("I2C File"), _T(""), inBuff, 255, szPath);
	stLVDSInfo.strI2CFileName = inBuff;

	GetPrivateProfileString(LVDS_AppName, _T("I2C File_2"), _T(""), inBuff, 255, szPath);
	stLVDSInfo.strI2CFileName_2 = inBuff;
	return TRUE;
}

//=============================================================================
// Method		: Save_LVDS
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LVDSInfo * pstLVDSInfo
// Qualifier	:
// Last Update	: 2018/1/10 - 20:20
// Desc.		:
//=============================================================================
BOOL CFile_Recipe::Save_LVDS(__in LPCTSTR szPath, __in const ST_LVDSInfo* pstLVDSInfo)
{
	if (NULL == szPath)
		return FALSE;

	CString strValue;

	// Board Number
// 	strValue.Format(_T("%d"), pstLVDSInfo->nBoardNo[0]);
// 	WritePrivateProfileString(LVDS_AppName, BoardNumber_KeyName, strValue, szPath);
// 
// 	// Sensor Type
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nSensorType);
// 	WritePrivateProfileString(LVDS_AppName, SensorType_KeyName, strValue, szPath);
// 
// 	// ConvFormat
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nConvFormat);
// 	WritePrivateProfileString(LVDS_AppName, ConvFormat_KeyName, strValue, szPath);
// 
// 	// ClockSelect
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nClockSelect);
// 	WritePrivateProfileString(LVDS_AppName, ClockSelect_KeyName, strValue, szPath);
// 
// 	// ClockUse
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nClockUse);
// 	WritePrivateProfileString(LVDS_AppName, ClockUse_KeyName, strValue, szPath);
// 
// 	// DataMode
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nDataMode);
// 	WritePrivateProfileString(LVDS_AppName, DataMode_KeyName, strValue, szPath);
// 
// 	// DvalUse
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nDvalUse);
// 	WritePrivateProfileString(LVDS_AppName, DvalUse_KeyName, strValue, szPath);
// 
// 	// HsyncPolarity
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nHsyncPolarity);
// 	WritePrivateProfileString(LVDS_AppName, HsyncPolarity_KeyName, strValue, szPath);
// 
// 	// MIPILane
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nMIPILane);
// 	WritePrivateProfileString(LVDS_AppName, MIPILane_KeyName, strValue, szPath);
// 
// 	// PClockPolarity
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nPClockPolarity);
// 	WritePrivateProfileString(LVDS_AppName, PClockPolarity_KeyName, strValue, szPath);
// 
// 	// VideoMode
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.nVideoMode);
// 	WritePrivateProfileString(LVDS_AppName, VideoMode_KeyName, strValue, szPath);
// 
// 	// Clock
// 	strValue.Format(_T("%d"), pstLVDSInfo->stLVDSOption.dwClock);
// 	WritePrivateProfileString(LVDS_AppName, Clock_KeyName, strValue, szPath);
// 
// 	// WidthMultiple
// 	strValue.Format(_T("%1.1f"), pstLVDSInfo->stLVDSOption.dWidthMultiple);
// 	WritePrivateProfileString(LVDS_AppName, WidthMultiple_KeyName, strValue, szPath);
// 
// 	// HeightMultiple
// 	strValue.Format(_T("%1.1f"), pstLVDSInfo->stLVDSOption.dHeightMultiple);
// 	WritePrivateProfileString(LVDS_AppName, HeightMultiple_KeyName, strValue, szPath);

	// I2C File
	strValue = pstLVDSInfo->strI2CFileName;
	WritePrivateProfileString(LVDS_AppName, _T("I2C File"), strValue, szPath);

	strValue = pstLVDSInfo->strI2CFileName_2;
	WritePrivateProfileString(LVDS_AppName, _T("I2C File_2"), strValue, szPath);
	return TRUE;
}
