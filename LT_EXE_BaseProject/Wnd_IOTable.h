﻿//*****************************************************************************
// Filename	: 	Wnd_IOTable.c
// Created	:	2017/02/05 - 13:47
// Modified	:	2017/02/05 - 13:47
//
// Author	:	
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_IOTable_h__
#define Wnd_IOTable_h__

#pragma once

#include <afxwin.h>
#include "VGStatic.h"
#include "DigitalIOCtrl.h"

#define		MAX_DIO_COUNT	128

//-----------------------------------------------------------------------------
// CWnd_IOTable
//-----------------------------------------------------------------------------
class CWnd_IOTable : public CWnd
{
	DECLARE_DYNAMIC(CWnd_IOTable)

public:
	CWnd_IOTable();
	virtual ~CWnd_IOTable();

protected:
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);	
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	afx_msg void	OnRangeCmds				(UINT nID);

	DECLARE_MESSAGE_MAP()

	CFont			m_font_Data;

	// 컨트롤
	CVGStatic		m_st_DI;
	CVGStatic		m_st_DO;
	CVGStatic		m_st_DI_Name[MAX_DIO_COUNT];
	CVGStatic		m_st_DI_Status[MAX_DIO_COUNT];
	CVGStatic		m_st_DO_Name[MAX_DIO_COUNT];
	CMFCButton		m_bn_DO_Status[MAX_DIO_COUNT];

	// 멤버 변수
	LPCTSTR*		m_psz_DI_Name			= NULL;					// DI 명칭 설정용 포인터
	LPCTSTR*		m_psz_DO_Name			= NULL;					// DO 명칭 설정용 포인터
	UINT			m_nDI_Count				= 16;					// DI 사용 최대 개수
	UINT			m_nDO_Count				= 16;					// DO 사용 최대 개수
	COLORREF		m_clrSet				= RGB(204, 255, 204);	// Bit Set 상태의 Static 색상
	COLORREF		m_clrClear				= RGB(255, 255, 255);	// Bit Clear 상태의 Static 색상

	UINT			m_nMaxRowCount			= 16;					// 화면에 표시될 Row 최대 개수

	// Digital I/O 명칭 UI에 표시
	void			Update_DIO_ItemName		();

public:

	// DI/DO의 사용 개수를 설정
	void			Set_DIO_Count			(__in UINT nDI_Count, __in UINT nDO_Count);
	void			Set_DIO_Name			(__in LPCTSTR* pszDI_name, __in LPCTSTR* pszDO_Name);

	// 색상 설정
	void			Set_Color_BitSet		(__in COLORREF clrSet);
	void			Set_Color_BitClear		(__in COLORREF clrClear);	

	// Row 최대 표시 개수 설정
	void			Set_MaxRowCount			(__in UINT nCount);

	// DI Bit 상태 UI에 표시
	void			Set_IO_DI_OffsetData	(__in BYTE byBitOffset, __in BOOL bOnOff);
	void			Set_IO_DI_Data			(__in LPBYTE lpbyDIData, __in UINT nCount);
	// DO Bit 상태 UI에 표시
	void			Set_IO_DO_OffsetData	(__in BYTE byBitOffset, __in BOOL bOnOff);
	void			Set_IO_DO_Data			(__in LPBYTE lpbyDOData, __in UINT nCount);
	
// 재정의
protected:
	CDigitalIOCtrl*		m_pstDevice				= NULL;

	// DIO 인터페이스로 Bit 상태를 변경함.
	virtual BOOL	OnSet_DO_BitStatus		(__in UINT nOffset, __in BOOL bBitStatus);

public:
	
	// DIO 인터페이스의 포인터 설정
	virtual void	SetPtr_Device			(__in CDigitalIOCtrl* pstDevice);
};

#endif // Wnd_IOTable_h__
