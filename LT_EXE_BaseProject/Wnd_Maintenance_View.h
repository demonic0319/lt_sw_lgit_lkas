﻿//*****************************************************************************
// Filename	: Wnd_Maintenance_View.h
// Created	: 2017/04/03
// Modified	: 2017/04/03
//
// Author	: KHO
//	
// Purpose	: 기본 화면용 윈도우
//*****************************************************************************
#ifndef Wnd_Maintenance_View_h__
#define Wnd_Maintenance_View_h__

#pragma once

#include "Wnd_BaseView.h"
#include "Def_TestDevice.h"
#include "Def_DataStruct.h"
#include "VGStatic.h"
#include "File_WatchList.h"
#ifndef MOTION_NOT_USE
#include "Wnd_MotorView.h"
#endif
#include "Wnd_Cfg_LightCtrl.h"

#include "Wnd_MT_PCBComm.h"
#include "Wnd_MT_Calibration.h"
#include "Wnd_MT_ImageProc.h"
#include "Wnd_MT_Particle.h"
#include "Wnd_ImgProc.h"
#include "Wnd_CylinderTable.h"

//=============================================================================
// CWnd_Maintenance_View
//=============================================================================
class CWnd_Maintenance_View : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Maintenance_View)

public:
	CWnd_Maintenance_View();
	virtual ~CWnd_Maintenance_View();

	CWnd_MT_PCBComm&	GetWnd_MT_PCBComm()
	{
		return m_wnd_MT_PCBComm;
	}

	CWnd_MT_Calibration&	GetWnd_MT_PCBCommPg2()
	{
		return m_wnd_MT_Calibration;
	}

	CWnd_MT_ImageProc&	GetWnd_MT_PCBCommPg3()
	{
		return m_wnd_MT_ImageTest;
	}

	CWnd_MT_Particle&	GetWnd_MT_PCBCommPg4()
	{
		return m_wnd_MT_Particle;
	}

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	afx_msg void	OnBnClickedRbCameraSelLeft();
	afx_msg void	OnBnClickedRbCameraSelRight();

	virtual void	MoveWindow_Image		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Info			(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);

	CFont					m_Font;

	// 좌측
	CWnd_ImgProc			m_wnd_ImageLive[USE_CHANNEL_CNT];
	CWnd_ImgProc			m_wnd_ImageLive_Vision;
	CMFCButton				m_rb_CameraSelect[USE_CHANNEL_CNT];		// 카메라 선택 버튼
	// Power On/Off
	// 영상 On/Off

	// 우측
	CMFCTabCtrl				m_tc_Item;
#ifndef MOTION_NOT_USE
	CWnd_MotorView			m_Wnd_MotorView;
#endif
	CWnd_Cfg_LightCtrl		m_Wnd_LightCtrl;
	CWnd_CylinderTable		m_Wnd_CylinderTable;

	CWnd_MT_PCBComm			m_wnd_MT_PCBComm;
	CWnd_MT_Calibration		m_wnd_MT_Calibration;
	CWnd_MT_ImageProc		m_wnd_MT_ImageTest;
	CWnd_MT_Particle		m_wnd_MT_Particle;

	ST_Device*				m_pDevice;
	ST_InspectionInfo*		m_pstInspInfo;

	CString					m_szMotorpath;
	CString					m_szMaintenancePath;

	// 검사기 설정
	enInsptrSysType			m_InspectionType = enInsptrSysType::Sys_Focusing;
	
public:
	
	// 전체 검사 정보 구조체 포인터 설정
	void	SetPtrInspectionInfo(__in ST_InspectionInfo* pstInspInfo)
	{
		if (pstInspInfo == NULL)
			return;

		m_pstInspInfo = pstInspInfo;

		m_Wnd_LightCtrl.SetPtr_MaintenanceInfo(&m_pstInspInfo->MaintenanceInfo);
#ifndef MOTION_NOT_USE
		m_Wnd_MotorView.SetPtr_MaintenanceInfo(&m_pstInspInfo->MaintenanceInfo);
#endif
	};

	void	SetPtr_Device	(__in ST_Device* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pDevice = pstDevice;
#ifndef MOTION_NOT_USE
		m_Wnd_MotorView.SetPtr_Device(&m_pDevice->MotionManager);
#endif
		m_Wnd_LightCtrl.SetPtr_Device(m_pDevice);
	};
	
	void	SetPath			(__in LPCTSTR szMotorPath, __in LPCTSTR szMaintenancePath)
	{
		if (NULL != szMotorPath)
			m_szMotorpath = szMotorPath;

		if (NULL != szMaintenancePath)
			m_szMaintenancePath = szMaintenancePath;

#ifndef MOTION_NOT_USE
		m_Wnd_MotorView.SetPath(m_szMotorpath);
#endif
		m_Wnd_LightCtrl.SetPath(szMaintenancePath);
	};

	void	UpdateMotorInfo			(__in CString szMotorFile);
	void	UpdateMaintenanceInfo	(__in CString szMaintenanceFile);

	// 검사기 종류 설정
	void	SetSystemType			(__in enInsptrSysType nSysType);

	void	ShowVideo				(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void	NoSignal_Ch				(__in INT iChIdx);

	void	ShowVideo_Vision		(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void	NoSignal_Ch_Vision		(__in INT iChIdx);

	
	void	Set_CameraSelect		(__in INT iChIdx, __in BOOL bUpdateRadioButton = TRUE);
};

#endif // Wnd_Maintenance_View
