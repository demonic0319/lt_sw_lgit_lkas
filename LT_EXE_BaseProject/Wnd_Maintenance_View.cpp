﻿//*****************************************************************************
// Filename	: Wnd_Maintenance_View.cpp
// Created	: 2017/04/03
// Modified	: 2017/04/03
//
// Author	: KHO
//	
// Purpose	: 
//*****************************************************************************
// Wnd_Maintenance_View.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_Maintenance_View.h"
#include "resource.h"

#define IDC_RB_CAMERA_SEL_LEFT		1002
#define IDC_RB_CAMERA_SEL_RIGHT		1003
//=============================================================================
// CWnd_Maintenance_View
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_Maintenance_View, CWnd_BaseView)

CWnd_Maintenance_View::CWnd_Maintenance_View()
{
	m_pDevice = NULL;
	VERIFY(m_Font.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Maintenance_View::~CWnd_Maintenance_View()
{
}

BEGIN_MESSAGE_MAP(CWnd_Maintenance_View, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_RB_CAMERA_SEL_LEFT, OnBnClickedRbCameraSelLeft)
	ON_BN_CLICKED(IDC_RB_CAMERA_SEL_RIGHT, OnBnClickedRbCameraSelRight)
END_MESSAGE_MAP()

//=============================================================================
// CWnd_Maintenance_View 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_Maintenance_View::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
int CWnd_Maintenance_View::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_tc_Item.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	UINT nWndID = 10;

	m_Wnd_CylinderTable.SetOwner(GetOwner());
	m_Wnd_CylinderTable.Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);

#ifndef MOTION_NOT_USE
	m_Wnd_MotorView.SetOwner(GetOwner());
	m_Wnd_MotorView.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Item, nWndID++);
#endif

	m_Wnd_LightCtrl.SetOwner(GetOwner());
	m_Wnd_LightCtrl.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Item, nWndID++);

	m_wnd_MT_PCBComm.SetOwner(GetParent());
	m_wnd_MT_PCBComm.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Item, nWndID++);

	m_wnd_MT_Calibration.SetOwner(GetParent());
	m_wnd_MT_Calibration.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Item, nWndID++);

	m_wnd_MT_ImageTest.SetOwner(GetParent());
	m_wnd_MT_ImageTest.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Item, nWndID++);

	m_wnd_MT_Particle.SetOwner(GetParent());
	m_wnd_MT_Particle.Create(NULL, _T(""), dwStyle, rectDummy, &m_tc_Item, nWndID++);


	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_ImageLive[nIdx].Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);

	}
	m_wnd_ImageLive[Para_Right].ShowWindow(SW_HIDE);

	m_wnd_ImageLive_Vision.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);

	// * 좌/우 카메라 선택
	m_rb_CameraSelect[Para_Left].Create(_T("Left Camera"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON | WS_GROUP, rectDummy, this, IDC_RB_CAMERA_SEL_LEFT);
	m_rb_CameraSelect[Para_Right].Create(_T("Right Camera"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON, rectDummy, this, IDC_RB_CAMERA_SEL_RIGHT);

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_rb_CameraSelect[nIdx].SetFont(&m_Font);
		m_rb_CameraSelect[nIdx].m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
		m_rb_CameraSelect[nIdx].SetImage(IDB_SELECTNO_16);
		m_rb_CameraSelect[nIdx].SetCheckedImage(IDB_SELECT_16);
		m_rb_CameraSelect[nIdx].SizeToContent();
		m_rb_CameraSelect[nIdx].SetCheck(TRUE);
		m_rb_CameraSelect[nIdx].ShowWindow(SW_HIDE);
	}


	UINT nIdxCnt = 0;

#ifndef MOTION_NOT_USE
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		m_tc_Item.AddTab(&m_Wnd_MotorView, _T("Motor"), nIdxCnt++, FALSE);
	}
#endif
	
	if ((0 < g_InspectorTable[m_InspectionType].LightPSU_Cnt) || (0 < g_InspectorTable[m_InspectionType].LightBrd_Cnt))
	{
		m_tc_Item.AddTab(&m_Wnd_LightCtrl, _T("Light"), nIdxCnt++, FALSE);
	}

	m_tc_Item.SetActiveTab(0);
	m_tc_Item.EnableTabSwap(FALSE);

	return 0;
}

//=============================================================================
// Method		: CWnd_Maintenance_View::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

 	int iMagrin = 5;
 	//int iSpacing = 5;
 	int iLeft = iMagrin;
 	int iTop = iMagrin;
 	int iWidth = cx - iMagrin - iMagrin;
 	int iHeight = cy - iMagrin - iMagrin;
 
 	int iImageW = 540;
 	int iImageH = 440;
 
 	int iCtrlHeight = 25;
 	int iCtrlWidth = iImageW / 3;
 	m_wnd_ImageLive[0].MoveWindow(iLeft, iTop, iImageW, iImageH);
 	//m_wnd_ImageLive[1].MoveWindow(iLeft, iTop, iImageW, iImageH);

	iTop += iImageH + 2;
	m_wnd_ImageLive_Vision.MoveWindow(iLeft, iTop, iImageW, iImageH);
 
//  	iTop += iImageH + 2;
//  	m_rb_CameraSelect[Para_Left].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
//  
//  	iLeft = iLeft + iImageW - iCtrlWidth;
//  	m_rb_CameraSelect[Para_Right].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

	if (Sys_Focusing == m_InspectionType)
	{
		iLeft = iMagrin;
		iTop += iCtrlHeight + iMagrin;
		m_Wnd_CylinderTable.MoveWindow(iLeft, iTop, iImageW, iHeight - iTop);
	}

	iLeft = iMagrin + iImageW + iMagrin;
	iTop = iMagrin;

	m_tc_Item.MoveWindow(iLeft, iTop, iWidth - (iImageW + iMagrin), iHeight);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual protected 
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2015/12/6 - 15:50
// Desc.		:
//=============================================================================
BOOL CWnd_Maintenance_View::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnBnClickedRbCameraSelLeft
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/13 - 15:44
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::OnBnClickedRbCameraSelLeft()
{
	Set_CameraSelect(Para_Left);
}

//=============================================================================
// Method		: OnBnClickedRbCameraSelRight
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/13 - 15:44
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::OnBnClickedRbCameraSelRight()
{
	Set_CameraSelect(Para_Right);
}


//=============================================================================
// Method		: MoveWindow_Image
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/3/13 - 15:44
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::MoveWindow_Image(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{

}

//=============================================================================
// Method		: MoveWindow_Info
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/3/13 - 15:44
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::MoveWindow_Info(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{

}

//=============================================================================
// Method		: UpdateMotorInfo
// Access		: public  
// Returns		: void
// Parameter	: __in CString szMotorFile
// Qualifier	:
// Last Update	: 2017/10/2 - 15:25
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::UpdateMotorInfo(__in CString szMotorFile)
{
#ifndef MOTION_NOT_USE
	m_Wnd_MotorView.UpdateMotorInfo(szMotorFile);
#endif
}

//=============================================================================
// Method		: UpdateMaintenanceInfo
// Access		: public  
// Returns		: void
// Parameter	: __in CString szMaintenanceFile
// Qualifier	:
// Last Update	: 2017/10/12 - 13:06
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::UpdateMaintenanceInfo(__in CString szMaintenanceFile)
{
	m_Wnd_LightCtrl.UpdateMaintenanceInfo(szMaintenanceFile);
}

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 13:58
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;

#ifndef MOTION_NOT_USE
	m_Wnd_MotorView.SetSystemType(m_InspectionType);
#endif
	m_Wnd_LightCtrl.SetSystemType(m_InspectionType);
}

//=============================================================================
// Method		: ShowVideo
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/3/15 - 11:31
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::ShowVideo(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (0 <= iChIdx)
	{
		m_wnd_ImageLive[iChIdx].Render(lpVideo, dwWidth, dwHeight);
	}
}

//=============================================================================
// Method		: NoSignal_Ch
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Qualifier	:
// Last Update	: 2018/3/15 - 11:31
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::NoSignal_Ch(__in INT iChIdx)
{
	if (0 <= iChIdx)
	{
		m_wnd_ImageLive[iChIdx].Render_Empty();
	}
}


void CWnd_Maintenance_View::ShowVideo_Vision(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (0 <= iChIdx)
	{
		m_wnd_ImageLive_Vision.Render(lpVideo, dwWidth, dwHeight);
	}
}

void CWnd_Maintenance_View::NoSignal_Ch_Vision(__in INT iChIdx)
{
	if (0 <= iChIdx)
	{
		m_wnd_ImageLive_Vision.Render_Empty();
	}
}

//=============================================================================
// Method		: Set_CameraSelect
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in BOOL bUpdateRadioButton
// Qualifier	:
// Last Update	: 2018/3/15 - 11:31
// Desc.		:
//=============================================================================
void CWnd_Maintenance_View::Set_CameraSelect(__in INT iChIdx, __in BOOL bUpdateRadioButton/* = TRUE*/)
{
	m_Wnd_CylinderTable.SetControlParaID(iChIdx);

	if (iChIdx < USE_CHANNEL_CNT)
	{
// 		if (Para_Left == iChIdx)
// 		{
// 			m_wnd_ImageLive[Para_Left].ShowWindow(SW_SHOW);
// 			m_wnd_ImageLive[Para_Right].ShowWindow(SW_HIDE);
// 		
// 			if (bUpdateRadioButton)
// 			{
// 				m_rb_CameraSelect[Para_Right].SetCheck(BST_UNCHECKED);
// 				m_rb_CameraSelect[Para_Left].SetCheck(BST_CHECKED);
// 			}
// 		}
// 		else if (Para_Right == iChIdx)
// 		{
// 			m_wnd_ImageLive[Para_Right].ShowWindow(SW_SHOW);
// 			m_wnd_ImageLive[Para_Left].ShowWindow(SW_HIDE);
// 
// 			if (bUpdateRadioButton)
// 			{
// 				m_rb_CameraSelect[Para_Left].SetCheck(BST_UNCHECKED);
// 				m_rb_CameraSelect[Para_Right].SetCheck(BST_CHECKED);
// 			}
// 		}
	}
}
