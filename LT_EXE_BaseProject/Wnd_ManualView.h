﻿//*****************************************************************************
// Filename	: Wnd_ManualView.h
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 기본 화면용 윈도우
//*****************************************************************************
#ifndef Wnd_ManualView_h__
#define Wnd_ManualView_h__

#pragma once

#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_Enum.h"
#include "Def_DataStruct.h"
#include "Def_TestDevice.h"

#include "Wnd_LGETestResult.h"

#include "Wnd_LayoutFrame.h"

//=============================================================================
// CWnd_ManualView
//=============================================================================
class CWnd_ManualView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_ManualView)

public:
	CWnd_ManualView();
	virtual ~CWnd_ManualView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);

	afx_msg void	OnCmdRanges				(__in UINT nID);
	afx_msg void	OnRangePort				(__in UINT nID);

	afx_msg LRESULT	OnMsgManualSequence		(WPARAM wParam, LPARAM lParam);
	
	void			MoveWindow_Monitoring	(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	void			MoveWindow_TestList		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	void			MoveWindow_Info			(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	void			MoveWindow_Status		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);

	CFont			m_font_Default;	
	CFont			m_Font;

	CVGStatic			m_st_frameZone;
	CVGStatic			m_st_frameSatus;
	CVGStatic			m_st_frameInfo;

	UINT			m_nPort;

	CMFCButton		m_bn_Item[Para_MaxEnum];

	// 검사결과
	CVGStatic			m_st_Judgment;

	CWnd_LayoutFrame	m_wndLayout;

	CWnd_LGETestResult	m_wnd_TestResult;
	
	// 데이터
	enPermissionMode	m_InspMode;
	ST_InspectionInfo*	m_pstInspInfo;

	ST_Device*			m_pDevice;

	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_Focusing;

public:
	
	// 검사기 종류 설정
	void		SetSystemType				(__in enInsptrSysType nSysType);

	void		SetPtrInspectionInfo		(__inout ST_InspectionInfo* pstInspInfo)
	{
		m_pstInspInfo = pstInspInfo;
	};

	void		SetPtr_Device				(__in ST_Device* pDevice)
	{
		m_pDevice = pDevice;
	};

	// 검사 모드 설정
	void		SetPermissionMode			(__in enPermissionMode InspMode);
	
	

	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};

#endif // Wnd_ManualView_h__
