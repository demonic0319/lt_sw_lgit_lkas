﻿// Wnd_ImageView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_ImageView.h"
// CWnd_ImageView

//#define CAM_IMAGE_WIDTH				640	// 영상	사이즈
//#define CAM_IMAGE_HEIGHT			480	// 영상	사이즈

IMPLEMENT_DYNAMIC(CWnd_ImageView, CWnd)

CWnd_ImageView::CWnd_ImageView()
{
	m_iROInum			= -1;
	m_nROICntMax		= 0;
	m_hTimerViewCheck	= NULL;
	m_hTimerQueue		= NULL;

#ifdef USE_VisionInspection
	m_pLoadImage		= NULL;
	m_pOriginImage		= NULL;
	m_pPicImage			= NULL;
#endif

	m_nWidth			= 640;
	m_nHeight			= 480;
	m_nExposure			= 0;

	m_BitmapInfo.bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);
	m_BitmapInfo.bmiHeader.biPlanes			= 1;
	m_BitmapInfo.bmiHeader.biCompression	= BI_RGB;
	m_BitmapInfo.bmiHeader.biSizeImage		= 0;
	m_BitmapInfo.bmiHeader.biXPelsPerMeter	= 0;
	m_BitmapInfo.bmiHeader.biYPelsPerMeter	= 0;
	m_BitmapInfo.bmiHeader.biClrUsed		= 0;
	m_BitmapInfo.bmiHeader.biClrImportant	= 0;
	m_BitmapInfo.bmiColors[0].rgbBlue		= 0;
	m_BitmapInfo.bmiColors[0].rgbGreen		= 0;
	m_BitmapInfo.bmiColors[0].rgbRed		= 0;
	m_BitmapInfo.bmiColors[0].rgbReserved	= 0;
	m_BitmapInfo.bmiHeader.biWidth			= 0;
	m_BitmapInfo.bmiHeader.biHeight			= 0;

	m_nMouseEdit		= MouseEdit_Init;

	m_bflg_MauseMode	= Mouse_None;
	m_bflg_Timer		= FALSE;

 	CreateTimerQueue_Mon();
 	CreateTimerViewCheck();
}

CWnd_ImageView::~CWnd_ImageView()
{
	DeleteTimerQueue_Mon();
}

BEGIN_MESSAGE_MAP(CWnd_ImageView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	ON_WM_ERASEBKGND()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()
// CWnd_ImageView 메시지 처리기입니다.

//=============================================================================
// Method		: CreateTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::CreateTimerQueue_Mon()
{
	__try
	{
		// Create the timer queue.
		m_hTimerQueue = CreateTimerQueue();
		if (NULL == m_hTimerQueue)
		{
			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
			return;
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		//	AddLog(_T("*** Exception Error : CreateTimerQueue_Mon ()"));
	}
}

//=============================================================================
// Method		: DeleteTimerQueue_Mon
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::DeleteTimerQueue_Mon()
{
	if (m_hTimerQueue == NULL)
		return;

	// 타이머가 종료될때까지 대기
	__try
	{
		if (!DeleteTimerQueue(m_hTimerQueue))
			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
		else
			m_hTimerQueue = NULL;
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_ImageView::DeleteTimerQueue_Mon()\n"));
	}

	TRACE(_T("타이머 종료 : CWnd_ImageView::DeleteTimerQueue_Mon()\n"));
}

//=============================================================================
// Method		: CreateTimer_InputCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::CreateTimerViewCheck()
{
	__try
	{
		// Time Check Timer
		if (NULL == m_hTimerViewCheck)
		if (!CreateTimerQueueTimer(&m_hTimerViewCheck, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutineViewCheck, (PVOID)this, 3000, 50, WT_EXECUTEDEFAULT))
		{
			TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_ImageView::CreateTimer_InputCheck()\n"));
	}
}

//=============================================================================
// Method		: DeleteTimerViewCheck
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_ImageView::DeleteTimerViewCheck()
{
	__try
	{
		if (DeleteTimerQueueTimer(m_hTimerQueue, m_hTimerViewCheck, NULL))
		{
			m_hTimerViewCheck = NULL;
		}
		else
		{
			TRACE(_T("DeleteTimerQueueTimer : m_hTimer_InputCheck failed (%d)\n"), GetLastError());
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CWnd_ImageView::DeleteTimerViewCheck()\n"));
	}
}

//=============================================================================
// Method		: TimerRoutine_InputCheck
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
VOID CALLBACK CWnd_ImageView::TimerRoutineViewCheck(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CWnd_ImageView* pThis = (CWnd_ImageView*)lpParam;

#ifdef USE_VisionInspection
	if (TRUE == pThis->m_bflg_Timer)
		pThis->OnImageView(_pstRecipeInfo->TestItemOpt.iPicItem);
#endif
}


//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/10/16 - 15:30
// Desc.		:
//=============================================================================
int CWnd_ImageView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_stImage.SetStaticStyle(CVGStatic::StaticStyle_Data);
	m_stImage.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_stImage.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_stImage.SetText(_T("EMPTY"));

	return 0;
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/10/16 - 15:30
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/10/16 - 15:30
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if (cx == 0 || cy == 0)
		return;

	m_stImage.MoveWindow(0, 0, cx, cy);
}

//=============================================================================
// Method		: OnLButtonDown
// Access		: protected  
// Returns		: void
// Parameter	: UINT nFlags
// Parameter	: CPoint point
// Qualifier	:
// Last Update	: 2017/10/17 - 12:56
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_bflg_MauseMode = Mouse_Edit;

	CRect rt;
	m_stImage.GetClientRect(&rt);

	if (rt.left <= point.x && rt.right >= point.x && rt.top <= point.y && rt.bottom >= point.y)
	{
		m_ptInit.x = (int)(point.x * (double)((double)m_nWidth / (double)rt.Width()));
		m_ptInit.y = (int)(point.y * (double)((double)m_nHeight / (double)rt.Height()));
	}

	CWnd::OnLButtonDown(nFlags, point);
}

//=============================================================================
// Method		: OnLButtonUp
// Access		: protected  
// Returns		: void
// Parameter	: UINT nFlags
// Parameter	: CPoint point
// Qualifier	:
// Last Update	: 2017/10/17 - 12:56
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_bflg_MauseMode = Mouse_None;
	SetCursor(LoadCursor(NULL, IDC_ARROW));

	CWnd::OnLButtonUp(nFlags, point);
}

//=============================================================================
// Method		: OnMouseMove
// Access		: protected  
// Returns		: void
// Parameter	: UINT nFlags
// Parameter	: CPoint point
// Qualifier	:
// Last Update	: 2017/10/17 - 12:56
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnMouseMove(UINT nFlags, CPoint point)
{
	CRect rt;
	m_stImage.GetClientRect(&rt);

	m_ptEdit = point;
	
	if (m_bflg_MauseMode == Mouse_Edit)
	{
		int mx = (int)(m_ptEdit.x * (double)((double)m_nWidth / (double)rt.Width()));
		int my = (int)(m_ptEdit.y * (double)((double)m_nHeight / (double)rt.Height()));

		int iAddX = mx - m_ptInit.x;
		int iAddY = my - m_ptInit.y;

		if (m_iROInum < 0)
			return;
		
		CRect rtROI = GetROIData(m_iROInum);

		switch (m_nMouseEdit)
		{
		case MouseEdit_Left_WE:
			if (rtROI.left - iAddX < 0)
				return;

			rtROI.left += iAddX;
			break;
		case MouseEdit_Right_WE:
			if (rtROI.right - iAddX < 0)
				return;

			rtROI.right += iAddX;
			break;
		case MouseEdit_Top_NS:
			if (rtROI.top - iAddY < 0)
				return;

			rtROI.top += iAddY;
			break;
		case MouseEdit_Bottom_NS:
			if (rtROI.bottom - iAddY < 0)
				return;

			rtROI.bottom += iAddY;
			break;
		case MouseEdit_Left_NWSE:
			if (rtROI.left - iAddX < 0 || rtROI.top - iAddY < 0)
				return;

			rtROI.left += iAddX;
			rtROI.top  += iAddY;
			break;
		case MouseEdit_Right_NWSE:
			if (rtROI.right - iAddX < 0 || rtROI.bottom - iAddY < 0)
				return;

			rtROI.right  += iAddX;
			rtROI.bottom += iAddY;
			break;
		case MouseEdit_Left_NESW:
			if (rtROI.right - iAddX < 0 || rtROI.top - iAddY < 0)
				return;

			rtROI.right += iAddX;
			rtROI.top	+= iAddY;
			break;
		case MouseEdit_Right_NESW:
			if (rtROI.left - iAddX < 0 || rtROI.bottom - iAddY < 0)
				return;

			rtROI.left	 += iAddX;
			rtROI.bottom += iAddY;
			break;
		case MouseEdit_ALL:
			rtROI.left	 += iAddX;
			rtROI.right	 += iAddX;
			rtROI.top	 += iAddY;
			rtROI.bottom += iAddY;
			break;
		default:
			break;
		}

		SetROIData(m_iROInum, rtROI);

		// GetOwner()->SendMessage(WM_CHANGE_PIC, 0, 0);

		m_ptInit.x = (int)(m_ptEdit.x * (double)((double)m_nWidth / (double)rt.Width()));
		m_ptInit.y = (int)(m_ptEdit.y * (double)((double)m_nHeight / (double)rt.Height()));
	}

	CWnd::OnMouseMove(nFlags, point);
}

//=============================================================================
// Method		: OnSetCursor
// Access		: protected  
// Returns		: BOOL
// Parameter	: CWnd * pWnd
// Parameter	: UINT nHitTest
// Parameter	: UINT message
// Qualifier	:
// Last Update	: 2017/10/17 - 12:57
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message)
{
	CRect rt;
	m_stImage.GetClientRect(&rt);
	
	if (m_ptEdit.x < rt.left || m_ptEdit.x > rt.right || m_ptEdit.y < rt.top || m_ptEdit.y > rt.bottom)
		return FALSE;

#ifdef USE_VisionInspection
	switch (m_pstRecipeInfo->TestItemOpt.iPicItem)
	{
	case Pic_Distortion:
		m_nROICntMax = ROI_DT_Max;
		break;
	case Pic_Dynamic:
		m_nROICntMax = ROI_DY_Max * IDX_DY_Max;
		break;
	case Pic_Rotate:
		m_nROICntMax = ROI_RT_Max;
		break;
	case Pic_SFR:
		m_nROICntMax = ROI_SFR_Max;
		break;
	case Pic_SNR_BW:
		m_nROICntMax = ROI_SN_BW_Max * IDX_SN_BW_Max;
		break;
	case Pic_SNR_IQ:
		m_nROICntMax = ROI_SNR_IQ_Max;
		break;
	case Pic_Tilt:
		m_nROICntMax = ROI_TI_Max;
		break;
	case Pic_FOV:
		m_nROICntMax = ROI_FO_Max;
		break;
	case Pic_Particle:
		m_nROICntMax = ROI_Particle_Max;
		break;
	default:
		break;
	}
#endif

	for (UINT nROI = 0; nROI < m_nROICntMax; nROI++)
	{
		CRect rtROI = GetROIData(nROI);

		int iLeft	= rtROI.left;
		int iRight	= rtROI.right;
		int iTop	= rtROI.top;
		int iBottom = rtROI.bottom;

		double x = m_ptEdit.x;
		double y = m_ptEdit.y;

		if (m_ptEdit.x > iLeft && m_ptEdit.x < iRight && m_ptEdit.y > iTop && m_ptEdit.y < iBottom)
		{
			m_iROInum = nROI;

			int mx = (int)(x * (double)((double)m_nWidth / (double)rt.Width()));
			int my = (int)(y * (double)((double)m_nHeight / (double)rt.Height()));

			if (mx > iLeft + 10 && mx < iRight - 10 && my > iTop + 10 && my < iBottom - 10)
			{
				m_nMouseEdit = MouseEdit_ALL;
				break;
			}
			else if ((mx >= iLeft - 10 && mx <= iLeft + 10 && my >= iTop - 10 && my <= iTop + 10))
			{
				m_nMouseEdit = MouseEdit_Left_NWSE;
				break;
			}
			else if ((mx >= iRight - 10 && mx <= iRight + 10 && my >= iBottom - 10 && my <= iBottom + 10))
			{
				m_nMouseEdit = MouseEdit_Right_NWSE;
				break;
			}
			else if ((mx >= iRight - 10 && mx <= iRight + 10 && my >= iTop - 10 && my <= iTop + 10))
			{
				m_nMouseEdit = MouseEdit_Left_NESW;
				break;
			}
			else if ((mx >= iLeft - 10 && mx <= iLeft + 10 && my >= iBottom - 10 && my <= iBottom + 10))
			{
				m_nMouseEdit = MouseEdit_Right_NESW;
				break;
			}
			else if (mx >= iLeft - 10 && mx <= iLeft + 10 && my >= iTop + 10 && my <= iBottom - 10)
			{
				m_nMouseEdit = MouseEdit_Left_WE;
				break;
			}
			else if (mx >= iRight - 10 && mx <= iRight + 10 && my >= iTop + 10 && my <= iBottom - 10)
			{
				m_nMouseEdit = MouseEdit_Right_WE;
				break;
			}
			else if (my >= iTop - 10 && my <= iTop + 10 && mx >= iLeft + 10 && mx <= iRight - 10)
			{
				m_nMouseEdit = MouseEdit_Top_NS;
				break;
			}
			else if (my >= iBottom - 10 && my <= iBottom + 10 && mx >= iLeft + 10 && mx <= iRight - 10)
			{
				m_nMouseEdit = MouseEdit_Bottom_NS;
				break;
			}
			else
			{
				m_nMouseEdit = MouseEdit_Init;
			}
		}
		else
		{
			m_nMouseEdit = MouseEdit_Init;
		}
	}

	// 마우스 화살표 모양 변경
	switch (m_nMouseEdit)
	{
	case MouseEdit_Init:
		SetCursor(LoadCursor(NULL, IDC_ARROW));
		break;
	case MouseEdit_Left_WE:
	case MouseEdit_Right_WE:
		SetCursor(LoadCursor(NULL, IDC_SIZEWE));
		break;
	case MouseEdit_Top_NS:
	case MouseEdit_Bottom_NS:
		SetCursor(LoadCursor(NULL, IDC_SIZENS));
		break;
	case MouseEdit_Left_NWSE:
	case MouseEdit_Right_NWSE:
		SetCursor(LoadCursor(NULL, IDC_SIZENWSE));
		break;
	case MouseEdit_Left_NESW:
	case MouseEdit_Right_NESW:
		SetCursor(LoadCursor(NULL, IDC_SIZENESW));
		break;
	case MouseEdit_ALL:
		SetCursor(LoadCursor(NULL, IDC_SIZEALL));
		break;
	default:
		break;
	}

	return TRUE;
}

//=============================================================================
// Method		: OnEraseBkgnd
// Access		: public  
// Returns		: BOOL
// Parameter	: CDC * pDC
// Qualifier	:
// Last Update	: 2017/2/27 - 15:02
// Desc.		:
//=============================================================================
BOOL CWnd_ImageView::OnEraseBkgnd(CDC* pDC)
{
	return CWnd::OnEraseBkgnd(pDC);
}

void CWnd_ImageView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (FALSE == bShow)
	{
		m_bflg_Timer = FALSE;
	}
}

//=============================================================================
// Method		: OnUpdataResetImage
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/17 - 20:23
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnUpdataResetImage()
{
#ifdef USE_VisionInspection
	if (m_pLoadImage != NULL)
	{
		cvReleaseImage(&m_pLoadImage);
		m_pLoadImage = NULL;
	}

	if (m_pOriginImage != NULL)
	{
		cvReleaseImage(&m_pOriginImage);
		m_pOriginImage = NULL;
	}

	if (m_pPicImage != NULL)
	{
		cvReleaseImage(&m_pPicImage);
		m_pPicImage = NULL;
	}
#endif

	m_bflg_Timer = FALSE;
}

//=============================================================================
// Method		: OnUpdataLoadImage
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/16 - 15:36
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnUpdataLoadImage()
{
#ifdef USE_VisionInspection
	CString strFileExt = _T("Image (*.PNG) | *.PNG; | All Files(*.*) |*.*||");
	CFileDialog fileDlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, strFileExt);
	fileDlg.m_ofn.lpstrInitialDir = m_strImagePath;

	m_bflg_Timer = FALSE;

	Sleep(100);

	if (IDOK == fileDlg.DoModal())
	{
		CString strFilePath = fileDlg.GetPathName();

		if (m_pLoadImage != NULL)
		{
			cvReleaseImage(&m_pLoadImage);
			m_pLoadImage = NULL;
		}

		if (m_pOriginImage != NULL)
		{
			cvReleaseImage(&m_pOriginImage);
			m_pOriginImage = NULL;
		}

		if (m_pPicImage != NULL)
		{
			cvReleaseImage(&m_pPicImage);
			m_pPicImage = NULL;
		}

		CString strFileFormat;
		if (AfxExtractSubString(strFileFormat, strFilePath, 1, '.') == TRUE)
		{
			if (strFileFormat.MakeLower() == _T("raw"))
			{
				WORD*	pBuf = new WORD[m_nHeight * m_nWidth];

				USES_CONVERSION;
				FILE *infile = fopen(CT2A(strFilePath.GetBuffer(0)), "rb");
			
				if (infile == NULL)
				{
					return;
				}

				fread((char*)pBuf, sizeof(char), m_nHeight * m_nWidth * 2, infile);
				fclose(infile);


				BYTE*	pBufData = new BYTE[m_nHeight * m_nWidth];
				m_pLoadImage = cvCreateImage(cvSize(m_nWidth, m_nHeight), IPL_DEPTH_16U, 1);

				memcpy(m_pLoadImage->imageData, (PWORD)pBuf, m_nHeight * m_nWidth * sizeof(WORD));

				delete[] pBufData;
				delete[] pBuf;
			}
		}
		if (m_pLoadImage == NULL)
		{
			USES_CONVERSION;
			m_pLoadImage = cvLoadImage(CT2A(strFilePath.GetBuffer(0)), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
		}

		m_pOriginImage = cvCreateImage(cvSize(m_pLoadImage->width, m_pLoadImage->height), IPL_DEPTH_8U, 3);
		m_pPicImage = cvCreateImage(cvSize(m_pOriginImage->width, m_pOriginImage->height), IPL_DEPTH_8U, 3);

	}
#endif
	m_bflg_Timer = TRUE;
}

//=============================================================================
// Method		: OnMonitorView
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/16 - 15:48
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnImageView(__in UINT nOverlayIndex)
{
#ifdef USE_VisionInspection
	if (m_pOriginImage != NULL && m_pPicImage != NULL)
	{
		if (m_stImage.m_hWnd != NULL)
		{
			SetImageExposure();

			m_BitmapInfo.bmiHeader.biBitCount	= m_pOriginImage->depth * m_pOriginImage->nChannels;
			m_BitmapInfo.bmiHeader.biWidth		= m_pOriginImage->width;
			m_BitmapInfo.bmiHeader.biHeight		= -1 * m_pOriginImage->height;

			cvCopy(m_pOriginImage, m_pPicImage);


			switch (nOverlayIndex)
			{
			case Pic_Distortion:
				PicDistortion(m_pPicImage);
				break;
			case Pic_Dynamic:
				PicDynamic(m_pPicImage);
				break;
			case Pic_Rotate:
				PicRotate(m_pPicImage);
				break;
			case Pic_SFR:
				PicSFR(m_pPicImage);
				break;
			case Pic_SNR_BW:
				PicSNR_BW(m_pPicImage);
				break;
			case Pic_SNR_IQ:
				PicSNR_IQ(m_pPicImage);
				break;
			case Pic_Tilt:
				PicTilt(m_pPicImage);
				break;
			case Pic_FOV:
				PicFov(m_pPicImage);
				break;
			case Pic_DefectPixel:
				PicDefectPixel(m_pPicImage);
				break;
			case Pic_Particle:
				PicParticle(m_pPicImage);
				break;
			case Pic_CenterPoint:
				PicCenterPoint(m_pPicImage);
				break;
			default:
				break;
			}

			CDC *pcDC = m_stImage.GetDC();
			pcDC->SetStretchBltMode(COLORONCOLOR);

			CRect rt;
			m_stImage.GetClientRect(rt);

			StretchDIBits(pcDC->m_hDC, 0, 0, rt.Width(), rt.Height(), 0, 0, m_pPicImage->width, m_pPicImage->height, m_pPicImage->imageData, &m_BitmapInfo, DIB_RGB_COLORS, SRCCOPY);

			m_stImage.ReleaseDC(pcDC);
		}
	}
	else
	{
		m_stImage.SetText(_T("EMPTY"));
	}

	Sleep(10);
#endif
}

//=============================================================================
// Method		: OnImageExposure
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/19 - 22:16
// Desc.		:
//=============================================================================
void CWnd_ImageView::SetImageExposure()
{
#ifdef USE_VisionInspection
	if (NULL == m_pOriginImage || NULL == m_pLoadImage)
		return;

	DOUBLE delta = pow(2.0, m_pstRecipeInfo->iExposure);
	double dAlpha = (0 != delta) ? (1.0f / delta) : 1.0f;
	double dBeta = 0.0f;

	cv::Mat Bit16Mat(m_pLoadImage->height, m_pLoadImage->width, CV_16UC1, m_pLoadImage->imageData);
	Bit16Mat.convertTo(Bit16Mat, CV_8UC1, dAlpha, 0);

	cv::Mat rgb8BitMat(m_pOriginImage->height, m_pOriginImage->width, CV_8UC3, m_pOriginImage->imageData);
	cv::cvtColor(Bit16Mat, rgb8BitMat, CV_GRAY2BGR);
#endif
}


//=============================================================================
// Method		: SetROIData
// Access		: protected  
// Returns		: void
// Parameter	: UINT nROI
// Parameter	: CRect rtROI
// Qualifier	:
// Last Update	: 2017/10/17 - 19:20
// Desc.		:
//=============================================================================
void CWnd_ImageView::SetROIData(UINT nROI, CRect rtROI)
{
	UINT nIdx = 0;
	UINT nTemp = 0;

#ifdef USE_VisionInspection
	switch (m_pstRecipeInfo->TestItemOpt.iPicItem)
	{
	case Pic_Distortion:
		m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case Pic_Dynamic:
		if (nROI < ROI_DY_Max)
		{
			nTemp = nROI;
			nIdx = IDX_DY_1ST;
		}
		else
		{
			nTemp = nROI - ROI_DY_Max;
			nIdx = IDX_DY_2ND;
		}

		m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nTemp].rtRoi = rtROI;
		break;
	case Pic_Rotate:
		m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case Pic_SFR:
		m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case Pic_SNR_BW:
		if (nROI < ROI_SN_BW_Max)
		{
			nTemp = nROI;
			nIdx = IDX_SN_BW_1ST;
		}
		else
		{
			nTemp = nROI - ROI_SN_BW_Max;
			nIdx = IDX_SN_BW_2ND;
		}

		m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nTemp].rtRoi = rtROI;
		break;
	case Pic_SNR_IQ:
		m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case Pic_Tilt:
		m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case Pic_FOV:
		m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	case Pic_Particle:
		m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nROI].rtRoi = rtROI;
		break;
	default:
		break;
	}
#endif
}

//=============================================================================
// Method		: GetROIData
// Access		: protected  
// Returns		: CRect
// Parameter	: UINT nROI
// Qualifier	:
// Last Update	: 2017/10/17 - 19:03
// Desc.		:
//=============================================================================
CRect CWnd_ImageView::GetROIData(UINT nROI)
{
	CRect rtROI;
	UINT nIdx = 0;
	UINT nTemp = 0;
#ifdef USE_VisionInspection
	switch (m_pstRecipeInfo->TestItemOpt.iPicItem)
	{
	case Pic_Distortion:
		rtROI = m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nROI].rtRoi;
		break;
	case Pic_Dynamic:
		if (nROI < ROI_DY_Max)
		{
			nTemp = nROI;
			nIdx = IDX_DY_1ST;
		}
		else
		{
			nTemp = nROI - ROI_DY_Max;
			nIdx = IDX_DY_2ND;
		}

		rtROI = m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nTemp].rtRoi;
		break;
	case Pic_Rotate:
		rtROI = m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nROI].rtRoi;
		break;
	case Pic_SFR:
		rtROI = m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nROI].rtRoi;
		break;
	case Pic_SNR_BW:
		if (nROI < ROI_SN_BW_Max)
		{
			nTemp = nROI;
			nIdx = IDX_SN_BW_1ST;
		}
		else
		{
			nTemp = nROI - ROI_SN_BW_Max;
			nIdx = IDX_SN_BW_2ND;
		}

		rtROI = m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nTemp].rtRoi;
		break;
	case Pic_SNR_IQ:
		rtROI = m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nROI].rtRoi;
		break;
	case Pic_Tilt:
		rtROI = m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nROI].rtRoi;
		break;
	case Pic_FOV:
		rtROI = m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nROI].rtRoi;
		break;
	case Pic_Particle:
		rtROI = m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nROI].rtRoi;
		break;
	default:
		break;
	}
#endif
	return rtROI;
}
#ifdef USE_VisionInspection
//=============================================================================
// Method		: PicCenterPoint
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/12/8 - 15:05
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicCenterPoint(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;

	iSize = 2;
	OnDisplayPIC(lpImage, PICMode_LINE,
		m_pstRecipeInfo->TestItemOpt.stCenterPoint.stCenterData.nStandardX - 1,
		m_pstRecipeInfo->TestItemOpt.stCenterPoint.stCenterData.nStandardY - 1,
		m_pstRecipeInfo->TestItemOpt.stCenterPoint.stCenterData.nStandardX + 1,
		m_pstRecipeInfo->TestItemOpt.stCenterPoint.stCenterData.nStandardY + 1,
		0, 255, 255, iSize);
}

//=============================================================================
// Method		: PicDistortion
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:45
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicDistortion(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < ROI_DT_Max; nIdx++)
	{
		CString strText;
		strText.Format(_T("%s"), g_szRoiDistortion[nIdx]);

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.top,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize);

		int iTop = m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.top - 7;
		
		if (iTop < 10)
			iTop = m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_TXT,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.left, iTop,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize, fFont, strText);

		iSize = 2;
		OnDisplayPIC(lpImage, PICMode_LINE,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionData.ptCenter[nIdx].x - 1,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionData.ptCenter[nIdx].y - 1,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionData.ptCenter[nIdx].x + 1,
			m_pstRecipeInfo->TestItemOpt.stDistortion.stDistortionData.ptCenter[nIdx].y + 1,
			0, 255, 255, iSize);
	}
}

//=============================================================================
// Method		: PicDynamic
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:45
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicDynamic(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < IDX_DY_Max; nIdx++)
	{
		for (UINT nROI = 0; nROI < ROI_DY_Max; nROI++)
		{
			CString strText;
			strText.Format(_T("%d.%s"), nIdx + 1, g_szRoiDynamic[nROI]);

			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.left,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.top,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.bottom,
				255, 255, 0, iSize);

			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.left,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.top - 7,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stDynamic.stDynamicOpt[nIdx].stRegionOp[nROI].rtRoi.bottom,
				255, 255, 0, iSize, fFont, strText);
		}
	}
}

//=============================================================================
// Method		: PicRotate
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:45
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicRotate(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < ROI_RT_Max; nIdx++)
	{
		CString strText;
		strText.Format(_T("%s"), g_szRoiRotate[nIdx]);

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.top,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize);

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_TXT,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.top - 7,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize, fFont, strText);

		iSize = 2;
		OnDisplayPIC(lpImage, PICMode_LINE,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateData.ptCenter[nIdx].x - 1,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateData.ptCenter[nIdx].y - 1,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateData.ptCenter[nIdx].x + 1,
			m_pstRecipeInfo->TestItemOpt.stRotate.stRotateData.ptCenter[nIdx].y + 1,
			0, 255, 255, iSize);
	}
}

//=============================================================================
// Method		: PicSFR
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:34
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicSFR(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < ROI_SFR_Max; nIdx++)
	{
		CString strText;
		strText.Format(_T("%d"), nIdx);

		if (TRUE == m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].bEnable)
		{
			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.left,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.top,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.bottom,
				255, 255, 0, iSize);
			
			int iTop = m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.top - 7;

			if (iTop < 10)
				iTop = m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.bottom + 15;

			iSize = 2;
			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.CenterPoint().x,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.top - 7,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stSFR.stSFROpt.stRegionOp[nIdx].rtRoi.bottom,
				255, 255, 0, iSize, fFont, strText);
		}
	}
}

//=============================================================================
// Method		: PicSNR_BW
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:45
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicSNR_BW(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < IDX_SN_BW_Max; nIdx++)
	{
		for (UINT nROI = 0; nROI < ROI_SN_BW_Max; nROI++)
		{
			CString strText;
			strText.Format(_T("%d.%s"), nIdx + 1, g_szRoiSNR_BW[nROI]);

			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.left,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.top,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.bottom,
				255, 255, 0, iSize);

			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.left,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.top - 7,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stSNR_BW.stSNR_BWOpt[nIdx].stRegionOp[nROI].rtRoi.bottom,
				255, 255, 0, iSize, fFont, strText);
		}
	}
}

//=============================================================================
// Method		: PicSNR_IQ
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/23 - 15:52
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicSNR_IQ(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < ROI_SNR_IQ_Max; nIdx++)
	{
		CString strText;
		strText.Format(_T("%d"), nIdx);

		if (TRUE == m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].bEnable)
		{
			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.left,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.top,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.bottom,
				255, 255, 0, iSize);

			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_TXT,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.left,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.top - 7,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.right,
				m_pstRecipeInfo->TestItemOpt.stSNR_IQ.stSNR_IQOpt.stRegionOp[nIdx].rtRoi.bottom,
				255, 255, 0, iSize, fFont, strText);
		}
	}
}

//=============================================================================
// Method		: PicTilt
// Access		: public  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/10/15 - 17:45
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicTilt(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < ROI_TI_Max; nIdx++)
	{
		CString strText;
		strText.Format(_T("%s"), g_szRoiTilt[nIdx]);

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.top,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize);

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_TXT,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.top - 7,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stTilt.stTiltOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize, fFont, strText);
	}
}

//=============================================================================
// Method		: PicFov
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/11/5 - 16:11
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicFov(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < ROI_FO_Max; nIdx++)
	{
		CString strText;
		strText.Format(_T("%s"), g_szRoiFOV[nIdx]);

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.top,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize);

		int iTop = m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.top - 7;

		if (iTop < 10)
			iTop = m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_TXT,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.left, iTop,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize, fFont, strText);

		iSize = 2;
		OnDisplayPIC(lpImage, PICMode_LINE,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVData.ptCenter[nIdx].x - 1,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVData.ptCenter[nIdx].y - 1,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVData.ptCenter[nIdx].x + 1,
			m_pstRecipeInfo->TestItemOpt.stFOV.stFOVData.ptCenter[nIdx].y + 1,
			0, 255, 255, iSize);
	}
}

//=============================================================================
// Method		: PicDefectPixel
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/11/5 - 16:11
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicDefectPixel(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	if (0 == m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.nFailCount)
		return;

	int iSize;
	float fFont = 0.5;


	for (UINT nIdx = 0; nIdx < m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.nFailCount; nIdx++)
	{
		CString strText;
		strText.Format(_T("%d"), nIdx);

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_RECTANGLE,
			m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].x - 2,
			m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].y - 2,
			m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].x + 2,
			m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].y + 2,
			255, 255, 0, iSize);

		int iTop = m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].y - 2 - 7;

		if (iTop < 10)
			iTop = m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].y + 2 + 15;

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_TXT,
			m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].x - 2, iTop,
			m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].x + 2,
			m_pstRecipeInfo->TestItemOpt.stDefectPixel.stDefectPixelData.ptCenter[nIdx].y + 2,
			255, 255, 0, iSize, fFont, strText);
	}
}

//=============================================================================
// Method		: PicParticle
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * lpImage
// Qualifier	:
// Last Update	: 2017/11/30 - 11:51
// Desc.		:
//=============================================================================
void CWnd_ImageView::PicParticle(IplImage* lpImage)
{
	if (NULL == m_pstRecipeInfo)
		return;

	if (0 == m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.byFailType)
		return;

	int iSize;
	float fFont = 0.5;

	for (UINT nIdx = 0; nIdx < ROI_Particle_Max; nIdx++)
	{
		CString strText;
		strText.Format(_T("%s"), g_szROI_Particle[nIdx]);

		enPicMode	enMode;

		if (TRUE == m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].bEllipse)
			enMode = PICMode_CIRCLE;
		else
			enMode = PICMode_RECTANGLE;

		iSize = 1;
		OnDisplayPIC(lpImage, enMode,
			m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.left,
			m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.top,
			m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize);

		int iTop = m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.top - 7;

		if (iTop < 10)
			iTop = m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.bottom + 15;

		iSize = 1;
		OnDisplayPIC(lpImage, PICMode_TXT,
			m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.left, iTop,
			m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.right,
			m_pstRecipeInfo->TestItemOpt.stParticle.stParticleOpt.stRegionOp[nIdx].rtRoi.bottom,
			255, 255, 0, iSize, fFont, strText);


		for (UINT nROI = 0; nROI < 60; nROI++)
		{
			if (Particle_Type_Stain == m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.byFailType[nROI]
				|| Particle_Type_Ymean == m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.byFailType[nROI]
				|| Particle_Type_DeadPixel == m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.byFailType[nROI])
		{
			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_RECTANGLE,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].left,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].top,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].right,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].bottom,
				255, 255, 0, iSize);

				int iTop = m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].top - 20;

			if (iTop < 20)
					iTop = m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].bottom + 15;

				switch (m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.byFailType[nROI])
			{
			case Particle_Type_Stain:
				strText = _T("Stain");
				break;

			case Particle_Type_Ymean:
				strText = _T("Ymean");
				break;

			case Particle_Type_DeadPixel:
				strText = _T("DeadPixel");
				break;
			default:
				break;
			}


			iSize = 1;
			OnDisplayPIC(lpImage, PICMode_TXT,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].left, iTop,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].right,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].bottom,
				255, 255, 0, iSize, fFont, strText);

				iTop = m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].top - 4;

			if (iTop < 10)
					iTop = m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].bottom + 31;

				strText.Format(_T("%.2f"), m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.fConcentration[nROI]);

			OnDisplayPIC(lpImage, PICMode_TXT,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].left, iTop,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].right,
					m_pstRecipeInfo->TestItemOpt.stParticle.stParticleData.rtFailRoi[nROI].bottom,
				255, 255, 0, iSize, fFont, strText);
			}
		}
	}
}
#endif
//=============================================================================
// Method		: OnUpdataROI
// Access		: protected  
// Returns		: void
// Parameter	: IplImage * testImage
// Parameter	: enPicMode enMode
// Parameter	: int iLeft
// Parameter	: int iTop
// Parameter	: int iRight
// Parameter	: int iBottom
// Parameter	: int iRed
// Parameter	: int iGreen
// Parameter	: int iBlue
// Parameter	: int iTickness
// Parameter	: float fFontSize
// Parameter	: CString strText
// Qualifier	:
// Last Update	: 2017/10/15 - 17:29
// Desc.		:
//=============================================================================
void CWnd_ImageView::OnDisplayPIC(IplImage* testImage, enPicMode enMode, int iLeft, int iTop, int iRight, int iBottom, int iRed, int iGreen, int iBlue, int iTickness, float fFontSize/* = 1*/, CString strText /*= _T("")*/)
{
	CvPoint nStartPt;
	nStartPt.x = iLeft;
	nStartPt.y = iTop;

	CvPoint nEndPt;
	nEndPt.x = iRight;
	nEndPt.y = iBottom;

	int iPosX = iLeft + ((iRight - iLeft) / 2);
	int iPosY = iTop + ((iBottom - iTop) / 2);

	CvPoint nCenterPt;
	nCenterPt.x = iPosX;
	nCenterPt.y = iPosY;

	switch (enMode)
	{
	case PICMode_LINE:
		cvLine(testImage, nStartPt, nEndPt, CV_RGB(iRed, iGreen, iBlue), iTickness);
		break;
	case PICMode_RECTANGLE:
		cvRectangle(testImage, nStartPt, nEndPt, CV_RGB(iRed, iGreen, iBlue), iTickness);
		break;
	case PICMode_CIRCLE:
		cvCircle(testImage, nCenterPt, abs(iRight - iLeft) / 2, CV_RGB(iRed, iGreen, iBlue), iTickness);
		break;
	case PICMode_TXT:
	{
		CvFont* font = new CvFont;
		cvInitFont(font, CV_FONT_VECTOR0, fFontSize, fFontSize, 0, iTickness);
		cvPutText(testImage, CT2A(strText), nStartPt, font, CV_RGB(iRed, iGreen, iBlue)); // cvPoint 위치에 설정 색상으로 글씨 넣기

		delete font; // 해제
	}
		break;
	default:
		break;
	}
}
