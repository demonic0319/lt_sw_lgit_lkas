﻿// Wnd_TestResult_IR_ImgT.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_IR_ImgT.h"

// CWnd_TestResult_ImgT
typedef enum TestResult_IR_ImgID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_IR_ImgT, CWnd)

CWnd_TestResult_IR_ImgT::CWnd_TestResult_IR_ImgT()
{
	m_InspectionType = Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_IR_ImgT::~CWnd_TestResult_IR_ImgT()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_IR_ImgT, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_ImgT 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_IR_ImgT::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_IR_ImgT_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_IR_ImgT_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_default.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_default.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_default.SetFont_Gdip(L"Arial", 9.0F);
	m_st_default.Create(_T("TEST RESULT"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	UINT	nWndID = IDC_LIST_ITEM;
	//m_tc_Option.Create(CMFCTabCtrl::STYLE_3D_SCROLLED, rectDummy, this, nWndID++, CMFCTabCtrl::LOCATION_BOTTOM);

	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);

	m_wnd_CurrentRst.SetOwner(GetOwner());
	m_wnd_CurrentRst.Create(NULL, _T("Current"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_IICRst.SetOwner(GetOwner());
	m_wnd_IICRst.Create(NULL, _T("Current"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
 
	m_wnd_SFRRst.SetOwner(GetOwner());
	m_wnd_SFRRst.Create(NULL, _T("SFR"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
 
 
	m_wnd_DynamicRst.SetOwner(GetOwner());
	m_wnd_DynamicRst.Create(NULL, _T("Dynamic & BW"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_YmeanRst.SetOwner(GetOwner());
	m_wnd_YmeanRst.Create(NULL, _T("Ymean"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_LCBRst.SetOwner(GetOwner());
	m_wnd_LCBRst.Create(NULL, _T("LCB"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_BlackSpotRst.SetOwner(GetOwner());
	m_wnd_BlackSpotRst.Create(NULL, _T("BlackSpot"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_ShadingRst.SetOwner(GetOwner());
	m_wnd_ShadingRst.Create(NULL, _T("Shading"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	
	m_wnd_RIRst.SetOwner(GetOwner());
	m_wnd_RIRst.Create(NULL, _T("Rllumination"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_VisionRst.SetOwner(GetOwner());
	m_wnd_VisionRst.Create(NULL, _T("Vision"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_DisplaceRst.SetOwner(GetOwner());
	m_wnd_DisplaceRst.Create(NULL, _T("Displace"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
		m_wnd_DistortionRst.SetOwner(GetOwner());
	m_wnd_DistortionRst.Create(NULL, _T("Distortion"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_DefectBlackRst.SetOwner(GetOwner());
	m_wnd_DefectBlackRst.Create(NULL, _T("Defect_Black"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_DefectWhiteRst.SetOwner(GetOwner());
	m_wnd_DefectWhiteRst.Create(NULL, _T("Defect_White"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);

	m_wnd_OpticalCenterRst.SetOwner(GetOwner());
	m_wnd_OpticalCenterRst.Create(NULL, _T("Optical_Cetner"), dwStyle /*| WS_BORDER*/, rectDummy, this, nWndID++);
	int nItemCnt = 0;

	m_cb_TestItem.AddString(_T("Vision"));
	m_cb_TestItem.AddString(_T("Displace"));
	m_cb_TestItem.AddString(_T("Current"));
	m_cb_TestItem.AddString(_T("SFR"));
	m_cb_TestItem.AddString(_T("Shading"));
	m_cb_TestItem.AddString(_T("Ymean"));
	m_cb_TestItem.AddString(_T("LCB"));
	m_cb_TestItem.AddString(_T("BlackSpot"));
	m_cb_TestItem.AddString(_T("Distortion"));
	m_cb_TestItem.AddString(_T("Defect_Black"));
	m_cb_TestItem.AddString(_T("Defect_White"));
	m_cb_TestItem.AddString(_T("Optical_Cetner"));
	m_cb_TestItem.AddString(_T("IIC"));

	m_iSelectTest[TR_IR_ImgT_ECurrent]			= TI_IR_ImgT_Fn_Current;	//들어온 순서대로 ID Input;							
	m_iSelectTest[TR_IR_ImgT_SFR]				= TI_IR_ImgT_Fn_SFR;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_Shading]			= TI_IR_ImgT_Fn_Shading;	//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_Ymean]				= TI_IR_ImgT_Fn_Ymean;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_LCB]				= TI_IR_ImgT_Fn_LCB;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_BlackSpot]			= TI_IR_ImgT_Fn_BlackSpot;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_Vision]			= TI_IR_ImgT_Fn_Vision;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_Displace]			= TI_IR_ImgT_Fn_Displace;	//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_Distortion]		= TI_IR_ImgT_Fn_Distortion;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_Defect_Black]		= TI_IR_ImgT_Fn_DefectBlack;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_Defect_White]		= TI_IR_ImgT_Fn_DefectWhite;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_OpticalCenter]		= TI_IR_ImgT_Fn_OpticalCenter;		//들어온 순서대로 ID Input;
	m_iSelectTest[TR_IR_ImgT_IIC]				= TI_IR_ImgT_Fn_IIC;		//들어온 순서대로 ID Input;

	m_wnd_CurrentRst.ShowWindow(SW_HIDE);
	m_wnd_IICRst.ShowWindow(SW_HIDE);
	m_wnd_SFRRst.ShowWindow(SW_HIDE);
//	m_wnd_DynamicRst.ShowWindow(SW_HIDE);
	m_wnd_YmeanRst.ShowWindow(SW_HIDE);
	m_wnd_LCBRst.ShowWindow(SW_HIDE);
	m_wnd_BlackSpotRst.ShowWindow(SW_HIDE);
	m_wnd_ShadingRst.ShowWindow(SW_HIDE);
//	m_wnd_RIRst.ShowWindow(SW_HIDE);
	m_wnd_VisionRst.ShowWindow(SW_HIDE);
	m_wnd_DisplaceRst.ShowWindow(SW_HIDE);
	m_wnd_DistortionRst.ShowWindow(SW_HIDE);
	m_wnd_DefectBlackRst.ShowWindow(SW_HIDE);
	m_wnd_DefectWhiteRst.ShowWindow(SW_HIDE);
	m_wnd_OpticalCenterRst.ShowWindow(SW_HIDE);
	SelectNum(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 20;
	int iCbHeight = 25;

	m_st_Item[STI_TR_IR_ImgT_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);
	iTop += iStHeight + 2;

	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, 100);
	iTop += iCbHeight + 2;

	m_wnd_IICRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_CurrentRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_SFRRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
//	m_wnd_DynamicRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_YmeanRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_LCBRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_BlackSpotRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_ShadingRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
//	m_wnd_RIRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_VisionRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_DisplaceRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_DistortionRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_DefectBlackRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_DefectWhiteRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_wnd_OpticalCenterRst.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);

}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_IR_ImgT::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetShowWindowResult
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 10:59
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetShowWindowResult(int iItem)
{
// 	switch (m_InspectionType)
// 	{
// 	case Sys_Focusing:
// 		break;
// 	case Sys_2D_Cal:
// 		break;
// 	case Sys_Image_Test:
// 		break;
// 	case Sys_Stereo_Cal:
// 		break;
// 	case Sys_3D_Cal:
// 		break;
// 	default:
// 		break;
// 	}
}

//=============================================================================
// Method		: MoveWindow_Result
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Qualifier	:
// Last Update	: 2017/10/21 - 11:16
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::MoveWindow_Result(int x, int y, int nWidth, int nHeight)
{
	int iHeaderH = 40;
	int iListH = iHeaderH + 3 * 20;
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (SW_HIDE == bShow)
	{
		SetShowWindowResult(-1);
	}
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: SetTestItemResult
// Access		: public  
// Returns		: void
// Parameter	: __in  int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 10:45
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetTestItem(__in int iTestItem)
{
	SetShowWindowResult(iTestItem);
}

//=============================================================================
// Method		: SetUpdateResult
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo_Base * pstRecipeInfo
// Parameter	: __in int iTestItem
// Qualifier	:
// Last Update	: 2017/10/21 - 12:44
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetUpdateResult(__in const ST_RecipeInfo_Base* pstRecipeInfo, __in int iTestItem)
{
	if (pstRecipeInfo == NULL)
		return;
// 
// 	switch (m_InspectionType)
// 	{
// 	case Sys_Focusing:
// 		break;
// 	case Sys_2D_Cal:
// 		break;
// 	case Sys_Image_Test:
// 		break;
// 	case Sys_Stereo_Cal:
// 		break;
// 	case Sys_3D_Cal:
// 		break;
// 	default:
// 		break;
// 	}
}

//=============================================================================
// Method		: SetUpdateClear
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/21 - 14:23
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetUpdateClear()
{
}

//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 11:22
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::AllDataReset()
{
	m_wnd_IICRst.Result_Reset();
	m_wnd_CurrentRst.Result_Reset();
	m_wnd_SFRRst.Result_Reset();
//	m_wnd_DynamicRst.Result_Reset();
	m_wnd_YmeanRst.Result_Reset();
	m_wnd_LCBRst.Result_Reset();
	m_wnd_BlackSpotRst.Result_Reset();
	m_wnd_ShadingRst.Result_Reset();
//	m_wnd_RIRst.Result_Reset();
	m_wnd_VisionRst.Result_Reset();
	m_wnd_DisplaceRst.Result_Reset();
	m_wnd_DistortionRst.Result_Reset();
	m_wnd_DefectBlackRst.Result_Reset();
	m_wnd_DefectWhiteRst.Result_Reset();
	m_wnd_OpticalCenterRst.Result_Reset();


}

//=============================================================================
// Method		: SetUI_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/5/12 - 11:22
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetUI_Reset(int nIdx)
{
	switch (nIdx)
	{
	case TI_IR_ImgT_Fn_Vision:
		m_wnd_VisionRst.Result_Reset();
		break;

	case TI_IR_ImgT_Fn_Displace:
		m_wnd_DisplaceRst.Result_Reset();
		break;

	case TI_IR_ImgT_Fn_Current:
		m_wnd_CurrentRst.Result_Reset();
		break;

	case TI_IR_ImgT_Fn_IIC:
		m_wnd_IICRst.Result_Reset();
		break;

	case TI_IR_ImgT_Fn_SFR:
		m_wnd_SFRRst.Result_Reset();
		break;

	case TI_IR_ImgT_Fn_Ymean:
		m_wnd_YmeanRst.Result_Reset();
		break;
	case TI_IR_ImgT_Fn_LCB:
		m_wnd_LCBRst.Result_Reset();
		break;
	case TI_IR_ImgT_Fn_BlackSpot:
		m_wnd_BlackSpotRst.Result_Reset();
		break;

	case TI_IR_ImgT_Fn_Shading:
		m_wnd_ShadingRst.Result_Reset();
		break;
	case TI_IR_ImgT_Fn_Distortion:
		m_wnd_DistortionRst.Result_Reset();
		break;
	case TI_IR_ImgT_Fn_DefectBlack:
		m_wnd_DefectBlackRst.Result_Reset();
		break;
	case TI_IR_ImgT_Fn_DefectWhite:
		m_wnd_DefectWhiteRst.Result_Reset();
		break;
	case TI_IR_ImgT_Fn_OpticalCenter:
		m_wnd_OpticalCenterRst.Result_Reset();
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: LPVOID pParam
// Qualifier	:
// Last Update	: 2018/5/12 - 11:24
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetUIData(int nIdx, LPVOID pParam)
{
	if (pParam == NULL)
	{
		return;
	}
	for (int t = 0; t < m_cb_TestItem.GetCount(); t++)
	{
		if (m_iSelectTest[t] == nIdx)
		{
			SelectNum(t);
			break;
		}
	}

	ST_IR_Result* pData = NULL;
	switch (nIdx)
	{
	case TI_IR_ImgT_Fn_Vision:
	{
		pData = (ST_IR_Result *)pParam;
		for (int nNum = 0; nNum < enVision_Result_Max; nNum++)
		{
			m_wnd_VisionRst.Result_Display(0, nNum, pData->stVision.bResult[nNum], pData->stVision.dbValue[nNum]);
		}

	}
	break;

	case TI_IR_ImgT_Fn_Displace:
	{
		pData = (ST_IR_Result *)pParam;
		for (int nNum = 0; nNum < enDisplace_Result_Max; nNum++)
		{
			m_wnd_DisplaceRst.Result_Display(0, nNum, pData->stDisplace.bResult[nNum], pData->stDisplace.dbValue[nNum + 4]);
		}

	}
	break;


	case TI_IR_ImgT_Fn_Current:
	{
								  pData = (ST_IR_Result *)pParam;
								 // for (int nNum = 0; nNum < enCurrent_Result_Max; nNum++)
								  m_wnd_CurrentRst.Result_Display(0, enCurrent_Result_CH2, pData->stCurrent.bResult[enCurrent_Result_CH2], pData->stCurrent.dbValue[enCurrent_Result_CH2]);
	}
		break;

	case TI_IR_ImgT_Fn_IIC:
	{
							  pData = (ST_IR_Result *)pParam;
							  for (int nNum = 0; nNum < enIIC_Result_Max; nNum++)
							  {
								  m_wnd_IICRst.Result_Display(0, nNum, pData->stIIC.bResult[nNum], pData->stIIC.dbValue[nNum]);
							  }
	}
		break;

	case TI_IR_ImgT_Fn_SFR:
	{
							  pData = (ST_IR_Result *)pParam;
							  for (int nNum = 0; nNum < enSFR_Result_Max;nNum++)
							  {
								  m_wnd_SFRRst.Result_Display(0, nNum, pData->stSFR.bResult[nNum], pData->stSFR.dbValue[nNum]);
							  }
							  
	}
		break;

	case TI_IR_ImgT_Fn_Ymean:
	{
								pData = (ST_IR_Result *)pParam;
								for (int nNum = 0; nNum < enYmean_Result_Max; nNum++)
								{
									m_wnd_YmeanRst.Result_Display(0, nNum, pData->stYmean.bYmeanResult, pData->stYmean.nDefectCount);
								}
	}
		break;

	case TI_IR_ImgT_Fn_LCB:
		pData = (ST_IR_Result *)pParam;
		for (int nNum = 0; nNum < enLCB_Result_Max; nNum++)
		{
			m_wnd_LCBRst.Result_Display(0, nNum, pData->stLCB.bLCBResult, pData->stLCB.nDefectCount);
		}
		break;

	case TI_IR_ImgT_Fn_BlackSpot:
		pData = (ST_IR_Result *)pParam;
		for (int nNum = 0; nNum < enBlackSpot_Result_Max; nNum++)
		{
			m_wnd_BlackSpotRst.Result_Display(0, nNum, pData->stBlackSpot.bBlackSpotResult, pData->stBlackSpot.nDefectCount);
		}
		break;

	case TI_IR_ImgT_Fn_Distortion:
	{
								pData = (ST_IR_Result *)pParam;
								for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
								{
									m_wnd_DistortionRst.Result_Display(0, nNum, pData->stDistortion.bRotation, pData->stDistortion.dbRotation);
								}
	}
		break;

	case TI_IR_ImgT_Fn_Shading:
	{
								  pData = (ST_IR_Result *)pParam;
								  for (int nNum = 0; nNum < enShading_Result_Max; nNum++)
								  {
									  m_wnd_ShadingRst.Result_Display(0, nNum, pData->stShading.bResult[nNum], pData->stShading.dValue[nNum]);
								  }

	}
		break;

	case TI_IR_ImgT_Fn_DefectBlack:
	{
		pData = (ST_IR_Result *)pParam;
		for (int nNum = 0; nNum < enDefect_Black_Result_Max; nNum++)
		{
			m_wnd_DefectBlackRst.Result_Display(0, nNum, pData->stDefect_Black.bDefect_BlackResult, pData->stDefect_Black.nDefect_BlackCount);
		}
	}
		break;
	case TI_IR_ImgT_Fn_DefectWhite:
	{
								pData = (ST_IR_Result *)pParam;
								for (int nNum = 0; nNum < enDefect_White_Result_Max; nNum++)
								{
									m_wnd_DefectWhiteRst.Result_Display(0, nNum, pData->stDefect_White.bDefect_WhiteResult, pData->stDefect_White.nDefect_WhiteCount);
								}
	}
		break;

	case TI_IR_ImgT_Fn_OpticalCenter:
	{
		pData = (ST_IR_Result *)pParam;
		for (int nNum = 0; nNum < enOpticalCenter_Result_Max; nNum++)
		{
			m_wnd_OpticalCenterRst.Result_Display(0, nNum, pData->stOpticalCenter.bResult[nNum], pData->stOpticalCenter.dValue[nNum]);
		}
	}
	break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 11:25
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetClearTab()
{
}

//=============================================================================
// Method		: SetAddTab
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: int iItemCnt
// Qualifier	:
// Last Update	: 2018/5/12 - 11:25
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::SetAddTab(int nIdx, int iItemCnt)
{
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 11:24
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_ImgT::OnLbnSelChangeTest()
{
	m_wnd_VisionRst.ShowWindow(SW_HIDE);
	m_wnd_DisplaceRst.ShowWindow(SW_HIDE);
	m_wnd_IICRst.ShowWindow(SW_HIDE);
	m_wnd_CurrentRst.ShowWindow(SW_HIDE);
	m_wnd_SFRRst.ShowWindow(SW_HIDE);
//	m_wnd_DynamicRst.ShowWindow(SW_HIDE);
	m_wnd_YmeanRst.ShowWindow(SW_HIDE);
	m_wnd_LCBRst.ShowWindow(SW_HIDE);
	m_wnd_BlackSpotRst.ShowWindow(SW_HIDE);
	m_wnd_ShadingRst.ShowWindow(SW_HIDE);
//	m_wnd_RIRst.ShowWindow(SW_HIDE);
	m_wnd_DistortionRst.ShowWindow(SW_HIDE);
	m_wnd_DefectBlackRst.ShowWindow(SW_HIDE);
	m_wnd_DefectWhiteRst.ShowWindow(SW_HIDE);
	m_wnd_OpticalCenterRst.ShowWindow(SW_HIDE);

 	int nIndex = m_cb_TestItem.GetCurSel();

	switch (m_iSelectTest[nIndex])
	{
	case TI_IR_ImgT_Fn_Current:
	{
		m_wnd_CurrentRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_IIC:
	{
		m_wnd_IICRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_SFR:
	{
		m_wnd_SFRRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_Ymean:
	{
		m_wnd_YmeanRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_LCB:
		m_wnd_LCBRst.ShowWindow(SW_SHOW);
		break;

	case TI_IR_ImgT_Fn_BlackSpot:
		m_wnd_BlackSpotRst.ShowWindow(SW_SHOW);
		break;

	case TI_IR_ImgT_Fn_Distortion:
	{
		m_wnd_DistortionRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_Shading:
	{
		m_wnd_ShadingRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_Vision:
	{
		m_wnd_VisionRst.ShowWindow(SW_SHOW);
	}
	break;

	case TI_IR_ImgT_Fn_Displace:
	{
		m_wnd_DisplaceRst.ShowWindow(SW_SHOW);
	}
	break;

	case TI_IR_ImgT_Fn_DefectBlack:
	{
		m_wnd_DefectBlackRst.ShowWindow(SW_SHOW);
	}
		break;
	case TI_IR_ImgT_Fn_DefectWhite:
	{
		m_wnd_DefectWhiteRst.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_OpticalCenter:
	{
		m_wnd_OpticalCenterRst.ShowWindow(SW_SHOW);
	}
	break;
	default:
		break;
	}
}

void CWnd_TestResult_IR_ImgT::SelectNum(UINT nSelect)
{
	m_cb_TestItem.SetCurSel(nSelect);
	OnLbnSelChangeTest();
}