﻿#include "stdafx.h"
#include "File_Maintenance.h"
#include "Def_Enum.h"
#include "CommonFunction.h"

#define		TEACH_AppName					_T("TEACH_OP")

CFile_Maintenance::CFile_Maintenance()
{
}


CFile_Maintenance::~CFile_Maintenance()
{
}

//=============================================================================
// Method		: LoadMaintenanceFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_MaintenanceInfo & stMaintenanceInfo
// Qualifier	:
// Last Update	: 2017/9/29 - 17:07
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::LoadMaintenanceFile(__in LPCTSTR szPath, __out ST_MaintenanceInfo& stMaintenanceInfo)
{
	if (NULL == szPath)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn  = LoadTeachFile		(szPath, stMaintenanceInfo.stTeachInfo);
	bReturn &= LoadLightFile		(szPath, stMaintenanceInfo.stLightInfo);

	return bReturn;
}

//=============================================================================
// Method		: SaveMaintenanceFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_MaintenanceInfo * pstMaintenanceInfo
// Qualifier	:
// Last Update	: 2017/9/29 - 17:09
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::SaveMaintenanceFile(__in LPCTSTR szPath, __in const ST_MaintenanceInfo* pstMaintenanceInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstMaintenanceInfo)
		return FALSE;

	BOOL bReturn = TRUE;

	bReturn  = SaveTeachFile		(szPath, &pstMaintenanceInfo->stTeachInfo);
	bReturn &= SaveLightFile		(szPath, &pstMaintenanceInfo->stLightInfo);
	
	return bReturn;
}

//=============================================================================
// Method		: LoadLightFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_LightInfo & stLightInfo
// Qualifier	:
// Last Update	: 2017/9/29 - 15:50
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::LoadLightFile(__in LPCTSTR szPath, __out ST_LightInfo& stLightInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR	inBuff[80] = { 0, };

	CString strValue;
	CString strAppName;

	for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("LightPSU_%02d"), nIdx);

		GetPrivateProfileString(strAppName, _T("Voltage"), _T("12.0"), inBuff, 80, szPath);
		stLightInfo.stLightPSU[nIdx].fVolt = (float)_ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Current"), _T("0.0"), inBuff, 80, szPath);
		stLightInfo.stLightPSU[nIdx].fCurrent = (float)_ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Step"), _T("0"), inBuff, 80, szPath);
		stLightInfo.stLightPSU[nIdx].wStep = _ttoi(inBuff);
	}

	for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("LightBrd_%02d"), nIdx);

		GetPrivateProfileString(strAppName, _T("Voltage"), _T("12.0"), inBuff, 80, szPath);
		stLightInfo.stLightBrd[nIdx].fVolt = (float)_ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Current"), _T("0.0"), inBuff, 80, szPath);
		stLightInfo.stLightBrd[nIdx].fCurrent = (float)_ttof(inBuff);

		GetPrivateProfileString(strAppName, _T("Step"), _T("0"), inBuff, 80, szPath);
		stLightInfo.stLightBrd[nIdx].wStep = _ttoi(inBuff);
	}

	return TRUE;
}

//=============================================================================
// Method		: SaveLightFile
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_LightInfo * pstLightInfo
// Qualifier	:
// Last Update	: 2017/9/29 - 15:50
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::SaveLightFile(__in LPCTSTR szPath, __in const ST_LightInfo* pstLightInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstLightInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("LightPSU_%02d"), nIdx);

		strValue.Format(_T("%.2f"), pstLightInfo->stLightPSU[nIdx].fVolt);
		WritePrivateProfileString(strAppName, _T("Voltage"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstLightInfo->stLightPSU[nIdx].fCurrent);
		WritePrivateProfileString(strAppName, _T("Current"), strValue, szPath);

		strValue.Format(_T("%d"), pstLightInfo->stLightPSU[nIdx].wStep);
		WritePrivateProfileString(strAppName, _T("Step"), strValue, szPath);
	}

	for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
	{
		strAppName.Format(_T("LightBrd_%02d"), nIdx);

		strValue.Format(_T("%.2f"), pstLightInfo->stLightBrd[nIdx].fVolt);
		WritePrivateProfileString(strAppName, _T("Voltage"), strValue, szPath);

		strValue.Format(_T("%.2f"), pstLightInfo->stLightBrd[nIdx].fCurrent);
		WritePrivateProfileString(strAppName, _T("Current"), strValue, szPath);

		strValue.Format(_T("%d"), pstLightInfo->stLightBrd[nIdx].wStep);
		WritePrivateProfileString(strAppName, _T("Step"), strValue, szPath);
	}

	return TRUE;
}

//=============================================================================
// Method		: LoaPowerSupplyFile
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_PowerInfo & stPowerInfo
// Qualifier	:
// Last Update	: 2017/11/12 - 21:43
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::LoadPowerSupplyFile(__in LPCTSTR szPath, __out ST_PowerInfo& stPowerInfo)
{
	if (NULL == szPath)
		return FALSE;

	TCHAR	inBuff[80] = { 0, };

	CString strValue;
	CString strAppName;

	GetPrivateProfileString(_T("POWERSUPPLY_OP"), _T("VOLTAGE"), _T("12.0"), inBuff, 80, szPath);
	stPowerInfo.fVoltage = (float)_ttof(inBuff);

	GetPrivateProfileString(_T("POWERSUPPLY_OP"), _T("CURRENT"), _T("10.0"), inBuff, 80, szPath);
	stPowerInfo.fCurrent = (float)_ttof(inBuff);

	return TRUE;
}

//=============================================================================
// Method		: SavPowerSupplyFile
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_PowerInfo * pstPowerInfo
// Qualifier	:
// Last Update	: 2017/11/12 - 21:43
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::SavePowerSupplyFile(__in LPCTSTR szPath, __in const ST_PowerInfo* pstPowerInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstPowerInfo)
		return FALSE;

	CString strValue;

	strValue.Format(_T("%.2f"), pstPowerInfo->fVoltage);
	WritePrivateProfileString(_T("POWERSUPPLY_OP"), _T("VOLTAGE"), strValue, szPath);

	strValue.Format(_T("%.2f"), pstPowerInfo->fCurrent);
	WritePrivateProfileString(_T("POWERSUPPLY_OP"), _T("CURRENT"), strValue, szPath);

	return TRUE;
}

//=============================================================================
// Method		: Load_Teach_Info
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __out ST_TeachInfo & stTeachInfo
// Qualifier	:
// Last Update	: 2018/3/13 - 10:48
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::LoadTeachFile(__in LPCTSTR szPath, __out ST_TeachInfo& stTeachInfo)
{
	TCHAR   inBuff[255] = { 0, };
	CString strValue, strApp;

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
	{
		strApp.Format(_T("Teach_%d"), nIdx);
		GetPrivateProfileString(TEACH_AppName, strApp, strValue, inBuff, 80, szPath);
		stTeachInfo.dbTeachData[nIdx] = _ttof(inBuff);
	}
	return TRUE;
}

//=============================================================================
// Method		: Save_Teach_Info
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in const ST_TeachInfo * pstTeachInfo
// Qualifier	:
// Last Update	: 2018/3/13 - 10:48
// Desc.		:
//=============================================================================
BOOL CFile_Maintenance::SaveTeachFile(__in LPCTSTR szPath, __in const ST_TeachInfo* pstTeachInfo)
{
	if (NULL == szPath)
		return FALSE;

	if (NULL == pstTeachInfo)
		return FALSE;

	CString strValue;
	CString strAppName;

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
	{
		strValue.Format(_T("%6.2f"), pstTeachInfo->dbTeachData[nIdx]);
		strAppName.Format(_T("Teach_%d"), nIdx);
		WritePrivateProfileString(TEACH_AppName, strAppName, strValue, szPath);
	}
	return TRUE;
}
