﻿#ifndef TestManager_TestMES_LKAS_h__
#define TestManager_TestMES_LKAS_h__

#pragma once

#include <windows.h>

#include "Def_DataStruct.h"
#include "File_Mes.h"
#include "Def_Mes_IR_ImgT.h"
#include "Def_Mes_Foc.h"
#include "Def_Mes_EachTest.h"


class CTestManager_TestMES_LKAS 
{
public:
	CTestManager_TestMES_LKAS();
	virtual ~CTestManager_TestMES_LKAS();

	// 모든 정보
	ST_InspectionInfo *m_pInspInfo;

	void	SetPtr_InspInfo(ST_InspectionInfo* pstInfo)
	{
		if (pstInfo == NULL)
			return;

		m_pInspInfo = pstInfo;
	};


	// 전류
	void SaveData_Current			(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_IIC				(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_SFR				(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_Distortion		(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_DefectBlock		(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_DefectWhite		(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_Ymean				(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_LCB				(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_BlackSpot			(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_Shading			(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);
	void SaveData_OpticalCenter		(__in int iParaIdx, __in ST_MES_Data_LKAS *pMesData);


	// 개별 저장 
	void Current_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void SFR_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DynamicRange_Each_DataSave	(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Ymean_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void LCB_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void BlackSpot_Each_DataSave	(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void RI_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Distortion_Each_DataSave	(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DefectBlack_Each_DataSave	(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DefectWhite_Each_DataSave	(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Shading_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	
	void OpicalCenter_Each_DataSave	(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Vision_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Displace_Each_DataSave		(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void IIC_Each_DataSave			(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
};

#endif // TestManager_TestMES_h__
