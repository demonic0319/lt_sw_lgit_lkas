﻿//*****************************************************************************
// Filename	: 	Wnd_TeachOp.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "MotionManager.h"
#include "Def_Motion.h"

#include "File_Maintenance.h"

enum enTeachnOpButton
{
	BN_TC_SAVE = 0,
	BN_TC_MAXNUM,
};

static LPCTSTR g_szTeachOpButton[] =
{
	_T("SAVE"),
	NULL
};

// CWnd_TeachOp
class CWnd_TeachOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_TeachOp)

public:
	CWnd_TeachOp();
	virtual ~CWnd_TeachOp();

	CMotionManager*	m_pstDevice;

	void SetPtr_MaintenanceInfo(__in ST_MaintenanceInfo* pstMaintInfo)
	{
		if (pstMaintInfo == NULL)
			return;

		m_pstMaintenanceInfo = pstMaintInfo;
	}

	void GetPtr_MaintenanceInfo(__out ST_MaintenanceInfo& stMaintInfo)
	{
		stMaintInfo = *m_pstMaintenanceInfo;
	}

	void SetPtr_Device(__in CMotionManager* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	enInsptrSysType m_nSysType;
	void SetSystemType(__in enInsptrSysType nSysType)
	{
		m_nSysType = nSysType;
	
		SetUpdateItem();
	};

	void	SetUpdateData	();

protected:

	CVGStatic				m_st_Load;
	CVGStatic				m_st_Vision;
	CVGStatic				m_st_DisplaceA;
	CVGStatic				m_st_DisplaceB;
	CVGStatic				m_st_DisplaceC;
	CVGStatic				m_st_DisplaceD;
	CVGStatic				m_st_Chart;
	CVGStatic				m_st_Blemish;

	UINT					m_nItemCnt;
	CFont					m_font;
	CVGStatic				m_st_Item[TC_ALL_MAX];
	CMFCMaskedEdit			m_ed_Item[TC_ALL_MAX];
	CMFCButton				m_bn_Item[BN_TC_MAXNUM];
	CString					m_szTeacOpName[TC_ALL_MAX];

	ST_MaintenanceInfo*		m_pstMaintenanceInfo;

	CFile_Maintenance		m_fileMaintenance;

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnSave	();
	virtual BOOL	PreTranslateMessage	(MSG* pMsg);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);
	
	void	SetUpdateItem	();
	void	GetUpdateData	();
	
	DECLARE_MESSAGE_MAP()
};