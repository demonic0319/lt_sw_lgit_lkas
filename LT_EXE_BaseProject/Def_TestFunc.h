#ifndef Def_TestFunc_h__
#define Def_TestFunc_h__

typedef enum enParaManual
{
	ManuP_Pallet_In = 0,
	ManuP_Dock_Act,
	ManuP_Dock_Mov,
	ManuP_Insp_Act,
	ManuP_Insp_Mov,
	ManuP_UnLoad_Act,
	ManuP_UnLoad_Mov,
	ManuP_Pallet_Out,
	ManuP_Max_Num,
};

static LPCTSTR g_szManuParaName[] =
{
	_T("Product In"),
	_T("Dock Act"),
	_T("Dock Pos"),
	_T("Insp Act"),
	_T("Insp Mov"),
	_T("UnLoad Act"),
	_T("UnLoad Pos"),
	_T("Product Out"),
	NULL
};

#endif // Def_TestFunc_h__
