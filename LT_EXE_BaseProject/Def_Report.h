//*****************************************************************************
// Filename	: 	Def_Report.h
// Created	:	2017/11/16 - 22:30
// Modified	:	2017/11/16 - 22:30
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_Report_h__
#define Def_Report_h__

#include <afxwin.h>
#include "Def_MES_Cm.h"

#pragma pack(push, 1)

//---------------------------------------------------------
// 검사 결과 저장용 구조체
//---------------------------------------------------------
// 검사 항목
typedef enum enWorklist
{
	WL_Item_01,
	WL_Item_02,
	WL_Item_03,
	WL_Item_04,
	WL_MaxEnum,
};

static LPCTSTR g_szWorklist[] =
{
	_T("Item 01"),
	_T("Item 02"),
	_T("Item 03"),
	_T("Item 04"),
};

typedef struct _tag_Worklist : public ST_MES_FinalResult
{
	// 검사 항목
	CString szItem[WL_MaxEnum];

	_tag_Worklist()
	{
		Itemz;

		for (UINT nIdx = 0; nIdx < WL_MaxEnum; nIdx++)
		{
			ItemHeaderz.Add(g_szWorklist[nIdx]);
		}
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < WL_MaxEnum; nIdx++)
		{
			szItem[nIdx].Empty();
		}
	};

	void MakeItemz()
	{
		Itemz.RemoveAll();

		for (UINT nIdx = 0; nIdx < WL_MaxEnum; nIdx++)
		{
			Itemz.Add(szItem[nIdx]);
		}

	};

	_tag_Worklist& operator= (_tag_Worklist& ref)
	{
		//__super::operator=(ref);

		for (UINT nIdx = 0; nIdx < WL_MaxEnum; nIdx++)
		{
			szItem[nIdx] = ref.szItem[nIdx];
		}

		return *this;
	};

}ST_Worklist, *PST_Worklist;

//-----------------------------------------------------------------------------
// Barcode 결과 파일
//-----------------------------------------------------------------------------
typedef enum enWorkList_Barcode
{
	WL_WIP_WIP_ID,
	WL_WIP_PalletID,
	WL_WIP_SleepCurr,
	WL_WIP_OperationCurr,
	WL_WIP_MaxEnum,
};

static LPCTSTR g_szWorklistWIPID[] =
{
	_T("Barcode"),
	_T("Pallet ID"),
	_T("Sleep Current"),
	_T("Operation Current"),
	NULL
};

typedef struct _tag_WorkList_Barcode : public ST_MES_FinalResult
{
	// 검사 항목
	CString szItem[WL_WIP_MaxEnum];

	_tag_WorkList_Barcode()
	{
		Itemz;

		for (UINT nIdx = 0; nIdx < WL_WIP_MaxEnum; nIdx++)
		{
			ItemHeaderz.Add(g_szWorklist[nIdx]);
		}
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < WL_WIP_MaxEnum; nIdx++)
		{
			szItem[nIdx].Empty();
		}
	};

	void MakeItemz()
	{
		Itemz.RemoveAll();

		for (UINT nIdx = 0; nIdx < WL_WIP_MaxEnum; nIdx++)
		{
			Itemz.Add(szItem[nIdx]);
		}

	};

	_tag_WorkList_Barcode& operator= (_tag_WorkList_Barcode& ref)
	{
		//__super::operator=(ref);

		for (UINT nIdx = 0; nIdx < WL_WIP_MaxEnum; nIdx++)
		{
			szItem[nIdx] = ref.szItem[nIdx];
		}

		return *this;
	};

}ST_WorkList_Barcode, *PST_WorkList_Barcode;

//-----------------------------------------------------------------------------
// 2D CAL 결과 파일
//-----------------------------------------------------------------------------
typedef enum enWorklist_2DCAL
{
	WL_2DCAL_KK_00,
	WL_2DCAL_KK_01,
	WL_2DCAL_KK_02,
	WL_2DCAL_KK_03,
	WL_2DCAL_Kc_00,
	WL_2DCAL_Kc_01,
	WL_2DCAL_Kc_02,
	WL_2DCAL_Kc_03,
	WL_2DCAL_Kc_04,
	WL_2DCAL_DistFOV,
	WL_2DCAL_R2Max,
	WL_2DCAL_RepError,
	WL_2DCAL_EvalError,
	WL_2DCAL_OrgOffset,
	WL_2DCAL_DetectionFailureCnt,
	WL_2DCAL_InvalidPointCnt,
	WL_2DCAL_ValidPointCnt,
	WL_2DCAL_Date,

	WL_2DCAL_MaxEnum,
};

static LPCTSTR g_szWorklist2DCAL[] =
{
	_T("KK_00"),
	_T("KK_01"),
	_T("KK_02"),
	_T("KK_03"),
	_T("Kc_00"),
	_T("Kc_01"),
	_T("Kc_02"),
	_T("Kc_03"),
	_T("Kc_04"),
	_T("Dist FOV"),
	_T("R2 Max"),
	_T("Rep Error"),
	_T("Eval Error"),
	_T("Org Offset"),
	_T("Detection Failure Cnt"),
	_T("Invalid Point Cnt"),
	_T("Valid Point Cnt"),
	_T("Date"),
	
	NULL
};

typedef struct _tag_Worklist_2DCAL : public ST_MES_FinalResult
{
	// 검사 항목
	CString szItem[WL_2DCAL_MaxEnum];

	_tag_Worklist_2DCAL()
	{
		Itemz;

		for (UINT nIdx = 0; nIdx < WL_2DCAL_MaxEnum; nIdx++)
		{
			ItemHeaderz.Add(g_szWorklist[nIdx]);
		}
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < WL_2DCAL_MaxEnum; nIdx++)
		{
			szItem[nIdx].Empty();
		}
	};

	void MakeItemz()
	{
		Itemz.RemoveAll();

		for (UINT nIdx = 0; nIdx < WL_2DCAL_MaxEnum; nIdx++)
		{
			Itemz.Add(szItem[nIdx]);
		}

	};

	_tag_Worklist_2DCAL& operator= (_tag_Worklist_2DCAL& ref)
	{
		//__super::operator=(ref);

		for (UINT nIdx = 0; nIdx < WL_2DCAL_MaxEnum; nIdx++)
		{
			szItem[nIdx] = ref.szItem[nIdx];
		}

		return *this;
	};

}ST_Worklist_2DCAL, *PST_Worklist_2DCAL;

//-----------------------------------------------------------------------------
// 3D CAL 결과 파일
//-----------------------------------------------------------------------------
typedef enum enWorklist_3DCAL
{
	WL_3DCAL_De_DistCoef_00,		// 6
	WL_3DCAL_De_DistCoef_01,		// 6
	WL_3DCAL_De_DistCoef_02,		// 6
	WL_3DCAL_De_DistCoef_03,		// 6
	WL_3DCAL_De_DistCoef_04,		// 6
	WL_3DCAL_De_DistCoef_05,		// 6
	WL_3DCAL_De_TotalFittingError,
	WL_3DCAL_De_AccracyMap_00,		// 30
	WL_3DCAL_De_AccracyMap_01,		// 30
	WL_3DCAL_De_AccracyMap_02,		// 30
	WL_3DCAL_De_AccracyMap_03,		// 30
	WL_3DCAL_De_AccracyMap_04,		// 30
	WL_3DCAL_De_AccracyMap_05,		// 30
	WL_3DCAL_De_AccracyMap_06,		// 30
	WL_3DCAL_De_AccracyMap_07,		// 30
	WL_3DCAL_De_AccracyMap_08,		// 30
	WL_3DCAL_De_AccracyMap_09,		// 30
	WL_3DCAL_De_AccracyMap_10,		// 30
	WL_3DCAL_De_AccracyMap_11,		// 30
	WL_3DCAL_De_AccracyMap_12,		// 30
	WL_3DCAL_De_AccracyMap_13,		// 30
	WL_3DCAL_De_AccracyMap_14,		// 30
	WL_3DCAL_De_AccracyMap_15,		// 30
	WL_3DCAL_De_AccracyMap_16,		// 30
	WL_3DCAL_De_AccracyMap_17,		// 30
	WL_3DCAL_De_AccracyMap_18,		// 30
	WL_3DCAL_De_AccracyMap_19,		// 30
	WL_3DCAL_De_AccracyMap_20,		// 30
	WL_3DCAL_De_AccracyMap_21,		// 30
	WL_3DCAL_De_AccracyMap_22,		// 30
	WL_3DCAL_De_AccracyMap_23,		// 30
	WL_3DCAL_De_AccracyMap_24,		// 30
	WL_3DCAL_De_AccracyMap_25,		// 30
	WL_3DCAL_De_AccracyMap_26,		// 30
	WL_3DCAL_De_AccracyMap_27,		// 30
	WL_3DCAL_De_AccracyMap_28,		// 30
	WL_3DCAL_De_AccracyMap_29,		// 30
	WL_3DCAL_De_InvalidRatio,
	WL_3DCAL_De_FailureCnt,
	WL_3DCAL_Ev_Chess_00,			// 3
	WL_3DCAL_Ev_Chess_01,			// 3
	WL_3DCAL_Ev_Chess_02,			// 3
	WL_3DCAL_Ev_TofDist_00,			// 3
	WL_3DCAL_Ev_TofDist_01,			// 3
	WL_3DCAL_Ev_TofDist_02,			// 3
	WL_3DCAL_Ev_Error_00,			// 18
	WL_3DCAL_Ev_Error_01,			// 18
	WL_3DCAL_Ev_Error_02,			// 18
	WL_3DCAL_Ev_Error_03,			// 18
	WL_3DCAL_Ev_Error_04,			// 18
	WL_3DCAL_Ev_Error_05,			// 18
	WL_3DCAL_Ev_Error_06,			// 18
	WL_3DCAL_Ev_Error_07,			// 18
	WL_3DCAL_Ev_Error_08,			// 18
	WL_3DCAL_Ev_Error_09,			// 18
	WL_3DCAL_Ev_Error_10,			// 18
	WL_3DCAL_Ev_Error_11,			// 18
	WL_3DCAL_Ev_Error_12,			// 18
	WL_3DCAL_Ev_Error_13,			// 18
	WL_3DCAL_Ev_Error_14,			// 18
	WL_3DCAL_Ev_Error_15,			// 18
	WL_3DCAL_Ev_Error_16,			// 18
	WL_3DCAL_Ev_Error_17,			// 18
	WL_3DCAL_Ev_ShadingH_00,		// 10
	WL_3DCAL_Ev_ShadingH_01,		// 10
	WL_3DCAL_Ev_ShadingH_02,		// 10
	WL_3DCAL_Ev_ShadingH_03,		// 10
	WL_3DCAL_Ev_ShadingH_04,		// 10
	WL_3DCAL_Ev_ShadingH_05,		// 10
	WL_3DCAL_Ev_ShadingH_06,		// 10
	WL_3DCAL_Ev_ShadingH_07,		// 10
	WL_3DCAL_Ev_ShadingH_08,		// 10
	WL_3DCAL_Ev_ShadingH_09,		// 10
	WL_3DCAL_Ev_ShadingV_00,		// 6
	WL_3DCAL_Ev_ShadingV_01,		// 6
	WL_3DCAL_Ev_ShadingV_02,		// 6
	WL_3DCAL_Ev_ShadingV_03,		// 6
	WL_3DCAL_Ev_ShadingV_04,		// 6
	WL_3DCAL_Ev_ShadingV_05,		// 6
	WL_3DCAL_Ev_ShadingD0_00,		// 10
	WL_3DCAL_Ev_ShadingD0_01,		// 10
	WL_3DCAL_Ev_ShadingD0_02,		// 10
	WL_3DCAL_Ev_ShadingD0_03,		// 10
	WL_3DCAL_Ev_ShadingD0_04,		// 10
	WL_3DCAL_Ev_ShadingD0_05,		// 10
	WL_3DCAL_Ev_ShadingD0_06,		// 10
	WL_3DCAL_Ev_ShadingD0_07,		// 10
	WL_3DCAL_Ev_ShadingD0_08,		// 10
	WL_3DCAL_Ev_ShadingD0_09,		// 10
	WL_3DCAL_Ev_ShadingD1_00,		// 10
	WL_3DCAL_Ev_ShadingD1_01,		// 10
	WL_3DCAL_Ev_ShadingD1_02,		// 10
	WL_3DCAL_Ev_ShadingD1_03,		// 10
	WL_3DCAL_Ev_ShadingD1_04,		// 10
	WL_3DCAL_Ev_ShadingD1_05,		// 10
	WL_3DCAL_Ev_ShadingD1_06,		// 10
	WL_3DCAL_Ev_ShadingD1_07,		// 10
	WL_3DCAL_Ev_ShadingD1_08,		// 10
	WL_3DCAL_Ev_ShadingD1_09,		// 10
	WL_3DCAL_Ev_FailureCnt,
	WL_3DCAL_Ev_IntensityC,
	WL_3DCAL_MaxEnum,
};

static LPCTSTR g_szWorklist3DCAL[WL_3DCAL_MaxEnum] =
{
	_T("Dist Coef_00"),
	_T("Dist Coef_01"),
	_T("Dist Coef_02"),
	_T("Dist Coef_03"),
	_T("Dist Coef_04"),
	_T("Dist Coef_05"),
	_T("Total Fitting Error"),
	_T("Accracy Map_00"),
	_T("Accracy Map_01"),
	_T("Accracy Map_02"),
	_T("Accracy Map_03"),
	_T("Accracy Map_04"),
	_T("Accracy Map_05"),
	_T("Accracy Map_06"),
	_T("Accracy Map_07"),
	_T("Accracy Map_08"),
	_T("Accracy Map_09"),
	_T("Accracy Map_10"),
	_T("Accracy Map_11"),
	_T("Accracy Map_12"),
	_T("Accracy Map_13"),
	_T("Accracy Map_14"),
	_T("Accracy Map_15"),
	_T("Accracy Map_16"),
	_T("Accracy Map_17"),
	_T("Accracy Map_18"),
	_T("Accracy Map_19"),
	_T("Accracy Map_20"),
	_T("Accracy Map_21"),
	_T("Accracy Map_22"),
	_T("Accracy Map_23"),
	_T("Accracy Map_24"),
	_T("Accracy Map_25"),
	_T("Accracy Map_26"),
	_T("Accracy Map_27"),
	_T("Accracy Map_28"),
	_T("Accracy Map_29"),
	_T("Invalid Ratio"),
	_T("Failure Cnt"),
	_T("Chess_00"),
	_T("Chess_01"),
	_T("Chess_02"),
	_T("T of Dist_00"),
	_T("T of Dist_01"),
	_T("T of Dist_02"),
	_T("Error_00"),
	_T("Error_01"),
	_T("Error_02"),
	_T("Error_03"),
	_T("Error_04"),
	_T("Error_05"),
	_T("Error_06"),
	_T("Error_07"),
	_T("Error_08"),
	_T("Error_09"),
	_T("Error_10"),
	_T("Error_11"),
	_T("Error_12"),
	_T("Error_13"),
	_T("Error_14"),
	_T("Error_15"),
	_T("Error_16"),
	_T("Error_17"),
	_T("Shading H_00"),
	_T("Shading H_01"),
	_T("Shading H_02"),
	_T("Shading H_03"),
	_T("Shading H_04"),
	_T("Shading H_05"),
	_T("Shading H_06"),
	_T("Shading H_07"),
	_T("Shading H_08"),
	_T("Shading H_09"),
	_T("Shading V_00"),
	_T("Shading V_01"),
	_T("Shading V_02"),
	_T("Shading V_03"),
	_T("Shading V_04"),
	_T("Shading V_05"),
	_T("Shading D0_00"),
	_T("Shading D0_01"),
	_T("Shading D0_02"),
	_T("Shading D0_03"),
	_T("Shading D0_04"),
	_T("Shading D0_05"),
	_T("Shading D0_06"),
	_T("Shading D0_07"),
	_T("Shading D0_08"),
	_T("Shading D0_09"),
	_T("Shading D1_00"),
	_T("Shading D1_01"),
	_T("Shading D1_02"),
	_T("Shading D1_03"),
	_T("Shading D1_04"),
	_T("Shading D1_05"),
	_T("Shading D1_06"),
	_T("Shading D1_07"),
	_T("Shading D1_08"),
	_T("Shading D1_09"),
	_T("Failure Cnt"),
	_T("Intensity C"),
	//NULL
};

typedef struct _tag_Worklist_3DCAL : public ST_MES_FinalResult
{
	// 검사 항목
	CString szItem[WL_3DCAL_MaxEnum];

	_tag_Worklist_3DCAL()
	{
		Itemz;

		for (UINT nIdx = 0; nIdx < WL_3DCAL_MaxEnum; nIdx++)
		{
			ItemHeaderz.Add(g_szWorklist[nIdx]);
		}
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < WL_3DCAL_MaxEnum; nIdx++)
		{
			szItem[nIdx].Empty();
		}
	};

	void MakeItemz()
	{
		Itemz.RemoveAll();

		for (UINT nIdx = 0; nIdx < WL_3DCAL_MaxEnum; nIdx++)
		{
			Itemz.Add(szItem[nIdx]);
		}

	};

	_tag_Worklist_3DCAL& operator= (_tag_Worklist_3DCAL& ref)
	{
		//__super::operator=(ref);

		for (UINT nIdx = 0; nIdx < WL_3DCAL_MaxEnum; nIdx++)
		{
			szItem[nIdx] = ref.szItem[nIdx];
		}

		return *this;
	};

}ST_Worklist_3DCAL, *PST_Worklist_3DCAL;


//-----------------------------------------------------------------------------
// Image Quality 결과 파일
//-----------------------------------------------------------------------------
typedef enum enWorklist_IQ
{
	WL_IQ_Item_01,
	WL_IQ_Item_02,
	WL_IQ_Item_03,
	WL_IQ_Item_04,
	WL_IQ_MaxEnum,
};

static LPCTSTR g_szWorklistIQ[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef struct _tag_Worklist_IQ : public ST_MES_FinalResult
{
	// 검사 항목
	CString szItem[WL_IQ_MaxEnum];

	_tag_Worklist_IQ()
	{
		Itemz;

		for (UINT nIdx = 0; nIdx < WL_IQ_MaxEnum; nIdx++)
		{
			ItemHeaderz.Add(g_szWorklist[nIdx]);
		}
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < WL_IQ_MaxEnum; nIdx++)
		{
			szItem[nIdx].Empty();
		}
	};

	void MakeItemz()
	{
		Itemz.RemoveAll();

		for (UINT nIdx = 0; nIdx < WL_IQ_MaxEnum; nIdx++)
		{
			Itemz.Add(szItem[nIdx]);
		}

	};

	_tag_Worklist_IQ& operator= (_tag_Worklist_IQ& ref)
	{
		//__super::operator=(ref);

		for (UINT nIdx = 0; nIdx < WL_IQ_MaxEnum; nIdx++)
		{
			szItem[nIdx] = ref.szItem[nIdx];
		}

		return *this;
	};

}ST_Worklist_IQ, *PST_Worklist_IQ;

//-----------------------------------------------------------------------------
// Paticle 결과 파일
//-----------------------------------------------------------------------------
typedef enum enWorklist_Pat
{
	WL_Pat_Item_01,
	WL_Pat_Item_02,
	WL_Pat_Item_03,
	WL_Pat_Item_04,
	WL_Pat_MaxEnum,
};

static LPCTSTR g_szWorklistPat[] =
{
	_T(""),
	_T(""),
	_T(""),
	_T(""),
	NULL
};

typedef struct _tag_Worklist_Pat : public ST_MES_FinalResult
{
	// 검사 항목
	CString szItem[WL_Pat_MaxEnum];

	_tag_Worklist_Pat()
	{
		Itemz;

		for (UINT nIdx = 0; nIdx < WL_Pat_MaxEnum; nIdx++)
		{
			ItemHeaderz.Add(g_szWorklist[nIdx]);
		}
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < WL_Pat_MaxEnum; nIdx++)
		{
			szItem[nIdx].Empty();
		}
	};

	void MakeItemz()
	{
		Itemz.RemoveAll();

		for (UINT nIdx = 0; nIdx < WL_Pat_MaxEnum; nIdx++)
		{
			Itemz.Add(szItem[nIdx]);
		}

	};

	_tag_Worklist_Pat& operator= (_tag_Worklist_Pat& ref)
	{
		//__super::operator=(ref);

		for (UINT nIdx = 0; nIdx < WL_Pat_MaxEnum; nIdx++)
		{
			szItem[nIdx] = ref.szItem[nIdx];
		}

		return *this;
	};

}ST_Worklist_Pat, *PST_Worklist_Pat;


//-----------------------------------------------------------------------------
// OQA 결과 파일
//-----------------------------------------------------------------------------
typedef enum enWorklist_OQA
{
	WL_OQA_WIP_ID_Verify,
	WL_OQA_SW_Version,
	WL_OQA_DTC_Check,
	WL_OQA_DummyHand,	
	WL_OQA_MaxEnum,
};

static LPCTSTR g_szWorklistOQA[] =
{
	_T("Barcode"),
	_T("SW Version"),
	_T("DTC Check"),
	_T("Dummy Hand Test"),
	NULL
};

typedef struct _tag_Worklist_OQA : public ST_MES_FinalResult
{
	// 검사 항목
	CString szItem[WL_OQA_MaxEnum];

	_tag_Worklist_OQA()
	{
		Itemz;

		for (UINT nIdx = 0; nIdx < WL_OQA_MaxEnum; nIdx++)
		{
			ItemHeaderz.Add(g_szWorklist[nIdx]);
		}
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < WL_OQA_MaxEnum; nIdx++)
		{
			szItem[nIdx].Empty();
		}
	};

	void MakeItemz()
	{
		Itemz.RemoveAll();

		for (UINT nIdx = 0; nIdx < WL_OQA_MaxEnum; nIdx++)
		{
			Itemz.Add(szItem[nIdx]);
		}

	};

	_tag_Worklist_OQA& operator= (_tag_Worklist_OQA& ref)
	{
		//__super::operator=(ref);

		for (UINT nIdx = 0; nIdx < WL_OQA_MaxEnum; nIdx++)
		{
			szItem[nIdx] = ref.szItem[nIdx];
		}

		return *this;
	};

}ST_Worklist_OQA, *PST_Worklist_OQA;



#pragma pack (pop)

#endif // Def_Report_h__
