﻿//*****************************************************************************
// Filename	: 	Wnd_Cfg_Model.cpp
// Created	:	2016/3/14 - 10:57
// Modified	:	2016/3/14 - 10:57
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_Model.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_Model.h"
#include "resource.h"
#include "Def_WindowMessage.h"
#include "File_RomBinary.h"

#define IDC_ED_MODELCODE			1001
#define IDC_CB_MODELTYPE			1002
#define IDC_CB_INIFILETYPE			1003
#define IDC_CB_TEST_RETRY			1004
#define IDC_CB_SN_VERIFY			1005
#define IDC_ED_EXPOSURE				1006
#define IDC_ED_ALPHA				1007
#define IDC_ED_FOCALLENGTH			1008
#define IDC_ED_PIXELSIZE			1009
#define IDC_ED_LIGHT_DELAY			1010
#define IDC_ED_CHANGE_DELAY			1011
#define IDC_CB_USE_STECAL_FLAG		1012
#define IDC_EB_LEDRMSCycle			1013

#define		TABSTYLE_COUNT			1

 static UINT g_TabOrder[TABSTYLE_COUNT] =
 {
 	1001
 };


// CWnd_Cfg_Model
IMPLEMENT_DYNAMIC(CWnd_Cfg_Model, CWnd_BaseView)

CWnd_Cfg_Model::CWnd_Cfg_Model()
{
	VERIFY(m_font_Default.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_Cfg_Model::~CWnd_Cfg_Model()
{
	m_font_Default.DeleteObject();
}


BEGIN_MESSAGE_MAP(CWnd_Cfg_Model, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_CBN_SELENDOK(IDC_CB_MODELTYPE, OnCbnSelendokModelType)
END_MESSAGE_MAP()


// CWnd_Cfg_Model message handlers
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
int CWnd_Cfg_Model::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD /*| WS_CLIPCHILDREN*/ | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_ModelCode.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_ModelCode.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_ModelCode.SetFont_Gdip(L"Arial", 9.0F);
	m_st_ModelCode.Create(_T("Model"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_cb_ModelType.Create(dwStyle | CBS_DROPDOWNLIST | WS_TABSTOP, rectDummy, this, IDC_CB_MODELTYPE);

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		m_cb_ModelType.AddString(g_szModelName[Model_OMS_Entry]);
		m_cb_ModelType.AddString(g_szModelName[Model_OMS_Front]);
		m_cb_ModelType.AddString(g_szModelName[Model_OMS_Front_Set]);
		break;

	case Sys_3D_Cal:
		//m_cb_ModelType.AddString(g_szModelName[Model_OMS_Entry]);
		m_cb_ModelType.AddString(g_szModelName[Model_OMS_Front_Set]);
		break;

	case Sys_Focusing:
		m_cb_ModelType.AddString(g_szModelName[Model_OMS_Front]);
		m_cb_ModelType.AddString(g_szModelName[Model_MRA2]);
		m_cb_ModelType.AddString(g_szModelName[Model_IKC]);
		break;

	case Sys_IR_Image_Test:
		m_cb_ModelType.AddString(g_szModelName[Model_OV10642]);
		m_cb_ModelType.AddString(g_szModelName[Model_AR0220]);
		break;

	case Sys_Image_Test:
		m_cb_ModelType.AddString(g_szModelName[Model_OMS_Entry]);
		m_cb_ModelType.AddString(g_szModelName[Model_OMS_Front]);
		m_cb_ModelType.AddString(g_szModelName[Model_MRA2]);
		m_cb_ModelType.AddString(g_szModelName[Model_IKC]);
		break;

	default:
		break;
	}

	m_cb_ModelType.SetCurSel(0);

	m_st_iniFile.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_iniFile.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_iniFile.SetFont_Gdip(L"Arial", 9.0F);
	m_st_iniFile.Create(_T("*.ini"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_cb_iniFile.Create(dwStyle | CBS_DROPDOWNLIST | WS_TABSTOP, rectDummy, this, IDC_CB_INIFILETYPE);

	if (!m_szI2CFolderPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szI2CFolderPath, _T("ini"));
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());
		m_cb_iniFile.SetCurSel(0);
	}

	// 전체 검사 불량 시 재시도 횟수
	m_st_Test_RetryCnt.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Test_RetryCnt.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Test_RetryCnt.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Test_RetryCnt.Create(_T("Test Retry Count"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_Test_RetryCnt.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_TEST_RETRY);
	
	CString szText;
	for (UINT nIdx = 0; nIdx <= 5; nIdx++)
	{
		szText.Format(_T("%d"), nIdx);
		m_cb_Test_RetryCnt.AddString(szText);
	}
	m_cb_Test_RetryCnt.SetCurSel(0);


	// 검사 진행
	m_st_Test_Continue.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Test_Continue.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Test_Continue.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Test_Continue.Create(_T("Test Mode"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_Test_Continue.Create(dwStyle | CBS_DROPDOWNLIST, rectDummy, this, IDC_CB_TEST_RETRY);

	for (UINT nIdx = 0; nIdx < TestMode_MaxNum; nIdx++)
	{
		m_cb_Test_Continue.AddString(g_szTestMode[nIdx]);
	}
	m_cb_Test_Continue.SetCurSel(0);

	// Exposure
	m_st_Exposure.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Exposure.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Exposure.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Exposure.Create(_T("Exposure"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	
	m_ed_Exposure.Create(dwStyle | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_ED_EXPOSURE);
	m_ed_Exposure.SetValidChars(_T("0123456789.-"));
	m_ed_Exposure.SetFont(&m_font_Default);

	// 검사 관련 수치 설정
	m_st_FocalLength.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_FocalLength.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_FocalLength.SetFont_Gdip(L"Arial", 9.0F);
	m_st_FocalLength.Create(_T("Focal Length"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_FocalLength.Create(dwStyle | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_ED_FOCALLENGTH);
	m_ed_FocalLength.SetValidChars(_T("0123456789.-"));
	m_ed_FocalLength.SetFont(&m_font_Default);

	m_st_PixelSize.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_PixelSize.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_PixelSize.SetFont_Gdip(L"Arial", 9.0F);
	m_st_PixelSize.Create(_T("Pixel Size"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_PixelSize.Create(dwStyle | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_ED_PIXELSIZE);
	m_ed_PixelSize.SetValidChars(_T("0123456789.-"));
	m_ed_PixelSize.SetFont(&m_font_Default);

	m_st_LightDelay.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_LightDelay.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_LightDelay.SetFont_Gdip(L"Arial", 9.0F);
	m_st_LightDelay.Create(_T("Light Delay"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_LightDelay.Create(dwStyle | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_ED_LIGHT_DELAY);
	m_ed_LightDelay.SetValidChars(_T("0123456789"));
	m_ed_LightDelay.SetFont(&m_font_Default);
	
	m_st_Visel_LED.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Visel_LED.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Visel_LED.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Visel_LED.Create(_T("LED Intensity"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_Visel_LED.Create(dwStyle | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_ED_LIGHT_DELAY);
	m_ed_Visel_LED.SetValidChars(_T("0123456789"));
	m_ed_Visel_LED.SetFont(&m_font_Default);

	
	m_st_LEDRMSCycle.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_LEDRMSCycle.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_LEDRMSCycle.SetFont_Gdip(L"Arial", 9.0F);
	m_st_LEDRMSCycle.Create(_T("LED DutyControl"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_LEDRMSCycle.Create(dwStyle | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_EB_LEDRMSCycle);
	m_ed_LEDRMSCycle.SetValidChars(_T("0123456789.-"));
	m_ed_LEDRMSCycle.SetFont(&m_font_Default);
	

	m_st_CamImageType.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CamImageType.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_CamImageType.SetFont_Gdip(L"Arial", 9.0F);

	m_st_CamImageType.Create(_T("Cam Image Type"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_CamImageType.Create(dwStyle | CBS_DROPDOWNLIST | WS_TABSTOP, rectDummy, this, IDC_CB_MODELTYPE);

	m_st_ChangeDelay.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_ChangeDelay.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_ChangeDelay.SetFont_Gdip(L"Arial", 9.0F);
	m_st_ChangeDelay.Create(_T("Cam Change Time (s)"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	m_ed_ChangeDelay.Create(dwStyle | WS_BORDER | ES_CENTER | WS_TABSTOP, rectDummy, this, IDC_ED_CHANGE_DELAY);
	m_ed_ChangeDelay.SetValidChars(_T("0123456789"));
	m_ed_ChangeDelay.SetFont(&m_font_Default);

	for (int t = 0; t < CAM_STATE_MAXNUM; t++)
	{
		m_cb_CamImageType.AddString(g_szCamState[t]);
	}

	m_cb_CamImageType.SetCurSel(0);

	// 스테레오 Calibration 전용
	m_st_UseStereoCal_Flag.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_UseStereoCal_Flag.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_UseStereoCal_Flag.SetFont_Gdip(L"Arial", 9.0F);
	m_st_UseStereoCal_Flag.Create(_T("Use Calibration Flag"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	m_cb_UseStereoCal_Flag.Create(dwStyle | CBS_DROPDOWNLIST | WS_TABSTOP, rectDummy, this, IDC_CB_USE_STECAL_FLAG);
	m_cb_UseStereoCal_Flag.AddString(_T("Not Use"));
	m_cb_UseStereoCal_Flag.AddString(_T("Use"));
	m_cb_UseStereoCal_Flag.SetCurSel(0);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 10;
	int iSpacing		= 5;
	int iCateSpacing	= 10;

	int iLeft			= iMagrin;
	int iTop			= iMagrin;
	int iWidth			= cx - iMagrin - iMagrin;
	int iHeight			= cy - iMagrin - iMagrin;

	int iCtrlHeight		= 25;
	int iCapWidth		= (iWidth - iSpacing - iSpacing) / 6;
	int iDataWidth		= iCapWidth * 2;
	int iLeftSub		= iLeft + iCapWidth + iSpacing;
	int iTempWidth		= 0;

	iTop = iMagrin;
	m_st_ModelCode.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_ModelType.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_iniFile.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_iniFile.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_Test_RetryCnt.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_Test_RetryCnt.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

	iTop += iCtrlHeight + iSpacing;
	m_st_Test_Continue.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
	m_cb_Test_Continue.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

	m_st_Exposure.MoveWindow(0, 0, 0, 0);
	m_ed_Exposure.MoveWindow(0, 0, 0, 0);

	m_st_FocalLength.MoveWindow(0, 0, 0, 0);
	m_ed_FocalLength.MoveWindow(0, 0, 0, 0);

	m_st_PixelSize.MoveWindow(0, 0, 0, 0);
	m_ed_PixelSize.MoveWindow(0, 0, 0, 0);

	m_st_CamImageType.MoveWindow(0, 0, 0, 0);
	m_cb_CamImageType.MoveWindow(0, 0, 0, 0);

	m_st_UseStereoCal_Flag.MoveWindow(0, 0, 0, 0);
	m_cb_UseStereoCal_Flag.MoveWindow(0, 0, 0, 0);

	m_st_LEDRMSCycle.MoveWindow(0, 0, 0, 0);
	m_ed_LEDRMSCycle.MoveWindow(0, 0, 0, 0);

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	{
		iTop += iCtrlHeight + iSpacing;
		m_st_FocalLength.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_FocalLength.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
		m_st_PixelSize.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_PixelSize.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
						 
		iTop += iCtrlHeight + iSpacing;
		m_st_LightDelay.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_LightDelay.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
		
		iTop += iCtrlHeight + iSpacing;
		m_st_ChangeDelay.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_ChangeDelay.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
		m_st_CamImageType.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_cb_CamImageType.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
	}
		break;

	case Sys_2D_Cal:
	{
		iTop += iCtrlHeight + iSpacing;
		m_st_Exposure.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_Exposure.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
	}
		break;

	case Sys_IR_Image_Test:
	{
// 		iTop += iCtrlHeight + iSpacing;
// 		m_st_Exposure.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 		m_ed_Exposure.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
// 
// 		iTop += iCtrlHeight + iSpacing;
// 		m_st_LightDelay.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 		m_ed_LightDelay.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
// 
// 		iTop += iCtrlHeight + iSpacing;
// 		m_st_CamImageType.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 		m_cb_CamImageType.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
// 
// 		iTop += iCtrlHeight + iSpacing;
// 		m_st_Visel_LED.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 		m_ed_Visel_LED.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
// 
// 		iTop += iCtrlHeight + iSpacing;
// 		m_st_LEDRMSCycle.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
// 		m_ed_LEDRMSCycle.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
	}
		break; 
	
	case Sys_Image_Test:
	{
		iTop += iCtrlHeight + iSpacing;
		m_st_Exposure.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_Exposure.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
		m_st_FocalLength.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_FocalLength.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
		m_st_PixelSize.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_PixelSize.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
		m_st_LightDelay.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_LightDelay.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;

		m_st_CamImageType.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_cb_CamImageType.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);

		
	}
		break;

	case Sys_3D_Cal:
	{
		iTop += iCtrlHeight + iSpacing;
		m_st_Exposure.MoveWindow(iLeft, iTop, iCapWidth, iCtrlHeight);
		m_ed_Exposure.MoveWindow(iLeftSub, iTop, iDataWidth, iCtrlHeight);
	}
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd_BaseView::PreCreateWindow(cs);
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/3/17 - 11:33
// Desc.		:
//=============================================================================
BOOL CWnd_Cfg_Model::PreTranslateMessage(MSG* pMsg)
{
	switch (pMsg->message)
	{
	case WM_CHAR:
		if (VK_TAB == pMsg->wParam)
		{
			UINT nID = GetFocus()->GetDlgCtrlID();
 			for (int iCnt = 0; iCnt < TABSTYLE_COUNT; iCnt++)
 			{
 				if (nID == g_TabOrder[iCnt])
 				{
 					if ((TABSTYLE_COUNT - 1) == iCnt)
 					{
 						nID = g_TabOrder[0];
 					}
 					else
 					{
 						nID = g_TabOrder[iCnt + 1];
 					}
 
 					break;
 				}
 			}

			GetDlgItem(nID)->SetFocus();
		}
		break;

	default:
		break;
	}

	return CWnd_BaseView::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: OnCbnSelendokModelType
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/8 - 17:29
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::OnCbnSelendokModelType()
{
	int iSel = m_cb_ModelType.GetCurSel();

	if (0 <= iSel)
	{
		CString strValue;
		m_cb_ModelType.GetWindowText(strValue);

		enModelType ModelType;
		for (UINT nIdx = 0; ((NULL != g_szModelName[nIdx]) || (nIdx < Model_MaxEnum)); nIdx++)
		{
			if (0 == strValue.Compare(g_szModelName[nIdx]))
			{
				ModelType = (enModelType)nIdx;
			}
		}
		
		GetOwner()->SendNotifyMessage(WM_CHANGED_MODEL_TYPE, (WPARAM)ModelType, 0);
	}
}

void CWnd_Cfg_Model::RefreshFileList(__in const CStringList* pFileList)
{
	m_cb_iniFile.ResetContent();

	INT_PTR iFileCnt = pFileList->GetCount();

	POSITION pos;
	for (pos = pFileList->GetHeadPosition(); pos != NULL;)
	{
		m_cb_iniFile.AddString(pFileList->GetNext(pos));
	}

	// 초기화
	if (0 < pFileList->GetCount())
	{
		m_cb_iniFile.SetCurSel(0);
	}
}

//=============================================================================
// Method		: GetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/17 - 13:13
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetUIData(__out ST_RecipeInfo& stRecipeInfo)
{
	CString strValue;	

	// 모델명
	if (0 <= m_cb_ModelType.GetCurSel())
	{
		m_cb_ModelType.GetWindowText(strValue);
		stRecipeInfo.szModelCode = strValue;

		for (UINT nIdx = 0; ((NULL != g_szModelName[nIdx]) || (nIdx < Model_MaxEnum)); nIdx++)
		{
			if (0 == strValue.Compare(g_szModelName[nIdx]))
			{
				stRecipeInfo.ModelType = (enModelType)nIdx;
			}
		}
	}


	// Test Retry Count
	int iSel = m_cb_Test_RetryCnt.GetCurSel();
	stRecipeInfo.nTestRetryCount = (0 <= iSel) ? iSel : 0;

	stRecipeInfo.nTestMode = m_cb_Test_Continue.GetCurSel();

	//
	m_ed_Exposure.GetWindowText(strValue);
	stRecipeInfo.dExposure = _ttof(strValue);

	m_ed_LEDRMSCycle.GetWindowText(strValue);
	stRecipeInfo.dLEDRMSCycle = _ttof(strValue);

	m_ed_FocalLength.GetWindowText(strValue);
	stRecipeInfo.fFocalLength = (float)_ttof(strValue);

	m_ed_PixelSize.GetWindowText(strValue);
	stRecipeInfo.fPixelSize = (float)_ttof(strValue);

	m_ed_LightDelay.GetWindowText(strValue);
	stRecipeInfo.nLightDelay = _ttoi(strValue);

	m_ed_Visel_LED.GetWindowText(strValue);
	stRecipeInfo.nViselLED = _ttoi(strValue);

	m_ed_ChangeDelay.GetWindowText(strValue);
	stRecipeInfo.nChangeDelay = _ttoi(strValue);

	iSel = m_cb_CamImageType.GetCurSel();
	stRecipeInfo.nCamImageType = (0 <= iSel) ? iSel : 0;

	iSel = m_cb_UseStereoCal_Flag.GetCurSel();
	stRecipeInfo.bUseStereoCal_Flag = (0 == iSel) ? FALSE : TRUE;

	m_cb_iniFile.GetWindowText(strValue);
	stRecipeInfo.sziicFile = strValue;
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetUIData(__in const ST_RecipeInfo* pRecipeInfo)
{
	CString strValue;
	// 모델명
	int iIndex = m_cb_ModelType.FindString(0, g_szModelName[pRecipeInfo->ModelType]);
	if (CB_ERR != iIndex)
	{
		m_cb_ModelType.SetCurSel(iIndex);	
	}

	// Test Retry Count
	int iSel = (pRecipeInfo->nTestRetryCount <= 5) ? pRecipeInfo->nTestRetryCount : 5;
	m_cb_Test_RetryCnt.SetCurSel(iSel);

	m_cb_Test_Continue.SetCurSel(pRecipeInfo->nTestMode);

	strValue.Format(_T("%.6f"), pRecipeInfo->dExposure);
	m_ed_Exposure.SetWindowText(strValue);

	strValue.Format(_T("%.2f"), pRecipeInfo->fFocalLength);
	m_ed_FocalLength.SetWindowText(strValue);

	strValue.Format(_T("%d"), pRecipeInfo->nViselLED);
	m_ed_Visel_LED.SetWindowText(strValue);

	strValue.Format(_T("%.2f"), pRecipeInfo->dLEDRMSCycle);
	m_ed_LEDRMSCycle.SetWindowText(strValue);

	strValue.Format(_T("%.2f"), pRecipeInfo->fPixelSize);
	m_ed_PixelSize.SetWindowText(strValue);

	strValue.Format(_T("%d"), pRecipeInfo->nLightDelay);
	m_ed_LightDelay.SetWindowText(strValue);

	strValue.Format(_T("%d"), pRecipeInfo->nChangeDelay);
	m_ed_ChangeDelay.SetWindowText(strValue);

	m_cb_CamImageType.SetCurSel(pRecipeInfo->nCamImageType);

	m_cb_UseStereoCal_Flag.SetCurSel(pRecipeInfo->bUseStereoCal_Flag);

	//strValue = m_szI2CFolderPath + _T("\\")+ pRecipeInfo->sziicFile;

	if (!m_szI2CFolderPath.IsEmpty())
	{
		m_IniWatch.SetWatchOption(m_szI2CFolderPath, _T("ini"));
		m_IniWatch.RefreshList();

		RefreshFileList(m_IniWatch.GetFileList());

		int iSel =  m_cb_iniFile.FindString(-1, pRecipeInfo->sziicFile);
		m_cb_iniFile.SetCurSel(iSel);
	}


}

//=============================================================================
// Method		: SetRecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in const ST_RecipeInfo * pRecipeInfo
// Qualifier	:
// Last Update	: 2016/3/18 - 16:58
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::SetRecipeInfo(__in const ST_RecipeInfo* pRecipeInfo)
{
	SetUIData(pRecipeInfo);
}

//=============================================================================
// Method		: GetRecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stRecipeInfo
// Qualifier	:
// Last Update	: 2017/1/4 - 10:42
// Desc.		:
//=============================================================================
void CWnd_Cfg_Model::GetRecipeInfo(__out ST_RecipeInfo& stRecipeInfo)
{
	GetUIData(stRecipeInfo);
	
}
