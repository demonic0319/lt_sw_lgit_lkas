﻿//*****************************************************************************
// Filename	: Wnd_MainView.h
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 기본 화면용 윈도우
//*****************************************************************************
#ifndef Wnd_MainView_h__
#define Wnd_MainView_h__

#pragma once

#include "Def_TestDevice.h"
#include "Def_DataStruct.h"
#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Wnd_LGEProgress.h"
#include "Wnd_LGEInspInfo.h"
#include "Wnd_LGETestResult.h"
#include "Wnd_Consumables.h"
#include "Wnd_Torque.h"
#include "List_Barcode.h"
#include "Grid_CycleTime.h"
#include "Grid_Yield.h"
#include "Grid_Yield_Test.h"
#include "Wnd_ImgProc.h"
//#include "Tab_ImgProc.h"
#include "Wnd_TabImgProc.h"
#include "Wnd_CameraView_Cv.h"
#include "Wnd_TestResult_IR_Img_MainView.h"

#include "Wnd_Monitoring.h"
#include "Wnd_TimerView.h"
#include "Wnd_Graph.h"

//=============================================================================
// CWnd_MainView
//=============================================================================
class CWnd_MainView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_MainView)

public:
	CWnd_MainView();
	virtual ~CWnd_MainView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);

	afx_msg void	OnBnClickedRbCameraSelLeft	();
	afx_msg void	OnBnClickedRbCameraSelRight	();

	virtual void	MoveWindow_Monitoring		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Info				(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Info_SteCal		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Info_Foc			(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Info_ImgT		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Info_IR_ImgT		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Status			(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Image			(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);

	virtual void	MoveWindow_Image_2DCal		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Image_SteCal		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Image_Foc		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Image_ImgT		(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);
	virtual void	MoveWindow_Image_IR_ImgT	(int x, int y, int nWidth, int nHeight, BOOL bRepaint = TRUE);


	// 검사기 설정
	enInsptrSysType m_InspectionType		= enInsptrSysType::Sys_Focusing;

	CFont			m_font_Default;	
	CFont			m_Font;

	// 검사 결과 판정
	CVGStatic			m_st_Judgment;
	CVGStatic			m_st_OperateMode;

	// 검사 진행 현황 (완료 검사 항목 수 / 전체 검사 항목 수 = 진행률%)
	CWnd_LGEProgress	m_wnd_Progress;
	// 검사 항목 진행 현황
	CWnd_LGETestResult	m_wnd_TestResult[USE_CHANNEL_CNT];	// 2개
	// 알람
	CVGStatic			m_st_Alarm;
	
	// 영상 뷰 윈도우
	//CMFCButton			m_rb_CameraSelect[USE_CHANNEL_CNT];		// 카메라 선택 버튼

	CWnd_ImgProc		m_wnd_ImageLive[USE_CHANNEL_CNT];
	CWnd_ImgProc		m_wnd_ImageLive_Vision;

	CWnd_TabImgProc		m_tc_ImageCapture[USE_CHANNEL_CNT];		// 탭 컨트롤
	
	// 검사 정보 표시용 윈도우
	CWnd_LGEInspInfo	m_wnd_InspInfo;		// 검사 정보
	CWnd_Torque			m_wnd_Torque;		// Torque
	CWnd_Graph			m_wnd_Graph;
	CWnd_Monitoring		m_Wnd_Monitoring;	// 모터 센서 모니터링
	CWnd_Consumables	m_wnd_ConsumInfo;	// 소모품 사용 현황
	CList_Barcode		m_lc_Barcode;		// Barcode 이력
	CGrid_CycleTime		m_grid_CycleTime;	// 검사 시간
	CGrid_Yield			m_grid_Yield;		// 수율
	CGrid_Yield_Test	m_grid_YieldTest;	// 검사 항목 별 수율
	CMFCTabCtrl			m_tc_Info;			// 탭 컨트롤

	// 외곽 가이드 라인 용도
	CVGStatic			m_st_frameZone;
	CVGStatic			m_st_frameSatus;
	CVGStatic			m_st_frameInfo;

	// 데이터
	enPermissionMode	m_InspMode;
	ST_InspectionInfo*	m_pstInspInfo;
	// 주변장치 제어 구조체
	ST_Device*			m_pDevice				= NULL;
	
	BOOL				m_bUseImageViewWnd		= TRUE;



public:

	CWnd_TestResult_IR_Img_MainView		m_wnd_TestResult_IR_Img[USE_CHANNEL_CNT];
	CWnd_TimerView		m_wnd_TimerView;

	// 전체 검사 정보 구조체 포인터 설정
	void		SetPtrInspectionInfo(__inout ST_InspectionInfo* pstInspInfo)
	{
		m_pstInspInfo = pstInspInfo;

		//m_wnd_Graph.SetPtr_RecipeInfo(&pstInspInfo->RecipeInfo.stFocus.stGraphOpt);
	};

	void		SetPtr_Device(__in ST_Device* pDevice)
	{
		m_pDevice = pDevice;
	};

	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);
	
	void		SetModelType			(__in enModelType nModelType);
	
	void		SetJudgmentStatus		(__in enTestResult nJudgment);

	// 검사 진행 상태
	void		SetProgressStatus		(__in UINT nTotalStep, __in UINT nProgStep);
	// 파라미터 검사 진행 상태
	void		SetParaStatus			(__in enTestProcess nProgress, __in UINT nParaIndex = 0);
	// Barcode 정보 표시
	void		SetBarcode				(__in LPCTSTR szBarcode, __in UINT nRetryCnt = 0,  __in UINT nParaIndex = 0);
	// 파라미터 별 제품 투입 시간
	void		SetParaInputTime		(__in PSYSTEMTIME pstTime, __in UINT nParaIndex = 0);

	// 검사 모드 설정
	void		SetPermissionMode		(__in enPermissionMode InspMode);
	// 설비 구동 모드
	void		SetOperateMode			(__in enOperateMode nOperMode);
	
	void	UpdateEquipmentInfo			();
	void	UpdateRecipeInfo			();	// 모델 정보 갱신
	void	UpdateYield					();	// 수율 정보 갱신
	void	UpdatePogoCount				();	// 포고 카운트 갱신
	void	UpdatePogoCount				(__in UINT nUnitIdx);
	void	UpdateCycleTime				(); // 검사 시간 갱신
	
	// 검사별 상세 정보 --------------------------------------------------------
	void	UpdateTestProgress			();						// 전체 파라의 검사 진행 상태 표시
	void	UpdateTestProgress_Unit		(__in UINT nUnitIdx);	// 개별 파라의 검사 진행 상태 표시
	void	UpdateBarcode				(__in UINT nUnitIdx);
	void	Update_TestReport			(__in UINT nUnitIdx);	// 검사 결과 UI 업데이트
	
	// 검사 결과 표시
	void	SetTestProgressStep			(__in UINT nTotalStep, __in UINT nProgStep);
	void	SetTestResult_Unit			(__in UINT nUnitIdx, __in enTestResult nResult);
	void	SetTestResultCode_Unit		(__in UINT nUnitIdx, __in LRESULT nResultCode);
	void	SetTestStep_Select			(__in UINT nStepIdx, __in UINT nParaIdx = 0);
	void	SetTestStep_Result			(__in UINT nStepIdx, __in enTestResult nResult, __in UINT nParaIdx = 0);
	
	void	SetMoterMoniter				(__in CString szDistance = NULL, __in CString szChartTX = NULL, __in CString szChartTZ = NULL);

	// 그래프 갱신
	void	UpdateGraphData				(__in double dbCurrentSFR, __in double dbMaxSFR);
	void	UpdateGraphReset			();

	// 검사 진행 시간 갱신
	void	UpdateElapTime_TestUnit		(__in UINT nUnitIdx);
	void	UpdateElapTime_Cycle		();
	void	UpdateInputTime				();
	void	UpdateInputTime_Unit		(__in UINT nUnitIdx);
	
	// 검사 결과 초기화
	void	InitTestResult				();

	// 바코드 세팅 (핸디 스캐너로 읽은 바코드)
	void	Insert_ScanBarcode			(__in LPCTSTR szBarcode, __in UINT nParaIdx);
	void	Insert_ScanBarcode			(__in LPCTSTR szBarcode);

	// UI 데이터 초기화
	void	ResetInfo_Loading			();	// 제품 투입시 UI 초기화
	void	ResetInfo_StartTest			(__in UINT nParaIdx = 0);	// 제품 검사 시작시 UI 초기화
	void	ResetInfo_Unloading			();	// 제품 배출시 UI 초기화
	void	ResetInfo_Measurment		(__in UINT nParaIdx = 0);	// 재검사 할 때 UI 초기화

	// 알람 표시 (시간, 대상, 메세지, 알람ID)
	void	Reset_Alarm					();
	void	Set_Alarm					(__in LPCTSTR szAlarm);

	// 영상 출력
	void	ShowVideo					(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void	NoSignal_Ch					(__in INT iChIdx);
	void	Set_FrameRate				(__in INT iChIdx, __in DWORD dwFrameRate);

	// 비전 영상 출력
	void	ShowVideo_Vision			(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void	NoSignal_Ch_Vision			(__in INT iChIdx);
	void	Set_FrameRate_Vision		(__in INT iChIdx, __in DWORD dwFrameRate);

	//void	Set_Overlay_Particle		(__in INT iChIdx, __in const stParticleList* pstYmeanList);
	void	Add_ImageHistory			(__in INT iChIdx, __in LPCTSTR szTitle, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void	Set_ImageHistory			(__in INT iChIdx, __in LPCTSTR szTitle, __in UINT nIndex, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight);
	void	Clear_ImageHistory			(__in INT iChIdx);
	
	// 카메라 선택 (왼/오른)
	void	Set_CameraSelect			(__in INT iChIdx, __in BOOL bUpdateRadioButton = TRUE);

	// 실시간 영상 윈도우 표시 여부
	void	Set_UseImageViewWnd			(__in BOOL bUseImageViewWnd);
	//afx_msg void OnBnClickedPreWork();
	void	Foc_ChangeCameraMode		(BOOL bMode, int nCamIdx);
	void	Foc_ChangeCameraStop		();
	BOOL	Foc_ChangeCamFlag			();
	void	SetTestResult_Total			(__in enTestResult nResult);
	void	SetLightCount				(int nCnt);
};

#endif // Wnd_MainView_h__
