﻿//*****************************************************************************
// Filename	: 	Wnd_MotionOp.h
// Created	:	2017/03/28 - 13:47
// Modified	:	2017/03/28 - 13:47
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************

#pragma once
#include "VGStatic.h"
#include "resource.h"
#include "MotionManager.h"

enum enMotionOpStatic
{
	ST_MO_PULSE = 0,
	ST_MO_SERVO,
	ST_MO_POSITION,
	ST_MO_ENC,
	ST_MO_ALARAM,
	ST_MO_HOME,
	ST_MO_HLIMIT,
	ST_MO_LLIMIT,
	ST_MO_EMO,
	ST_MO_UNIT_PER,
	ST_MO_PULSE_PER,
	ST_MO_USE,
	ST_MO_NUMBER,
	ST_MO_NAME,
	ST_MO_VAL,
	ST_MO_ACC,
	ST_MO_MAXNUM,
};

static LPCTSTR g_szMotionOpName[] =
{
	_T("Puls Output"),
	_T("Servo On Level"),
	_T("In Position"),
	_T("Enc. Input"),
	_T("Alarm Level"),
	_T("Home Level"),
	_T("+ Limit Level"),
	_T("- Limit Level"),
	_T("Emergency Level"),
	_T("[Unit] Per Pulse"),
	_T("Unit Per [Pulse]"),
	_T("Use Axis"),
	_T("Use Axis Number"),
	_T("Axis Name"),
	_T("Axis Val"),
	_T("Axis Acc"),
	NULL
};

enum enMotionOpComboItem
{
	CB_MO_PULSE = 0,
	CB_MO_SERVO,
	CB_MO_POSITION,
	CB_MO_ENC,
	CB_MO_ALRAM,
	CB_MO_HOME,
	CB_MO_HLIMIT,
	CB_MO_LLIMIT,
	CB_MO_EMO,
	CB_MO_USED,
	CB_MO_MAXNUM,
};

enum enMotorOpEditItem
{
	ED_MO_NUMBER = 0,
	ED_MO_NAME,
	ED_MO_VAL,
	ED_MO_ACC,
	ED_MO_UINT_PER,
	ED_MO_PULSE_PER,
	ED_MO_MAXNUM,
};

static LPCTSTR g_szMotorOpPuls[] =
{
	_T(" 0 : OneHighLowHigh"),
	_T(" 1 : OneHighHighLow"),
	_T(" 2 : OneLowLowHigh"),
	_T(" 3 : OneLowHightLow"),
	_T(" 4 : TwoCcwCwHigh"),
	_T(" 5 : TwoCwCcwLow"),
	_T(" 6 : TwoCwCcwHigh"),
	_T(" 7 : TwoCwCcwLow"),
	_T(" 8 : TwoPhase"),
	_T(" 9 : TwoPhaseReverse"),
	NULL
};

static LPCTSTR g_szMotorOpEnc[] =
{
	_T(" 0 : UpDownMode"),
	_T(" 1 : Sqr 1Mode"),
	_T(" 2 : Sqr 2Mode"),
	_T(" 3 : Sqr 4Mode"),
	_T(" 4 : Reverse UpDownMode"),
	_T(" 5 : ReverseSqr 1Mode"),
	_T(" 6 : ReverseSqr 2Mode"),
	_T(" 7 : ReverseSqr 4Mode"),
	NULL
};

static LPCTSTR g_szMotorOpLevel[] =
{
	_T(" 0 : LOW"),
	_T(" 1 : HIGH"),
	_T(" 2 : UNUSED"),
	_T(" 3 : USED"),
	NULL
};

static LPCTSTR g_szMotorOpServo[] =
{
	_T(" 0 : LOW"),
	_T(" 1 : HIGH"),
	NULL
};

static LPCTSTR g_szMotorOpUse[] =
{
	_T(" 0 : UNUSED"),
	_T(" 1 : USED"),
	NULL
};

static LPCTSTR g_szMotionOpButton[] =
{
	_T("SETTING"),
	_T("SAVE"),
	NULL
};

enum enMotionOpButton
{
	BN_MO_SETTING = 0,
	BN_MO_SAVE,
	BN_MO_MAXNUM,
};

// CWnd_MotionOp
class CWnd_MotionOp : public CWnd
{
	DECLARE_DYNAMIC(CWnd_MotionOp)

public:
	CWnd_MotionOp();
	virtual ~CWnd_MotionOp();

	CMotionManager*	m_pstDevice;

	void SetPtr_Device(__in CMotionManager* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;
	};

	void	SetUpdateData();
	void	SetSelectAxis(UINT nAxis);

protected:

	CFont			m_font;
	CVGStatic		m_st_Name;
	UINT			m_nAxis;
	CVGStatic		m_st_Item[ST_MO_MAXNUM];
	CMFCButton		m_bn_Item[BN_MO_MAXNUM];
	CComboBox		m_cb_Item[CB_MO_MAXNUM];
	CMFCMaskedEdit	m_ed_Item[ED_MO_MAXNUM];

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnSave();
	afx_msg void	OnBnClickedBnSetting();
	virtual BOOL	PreTranslateMessage(MSG* pMsg);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	void			GetUpdateData();

	DECLARE_MESSAGE_MAP()
};