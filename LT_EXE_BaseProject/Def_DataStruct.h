﻿//*****************************************************************************
// Filename	: Def_DataStruct.h
// Created	: 2012/11/1
// Modified	: 2016/12/29
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_DataStruct_h__
#define Def_DataStruct_h__

#include <afxwin.h>
#include "Def_CompileOption.h"
#include "Def_Enum.h"
#include "Def_Test.h"
#include "Def_DataStruct_Cm.h"
#include "Def_ResultCode_Cm.h"
#include "Def_TestItem_Cm.h"
#include "Def_Report.h"
#include "Def_DAQWrapper.h"
#include "Def_Motion.h"

#pragma pack(push, 1)

typedef struct _tag_LightCfg
{
	float	fVolt;
	float	fCurrent;
	WORD	wStep;

	// Voltage Min Max ( 0 ~ 12V )
	float	fVoltMin;
	float	fVoltMax;
	// Current Min Max ( 0 ~ 3000 mA )
	float	fCurrMin;
	float	fCurrMax;
	// Step Min Max ( 0 ~ 1024 )
	WORD	wStepMin;
	WORD	wStepMax;

	_tag_LightCfg()
	{
		Reset();
	};

	_tag_LightCfg& operator= (const _tag_LightCfg& ref)
	{
		fVolt		= ref.fVolt;
		fCurrent	= ref.fCurrent;;
		wStep		= ref.wStep;

		fVoltMin	= ref.fVoltMin;
		fVoltMax	= ref.fVoltMax;
		fCurrMin	= ref.fCurrMin;
		fCurrMax	= ref.fCurrMax;
		wStepMin	= ref.wStepMin;
		wStepMax	= ref.wStepMax;

		return *this;
	};

	void Reset()
	{
		fVolt		= 0.0;
		fCurrent	= 0.0f;
		wStep		= 0;

		fVoltMin	= 0.0;
		fVoltMax	= 15.0;//12.0;
		fCurrMin	= 0.0;
		fCurrMax	= 3000.0;
		wStepMin	= 0;
		wStepMax	= 3000;
	};

}ST_LightCfg, *PST_LightCfg;

typedef struct _tag_LightInfo
{
	ST_LightCfg		stLightPSU[Chart_MaxEnum];
	ST_LightCfg		stLightBrd[Light_I_MaxEnum];

	_tag_LightInfo()
	{
		 
	};

	_tag_LightInfo& operator= (const _tag_LightInfo& ref)
	{
		for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
		{
			stLightPSU[nIdx] = ref.stLightPSU[nIdx];
		}

		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
			stLightBrd[nIdx] = ref.stLightBrd[nIdx];
		}

		return *this;
	};

	void Reset()
	{
		for (UINT nIdx = 0; nIdx < Chart_MaxEnum; nIdx++)
		{
			stLightPSU[nIdx].Reset();
		}

		for (UINT nIdx = 0; nIdx < Light_I_MaxEnum; nIdx++)
		{
			stLightBrd[nIdx].Reset();
		}
	};

}ST_LightInfo, *PST_LightInfo;


typedef struct _tag_PowerInfo
{
	float	fVoltage;
	float	fCurrent;

	_tag_PowerInfo()
	{
		fVoltage	= 0.0;
		fCurrent	= 0.0;
	};

	_tag_PowerInfo& operator= (_tag_PowerInfo& ref)
	{
		fVoltage = ref.fVoltage;
		fCurrent = ref.fCurrent;

		return *this;
	};

	void Reset()
	{
		fVoltage = 12.0;
		fCurrent = 0.0;
	};

}ST_PowerInfo, *PST_PowerInfo;

//---------------------------------------------------------
// 유지보수 설정값 구조체
//---------------------------------------------------------
typedef struct _tag_MaintenanceInfo
{
	enInsptrSysType		nInspectionType;

	CString				szMaintenanceFile;
	CString				szMaintenanceFullPath;
	CString				szMotorFile;
	CString				szLightFile;

	ST_LightInfo		stLightInfo;
	ST_TeachInfo		stTeachInfo;	// 티칭 위치 설정정보

	_tag_MaintenanceInfo()
	{
		nInspectionType = enInsptrSysType::Sys_Focusing;
	};

	_tag_MaintenanceInfo& operator= (_tag_MaintenanceInfo& ref)
	{
		szMaintenanceFile		= ref.szMaintenanceFile;
		szMaintenanceFullPath = ref.szMaintenanceFullPath;
		szMotorFile				= ref.szMotorFile;
		szLightFile				= ref.szLightFile;
		stLightInfo				= ref.stLightInfo;
		stTeachInfo				= ref.stTeachInfo;
		return *this;
	};

	void Reset()
	{
		szMaintenanceFile.Empty();
		szMaintenanceFullPath.Empty();
		szMotorFile.Empty();
		szLightFile.Empty();
		stLightInfo.Reset();
	};

void SetSystemType(__in enInsptrSysType nSysType)
{
	nInspectionType = nSysType;
};

}ST_MaintenanceInfo, *PST_MaintenanceInfo;

//---------------------------------------------------------
// LVDS Grabber 정보
//---------------------------------------------------------
typedef struct _tag_LVDSInfo
{
	UINT			nBoardNo[MAX_DAQ_CHANNEL];		// 보드 번호
	CString			strI2CFileName;					// I2C 파일 이름
	CString			strI2CFileName_2;				// I2C 파일 이름
	ST_DAQOption	stLVDSOption;					// DAQ Grabber 세팅 옵션

	UINT			nCameraDelay;	// 카메라 안정화 시간
	DWORD			dwWidth;		// Width 
	DWORD			dwHeight;		// Height 
	DOUBLE			dFrameRate;

	_tag_LVDSInfo()
	{
		strI2CFileName.Empty();
		strI2CFileName_2.Empty();
		nBoardNo[0] = DAQBoardNumber_0;
		nBoardNo[1] = DAQBoardNumber_1;
	};

	void Reset()
	{
		strI2CFileName.Empty();
		strI2CFileName_2.Empty();
		nBoardNo[0] = DAQBoardNumber_0;
		nBoardNo[1] = DAQBoardNumber_1;

		nCameraDelay	= 200;
		dwWidth			= 640;
		dwHeight		= 480;
		dFrameRate		= 20.0f;
	};

	_tag_LVDSInfo& operator= (_tag_LVDSInfo& ref)
	{
		for (int i = 0; i < MAX_DAQ_CHANNEL; i++)
			nBoardNo[i] = ref.nBoardNo[i];

		strI2CFileName	= ref.strI2CFileName;
		stLVDSOption	= ref.stLVDSOption;

		dwWidth			= ref.dwWidth;
		dwHeight		= ref.dwHeight;
		nCameraDelay	= ref.nCameraDelay;
		dFrameRate		= ref.dFrameRate;
		strI2CFileName_2 = ref.strI2CFileName_2;

		return *this;
	};

}ST_LVDSInfo, *PST_LVDSInfo;

//---------------------------------------------------------
// 레시피 설정값 구조체
//---------------------------------------------------------
typedef struct _tag_RecipeInfo : ST_RecipeInfo_Base
{
	ST_LVDSInfo			stLVDSInfo;
	CString				szMotorFile;

	// Parameter
	ST_2DCal_Info		st2D_CAL;

	ST_IR_Opt			stIR_ImageQ;
	ST_IQ_Info			stImageQ;
	ST_Foc_Info			stFocus;
	
	enOverlayItem		nOverlayItem;		// 테스트 중인 ITEM

	_tag_RecipeInfo()
	{
		nInspectionType		= enInsptrSysType::Sys_Focusing;
		nOverlayItem		= Ovr_SFR;
	};

	_tag_RecipeInfo& operator= (_tag_RecipeInfo& ref)
	{
		__super:: operator= (ref);

		stLVDSInfo		= ref.stLVDSInfo;
		szMotorFile		= ref.szMotorFile;
		st2D_CAL		= ref.st2D_CAL;
		nOverlayItem	= ref.nOverlayItem;
		stFocus			= ref.stFocus;
		stImageQ		= ref.stImageQ;
		stIR_ImageQ		= ref.stIR_ImageQ;

		return *this;
	};

	virtual void Reset()
	{
		stLVDSInfo.Reset();
		szMotorFile.Empty();
		st2D_CAL.Reset();
		stFocus.Reset();
		stImageQ.Reset();
		stIR_ImageQ.Reset();
	};

	// 검사기 종류 설정
	virtual void SetSystemType(__in enInsptrSysType nSysType)
	{
		__super::SetSystemType(nSysType);

	};

}ST_RecipeInfo, *PST_RecipeInfo;

//---------------------------------------------------------
// 프로그램에 사용되는 경로
//---------------------------------------------------------
typedef struct _tag_ProgramPath
{
	CString		szProgram;		// 프로그램 시작 경로
	CString		szLog;			// LOG 경로
	CString		szReport;		// 검사 결과 Report 경로
	CString		szRecipe;		// 모델 설정 파일 경로
	CString		szImage;		// 이미지 저장 경로
	CString		szI2c;			// I2c 경로
	CString		szI2cImage;		// I2c 이미지 경로
	CString		szVisionImage;		// I2c 이미지 경로
	CString		szConsumables;	// 포고 설정 파일 저장 경로
	CString		szMotor;		// Motor File 저장 경로
	CString		szMaintenance;	// Maintenance File 저장 경로

	CString		szInitIniFile;
}ST_ProgramPath, *PST_ProgramPath;

//---------------------------------------------------------
// 전체 검사에 관련된 데이터 기록용 구조체
//---------------------------------------------------------
typedef struct _tag_InspectionInfo : public ST_InspectionInfo_Base
{
	ST_CamInfo			CamInfo[USE_CHANNEL_CNT];		// Camera 검사 정보

	ST_RecipeInfo		RecipeInfo;						// 레시피 정보
	ST_Worklist			WorklistInfo;					// 검사 결과 워크리스트
	ST_ProgramPath		Path;							// 프로그램에 사용되는 폴더
	ST_MaintenanceInfo	MaintenanceInfo;				// 유지 정보

	INT					nTestPara;						// 현재 검사 중인 Para

	CString				szBarcodeBuf;					// 바코드 수신시 저장용 버퍼
	
	// Digital In 신호 
	BYTE				byDIO_DI[MAX_DIGITAL_IO];
	BYTE				byDIO_DO[MAX_DIGITAL_IO];
	DWORD64				dwDI;
	DWORD64				dwDO;

	// 영상 신호 상태
	//BOOL				bVideoSignal[USE_CHANNEL_CNT];

	_tag_InspectionInfo()
	{
		// 초기값 설정
		nTestPara			= -1;
		memset(byDIO_DI, 0, MAX_DIGITAL_IO);
		memset(byDIO_DO, 0, MAX_DIGITAL_IO);

		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
			CamInfo[nIdx].TestInfo.SetStepInfo_TestItemInfo(&RecipeInfo.StepInfo, &RecipeInfo.TestItemInfo);

			//bVideoSignal[nIdx] = FALSE;
		}
	};
	
	// 검사기 종류 설정
	void SetSystemType(__in enInsptrSysType nSysType)
	{
		RecipeInfo.SetSystemType(nSysType);

		UpdateTestInfo();
	};

	// 검사 정보 갱신
	void UpdateTestInfo()
	{
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
			CamInfo[nIdx].TestInfo.UpdateStepInfo_TestItemInfo();
		}			
	};

	// 개별 카메라 정보 초기화
	void ResetCamInfo(__in UINT nUnitIdx = 0)
	{
		if (nUnitIdx < USE_CHANNEL_CNT)
		{
			CamInfo[nUnitIdx].Reset(TRUE); // 바코드 초기화 안함
		}
	};
	
	// 모든 카메라 정보 초기화
	void ResetCamInfo_All(__in BOOL bWithoutBarcode = FALSE)
	{
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
			CamInfo[nIdx].Reset(bWithoutBarcode);
		}
	};

	// 측정 검사 초기화 (불량시 재검사 용도)
	virtual void Reset_Measurment(__in UINT nUnitIdx = 0)
	{
		if (nUnitIdx < USE_CHANNEL_CNT)
		{
			CamInfo[nUnitIdx].Reset_Measurment();
		}
	};
	
	// 제품 투입 정보 설정
	void SetInformation_Input (__in LPCTSTR szBarcode, __in UINT nUnitIdx = 0)
	{
		CamInfo[nUnitIdx].szLotID			= szLotName;
		CamInfo[nUnitIdx].szBarcode			= szBarcode;		
		CamInfo[nUnitIdx].szRecipeName		= szRecipeName;
		CamInfo[nUnitIdx].szOperatorName	= szOperatorName;
	};

	// 제품 투입 시간 (제품 Loading 시작)
	void SetInputTime()
	{
		for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
		{
			CamInfo[nUnitIdx].Set_StartLoading();
		}
	};

	void SetInputTime_Uint(__in UINT nUnitIdx = 0)
	{
		CamInfo[nUnitIdx].Set_StartLoading();
	};

	// 제품 검사 시작 시간 (제품  Loading 완료 후  검사 시작)
	void SetBeginTestTime(__in UINT nUnitIdx = 0)
	{
		if (nUnitIdx < USE_CHANNEL_CNT)
		{
			CamInfo[nUnitIdx].Set_StartTest();
		}
	};

	void SetEndTestTime(__in UINT nUnitIdx = 0)
	{
		if (nUnitIdx < USE_CHANNEL_CNT)
		{
			CamInfo[nUnitIdx].Set_EndTest();
		}
	};

	// 제품 배출 시간 (Product Unloading 완료)
	void SetOutputTime()
	{
		for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
		{
			CamInfo[nUnitIdx].Set_EndUnloading();

			// 배출 시간 설정
			dwTactTime = SetTactTime(CamInfo[nUnitIdx].TestTime.Cycle.dwEnd);
			CamInfo[nUnitIdx].dwTactTime = dwTactTime;
		}
	};

	// 바코드 임시 버퍼 초기화
	void ResetBarcodeBuffer()
	{
		szBarcodeBuf.Empty();
	};

	// 검사 진행 상태 리턴
	enTestProcess GetTestProgress(__in UINT nParaIdx = 0)
	{
		return CamInfo[nParaIdx].nProgressStatus;
	};

	// 검사 스텝 정보 얻기
	ST_StepMeasInfo* GetStepMeasInfo(__in UINT nParaIdx = 0)
	{
		return &CamInfo[nParaIdx].TestInfo;
	};

	// 검사 항목 측정데이터 얻기
	ST_TestItemMeas* GetTestItemMeas(__in UINT nStepIdx, __in UINT nParaIdx = 0)
	{
		if (nStepIdx < CamInfo[nParaIdx].TestInfo.GetCount())
		{
			return &CamInfo[nParaIdx].TestInfo.TestMeasList[nStepIdx];
		}
		else
		{
			return NULL;
		}
	};

	// 검사 항목 측정데이터 얻기
	ST_TestItemMeas* GetMeasurmentData(__in UINT nTestItem, __in UINT nParaIdx = 0)
	{
		return CamInfo[nParaIdx].TestInfo.GetMeasurmentData(nTestItem);
	};

	// 불량 판정된 검사 항목 측정데이터 얻기
	ST_TestItemMeas* GetMeasurmentData_Fail(__in UINT nTestItem, __in UINT nParaIdx, __out UINT& nIndex)
	{
		return CamInfo[nParaIdx].TestInfo.GetMeasurmentData(nTestItem);
	};

	// 검사 스텝정보 얻기
	ST_StepUnit* GetTestStep(__in UINT nStepIdx)
	{
		if (nStepIdx < RecipeInfo.StepInfo.GetCount())
		{
			return &RecipeInfo.StepInfo.StepList[nStepIdx];
		}
		else
		{
			return NULL;
		}
	};

	// 검사 시간 얻기
	ST_TestTime* GetTestTime(__in UINT nParaIdx = 0)
	{
		return &CamInfo[nParaIdx].TestTime;
	};

	// 검사 항목의 검사 시간 얻기
	DWORD	GetTestItemElapTime(__in UINT nTestItemID, __in UINT nParaIdx = 0)
	{
		return CamInfo[nParaIdx].TestTime.TestItem[nTestItemID].dwDuration;
	}

	// 측정 데이터 설정
	void Set_Measurement(__in UINT nParaIdx, __in UINT nStepIdx, __in UINT nMeasCount, __in COleVariant* pMeasurValue, __in DWORD dwDuration = 0)
	{
		CamInfo[nParaIdx].TestInfo.Set_Measurement(nStepIdx, nMeasCount, pMeasurValue, dwDuration);
	};

	// 측정 데이터 설정 (검사 결과 판정 처리 않함)
	void Set_Measurement_NoJudge(__in UINT nParaIdx, __in UINT nStepIdx, __in UINT nMeasCount, __in COleVariant* pMeasurValue, __in DWORD dwDuration = 0)
	{
		CamInfo[nParaIdx].TestInfo.Set_Measurement_NoJudge(nStepIdx, nMeasCount, pMeasurValue, dwDuration);
	};

	// 검사 결과 판정 처리
	void Set_Judgment(__in UINT nParaIdx, __in UINT nStepIdx, __in UINT nIN_Judgment)
	{
		CamInfo[nParaIdx].TestInfo.Set_Judgment(nStepIdx, nIN_Judgment);
	};

	// 진행 시간
	void Set_ElapTime(__in UINT nParaIdx, __in UINT nStepIdx, __in DWORD dwDuration = 0)
	{
		CamInfo[nParaIdx].TestInfo.Set_ElapTime(nStepIdx, dwDuration);
	};

	// Barcode 설정
	void Set_Barcode		(__in LPCTSTR szBarcode, __in UINT nRetryCnt = 0, __in UINT nParaIdx = 0)
	{
		CamInfo[nParaIdx].szBarcode			= szBarcode;
		CamInfo[nParaIdx].nMES_TryCnt		= nRetryCnt;
		CamInfo[nParaIdx].bMES_Validation	= TRUE;
	};

	// Current 측정 데이터
	void Set_CurrentMeas(__in long* lInCurrent, __in UINT nCount, __in UINT nParaIdx = 0)
	{
		CamInfo[nParaIdx].Set_CurrentMeas(lInCurrent, nCount);
	};

	void Set_ResultCode(__in LRESULT lResultCode, __in UINT nParaIdx = 0)
	{
		CamInfo[nParaIdx].ResultCode = lResultCode;
	};

	// 현재 설정된 제품 모델 형태 구하기
	enModelType Get_ModelType()
	{
		return RecipeInfo.ModelType;
	};

	// 설비/카메라 종류 별 
	UINT GetTestChannelCount()
	{
		UINT nCount = 1;

		switch (RecipeInfo.nInspectionType)
		{
		case Sys_2D_Cal:
		case Sys_3D_Cal:
		case Sys_Image_Test:
		case Sys_IR_Image_Test:
			nCount = g_IR_ModelTable[RecipeInfo.ModelType].Camera_Cnt;
			break;

		case Sys_Focusing:
			if (Model_OMS_Front == RecipeInfo.ModelType)
			{
				nCount = 1;
			}
			else
			{
				nCount = g_IR_ModelTable[RecipeInfo.ModelType].Camera_Cnt;
			}
			break;

		default:
			break;
		}

		return nCount;
	};

}ST_InspectionInfo, *PST_InspectionInfo;

//---------------------------------------------------------
// 영상 뷰 모드
//---------------------------------------------------------
typedef struct _tag_ImageMode
{
	enImageMode eImageMode;
	CString szImagePath;

	_tag_ImageMode()
	{
		eImageMode = ImageMode_LiveCam;
	};

	_tag_ImageMode& operator= (_tag_ImageMode& ref)
	{

		eImageMode = ref.eImageMode;
		szImagePath = ref.szImagePath;
		return *this;
	};
}ST_ImageMode, *PST_ImageMode;

#pragma pack (pop)

#endif // Def_DataStruct_h__
