﻿//*****************************************************************************
// Filename	: Def_Enum.h
// Created	: 2010/11/23
// Modified	: 2016/06/30
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef Def_Enum_h__
#define Def_Enum_h__

#include "Def_Enum_Cm.h"

//-------------------------------------------------------------------
// UI 관련
//-------------------------------------------------------------------

// 통신 Device
typedef enum 
{
	COMM_DEV_BASE		= 100,
	COMM_DEV_FTDI		= 200,
	COMM_DEV_MCTRL		= 300,
	COMM_DEV_LPRNT		= 400,
	COMM_DEV_LAST		= COMM_DEV_LPRNT,// 마지막으로 설정할것.
	COMM_DEV_MAX,
}enumCommStatus_Device;

typedef enum _ConstVar_Eqp
{
 	USE_CHANNEL_CNT			= 2,	// 장비에서 실제 사용하는 채널 수 (소켓, 사이트 등)	
};

typedef enum enLight_Loading
{
	Light_LD_Max,
};

typedef enum enLight_2D_CAL
{
	Light_2D_CAL_Left,
	Light_2D_CAL_Right,
	Light_2D_CAL_Center,
	Light_2D_CAL_Max,
};

typedef enum enLight_Particle
{
	Light_PT_Particle,
	Light_PT_Max,
};

typedef enum enLight_IQ
{
	Light_IQ_Max,
};

typedef enum enLight_3D_CAL
{
	Light_3D_CAL_Max,
};

typedef enum enImageMode
{
	ImageMode_LiveCam,
	ImageMode_StillShotImage,
	ImageMode_MaxNum,
};

typedef enum enFocus_AAView
{
	Foc_AA,
	Foc_AA_Torque,
	Foc_AA_MaxNum,
};


typedef enum enRegisterChange
{
	Reg_Chg_Normal,
	Reg_Chg_Dust,
	Reg_Chg_No,
	Reg_Chg_MaxNum,
};

typedef enum enTestMode
{
	TestMode_NGSkip,
	TestMode_Continue,
	TestMode_MaxNum
};

static LPCTSTR	g_szTestMode[] =
{
	_T("NG SKIP"),
	_T("CONTINUE"),
	NULL

};


#endif // Def_Enum_h__

