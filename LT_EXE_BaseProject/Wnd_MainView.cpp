﻿//*****************************************************************************
// Filename	: Wnd_MainView.cpp
// Created	: 2016/03/11
// Modified	: 2016/03/11
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
// Wnd_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MainView.h"
#include "resource.h"
#include "Dlg_ChkPassword.h"

#define IDW_STAGE_WND_BASE			1000
#define IDC_STATIC_RT_GRAPH			1001
#define IDC_RB_CAMERA_SEL_LEFT		1002
#define IDC_RB_CAMERA_SEL_RIGHT		1003

#define		IDC_BN_PREWORK		1004
#define		IDC_BN_MASTER		1005
//=============================================================================
// CWnd_MainView
//=============================================================================
IMPLEMENT_DYNAMIC(CWnd_MainView, CWnd_BaseView)

CWnd_MainView::CWnd_MainView()
{
	VERIFY(m_font_Default.CreateFont(
		24,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
	
	VERIFY(m_Font.CreateFont(
		18,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_BOLD,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename

	m_InspMode		= Permission_Operator;
	m_pstInspInfo	= NULL;
	m_pDevice		= NULL;
}

CWnd_MainView::~CWnd_MainView()
{
	TRACE(_T("<<< Start ~CWnd_MainView >>> \n"));
	
	m_font_Default.DeleteObject();
	m_Font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_MainView, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE	()
	//ON_BN_CLICKED(IDC_BN_PREWORK, OnBnClickedPreWork)
	ON_BN_CLICKED(IDC_RB_CAMERA_SEL_LEFT,	OnBnClickedRbCameraSelLeft)
	ON_BN_CLICKED(IDC_RB_CAMERA_SEL_RIGHT,	OnBnClickedRbCameraSelRight)
END_MESSAGE_MAP()


//=============================================================================
// CWnd_MainView 메시지 처리기입니다.
//=============================================================================
//=============================================================================
// Method		: CWnd_MainView::OnCreate
// Access		: protected 
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
int CWnd_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd_BaseView::OnCreate(lpCreateStruct) == -1)
		return -1;

	//SetBackgroundColor(RGB(0, 0, 0));

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	/*m_Font.CreateStockObject(DEFAULT_GUI_FONT);*/	
	UINT	nWndID = 10;
	
	// * 검사 최종 판정 ----------------------------------------------
	m_st_Judgment.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_Judgment.SetTextColor(Gdiplus::Color::Black, Gdiplus::Color::Black);
	//m_st_Judgment.SetSmoothMode(SmoothingMode::SmoothingModeAntiAlias);
	m_st_Judgment.SetTextRenderingHint(TextRenderingHint::TextRenderingHintAntiAliasGridFit);
	if (46 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
	{
		m_st_Judgment.SetFont_Gdip(L"Arial", 36.0F);
	}
	else if (40 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
	{
		m_st_Judgment.SetFont_Gdip(L"Arial", 54.0F);
	}
	else
	{
		m_st_Judgment.SetFont_Gdip(L"Arial", 72.0F);
	}
	m_st_Judgment.Create(_T("Judgment"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	// * 설비 구동 모드 ----------------------------------------------
	m_st_OperateMode.SetStaticStyle(CVGStatic::StaticStyle_Title);
	m_st_OperateMode.SetColorStyle(CVGStatic::ColorStyle_SkyBlue);
	m_st_OperateMode.SetTextColor(Gdiplus::Color::Black, Gdiplus::Color::Black);
	//m_st_OperateMode.SetSmoothMode(SmoothingMode::SmoothingModeAntiAlias);
	m_st_OperateMode.SetTextRenderingHint(TextRenderingHint::TextRenderingHintAntiAliasGridFit);

	if (46 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
	{
		m_st_OperateMode.SetFont_Gdip(L"Arial", 24.0F);
	}
	else if (40 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
	{
		m_st_OperateMode.SetFont_Gdip(L"Arial", 36.0F);
	}
	else
	{
		m_st_OperateMode.SetFont_Gdip(L"Arial", 48.0F);
	}

	m_st_OperateMode.Create(_T("Judgment"), WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	// * Alarm 상태 표시 ---------------------------------------------
	m_st_Alarm.Create(_T("Alarm"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);

	// * 파라별 검사 진행 상태 ---------------------------------------
	if (46 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
	{
		m_wnd_Progress.SetFontSizeLevel(enSizeLevel::SizeLv_Small);
	}
	else
	{
		m_wnd_Progress.SetFontSizeLevel(enSizeLevel::SizeLv_Medium);
	}
	m_wnd_Progress.SetOwner(GetParent());
	m_wnd_Progress.Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);

	// * 검사 항목별 결과값 표시 -------------------------------------
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		if (46 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
		{
			m_wnd_TestResult[nIdx].SetFontSize(8.0);
		}

		m_wnd_TestResult[nIdx].Set_TestItemCount(g_InspectorTable[m_InspectionType].MaxStep_Cnt);
		m_wnd_TestResult[nIdx].SetOwner(GetParent());
		m_wnd_TestResult[nIdx].Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);
	}
	m_wnd_TestResult[Para_Right].ShowWindow(SW_HIDE);

	for (int t = 0; t < USE_CHANNEL_CNT; t++)
	{
		m_wnd_TestResult_IR_Img[t].SetOwner(GetOwner());
		m_wnd_TestResult_IR_Img[t].Create(NULL, _T("Result IMGT"), dwStyle /*| WS_BORDER*/, rectDummy, this, 101 + t);
	}

	m_wnd_TestResult_IR_Img[Para_Left].ShowWindow(SW_SHOW);
	m_wnd_TestResult_IR_Img[Para_Right].ShowWindow(SW_HIDE);


	// * 검사 정보 표시 ----------------------------------------------
	m_wnd_InspInfo.SetOwner(GetParent());
	m_wnd_InspInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);

	// * 포고 개수 설정 ----------------------------------------------
	m_wnd_ConsumInfo.SetOwner(GetParent());
	m_wnd_ConsumInfo.Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);
	
	// * 탭 컨트롤 ---------------------------------------------------
	m_tc_Info.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, nWndID++, CMFCTabCtrl::LOCATION_BOTTOM);

	// * Barcode, Barcode 표시 --------------------------------------
	m_lc_Barcode.Set_UseParaInfo(FALSE);
	m_lc_Barcode.Create(WS_CHILD | WS_VISIBLE, rectDummy, &m_tc_Info, nWndID++);

	m_Wnd_Monitoring.SetOwner(GetParent());
	m_Wnd_Monitoring.Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);

	m_wnd_Torque.SetOwner(GetParent());
	m_wnd_Torque.Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);

	m_wnd_Graph.SetOwner(GetParent());
	m_wnd_Graph.Create(NULL, _T("Graph"), dwStyle , rectDummy, this, nWndID++);

	
	m_wnd_TimerView.SetOwner(GetParent());
	m_wnd_TimerView.Create(NULL, _T(""), dwStyle, rectDummy, this, nWndID++);

	// * Cycle Time -------------------------------------------------
	m_grid_CycleTime.SetUseSocketCount(g_InspectorTable[m_InspectionType].Grabber_Cnt);
	if (!m_grid_CycleTime.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, &m_tc_Info, nWndID++))
	{
		TRACE("m_grid_CycleTime 출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	// * Yield ------------------------------------------------------
	m_grid_Yield.SetUseSocketCount(g_InspectorTable[m_InspectionType].Grabber_Cnt);
	if (!m_grid_Yield.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, &m_tc_Info, nWndID++))
	{
		TRACE("m_grid_Yield 출력 창을 만들지 못했습니다.\n");
		return -1;
	}

	// * Yield Test Item --------------------------------------------
	if (!m_grid_YieldTest.CreateGrid(WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS, rectDummy, &m_tc_Info, nWndID++))
	{
		TRACE("m_grid_YieldTest 출력 창을 만들지 못했습니다.\n");
		return -1;
	}
	m_grid_YieldTest.SetTestItemInfo(&m_pstInspInfo->RecipeInfo.TestItemInfo);

	// * 검사 결과 정보	---------------------------------------------
	m_tc_Info.AddTab(&m_lc_Barcode,		_T("Barcode"),		0, FALSE);
 	//m_tc_Info.AddTab(&m_grid_CycleTime,	_T("C/T"),			1, FALSE);
 	//m_tc_Info.AddTab(&m_grid_Yield,		_T("Yield"),		2, FALSE);
	//m_tc_Info.AddTab(&m_grid_YieldTest,	_T("Yield Test"),	3, FALSE);
	m_tc_Info.SetActiveTab(0);
	m_tc_Info.EnableTabSwap(FALSE);

	// * 영상 뷰 ----------------------------------------------------
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_ImageLive[nIdx].Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);
		
		// * 이미지 히스토리 탭 컨트롤
		//m_tc_ImageCapture[nIdx].Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, nWndID++, CMFCTabCtrl::LOCATION_BOTTOM);
		m_tc_ImageCapture[nIdx].Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);
	}

	m_wnd_ImageLive[Para_Right].ShowWindow(SW_HIDE);
	m_tc_ImageCapture[Para_Right].ShowWindow(SW_HIDE);
	  

	m_wnd_ImageLive_Vision.Create(NULL, _T(""), dwStyle | WS_BORDER, rectDummy, this, nWndID++);

	// * 좌/우 카메라 선택 -------------------------------------------
// 	m_rb_CameraSelect[Para_Left].Create(_T("Left Camera"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON | WS_GROUP, rectDummy, this, IDC_RB_CAMERA_SEL_LEFT);
// 	m_rb_CameraSelect[Para_Right].Create(_T("Right Camera"), WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_AUTORADIOBUTTON, rectDummy, this, IDC_RB_CAMERA_SEL_RIGHT);
// 
// 	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
// 	{
// 		m_rb_CameraSelect[nIdx].SetFont(&m_Font);
// 		m_rb_CameraSelect[nIdx].m_nFlatStyle = CMFCButton::BUTTONSTYLE_SEMIFLAT;
// 		m_rb_CameraSelect[nIdx].SetImage(IDB_SELECTNO_16);
// 		m_rb_CameraSelect[nIdx].SetCheckedImage(IDB_SELECT_16);
// 		m_rb_CameraSelect[nIdx].SizeToContent();
// 		m_rb_CameraSelect[nIdx].SetCheck(TRUE);
// 	}

	// * 가이드 라인	 ------------------------------------------------
	m_st_frameZone.SetColorStyle(CVGStatic::ColorStyle_Black);
	m_st_frameZone.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);
	m_st_frameSatus.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);
	m_st_frameInfo.Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE | SS_BLACKFRAME, rectDummy, this, IDC_STATIC);

	SetJudgmentStatus(enTestResult::TR_Ready);
	Set_CameraSelect(Para_Left);

	return 0;
}

//=============================================================================
// Method		: CWnd_MainView::OnSize
// Access		: protected 
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2010/11/26 - 14:25
// Desc.		:
//=============================================================================
void CWnd_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd_BaseView::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMagrin			= 5;
	int iSpacing		= 5;
	int iCateSpacing	= 5;

	int iLeft		= iMagrin;
	int iTop		= iMagrin;
	int iWidth		= cx - iMagrin - iMagrin;
	int iHeight		= cy - iMagrin - iMagrin;

	int iInfoWidth	= 280;

	//int iHalfWidth	= (iWidth - iCateSpacing) * 75 / 100;	
	int iHalfWidth	= iWidth - iCateSpacing - iInfoWidth;
	int iHalfHeight = 0;

// 	if (50 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
// 	{
// 		iHalfHeight = (iHeight - iCateSpacing) * 1 / 10;		// 검사 스텝 50개 이상일 경우
// 	}
// 	else if (40 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
// 	{
// 		iHalfHeight = (iHeight - iCateSpacing) * 1 / 9;		// 검사 스텝 46개 이상일 경우
// 	}
// 	else if (36 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
// 	{
// 		iHalfHeight = (iHeight - iCateSpacing) * 1 / 8;		// 검사 스텝 36개 이상일 경우
// 	}
// 	else if (30 <= g_InspectorTable[m_InspectionType].MaxStep_Cnt)
// 	{
		iHalfHeight = (iHeight - iCateSpacing) * 1 / 6;		// 검사 스텝 30개 이상일 경우
// 	}
// 	else
// 	{
// 		iHalfHeight = (iHeight - iCateSpacing) * 1 / 6;	// 기본
// 	}

	int iTempHeight = iHeight - iCateSpacing - iHalfHeight;
	int iTempWidth	= iWidth - iCateSpacing - iHalfWidth;
	int iImageWidth = 640 + 2; // 640 * 1.25

	m_wnd_InspInfo.Set_ParaCount(g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt);
	m_wnd_ConsumInfo.SetItemCount(g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt);

	MoveWindow_Image(iLeft, iTop, iImageWidth, iHeight);

	iLeft += iImageWidth + iCateSpacing;
	iHalfWidth = iWidth - iLeft - iCateSpacing - iInfoWidth;
	MoveWindow_Status(iLeft, iTop, iHalfWidth, iHalfHeight);

	iTop += iHalfHeight + iCateSpacing;
	MoveWindow_Monitoring(iLeft, iTop, iHalfWidth, iTempHeight * 3 / 5);

	iTop += iTempHeight * 3 / 5 + iSpacing;
	m_wnd_TestResult_IR_Img[Para_Left].MoveWindow(iLeft, iTop, iHalfWidth, iTempHeight * 2 / 5);

	iLeft += iHalfWidth + iCateSpacing;
	iTop = iMagrin;
	MoveWindow_Info_IR_ImgT(iLeft, iTop, iTempWidth, iHeight);
}

//=============================================================================
// Method		: OnBnClickedRbCameraSelLeft
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/12 - 21:04
// Desc.		:
//=============================================================================
void CWnd_MainView::OnBnClickedRbCameraSelLeft()
{
	Set_CameraSelect(Para_Left);
}

void CWnd_MainView::OnBnClickedRbCameraSelRight()
{
	Set_CameraSelect(Para_Right);
}

//=============================================================================
// Method		: MoveWindow_Monitoring
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2016/5/17 - 11:32
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Monitoring(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing		= 5;
	int iLeft			= x;
	int iTop			= y;
	int iAlarmHeight	= 50;
	int iTempHeight		= nHeight - iSpacing - iAlarmHeight;
	
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_TestResult[nIdx].MoveWindow(iLeft, iTop, nWidth, iTempHeight);
	}

	iTop += iTempHeight + iSpacing;
	m_st_Alarm.MoveWindow(iLeft, iTop, nWidth, iAlarmHeight);


}

//=============================================================================
// Method		: MoveWindow_Info
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2016/5/17 - 11:32
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Info(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing	= 5;
	int iLeft		= x;
	int iTop		= y;
	int iBnWidth	= (nWidth - iSpacing) / 2;
	int iBnHeight	= 30;
	int iHalfHeight = (nHeight - iBnHeight - (iSpacing * 5)) * 3 / 9;
	int iTempHeight = (nHeight - (iSpacing * 6)) / 9;

	int nW = (nWidth - iSpacing) / 2;
	int nH = iTempHeight /2;

	// 카메라 선택 버튼
	//m_rb_CameraSelect[Para_Left].MoveWindow(iLeft, iTop, iBnWidth, iBnHeight);
	//m_rb_CameraSelect[Para_Right].MoveWindow(iLeft + iBnWidth + iSpacing, iTop, iBnWidth, iBnHeight);
	//iTop += iBnHeight + iSpacing;

	// 검사 정보
	m_wnd_InspInfo.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);

	iTop += iHalfHeight + iSpacing;
	
	m_Wnd_Monitoring.MoveWindow(iLeft, iTop, nWidth, iTempHeight);

	iTop += iTempHeight + iSpacing;
	m_wnd_ConsumInfo.MoveWindow(iLeft, iTop, nWidth, iTempHeight);

	iTop += iTempHeight + iSpacing;
//	m_bn_PreWork.MoveWindow(iLeft, iTop, nWidth, nH);
//	m_bn_MasterMode.MoveWindow(iLeft + nW + iSpacing, iTop, nW, nH);

//	iTop += nH + iSpacing;
	iHalfHeight = nHeight - iTop;
	m_tc_Info.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
	//m_st_frameInfo.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
}

void CWnd_MainView::MoveWindow_Info_SteCal(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing = 5;
	int iLeft = x;
	int iTop = y;
	int iHalfHeight = (nHeight - (iSpacing * 4)) * 3 / 9;
	int iTempHeight = (nHeight - (iSpacing * 6)) / 9;

	int nW = (nWidth - iSpacing) / 2;
	int nH = iTempHeight / 2;

	// 카메라 선택 버튼
// 	m_rb_CameraSelect[Para_Left].MoveWindow(0, 0, 0, 0);
// 	m_rb_CameraSelect[Para_Right].MoveWindow(0, 0, 0, 0);

	// 검사 정보
	m_wnd_InspInfo.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);

	iTop += iHalfHeight + iSpacing;

	m_Wnd_Monitoring.MoveWindow(iLeft, iTop, nWidth, iTempHeight);

	iTop += iTempHeight + iSpacing;
	m_wnd_ConsumInfo.MoveWindow(iLeft, iTop, nWidth, iTempHeight);

	iTop += iTempHeight + iSpacing;
	//	m_bn_PreWork.MoveWindow(iLeft, iTop, nWidth, nH);
	//	m_bn_MasterMode.MoveWindow(iLeft + nW + iSpacing, iTop, nW, nH);

	//	iTop += nH + iSpacing;
	iHalfHeight = nHeight - iTop;
	m_tc_Info.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
	//m_st_frameInfo.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
}

//=============================================================================
// Method		: MoveWindow_Info_Foc
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/3/10 - 14:27
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Info_Foc(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing	= 5;
	int iLeft		= x;
	int iTop		= y;
	int iBnWidth	= (nWidth - iSpacing) / 2;
	int iBnHeight	= 30;
	int iHalfHeight = (nHeight - iBnHeight - (iSpacing * 4)) * 2 / 5;
	int iTempHeight = (nHeight - iBnHeight - (iSpacing * 4)) / 5;

	int nW = (nWidth - iSpacing) / 2;
	int nH = iTempHeight / 2;

	// 카메라 선택 버튼
// 	m_rb_CameraSelect[Para_Left].MoveWindow(iLeft, iTop, iBnWidth, iBnHeight);
// 	m_rb_CameraSelect[Para_Right].MoveWindow(iLeft + iBnWidth + iSpacing, iTop, iBnWidth, iBnHeight);
// 	iTop += iBnHeight + iSpacing;

	// 검사 정보
	m_wnd_InspInfo.SetUseButton(FALSE);
	m_wnd_InspInfo.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);

// 	iTop += iHalfHeight + iSpacing;
// 	m_wnd_Torque.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);

	iTop += iHalfHeight + iSpacing;
	m_wnd_ConsumInfo.MoveWindow(iLeft, iTop, nWidth, iTempHeight);

	iTop += iTempHeight + iSpacing;
	iHalfHeight = nHeight - iTop;
	m_tc_Info.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
	//m_st_frameInfo.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
}

//=============================================================================
// Method		: MoveWindow_Info_ImgT
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/3/16 - 9:56
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Info_ImgT(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing	= 5;
	int iLeft		= x;
	int iTop		= y;
	int iBnWidth	= (nWidth - iSpacing) / 2;
	int iBnHeight	= 30;
	int iHalfHeight = (nHeight - iBnHeight - (iSpacing * 4)) * 2 / 5;
	int iTempHeight = ((nHeight - iHalfHeight) - (iSpacing * 5)) / 11;
	int iWndHeight	= iTempHeight * 7;

	// 카메라 선택 버튼
// 	m_rb_CameraSelect[Para_Left].MoveWindow(iLeft, iTop, iBnWidth, iBnHeight);
// 	m_rb_CameraSelect[Para_Right].MoveWindow(iLeft + iBnWidth + iSpacing, iTop, iBnWidth, iBnHeight);
// 	iTop += iBnHeight + iSpacing;

	// 검사 정보
	m_wnd_InspInfo.MoveWindow(iLeft, iTop, nWidth, iWndHeight);

	iTop += iWndHeight + iSpacing;
	iWndHeight = iTempHeight;
	m_Wnd_Monitoring.MoveWindow(iLeft, iTop, nWidth, iWndHeight);

	iTop += iWndHeight + iSpacing;
	iWndHeight = iTempHeight * 3;
	m_wnd_ConsumInfo.MoveWindow(iLeft, iTop, nWidth, iWndHeight);

	iTop += iWndHeight + iSpacing;
	iHalfHeight = nHeight - iTop;
	m_tc_Info.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
	//m_st_frameInfo.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
}

//=============================================================================
// Method		: MoveWindow_Info_IR_ImgT
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/8/5 - 14:06
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Info_IR_ImgT(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing	= 5;
	int iLeft		= x;
	int iTop		= y;
	int iBnWidth	= (nWidth - iSpacing) / 2;
	int iBnHeight	= 30;
	int iHalfHeight = (nHeight - iBnHeight - (iSpacing * 4)) * 2 / 5;
	int iTempHeight = ((nHeight - iHalfHeight) - (iSpacing * 5)) / 11;
	int iWndHeight	= iTempHeight * 7;

	// 카메라 선택 버튼
// 	m_rb_CameraSelect[Para_Left].MoveWindow(iLeft, iTop, iBnWidth, iBnHeight);
// 	m_rb_CameraSelect[Para_Right].MoveWindow(iLeft + iBnWidth + iSpacing, iTop, iBnWidth, iBnHeight);
// 	iTop += iBnHeight + iSpacing;

	// 검사 정보
	m_wnd_InspInfo.SetUseButton(FALSE);
	m_wnd_InspInfo.MoveWindow(iLeft, iTop, nWidth, iWndHeight);

// 	iTop += iWndHeight + iSpacing;
// 	iWndHeight = iTempHeight;
// 	m_Wnd_Monitoring.MoveWindow(iLeft, iTop, nWidth, iWndHeight);

	iTop += iWndHeight + iSpacing;
	iWndHeight = iTempHeight * 3;
	m_wnd_ConsumInfo.MoveWindow(iLeft, iTop, nWidth, iWndHeight);

	iTop += iWndHeight + iSpacing;
	iHalfHeight = nHeight - iTop;
	m_tc_Info.MoveWindow(iLeft, iTop, nWidth, iHalfHeight);
}

//=============================================================================
// Method		: MoveWindow_Status
// Access		: protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2016/5/17 - 11:32
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Status(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing = 5;

	int iLeft = x;
	int iTop = y;
	//int iTempHeight = (nHeight - iSpacing) * 3 / 4;
	int iTempHeight = nHeight;

 	m_st_Judgment.MoveWindow(iLeft, iTop, nWidth, iTempHeight);
// 	m_st_OperateMode.MoveWindow(iLeft, iTop, nWidth, iTempHeight);
// 
// 	iTop += iTempHeight + iSpacing;
// 	iTempHeight = nHeight - iSpacing - iTempHeight;
// 	m_wnd_Progress.MoveWindow(iLeft, iTop, nWidth, iTempHeight);


	//m_st_ProgressInfo.MoveWindow(iLeft, iTop, nWidth, iTempHeight);
	
	//m_st_frameSatus.MoveWindow(iLeft, iTop, nWidth, nHeight);	
}

//=============================================================================
// Method		: MoveWindow_Image
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/1/16 - 17:01
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Image(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		MoveWindow_Image_2DCal(x, y, nWidth, nHeight, bRepaint);
		break;
	
	case Sys_Focusing:
		MoveWindow_Image_Foc(x, y, nWidth, nHeight, bRepaint);
		break;
	
	case Sys_IR_Image_Test:
		MoveWindow_Image_IR_ImgT(x, y, nWidth, nHeight, bRepaint);
		break;

	case Sys_Image_Test:
		MoveWindow_Image_ImgT(x, y, nWidth, nHeight, bRepaint);
		break;

	case Sys_3D_Cal:
	default:
		MoveWindow_Image_2DCal(x, y, nWidth, nHeight, bRepaint);
		break;
	}
}

//=============================================================================
// Method		: MoveWindow_Image_2DCal
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/2/14 - 15:44
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Image_2DCal(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing	= 5;
	int iLeft		= x;
	int iTop		= y;
	int iCtrlHeight = 38;
	int iCtrlWidth	= nWidth / 3;

	// OMS Entry / OMS Front
	int iCamCount = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt;
	int iCamWidth = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Width;
	int iCamHeight = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Height;

	//int iImgWidth		= 640;
	//int iImgHeight	= 480;
	int iImgWidth	= nWidth;
	int iImgHeight	= iImgWidth * iCamHeight / iCamWidth;

	// Live 영상
	m_wnd_ImageLive[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);

	if (1 < iCamCount)
	{
		m_wnd_ImageLive[Para_Right].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
	}
	else
	{
		m_wnd_ImageLive[Para_Right].MoveWindow(0, 0, 0, 0);
	}

	iTop += iImgHeight + iSpacing;

	// 캡쳐 이미지 이력
	iLeft = x;
	int iHeight = y + nHeight - iTop;

	m_tc_ImageCapture[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iHeight);

	if (1 < iCamCount)
	{
		m_tc_ImageCapture[Para_Right].MoveWindow(iLeft, iTop, iImgWidth, iHeight);
	}
	else
	{
		m_tc_ImageCapture[Para_Right].MoveWindow(0, 0, 0, 0);
	}
}

//=============================================================================
// Method		: MoveWindow_Image_SteCal
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/2/20 - 12:05
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Image_SteCal(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing	= 5;
	int iLeft		= x;
	int iTop		= y;
	int iCtrlHeight = 30;
	int iCtrlWidth	= nWidth / 3;

	// MRA2 / IKC
	int iCamCount = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt;
	int iCamWidth = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Width;
	int iCamHeight = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Height;	// 1280x964, 1280x724
	if ((Model_IKC == m_pstInspInfo->Get_ModelType()) || (Model_MRA2 == m_pstInspInfo->Get_ModelType()))
	{
		iCamHeight = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Height + 4;
	}

	if (1 < iCamCount)
	{
		int iCtrlHeight = (nHeight - iSpacing) / 2;
		int iCtrlWidth = nWidth;

		// Live 영상
		m_wnd_ImageLive[Para_Left].MoveWindow(0, 0, 0, 0);
		m_wnd_ImageLive[Para_Right].MoveWindow(0, 0, 0, 0);
		
		// 캡쳐 이미지 이력
		m_tc_ImageCapture[Para_Left].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);

		iTop += iCtrlHeight + iSpacing;
		m_tc_ImageCapture[Para_Right].MoveWindow(iLeft, iTop, iCtrlWidth, iCtrlHeight);
		
	}
	else // 카메라 1개
	{
		int iImgWidth = nWidth;
		int iImgHeight = iImgWidth * iCamHeight / iCamWidth;

		// Live 영상
		m_wnd_ImageLive[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
		m_wnd_ImageLive[Para_Right].MoveWindow(0, 0, 0, 0);

		iTop += iImgHeight + iSpacing;
		
		// 캡쳐 이미지 이력
		iLeft = x;
		int iHeight = y + nHeight - iTop;

		m_tc_ImageCapture[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iHeight);
		m_tc_ImageCapture[Para_Right].MoveWindow(0, 0, 0, 0);
	}
}

//=============================================================================
// Method		: MoveWindow_Image_Foc
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/2/20 - 12:05
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Image_Foc(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
// 	int iSpacing	= 5;
// 	int iLeft		= x;
// 	int iTop		= y;
// 	int iCtrlHeight = 38;
// 	int iCtrlWidth	= nWidth / 3;
// 
// 	// OMS Entry / OMS Front
// 	int iCamCount	= g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt;
// 	int iCamWidth	= g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Width;
// 	int iCamHeight	= g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Height;
// 	int iImgWidth	= nWidth;
// 
// 	//int iImgWidth		= 640;
// 	//int iImgHeight	= 480;
// 
// 	int iHeightTemp	= 230;
// 	int iImgHeight	= (iImgWidth - 190) * iCamHeight / iCamWidth;
// 
// // 	int iHeightTemp	= 0;
// // 	int iImgHeight = (iImgWidth)* iCamHeight / iCamWidth;
// 
// 
// 	// 그래프
// 	m_wnd_Graph.MoveWindow(iLeft, iTop, iImgWidth, iHeightTemp);
// 	
// 	// Live 영상
// 	iTop += iHeightTemp + iSpacing;
// 	m_wnd_ImageLive[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
// 
// 	if (1 < iCamCount)
// 	{
// 		m_wnd_ImageLive[Para_Right].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
// 	}
// 	else
// 	{
// 		m_wnd_ImageLive[Para_Right].MoveWindow(0, 0, 0, 0);
// 	}
// 
// 	iTop += iImgHeight + iSpacing;
// 
// 	// 캡쳐 결과 이력
// 	iLeft = x;
// 	int iHeight = y + nHeight - iTop;
// 
// 	m_wnd_TimerView.MoveWindow(iLeft, iTop, iImgWidth, iHeight);
// 	m_wnd_TestResult_Foc[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iHeight);
// 
// 	if (1 < iCamCount)
// 	{
// 		m_wnd_TestResult_Foc[Para_Right].MoveWindow(iLeft, iTop, iImgWidth, iHeight);
// 	}
// 	else
// 	{
// 		m_wnd_TestResult_Foc[Para_Right].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_ImgT[Para_Left].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_ImgT[Para_Right].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_IR_Img[Para_Left].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_IR_Img[Para_Right].MoveWindow(0, 0, 0, 0);
// 	}
}

//=============================================================================
// Method		: MoveWindow_Image_ImgT
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/2/20 - 12:05
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Image_ImgT(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
// 	int iSpacing	= 5;
// 	int iLeft		= x;
// 	int iTop		= y;
// 	int iCtrlHeight = 38;
// 	int iCtrlWidth	= nWidth / 3;
// 
// 	// OMS Entry / OMS Front
// 	int iCamCount = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt;
// 	int iCamWidth = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Width;
// 	int iCamHeight = g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Height;
// 
// 	//int iImgWidth		= 640;
// 	//int iImgHeight	= 480;
// 	int iImgWidth = nWidth;
// 	if (nWidth%2==1)
// 	{
// 		iImgWidth = nWidth - 1;
// 	}
// 	
// 	int iImgHeight = iImgWidth * iCamHeight / iCamWidth;
// 
// 	// Live 영상
// 	m_wnd_ImageLive[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
// 
// 	if (1 < iCamCount)
// 	{
// 		m_wnd_ImageLive[Para_Right].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);
// 	}
// 	else
// 	{
// 		m_wnd_ImageLive[Para_Right].MoveWindow(0, 0, 0, 0);
// 	}
// 
// 
// 	iTop += iImgHeight + iSpacing;
// 
// 	// 캡쳐 결과 이력
// 	iLeft = x;
// 	int iHeight = (y + nHeight - iTop)/2;
// 
// 	m_wnd_TestResult_ImgT[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, 300);
// 
// 	if (1 < iCamCount)
// 	{
// 		m_wnd_TestResult_ImgT[Para_Right].MoveWindow(iLeft, iTop, iImgWidth, 300);
// 	}
// 	else
// 	{
// 		m_wnd_TestResult_ImgT[Para_Right].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_Foc[Para_Left].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_Foc[Para_Right].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_IR_Img[Para_Left].MoveWindow(0, 0, 0, 0);
// 		m_wnd_TestResult_IR_Img[Para_Right].MoveWindow(0, 0, 0, 0);
// 	}
// 
// 	iTop += iHeight;
}

//=============================================================================
// Method		: MoveWindow_Image_IR_ImgT
// Access		: virtual protected  
// Returns		: void
// Parameter	: int x
// Parameter	: int y
// Parameter	: int nWidth
// Parameter	: int nHeight
// Parameter	: BOOL bRepaint
// Qualifier	:
// Last Update	: 2018/8/5 - 14:06
// Desc.		:
//=============================================================================
void CWnd_MainView::MoveWindow_Image_IR_ImgT(int x, int y, int nWidth, int nHeight, BOOL bRepaint /*= TRUE*/)
{
	int iSpacing	= 5;
	int iLeft		= x;
	int iTop		= y;
	int iCtrlHeight = 38;
	int iCtrlWidth	= nWidth / 3;

	int iCamCount	= g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt;
	int iCamWidth	= g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Width;
	int iCamHeight	= g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Img_Height;
	int iImgWidth	= nWidth;

	int iHeightTemp = 187;
	int iImgHeight	= (iImgWidth - 100) * iCamHeight / iCamWidth;

	// Live 영상
	//iTop += iHeightTemp + iSpacing;
	m_wnd_ImageLive[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);

	iTop += iImgHeight + iSpacing;
	// 비젼 이미지
	m_wnd_ImageLive_Vision.MoveWindow(iLeft, iTop, iImgWidth, iImgHeight);

	// 캡쳐 결과 이력
	iLeft = x;
	//int iHeight = y + nHeight - iTop;
	//m_wnd_TestResult_IR_Img[Para_Left].MoveWindow(iLeft, iTop, iImgWidth, iHeight);
}

//=============================================================================
// Method		: CheckOtherParaStatus
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/9/25 - 23:55
// Desc.		:
//=============================================================================
// BOOL CWnd_MainView::CheckOtherParaStatus()
// {
// 	BOOL bProc = TRUE;
// 	for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
// 	{
// 		// TP_Testing, TP_WaitPLCOut, TP_Unloading 일때는 Ready 표시 안함
// 		switch (m_pstInspInfo->CamInfo[nIdx].nProgressStatus)
// 		{
// 		case TP_Ready:
// 		case TP_Loading:
// 			break;
// 
// 		default:
// 			bProc = FALSE;
// 			break;
// 		}
// 	}
// 
// 	// 양쪽 파라가 모두 대기 상태일때
// 	if (bProc)
// 	{
// 		SetJudgmentStatus(TR_Ready);
// 	}
// 
// 	return bProc;
// }

//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/21 - 21:10
// Desc.		:
//=============================================================================
void CWnd_MainView::SetSystemType(__in enInsptrSysType nSysType)
{
	m_InspectionType = nSysType;
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_TestResult[nIdx].SetSystemType(m_InspectionType);
		m_wnd_TestResult[nIdx].Set_TestItemCount(g_InspectorTable[m_InspectionType].MaxStep_Cnt);
	}
 	m_wnd_InspInfo.SetSystemType(m_InspectionType);
// 	m_wnd_Progress.SetSystemType(m_InspectionType);
// 	m_wnd_Progress.SetParaCount(g_InspectorTable[m_InspectionType].Grabber_Cnt);
	
	m_Wnd_Monitoring.SetSystemType(m_InspectionType);

	m_wnd_ConsumInfo.SetItemCount(g_InspectorTable[m_InspectionType].CtrlBrd_Cnt);
 
//  	if (2 <= g_InspectorTable[m_InspectionType].Grabber_Cnt)
//  		m_lc_Barcode.Set_UseParaInfo(TRUE);
//  	else
//  		m_lc_Barcode.Set_UseParaInfo(FALSE);

	if (Sys_Focusing == m_InspectionType)
	{
		m_wnd_Torque.SetItemCount(g_IR_ModelTable[m_pstInspInfo->RecipeInfo.ModelType].Camera_Cnt);
	}
}

//=============================================================================
// Method		: SetModelType
// Access		: public  
// Returns		: void
// Parameter	: __in enModelType nModelType
// Qualifier	:
// Last Update	: 2018/2/9 - 18:58
// Desc.		:
//=============================================================================
void CWnd_MainView::SetModelType(__in enModelType nModelType)
{
	UINT nViewCount = 1;

	switch (nModelType)
	{
	case Model_OMS_Entry:
		nViewCount = 1;
		break;

	case Model_OMS_Front:
		if (Sys_Focusing == m_InspectionType)
		{
			nViewCount = 1;
		}
		else
		{
			nViewCount = 2;
		}
		break;

	case Model_OMS_Front_Set:
		nViewCount = 2;

	case Model_MRA2:
		nViewCount = 2;
		break;

	case Model_IKC:
		nViewCount = 1;
		break;

	default:
		nViewCount = 1;
		break;
	}

	if (Sys_Focusing == m_InspectionType)
	{
		m_wnd_Torque.SetItemCount(nViewCount);
	}

	m_wnd_Progress.SetParaCount(nViewCount);
	if (((Model_IKC == m_pstInspInfo->Get_ModelType()) || (Model_MRA2 == m_pstInspInfo->Get_ModelType())))
	{
		m_tc_ImageCapture[Para_Left].Set_ImageSize(g_IR_ModelTable[nModelType].Img_Width, g_IR_ModelTable[nModelType].Img_Height + 4);
		m_tc_ImageCapture[Para_Right].Set_ImageSize(g_IR_ModelTable[nModelType].Img_Width, g_IR_ModelTable[nModelType].Img_Height + 4);
	}
	else
	{
		m_tc_ImageCapture[Para_Left].Set_ImageSize(g_IR_ModelTable[nModelType].Img_Width, g_IR_ModelTable[nModelType].Img_Height);
		m_tc_ImageCapture[Para_Right].Set_ImageSize(g_IR_ModelTable[nModelType].Img_Width, g_IR_ModelTable[nModelType].Img_Height);
	}

	for (UINT nIdx = 0; nIdx < nViewCount; nIdx++)
	{
		m_wnd_ImageLive[nIdx].ShowWindow(SW_SHOW);
		m_tc_ImageCapture[nIdx].ShowWindow(SW_SHOW);
	}
	for (UINT nIdx = nViewCount; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_ImageLive[nIdx].ShowWindow(SW_HIDE);
		m_tc_ImageCapture[nIdx].ShowWindow(SW_HIDE);
	}

	// 영상이 2개 이상이면
	if (1 < nViewCount)
	{
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
		//	m_rb_CameraSelect[nIdx].EnableWindow(TRUE);
			//m_wnd_TestResult_ImgT[nIdx].ShowWindow(SW_SHOW);
			m_wnd_TestResult_IR_Img[nIdx].ShowWindow(SW_SHOW);
			//m_wnd_TestResult_Foc[nIdx].ShowWindow(SW_SHOW);
		}

	}
	else // 영상이 1개
	{
		for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
		{
		//	m_rb_CameraSelect[nIdx].EnableWindow(FALSE);
		//	m_wnd_TestResult_ImgT[nIdx].ShowWindow(SW_HIDE);
			m_wnd_TestResult_IR_Img[nIdx].ShowWindow(SW_HIDE);
		//	m_wnd_TestResult_Foc[nIdx].ShowWindow(SW_HIDE);
		}
	}

	Set_CameraSelect(Para_Left);

	if (GetSafeHwnd())
	{
		CRect rc;
		GetClientRect(rc);
		OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
		//SendMessage(WM_SIZE, (WPARAM)SIZE_RESTORED, MAKELPARAM(rc.Width(), rc.Height()));
	}
}

//=============================================================================
// Method		: SetJudgmentStatus
// Access		: protected  
// Returns		: void
// Parameter	: __in enTestResult nJudgment
// Qualifier	:
// Last Update	: 2017/9/19 - 13:40
// Desc.		:
//=============================================================================
void CWnd_MainView::SetJudgmentStatus(__in enTestResult nJudgment)
{
	m_st_Judgment.SetText(g_TestResult[nJudgment].szText);

	switch (nJudgment)
	{
	case TR_Fail:
		m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Judgment.SetBorderColor(Gdiplus::Color::Black);
		m_st_Judgment.SetTextColor(Gdiplus::Color::Black, Gdiplus::Color::Black);		
		break;

	case TR_Pass:
		m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_Judgment.SetBorderColor(Gdiplus::Color::Black);
		m_st_Judgment.SetTextColor(Gdiplus::Color::Black, Gdiplus::Color::Black);
		break;

	case TR_Check:
		m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_Yellow);
		m_st_Judgment.SetBorderColor(Gdiplus::Color::Black);
		m_st_Judgment.SetTextColor(Gdiplus::Color::Black, Gdiplus::Color::Black);
		break;

	case TR_Stop:
		m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_Red);
		m_st_Judgment.SetBorderColor(Gdiplus::Color::Black);
		m_st_Judgment.SetTextColor(Gdiplus::Color::Black, Gdiplus::Color::White);
		break;

	case TR_Ready:
		m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_White);
		m_st_Judgment.SetBorderColor(Gdiplus::Color::Black);
		m_st_Judgment.SetTextColor(Gdiplus::Color::Green, Gdiplus::Color::Green);
		break;

	case TR_Testing:
		m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_Yellow);
		m_st_Judgment.SetBorderColor(Gdiplus::Color::Black);
		m_st_Judgment.SetTextColor(Gdiplus::Color::Red, Gdiplus::Color::Red);
		break;
	
	case TR_Empty:
		break;
	case TR_Skip:
		break;
	case TR_Rework:
		break;
	case TR_Timeout:
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetProgressStatus
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTotalStep
// Parameter	: __in UINT nProgStep
// Qualifier	:
// Last Update	: 2018/6/3 - 19:48
// Desc.		:
//=============================================================================
void CWnd_MainView::SetProgressStatus(__in UINT nTotalStep, __in UINT nProgStep)
{
	m_wnd_Progress.SetProgress(nTotalStep, nProgStep);
}

//=============================================================================
// Method		: SetParaStatus
// Access		: public  
// Returns		: void
// Parameter	: __in enTestProcess nProgress
// Parameter	: __in UINT nParaIndex
// Qualifier	:
// Last Update	: 2018/6/3 - 19:48
// Desc.		:
//=============================================================================
void CWnd_MainView::SetParaStatus(__in enTestProcess nProgress, __in UINT nParaIndex /*= 0*/)
{
	m_wnd_Progress.SetParaStatus(nProgress, nParaIndex);
}

//=============================================================================
// Method		: SetBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in UINT nRetryCnt
// Parameter	: __in UINT nParaIndex
// Qualifier	:
// Last Update	: 2018/6/3 - 19:48
// Desc.		:
//=============================================================================
void CWnd_MainView::SetBarcode(__in LPCTSTR szBarcode, __in UINT nRetryCnt /*= 0*/, __in UINT nParaIndex /*= 0*/)
{
	CString str_BarCode;
	str_BarCode.Format(_T("%s"), szBarcode);

	CString strTemp16 = str_BarCode.Right(16);

	m_wnd_Progress.SetBarcode(strTemp16, nParaIndex);
	m_wnd_InspInfo.Set_Barcode(strTemp16, nParaIndex);

	CString szLotCnt;
	szLotCnt.Format(_T("%d"), nRetryCnt);
	m_wnd_InspInfo.Set_LotCnt(szLotCnt, nParaIndex);
}

//=============================================================================
// Method		: SetParaInputTime
// Access		: public  
// Returns		: void
// Parameter	: __in PSYSTEMTIME pstTime
// Parameter	: __in UINT nParaIndex
// Qualifier	:
// Last Update	: 2018/6/3 - 19:48
// Desc.		:
//=============================================================================
void CWnd_MainView::SetParaInputTime(__in PSYSTEMTIME pstTime, __in UINT nParaIndex /*= 0*/)
{
	m_wnd_Progress.SetParaInputTime(pstTime, nParaIndex);
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: enum_Inspection_Mode InspMode
// Qualifier	:
// Last Update	: 2016/1/13 - 14:18
// Desc.		:
//=============================================================================
void CWnd_MainView::SetPermissionMode(enPermissionMode InspMode)
{
	//m_wnd_InspInfo.SetPermissionMode(InspMode);

	switch (InspMode)
	{
	case Permission_Operator:
		m_InspMode = Permission_Operator;
		break;

	case Permission_Manager:
	case Permission_Administrator:
		m_InspMode = Permission_Administrator;
		break;

	default:
		break;
	}

	m_wnd_InspInfo.SetPermissionMode(m_InspMode);
}

//=============================================================================
// Method		: SetOperateMode
// Access		: public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/12 - 19:52
// Desc.		:
//=============================================================================
void CWnd_MainView::SetOperateMode(__in enOperateMode nOperMode)
{
	switch (nOperMode)
	{
	case OpMode_Production:
	{
		m_st_Judgment.ShowWindow(SW_SHOW);
		m_st_OperateMode.ShowWindow(SW_HIDE);
	}
		break;

	case OpMode_Master:
	case OpMode_StartUp_Check:
	case OpMode_DryRun:
	{
		m_st_OperateMode.SetText(g_szOperateMode[nOperMode]);
		m_st_OperateMode.ShowWindow(SW_SHOW);
		m_st_Judgment.ShowWindow(SW_HIDE);
	}
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: UpdateEquipmentInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 20:53
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateEquipmentInfo()
{
	if (NULL != m_pstInspInfo)
	{
		m_wnd_InspInfo.Set_EquipmentID(m_pstInspInfo->szEquipmentID);
	}
}

//=============================================================================
// Method		: UpdateRecipeInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/7/12 - 16:03
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateRecipeInfo()
{
	m_wnd_InspInfo.Set_RecipeName(m_pstInspInfo->RecipeInfo.szRecipeFile);
	m_wnd_InspInfo.Set_Model(m_pstInspInfo->RecipeInfo.szModelCode);
	m_wnd_InspInfo.Set_EquipmentID(m_pstInspInfo->szEquipmentID);

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_TestResult[nIdx].Set_TestInfo(&m_pstInspInfo->RecipeInfo.StepInfo, &m_pstInspInfo->RecipeInfo.TestItemInfo);
	}
	
	SetModelType(m_pstInspInfo->RecipeInfo.ModelType);

// 	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
// 	{
// 		m_wnd_TestResult_ImgT[nIdx].SetModelType(m_pstInspInfo->RecipeInfo.ModelType);
// 	}

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_TestResult_IR_Img[nIdx].SetModelType(m_pstInspInfo->RecipeInfo.ModelType);
	}

	m_wnd_Graph.SetUpdateSpec();
}

//=============================================================================
// Method		: UpdateYield
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateYield()
{
	m_grid_Yield.SetYield(&m_pstInspInfo->YieldInfo);
	m_grid_YieldTest.SetYield(&m_pstInspInfo->YieldInfo);
}

//=============================================================================
// Method		: UpdatePogoCount
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/1/4 - 10:54
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdatePogoCount()
{
	for (UINT nUnitIdx = 0; nUnitIdx < m_pstInspInfo->ConsumablesInfo.ItemCount; nUnitIdx++)
	{
		m_wnd_ConsumInfo.SetItemData(nUnitIdx, m_pstInspInfo->ConsumablesInfo.Item[nUnitIdx].dwCount_Max, m_pstInspInfo->ConsumablesInfo.Item[nUnitIdx].dwCount);
	}
}

//=============================================================================
// Method		: UpdatePogoCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2018/3/10 - 14:52
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdatePogoCount(__in UINT nUnitIdx)
{
	m_wnd_ConsumInfo.SetItemData(nUnitIdx, m_pstInspInfo->ConsumablesInfo.Item[nUnitIdx].dwCount_Max, m_pstInspInfo->ConsumablesInfo.Item[nUnitIdx].dwCount);
}

//=============================================================================
// Method		: UpdateCycleTime
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/11 - 21:02
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateCycleTime()
{
	//m_grid_CycleTime.SetCycleTime(&m_pstInspInfo->CycleTime);	
}

//=============================================================================
// Method		: UpdateTestProgress
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 16:21
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestProgress()
{
	for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
	{
		m_wnd_Progress.SetParaStatus(m_pstInspInfo->GetTestProgress(nIdx), nIdx);
		//m_wnd_Progress.SetParaStatus(m_pstInspInfo->CamInfo[nIdx].nProgressStatus, nIdx);
	}

	//CheckOtherParaStatus();
}

//=============================================================================
// Method		: UpdateTestProgress_Unit
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2017/1/4 - 14:59
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateTestProgress_Unit(__in UINT nUnitIdx)
{
	m_wnd_Progress.SetParaStatus(m_pstInspInfo->GetTestProgress(nUnitIdx), nUnitIdx);

	if (TP_Ready == m_pstInspInfo->GetTestProgress(nUnitIdx))
	{
		//CheckOtherParaStatus();
	}
}

//=============================================================================
// Method		: UpdateTestResult
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/29 - 16:58
// Desc.		:
//=============================================================================
// void CWnd_MainView::UpdateTestResult()
// {
// 	if ( 0 <= m_pstInspInfo->nTestPara)
// 	{
// 		SetJudgmentStatus(m_pstInspInfo->Judgment_All);
// 
// 		//ST_CamInfo* pCam = &m_pstInspInfo->CamInfo[m_pstInspInfo->nTestPara];
// 		//m_lc_WipID.InsertWipID(pCam->szBarcode, pCam->nJudgment, &pCam->TestTime.tmStart_Loading, m_pstInspInfo->nTestPara);
// 	}
// 
// 	CheckOtherParaStatus();
// }

//=============================================================================
// Method		: UpdateBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2017/2/21 - 10:01
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateBarcode(__in UINT nUnitIdx)
{
	if (nUnitIdx < USE_CHANNEL_CNT)
	{		
		//m_wnd_Progress.SetParaBarcode( m_pstInspInfo->CamInfo[nUnitIdx].szBarcode, nUnitIdx);
		
		m_lc_Barcode.InsertWipID(m_pstInspInfo->CamInfo[nUnitIdx].szBarcode, &m_pstInspInfo->CamInfo[nUnitIdx].TestTime.tmStart_Loading, nUnitIdx);
	}
}

//=============================================================================
// Method		: Update_TestReport
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2018/2/20 - 16:24
// Desc.		:
//=============================================================================
void CWnd_MainView::Update_TestReport(__in UINT nUnitIdx)
{
	if (nUnitIdx < USE_CHANNEL_CNT)
	{
		ST_CamInfo* pCam = &m_pstInspInfo->CamInfo[nUnitIdx];
		
		if (16 < pCam->szBarcode.GetLength())
		{
			CString szBarcode = pCam->szBarcode.Right(16);
			m_lc_Barcode.InsertWipID(szBarcode, pCam->nJudgment, &pCam->TestTime.tmStart_Loading, nUnitIdx);
		}
		else
		{
			m_lc_Barcode.InsertWipID(pCam->szBarcode, pCam->nJudgment, &pCam->TestTime.tmStart_Loading, nUnitIdx);
		}
	}
}

//=============================================================================
// Method		: SetTestProgressStep
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTotalStep
// Parameter	: __in UINT nProgStep
// Qualifier	:
// Last Update	: 2017/9/29 - 10:53
// Desc.		:
//=============================================================================
void CWnd_MainView::SetTestProgressStep(__in UINT nTotalStep, __in UINT nProgStep)
{
	m_wnd_Progress.SetProgress(nTotalStep, nProgStep);
}

//=============================================================================
// Method		: SetTestResult_Unit
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in enTestResult nResult
// Qualifier	:
// Last Update	: 2016/7/18 - 16:34
// Desc.		:
//=============================================================================
void CWnd_MainView::SetTestResult_Unit(__in UINT nUnitIdx, __in enTestResult nResult)
{
	if (nUnitIdx == m_pstInspInfo->nTestPara)
	{
		SetJudgmentStatus(nResult);
	}
	else if (TR_Ready == nResult)
	{
		//CheckOtherParaStatus();
// 		BOOL bProc = TRUE;
// 		for (UINT nIdx = 0; nIdx < MAX_SITE_CNT; nIdx++)
// 		{
// 			// TP_Testing, TP_WaitPLCOut, TP_Unloading 일때는 Ready 표시 안함
// 			switch (m_pstInspInfo->CamInfo[nIdx].nProgressStatus)
// 			{
// 			case TP_Ready:
// 			case TP_Loading:
// 			case TP_WaitTest:
// 				break;
// 
// 			default:
// 				bProc = FALSE;
// 				break;
// 			}			
// 		}
// 
// 		// 양쪽 파라가 모두 대기 상태일때
// 		if (bProc)
// 		{
// 			SetJudgmentStatus(TR_Ready);
// 		}
	}
}

//=============================================================================
// Method		: SetTestResultCode_Unit
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in LRESULT nResultCode
// Qualifier	:
// Last Update	: 2017/2/21 - 10:01
// Desc.		:
//=============================================================================
void CWnd_MainView::SetTestResultCode_Unit(__in UINT nUnitIdx, __in LRESULT nResultCode)
{
	//m_wnd_SiteInfo[nUnitIdx].SetErrorCode(nResultCode);
}

//=============================================================================
// Method		: SetTestStep_Select
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/27 - 17:22
// Desc.		:
//=============================================================================
void CWnd_MainView::SetTestStep_Select(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	if (nParaIdx < USE_CHANNEL_CNT)
	{
		m_wnd_TestResult[nParaIdx].SelectItem(nStepIdx);
	}
}

//=============================================================================
// Method		: SetTestStep_Result
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nStepIdx
// Parameter	: __in enTestResult nResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/10 - 14:51
// Desc.		:
//=============================================================================
void CWnd_MainView::SetTestStep_Result(__in UINT nStepIdx, __in enTestResult nResult, __in UINT nParaIdx /*= 0*/)
{
	if (nParaIdx < USE_CHANNEL_CNT)
	{
		m_wnd_TestResult[nParaIdx].Set_Result(nStepIdx, m_pstInspInfo->GetTestItemMeas(nStepIdx, nParaIdx), m_pstInspInfo->GetTestStep(nStepIdx)->bTest);
	}
}

//=============================================================================
// Method		: SetMoterMoniter
// Access		: public  
// Returns		: void
// Parameter	: __in CString szDistance
// Parameter	: __in CString szChartTX
// Parameter	: __in CString szChartTZ
// Qualifier	:
// Last Update	: 2018/3/12 - 14:03
// Desc.		:
//=============================================================================
void CWnd_MainView::SetMoterMoniter(__in CString szDistance /*= NULL*/, __in CString szChartTX /*= NULL*/, __in CString szChartTZ /*= NULL*/)
{
	if (FALSE == szDistance.IsEmpty())
	{
		m_Wnd_Monitoring.Set_Stage(szDistance);
	}

	if (FALSE == szChartTX.IsEmpty())
	{
		m_Wnd_Monitoring.Set_Chart_Tx(szChartTX);
	}

	if (FALSE == szChartTZ.IsEmpty())
	{
		m_Wnd_Monitoring.Set_Chart_Tz(szChartTZ);
	}
}

//=============================================================================
// Method		: UpdateGraph
// Access		: public  
// Returns		: void
// Parameter	: __in double dbCurrentSFR
// Parameter	: __in double dbMaxSFR
// Qualifier	:
// Last Update	: 2018/7/6 - 9:17
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateGraphData(__in double dbCurrentSFR, __in double dbMaxSFR)
{
	m_wnd_Graph.SetUpdateData(dbCurrentSFR, dbMaxSFR);
}

//=============================================================================
// Method		: UpdateGraphReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/7/6 - 11:05
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateGraphReset()
{
	m_wnd_Graph.SetResetData();
}

//=============================================================================
// Method		: UpdateElapsedTime
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2016/6/8 - 12:13
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateElapTime_TestUnit(__in UINT nUnitIdx)
{
	if (nUnitIdx < USE_CHANNEL_CNT)
	{
		m_pstInspInfo->CamInfo[nUnitIdx].TestTime.Test.dwDuration;
	}

	m_wnd_InspInfo.Set_TestTime(m_pstInspInfo->CamInfo[nUnitIdx].TestTime.Test.dwDuration, nUnitIdx);
}

//=============================================================================
// Method		: UpdateElapTime_Cycle
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/23 - 22:45
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateElapTime_Cycle()
{
// 	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
// 	{
// 		m_wnd_InspInfo.Set_TestTime(m_pstInspInfo->CamInfo[nParaIdx].TestTime.Cycle.dwDuration, nParaIdx);
// 	}

	m_wnd_InspInfo.Set_CycleTime(m_pstInspInfo->CamInfo[Para_Left].TestTime.Cycle.dwDuration);
}

//=============================================================================
// Method		: UpdateInputTime
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2017/9/22 - 19:48
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateInputTime()
{
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_wnd_Progress.SetParaInputTime(&m_pstInspInfo->CamInfo[nIdx].TestTime.tmStart_Loading, nIdx);
	}
}

//=============================================================================
// Method		: UpdateInputTime_Unit
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2018/3/18 - 13:30
// Desc.		:
//=============================================================================
void CWnd_MainView::UpdateInputTime_Unit(__in UINT nUnitIdx)
{
	m_wnd_Progress.SetParaInputTime(&m_pstInspInfo->CamInfo[nUnitIdx].TestTime.tmStart_Loading, nUnitIdx);
}

//=============================================================================
// Method		: InitTestResult
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/21 - 21:40
// Desc.		:
//=============================================================================
void CWnd_MainView::InitTestResult()
{
	SetJudgmentStatus(enTestResult::TR_Ready);
}

//=============================================================================
// Method		: Insert_ScanBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/25 - 21:27
// Desc.		:
//=============================================================================
void CWnd_MainView::Insert_ScanBarcode(__in LPCTSTR szBarcode, __in UINT nParaIdx)
{
 // 	if (nParaIdx < USE_CHANNEL_CNT)
 // 	{
 // 		m_lc_Barcode.InsertBarcode(szBarcode, &m_pstInspInfo->CamInfo[nParaIdx].TestTime.tmStart_Loading, nParaIdx);
 // 	}
}

//=============================================================================
// Method		: Insert_ScanBarcode
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Qualifier	:
// Last Update	: 2018/3/10 - 14:52
// Desc.		:
//=============================================================================
void CWnd_MainView::Insert_ScanBarcode(__in LPCTSTR szBarcode)
{
 	//m_lc_Barcode.InsertBarcode(szBarcode, &m_pstInspInfo->CamInfo[0].TestTime.tmStart_Loading);
 
}

//=============================================================================
// Method		: ResetInfo_Loading
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/23 - 22:48
// Desc.		:
//=============================================================================
void CWnd_MainView::ResetInfo_Loading()
{
	InitTestResult();

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		m_wnd_InspInfo.ResetTestTime(nParaIdx);
		m_tc_ImageCapture[nParaIdx].Remove_ImageTab();

		m_wnd_TestResult[nParaIdx].ResetTestData();
	}

	Reset_Alarm(); // 오류 메세지 초기화
}

//=============================================================================
// Method		: ResetInfo_StartTest
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 22:36
// Desc.		:
//=============================================================================
void CWnd_MainView::ResetInfo_StartTest(__in UINT nParaIdx /*= 0*/)
{
	m_wnd_Progress.ResetProgress();

	if (nParaIdx < USE_CHANNEL_CNT)
	{
		m_wnd_TestResult[nParaIdx].ResetTestData();
		m_tc_ImageCapture[nParaIdx].Remove_ImageTab();
	}
}

//=============================================================================
// Method		: ResetInfo_Unloading
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/23 - 16:40
// Desc.		:
//=============================================================================
void CWnd_MainView::ResetInfo_Unloading()
{
	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		// Ready 상태, 바코드, 시간 초기화?
		m_wnd_Progress.ResetParaInfo(nParaIdx);
		
		// 검사 중이 아니면
		m_wnd_InspInfo.ResetTestingInfo(nParaIdx);
	}
}

//=============================================================================
// Method		: ResetInfo_Measurment
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/16 - 11:57
// Desc.		:
//=============================================================================
void CWnd_MainView::ResetInfo_Measurment(__in UINT nParaIdx /*= 0*/)
{
	if (nParaIdx < USE_CHANNEL_CNT)
	{
		m_wnd_TestResult[nParaIdx].ResetTestData();
	}
}

//=============================================================================
// Method		: Reset_Alarm
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/12/4 - 15:54
// Desc.		:
//=============================================================================
void CWnd_MainView::Reset_Alarm()
{
	m_st_Alarm.SetBackColor(Gdiplus::Color::White);
	m_st_Alarm.SetTextColor(Gdiplus::Color::Black);
	m_st_Alarm.SetText(L"Alarm");
}

//=============================================================================
// Method		: Set_Alarm
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szAlarm
// Qualifier	:
// Last Update	: 2017/12/4 - 15:56
// Desc.		:
//=============================================================================
void CWnd_MainView::Set_Alarm(__in LPCTSTR szAlarm)
{
	m_st_Alarm.SetBackColor(Gdiplus::Color::Red);
	m_st_Alarm.SetTextColor(Gdiplus::Color::White);
	m_st_Alarm.SetText(szAlarm);
}

//=============================================================================
// Method		: ShowVideo
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/5 - 10:13
// Desc.		:
//=============================================================================
void CWnd_MainView::ShowVideo(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if ((0 <= iChIdx) && (iChIdx < USE_CHANNEL_CNT))
	{
		m_wnd_ImageLive[iChIdx].Render(lpVideo, dwWidth, dwHeight);
	}
}

//=============================================================================
// Method		: NoSignal_Ch
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Qualifier	:
// Last Update	: 2018/2/8 - 15:41
// Desc.		:
//=============================================================================
void CWnd_MainView::NoSignal_Ch(__in INT iChIdx)
{
	if ((0 <= iChIdx) && (iChIdx < USE_CHANNEL_CNT))
	{
		m_wnd_ImageLive[iChIdx].Render_Empty();
	}
}

//=============================================================================
// Method		: Set_FrameRate
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in DWORD dwFrameRate
// Qualifier	:
// Last Update	: 2018/2/8 - 15:41
// Desc.		:
//=============================================================================
void CWnd_MainView::Set_FrameRate(__in INT iChIdx, __in DWORD dwFrameRate)
{
	if (0 <= iChIdx)
	{
		//m_wnd_ImageLive[iChIdx].SetFPS(dwFrameRate);
	}
}

void CWnd_MainView::ShowVideo_Vision(__in INT iChIdx, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if ((0 <= iChIdx) )
	{
		m_wnd_ImageLive_Vision.Render(lpVideo, dwWidth, dwHeight);
	}
}

void CWnd_MainView::NoSignal_Ch_Vision(__in INT iChIdx)
{
	if ((0 <= iChIdx))
	{
		m_wnd_ImageLive_Vision.Render_Empty();
	}
}

void CWnd_MainView::Set_FrameRate_Vision(__in INT iChIdx, __in DWORD dwFrameRate)
{
	if (0 <= iChIdx)
	{
		//m_wnd_ImageLive[iChIdx].SetFPS(dwFrameRate);
	}
}

//=============================================================================
// Method		: Add_ImageHistory
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in LPCTSTR szTitle
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/8 - 15:41
// Desc.		:
//=============================================================================
void CWnd_MainView::Add_ImageHistory(__in INT iChIdx, __in LPCTSTR szTitle, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (iChIdx < USE_CHANNEL_CNT)
	{
		int iIndex = m_tc_ImageCapture[iChIdx].Add_ImageTab(szTitle);

		if (0 <= iIndex)
		{
			m_tc_ImageCapture[iChIdx].ShowVideo(iIndex, lpVideo, dwWidth, dwHeight);
		}
	}
}

//=============================================================================
// Method		: Set_ImageHistory
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in LPCTSTR szTitle
// Parameter	: __in UINT nIndex
// Parameter	: __in LPBYTE lpVideo
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/8 - 15:41
// Desc.		:
//=============================================================================
void CWnd_MainView::Set_ImageHistory(__in INT iChIdx, __in LPCTSTR szTitle, __in UINT nIndex, __in LPBYTE lpVideo, __in DWORD dwWidth, __in DWORD dwHeight)
{
	if (iChIdx < USE_CHANNEL_CNT)
	{
		m_tc_ImageCapture[iChIdx].ShowVideo(nIndex, lpVideo, dwWidth, dwHeight);
	}
}

//=============================================================================
// Method		: Clear_ImageHistory
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Qualifier	:
// Last Update	: 2018/2/8 - 15:41
// Desc.		:
//=============================================================================
void CWnd_MainView::Clear_ImageHistory(__in INT iChIdx)
{
	if (iChIdx < USE_CHANNEL_CNT)
	{
		m_tc_ImageCapture[iChIdx].Remove_ImageTab();
	}
}

//=============================================================================
// Method		: Set_CameraSelect
// Access		: public  
// Returns		: void
// Parameter	: __in INT iChIdx
// Parameter	: __in BOOL bUpdateRadioButton
// Qualifier	:
// Last Update	: 2018/3/17 - 11:56
// Desc.		:
//=============================================================================
void CWnd_MainView::Set_CameraSelect(__in INT iChIdx, __in BOOL bUpdateRadioButton/* = TRUE*/)
{
	if (iChIdx < USE_CHANNEL_CNT)
	{
		m_wnd_Progress.Set_CameraSelect(iChIdx);
		m_wnd_TimerView.ShowWindow(SW_HIDE);

		if (Para_Left == iChIdx)
		{
		
			m_wnd_ImageLive[Para_Left].ShowWindow(SW_SHOW);
			m_wnd_ImageLive[Para_Right].ShowWindow(SW_HIDE);
			

			m_wnd_TestResult[Para_Left].ShowWindow(SW_SHOW);
			m_wnd_TestResult[Para_Right].ShowWindow(SW_HIDE);

			m_tc_ImageCapture[Para_Left].ShowWindow(SW_SHOW);
			m_tc_ImageCapture[Para_Right].ShowWindow(SW_HIDE);

			//m_wnd_TestResult_ImgT[Para_Left].ShowWindow(SW_SHOW);
			//m_wnd_TestResult_ImgT[Para_Right].ShowWindow(SW_HIDE);

			m_wnd_TestResult_IR_Img[Para_Left].ShowWindow(SW_SHOW);
			m_wnd_TestResult_IR_Img[Para_Right].ShowWindow(SW_HIDE);

			//m_wnd_TestResult_Foc[Para_Left].ShowWindow(SW_SHOW);
			//m_wnd_TestResult_Foc[Para_Right].ShowWindow(SW_HIDE);
			
// 			if (bUpdateRadioButton)
// 			{
// 				m_rb_CameraSelect[Para_Right].SetCheck(BST_UNCHECKED);
// 				m_rb_CameraSelect[Para_Left].SetCheck(BST_CHECKED);
// 			}
		}
		else if (Para_Right == iChIdx)
		{
			
			if (Sys_Focusing != m_InspectionType)
			{
			m_wnd_ImageLive[Para_Right].ShowWindow(SW_SHOW);
			m_wnd_ImageLive[Para_Left].ShowWindow(SW_HIDE);
			}
			

			m_wnd_TestResult[Para_Right].ShowWindow(SW_SHOW);
			m_wnd_TestResult[Para_Left].ShowWindow(SW_HIDE);
			
			m_tc_ImageCapture[Para_Right].ShowWindow(SW_SHOW);
			m_tc_ImageCapture[Para_Left].ShowWindow(SW_HIDE);


			//m_wnd_TestResult_ImgT[Para_Right].ShowWindow(SW_SHOW);
			//m_wnd_TestResult_ImgT[Para_Left].ShowWindow(SW_HIDE);

			m_wnd_TestResult_IR_Img[Para_Right].ShowWindow(SW_SHOW);
			m_wnd_TestResult_IR_Img[Para_Left].ShowWindow(SW_HIDE);

		//	m_wnd_TestResult_Foc[Para_Right].ShowWindow(SW_SHOW);
		//	m_wnd_TestResult_Foc[Para_Left].ShowWindow(SW_HIDE);
			
// 			if (bUpdateRadioButton)
// 			{
// 				m_rb_CameraSelect[Para_Left].SetCheck(BST_UNCHECKED);
// 				m_rb_CameraSelect[Para_Right].SetCheck(BST_CHECKED);
// 			}
		}
	}
}

//=============================================================================
// Method		: Set_UseImageViewWnd
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUseImageViewWnd
// Qualifier	:
// Last Update	: 2018/3/6 - 16:37
// Desc.		:
//=============================================================================
void CWnd_MainView::Set_UseImageViewWnd(__in BOOL bUseImageViewWnd)
{
	if (m_bUseImageViewWnd != bUseImageViewWnd)
	{
		m_bUseImageViewWnd = bUseImageViewWnd;

		// 다시 그리기
		if (GetSafeHwnd())
		{
			CRect rc;
			GetClientRect(rc);
			OnSize(SIZE_RESTORED, rc.Width(), rc.Height());
		}
	}
}

// void CWnd_MainView::OnBnClickedPreWork(){
// }

void CWnd_MainView::Foc_ChangeCameraMode(BOOL bMode, int nCamIdx)
{
	if (bMode == TRUE)
	{
	//	m_wnd_TestResult_Foc[Para_Left].ShowWindow(SW_HIDE);
	//	m_wnd_TestResult_Foc[Para_Right].ShowWindow(SW_HIDE);
		m_wnd_TimerView.ShowWindow(SW_SHOW);

		m_wnd_TimerView.TimeSetting(m_pstInspInfo->RecipeInfo.nChangeDelay);
		m_wnd_TimerView.Start_CounterCheck_Mon();
		Sleep(100);
	}
	else
	{
		m_wnd_TimerView.ShowWindow(SW_HIDE);

		Set_CameraSelect(nCamIdx);
	}
}

//=============================================================================
// Method		: Foc_ChangeCameraStop
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/17 - 11:27
// Desc.		:
//=============================================================================
void CWnd_MainView::Foc_ChangeCameraStop()
{
		m_wnd_TimerView.Stop_CounterCheck_Mon();
}

//=============================================================================
// Method		: Foc_ChangeCamFlag
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/17 - 11:27
// Desc.		:
//=============================================================================
BOOL CWnd_MainView::Foc_ChangeCamFlag()
{
	return m_wnd_TimerView.m_bFlag_Counter;
}


void CWnd_MainView::SetTestResult_Total( __in enTestResult nResult)
{
		SetJudgmentStatus(nResult);
}

void CWnd_MainView::SetLightCount(int nCnt){
	CString str;
	str.Format(_T("%d"), nCnt);
	m_st_Judgment.SetText(str);

	m_st_Judgment.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_Judgment.SetBorderColor(Gdiplus::Color::Black);
	m_st_Judgment.SetTextColor(Gdiplus::Color::Green, Gdiplus::Color::Green);
}