﻿// Wnd_TeachOp.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_TeachOp.h"

// CWnd_TeachOp

IMPLEMENT_DYNAMIC(CWnd_TeachOp, CWnd)

typedef enum TeachOp_ID
{
	IDC_TC_BN_SAVE	= 1100,
	IDC_TC_ED_ITEM1	= 1200,
	IDC_TC_ED_ITEM2,
	IDC_TC_ED_ITEM3,
	IDC_TC_ED_ITEM4,
	IDC_TC_ED_ITEM5,
	IDC_TC_ED_ITEM6,
	IDC_TC_ED_ITEM7,
	IDC_TC_ED_ITEM8,
	IDC_TC_ED_ITEM9,
	IDC_TC_ED_ITEM10,
	IDC_TC_ED_ITEM11,
	IDC_TC_ED_ITEM12,
	IDC_TC_ED_ITEM13,
	IDC_TC_ED_ITEM14,
	IDC_TC_MAXNUM,
};

CWnd_TeachOp::CWnd_TeachOp()
{
	m_nItemCnt = 0;

	m_pstMaintenanceInfo = NULL;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TeachOp::~CWnd_TeachOp()
{
	m_font.DeleteObject		();
}

BEGIN_MESSAGE_MAP(CWnd_TeachOp, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_TC_BN_SAVE, OnBnClickedBnSave)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/3/28 - 17:02
// Desc.		:
//=============================================================================
int CWnd_TeachOp::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwEtStyle = WS_VISIBLE | WS_BORDER | ES_CENTER;
	DWORD dwStyle	= WS_VISIBLE | WS_CHILD;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
	{
		m_st_Item[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_Item[nIdx].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_Item[nIdx].Create(m_szTeacOpName[nIdx], dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	}

	for (UINT nIdx = 0; nIdx < BN_TC_MAXNUM; nIdx++)
	{
		m_bn_Item[nIdx].Create(g_szTeachOpButton[nIdx], dwStyle | BS_PUSHBUTTON, rectDummy, this, IDC_TC_BN_SAVE + nIdx);
		m_bn_Item[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
	{
		m_ed_Item[nIdx].Create(dwEtStyle, rectDummy, this, IDC_TC_ED_ITEM1 + nIdx);
		m_ed_Item[nIdx].SetWindowText(_T("0"));
		m_ed_Item[nIdx].SetValidChars(_T("0123456789.-"));
		m_ed_Item[nIdx].SetFont(&m_font);
	}

	m_st_Load.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Load.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Load.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Load.Create(_T("Loading/Unloading"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Vision.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Vision.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Vision.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Vision.Create(_T("Vision"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_DisplaceA.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_DisplaceA.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_DisplaceA.SetFont_Gdip(L"Arial", 9.0F);
	m_st_DisplaceA.Create(_T("Displace A"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_DisplaceB.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_DisplaceB.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_DisplaceB.SetFont_Gdip(L"Arial", 9.0F);
	m_st_DisplaceB.Create(_T("Displace B"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_DisplaceC.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_DisplaceC.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_DisplaceC.SetFont_Gdip(L"Arial", 9.0F);
	m_st_DisplaceC.Create(_T("Displace C"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_DisplaceD.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_DisplaceD.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_DisplaceD.SetFont_Gdip(L"Arial", 9.0F);
	m_st_DisplaceD.Create(_T("Displace D"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Chart.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Chart.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Chart.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Chart.Create(_T("Chart"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	m_st_Blemish.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Blemish.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_Blemish.SetFont_Gdip(L"Arial", 9.0F);
	m_st_Blemish.Create(_T("Blemish"), dwStyle | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_TeachOp::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin		= 5;
	int iSpacing	= 3;

	int iLeft		= iMargin;
	int iLeftSub	= iMargin;
	int iTop		= iMargin;
	int iWidth		= cx - iMargin - iMargin;
	int iHeight		= cy - iMargin - iMargin;
	int iEdWidth	= (iWidth - iSpacing * 9) / 8;

	int iEditHeight = (iHeight - iSpacing * 11) / 10; 
	int iBtnWidth	= 0;

	int iTitleW		= (iWidth  - iMargin * 3 - 16) / 8 ;
	int iItemW		= (iTitleW - iMargin * 1) / 2;
	int iItemH		= (iHeight - iSpacing * 10) / 11;

	switch (m_nSysType)
	{
	case Sys_IR_Image_Test:
		m_st_Load.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_Load_Y; nIdx <= TC_IR_Imgt_Load_TY; nIdx++)
		{
			iLeft = iMargin;
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iLeft  = iMargin + (iTitleW + iMargin);
		iTop   = iMargin;
		m_st_Vision.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_Vision_Y; nIdx <= TC_IR_Imgt_Vision_TY; nIdx++)
		{
			iLeft = iMargin + (iTitleW + iMargin);
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iLeft = iMargin + (iTitleW + iMargin) * 2;
		iTop = iMargin;
		m_st_DisplaceA.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_DisplaceA_Y; nIdx <= TC_IR_Imgt_DisplaceA_TY; nIdx++)
		{
			iLeft = iMargin + (iTitleW + iMargin) * 2;
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iLeft = iMargin + (iTitleW + iMargin) * 3;
		iTop = iMargin;
		m_st_DisplaceB.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_DisplaceB_Y; nIdx <= TC_IR_Imgt_DisplaceB_TY; nIdx++)
		{
			iLeft = iMargin + (iTitleW + iMargin) * 3;
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iLeft = iMargin + (iTitleW + iMargin) * 4;
		iTop = iMargin;
		m_st_DisplaceC.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_DisplaceC_Y; nIdx <= TC_IR_Imgt_DisplaceC_TY; nIdx++)
		{
			iLeft = iMargin + (iTitleW + iMargin) * 4;
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iLeft = iMargin + (iTitleW + iMargin) * 5;
		iTop = iMargin;
		m_st_DisplaceD.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_DisplaceD_Y; nIdx <= TC_IR_Imgt_DisplaceD_TY; nIdx++)
		{
			iLeft = iMargin + (iTitleW + iMargin) * 5;
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iLeft = iMargin + (iTitleW + iMargin) * 6;
		iTop = iMargin;
		m_st_Chart.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_Chart_Y; nIdx <= TC_IR_Imgt_Chart_TY; nIdx++)
		{
			iLeft = iMargin + (iTitleW + iMargin) * 6;
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iLeft = iMargin + (iTitleW + iMargin) * 7;
		iTop = iMargin;
		m_st_Blemish.MoveWindow(iLeft, iTop, iTitleW, iItemH);

		for (UINT nIdx = TC_IR_Imgt_Blemish_Y; nIdx <= TC_IR_Imgt_Blemish_TY; nIdx++)
		{
			iLeft = iMargin + (iTitleW + iMargin) * 7;
			iTop += iItemH + iSpacing;
			m_st_Item[nIdx].MoveWindow(iLeft, iTop, iItemW, iItemH);

			iLeftSub = iLeft + iItemW + iMargin;
			m_ed_Item[nIdx].MoveWindow(iLeftSub, iTop, iItemW, iItemH);
		}

		iTop	 += iItemH + iSpacing;
		iBtnWidth = iTitleW;

		m_bn_Item[BN_TC_SAVE].MoveWindow(iLeft, iTop, iBtnWidth, iEditHeight);

		break;

	case Sys_Focusing:
		break;

	case Sys_2D_Cal:
		for (UINT nIdx = 1; nIdx < m_nItemCnt + 1; nIdx++)
		{
			m_st_Item[nIdx - 1].MoveWindow(iLeft, iTop, iEdWidth + iSpacing * 3, iEditHeight);
			iLeft += iEdWidth + iSpacing * 3 + 2;

			m_ed_Item[nIdx - 1].MoveWindow(iLeft, iTop, iEdWidth - iSpacing * 3, iEditHeight);
			iLeft += iEdWidth - iSpacing * 1;

			if (nIdx % 4 == 0 && nIdx != 0)
			{
				iTop += iEditHeight + iSpacing;
				iLeft = iMargin;
			}
		}

		iBtnWidth = iTitleW;
		iLeft = cx - iSpacing - iBtnWidth;

		m_bn_Item[BN_TC_SAVE].MoveWindow(iLeft, iTop, iBtnWidth, iEditHeight);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnBnClickedBnSave
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/30 - 9:56
// Desc.		:
//=============================================================================
void CWnd_TeachOp::OnBnClickedBnSave()
{
	// UI 데이터 수집
	GetUpdateData();

	// 수집된 데이터 저장
	m_pstDevice->SaveMotionInfo();

	CString szMaintenanceFullPath;

	m_fileMaintenance.SaveTeachFile(m_pstMaintenanceInfo->szMaintenanceFullPath, &m_pstMaintenanceInfo->stTeachInfo);
	SetUpdateData();
}

//=============================================================================
// Method		: PreTranslateMessage
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: MSG * pMsg
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
BOOL CWnd_TeachOp::PreTranslateMessage(MSG* pMsg)
{
	return CWnd::PreTranslateMessage(pMsg);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_TeachOp::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: SetUpdateItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/5 - 19:02
// Desc.		:
//=============================================================================
void CWnd_TeachOp::SetUpdateItem()
{
	m_nItemCnt = TC_ALL_MAX;

	switch (m_nSysType)
	{
	case Sys_Focusing:
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_Focus[nIdx];
		}
		break;
	case Sys_2D_Cal:
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_2D_CAL[nIdx];
		}
		break;
	case Sys_IR_Image_Test:
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_IR_Image[nIdx];
		}
		break;
	case Sys_Image_Test:
		for (UINT nIdx = 0; nIdx < m_nItemCnt; nIdx++)
		{
			m_szTeacOpName[nIdx] = g_szTeachItem_Image[nIdx];
		}
		break;
	default:
		break;
	}
}

//=============================================================================
// Method		: SetUpdateData
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/7 - 16:38
// Desc.		:
//=============================================================================
void CWnd_TeachOp::SetUpdateData()
{
	if (NULL == m_pstMaintenanceInfo)
		return;

	CString szData;

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
	{
		szData.Format(_T("%.1f"), m_pstMaintenanceInfo->stTeachInfo.dbTeachData[nIdx]);
		m_ed_Item[nIdx].SetWindowText(szData);
	}
}

//=============================================================================
// Method		: GetUpdateData
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/7 - 16:38
// Desc.		:
//=============================================================================
void CWnd_TeachOp::GetUpdateData()
{
	if (NULL == m_pstMaintenanceInfo)
		return;

	CString szData;

	for (UINT nIdx = 0; nIdx < TC_ALL_MAX; nIdx++)
	{
		m_ed_Item[nIdx].GetWindowText(szData);
		m_pstMaintenanceInfo->stTeachInfo.dbTeachData[nIdx] = _ttof(szData);
	}
}
