#pragma once

// CWnd_LayoutFrame
#ifndef Wnd_LayoutFrame_h__
#define Wnd_LayoutFrame_h__

#include "VGStatic.h"
#include "Wnd_BaseView.h"
#include "Wnd_ParaCtrl.h"

#include "Def_TestDevice.h"

class CWnd_LayoutFrame : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_LayoutFrame)

public:
	CWnd_LayoutFrame();
	virtual ~CWnd_LayoutFrame();

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow		(CREATESTRUCT& cs);

	void Set_Device						(__in ST_Device*		pDevice)
	{
		m_pDevice = pDevice;
	}

	// 설비의 Para 갯수 (0으로 설정하면 UI상에 Para 정보 표시하지 않음)
	void			SetParaCount		(__in UINT nCount);

protected:
	DECLARE_MESSAGE_MAP()

	ST_Device*		m_pDevice;
	CWnd_ParaCtrl	m_wndParaCtrl[Para_MaxEnum];

	// 설비의 Para 갯수 (WipID : 1, 2D Cal : 2, IQ : 2, 이물 : 1 or 0, 3D Cal : 2)
	UINT			m_nParaCount = 2;		// 0 ~ 2개

};

#endif // Wnd_LayoutFrame_h__