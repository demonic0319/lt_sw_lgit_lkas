﻿//*****************************************************************************
// Filename	: 	Def_TestDevice.h
// Created	:	2016/5/26 - 5:44
// Modified	:	2016/5/26 - 5:44
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Def_TestDevice_h__
#define Def_TestDevice_h__

#include "Def_CompileOption.h"

// Device
#include "Def_Enum.h"
#include "MES_LG.h"
#include "LGE_LightBrd.h"
#include "PCBCamBrd.h"
#include "ODA_PT.h"
#include "DAQ_LG_IRCam.h"
#include "DigitalIOCtrl.h"
#include "TorqueDriver.h"

#include "Keyence_Displace.h"
#include "MIU_Ctrl.h"
#include "VisionCam.h"

#include "FCM30_Client.h"

#ifdef USE_BARCODE_SCANNER
#include "BCRCtrl.h"
#endif

#ifndef MOTION_NOT_USE
#include "MotionManager.h"
#endif

// 핸들러 / 테스터 선택 ------------------------------------
typedef struct _tag_Device
{
	CMES_LG				MES;

#ifdef USE_BARCODE_SCANNER
	CBCRCtrl			HandyBCR;	// 핸디 바코드 리더기
#endif

	CPCBCamBrd			PCBCamBrd[USE_CHANNEL_CNT];		// 제어 보드 : 1 ~ 2개

	CLGE_LightBrd		LightBrd[MAX_LIGHT_BRD_CNT];	// 이물 광원 보드
	CODA_PT				LightPSU[MAX_LIGHT_BRD_CNT];	// 광원 제어 파워

	CDAQ_LG_IRCam		DAQ_LVDS;

	CKeyence_Displace	Displace;
	CMIU_Ctrl			MIUCtrl;
	CVisionCam			Vision;

	CDigitalIOCtrl		DigitalIOCtrl;

#ifndef MOTION_NOT_USE
	CMotionManager		MotionManager;
#endif

	CTorqueDriver		Torque;							//Torque 

	CFCM30_Client		FCM30;

}ST_Device, *PST_Device;

#endif // Def_TestDevice_h__
