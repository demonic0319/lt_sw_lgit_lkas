﻿//*****************************************************************************
// Filename	: 	TestManager_Test.h
// Created	:	2017/10/14 - 18:01
// Modified	:	2017/10/14
//
// Author	:	KHO
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_Test_h__
#define TestManager_Test_h__

#pragma once

#include "Def_ResultCode_Cm.h"
#include "TI_CircleDetect.h"

#include "Def_UI_DynamicBW.h"
#include "Def_UI_Shading.h"
#include "Def_T_IR.h"

#include "LGIT_Algorithm.h"

class CTestManager_Test
{
public:
	CTestManager_Test();
	virtual ~CTestManager_Test();

	LRESULT		LGIT_Func_Chart					(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Chart stOption,			__out ST_Result_Chart& stResult);
	LRESULT		LGIT_Func_SFR					(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_SFR stOption,			__out ST_Result_SFR& stResult);
	LRESULT		LGIT_Func_DynamicBW				(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_DynamicBW stOption,		__out ST_Result_DynamicBW& stResult);
	LRESULT		LGIT_Func_Shading				(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Shading stOption,		__out ST_Result_Shading& stResult);
	LRESULT		LGIT_Func_Rllumination			(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rllumination stOption,	__out ST_Result_Rllumination& stResult);
	LRESULT		LGIT_Func_Ymean					(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Ymean stOption,			__out ST_Result_Ymean& stResult);
	LRESULT		LGIT_Func_BlackSpot				(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_BlackSpot stOption, __out ST_Result_BlackSpot& stResult);
	LRESULT		LGIT_Func_LCB					(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_LCB stOption, __out ST_Result_LCB& stResult);


	LRESULT		Luri_Func_OpticalCenter			(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_OpticalCenter stOption,	__out ST_Result_OpticalCenter& stResult);
	LRESULT		Luri_Func_Shading				(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Shading stOption,		__out ST_Result_Shading& stResult);
	LRESULT		Luri_Func_Vision				(__in LPBYTE pImageBuf, __in UINT nImageW, __in UINT nImageH, __in CString szMasterPath, __in ST_UI_Vision stOption, __out ST_Result_Vision& stResult, __out LPBYTE pDstImageBuf);
	LRESULT		Luri_Func_IIC					(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in __in LPBYTE pMasterImage,		__out ST_Result_IIC& stResult);
	//LRESULT	Luri_Func_Displace				(__in CString szBarcode, __in ST_UI_Displace stOption, __out ST_Result_Displace& stResult);

	LRESULT		LGIT_Func_DefectBlack			(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_Black stOption, __out ST_Result_Defect_Black& stResult);
	LRESULT		LGIT_Func_DefectWhite			(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Defect_White stOption, __out ST_Result_Defect_White& stResult);
	LRESULT		LGIT_Func_Distortion			(__in LPBYTE pImage8bit, __in LPBYTE pImageRaw, __in int iImageW, __in int iImageH, __in ST_UI_Rotation stOption, __out ST_Result_Rotation& stResult);

	BOOL		Get_MeasurmentData				(__in BOOL bMinUse, __in BOOL bMaxUse, __in double dbMinSpec, __in double dbMaxSpec, __in double dbValue);
	BOOL		Get_MeasurmentData				(__in BOOL bMinUse, __in BOOL bMaxUse, __in int iMinSpec, __in int iMaxSpec,__in int iValue);
	

	BOOL	GetTextFileData_FindBarcode(__in CString szBarcode, __in int iColumn, __out CString& szOutData);
	void	GetTiltXY_4PointInput(__in double* x, __in double* y, __in double* height, __out double& tiltx, __out double& tilty);
	bool	RCCC2CCCC(IplImage *src, IplImage *dst, int nType);

protected:

	CTI_CircleDetect	m_LT_CircleDetect;
	CLG_LGIT_Algorithm	m_LG_Algorithm;

	// 보정 ROI 값 산풀
	LRESULT		LURI_Func_DetectROI_Ver1		(__in LPBYTE pImageBuf, __in int iImageW, __in int iImageH, __in CRect rtOption, __out CRect &rtResult);
	LRESULT		LURI_Func_DetectROI_Ver2		(__in IplImage* pFrameImage, __in CPoint ptCenter, __in CRect rtOption, __out CRect &rtResult); // SFR 전용
	void		OnTestProcess_SFRROI			(__in IplImage* pFrameImage, __in ST_UI_SFR stOpt, __out ST_Result_SFR &stTestData);

	double		GetDistance						(__in int x1, __in int y1, __in int x2, __in int y2);

	// 외부에서 접근 할 필요가 없는 거는 protected 로 선언해서 사용하세요.
	//!SH _180808: 셋팅 Array 부분 포인터로 넘기기에 필요한 배열들 셋팅함.
	void		Set_Shading_ROI_Array			(__in ST_UI_Shading & stOption, __out double * pdHorThres, __out double * pdVerThres, __out double * pdDiaThres, __out POINT * ppHorizonPoint, __out POINT * ppVerticalPoint, __out POINT * ppDiaAPoint, __out POINT * ppDiaBPoint);
	void		Set_Dinamic_ROI_Array			(__in ST_UI_DynamicBW & stOption, __out POINT * ppDinamicBWPoint);

	void	CvtByte_IplImage(__in LPBYTE pImageBuf, __out IplImage* dstImage);
	void	CvtIplImage_Byte(__in IplImage* srcImage, __out LPBYTE& pImageBuf);

	
	void	GetField_4Point(__in int iWidth, int iHeight, double dbField, __out CvPoint& Left_Top, CvPoint& Left_Bottom, CvPoint& Right_Top, CvPoint& Right_Bottom);
	void	RawToBmpQuater(unsigned char *pRaw8, unsigned char *pBMP, int image_width, int image_height, int ch);
	void	PRI_12BitToRaw8(unsigned char *p12Bit, unsigned char *pRawBit8, int w, int h);

	void	Shift12to16Bit_QuarterMode(unsigned char* p12bitRaw, unsigned char* p16bitRaw, int nWidth, int nHeight, int Mode);
	void	Shift16to12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight);
	void	Shift12BitMode(unsigned char* pImage, unsigned char* pDest, unsigned int nWidth, unsigned int nHeight);

	void	SetCamState_OC(__in UINT nCamState, __in double iIn_X, __in double iIn_Y, __out double& iOut_X, __out double& iOut_Y);
};

#endif // TestManager_Test_h__
