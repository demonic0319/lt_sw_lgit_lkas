//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem.cpp
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
// Wnd_Cfg_TestItem.cpp : implementation file
//

#include "stdafx.h"
#include "Wnd_Cfg_TestItem.h"

// CWnd_Cfg_TestItem

IMPLEMENT_DYNAMIC(CWnd_Cfg_TestItem, CWnd_BaseView)

CWnd_Cfg_TestItem::CWnd_Cfg_TestItem()
{
	m_InspectionType = enInsptrSysType::Sys_Focusing;
}

CWnd_Cfg_TestItem::~CWnd_Cfg_TestItem()
{
}

BEGIN_MESSAGE_MAP(CWnd_Cfg_TestItem, CWnd_BaseView)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/9/28 - 17:51
// Desc.		:
//=============================================================================
int CWnd_Cfg_TestItem::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();
	
	UINT nID_Index = 0;

	m_tc_Option.Create(CMFCTabCtrl::STYLE_3D, rectDummy, this, 1, CMFCTabCtrl::LOCATION_BOTTOM);

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	{
	}
		break;

	case Sys_2D_Cal:
	{
		// 2D Calibration
		m_wnd_Cfg2DCal.SetOwner(GetOwner());
		m_wnd_Cfg2DCal.Create(NULL, _T("Parameter"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);

		m_wnd_Cfg2DCal_Spec.SetOwner(GetOwner());
		m_wnd_Cfg2DCal_Spec.Create(NULL, _T("Spec"), dwStyle, rectDummy, &m_tc_Option, nID_Index++);
	}
		break;

	case Sys_Image_Test:
	{
		
	}
		break;

	case Sys_3D_Cal:
	{
	
	
	}
		break;

	default:
		break;
	}
	m_wnd_TestItem_EachTest.SetCameraParaIdx(m_pnCamParaIdx);

	m_wnd_TestItem_EachTest.Create(NULL, _T("개별"), dwStyle /*| WS_BORDER*/, rectDummy, this, nID_Index++);

	// 검사기 별로 탭 컨트롤 정의
	SetSysAddTabCreate();
	if (m_tc_Option.GetTabsNum() > 0)
	{
		m_tc_Option.SetActiveTab(0);
		m_tc_Option.EnableTabSwap(FALSE);
	}
	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/9/28 - 17:52
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((cx == 0) && (cy == 0))
		return;

	int iMargin = 5;
	int iLeft	= iMargin;
	int iTop	= iMargin;
	int iWidth	= cx - iMargin - iMargin;
	int iHeight = cy - iMargin - iMargin;

	int nTabW = iWidth * 4 / 5;
	m_tc_Option.MoveWindow(iLeft, iTop, nTabW, iHeight);

	iLeft += iMargin + nTabW;

	m_wnd_TestItem_EachTest.MoveWindow(iLeft, iTop, iWidth - (iMargin + nTabW), iHeight);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: protected  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/10/21 - 11:21
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);

	if (TRUE == bShow)
	{
		m_tc_Option.SetActiveTab(0);
	}
}

//=============================================================================
// Method		: OnNMClickTestItem
// Access		: protected  
// Returns		: void
// Parameter	: NMHDR * pNMHDR
// Parameter	: LRESULT * pResult
// Qualifier	:
// Last Update	: 2017/9/28 - 18:36
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::OnNMClickTestItem(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);

	*pResult = 0;
}

//=============================================================================
// Method		: SetSysAddTabCreate
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/15 - 10:43
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::SetSysAddTabCreate()
{
	UINT nItemCnt = 0;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
	{
		//m_tc_Option.AddTab(&m_wnd_CfgWipID, _T("Spec"), nItemCnt++, TRUE);
	}
	break;

	case Sys_2D_Cal:
	{
		m_tc_Option.AddTab(&m_wnd_Cfg2DCal,			_T("Parameter"),	nItemCnt++, TRUE);
		m_tc_Option.AddTab(&m_wnd_Cfg2DCal_Spec,	_T("Spec"),			nItemCnt++, TRUE);
	}
	break;

	case Sys_Image_Test:
	{
		
	}
	break;

	case Sys_3D_Cal:
	{
		
	}
	break;

	default:
		break;
	}

}

//=============================================================================
// Method		: SetInitListCtrl
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 18:20
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::SetInitListCtrl()
{

}

// CWnd_Cfg_TestItem message handlers
//=============================================================================
// Method		: SetSystemType
// Access		: public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 14:12
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::SetSystemType(__in enInsptrSysType nSysType)
{
	m_wnd_Cfg2DCal.SetSystemType(nSysType);
	m_wnd_TestItem_EachTest.SetSystemType(nSysType);
	m_InspectionType = nSysType;
}

//=============================================================================
// Method		: Set_ModelType
// Access		: public  
// Returns		: void
// Parameter	: __in enModelType nModelType
// Qualifier	:
// Last Update	: 2018/3/6 - 13:45
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::Set_ModelType(__in enModelType nModelType)
{
switch (m_InspectionType)
	{
	case Sys_Focusing:
	{
		
	}
		break;

	case Sys_2D_Cal:
	{
		
	}
		break;

	case Sys_Image_Test:
	{
		
	}
		break;

	case Sys_3D_Cal:
	{
		
	}
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: Set_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __in ST_RecipeInfo * pstRecipeInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::Set_RecipeInfo(__in ST_RecipeInfo* pstRecipeInfo)
{
	switch (m_InspectionType)
	{
	case Sys_Focusing:
	{
		//m_wnd_CfgWipID.Set_TestItemInfo(&pstRecipeInfo->TestItemInfo);
	}
		break;

	case Sys_2D_Cal:
	{
		m_wnd_Cfg2DCal.Set_2DCal_Para(&pstRecipeInfo->st2D_CAL.Parameter);
		m_wnd_Cfg2DCal_Spec.Set_TestItemInfo(&pstRecipeInfo->TestItemInfo);
		
		m_wnd_TestItem_EachTest.Set_RecipeInfo(pstRecipeInfo);
	}
		break;

	case Sys_Image_Test:
	{
		
	}
		break;

	case Sys_3D_Cal:
	{
		
	}
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: Get_RecipeInfo
// Access		: public  
// Returns		: void
// Parameter	: __out ST_RecipeInfo & stOutRecipInfo
// Qualifier	:
// Last Update	: 2017/11/10 - 20:50
// Desc.		:
//=============================================================================
void CWnd_Cfg_TestItem::Get_RecipeInfo(__out ST_RecipeInfo& stOutRecipInfo)
{
	switch (m_InspectionType)
	{
	case Sys_Focusing:
	{
		//m_wnd_CfgWipID.Get_TestItemInfo(stOutRecipInfo.TestItemInfo);
	}
	break;

	case Sys_2D_Cal:
	{
		m_wnd_Cfg2DCal.Get_2DCal_Para(stOutRecipInfo.st2D_CAL.Parameter);
		m_wnd_Cfg2DCal_Spec.Get_TestItemInfo(stOutRecipInfo.TestItemInfo);
	}
	break;

	case Sys_Image_Test:
	{
		
	}
	break;

	case Sys_3D_Cal:
	{

	}
	break;

	default:
		break;
	}
}
