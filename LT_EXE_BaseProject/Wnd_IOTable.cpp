﻿//*****************************************************************************
// Filename	: 	Wnd_IOTable.cpp
// Created	:	2018/1/5 - 17:26
// Modified	:	2018/1/5 - 17:26
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// Wnd_IOTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_IOTable.h"
#include "Def_Digital_IO.h"

// CWnd_IOTable

#define IDC_ST_DIN_DESC			1000
#define IDC_ST_DIN_SIGNAL		1200
#define IDC_ST_DOUT_DESC		1400
#define IDC_BN_DOUT_SIGNAL		1600

#define	LIGHT_RED			RGB(255,	000,	000)

IMPLEMENT_DYNAMIC(CWnd_IOTable, CWnd)


CWnd_IOTable::CWnd_IOTable()
{
	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_SEMIBOLD,			// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_IOTable::~CWnd_IOTable()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_IOTable, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BN_DOUT_SIGNAL, IDC_BN_DOUT_SIGNAL + MAX_DIO_COUNT, OnRangeCmds)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
int CWnd_IOTable::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_DI.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_DI.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_DI.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_DI.SetFont_Gdip(L"Arial", 15.0F);
	m_st_DI.Create(_T("DIGITAL INPUT LIST"), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	
	m_st_DO.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_DO.SetColorStyle(CVGStatic::ColorStyle_DarkGray);
	m_st_DO.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_DO.SetFont_Gdip(L"Arial", 15.0F);
	m_st_DO.Create(_T("DIGITAL OUTPUT LIST"), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < MAX_DIO_COUNT; nIdx++)
	{
		m_st_DI_Name[nIdx].SetFont_Gdip(L"Arial", 9.0f, Gdiplus::FontStyleRegular);
		m_st_DI_Name[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_DI_Name[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_DIN_DESC + nIdx);

		m_st_DI_Status[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Title);
		m_st_DI_Status[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_DI_Status[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_ST_DIN_SIGNAL + nIdx);
		m_st_DI_Status[nIdx].SetBackColor(LIGHT_RED);
	}

	for (UINT nIdx = 0; nIdx < MAX_DIO_COUNT; nIdx++)
	{
		m_st_DO_Name[nIdx].SetFont_Gdip(L"Arial", 9.0f, Gdiplus::FontStyleRegular);
		m_st_DO_Name[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_DO_Name[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_DOUT_DESC + nIdx);

		m_bn_DO_Status[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | BS_PUSHLIKE | BS_AUTOCHECKBOX, rectDummy, this, IDC_BN_DOUT_SIGNAL + nIdx);
	}

	Update_DIO_ItemName();

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_IOTable::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	if (m_nDI_Count <= 0 || m_nDO_Count <= 0)
		return;

	int iMargin		= 10;
	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;
	int iLeftSub	= 0;
	int iGroupWidth = (iWidth - iSpacing) / 2;
	int iCtrlH		= (iHeight - iSpacing * m_nMaxRowCount) / (m_nMaxRowCount + 1);

	// 16개 단위로 열 개수를 구분한다.
// 	m_nMaxRowCount;
// 	m_nDI_Count;
// 	m_nDO_Count;

	int iCol = (m_nDI_Count - 1) / m_nMaxRowCount + 1;

	//int iCtrlW		= iGroupWidth - (iMargin * 2);
	int iCtrlW		= (iGroupWidth - (iSpacing * (iCol - 1))) / iCol;
	int iStatusW	= 80; // 윈도우 크기에 따라서 가변
	int iItemW		= iCtrlW - iSpacing - iStatusW;

	m_st_DI.MoveWindow(0, 0, iGroupWidth, iCtrlH);
	m_st_DO.MoveWindow(iGroupWidth + iSpacing, 0, iGroupWidth, iCtrlH);

	iLeft		= 0;
	iLeftSub	= iLeft + iItemW + iSpacing;
	iTop		= iCtrlH + iSpacing;

	UINT nOffset = 0;

	for (int iIdx_Col = 0; iIdx_Col < iCol; iIdx_Col++)
	{
		for (int iDIIdx = 0; iDIIdx < (int)m_nMaxRowCount; iDIIdx++)
		{
			m_st_DI_Name[nOffset].MoveWindow(iLeft, iTop, iItemW, iCtrlH);
			m_st_DI_Status[nOffset].MoveWindow(iLeftSub, iTop, iStatusW, iCtrlH);
			iTop += iCtrlH + iSpacing;

			nOffset++;

			if (m_nDI_Count <= nOffset)
				break;
		}

		// Left 이동
		iLeft	 = iLeftSub + iStatusW + iSpacing;
		iLeftSub = iLeft + iItemW + iSpacing;
		iTop	 = iCtrlH + iSpacing;
	}


	iCol = (m_nDO_Count - 1) / m_nMaxRowCount + 1;
	
	iLeft		= iGroupWidth + iSpacing;
	iLeftSub	= iLeft + iItemW + iSpacing;
	iTop		= iCtrlH + iSpacing;

	nOffset = 0;

	for (int iIdx_Col = 0; iIdx_Col < iCol; iIdx_Col++)
	{
		for (int iDOIdx = 0; iDOIdx < (int)m_nMaxRowCount; iDOIdx++)
		{
			m_st_DO_Name[nOffset].MoveWindow(iLeft, iTop, iItemW, iCtrlH);
			m_bn_DO_Status[nOffset].MoveWindow(iLeftSub, iTop, iStatusW, iCtrlH);
			iTop += iCtrlH + iSpacing;

			nOffset++;

			if (m_nDO_Count <= nOffset)
				break;
		}

		// Left 이동
		iLeft = iLeftSub + iStatusW + iSpacing;
		iLeftSub = iLeft + iItemW + iSpacing;
		iTop = iCtrlH + iSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_IOTable::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_IOTable::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_BN_DOUT_SIGNAL;

	if (BST_CHECKED == m_bn_DO_Status[nIndex].GetCheck())
	{
		OnSet_DO_BitStatus(nIndex, TRUE);
	}
	else
	{
		OnSet_DO_BitStatus(nIndex, FALSE);
	}
}

//=============================================================================
// Method		: SetOnUpdateItem
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/28 - 14:17
// Desc.		:
//=============================================================================
void CWnd_IOTable::Update_DIO_ItemName()
{
	CString szText;
	
	if (NULL == GetSafeHwnd())
		return;

	if (NULL != m_psz_DI_Name)
	{
		for (UINT nDIIdx = 0; nDIIdx < m_nDI_Count; nDIIdx++)
		{
			szText.Format(_T("%02d : %s"), nDIIdx, m_psz_DI_Name[nDIIdx]);
			m_st_DI_Name[nDIIdx].SetText(szText);
		}
	}
	else
	{
		for (UINT nDIIdx = 0; nDIIdx < m_nDI_Count; nDIIdx++)
		{
			szText.Format(_T("%02d : "), nDIIdx);
			m_st_DI_Name[nDIIdx].SetText(szText);
		}
	}

	if (NULL != m_psz_DO_Name)
	{
		for (UINT nDOIdx = 0; nDOIdx < m_nDO_Count; nDOIdx++)
		{
			szText.Format(_T("%02d : %s"), nDOIdx, m_psz_DO_Name[nDOIdx]);
			m_st_DO_Name[nDOIdx].SetText(szText);
		}
	}
	else
	{
		for (UINT nDOIdx = 0; nDOIdx < m_nDO_Count; nDOIdx++)
		{
			szText.Format(_T("%02d : "), nDOIdx);
			m_st_DO_Name[nDOIdx].SetText(szText);
		}
	}
}

//=============================================================================
// Method		: OnSet_DO_BitStatus
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT nOffset
// Parameter	: __in BOOL bBitStatus
// Qualifier	:
// Last Update	: 2018/1/18 - 16:22
// Desc.		:
//=============================================================================
BOOL CWnd_IOTable::OnSet_DO_BitStatus(__in UINT nOffset, __in BOOL bBitStatus)
{
	if (m_pstDevice == NULL)
		return FALSE;

	BOOL bReturn = TRUE;

	if (bBitStatus)
	{
		bReturn = m_pstDevice->Set_DO_Status(nOffset, enum_IO_SignalType::IO_SignalT_SetOn);
	}
	else
	{
		bReturn = m_pstDevice->Set_DO_Status(nOffset, enum_IO_SignalType::IO_SignalT_SetOff);
	}

	return bReturn;
}

//=============================================================================
// Method		: Set_DIO_Count
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nDI_Count
// Parameter	: __in UINT nDO_Count
// Qualifier	:
// Last Update	: 2018/1/18 - 16:40
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_DIO_Count(__in UINT nDI_Count, __in UINT nDO_Count)
{
	m_nDI_Count = nDI_Count;
	m_nDO_Count = nDO_Count;
}

//=============================================================================
// Method		: Set_DIO_Name
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR * pszDI_name
// Parameter	: __in LPCTSTR * pszDO_Name
// Qualifier	:
// Last Update	: 2018/1/18 - 17:03
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_DIO_Name(__in LPCTSTR* pszDI_name, __in LPCTSTR* pszDO_Name)
{
	m_psz_DI_Name = pszDI_name;
	m_psz_DO_Name = pszDO_Name;
}

//=============================================================================
// Method		: Set_Color_BitSet
// Access		: public  
// Returns		: void
// Parameter	: __in COLORREF clrSet
// Qualifier	:
// Last Update	: 2018/1/18 - 17:24
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_Color_BitSet(__in COLORREF clrSet)
{
	m_clrSet = clrSet;
}

//=============================================================================
// Method		: Set_Color_BitClear
// Access		: public  
// Returns		: void
// Parameter	: __in COLORREF clrClear
// Qualifier	:
// Last Update	: 2018/1/18 - 17:24
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_Color_BitClear(__in COLORREF clrClear)
{
	m_clrClear = clrClear;
}

//=============================================================================
// Method		: Set_MaxRowCount
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2018/1/18 - 17:31
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_MaxRowCount(__in UINT nCount)
{
	m_nMaxRowCount = nCount;
}

//=============================================================================
// Method		: Set_IO_DI_OffsetData
// Access		: protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/10/31 - 17:01
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_IO_DI_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	if (m_nDI_Count <= byBitOffset)
		return;

	if (0 < bOnOff)
	{
		m_st_DI_Status[byBitOffset].SetColorStyle(CVGStatic::ColorStyle_Green);
		m_st_DI_Name[byBitOffset].SetBackColor_COLORREF(m_clrSet);
	}
	else
	{
		m_st_DI_Status[byBitOffset].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_DI_Name[byBitOffset].SetBackColor_COLORREF(m_clrClear);
	}
}

//=============================================================================
// Method		: Set_IO_DI_Data
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpbyDIData
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/10/31 - 17:33
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_IO_DI_Data(__in LPBYTE lpbyDIData, __in UINT nCount)
{
	UINT nLimitCnt = (m_nDI_Count <= nCount) ? m_nDI_Count : nCount;

	for (UINT nBitIdx = 0; nBitIdx < nLimitCnt; nBitIdx++)
	{
		Set_IO_DI_OffsetData(nBitIdx, lpbyDIData[nBitIdx]);
	}
}

//=============================================================================
// Method		: Set_IO_DO_OffsetData
// Access		: public  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/10/31 - 17:33
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_IO_DO_OffsetData(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	if (m_nDO_Count <= byBitOffset)
		return;

	if (0 < bOnOff)
	{
		m_bn_DO_Status[byBitOffset].SetWindowText(_T("Ⅴ"));
		m_st_DO_Name[byBitOffset].SetBackColor_COLORREF(m_clrSet);
		m_bn_DO_Status[byBitOffset].SetCheck(1);
	}
	else
	{
		m_bn_DO_Status[byBitOffset].SetWindowText(_T(""));
		m_st_DO_Name[byBitOffset].SetBackColor_COLORREF(m_clrClear);
		m_bn_DO_Status[byBitOffset].SetCheck(0);
	}
}

//=============================================================================
// Method		: Set_IO_DO_Data
// Access		: public  
// Returns		: void
// Parameter	: __in LPBYTE lpbyDOData
// Parameter	: __in UINT nCount
// Qualifier	:
// Last Update	: 2017/10/31 - 17:33
// Desc.		:
//=============================================================================
void CWnd_IOTable::Set_IO_DO_Data(__in LPBYTE lpbyDOData, __in UINT nCount)
{
	UINT nLimitCnt = (m_nDO_Count <= nCount) ? m_nDO_Count : nCount;

	for (UINT nBitIdx = 0; nBitIdx < nLimitCnt; nBitIdx++)
	{
		Set_IO_DO_OffsetData(nBitIdx, lpbyDOData[nBitIdx]);
	}
}

//=============================================================================
// Method		: SetPtr_Device
// Access		: virtual public  
// Returns		: void
// Parameter	: __in CDigitalIOCtrl * pstDevice
// Qualifier	:
// Last Update	: 2018/1/18 - 16:41
// Desc.		:
//=============================================================================
void CWnd_IOTable::SetPtr_Device(__in CDigitalIOCtrl* pstDevice)
{
	if (pstDevice == NULL)
		return;

	m_pstDevice = pstDevice;
};