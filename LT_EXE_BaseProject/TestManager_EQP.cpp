﻿//*****************************************************************************
// Filename	: 	TestManager_EQP.cpp
// Created	:	2016/5/9 - 13:32
// Modified	:	2016/08/10
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "TestManager_EQP.h"
#include "CommonFunction.h"
#include "File_Recipe.h"
#include "Def_Digital_IO.h"
#include "CRC16.h"

#include <Mmsystem.h>
#pragma comment (lib,"winmm.lib")

CTestManager_EQP::CTestManager_EQP()
{
	// 쓰레드 관련
	m_hThrTest_Manual	= NULL;
	m_hThr_LoadUnload	= NULL;
	m_hThrTest_All		= NULL;
	m_hThrTest_Monitor  = NULL;

	for (int iCnt = 0; iCnt < MAX_OPERATION_THREAD; iCnt++)
	{
		m_hThrTest_Unit[iCnt]	= NULL;
	}

	m_hEvent_ReqCapture		= NULL;
	m_hThrTest_Item			= NULL;

	m_bFlag_ReadyTest		= FALSE;
	m_bFlag_MotorMonitor	= FALSE;
	
	m_dwTimeCheck			= 0;

	OnInitialize();
}

CTestManager_EQP::~CTestManager_EQP()
{
	TRACE(_T("<<< Start ~CTestManager_EQP >>> \n"));

	this->OnFinalize();

	TRACE(_T("<<< End ~CTestManager_EQP >>> \n"));	
}


//=============================================================================
// Method		: OnLoadOption
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/9/28 - 20:04
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnLoadOption()
{
	BOOL bReturn = __super::OnLoadOption();

	if (!m_stOption.Inspector.szPath_Log.IsEmpty())
		m_stInspInfo.Path.szLog = m_stOption.Inspector.szPath_Log + _T("\\");

	if (!m_stOption.Inspector.szPath_Report.IsEmpty())
		m_stInspInfo.Path.szReport = m_stOption.Inspector.szPath_Report + _T("\\");

	if (!m_stOption.Inspector.szPath_Recipe.IsEmpty())
	{
		if (m_bUseForcedModel)
		{
			m_stInspInfo.Path.szRecipe.Format(_T("%s\\%s\\"), m_stOption.Inspector.szPath_Recipe.GetBuffer(0), g_szModelFolder[m_nModelType]);
		}
		else
		{
			m_stInspInfo.Path.szRecipe = m_stOption.Inspector.szPath_Recipe + _T("\\");
		}
	}
	
	if (!m_stOption.Inspector.szPath_Consumables.IsEmpty())
		m_stInspInfo.Path.szConsumables = m_stOption.Inspector.szPath_Consumables + _T("\\");

	if (!m_stOption.Inspector.szPath_Motor.IsEmpty())
		m_stInspInfo.Path.szMotor = m_stOption.Inspector.szPath_Motor + _T("\\");

	if (!m_stOption.Inspector.szPath_Maintenance.IsEmpty())
		m_stInspInfo.Path.szMaintenance = m_stOption.Inspector.szPath_Maintenance + _T("\\");

	if (!m_stOption.Inspector.szPath_Image.IsEmpty())
		m_stInspInfo.Path.szImage = m_stOption.Inspector.szPath_Image + _T("\\");

	if (!m_stOption.Inspector.szPath_I2c.IsEmpty())
		m_stInspInfo.Path.szI2c = m_stOption.Inspector.szPath_I2c + _T("\\");

	if (!m_stOption.Inspector.szPath_I2cImage.IsEmpty())
		m_stInspInfo.Path.szI2cImage = m_stOption.Inspector.szPath_I2cImage + _T("\\");

	if (!m_stOption.Inspector.szPath_VisionImage.IsEmpty())
		m_stInspInfo.Path.szVisionImage = m_stOption.Inspector.szPath_VisionImage + _T("\\");

	// MES
	m_stInspInfo.szEquipmentID = m_stOption.MES.szEquipmentID;
	 
	OnUpdate_EquipmentInfo();
	return bReturn;
}

//=============================================================================
// Method		: InitDevicez
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in HWND hWndOwner
// Qualifier	:
// Last Update	: 2017/11/11 - 21:37
// Desc.		:
//=============================================================================
void CTestManager_EQP::InitDevicez(__in HWND hWndOwner /*= NULL*/)
{
	__super::InitDevicez(hWndOwner);
#ifndef MOTION_NOT_USE
	m_tm_Motion.SetLogMsgID(hWndOwner, WM_LOGMSG);
#endif
}

//=============================================================================
// Method		: SetEvent_EjectModule
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 22:13
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetEvent_EjectModule(__in UINT nParaIdx /*= 0*/)
{
	if (FALSE == SetEvent(m_hEvent_EjectModule[nParaIdx]))
	{
		TRACE(_T("Set PLC Out [Para: %d] SetEvent 실패!!\n"), nParaIdx);
	}
	else
	{
		TRACE(_T("Set PLC Out [Para: %d] SetEvent 성공!!\n"), nParaIdx);
	}
}

//=============================================================================
// Method		: ResetEvent_EjectModule
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/12 - 14:15
// Desc.		:
//=============================================================================
void CTestManager_EQP::ResetEvent_EjectModule(__in UINT nParaIdx /*= 0*/)
{
	if (FALSE == ResetEvent(m_hEvent_EjectModule[nParaIdx]))
	{
		TRACE(_T("Set PLC Out [Para: %d] ResetEvent 실패!!\n"), nParaIdx);
	}
	else
	{
		TRACE(_T("Set PLC Out [Para: %d] ResetEvent 성공!!\n"), nParaIdx);
	}
}


//=============================================================================
// Method		: SetEvent_ReqCapture
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/12/12 - 12:08
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetEvent_ReqCapture()
{
	if (FALSE == SetEvent(m_hEvent_ReqCapture))
	{
		TRACE(_T("Set Req Capture SetEvent 실패!!\n"));
	}
	else
	{
		TRACE(_T("Set Req Capture SetEvent 성공!!\n"));
	}
}

//=============================================================================
// Method		: ResetEvent_ReqCapture
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/12 - 14:15
// Desc.		:
//=============================================================================
void CTestManager_EQP::ResetEvent_ReqCapture()
{
	if (FALSE == ResetEvent(m_hEvent_ReqCapture))
	{
		TRACE(_T("Set Req Capture ResetEvent 실패!!\n"));
	}
	else
	{
		TRACE(_T("Set Req Capture] ResetEvent 성공!!\n"));
	}
}

//=============================================================================
// Method		: SetEvent_Button
// Access		: protected  
// Returns		: void
// Parameter	: __in enEvent_Button nButton
// Qualifier	:
// Last Update	: 2018/3/17 - 14:37
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetEvent_Button(__in enEvent_Button nButton)
{
	if (nButton < Event_Bn_MaxEnum)
	{
		if (FALSE == SetEvent(m_hEvent_Butoon[nButton]))
		{
			TRACE(_T("Set Button Event[%d] 실패!!\n"), nButton);
		}
		else
		{
			TRACE(_T("Set Button Event[%d] 성공!!\n"), nButton);
		}
	}
}

//=============================================================================
// Method		: ResetEvent_Button
// Access		: protected  
// Returns		: void
// Parameter	: __in enEvent_Button nButton
// Qualifier	:
// Last Update	: 2018/3/17 - 14:40
// Desc.		:
//=============================================================================
void CTestManager_EQP::ResetEvent_Button(__in enEvent_Button nButton)
{
	if (nButton < Event_Bn_MaxEnum)
	{
		if (FALSE == ResetEvent(m_hEvent_Butoon[nButton]))
		{
			TRACE(_T("Reset Button Event[%d] 실패!!\n"), nButton);
		}
		else
		{
			TRACE(_T("Reset Button Event[%d] 성공!!\n"), nButton);
		}
	}
}

//=============================================================================
// Method		: ResetEvent_Button_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/17 - 15:06
// Desc.		:
//=============================================================================
void CTestManager_EQP::ResetEvent_Button_All()
{
	for (UINT nIdx = 0; nIdx < Event_Bn_MaxEnum; nIdx++)
	{
		ResetEvent_Button((enEvent_Button)nIdx);
	}
}

//=============================================================================
// Method		: Wait_Event_Button
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in DWORD dwTimeout
// Qualifier	:
// Last Update	: 2018/3/17 - 14:02
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::Wait_Event_Button(__out UINT& nOutEventType, __in DWORD dwTimeout /*= 600000*/)
{
	TRACE("Wait_Event_Button()\n");

	LRESULT lReturn = RC_OK;

	// * Req Capture 이벤트 대기
	HANDLE	hEventz[3] = { NULL, };
	hEventz[0] = m_hEvent_Butoon[Event_Bn_Start];
	hEventz[1] = m_hEvent_Butoon[Event_Bn_Stop];
	hEventz[2] = m_hEvent_ForcedStop; //강제 정지

	// 모든 채널의 검사 쓰레드가 종료 될때까지 대기
	DWORD dwEvent = WaitForMultipleObjects(3, hEventz, FALSE, dwTimeout);

	switch (dwEvent)
	{
	case WAIT_OBJECT_0:	// Start 버튼 
		lReturn = RC_OK;
		nOutEventType = Event_Bn_Start;
		ResetEvent_Button(Event_Bn_Start);
		TRACE(_T("CTestManager_EQP::Wait_Event_Button() : Start Button Event !!\n"));
		break;

	case WAIT_OBJECT_0 + 1:	// Stop 버튼
		lReturn = RC_OK;
		nOutEventType = Event_Bn_Stop;
		ResetEvent_Button(Event_Bn_Stop);
		TRACE(_T("CTestManager_EQP::Wait_Event_Button() : Stop Button Event !!\n"));
		break;

	case WAIT_OBJECT_0 + 2:	// 강제 작업 중지 이벤트 발생
		TRACE(_T("CTestManager_EQP::Wait_Event_Button() : Forced Stop Event !!\n"));
		lReturn = RC_Forced_Stoped;
		break;

	case WAIT_TIMEOUT:
		TRACE(_T("CTestManager_EQP::Wait_Event_Button() : Timeout !!\n"));
		lReturn = RC_WaitPLCOut_TimeOut;
		break;

	case WAIT_FAILED:
		TRACE(_T("CTestManager_EQP::Wait_Event_Button() : Wait Failed !!\n"));
		lReturn = RC_WaitPLCOut_Wait_Faild;
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: StartOperation_LoadUnload
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:19
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartOperation_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	if (FALSE == m_bFlag_ReadyTest)
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("검사가 진행 가능한 상태가 아닙니다."));
		return RC_NotReadyTest;
	}

	if (TRUE == bLoad)
	{
		lReturn = OnCheck_SaftyJIG();
		if (RC_OK != lReturn)
		{
			return lReturn;
		}

		lReturn = MES_CheckBarcodeValidation();
		if (RC_OK != lReturn)
		{
			if (RC_NoBarcode == lReturn)
			{
				AfxMessageBox(_T("No Barcode"), MB_SYSTEMMODAL);
			}
			else if (RC_MES_Err_BarcodeValidation == lReturn)
			{
				AfxMessageBox(_T("MES : Barcode is not Validation"), MB_SYSTEMMODAL);
			}

			return lReturn;
		}

		if (IsTesting())
		{
			TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
			OnLog_Err(_T("Inspection is in progress."));
			return RC_AlreadyTesting;
		}
	}

	// 쓰레드 핸들 리셋
	if (NULL != m_hThr_LoadUnload)
	{
		CloseHandle(m_hThr_LoadUnload);
		m_hThr_LoadUnload = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner		= this;
	pParam->nIndex		= 0;
	pParam->nCondition	= bLoad;
	pParam->nArg_2		= 0;	

	m_hThr_LoadUnload = HANDLE(_beginthreadex(NULL, 0, ThreadLoadUnload, pParam, 0, NULL));

	::SetThreadPriority(m_hThr_LoadUnload, THREAD_PRIORITY_HIGHEST);

	return RC_OK;
}

//=============================================================================
// Method		: StartOperation_Inspection
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:21
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartOperation_Inspection(__in UINT nParaIdx /*= 0*/)
{
	if (FALSE == m_bFlag_ReadyTest)
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("검사가 진행 가능한 상태가 아닙니다."));
		return RC_NotReadyTest;
	}

	if (IsTesting_All())
	{
		TRACE(_T("다른쪽 파라에서 검사가 진행 중이므로 진행 불가.\n"));
		OnLog_Err(_T("Inspection is in progress."));
		return RC_AlreadyTesting;
	}

	if (NULL != m_hThrTest_All)
	{
		CloseHandle(m_hThrTest_All);
		m_hThrTest_All = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner		= this;
	pParam->nIndex		= nParaIdx;
	pParam->nCondition	= 0;
	pParam->nArg_2		= 0;

	m_hThrTest_All = HANDLE(_beginthreadex(NULL, 0, ThreadTest_All, pParam, 0, NULL));

	return RC_OK;
}

//=============================================================================
// Method		: StartOperation_Test_Unit
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in enThreadTestType nTestType
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/14 - 13:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartOperation_Test_Unit(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx /*= 0*/)
{
	if (FALSE == m_bFlag_ReadyTest)
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("검사가 진행 가능한 상태가 아닙니다."));
		return RC_NotReadyTest;
	}

	// 테스트 가능 여부 판단
	if (IsTesting_Unit(nUnitIdx))
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("Inspection is in progress."));
		return RC_AlreadyTesting;
	}

	// 쓰레드 핸들 리셋
	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		CloseHandle(m_hThrTest_Unit[nUnitIdx]);
		m_hThrTest_Unit[nUnitIdx] = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner		= this;
	pParam->nIndex		= nUnitIdx;
	pParam->nCondition	= (UINT)nTestType;
	pParam->nArg_2		= nStepIdx;

	m_hThrTest_Unit[nUnitIdx] = HANDLE(_beginthreadex(NULL, 0, ThreadTest_Unit, pParam, 0, NULL));

	::SetThreadPriority(m_hThrTest_Unit[nUnitIdx], THREAD_PRIORITY_HIGHEST);

	return RC_OK;
}

//=============================================================================
// Method		: StartOperation_Manual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartOperation_Manual(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	if (IsTesting_Manual())
	{
		//AfxMessageBox(_T("Inspection is in progress.\r\nTry it after the Test is finished."), MB_SYSTEMMODAL);
		TRACE(_T("개별 TEST 진행중.\n"));
		OnLog_Err(_T("Manual Test is Already."));
		return RC_AlreadyTesting;
	}

	if (NULL != m_hThrTest_Manual)
	{
		CloseHandle(m_hThrTest_Manual);
		m_hThrTest_Manual = NULL;
	}

	stThreadParam* pParam = new stThreadParam;
	pParam->pOwner		= this;
	pParam->nIndex		= nParaIdx;
	pParam->nStepIndex	= nStepIdx;
//	pParam->nCondition	= bLoad;
	pParam->nArg_2		= 0;

	m_hThrTest_Manual = HANDLE(_beginthreadex(NULL, 0, ThreadManualTest, pParam, 0, NULL));

	return RC_OK;
}

//=============================================================================
// Method		: StartOperation_InspManual
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		: 이미지 검사 개별 테스트용
//=============================================================================
LRESULT CTestManager_EQP::StartOperation_InspManual(__in UINT nParaIdx /*= 0*/)
{
	return RC_OK;
}

//=============================================================================
// Method		: StartOperation_Monitoring
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/12 - 14:13
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartThread_Monitoring()
{
	if (NULL != m_hThrTest_Monitor)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Monitor, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return RC_AlreadyTestItem_Thread;
		}
	}

	if (NULL != m_hThrTest_Monitor)
	{
		CloseHandle(m_hThrTest_Monitor);
		m_hThrTest_Monitor = NULL;
	}

	m_hThrTest_Monitor = HANDLE(_beginthreadex(NULL, 0, Thread_MotorMonitoring, this, 0, NULL));

	return RC_OK;

}

//=============================================================================
// Method		: StopProcess_LoadUnload
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/19 - 19:35
// Desc.		:
//=============================================================================
void CTestManager_EQP::StopProcess_LoadUnload()
{
	if (NULL != m_hThr_LoadUnload)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_LoadUnload, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThr_LoadUnload, dwExitCode);
			WaitForSingleObject(m_hThr_LoadUnload, WAIT_ABANDONED);
			CloseHandle(m_hThr_LoadUnload);
			m_hThr_LoadUnload = NULL;
		}
	}
}

//=============================================================================
// Method		: StopProcess_Test_All
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/6/10 - 21:14
// Desc.		:
//=============================================================================
void CTestManager_EQP::StopProcess_Test_All()
{
	m_stInspInfo.bForcedStop = TRUE;
	SetEvent_ForcedStop();
	
	//OnSet_TestProgress(TP_Loading);

	// 	StopProcess_LoadUnload(nIdx);

	// 	if (NULL != m_hThrTest_All)
	// 	{
	// 		DWORD dwExitCode = NULL;
	// 		GetExitCodeThread(m_hThrTest_All, &dwExitCode);
	// 
	// 		if (STILL_ACTIVE == dwExitCode)
	// 		{
	// 			TerminateThread(m_hThrTest_All, dwExitCode);
	// 			WaitForSingleObject(m_hThrTest_All, WAIT_ABANDONED);
	// 			CloseHandle(m_hThrTest_All);
	// 			m_hThrTest_All = NULL;
	// 		}
	// 	}

}

//=============================================================================
// Method		: StopProcess_Test_Unit
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2018/2/14 - 13:34
// Desc.		:
//=============================================================================
void CTestManager_EQP::StopProcess_Test_Unit(__in UINT nUnitIdx)
{
	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Unit[nUnitIdx], &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			TerminateThread(m_hThrTest_Unit[nUnitIdx], dwExitCode);
			WaitForSingleObject(m_hThrTest_Unit[nUnitIdx], WAIT_ABANDONED);
			CloseHandle(m_hThrTest_Unit[nUnitIdx]);
			m_hThrTest_Unit[nUnitIdx] = NULL;
		}
	}
}

//=============================================================================
// Method		: StopProcess_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/20 - 11:15
// Desc.		:
//=============================================================================
void CTestManager_EQP::StopProcess_InspManual(__in UINT nParaIdx /*= 0*/)
{

}

//=============================================================================
// Method		: ThreadLoadUnload
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2016/5/18 - 17:37
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadLoadUnload(__in LPVOID lParam)
{
	CTestManager_EQP*	pThis		= (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	BOOL				bLoad		= ((stThreadParam*)lParam)->nCondition;

	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess_LoadUnload(bLoad);
		
	//_endthreadex(0);
	return 0;
}

//=============================================================================
// Method		: ThreadTest_All
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2016/5/26 - 10:17
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadTest_All(__in LPVOID lParam)
{
	CTestManager_EQP*	pThis		= (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	UINT				nParaIdx	= ((stThreadParam*)lParam)->nIndex;

	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess_Test_All(nParaIdx);

	return 0;
}

//=============================================================================
// Method		: ThreadTest_Unit
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2018/2/14 - 13:34
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadTest_Unit(__in LPVOID lParam)
{
	CTestManager_EQP*	pThis = (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	UINT				nUnitIdx = ((stThreadParam*)lParam)->nIndex;
	enThreadTestType	nTestType = (enThreadTestType)((stThreadParam*)lParam)->nCondition;
	UINT				nStepIdx = ((stThreadParam*)lParam)->nArg_2;


	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess_Test_Unit(nUnitIdx, nTestType, nStepIdx);

	//_endthreadex(0);
	return 0;
}

//=============================================================================
// Method		: AutomaticProcess_LoadUnload
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:23
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::AutomaticProcess_LoadUnload(__in BOOL bLoad)
{
	TRACE(_T("=-=-= %s 작업 시작 =-=-=\n"), g_szProductLoad[bLoad]);
	OnLog(_T("=-=-= Start %s =-=-="), g_szProductLoad[bLoad]);
	
	LRESULT lReturn = RC_OK;
	__try
	{
		OnInitial_LoadUnload(bLoad);

		lReturn = OnStart_LoadUnload(bLoad);
	}
	__finally
	{
		if (RC_OK == lReturn)
			OnFinally_LoadUnload(bLoad);
		else
			OnFinally_LoadUnload(bLoad);

		// 불량 코드
		if ((RC_OK != lReturn) && (RC_TestSkip != lReturn))
		{
			m_stInspInfo.Set_ResultCode(lReturn);
		}
	}

	if (RC_OK == lReturn)
	{
		TRACE(_T("=-=-= Complete %s =-=-=\n"), g_szProductLoad[bLoad]);
		OnLog(_T("=-=-= Complete %s =-=-="), g_szProductLoad[bLoad]);
		return TRUE;
	}
	else
	{
		TRACE(_T("=-=-= Stop %s : Code -> %d =-=-=\n"), g_szProductLoad[bLoad], lReturn);
		OnLog(_T("=-=-= Stop %s : Code -> %d =-=-="), g_szProductLoad[bLoad], lReturn);
		return FALSE;
	}
}

//=============================================================================
// Method		: ThreadManualTest
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2018/2/25 - 11:18
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::ThreadManualTest(__in LPVOID lParam)
{
	CTestManager_EQP*	pThis = (CTestManager_EQP*)((stThreadParam*)lParam)->pOwner;
	UINT				nParaIdx = ((stThreadParam*)lParam)->nIndex;
	UINT				nStepIdx = ((stThreadParam*)lParam)->nStepIndex;

	if (NULL != lParam)
		delete lParam;

	pThis->OnManualTestItem(nParaIdx, nStepIdx);

	//_endthreadex(0);
	return 0;
}

//=============================================================================
// Method		: Thread_MoterMonitoring
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2018/3/12 - 13:43
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::Thread_MotorMonitoring(__in LPVOID lParam)
{
	CTestManager_EQP* pThis = (CTestManager_EQP*)lParam;

	HANDLE hEvent[2] = { NULL, NULL };
	hEvent[0] = pThis->m_hEvent_ProgramExit;

	if (NULL != pThis->m_hEvent_ForcedStop)
	{
		hEvent[1] = pThis->m_hEvent_ForcedStop;
	}

	BOOL bContinue = TRUE;
	DWORD dwEvent = 0;

	__try
	{
		while (bContinue)
		{
			// 종료 이벤트, 연결해제 이벤트 체크
			if (NULL != hEvent[1])
				dwEvent = WaitForMultipleObjects(2, hEvent, FALSE, 300);
			else
				dwEvent = WaitForSingleObject(pThis->m_hEvent_ProgramExit, 2000);

			switch (dwEvent)
			{
			case WAIT_OBJECT_0:	// Exit Program
				TRACE(_T(" -- 프로그램 종료 m_hInternalExitEvent 이벤트 감지 \n"));
				bContinue = FALSE;
				break;

			case WAIT_OBJECT_0 + 1:// Exit Program
				TRACE(_T(" -- 프로그램 종료 m_hExternalExitEvent 이벤트 감지 \n"));
				bContinue = FALSE;
				break;

			case WAIT_TIMEOUT:
				pThis->AutomaticProcess_Motor();
				break;
			}
		}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
		TRACE(_T("*** Exception Error : CBSocketClient::ThreadTryConnect()\n"));
	}


	return TRUE;
}

//=============================================================================
// Method		: AutomaticProcess_Test_All
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:26
// Desc.		:
//=============================================================================
void CTestManager_EQP::AutomaticProcess_Test_All(__in UINT nParaIdx /*= 0*/)
{
	TRACE(_T("=-=-= [%d] Start Test operation =-=-=\n"), nParaIdx + 1);
	OnLog(_T("=-=-= [%d] Start Test operation =-=-="), nParaIdx + 1);

	LRESULT lReturn = RC_OK;

	__try
	{
		// 초기화
		OnInitial_Test_All(nParaIdx);	

		lReturn = OnStart_Test_All(nParaIdx);
	}
	__finally
	{
		// 작업 종료 처리
		if (RC_OK == lReturn)
		{
			OnFinally_Test_All(TRUE, nParaIdx);
			TRACE(_T("=-=-= [%d] Complete the entire test =-=-=\n"), nParaIdx + 1);
			OnLog(_T("=-=-= [%d] Complete the entire test =-=-="), nParaIdx + 1);
		}
		else
		{
			m_stInspInfo.CamInfo[nParaIdx].ResultCode = lReturn;

			OnFinally_Test_All(FALSE, nParaIdx);
			TRACE(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-=\n"), nParaIdx + 1, lReturn);
			OnLog(_T("=-=-= [%d] Stop Test activity : Code -> %d =-=-="), nParaIdx + 1), lReturn;


			// * 검사 결과 판정
			OnSet_TestResult_Unit(TR_Check, nParaIdx);
			OnUpdate_TestReport(nParaIdx);

			// * Unload Product
			lReturn = StartOperation_LoadUnload(Product_Unload);
			if (RC_OK != lReturn)
			{
				// 에러 상황
			}
		}
	}
}

//=============================================================================
// Method		: AutomaticProcess_Test_Unit
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in enThreadTestType nTestType
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/14 - 13:34
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::AutomaticProcess_Test_Unit(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx)
{
	TRACE(_T("=-=-= [%d] Test Unit 작업 시작 =-=-=\n"), nUnitIdx + 1);
//	OnLog(_T("=-=-= [%d] Start Test Unit =-=-="), nUnitIdx + 1);

	LRESULT lReturn = RC_OK;
	__try
	{
		lReturn = OnStart_Test_Unit(nUnitIdx, nTestType, nStepIdx);
	}
	__finally
	{

	}

	if (RC_OK == lReturn)
	{
		TRACE(_T("=-=-= [%d] Complete Test Unit =-=-=\n"), nUnitIdx + 1);
		OnLog(_T("=-=-= [%d] Complete Test Unit =-=-="), nUnitIdx + 1);
		return TRUE;
	}
	else
	{
		TRACE(_T("=-=-= [%d] Stop Test Unit : Code -> %d =-=-=\n"), nUnitIdx + 1, lReturn);
		OnLog(_T("=-=-= [%d] Stop Test Unit : Code -> %d =-=-="), nUnitIdx + 1, lReturn);
		return FALSE;
	}
}

//=============================================================================
// Method		: AutomaticProcess_Test_InspManual
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:06
// Desc.		:
//=============================================================================
void CTestManager_EQP::AutomaticProcess_Test_InspManual(__in UINT nParaIdx /*= 0*/)
{
	TRACE(_T("=-=-= [%d] Start InspManual Test operation =-=-=\n"), nParaIdx + 1);
	OnLog(_T("=-=-= [%d] Start InspManual Test operation =-=-="), nParaIdx + 1);

	LRESULT lReturn = RC_OK;

	__try
	{
		// 초기화
		OnInitial_Test_InspManual(nParaIdx);

		lReturn = OnStart_Test_InspManual(nParaIdx);
	}
	__finally
	{
		// 작업 종료 처리
		if (RC_OK == lReturn)
		{
			OnFinally_Test_InspManual(TRUE, nParaIdx);
			TRACE(_T("=-=-= [%d] Complete the entire  InspManual test =-=-=\n"), nParaIdx + 1);
			OnLog(_T("=-=-= [%d] Complete the entire  InspManual test =-=-="), nParaIdx + 1);
		}
		else
		{
			m_stInspInfo.CamInfo[nParaIdx].ResultCode = lReturn;

			OnFinally_Test_InspManual(FALSE, nParaIdx);
			TRACE(_T("=-=-= [%d] Stop  InspManual Test activity : Code -> %d =-=-=\n"), nParaIdx + 1, lReturn);
			OnLog(_T("=-=-= [%d] Stop  InspManual Test activity : Code -> %d =-=-="), nParaIdx + 1), lReturn;
		}
	}
}

//=============================================================================
// Method		: AutomaticProcess_Moter
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/12 - 13:44
// Desc.		:
//=============================================================================
void CTestManager_EQP::AutomaticProcess_Motor()
{
#ifndef MOTION_NOT_USE
	double dbDistance = 0.0;
	double dbChartTX = 0.0;
	double dbChartTZ = 0.0;
		
	CString szDistance;
	CString szChartTX;
	CString szChartTZ;

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:

		dbDistance = m_Device.MotionManager.GetCurrentPos(AX_2D_StageY);
		dbChartTX  = m_Device.MotionManager.GetCurrentPos(AX_2D_ChartR);
		szDistance.Format(_T("%.f mm"), m_stInspInfo.MaintenanceInfo.stTeachInfo.dbTeachData[TC_2D_Distance] - dbDistance + m_stInspInfo.MaintenanceInfo.stTeachInfo.dbTeachData[TC_2D_Center_Y]);
		szChartTX.Format(_T("%.f˚"), dbChartTX);

		switch (m_stInspInfo.RecipeInfo.ModelType)
		{
		case Model_OMS_Front:
			break;

		case Model_OMS_Front_Set:
			break;
		
		case Model_OMS_Entry:
			break;

		default:
			break;
		}

		break;

	case Sys_IR_Image_Test:
		//dbDistance = m_Device.MotionManager.GetCurrentPos(AX_IR_Image_StageY) / 1000;
		//szDistance.Format(_T("%.f mm"), dbDistance + m_stInspInfo.MaintenanceInfo.stTeachInfo.dbTeachData[TC_IR_Imgt_Distance]);
		break;

	case Sys_Image_Test:

		dbDistance = m_Device.MotionManager.GetCurrentPos(AX_Image_StageY) / 1000;
		szDistance.Format(_T("%.f mm"), dbDistance + m_stInspInfo.MaintenanceInfo.stTeachInfo.dbTeachData[TC_Imgt_Distance]);
		break;

	default:
		break;
	}

	SetMoterMoniter(szDistance, szChartTX, szChartTZ);
#endif
}

//=============================================================================
// Method		: OnInitial_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:28
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitial_LoadUnload(__in BOOL bLoad)
{
	if (bLoad)	// Loading
	{
		// * Reset Alarm
		//OnResetAlarm();
		
		// * 데이터 초기화
		OnResetInfo_Loading();

		// * 투입 시간 설정
		OnSet_InputTime();

		// * Status : Loading
		OnSet_TestResult(TR_Ready);
		OnSet_TestProgress(TP_Loading);

		// * 경광등 검사 중 표시
		OnDOut_TowerLamp(enLampColor::Lamp_Yellow, Bit_Clear);
		OnDOut_TowerLamp(enLampColor::Lamp_Green, Bit_Set);
	}
	else	// Unloading
	{
		OnSet_TestProgress(TP_Unloading);
	}
}

//=============================================================================
// Method		: OnFinally_LoadUnload
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:29
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinally_LoadUnload(__in BOOL bLoad)
{
	if (bLoad)	// Loading
	{
		// 제품 로딩에 걸린 시간 체크
		//m_stInspInfo.SetEndTestTime();
	}
	else	// Unloading
	{
		// * 검사 결과 LOG, MES 데이터
		OnJugdement_And_Report_All();

		// 검사 결과 표시 (?)
		//OnSet_TestResult(m_stInspInfo.CamInfo[nParaIdx].nJudgment);

		// * 팔레트 배출 시간 체크
		OnSet_OutputTime();

		// * C/T, T/T 갱신
		OnSet_CycleTime();

		// 배출시 데이터 초기화
		OnResetInfo_Unloading();

		// * 상태 : 대기
		OnSet_TestProgress(TP_Ready);

		// * 경광등 검사 완료 표시
		OnDOut_TowerLamp(enLampColor::Lamp_Green, Bit_Clear);
		OnDOut_TowerLamp(enLampColor::Lamp_Yellow, Bit_Set);
	}
}

//=============================================================================
// Method		: OnStart_LoadUnload
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bLoad
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStart_LoadUnload(__in BOOL bLoad)
{
	LRESULT lReturn = RC_OK;

	if (bLoad)
	{
		lReturn = Load_Product();

		if (RC_OK != lReturn)
		{
			// * 에러 상황 표시
			//UnloadPallet(nParaIdx);
		}
	}
	else
	{
		lReturn = Unload_Product();
	}

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 19:36
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitial_Test_All(__in UINT nParaIdx /*= 0*/)
{
 	// 테스트 관련 UI 초기화
// 	OnResetInfo_StartTest();

	// 테스트 상태 : Run	
	m_stInspInfo.nTestPara = nParaIdx;
	OnSet_TestResult_Unit(TR_Testing, nParaIdx);
	OnSet_TestProgress_Unit(TP_Testing, nParaIdx);

	// 검사 시작 시간 설정
	OnSet_BeginTestTime(nParaIdx);	

	// 검사 중인 Barcode 표시
	//OnUpdate_Testing_Barcode(nParaIdx);

	// 채널 카메라 데이터 초기화
	m_stInspInfo.WorklistInfo.Reset();
}

//=============================================================================
// Method		: OnFinally_Test_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 13:05
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinally_Test_All(__in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{
	OnSet_EndTestTime(nParaIdx);

	OnJugdement_And_Report(nParaIdx);

	// 검사 결과 표시
	//OnSet_TestResult(m_stInspInfo.CamInfo[nParaIdx].nJudgment);
/*
	if (RC_OK)
	{
		OnJugdement_And_Report();
	}
	else
	{
		// 에러 코드 처리
		//OnPopupMessageTest_All((enResultCode)lResult);
	}
*/
}

//=============================================================================
// Method		: OnStart_Test_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 16:30
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStart_Test_All(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// * 안전 기능 체크
	if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgeLoading)
	{
		if (RC_OK == m_stInspInfo.CamInfo[nParaIdx].ResultCode)
			lReturn = RC_LoadingFailed;
		else
			lReturn = m_stInspInfo.CamInfo[nParaIdx].ResultCode;

		// * 임시 : 안전하게 배출하기 위하여
		// KHO  이거 지워도 되는 건지? (A:지우면 언로드가 안될때 있음 )
		Sleep(3000);

		return lReturn;
	}
		//
	// * 검사 시작 위치로 제품 이동
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		lReturn = OnMotion_MoveToTestPos(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황

			// 알람
			OnAddAlarm_F(_T("Motion : Move to Test Position [Para -> %s]"), g_szParaName[nParaIdx]);
			//return lReturn;
		}
	}	
	
	// * Step별로 검사 진행
	enTestResult nResult = TR_Pass;

	for (UINT nCamPara = 0; nCamPara < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; nCamPara++)
	{
		OnSetCamerParaSelect(nCamPara);

		for (UINT nTryCnt = 0; nTryCnt <= m_stInspInfo.RecipeInfo.nTestRetryCount; nTryCnt++)
		{
			// 검사기에 맞추어 검사 시작
			lReturn = StartTest_Equipment(nCamPara);

			// * 검사 결과 판정
			nResult = Judgment_Inspection(nCamPara);

			if ((TR_Pass == nResult) || (TR_Check == nResult))
			{
				break;
			}
			else
			{
				// 측정 데이터, UI 초기화
			}
		}
	
		// * 검사 결과 판정 정보 세팅 및 UI표시
		if ((m_stInspInfo.RecipeInfo.ModelType == Model_MRA2) && (nCamPara == Para_Left))
		{
			;
		}
		else
		{
			OnSet_TestResult_Unit(nResult, nParaIdx);
			OnUpdate_TestReport(nParaIdx);
		}	

		if ((Sys_Focusing == m_InspectionType) && (TR_Check == nResult))
		{
			break;
		}
	}

	Judgment_Inspection_All();

	// * MES 검사 결과 보고
	if (MES_Online == m_stInspInfo.MESOnlineMode)
	{
		lReturn = MES_SendInspectionData(nParaIdx);
		if (RC_OK != lReturn)
		{
			// 에러 상황
		}
	}

	// * Unload 위치로 Product 이동
// 	if (g_InspectorTable[m_InspectionType].UseMotion)
// 	{
// 		lReturn = OnMotion_MoveToStandbyPos(nParaIdx);
// 		if (RC_OK != lReturn)
// 		{
// 			// 에러 상황
// 
// 			// 알람
// 			OnAddAlarm_F(_T("Motion : Move to Standby Position [Para -> %s]"), g_szParaName[nParaIdx]);
// 		}
// 	}

	// * Unload Product
	lReturn = StartOperation_LoadUnload(Product_Unload);
	if (RC_OK != lReturn)
	{
		// 에러 상황
	}

	return lReturn;
}

//=============================================================================
// Method		: OnStart_Test_Unit
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nUnitIdx
// Parameter	: __in enThreadTestType nTestType
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/14 - 13:54
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStart_Test_Unit(__in UINT nUnitIdx, __in enThreadTestType nTestType, __in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	TRACE(_T("OnStart_Test_Unit [CH_%02d]\n"), nUnitIdx);

	switch (nTestType)
	{
	case TTT_Initialize:
		lReturn = _TI_Cm_Initialize(nStepIdx, nUnitIdx);
		break;

	case TTT_Fialize:
		lReturn = _TI_Cm_Finalize(nStepIdx, nUnitIdx);
		break;

	case TTT_CameraReboot:
		lReturn = _TI_Cm_CameraReboot(TRUE, nUnitIdx);
		break;

	case TTT_EEPROM_Write:
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnInitial_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:11
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitial_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{

}

//=============================================================================
// Method		: OnFinally_Test_InspManual
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nTestItemID
// Parameter	: __in LRESULT lResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/8 - 9:12
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinally_Test_InspManual(__in UINT nTestItemID, __in LRESULT lResult, __in UINT nParaIdx /*= 0*/)
{

}

//=============================================================================
// Method		: OnStart_Test_InspManual
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnStart_Test_InspManual(__in UINT nTestItemID, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// * 안전 기능 체크

	// * 검사 시작 위치로 Product 이동
	lReturn = OnMotion_MoveToTestPos(nParaIdx);
	if (RC_OK != lReturn)
	{
		// 에러 상황
		return lReturn;
	}

	// * Step별로 검사 진행 (VCSEL On/Off 검사기에 따라서)
	switch (m_InspectionType)
	{
	case SYS_FOCUSING:
		//lReturn = StartTest_Focusing_TestItem(nTestItemID, 0);
		break;

	case SYS_2D_CAL:
		//lReturn = StartTest_2D_CAL_TestItem(nTestItemID, nParaIdx);
		break;

	case SYS_IMAGE_TEST:
		//lReturn = StartTest_ImageTest_TestItem(nTestItemID, nParaIdx);
		break;

	case Sys_IR_Image_Test:
		//lReturn = StartTest_SteCAL_TestItem(nTestItemID, 0);
		break;

	case SYS_3D_CAL:
		//lReturn = StartTest_3D_CAL_TestItem(nTestItemID, nParaIdx);
		break;

	default:
		break;
	}

	// * 검사 결과 판정
	enTestResult nResult = Judgment_Inspection();
	OnSet_TestResult_Unit(nResult, nParaIdx);
	OnUpdate_TestReport(nParaIdx);

	return lReturn;
}

//=============================================================================
// Method		: Load_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::Load_Product()
{
	LRESULT lReturn = RC_OK;

#ifndef USE_TEST_MODE	

	// * 안전 기능 체크 (Main Power, EMO, Area Sensor, Door Sensor)
	lReturn = OnDIn_CheckSafetyIO();
	if (RC_OK != lReturn) // 에러 상황
	{
		// 알람
		OnAddAlarm_F(_T("Motion : Safety Error [%s]"), g_szResultCode[lReturn]);
		//return lReturn;
	}

	// * JIG Cover 체크 (2D CAL)
	if ((Sys_2D_Cal == m_InspectionType) || (Sys_IR_Image_Test == m_InspectionType) || (Sys_Image_Test == m_InspectionType))
	{
		lReturn = OnDIn_CheckJIGCoverStatus();
		if (RC_OK != lReturn) // 에러 상황
		{
			// 알람
			OnAddAlarm_F(_T("Motion : JIG Cover Opened"));
			//return lReturn;
		}
	}

#endif

	// * 통신 초기화, 전원 On, 광원 On

	// * 최종 정상이 아니면 CHECK 판정 후 배출한다.
 	if (RC_OK == lReturn)
 	{
		// * 모터 제어	
		lReturn = OnMotion_LoadProduct();
		if (RC_OK != lReturn) // 에러 상황
		{
			// 알람
			OnAddAlarm_F(_T("Motion : Error Loading Product"));
			//return lReturn;
		}

		// * POGO PIN 컨택 확인,  POGO PIN 사용 횟수 증가
		Increase_ConsumCount();

 		// * 검사 시작 (쓰레드)
 		lReturn = StartOperation_Inspection(0);
 		if (RC_OK != lReturn) // 에러 상황
 		{
 			OnAddAlarm_F(_T("===== Can't Start Inspection ====="));
 			//return lReturn;
 		}
 	}
 	else	// 에러 상황
 	{
 		// * 검사 판정 NG or CHECK
 		m_stInspInfo.CamInfo[Para_Left].nJudgeLoading	= TR_Fail;
 		m_stInspInfo.CamInfo[Para_Right].nJudgeLoading	= TR_Fail;
 		m_stInspInfo.CamInfo[Para_Left].ResultCode		= lReturn;
 		m_stInspInfo.CamInfo[Para_Right].ResultCode		= lReturn;
 
 		// * 검사 결과 판정 정보 세팅 및 UI표시
 		OnSet_TestResult_Unit(TR_Check, Para_Left);
 		OnSet_TestResult_Unit(TR_Check, Para_Right);
 		OnUpdate_TestReport(Para_Left);
 		OnUpdate_TestReport(Para_Right);
 		Judgment_Inspection_All();
 
 		AutomaticProcess_LoadUnload(Product_Unload);
 		//return lReturn;//RC_LoadingFailed
 	}

	return lReturn;
}

//=============================================================================
// Method		: Unload_Product
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::Unload_Product()
{
	LRESULT lReturn = RC_OK;

	// * 제품 최종 판정
	//Judgment_Inspection_All();

	// * 광원 끄기 프로그램 끌때만 끈다.
	//OnLightBrd_PowerOff();

	// * 모션 제어
	if (g_InspectorTable[m_InspectionType].UseMotion)
	{
		// * Motion_UnloadPallet;
		lReturn = OnMotion_UnloadProduct();
		if (RC_OK != lReturn)
		{
			OnAddAlarm_F(_T("Motion : Unload Product"));
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: StartTest_Equipment
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/21 - 21:51
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartTest_Equipment(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_InspectionType)
	{
	case SYS_FOCUSING:
		//lReturn = StartTest_Focusing();
		break;

	case SYS_2D_CAL:
#ifdef USE_THREAD_CONER_PT
		//lReturn = StartTest_2D_CAL_WithThread(nParaIdx);
#else
		//lReturn = StartTest_2D_CAL(nParaIdx);
#endif
		break;

	case SYS_IMAGE_TEST:
		//lReturn = StartTest_ImageTest(nParaIdx);
		break;

	case Sys_IR_Image_Test:
		//lReturn = StartTest_StereoCAL();
		break;

	case SYS_3D_CAL:
		//lReturn = StartTest_3D_CAL(nParaIdx);
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: StartTest_Eqp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/26 - 14:44
// Desc.		:
//=============================================================================
// LRESULT CTestManager_EQP::StartTest_Eqp(__in UINT nParaIdx /*= 0*/)
// {
//  	LRESULT lReturn = RC_OK;
//  	
//  	// * 설정된 스텝 진행
//  	for (INT nStepIdx = 0; nStepIdx < iStepCnt; nStepIdx++)
//  	{
//  		// 스텝 진행 시간 체크
//  		dwElapTime = Check_TimeStepStart();
//  
//  		// UI 스텝 선택
//  		OnSet_TestStepSelect(nStepIdx, nParaIdx);
//  
//  		// * 모션 제어 / IO 제어
// 			if (0 < pStepInfo->StepList[nStepIdx].bUseChart_Rot || 0 < pStepInfo->StepList[nStepIdx].nMoveY)
// 			{
// 				lReturn = OnMotion_Eqp_Test(pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot, nParaIdx);
// 				if (lReturn != RC_OK)
// 				{
// 					// ERROR
// 					TRACE("[Motion Err] OnMotion_Eqp_Test(x:%d, y:%d, charR:%d)\r\n", pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
// 					OnLog_Err(_T("[Motion Err] OnMotion_Eqp_Test(x:%d, y:%d, charR:%d)"), pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
// 				}
// 			}
// 
// 			if (lReturn == RC_OK)
// 			{
// 				// * 검사 시작
// 				if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
// 				{
// 					lReturn = StartTest_2D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, 0, nParaIdx);
// 				}
// 			}
//  		
//  		// 스텝 진행 시간 체크
//  		dwElapTime = Check_TimeStepStop(dwElapTime);
//  
//  		// 진행 시간 설정
//  		m_stInspInfo.Set_ElapTime(nParaIdx, nStepIdx, dwElapTime);
//  
//  		// 스텝 결과 UI에 표시
//  		OnSet_TestStepResult(nStepIdx, nParaIdx);
//  
//  		// * 검사 프로그레스		
//  		OnSet_TestProgressStep((UINT)iStepCnt, nStepIdx + 1);
// 
// 			// UI 갱신 딜레이
// 			Sleep(100);
// 
// 			// 스텝 딜레이
// 			if (0 < pStepInfo->StepList[nStepIdx].dwDelay)
// 			{
// 				Sleep(pStepInfo->StepList[nStepIdx].dwDelay);
// 			}
// 
// 			if (bInitialize == FALSE)
// 			{
// 				lReturn = RC_NotReadyTest;
// 				break;
// 			}
// 
// 		return lReturn;
// }

//=============================================================================
// Method		: StartTest_PresetMode
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nPresetIndex
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/4/6 - 9:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartTest_PresetMode(__in UINT nPresetIndex, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	return lReturn;
}

//=============================================================================
// Method		: StartThread_TestItem
// Access		: protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 11:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartThread_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
	if (FALSE == m_bFlag_ReadyTest)
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("검사가 진행 가능한 상태가 아닙니다."));
		return RC_NotReadyTest;
	}

	if (NULL != m_hThrTest_Item)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Item, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return RC_AlreadyTestItem_Thread;
		}
	}

	if (NULL != m_hThrTest_Item)
	{
		CloseHandle(m_hThrTest_Item);
		m_hThrTest_Item = NULL;
	}

	stParam_TestItem* pParam = new stParam_TestItem;

	pParam->pOwner		= this;
	pParam->nParaIdx	= nParaIdx;
	pParam->nTestItemID = nTestItemID;
	pParam->nStepIdx	= nStepIdx;
	pParam->nArg_1		= nCornerStep;
	pParam->nArg_2		= 0;

	m_hThrTest_Item = HANDLE(_beginthreadex(NULL, 0, Thread_TestItem, pParam, 0, NULL));

	return RC_OK;
}

//=============================================================================
// Method		: Thread_TestItem
// Access		: protected static  
// Returns		: UINT WINAPI
// Parameter	: __in LPVOID lParam
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
UINT WINAPI CTestManager_EQP::Thread_TestItem(__in LPVOID lParam)
{
	CTestManager_EQP*	pThis		= (CTestManager_EQP*)((stParam_TestItem*)lParam)->pOwner;
	UINT				nParaIdx	= ((stParam_TestItem*)lParam)->nParaIdx;
	UINT				nTestItemID = ((stParam_TestItem*)lParam)->nTestItemID;
	UINT				nStepIdx	= ((stParam_TestItem*)lParam)->nStepIdx;
	UINT				nArg_1		= ((stParam_TestItem*)lParam)->nArg_1;	// Corner Idx
	UINT				nArg_2		= ((stParam_TestItem*)lParam)->nArg_2;

	if (NULL != lParam)
		delete lParam;

	pThis->AutomaticProcess_TestItem(nParaIdx, nTestItemID, nStepIdx, nArg_1);

	return 0;
}

//=============================================================================
// Method		: AutomaticProcess_TestItem
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Parameter	: __in UINT nTestItemID
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nCornerStep
// Qualifier	:
// Last Update	: 2017/12/12 - 13:31
// Desc.		:
//=============================================================================
void CTestManager_EQP::AutomaticProcess_TestItem(__in UINT nParaIdx, __in UINT nTestItemID, __in UINT nStepIdx, __in UINT nCornerStep /*= 0*/)
{
	if (TI_2D_Detect_CornerExt == nTestItemID)
	{
		//LRESULT lReturn = _TI_2DCAL_CornerExt(nStepIdx, nCornerStep, nParaIdx);
	}
}

//=============================================================================
// Method		: Wait_FinishThead_TestItem
// Access		: protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/12/12 - 13:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::Wait_FinishThead_TestItem()
{
	LRESULT lReturn = RC_OK;

	// * 검사 중인가?
	if (NULL != m_hThrTest_Item)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Item, &dwExitCode);

		if (STILL_ACTIVE != dwExitCode)
		{
			return TRUE;
		}
	}

	// * 검사 쓰레드 종료 대기
	HANDLE	hEventz[2] = { NULL, };
	hEventz[0] = m_hThrTest_Item;
	hEventz[1] = m_hEvent_ForcedStop; //작업자 정지

	// 모든 채널의 검사 쓰레드가 종료 될때까지 대기
	DWORD dwEvent = WaitForMultipleObjects(2, hEventz, FALSE, Timeout_WaitFinish_TestItem);

	switch (dwEvent)
	{
	case WAIT_OBJECT_0:	// 테스트 종료 됨
		lReturn = RC_OK;
		break;

	case WAIT_OBJECT_0 + 1:	// 강제 작업 중지 이벤트 발생
		TRACE(_T("CTestManager_EQP::Wait_FinishThead_TestItem() : Forced Stop Event !!\n"));
		lReturn = RC_Forced_Stoped;
		break;

	case WAIT_TIMEOUT:
		TRACE(_T("CTestManager_EQP::Wait_FinishThead_TestItem() : Timeout !!\n"));
		lReturn = RC_WaitEndTest_TimeOut;
		break;

	case WAIT_FAILED:
		TRACE(_T("CTestManager_EQP::Wait_FinishThead_TestItem() : Wait Failed !!\n"));
		lReturn = RC_WaitEndTest_Wait_Faild;
		break;

	default:
		break;
	}
	return TRUE;
}

//=============================================================================
// Method		: Wait_Event_ReqCapture
// Access		: protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/12/12 - 12:09
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::Wait_Event_ReqCapture()
{
	TRACE("Wait_Event_ReqCapture()\n");

	LRESULT lReturn = RC_OK;

	// * Req Capture 이벤트 대기
	HANDLE	hEventz[2] = { NULL, };
	hEventz[0] = m_hEvent_ReqCapture;
	hEventz[1] = m_hEvent_ForcedStop; //작업자 정지

	// 모든 채널의 검사 쓰레드가 종료 될때까지 대기
	DWORD dwEvent = WaitForMultipleObjects(2, hEventz, FALSE, Timeout_WaitPLCOut);

	switch (dwEvent)
	{
	case WAIT_OBJECT_0:	// 테스트 종료 됨
		lReturn = RC_OK;
		ResetEvent_ReqCapture();
		TRACE(_T("CTestManager_EQP::Wait_Event_ReqCapture() : Req Capture Event !!\n"));
		break;

	case WAIT_OBJECT_0 + 1:	// 강제 작업 중지 이벤트 발생
		TRACE(_T("CTestManager_EQP::Wait_Event_ReqCapture() : Forced Stop Event !!\n"));
		lReturn = RC_Forced_Stoped;
		break;

	case WAIT_TIMEOUT:
		TRACE(_T("CTestManager_EQP::Wait_Event_ReqCapture() : Timeout !!\n"));
		lReturn = RC_WaitPLCOut_TimeOut;
		break;

	case WAIT_FAILED:
		TRACE(_T("CTestManager_EQP::WaitForPLCOut() : Wait Failed !!\n"));
		lReturn = RC_WaitPLCOut_Wait_Faild;
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: StartThread_TestUnit_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enThreadTestType nThreadTestType
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/3/26 - 17:06
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::StartThread_TestUnit_All(__in enThreadTestType nThreadTestType, __in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (IsTesting_Unit(nParaIdx))
		{
			TRACE(_T("%s Error : Already Testing [%02d]\n"), g_szThreadTestType[nThreadTestType], nParaIdx);
			return RC_AlreadyTesting;
		}
	}

	//  개별 검사 쓰레드 실행
	HANDLE	hEventz[MAX_OPERATION_THREAD] = { NULL, };
	UINT	nThrCount = 0;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		// 제품 초기화 상태이거나 검사 진행이 Pass 상태이면 다음 검사 계속 진행
		if (StartOperation_Test_Unit(nParaIdx, nThreadTestType, nStepIdx))
		{
			hEventz[nThrCount++] = m_hThrTest_Unit[nParaIdx];
		}

		Sleep(50);
	}

	// 모든 쓰레드 종료 대기
	DWORD	dwWaitTime		= 60000;
	BOOL	bRet			= FALSE;
	BOOL	fDone			= TRUE;
	DWORD	dwStartTick		= GetTickCount();
	DWORD	dwChkTick		= 0;
	DWORD	dwResult		= 0;
	DWORD	dwCurrentTick	= 0;
	DWORD	dwEventCount	= 0;

	while (fDone)
	{
		dwCurrentTick = GetTickCount();
		if (dwStartTick <= dwCurrentTick)
		{
			dwChkTick = dwWaitTime - (dwCurrentTick - dwStartTick);
		}
		else
		{
			dwChkTick = dwWaitTime - (0xFFFFFFFF - dwStartTick + dwCurrentTick);
		}

		//DWORD dwEvent = WaitForMultipleObjects(nThrCount, hEventz, TRUE, 60000);
		dwResult = MsgWaitForMultipleObjects(nThrCount, hEventz, FALSE, dwChkTick, QS_ALLINPUT);
		switch (dwResult)
		{
// 		case WAIT_OBJECT_0:
// 			TRACE (_T("StartThread_TestUnit_All : MsgWaitForMultipleObjects is Done\n"));
// 			fDone = FALSE;
// 			bRet = TRUE;
// 			break;

		case WAIT_OBJECT_0 + USE_CHANNEL_CNT:
			//TRACE (_T("WaitACK is Message Event\n"));
			DoEvents();
			break;

		case WAIT_TIMEOUT:
			TRACE(_T("%s Error : WAIT_TIMEOUT \n"), g_szThreadTestType[nThreadTestType]);
			fDone = FALSE;
			bRet = FALSE;
			break;

		case WAIT_FAILED:
			TRACE(_T("%s Error : WAIT_FAILED \n"), g_szThreadTestType[nThreadTestType]);
			fDone = FALSE;
			bRet = FALSE;

		default:
			if (nThrCount <= (++dwEventCount))
			{
				TRACE (_T("StartThread_TestUnit_All : MsgWaitForMultipleObjects is Done\n"));
				fDone = FALSE;
				bRet = TRUE;
			}
			break;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/13 - 19:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_Initialize_All(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (IsTesting_Unit(nParaIdx))
		{
			TRACE(_T("_TI_Cm_Initialize_All() Error : Already Testing [%02d]\n"), nParaIdx);
			return RC_AlreadyTesting;
		}
	}

	//  개별 검사 쓰레드 실행
	HANDLE	hEventz[MAX_OPERATION_THREAD] = { NULL, };
	UINT	nThrCount = 0;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		// 제품 초기화 상태이거나 검사 진행이 Pass 상태이면 다음 검사 계속 진행
		if (StartOperation_Test_Unit(nParaIdx, TTT_Initialize, nStepIdx))
		{
			hEventz[nThrCount++] = m_hThrTest_Unit[nParaIdx];
		}

		Sleep(50);
	}

	// 모든 쓰레드 종료 대기
	DWORD dwEvent = WaitForMultipleObjects(nThrCount, hEventz, TRUE, 60000);
	if (WAIT_TIMEOUT == dwEvent)
	{
		TRACE(_T("_TI_Cm_Initialize_All() Error : WAIT_TIMEOUT \n"));
		; // 타임 아웃 처리?
	}
	else if (WAIT_FAILED == dwEvent)
	{
		TRACE(_T("_TI_Cm_Initialize_All() Error : WAIT_FAILED \n"));
		;
	}

	// 양불 판정
	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (FALSE == m_stInspInfo.CamInfo[nParaIdx].bInitialize)
		{
			lReturn = RC_UnknownError;
		}
	}

	return lReturn;
}

LRESULT CTestManager_EQP::_TI_Cm_Initialize_All_NoThread(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	//  개별 검사 쓰레드 실행
	lReturn = StartThread_TestUnit_All(TTT_Initialize, nStepIdx);

	// 양불 판정
	if (RC_OK == lReturn)
	{
		for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
		{
			if (FALSE == m_stInspInfo.CamInfo[nParaIdx].bInitialize)
			{
				lReturn = RC_UnknownError;
			}
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize_All
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2018/2/14 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_Finalize_All(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (IsTesting_Unit(nParaIdx))
		{
			TRACE(_T("_TI_Cm_Finalize_All() Error : Already Testing [%02d]\n"), nParaIdx);
			return RC_AlreadyTesting;
		}
	}

	//  개별 검사 쓰레드 실행
	HANDLE	hEventz[MAX_OPERATION_THREAD] = { NULL, };
	UINT	nThrCount = 0;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		// 제품 초기화 상태이거나 검사 진행이 Pass 상태이면 다음 검사 계속 진행
		if (StartOperation_Test_Unit(nParaIdx, TTT_Fialize, nStepIdx))
		{
			hEventz[nThrCount++] = m_hThrTest_Unit[nParaIdx];
		}

		Sleep(25);
	}

	// 모든 쓰레드 종료 대기
	DWORD dwEvent = WaitForMultipleObjects(nThrCount, hEventz, TRUE, 60000);
	if (WAIT_TIMEOUT == dwEvent)
	{
		TRACE(_T("_TI_Cm_Finalize_All() Error : WAIT_TIMEOUT \n"));
		; // 타임 아웃 처리?
	}
	else if (WAIT_FAILED == dwEvent)
	{
		TRACE(_T("_TI_Cm_Finalize_All() Error : WAIT_FAILED \n"));
		;
	}

	// 양불 판정
	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (FALSE == m_stInspInfo.CamInfo[nParaIdx].bFinalize)
		{
			lReturn = RC_UnknownError;
		}
	}

	return lReturn;
}

LRESULT CTestManager_EQP::_TI_Cm_Finalize_All_NoThread(__in UINT nStepIdx)
{
	LRESULT lReturn = RC_OK;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (IsTesting_Unit(nParaIdx))
		{
			TRACE(_T("_TI_Cm_Finalize_All() Error : Already Testing [%02d]\n"), nParaIdx);
			return RC_AlreadyTesting;
		}
	}

	//  개별 검사 쓰레드 실행
	lReturn = StartThread_TestUnit_All(TTT_Fialize, nStepIdx);	

	// 양불 판정
	if (RC_OK == lReturn)
	{
		for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
		{
			if (FALSE == m_stInspInfo.CamInfo[nParaIdx].bFinalize)
			{
				lReturn = RC_UnknownError;
			}
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraReboot_All
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/7 - 19:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_CameraReboot_All()
{
	LRESULT lReturn = RC_OK;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		if (IsTesting_Unit(nParaIdx))
		{
			TRACE(_T("_TI_Cm_CameraReboot_All() Error : Already Testing [%02d]\n"), nParaIdx);
			return RC_AlreadyTesting;
		}
	}

	//  개별 검사 쓰레드 실행
	HANDLE	hEventz[MAX_OPERATION_THREAD] = { NULL, };
	UINT	nThrCount = 0;

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		// 제품 초기화 상태이거나 검사 진행이 Pass 상태이면 다음 검사 계속 진행
		if (StartOperation_Test_Unit(nParaIdx, TTT_CameraReboot))
		{
			hEventz[nThrCount++] = m_hThrTest_Unit[nParaIdx];
		}

		Sleep(50);
	}

	// 모든 쓰레드 종료 대기
	DWORD dwEvent = WaitForMultipleObjects(nThrCount, hEventz, TRUE, 60000);
	if (WAIT_TIMEOUT == dwEvent)
	{
		TRACE(_T("_TI_Cm_CameraReboot_All() Error : WAIT_TIMEOUT \n"));
		; // 타임 아웃 처리?
	}
	else if (WAIT_FAILED == dwEvent)
	{
		TRACE(_T("_TI_Cm_CameraReboot_All() Error : WAIT_FAILED \n"));
		;
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Initialize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 21:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_Initialize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	TRACE(_T("_TI_Cm_Initialize [CH_%02d]\n"), nParaIdx);

	if (RC_OK == lReturn)
	{
		lReturn = OnMIU_SensorInit(nParaIdx);

		if (RC_OK == lReturn)
		{
			// * Register Write (Alpha ??)
			lReturn = OnMIU_CaptureStart(nParaIdx);

			if (RC_OK == lReturn)
			{
				// * 영상 신호 대기
				//lReturn = OnMIU_SignalStatus(nParaIdx);
				

				if (RC_OK != lReturn)
				{
					TRACE(_T("Error : OnDAQ_CheckVideoSignal [CH_%02d]\n"), nParaIdx);
					//OnAddAlarm_F(_T("Grabber : No Video Signal [CH_%02d]"), nParaIdx);
				}
			}
			else
			{
				TRACE(_T("Error : OnDAQ_CaptureStart [CH_%02d]\n"), nParaIdx);
				//OnAddAlarm_F(_T("Grabber : Capture Start Failed [CH_%02d]"), nParaIdx);
			}
		}
		else
		{
			TRACE(_T("Error : OnDAQ_SensorInit_Unit [CH_%02d]\n"), nParaIdx);
			//OnAddAlarm_F(_T("Grabber : Sensor Register Init Failed [CH_%02d]"), nParaIdx);
		}
	}
	else
	{
		TRACE(_T("Error : OnCameraBrd_PowerOnOff [CH_%02d]\n"), nParaIdx);
		OnAddAlarm_F(_T("Camera Board : Power On Failed [CH_%02d]"), nParaIdx);
	}

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		TRACE(_T("_TI_Cm_Initialize Suceed [CH_%02d]\n"), nParaIdx);

		m_stInspInfo.CamInfo[nParaIdx].bInitialize = TRUE;

		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		TRACE(_T("_TI_Cm_Initialize Failed [CH_%02d]\n"), nParaIdx);
		AddLog_F(_T("Camera : Initialize Failed [CH_%02d]"), nParaIdx);

		m_stInspInfo.CamInfo[nParaIdx].bInitialize = FALSE;

		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	m_stInspInfo.CamInfo[nParaIdx].nInitialize = (UINT)lReturn;

	Sleep(100);

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/20 - 10:26
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_Finalize(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// * Capture Off
	OnMIU_CaptureStop(nParaIdx);

	if (RC_OK == lReturn)
	{
		m_stInspInfo.CamInfo[nParaIdx].bFinalize = TRUE;

		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		m_stInspInfo.CamInfo[nParaIdx].bFinalize = FALSE;

		// * 에러 상황
		//m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_Finalize_Error
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/10 - 10:47
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_Finalize_Error(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// * Capture Off
	//lReturn = OnDAQ_CaptureStop(nParaIdx);

#ifndef USE_TEST_MODE
	// * Camera Power Off
	lReturn = OnCameraBrd_PowerOnOff(enPowerOnOff::Power_Off, nParaIdx);
#endif

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CameraReboot
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bUseCaptureOn
// Parameter	: __in UINT nParaIdx
// Parameter	: __in DWORD dwRebootDelay
// Qualifier	:
// Last Update	: 2018/3/16 - 13:18
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_CameraReboot(__in BOOL bUseCaptureOn, __in UINT nParaIdx /*= 0*/, __in DWORD dwRebootDelay /*= 500*/)
{
	LRESULT lReturn = RC_OK;

	// * Capture Off
	//lReturn = OnDAQ_CaptureStop(nParaIdx);

#ifndef USE_TEST_MODE
	// * Camera Power Off
	lReturn = OnCameraBrd_PowerOnOff(enPowerOnOff::Power_Off, nParaIdx);

	Sleep(dwRebootDelay);

	// * Camera Power On
	lReturn = OnCameraBrd_PowerOnOff(enPowerOnOff::Power_On, nParaIdx);
#endif

	if (RC_OK == lReturn)
	{
		Sleep(1000);
		// * 전류 측정으로 제품 감지?
		//OnCameraBrd_Read_Current(nParaIdx);

		// * Register Write (Alpha ??)
		lReturn = OnDAQ_SensorInit_Unit(nParaIdx);

		if (RC_OK == lReturn)
		{
			if (bUseCaptureOn)
			{
			// * Capture On
			lReturn = OnDAQ_CaptureStart(nParaIdx);

			if (RC_OK == lReturn)
			{
				// * 영상 신호 대기
				lReturn = OnDAQ_CheckVideoSignal(10000, nParaIdx);

				if (RC_OK != lReturn)
				{
					TRACE(_T("Error : OnDAQ_CheckVideoSignal [CH_%02d]\n"), nParaIdx);
				}
			}
			else
			{
				TRACE(_T("Error : OnDAQ_CaptureStart [CH_%02d]\n"), nParaIdx);
			}
		}
		}
		else
		{
			TRACE(_T("Error : OnDAQ_SensorInit_Unit [CH_%02d]\n"), nParaIdx);
		}
	}
	else
	{
		TRACE(_T("Error : OnCameraBrd_PowerOnOff [CH_%02d]\n"), nParaIdx);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_MeasurCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/7 - 19:43
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_MeasurCurrent(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP::_TI_Cm_MeasurVoltage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: _TI_Cm_CaptureImage
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Parameter	: __in BOOL bSaveImage
// Parameter	: __in LPCTSTR szFileName
// Qualifier	:
// Last Update	: 2018/3/1 - 11:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_CaptureImage(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/, __in BOOL bSaveImage /*= FALSE*/, __in LPCTSTR szFileName /*= NULL*/)
{
	LRESULT lReturn = RC_OK;

	// 이미지 캡쳐 (Request Capture)
	ST_VideoRGB* pstVideo = m_Device.DAQ_LVDS.GetRecvVideoRGB(nParaIdx);
	LPWORD lpwImage = (LPWORD)m_Device.DAQ_LVDS.GetSourceFrameData(nParaIdx);
	LPBYTE lpbyRGBImage = m_Device.DAQ_LVDS.GetRecvRGBData(nParaIdx);

	// 이미지 버퍼에 복사
	memcpy(m_stImageBuf[nParaIdx].lpwImage_16bit, lpwImage, pstVideo->m_dwWidth * pstVideo->m_dwHeight * sizeof(WORD));
	memcpy(m_stImageBuf[nParaIdx].lpbyImage_8bit, lpbyRGBImage, pstVideo->m_dwWidth * pstVideo->m_dwHeight * 3);

	// 이미지 저장
	if (bSaveImage)
	{
		CString szPath;
		CString szImgFileName;
		CString szFullPath;
		
		if (szFileName)
		{
			szImgFileName = szFileName;
		}
		else
		{
			SYSTEMTIME tmLocal;
			GetLocalTime(&tmLocal);

			szImgFileName.Format(_T("%04d%02d%02d_%02d%02d%02d"), tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay, tmLocal.wHour, tmLocal.wMinute, tmLocal.wSecond);
		}

		if (1 < g_InspectorTable[m_InspectionType].Grabber_Cnt)
			szPath.Format(_T("%s\\%s\\%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[nParaIdx].szBarcode, g_szParaName[nParaIdx]);
		else
			szPath.Format(_T("%s\\%s"), m_stInspInfo.Path.szImage, m_stInspInfo.CamInfo[nParaIdx].szBarcode);


		// /Left/모듈번호/
		MakeDirectory(szPath.GetBuffer(0));

		if (m_stOption.Inspector.bSaveImage_Gray12)
		{
			szFullPath.Format(_T("%s\\%s.png"), szPath, szImgFileName);
			OnSaveImage_Gray16(szFullPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpwImage_16bit, pstVideo->m_dwWidth, pstVideo->m_dwHeight);
		}

		if (m_stOption.Inspector.bSaveImage_RGB)
		{
			szFullPath.Format(_T("%s\\%s.bmp"), szPath, szImgFileName);
			OnSaveImage_RGB(szFullPath.GetBuffer(0), m_stImageBuf[nParaIdx].lpbyImage_8bit, pstVideo->m_dwWidth, pstVideo->m_dwHeight);
		}

		if (m_stOption.Inspector.bSaveImage_RAW)
		{
			szFullPath.Format(_T("%s\\%s.raw"), szPath, szImgFileName);
			m_Device.DAQ_LVDS.SaveRawImage(nParaIdx, szFullPath.GetBuffer(0));
		}
	}

	// 영상 캡쳐 이력에 추가
// 	CString szLabel;
// 	szLabel.Format(_T("%02d"), nCornerStep);
// 	OnImage_AddHistory(nParaIdx, szLabel);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		//m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		//m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP::_TI_Cm_CameraRegisterSet(__in UINT nStepIdx, __in UINT nRegisterType, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// 영상 Capture On이면 Off 시킨다.

	// * Register Write
	lReturn = OnDAQ_SensorInit_Unit(nParaIdx);

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		//m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		//m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP::_TI_Cm_CameraAlphaSet(__in UINT nStepIdx, __in UINT nAlpha, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	//m_Device.DAQ_LVDS.Set_UseAutoI2CWrite(nIdxBrd, FALSE);

	// Alpha 값에 따라서

	m_Device.DAQ_LVDS.SetI2CFilePath(nParaIdx, m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName);


	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

LRESULT CTestManager_EQP::_TI_Cm_CameraPowerOnOff(__in UINT nStepIdx, __in enPowerOnOff nOnOff, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	// 스텝 결과
	if (RC_OK == lReturn)
	{
		// * 검사 항목별 결과
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_OK);
	}
	else
	{
		// * 에러 상황
		m_stInspInfo.Set_Judgment(nParaIdx, nStepIdx, JUDGE_NG);
	}

	return lReturn;
}

//=============================================================================
// Method		: Judgment_Inspection
// Access		: protected  
// Returns		: enTestResult
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/27 - 19:43
// Desc.		:
//=============================================================================
enTestResult CTestManager_EQP::Judgment_Inspection(__in UINT nParaIdx /*= 0*/)
{
	enTestResult lReturn = TR_Pass;

	for (UINT nStepIdx = 0; nStepIdx < m_stInspInfo.GetStepMeasInfo(nParaIdx)->GetCount(); nStepIdx++)
	{
		if (m_stInspInfo.GetTestStep(nStepIdx)->bTest)
		{
			switch(m_stInspInfo.GetTestItemMeas(nStepIdx, nParaIdx)->nJudgmentAll)
			{
			case TR_Fail:
				lReturn = TR_Fail;
				return lReturn;
				break;

			case TR_Pass:
				break;

			case TR_Check:
				lReturn = TR_Check;
				return lReturn;
				break;

			case TR_Stop:
				lReturn = TR_Stop;
				return lReturn;
				break;
			
			default:
				lReturn = TR_Fail;
				return lReturn;
				break;
			}
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: Judgment_Inspection_All
// Access		: protected  
// Returns		: enTestResult
// Qualifier	:
// Last Update	: 2018/2/20 - 14:42
// Desc.		:
//=============================================================================
enTestResult CTestManager_EQP::Judgment_Inspection_All()
{
	enTestResult lReturn = TR_Pass;
	UINT nTestCount = 1;

	if (1 < g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt)
	{
		if ((Sys_Focusing == m_InspectionType) && (Model_OMS_Front == m_stInspInfo.RecipeInfo.ModelType))
		{
			nTestCount = 1;
		}
		else
		{
			nTestCount = g_IR_ModelTable[m_stInspInfo.RecipeInfo.ModelType].Camera_Cnt; // 2
		}
	}

	for (UINT nIdx = 0; nIdx < nTestCount; nIdx++)
	{
		if (TR_Pass != m_stInspInfo.CamInfo[nIdx].nJudgment)
		{
			lReturn = m_stInspInfo.CamInfo[nIdx].nJudgment;
			break;
		}
	}

	// 검사 최종 판정
	OnSet_TestResult(lReturn);

	return lReturn;
}

//=============================================================================
// Method		: Judgment_CheckStep
// Access		: protected  
// Returns		: enTestResult
// Parameter	: __in UINT nStepIdx
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 11:01
// Desc.		:
//=============================================================================
enTestResult CTestManager_EQP::Judgment_CheckStep(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	enTestResult lReturn = TR_Pass;

	if (m_stInspInfo.GetStepMeasInfo(nParaIdx)->GetCount() <= nStepIdx)
		return enTestResult::TR_Stop;

	for (UINT nStepIdx = 0; nStepIdx < nStepIdx; nStepIdx++)
	{
		//if (m_stInspInfo.GetTestStep(nStepIdx)->bTest)
		{
			switch (m_stInspInfo.GetTestItemMeas(nStepIdx, nParaIdx)->nJudgmentAll)
			{
			case TR_Fail:
				lReturn = TR_Fail;
				return lReturn;
				break;

			case TR_Pass:
				break;

			case TR_Check:
				lReturn = TR_Check;
				return lReturn;
				break;

			case TR_Stop:
				lReturn = TR_Stop;
				return lReturn;
				break;

			default:
				break;
			}
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: Check_TimeStepStart
// Access		: virtual protected  
// Returns		: DWORD
// Qualifier	:
// Last Update	: 2017/9/27 - 18:59
// Desc.		:
//=============================================================================
DWORD CTestManager_EQP::Check_TimeStepStart()
{
	return timeGetTime();
}

//=============================================================================
// Method		: Check_TimeStepStop
// Access		: virtual protected  
// Returns		: DWORD
// Parameter	: __in DWORD dwStart
// Qualifier	:
// Last Update	: 2018/3/13 - 14:08
// Desc.		:
//=============================================================================
DWORD CTestManager_EQP::Check_TimeStepStop(__in DWORD dwStart)
{
	DWORD dwTimeCheck = timeGetTime();
	DWORD dwDuration = 0;

	if (dwTimeCheck < dwStart)
	{
		dwDuration = 0xFFFFFFFF - dwStart + dwTimeCheck;
	}
	else
	{
		dwDuration = dwTimeCheck - dwStart;
	}

	return dwDuration;
}

//=============================================================================
// Method		: MES_CheckBarcodeValidation
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:25
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::MES_CheckBarcodeValidation(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	
	if (MES_Online == m_stInspInfo.MESOnlineMode)
	{
		if (m_stInspInfo.szBarcodeBuf.IsEmpty())
		{
			OnAddAlarm(_T("No Barcode"));

			return RC_NoBarcode;
		}

		if (FALSE == m_stInspInfo.CamInfo[0].bMES_Validation)
		{
			return RC_MES_Err_BarcodeValidation;
		}
	}
	else
	{
		if (m_stInspInfo.szBarcodeBuf.IsEmpty())
		{
			OnAddAlarm(_T("No Barcode"));

			return RC_NoBarcode;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: MES_SendInspectionData
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/4 - 16:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::MES_SendInspectionData(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn		= RC_OK;

	if (enOperateMode::OpMode_Production == m_stInspInfo.OperateMode)
	{
		switch (m_InspectionType)
		{
		case Sys_Focusing:
			//lReturn = MES_MakeInspecData_Foc(nParaIdx);
			break;

		case Sys_2D_Cal:
			//lReturn = MES_MakeInspecData_2D_CAL(nParaIdx);
			break;

		case Sys_Image_Test:
			//lReturn = MES_MakeInspecData_IQ(nParaIdx);
			break;

		case Sys_IR_Image_Test:
			//lReturn = MES_MakeInspecData_SteCAL(nParaIdx);
			break;

		case Sys_3D_Cal:
			//lReturn = MES_MakeInspecData_3D_CAL(nParaIdx);
			break;

		default:
			break;
		}

		// 파일 저장~


	}


	return lReturn;
}

//=============================================================================
// Method		: MES_MakeInspecData_2D_CAL
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/9 - 18:06
// Desc.		:
//=============================================================================
// LRESULT CTestManager_EQP::MES_MakeInspecData_Eqp(__in UINT nParaIdx /*= 0*/)
// {
// 	LRESULT lReturn = RC_OK;
// 
// 	return lReturn;
// }

//=============================================================================
// Method		: OnPopupMessageTest_All
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2016/7/21 - 18:59
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnPopupMessageTest_All(__in enResultCode nResultCode)
{
	if (RC_OK == nResultCode)
		return;

	switch (nResultCode)
	{
	case RC_UnknownError:
		break;
	case RC_OK:
		break;
	case RC_Invalid_Handle:
		break;
	case RC_Exception:
		break;

	case RC_LimitPogoCnt:
		OnLog_Err(_T("The pogo count is the maximum.\r\nCall an engineer."));
		//AfxMessageBox(_T("The pogo count is the maximum.\r\nCall an engineer."), MB_SYSTEMMODAL);
		break;

	case RC_Recipe_Err:
		OnLog_Err(_T("There is an error in the model settings.\r\nCall an engineer."));
		//AfxMessageBox(_T("There is an error in the model settings.\r\nCall an engineer."), MB_SYSTEMMODAL);
		break;

	case RC_NoBarcode:
		OnLog_Err(_T("No Barcode input.\r\nPlease Check Device."));
		//AfxMessageBox(_T("No Barcode input.\r\nPlease Check Device."), MB_SYSTEMMODAL);
		break;

	default:
		break;
	}
}


//=============================================================================
// Method		: OnMotion_SetOption_Model
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enModelType nModelType
// Qualifier	:
// Last Update	: 2018/2/22 - 11:29
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnMotion_SetOption_Model(__in enModelType nModelType)
{
#ifndef MOTION_NOT_USE
	m_tm_Motion.SetModelType(nModelType);
#endif
}

//=============================================================================
// Method		: OnMotion_MoveToTestPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/19 - 20:42
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnMotion_MoveToTestPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// * 2D Cal, 화질검사, 3D Cal
	// * Barcode, 이물 검사는 필요없음??
	switch (m_InspectionType)
	{
	case SYS_FOCUSING:
		break;
	case SYS_2D_CAL:
		lReturn = m_tm_Motion.OnActionStandby_2D_CAL(m_stOption.Inspector.bUseBlindShutter);
		break;
	case SYS_IMAGE_TEST:
		//lReturn = m_MotionSequence.OnActionStandby();
		break;
	case SYS_3D_CAL:
		// * 검사 시작 위치로 이동
//		lReturn = m_MotionSequence.OnActionTesting(nParaIdx, 0, 700);
		break;
	default:
		break;
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_MoveToStandbyPos
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/11/15 - 9:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnMotion_MoveToStandbyPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// * 2D Cal, 화질검사, 3D Cal
	// * Barcode, 이물 검사는 필요없음??
	switch (m_InspectionType)
	{
	case SYS_FOCUSING:
	case SYS_2D_CAL:
	case SYS_IMAGE_TEST:
	case SYS_3D_CAL:
		// * 반대편 쪽 파라가 검사 시작 할 수 있게 팔레트 Unloading 위치로 이동
//		lReturn = m_MotionSequence.OnActionStandby(nParaIdx);
		break;

	default:
		break;
	}
#endif

	return lReturn;
}

LRESULT CTestManager_EQP::OnMotion_MoveToUnloadPos(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// * 2D Cal, 화질검사, 3D Cal
	// * Barcode, 이물 검사는 필요없음??
	switch (m_InspectionType)
	{
	case SYS_FOCUSING:
	case SYS_2D_CAL:
	case SYS_IMAGE_TEST:
	case SYS_3D_CAL:
		// * 반대편 쪽 파라가 검사 시작 할 수 있게 팔레트 Unloading 위치로 이동
//		lReturn = m_MotionSequence.OnActionUnLoading(nParaIdx);
		break;

	default:
		break;
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_Origin
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:27
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnMotion_Origin()
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	// * 모터 점검

	// * 로딩된 팔레트가 있는가?

	// * 로딩된 팔레트 위치 체크

	// * 언로딩 위치로 팔레트 이동??

	lReturn = m_tm_Motion.MotorAllOriginStart();
#endif

	if (RC_OK != lReturn)
	{
		AfxMessageBox(g_szResultCode[lReturn], MB_SYSTEMMODAL);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_LoadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:05
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnMotion_LoadProduct()
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	
	lReturn = m_tm_Motion.OnActionLoad();

	if (RC_OK != lReturn)
	{
		OnLog_Err(_T("Motion : Loading Err"));

		OnAddAlarm_F(_T("Motion : Error Load Product"));
		return lReturn;
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_UnloadProduct
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/15 - 3:16
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnMotion_UnloadProduct()
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_tm_Motion.OnActionLoad();

	if (RC_OK != lReturn)
	{
		OnLog_Err(_T("Motion : Unloading Err"));

		OnAddAlarm_F(_T("Motion : Error Unload Product"));
		return lReturn;
	}

#endif

	return lReturn;
}

//=============================================================================
// Method		: OnMotion_EStop
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/7 - 19:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnMotion_EStop()
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
	lReturn = m_tm_Motion.MotorAllEStop();

#endif

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DetectSignal
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/1 - 17:22
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDIn_DetectSignal(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		OnDIn_DetectSignal_2DCAL(byBitOffset, bOnOff);
		break;

	case Sys_3D_Cal:
		break;
	case Sys_IR_Image_Test:
		OnDIn_DetectSignal_ImgT(byBitOffset, bOnOff);
		break;
	case Sys_Image_Test:
		OnDIn_DetectSignal_ImgT(byBitOffset, bOnOff);
		break;
	case Sys_Focusing:
		OnDIn_DetectSignal_Foc(byBitOffset, bOnOff);
		break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnDIn_DetectSignal_2DCAL
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/2/13 - 17:08
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDIn_DetectSignal_2DCAL(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	enDI_2DCal nDI_Signal = (enDI_2DCal)byBitOffset;

	switch (nDI_Signal)
	{
	case DI_2D_00_MainPower:
		if (FALSE == bOnOff)
		{
			OnDIn_MainPower();
		}
		break;

	case DI_2D_01_EMO:
		if (FALSE == bOnOff)
		{
			OnDIn_EMO();
		}
		break;

	case DI_2D_02_AreaSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_AreaSensor();
		}
		break;

	case DI_2D_03_DoorSensor:
		if (FALSE == bOnOff)
		{
			OnDIn_DoorSensor();
		}
		break;

	case DI_2D_04_JIG_CoverCheck:
		if (bOnOff)
		{
			OnDIn_JIGCoverCheck();
		}
		break;
	
	case DI_2D_06_Start:
	{
		if (bOnOff)
		{
			//StartOperation_AutoAll();
			StartOperation_LoadUnload(Product_Load);
		}
		
	}
		break;

	case DI_2D_07_Stop:
	{
		if (bOnOff)
		{
			StopProcess_Test_All();
		}

	}
		break;
	
	default:
		break;
	}

}

//=============================================================================
// Method		: OnDIn_DetectSignal_ImgT
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/3/5 - 12:53
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDIn_DetectSignal_ImgT(__in BYTE byBitOffset, __in BOOL bOnOff)
{

		}

//=============================================================================
// Method		: OnDIn_DetectSignal_Foc
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BYTE byBitOffset
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2018/3/5 - 12:53
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDIn_DetectSignal_Foc(__in BYTE byBitOffset, __in BOOL bOnOff)
{
	enDI_Focusing nDI_Signal = (enDI_Focusing)byBitOffset;
}

//=============================================================================
// Method		: OnDIO_InitialSignal
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/3/8 - 20:30
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDIO_InitialSignal()
{

}

//=============================================================================
// Method		: OnDIn_MainPower
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_MainPower()
{
	LRESULT lReturn = RC_OK;

	OnLog_Err(_T("I/O : Main Power Off !!"));
	OnAddAlarm_F(_T("I/O : Main Power Off !!"));

	OnDOut_TowerLamp(enLampColor::Lamp_Red, TRUE);
	OnDOut_Buzzer(TRUE);

	if (IsTesting())
	{
		StopProcess_Test_All();
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_EMO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_EMO()
{
	LRESULT lReturn = RC_OK;

	m_bFlag_ReadyTest = FALSE;
	m_stInspInfo.bEMO = TRUE;

	OnLog_Err(_T("I/O : EMO Occurred !!"));
	OnAddAlarm_F(_T("I/O : EMO Occurred  !!"));

	if (IsTesting())
	{
		StopProcess_Test_All();

		// 현재 검사 중인 제품은 모두 재검 처리
		//if (FALSE == m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
		//{
		//	// 검사 중인가?
		//	// 검사가 끝난 제품인가???
		//	if (m_stInspInfo.CamInfo[0].nJudgment == TR_Pass)
		//	{
		//		m_stInspInfo.CamInfo[0].nJudgment = TR_Rework;
		//	}
		//}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_AreaSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_AreaSensor()
{
	LRESULT lReturn = RC_OK;

	if (TRUE == m_stOption.Inspector.bUseAreaSensor_Err)
	{
		if (IsTesting())
		{
			//m_bFlag_ReadyTest = FALSE;

			// 모터 정지??
			OnMotion_EStop();

			OnLog_Err(_T("I/O : Detected Area Sesnor !!"));
			OnAddAlarm_F(_T("I/O : Detected Area Sesnor  !!"));

			// 검사 중지??
			StopProcess_Test_All();

		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_DoorSensor
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 20:12
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_DoorSensor()
{
	LRESULT lReturn = RC_OK;

 	if (m_stOption.Inspector.bUseDoorOpen_Err)
	{
		OnLog_Err(_T("I/O : Door is Opened !!"));
		OnAddAlarm_F(_T("I/O : Door is Opened !!"));

 		if (IsTesting())
 		{
			// 모터 정지??
			OnMotion_EStop();

			OnDOut_TowerLamp(enLampColor::Lamp_Red, TRUE);
			OnDOut_Buzzer(TRUE);

 			StopProcess_Test_All();

			// 현재 검사 중인 제품은 모두 재검 처리?
			//  	if (FALSE == m_stInspInfo.CamInfo[0].szBarcode.IsEmpty())
			//  	{
			//  		// 검사 중인가?
			//  		// 검사가 끝난 제품인가???
			//  		if (m_stInspInfo.CamInfo[0].nJudgment == TR_Pass)
			//  		{
			//  			m_stInspInfo.CamInfo[0].nJudgment = TR_Rework;
			//  		}
			//  	}
 		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_JIGCoverCheck
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 19:41
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_JIGCoverCheck()
{
	LRESULT lReturn = RC_OK;

	// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
	// * 검사 진행 중 커버 열리면 오류 처리
	if (IsTesting())
	{
		if (m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
		{
			lReturn = RC_DIO_Err_JIGCorverCheck;

			// 모터 정지??
			OnMotion_EStop();

			OnDOut_TowerLamp(enLampColor::Lamp_Red, TRUE);
			OnDOut_Buzzer(TRUE);

			OnLog_Err(_T("I/O : JIG Cover is Opened !!"));
			OnAddAlarm_F(_T("I/O : JIG Cover is Opened !!"));

			StopProcess_Test_All();

			AfxMessageBox(_T("Opened JIG Cover!!"), MB_SYSTEMMODAL);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Start
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/7 - 19:20
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_Start()
{
	LRESULT lReturn = RC_OK;

	lReturn = StartOperation_LoadUnload(TRUE);

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_Stop
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/7 - 19:20
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_Stop()
{
	LRESULT lReturn = RC_OK;

	if (Permission_Operator != m_stInspInfo.PermissionMode)
	{
		StopProcess_Test_All();
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckSafetyIO
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 18:36
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_CheckSafetyIO()
{
	// * 안전 기능 체크 (Main Power, EMO, Area Sensor, Door Sensor)
	LRESULT lReturn = RC_OK;

	if (FALSE == m_stInspInfo.byDIO_DI[DI_2D_00_MainPower])
	{
		lReturn = RC_DIO_Err_MainPower;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_2D_01_EMO])
	{
		lReturn = RC_DIO_Err_EMO;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_2D_02_AreaSensor])
	{
		lReturn = RC_DIO_Err_AreaSensor;
	}

	if (FALSE == m_stInspInfo.byDIO_DI[DI_2D_03_DoorSensor])
	{
		lReturn = RC_DIO_Err_DoorSensor;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDIn_CheckJIGCoverStatus
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/7 - 19:24
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDIn_CheckJIGCoverStatus()
{
	LRESULT lReturn = RC_OK;

	// IKC 커버 센서 없음
	if (Model_IKC == m_stInspInfo.RecipeInfo.ModelType)
		return lReturn;

	// * JIG Cover 체크 (Clear : 커버 덮은 상태, Set : 커버 열린 상태)
	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		if (m_stInspInfo.byDIO_DI[DI_2D_04_JIG_CoverCheck])
			lReturn = RC_DIO_Err_JIGCorverCheck;
		break;

	case Sys_IR_Image_Test:
		if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_02_CoverSensor] && Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
			lReturn = RC_DIO_Err_JIGCorverCheck;
		break;

	case Sys_Image_Test:
		if (FALSE == m_stInspInfo.byDIO_DI[DI_ImgT_02_CoverSensor] && Model_MRA2 == m_stInspInfo.RecipeInfo.ModelType)
			lReturn = RC_DIO_Err_JIGCorverCheck;
		break;

	default:
		break;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_BoardPower
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/6 - 16:55
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDOut_BoardPower(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_FluorescentLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/2/14 - 11:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDOut_FluorescentLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	enum_IO_SignalType nSignal = (bOn) ? IO_SignalT_SetOn : IO_SignalT_SetOff;

	long lPort = -1;

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		lPort = DO_2D_03_FluorescentLamp;
		break;
	case Sys_Focusing:
		lPort = DO_Fo_06_Light;
		break;
	case Sys_IR_Image_Test:
	case Sys_Image_Test:
		lPort = DO_ImgT_09_Light;
		break;
	default:
		break;
	}

#ifndef MOTION_NOT_USE
	m_Device.DigitalIOCtrl.Set_DO_Status(lPort, nSignal);
#endif
	return lReturn;
}

//=============================================================================
// Method		: OnDOut_StartLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/5 - 17:18
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDOut_StartLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	enum_IO_SignalType nSignal = (bOn) ? IO_SignalT_SetOn : IO_SignalT_SetOff;

	long lPortL = -1;
	long lPortR = -1;

	switch (m_InspectionType)
	{	
	case Sys_2D_Cal:
		lPortL = DO_2D_06_StartLamp;
		break;
	case Sys_IR_Image_Test:
		lPortL = DO_IR_ImgT_02_StartLamp;
		break;
	case Sys_Focusing:
		lPortL = DO_Fo_08_StartLempL;
		lPortR = DO_Fo_09_StartLempR;
		break;
	case Sys_Image_Test:
		lPortL = DO_ImgT_05_StartLamp;
		break;
	default:
		break;
	}

#ifndef MOTION_NOT_USE
	m_tm_Motion.OnActionLampControl(lPortL, nSignal);

	if (Sys_Focusing == m_InspectionType)
	{
		m_tm_Motion.OnActionLampControl(lPortR, nSignal);
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_StopLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/5 - 17:23
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDOut_StopLamp(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

	enum_IO_SignalType nSignal = (bOn) ? IO_SignalT_SetOn : IO_SignalT_SetOff;

	long lPort = -1;

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		lPort = DO_2D_07_StopLamp;
		break;
	case Sys_IR_Image_Test:
		lPort = DO_IR_ImgT_03_StopLamp;
		break;
	case Sys_Focusing:
		lPort = DO_Fo_10_StopLemp;
		break;
	case Sys_Image_Test:
		lPort = DO_ImgT_06_StopLamp;
		break;
	default:
		break;
	}

	m_Device.DigitalIOCtrl.Set_DO_Status(lPort, nSignal);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_TowerLamp
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enLampColor nLampColor
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/4 - 16:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDOut_TowerLamp(__in enLampColor nLampColor, __in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

// 	enum_IO_SignalType enSignalType;
// 
// 	if (TRUE == bOn)
// 	{
// 		enSignalType = IO_SignalT_SetOn;
// 	}
// 	else
// 	{
// 		enSignalType = IO_SignalT_SetOff;
// 	}
// 
// 	long lLampPort = 0;
// 
// 	switch (m_InspectionType)
// 	{
// 	case Sys_2D_Cal:
// 		lLampPort = DO_2D_12_TowerLamp_Red + nLampColor;
// 		break;
// 	case Sys_Stereo_Cal:
// 		lLampPort = DO_St_12_TowerLamp_Red + nLampColor;
// 		break;
// 	case Sys_Focusing:
// 		lLampPort = DO_Fo_18_TowerLampRed + nLampColor;
// 		break;
// 	case Sys_Image_Test:
// 		lLampPort = DO_ImgT_11_TowerLampRed + nLampColor;
// 		break;
// 	default:
// 		break;
// 	}
// 
// 	m_Device.DigitalIOCtrl.Set_DO_Status(lLampPort, enSignalType);

	return lReturn;
}

//=============================================================================
// Method		: OnDOut_Buzzer
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/3/5 - 17:24
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDOut_Buzzer(__in BOOL bOn)
{
	LRESULT lReturn = RC_OK;

#ifndef MOTION_NOT_USE
//	lReturn = m_MotionSequence.OnDOut_Buzzer(bOn);
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_CaptureStart
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/1 - 11:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_CaptureStart(__in UINT nIdxBrd)
{
	LRESULT lReturn = RC_OK;

	TRACE(_T("Grabber : Capture Start [CH_%02d]\n"), nIdxBrd);
	if (FALSE == m_Device.DAQ_LVDS.Capture_Start(nIdxBrd))
	{
		lReturn = RC_Grab_Err_CapStartFail;	
		OnAddAlarm_F(_T("Grabber : Capture Start Failed [CH_%02d]"), nIdxBrd);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_CaptureStop
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/4 - 16:01
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_CaptureStop(__in UINT nIdxBrd)
{
	LRESULT lReturn = RC_OK;

	if (FALSE == m_Device.DAQ_LVDS.Capture_Stop(nIdxBrd))
	{
		lReturn = RC_Grab_Err_CapStartFail;
		OnAddAlarm_F(_T("Grabber : Capture Stop Failed [CH_%02d]"), nIdxBrd);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_SensorInit_Unit
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/4 - 16:01
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_SensorInit_Unit(__in UINT nIdxBrd, UINT nRegisterMode /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// 레지스터 파일 설정
	m_Device.DAQ_LVDS.Set_UseAutoI2CWrite(nIdxBrd, FALSE);
	if (nRegisterMode == Reg_Chg_Normal)
	{
		m_Device.DAQ_LVDS.SetI2CFilePath(nIdxBrd, m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName);
	}
	else
	{
		m_Device.DAQ_LVDS.SetI2CFilePath(nIdxBrd, m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName_2);
	}

	// 레지스터 데이터 I2C Write
	if (FALSE == m_Device.DAQ_LVDS.Send_SensorInit(nIdxBrd))
	{
		lReturn = RC_Grab_Err_RegisterInitFail;
		TRACE(_T("Grabber : Sensor Register Init Failed [CH_%02d]\n"), nIdxBrd);
		OnAddAlarm_F(_T("Grabber : Sensor Register Init Failed [CH_%02d]"), nIdxBrd);
	}
	//!SH _180806: I2C Wirte 하면 될듯, 
	/*
	Slave ID : 0x51
	Address : F9
	Data : 0x00 ~ 0x7F, 0 ~ 127
	Slave, addr는 고정, Data는 UI로 입력
	*/

	//!SH _180807: VISEL 밝기 넣기 지금은 IR IMGT 한정
	m_Device.DAQ_LVDS.Send_I2CWrite(nIdxBrd, m_stInspInfo.RecipeInfo.nViselLED);

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_SensorInit_All
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/8 - 10:24
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_SensorInit_All()
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_CheckVideoSignal
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in DWORD dwCheckTime
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/4 - 16:31
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_CheckVideoSignal(__in DWORD dwCheckTime, __in UINT nIdxBrd)
{
	LRESULT lReturn = RC_OK;

	TRACE(_T("Grabber : Check Video Signal [CH_%02d]\n"), nIdxBrd);
	if (FALSE == m_Device.DAQ_LVDS.Check_VideoSignal(nIdxBrd, dwCheckTime, 100))
	{
		lReturn = RC_Grab_Err_NoSignal;
		OnAddAlarm_F(_T("Grabber : No Video Signal [CH_%02d]"), nIdxBrd);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_CheckFrameCount
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in DWORD dwCheckTime
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/4 - 16:31
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnDAQ_CheckFrameCount(__in DWORD dwCheckTime, __in UINT nIdxBrd)
{
	BOOL bReturn = TRUE;

	DWORD dwMeasFPS = m_Device.DAQ_LVDS.Check_FrameCount(nIdxBrd, 15, 40, dwCheckTime, 50);



	return bReturn;
}

//=============================================================================
// Method		: OnDAQ_SetOption
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/2/1 - 13:59
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDAQ_SetOption(__in enModelType nModelType)
{
	// LVDS 옵션 설정
	ST_DAQOption stOptDAQ;
	m_Device.DAQ_LVDS.GetDAQOption(0, stOptDAQ);

	if (FALSE == m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName.IsEmpty())
	{
		// 파일이 존재?
		if (PathFileExists(m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName))
		{
			m_Device.DAQ_LVDS.SetI2CFilePath_All(m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName.GetBuffer(0));
		}
		else
		{
			CString szLog;
			szLog.Format(_T("I2C File is not found : %s"), m_stInspInfo.RecipeInfo.stLVDSInfo.strI2CFileName);

			AfxMessageBox(szLog, MB_SYSTEMMODAL);
		}
	}
	
	// 4종 모델 고정 설정 사용
	OnDAQ_SetOption_Model(nModelType);

	//m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.bIs24_32bitMode;
// 	stOptDAQ.dWidthMultiple		= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dWidthMultiple;
// 	stOptDAQ.dHeightMultiple	= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dHeightMultiple;
// 	stOptDAQ.dwClock			= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dwClock;
// 	stOptDAQ.nDataMode			= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nDataMode;
// 	stOptDAQ.nHsyncPolarity		= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nHsyncPolarity;
// 	stOptDAQ.nPClockPolarity	= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nPClockPolarity;
// 	stOptDAQ.nDvalUse			= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nDvalUse;
// 	stOptDAQ.nVideoMode			= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nVideoMode;
// 	stOptDAQ.nMIPILane			= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nMIPILane;
// 	stOptDAQ.nClockSelect		= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nClockSelect;
// 	stOptDAQ.nClockUse			= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nClockUse;
// 	stOptDAQ.nConvFormat		= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nConvFormat;
// 	stOptDAQ.nSensorType		= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.nSensorType;
// 	stOptDAQ.dwBitWise			= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dwBitWise;
// 	stOptDAQ.byBitPerPixels		= 12;
	//stOptDAQ.dShiftExp		= m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dShiftExp;
	//stOptDAQ.SetExposure(0.0f);
//	stOptDAQ.nCropFrame			= Crop_OddFrame;//Crop_OddFrame,Crop_EvenFrame;

	//m_Device.DAQ_LVDS.SetFrameRate_All(m_stInspInfo.RecipeInfo.dFrameRate);

	// 옵션 설정
	
//	m_Device.DAQ_LVDS.SetDAQOption_All(stOptDAQ);
}

//=============================================================================
// Method		: OnDAQ_SetOption_Model
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enModelType nModelType
// Qualifier	:
// Last Update	: 2018/2/5 - 14:38
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnDAQ_SetOption_Model(__in enModelType nModelType)
{
	m_stImageBuf[Para_Left].AssignMem(g_IR_ModelTable[nModelType].Img_Width, g_IR_ModelTable[nModelType].Img_Height);
	m_stImageBuf[Para_Right].AssignMem(g_IR_ModelTable[nModelType].Img_Width, g_IR_ModelTable[nModelType].Img_Height);
	
	m_stImageBuf[Para_Left].ResetData();
	m_stImageBuf[Para_Right].ResetData();

	switch (nModelType)
	{
	case Model_OMS_Entry:
	{
		m_Device.DAQ_LVDS.SetColorModel_All(enColorModel::CM_Bayer_GRBG, enColorModelOut::CMOUT_RGB);
		m_Device.DAQ_LVDS.SetFrameRate_All(30.0f);

		// OMS Entry 테스트 세팅 (1280 x 960 -> 640 x 480) 우/하
		ST_DAQOption stDaqOpt;
		stDaqOpt.dWidthMultiple		= 1.0;
		stDaqOpt.dHeightMultiple	= 0.5;
		stDaqOpt.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
		stDaqOpt.nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		stDaqOpt.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		stDaqOpt.nDvalUse			= enDAQDvalUse::DAQ_DeUse_DVAL_Use;
		stDaqOpt.nVideoMode			= enDAQVideoMode::DAQ_Video_MIPI_Mode;
		stDaqOpt.nMIPILane			= enDAQMIPILane::DAQMIPILane_2;
		stDaqOpt.nSensorType		= enImgSensorType::enImgSensorType_Monochrome_12bit;
		stDaqOpt.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
		stDaqOpt.nClockSelect		= DAQClockSelect_FixedClock;
		stDaqOpt.nClockUse			= DAQClockUse_Off;
		stDaqOpt.dwClock			= 0;
		stDaqOpt.byBitPerPixels		= 12;
		stDaqOpt.nCropFrame			= Crop_OddFrame;//Crop_OddFrame,Crop_EvenFrame;
		//stDaqOpt.dShiftExp			= 0.0f;m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dShiftExp;
		stDaqOpt.SetExposure(m_stInspInfo.RecipeInfo.dExposure);
	
		m_Device.DAQ_LVDS.SetDAQOption_All(stDaqOpt);
	}
		break;
	
	case Model_OMS_Front:
	{
		m_Device.DAQ_LVDS.SetColorModel_All(enColorModel::CM_Bayer_GRBG, enColorModelOut::CMOUT_RGB);
		m_Device.DAQ_LVDS.SetFrameRate_All(30.0f);

		// OMS Front 테스트 세팅 (1280 x 960 -> 640 x 480) 우/하
		ST_DAQOption stDaqOpt;
		stDaqOpt.dWidthMultiple		= 1.0;
		stDaqOpt.dHeightMultiple	= 0.5;
		stDaqOpt.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
		stDaqOpt.nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		stDaqOpt.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		stDaqOpt.nDvalUse			= enDAQDvalUse::DAQ_DeUse_DVAL_Use;
		stDaqOpt.nVideoMode			= enDAQVideoMode::DAQ_Video_MIPI_Mode;
		stDaqOpt.nMIPILane			= enDAQMIPILane::DAQMIPILane_2;
		stDaqOpt.nSensorType		= enImgSensorType::enImgSensorType_Monochrome_12bit;
		stDaqOpt.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
		stDaqOpt.nClockSelect		= DAQClockSelect_FixedClock;
		stDaqOpt.nClockUse			= DAQClockUse_Off;
		stDaqOpt.dwClock			= 0;
		stDaqOpt.byBitPerPixels		= 12;
		stDaqOpt.nCropFrame			= Crop_OddFrame;//Crop_OddFrame,Crop_EvenFrame;
		//stDaqOpt.dShiftExp			= 0.0f;m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dShiftExp;
		stDaqOpt.SetExposure(m_stInspInfo.RecipeInfo.dExposure);
	
		m_Device.DAQ_LVDS.SetDAQOption_All(stDaqOpt);
	}
		break;

	case Model_OMS_Front_Set:
	{
		m_Device.DAQ_LVDS.SetColorModel_All(enColorModel::CM_Bayer_GRBG, enColorModelOut::CMOUT_RGB);
		m_Device.DAQ_LVDS.SetFrameRate_All(30.0f);

		// OMS Front 테스트 세팅 (1280 x 960 -> 640 x 480) 우/하
		ST_DAQOption stDaqOpt;
		stDaqOpt.dWidthMultiple		= 1.0;
		stDaqOpt.dHeightMultiple	= 0.5;
		stDaqOpt.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
		stDaqOpt.nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		stDaqOpt.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		stDaqOpt.nDvalUse			= enDAQDvalUse::DAQ_DeUse_DVAL_Use;
		stDaqOpt.nVideoMode			= enDAQVideoMode::DAQ_Video_MIPI_Mode;
		stDaqOpt.nMIPILane			= enDAQMIPILane::DAQMIPILane_2;
		stDaqOpt.nSensorType		= enImgSensorType::enImgSensorType_Monochrome_12bit;
		stDaqOpt.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
		stDaqOpt.nClockSelect		= DAQClockSelect_FixedClock;
		stDaqOpt.nClockUse			= DAQClockUse_Off;
		stDaqOpt.dwClock			= 0;
		stDaqOpt.byBitPerPixels		= 12;
		stDaqOpt.nCropFrame			= Crop_OddFrame;//Crop_OddFrame,Crop_EvenFrame;
		//stDaqOpt.dShiftExp			= 0.0f;m_stInspInfo.RecipeInfo.stLVDSInfo.stLVDSOption.dShiftExp;
		stDaqOpt.SetExposure(m_stInspInfo.RecipeInfo.dExposure);
	
		m_Device.DAQ_LVDS.SetDAQOption_All(stDaqOpt);
	}
		break;
	
	case Model_MRA2:
	{
		m_Device.DAQ_LVDS.SetColorModel_All(enColorModel::CM_Bayer_GRBG, enColorModelOut::CMOUT_RGB);
		m_Device.DAQ_LVDS.SetFrameRate_All(30.0f);

		// MRA2 테스트 세팅 (1280 x 960)
		ST_DAQOption stDaqOpt;
		stDaqOpt.dWidthMultiple		= 1.0;
		stDaqOpt.dHeightMultiple	= 1.0;		
		stDaqOpt.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
		stDaqOpt.nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		stDaqOpt.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		stDaqOpt.nDvalUse			= enDAQDvalUse::DAQ_DeUse_HSync_Use;
		stDaqOpt.nVideoMode			= enDAQVideoMode::DAQ_Video_Signal_Mode;
		stDaqOpt.nMIPILane			= enDAQMIPILane::DAQMIPILane_1;
		stDaqOpt.nSensorType		= enImgSensorType::enImgSensorType_Monochrome_CCCC;
		stDaqOpt.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
		stDaqOpt.nClockSelect		= DAQClockSelect_FixedClock;
		stDaqOpt.nClockUse			= DAQClockUse_Off;
		stDaqOpt.dwClock			= 1000000;
		stDaqOpt.byBitPerPixels		= 12;
		stDaqOpt.nCropFrame			= Crop_None;
		
		stDaqOpt.SetCropLineVertical(2, 2);	// 위, 아래 2 pixel 제거
		stDaqOpt.SetExposure(4.0f);
		//stDaqOpt.SetExposure(m_stInspInfo.RecipeInfo.dExposure); // 다음 출장시 적용 필요 (2018.04.02)
	
		m_Device.DAQ_LVDS.SetDAQOption_All(stDaqOpt);
	}
		break;
	
	case Model_IKC:
	{
		m_Device.DAQ_LVDS.SetColorModel_All(enColorModel::CM_Bayer_GRBG, enColorModelOut::CMOUT_RGB);
		m_Device.DAQ_LVDS.SetFrameRate_All(30.0f);

		// IKC 테스트 세팅 (1280 x 720)
		ST_DAQOption stDaqOpt;
		stDaqOpt.dWidthMultiple		= 1.0;
		stDaqOpt.dHeightMultiple	= 1.0;		
		stDaqOpt.nDataMode			= enDAQDataMode::DAQ_Data_16bit_Mode;
		stDaqOpt.nHsyncPolarity		= enDAQHsyncPolarity::DAQ_HsyncPol_Inverse;
		stDaqOpt.nPClockPolarity	= enDAQPclkPolarity::DAQ_PclkPol_RisingEdge;
		stDaqOpt.nDvalUse			= enDAQDvalUse::DAQ_DeUse_HSync_Use;
		stDaqOpt.nVideoMode			= enDAQVideoMode::DAQ_Video_Signal_Mode;
		stDaqOpt.nMIPILane			= enDAQMIPILane::DAQMIPILane_1;
		stDaqOpt.nSensorType		= enImgSensorType::enImgSensorType_Monochrome_CCCC;
		stDaqOpt.nConvFormat		= enConvFormat::Conv_YCbYCr_BGGR;
		stDaqOpt.nClockSelect		= DAQClockSelect_FixedClock;
		stDaqOpt.nClockUse			= DAQClockUse_Off;
		stDaqOpt.dwClock			= 1000000;
		stDaqOpt.byBitPerPixels		= 12;
		stDaqOpt.nCropFrame			= Crop_None;
		
		stDaqOpt.SetCropLineVertical(2, 2);	// 위, 아래 2 pixel 제거
// 		stDaqOpt.SetExposure(4.0f);
		stDaqOpt.SetExposure(m_stInspInfo.RecipeInfo.dExposure); // 다음 출장시 적용 필요 (2018.04.02)
	
		m_Device.DAQ_LVDS.SetDAQOption_All(stDaqOpt);
	}
		break;
	
	default:
		break;
	}
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Write
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/12 - 14:37
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_EEPROM_Write(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	switch (m_InspectionType)
	{
	case Sys_2D_Cal:
		switch (m_stInspInfo.RecipeInfo.ModelType)
		{
		case Model_OMS_Entry:
			lReturn = OnDAQ_EEPROM_OMS_Entry_2D(nIdxBrd);
			break;

		case Model_OMS_Front:
			lReturn = OnDAQ_EEPROM_OMS_Front_2D(nIdxBrd);
			break;

		case Model_OMS_Front_Set:
			lReturn = OnDAQ_EEPROM_OMS_FrontSet_2D(nIdxBrd);
			break;

		default:
			break;
		}
		break;

	case Sys_3D_Cal:
		break;
	case Sys_Focusing:
		break;
	case Sys_Image_Test:
		break;

	default:
		break;
	}

// 	switch (m_stInspInfo.RecipeInfo.ModelType)
// 	{
// 	case Model_OMS_Entry:
// 	case Model_OMS_Front:
// 	case Model_MRA2:
// 	case Model_IKC:
// 	default:
//		break;
// 	}

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_OMS_Entry_2D
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/2/9 - 14:32
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_EEPROM_OMS_Entry_2D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	return lReturn;
}

LRESULT CTestManager_EQP::OnDAQ_EEPROM_OMS_Front_2D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP::OnDAQ_EEPROM_OMS_FrontSet_2D(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;


	return lReturn;
}

LRESULT CTestManager_EQP::OnDAQ_EEPROM_Verify_OMS_Entry(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;

	// Read

	// CRC 1 체크

	// CRC 2 체크

	// 광축 값 체크

	return lReturn;
}

//=============================================================================
// Method		: OnDAQ_EEPROM_Verify_IKC
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nIdxBrd
// Qualifier	:
// Last Update	: 2018/3/9 - 20:58
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnDAQ_EEPROM_Verify_IKC(__in UINT nIdxBrd /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	BOOL bReturn = TRUE;
	BYTE bySlaveAddr = 0x50 << 1;

	BYTE arbyBufRead[256] = { 0, };

	// * Read
	memset(arbyBufRead, 0x00, 256);
	if (FALSE == m_Device.DAQ_LVDS.I2C_Read_Repeat(nIdxBrd, bySlaveAddr, 2, 0x0000, 0x3D, arbyBufRead))
	{
		TRACE(_T("EEPROM Read Failed : Verify Data Read Failed\n"));
		return RC_EEPROM_Err_Read;
	}

	TRACE(_T("EEPROM Read CRC : %02X\n"), arbyBufRead[0x003C]);

	// * CRC 체크
	UINT16 crc_uint16 = 0;
	UINT8 crc = 0;
	for (UINT nIdx = 0; nIdx < 0x3C; nIdx++)
	{
		crc_uint16 += arbyBufRead[nIdx];
	}
	crc = crc_uint16 % 255;
	TRACE(_T("EEPROM Check CRC : %02X\n"), crc);

	if (crc == arbyBufRead[0x3C])
	{
		// Verify 성공
	}
	else
	{
		// Verify 실패
		lReturn = RC_EEPROM_Err_Verify;
	}

	return lReturn;
}

LRESULT CTestManager_EQP::OnMIU_SensorInit(__in UINT nIdxBrd)
{
	LRESULT lReturn = RC_OK;

	// 센서 세팅
	enModelType nModelType = m_stInspInfo.RecipeInfo.ModelType;

	switch (nModelType)
	{
	case Model_OMS_Entry:
		break;
	case Model_OMS_Front:
		break;
	case Model_MRA2:
		break;
	case Model_IKC:
		break;
	case Model_OMS_Front_Set:
		break;

	case Model_OV10642:
		if (FALSE == m_Device.MIUCtrl.Initialize_OV10642())
		{
			lReturn = RC_Grab_Err_RegisterInitFail;
			TRACE(_T("Grabber : Sensor Register Init Failed [CH_%02d]\n"), nIdxBrd);
			OnAddAlarm_F(_T("Grabber : Sensor Register Init Failed [CH_%02d]"), nIdxBrd);
		}
		break;

	case Model_AR0220:
		if (FALSE == m_Device.MIUCtrl.Initialize_AR0220())
		{
			lReturn = RC_Grab_Err_RegisterInitFail;
			TRACE(_T("Grabber : Sensor Register Init Failed [CH_%02d]\n"), nIdxBrd);
			OnAddAlarm_F(_T("Grabber : Sensor Register Init Failed [CH_%02d]"), nIdxBrd);
		}
		break;

	default:
		break;
	}
	
	// 레지스터 데이터 I2C filez
	CString szFilePath = m_stInspInfo.Path.szI2c + _T("\\") + m_stInspInfo.RecipeInfo.sziicFile + _T(".ini");

	if (FALSE == m_Device.MIUCtrl.SensorInit((CStringA)szFilePath))
	{
		lReturn = RC_Grab_Err_RegisterInitFail;
		TRACE(_T("Grabber : Sensor Register Init Failed [CH_%02d]\n"), nIdxBrd);
		OnAddAlarm_F(_T("Grabber : Sensor Register Init Failed [CH_%02d]"), nIdxBrd);
	}

	return lReturn;
}

LRESULT CTestManager_EQP::OnMIU_SignalStatus(__in UINT nIdxBrd)
{
	LRESULT lReturn = RC_OK;

	int iCheckCount = 0;

	for (int i = 0; i < 100; i++)
	{
		if (m_Device.MIUCtrl.GetVideoSignalStatus() == TRUE)
			iCheckCount++;

		if (iCheckCount >= 30)
		{
			lReturn = RC_OK;
			break;
		}

		DoEvents(10);
	}

	if (iCheckCount < 30)
	{
		lReturn = RC_Grab_Err_NoSignal;
	}

	return lReturn;
}

LRESULT CTestManager_EQP::OnMIU_CaptureStart(__in UINT nIdxBrd)
{
	LRESULT lReturn = RC_OK;

	TRACE(_T("Grabber : Capture Start [CH_%02d]\n"), nIdxBrd);
	if (FALSE == m_Device.MIUCtrl.Start())
	{
		lReturn = RC_Grab_Err_CapStartFail;
		OnAddAlarm_F(_T("Grabber : Capture Start Failed [CH_%02d]"), nIdxBrd);
	}

	return lReturn;
}

LRESULT CTestManager_EQP::OnMIU_CaptureStop(__in UINT nIdxBrd)
{
	LRESULT lReturn = RC_OK;

	TRACE(_T("Grabber : Capture Stop [CH_%02d]\n"), nIdxBrd);
	if (FALSE == m_Device.MIUCtrl.Stop())
	{
		lReturn = RC_Grab_Err_CapStartFail;
		//OnAddAlarm_F(_T("Grabber : Capture Start Failed [CH_%02d]"), nIdxBrd);
	}

	return lReturn;
}


LRESULT CTestManager_EQP::OnMIU_DynamicCurrent(__in UINT nIdxBrd, __out double* dCurrentVal)
{
	LRESULT lReturn = RC_OK;

	// 측정enModelType nModelType = m_stInspInfo.RecipeInfo.ModelType;
	UINT nSensorType = m_stInspInfo.RecipeInfo.ModelType;
	CString szFilePath = m_stInspInfo.Path.szI2c + _T("\\") + m_stInspInfo.RecipeInfo.sziicFile + _T(".ini");
	
	if (FALSE == m_Device.MIUCtrl.CurrentMeasure(nSensorType, (CStringA)szFilePath, dCurrentVal))
	{
		lReturn = RC_Grab_Err_Misc;
		OnAddAlarm_F(_T("Grabber : Current Measure Failed [CH_%02d]"), nIdxBrd);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnSaveImage_Raw
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInPath
// Parameter	: __in LPBYTE lpbyInImage
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/8/6 - 16:03
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnSaveImage_Raw(__in LPCTSTR szInPath, __in LPBYTE lpbyInImage, __in DWORD dwFileSize)
{
	if ((NULL == lpbyInImage) || (0 == dwFileSize))
		return FALSE;

	CFile	fileBin;
	CFileException ex;

	try
	{
		if (!fileBin.Open(szInPath, CFile::modeWrite | CFile::shareExclusive | CFile::modeCreate, &ex))
		{
			return FALSE;
		}

		fileBin.Write(lpbyInImage, dwFileSize);
		fileBin.Flush();
	}
	catch (CFileException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
	}
	catch (CMemoryException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
		AfxAbort();
	}

	fileBin.Close();

	return TRUE;
}

//=============================================================================
// Method		: OnLoadImage_Raw
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInPath
// Parameter	: __out LPBYTE lpbyOutImage
// Parameter	: __out DWORD & dwFileSize
// Qualifier	:
// Last Update	: 2018/8/6 - 16:13
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnLoadImage_Raw(__in LPCTSTR szInPath, __out LPBYTE lpbyOutImage, __out DWORD& dwFileSize)
{
	CFile	fileBin;
	CFileException ex;

	try
	{
		if (!fileBin.Open(szInPath, CFile::modeRead | CFile::shareExclusive, &ex))
		{
			return FALSE;
		}

		dwFileSize = (DWORD)fileBin.GetLength();

		fileBin.Read(lpbyOutImage, dwFileSize);
		fileBin.Flush();
	}

	catch (CFileException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
	}

	catch (CMemoryException* pEx)
	{
		pEx->ReportError();
		pEx->Delete();
		AfxAbort();
	}

	fileBin.Close();

	return TRUE;
}

//=============================================================================
// Method		: OnSaveImage_Gray8
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInPath
// Parameter	: __in LPWORD lpwInImage
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/5 - 14:58
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnSaveImage_Gray8(__in LPCTSTR szInPath, __in LPWORD lpwInImage, __in DWORD dwWidth, __in DWORD dwHeight)
{
	BOOL bReturn = FALSE;

	CStringA szPath;
	USES_CONVERSION;
	szPath = CT2A(szInPath);

	IplImage *pImageSave = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 1);

	if (NULL != pImageSave)
	{
		for (int y = 0; y < (int)dwHeight; y++)
		{
			for (int x = 0; x < (int)dwWidth; x++)
			{															// 첫번째 바이트 공간에서 << 4			or		두번째 바이트 공간에서 >> 4
				pImageSave->imageData[y * pImageSave->widthStep + x] = (lpwInImage[y * dwWidth + x] >> 4);
//				pImageSave->imageData[y * pImageSave->widthStep * x] = (lpwInImage[y * dwWidth * 2 + x + 0] & 0xF0) | (lpwInImage[y * dwWidth * 2 + x + 1] & 0x0f);
			}
		}

		cvSaveImage(szPath.GetBuffer(0), pImageSave);

		cvReleaseImage(&pImageSave);

		bReturn = TRUE;
	}

	return TRUE;
}

//=============================================================================
// Method		: OnSaveImage_Gray16
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInPath
// Parameter	: __in LPWORD lpwInImage
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/5 - 14:58
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnSaveImage_Gray16(__in LPCTSTR szInPath, __in LPWORD lpwInImage, __in DWORD dwWidth, __in DWORD dwHeight)
{
	BOOL bReturn = FALSE;

	CStringA szPath;
	USES_CONVERSION;
	szPath = CT2A(szInPath);

	IplImage *pImageSave = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_16U, 1);

	if (NULL != pImageSave)
	{
		memcpy(pImageSave->imageData, lpwInImage, dwWidth * dwHeight * 2);

		cvSaveImage(szPath.GetBuffer(0), pImageSave);

		cvReleaseImage(&pImageSave);

		bReturn = TRUE;
	}

	return TRUE;
}

//=============================================================================
// Method		: OnLoadImage_Gray16
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInPath
// Parameter	: __out LPWORD lpwOutImage
// Parameter	: __out DWORD & dwOutWidth
// Parameter	: __out DWORD & dwOutHeight
// Qualifier	:
// Last Update	: 2018/3/8 - 14:58
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnLoadImage_Gray16(__in LPCTSTR szInPath, __out LPWORD lpwOutImage, __out DWORD& dwOutWidth, __out DWORD& dwOutHeight)
{
	BOOL bReturn = FALSE;

	CStringA szPath;
	USES_CONVERSION;
	szPath = CT2A(szInPath);

	IplImage* pImageLoad = cvLoadImage(szPath.GetBuffer(0), CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
	
	if (NULL != pImageLoad)
	{
		memcpy(lpwOutImage, pImageLoad->imageData, pImageLoad->width * pImageLoad->height * 2);

		cvReleaseImage(&pImageLoad);

		bReturn = TRUE;

		dwOutWidth = pImageLoad->width;
		dwOutHeight = pImageLoad->height;
	}

	return bReturn;
}

//=============================================================================
// Method		: OnSaveImage_RGB
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInPath
// Parameter	: __in LPBYTE lpbyInImage
// Parameter	: __in DWORD dwWidth
// Parameter	: __in DWORD dwHeight
// Qualifier	:
// Last Update	: 2018/2/21 - 19:47
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnSaveImage_RGB(__in LPCTSTR szInPath, __in LPBYTE lpbyInImage, __in DWORD dwWidth, __in DWORD dwHeight)
{
	BOOL bReturn = FALSE;

	CStringA szPath;
	USES_CONVERSION;
	szPath = CT2A(szInPath);

	IplImage *pImageSave = cvCreateImage(cvSize(dwWidth, dwHeight), IPL_DEPTH_8U, 3);

	if (NULL != pImageSave)
	{
		memcpy(pImageSave->imageData, lpbyInImage, dwWidth * dwHeight * 3);

		cvSaveImage(szPath.GetBuffer(0), pImageSave);

		cvReleaseImage(&pImageSave);

		bReturn = TRUE;

		
	}

	return TRUE;
}

//=============================================================================
// Method		: OnLoadImage_RGB
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szInPath
// Parameter	: __out LPBYTE lpbyOutImage
// Parameter	: __out DWORD & dwOutWidth
// Parameter	: __out DWORD & dwOutHeight
// Qualifier	:
// Last Update	: 2018/3/8 - 14:59
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnLoadImage_RGB(__in LPCTSTR szInPath, __out LPBYTE lpbyOutImage, __out DWORD& dwOutWidth, __out DWORD& dwOutHeight)

{
	BOOL bReturn = FALSE;

	CStringA szPath;
	USES_CONVERSION;
	szPath = CT2A(szInPath);

	IplImage* pImageLoad = cvLoadImage(szPath.GetBuffer(0));

	if (NULL != pImageLoad)
	{
		memcpy(lpbyOutImage, pImageLoad->imageData, pImageLoad->width * pImageLoad->height * 3);

		cvReleaseImage(&pImageLoad);

		bReturn = TRUE;

		dwOutWidth = pImageLoad->width;
		dwOutHeight = pImageLoad->height;
	}

	return bReturn;
}

//=============================================================================
// Method		: OnCameraBrd_CheckSync
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/1 - 17:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCameraBrd_CheckSync()
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;
	BYTE	byBrdNo = 0;

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].CtrlBrd_Cnt; nIdx++)
	{
		if (m_Device.PCBCamBrd[nIdx].IsConnected())
		{
			lCommResult = m_Device.PCBCamBrd[nIdx].Send_BoardCheck(byBrdNo);

			if (SRC_OK == lCommResult)
			{
				OnSetStatus_Camera_Brd(COMM_STATUS_SYNC_OK, nIdx);
				OnLog(_T("Camera Board[%d] Sync OK"), nIdx + 1);
			}
			else
			{
				OnSetStatus_Camera_Brd(COMM_STATUS_CONNECT, nIdx);

				lReturn = RC_CamBrd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
				TRACE(_T("Camera Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
				OnLog_Err(_T("Camera Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);
			}
		}
		else
		{
			OnSetStatus_Camera_Brd(COMM_STATUS_NOTCONNECTED, nIdx);

			lReturn = RC_CamBrd_Err_Misc;
			TRACE(_T("Camera Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Camera Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnCameraBrd_PowerOnOff
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enPowerOnOff bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/1 - 17:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCameraBrd_PowerOnOff(__in enPowerOnOff bOn, __in UINT nParaIdx /*= 0*/, __in UINT nRetry /*= 0*/)
{
	LRESULT lReturn		= RC_OK;
	LRESULT lCommResult = SRC_OK;

	if (nParaIdx < g_InspectorTable[m_InspectionType].CtrlBrd_Cnt)
	{
		// 전류 ON
		for (UINT nCnt = 0; nCnt < nRetry + 1; nCnt++)
		{
			lCommResult = m_Device.PCBCamBrd[nParaIdx].Send_SetVolt((BOOL)bOn);

			if (0 < nRetry)
			{
				if (SRC_OK == lCommResult)
				{
					break;
				}

				Sleep(500);

				if (bOn)
				{
					m_Device.PCBCamBrd[nParaIdx].Send_SetVolt(FALSE);

					Sleep(500);
				}
			}
		}

		if (SRC_OK != lCommResult)
		{
			lReturn = RC_CamBrd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Camera Board[%d] Error : Code -> %d\n"), nParaIdx + 1, lReturn);
			OnLog_Err(_T("Camera Board[%d] Error : Code -> %d"), nParaIdx + 1, lReturn);
		}
	}
	else
	{
		lReturn = RC_CamBrd_Err_Parameter;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnCameraBrd_Read_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/1 - 17:33
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCameraBrd_Read_Current(__in UINT nParaIdx /*= 0*/, __out float faCurrent[])
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;
	ST_CamBrdCurrent	stCurrent;

	if (nParaIdx < g_InspectorTable[m_InspectionType].CtrlBrd_Cnt)
	{
		// 전류 ON
		lCommResult = m_Device.PCBCamBrd[nParaIdx].Send_GetCurrent(stCurrent);

		if (SRC_OK == lCommResult)
		{
			// CurrentMax
			faCurrent[0] = stCurrent.fOutCurrent[0];
			faCurrent[1] = stCurrent.fOutCurrent[1];
			faCurrent[2] = stCurrent.fOutCurrent[2];
			faCurrent[3] = stCurrent.fOutCurrent[3];
			faCurrent[4] = stCurrent.fOutCurrent[4];

			// MRA2, IKC (1.8V, 2.8V)
			// OMS (1.8V, 3.3V, 9.0V, 14.7V, -5V ~ 7V)
		}
		else
		{
			lReturn = RC_CamBrd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Camera Board[%d] Error : Code -> %d\n"), nParaIdx + 1, lReturn);
			OnLog_Err(_T("Camera Board[%d] Error : Code -> %d"), nParaIdx + 1, lReturn);
		}
	}
	else
	{
		lReturn = RC_CamBrd_Err_Parameter;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnCameraBrd_CheckOverCurrent
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __out BOOL bOverCurrent
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/12 - 19:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCameraBrd_CheckOverCurrent(__out BOOL bOverCurrent, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;

	lCommResult = m_Device.PCBCamBrd[nParaIdx].Send_CheckOverCurrent(bOverCurrent);

	if (SRC_OK != lCommResult)
	{
		lReturn = RC_CamBrd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
		TRACE(_T("Camera Board[%d] Error : Code -> %d\n"), nParaIdx + 1, lReturn);
		OnLog_Err(_T("Camera Board[%d] Error : Code -> %d"), nParaIdx + 1, lReturn);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnCameraBrd_LEDOnOff
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in enPowerOnOff bOn
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/8/11 - 15:46
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCameraBrd_LEDOnOff(__in enPowerOnOff bOn, __in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;

	if (nParaIdx < g_InspectorTable[m_InspectionType].CtrlBrd_Cnt)
	{
		lCommResult = m_Device.PCBCamBrd[nParaIdx].Send_LEDViselOnOff((BOOL)bOn);

		if (SRC_OK != lCommResult)
		{
			lReturn = RC_CamBrd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Camera Board[%d] Error : Code -> %d\n"), nParaIdx + 1, lReturn);
			OnLog_Err(_T("Camera Board[%d] Error : Code -> %d"), nParaIdx + 1, lReturn);
		}
	}
	else
	{
		lReturn = RC_CamBrd_Err_Parameter;
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLightBrd_CheckSync
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/10/19 - 20:22
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightBrd_CheckSync()
{
	LRESULT lReturn		= RC_OK;
	LRESULT lCommResult = SRC_OK;

#ifdef USE_LIGHT_PSU_PARTICLE

	if (m_Device.LightPSU_Part.IsConnected())
	{
		lCommResult = m_Device.LightPSU_Part.Send_PortCheck();

		if (SRC_OK == lCommResult)
		{
			OnSetStatus_LightBrd(COMM_STATUS_SYNC_OK, 0);
			OnLog(_T("Light PSU Particle Sync OK"), 1);
		}
		else
		{
			OnSetStatus_LightBrd(COMM_STATUS_CONNECT, 0);

			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light PSU Particle Error : Code -> %d\n"), 1, lReturn);
			OnLog_Err(_T("Light PSU Particle Error : Code -> %d"), 1, lReturn);
		}
	}
	else
	{
		OnSetStatus_LightBrd(COMM_STATUS_NOTCONNECTED, 0);

		lReturn = RC_Light_Brd_Err_PortOpen;
		TRACE(_T("Light PSU Particle Error : Code -> %d\n"), 1, lReturn);
		OnLog_Err(_T("Light PSU Particle Error : Code -> %d"), 1, lReturn);
	}
#else
	
	BYTE	byPort		= 0;	

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightBrd_Cnt; nIdx++)
	{
		if (m_Device.LightBrd[nIdx].IsConnected())
		{
			lCommResult = m_Device.LightBrd[nIdx].Send_PortCheck(byPort);

			if (SRC_OK == lCommResult)
			{
				OnSetStatus_LightBrd(COMM_STATUS_SYNC_OK, nIdx);
				OnLog(_T("Light Board[%d] Sync OK"), nIdx + 1);
			}
			else
			{
				OnSetStatus_LightBrd(COMM_STATUS_CONNECT, nIdx);

				lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
				TRACE(_T("Light Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
				OnLog_Err(_T("Light Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);
			}
		}
		else
		{
			OnSetStatus_LightBrd(COMM_STATUS_NOTCONNECTED, nIdx);

			lReturn = RC_Light_Brd_Err_PortOpen;
			TRACE(_T("Light Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);
		}
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnLightBrd_PowerOn
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in float fVolt
// Parameter	: __in WORD wStep
// Qualifier	:
// Last Update	: 2018/3/3 - 9:17
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightBrd_PowerOn(__in UINT nSlot, __in float fVolt, __in WORD wStep)
{
	LRESULT lReturn		= RC_OK;
	LRESULT lCommResult = SRC_OK;

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightBrd_Cnt; nIdx++)
	{
		//// 전압 ON 
		lCommResult = m_Device.LightBrd[nIdx].Send_EtcSetVolt(nSlot, fVolt);

		if (SRC_OK != lCommResult)
		{
			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);

			return lReturn;
		}

		// 전류 ON
		//lCommResult = m_Device.LightBrd[nIdx].Send_AmbientCurrentOn_All(wStep);
		lCommResult = m_Device.LightBrd[nIdx].Send_EtcSetCurrent(nSlot, wStep);
		if (SRC_OK != lCommResult)
		{
			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLightBrd_Volt_PowerOn
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in float fVolt
// Qualifier	:
// Last Update	: 2018/3/29 - 11:11
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightBrd_Volt_PowerOn(__in float fVolt)
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;

#ifdef USE_LIGHT_PSU_PARTICLE
	
	lCommResult = m_Device.LightPSU_Part.Send_Voltage(0);

	if (SRC_OK == lCommResult)
	{
		Sleep(300);
		lCommResult = m_Device.LightPSU_Part.Send_Output(enSwitchOnOff::Switch_ON);

		//if (SRC_OK == m_Device.LightPSU_Part.Send_Output_Status(nOutStatus))
		//{
		//	TRACE(_T("Light PSU Particle Output Different : Out-> %s, Set-> %s "), g_szOnOff[nOutStatus], g_szOnOff[nSwitch]);	
		//}
		//else
		//{
		//	;
		//}
	}
	else
	{
		lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
		TRACE(_T("Light PSU Particle Power On Error : Code -> %d\n"), lReturn);
		OnLog_Err(_T("Light PSU Particle Power On Error : Code -> %d"), lReturn);
	}

#else

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightBrd_Cnt; nIdx++)
	{
		//!SH _180811 : Volt On 따로
		// 전압 ON 
		//lCommResult = m_Device.LightBrd[nIdx].Send_AmbientVoltOn_All(fVolt);
		lCommResult = m_Device.LightBrd[nIdx].Send_EtcSetVolt(Slot_A,fVolt);

		if (SRC_OK != lCommResult)
		{
			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);

			return lReturn;
		}	
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnLightBrd_PowerOff
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/10/27 - 17:59
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightBrd_PowerOff()
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;

#ifdef USE_LIGHT_PSU_PARTICLE

	lCommResult = m_Device.LightPSU_Part.Send_Output(enSwitchOnOff::Switch_OFF);

	//if (SRC_OK == m_Device.LightPSU[nIdx].Send_Output_Status(nOutStatus))
	//{
	//	TRACE(_T("Light PSU Particle Output Different : Out-> %s, Set-> %s "), g_szOnOff[nOutStatus], g_szOnOff[nSwitch]);	
	//}
	//else
	//{
	//	;
	//}
	
	if (SRC_OK != lCommResult)
	{
		lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
		TRACE(_T("Light PSU Particle Power Off Error : Code -> %d\n"), lReturn);
		OnLog_Err(_T("Light PSU Particle Power Off Error : Code -> %d"), lReturn);
	}

#else

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightBrd_Cnt; nIdx++)
	{
		lCommResult = m_Device.LightBrd[nIdx].Send_AmbientVoltOff_All();

		if (SRC_OK != lCommResult)
		{
			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);

			return lReturn;
		}

		lCommResult = m_Device.LightBrd[nIdx].Send_AmbientCurrentOff_All();

		if (SRC_OK != lCommResult)
		{
			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light Board[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light Board[%d] Error : Code -> %d"), nIdx + 1, lReturn);
		}
	}
#endif

	return lReturn;
}

//=============================================================================
// Method		: OnLightPSU_CheckSync
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/11/8 - 17:48
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightPSU_CheckSync()
{
	LRESULT lReturn = RC_OK;
 	LRESULT lCommResult = SRC_OK;
 
 	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdx++)
 	{
 		if (m_Device.LightPSU[nIdx].IsConnected())
 		{
 			lCommResult = m_Device.LightPSU[nIdx].Send_PortCheck();
 
 			if (SRC_OK == lCommResult)
 			{
 				OnSetStatus_LightPSU(COMM_STATUS_SYNC_OK, nIdx);
 				OnLog(_T("Light PSU[%d] Sync OK"), nIdx + 1);
 			}
 			else
 			{
 				OnSetStatus_LightPSU(COMM_STATUS_CONNECT, nIdx);
 
 				lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
 				TRACE(_T("Light PSU[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
 				OnLog_Err(_T("Light PSU[%d] Error : Code -> %d"), nIdx + 1, lReturn);
 			}
 		}
 		else
 		{
 			OnSetStatus_LightPSU(COMM_STATUS_NOTCONNECTED, nIdx);
 
 			lReturn = RC_Light_Brd_Err_PortOpen;
 			TRACE(_T("Light PSU[%d] Error : Code -> %d\n"), nIdx + 1, lReturn);
 			OnLog_Err(_T("Light PSU[%d] Error : Code -> %d"), nIdx + 1, lReturn);
 		}
 	}

	return lReturn;
}

//=============================================================================
// Method		: OnLightPSU_PowerOnOff
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in BOOL bOn
// Qualifier	:
// Last Update	: 2018/2/6 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightPSU_PowerOnOff(__in BOOL bOn)
{
	//KHO 정리하자 정리
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;
	enSwitchOnOff nSwitch = (bOn) ? enSwitchOnOff::Switch_ON : enSwitchOnOff::Switch_OFF;

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdx++)
	{
		if (Switch_ON == nSwitch)
		{
			lCommResult = m_Device.LightPSU[nIdx].Send_Voltage(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightPSU[nIdx].fVolt);
		}

		if (SRC_OK == lCommResult)
		{
			Sleep(300);
			lCommResult = m_Device.LightPSU[nIdx].Send_Output(nSwitch);

// 			if (SRC_OK == m_Device.LightPSU[nIdx].Send_Output_Status(nOutStatus))
// 			{
// 				TRACE(_T("Light PSU[%d] Output Different : Out-> %s, Set-> %s "), nIdx, g_szOnOff[nOutStatus], g_szOnOff[nSwitch]);	
// 			}
// 			else
// 			{
// 				;
// 			}
		}
		else
		{
			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light PSU[%d] Power On/Off Error : Code -> %d\n"), nIdx + 1, lReturn);
		//	OnLog_Err(_T("Light PSU[%d] Power On/Off Error : Code -> %d"), nIdx + 1, lReturn);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLightPSU_Voltage
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/2 - 14:52
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightPSU_Voltage()
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdx++)
	{
		lCommResult = m_Device.LightPSU[nIdx].Send_Voltage(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightPSU[nIdx].fVolt);

		if (SRC_OK == lCommResult)
		{
			float fOutVoltage = 0.0f;
			if (SRC_OK == m_Device.LightPSU[nIdx].Send_Voltage_Req(fOutVoltage))
			{
				if (fOutVoltage != m_stInspInfo.MaintenanceInfo.stLightInfo.stLightPSU[nIdx].fVolt)
				{
					TRACE(_T("Light PSU[%d] Voltage Different : Out-> %.2f, Set-> %.2f "), nIdx, fOutVoltage, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightPSU[nIdx].fVolt);
				}
			}
			else
			{
				;
			}
		}

		if (SRC_OK == lCommResult)
		{
			OnLog(_T("Light PSU[%d] Voltage"), nIdx + 1);
		}
		else
		{
			lReturn &= RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light PSU[%d] Voltage Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light PSU[%d] Voltage Error : Code -> %d"), nIdx + 1, lReturn);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLightPSU_Current
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/6 - 15:20
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightPSU_Current()
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightPSU_Cnt; nIdx++)
	{
		lCommResult = m_Device.LightPSU[nIdx].Send_Current(m_stInspInfo.MaintenanceInfo.stLightInfo.stLightPSU[nIdx].fCurrent);

		if (SRC_OK == lCommResult)
		{
			float fOutCurrent = 0.0f;
			if (SRC_OK == m_Device.LightPSU[nIdx].Send_Current_Req(fOutCurrent))
			{
				if (fOutCurrent != m_stInspInfo.MaintenanceInfo.stLightInfo.stLightPSU[nIdx].fCurrent)
				{
					TRACE(_T("Light PSU[%d] Current Different : Out-> %.2f, Set-> %.2f "), nIdx, fOutCurrent, m_stInspInfo.MaintenanceInfo.stLightInfo.stLightPSU[nIdx].fCurrent);
				}
			}
			else
			{
				;
			}
		}

		if (SRC_OK == lCommResult)
		{
			OnLog(_T("Light PSU[%d] Current"), nIdx + 1);
		}
		else
		{
			lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
			TRACE(_T("Light PSU[%d] Current Error : Code -> %d\n"), nIdx + 1, lReturn);
			OnLog_Err(_T("Light PSU[%d] Current Error : Code -> %d"), nIdx + 1, lReturn);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnLightPSU_CheckLightOn
// Access		: protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/3/2 - 14:39
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightPSU_CheckLightOn()
{
	LRESULT lReturn = RC_OK;

	OnLightPSU_Voltage();
	//OnLightPSU_Current();

	lReturn = OnLightPSU_PowerOnOff(TRUE);

	return lReturn;
}

//=============================================================================
// Method		: OnLightBrd_CheckLightOn
// Access		: protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2018/2/12 - 14:45
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnLightBrd_CheckLightOn()
{
	LRESULT lReturn = RC_OK;

	LRESULT lCommResult = SRC_OK;

	float fVolt = 0.0f;
	WORD wStep = 0;

	for (UINT nIdx = 0; nIdx < g_InspectorTable[m_InspectionType].LightBrd_Cnt; nIdx++)
	{
		for (UINT nCnt = 0; nCnt < ERR_RETRY_CNT; nCnt++)
		{
			fVolt = m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nIdx].fVolt;
			lCommResult = m_Device.LightBrd[nIdx].Send_AmbientVoltOn_All(fVolt);

			if (SRC_OK == lCommResult)
			{
				OnLog(_T("Light Board[%d] Volt On"), nIdx + 1);
				break;
			}
			else
			{
				lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
				TRACE(_T("Light Board[%d] Volt On Error : Code -> %d\n"), nIdx + 1, lReturn);
				OnLog_Err(_T("Light Board[%d] Volt On Error : Code -> %d"), nIdx + 1, lReturn);
			}

			wStep = m_stInspInfo.MaintenanceInfo.stLightInfo.stLightBrd[nIdx].wStep;
			lCommResult = m_Device.LightBrd[nIdx].Send_AmbientCurrentOn_All(wStep);

			if (SRC_OK == lCommResult)
			{
				lReturn = RC_OK;
				OnLog(_T("Light Board[%d] CurrentOn_"), nIdx + 1);
				break;
			}
			else
			{
				lReturn = RC_Light_Brd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
				TRACE(_T("Light Board[%d] CurrentOn_ Error : Code -> %d\n"), nIdx + 1, lReturn);
				OnLog_Err(_T("Light Board[%d] CurrentOn_ Error : Code -> %d"), nIdx + 1, lReturn);
			}
		}
	}

	return lReturn;
}

LRESULT CTestManager_EQP::OnDisplace_Read_Value(__in UINT nParaIdx, __out double& fReadVal)
{
	LRESULT lReturn = RC_OK;
	LRESULT lCommResult = SRC_OK;

	// 전류 ON
	//lCommResult = m_Device.Displace.Send_OutputData(fReadVal);

	double dAvgValue = 999;
	double dCurValue = 0;
// 	for (int i = 0; i < 20; i++)
// 	{
// 		lCommResult = m_Device.Displace.Send_OutputData(dCurValue);
// 		Sleep(50);
// 
// 		if (dAvgValue > dCurValue)
// 		{
// 			dAvgValue = dCurValue;
// 		}
// 	}

//	fReadVal = dAvgValue;

	Sleep(300);

	lCommResult = m_Device.Displace.Send_OutputData(dCurValue);
	fReadVal = dCurValue;
	
	if (SRC_OK == lCommResult)
	{

	}
	else
	{
		lReturn = RC_CamBrd_Err_Misc + Conv_SerialResultToResult((enSerialResultCode)lCommResult);
		TRACE(_T("Displace Sensor Error : Code -> %d\n"), nParaIdx + 1, lReturn);
		OnLog_Err(_T("Displace Sensor Error : Code -> %d"), nParaIdx + 1, lReturn);
	}

	return lReturn;
}

//=============================================================================
// Method		: OnManualSequence
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in enParaManual enFunc
// Qualifier	: 메뉴얼 시퀀스 동작 테스트
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnManualSequence(__in UINT nParaID, __in enParaManual enFunc)
{
    LRESULT lReturn = RC_OK;

    switch (enFunc)
    {
    case ManuP_Pallet_In:
        break;
    case ManuP_Dock_Act:
        break;
    case ManuP_Dock_Mov:
		lReturn = OnMotion_LoadProduct();
		if (RC_OK != lReturn)
			return lReturn;
		break;
    case ManuP_Insp_Act:
//		AutomaticProcess_Test_All(nParaID);
        break;
    case ManuP_Insp_Mov:
		lReturn = OnMotion_MoveToTestPos(nParaID);
		if (RC_OK != lReturn)
			return lReturn;
        break;
    case ManuP_UnLoad_Act:
        break;
    case ManuP_UnLoad_Mov:
		lReturn = OnMotion_LoadProduct();
		if (RC_OK != lReturn)
			return lReturn;

// 		lReturn = OnMotion_MoveToStandbyPos(nParaID);
// 		if (RC_OK != lReturn)
// 			return lReturn;
// 
// 		lReturn = OnMotion_UnloadProduct(nParaID);
		if (RC_OK != lReturn)
			return lReturn;
        break;
    case ManuP_Pallet_Out:
        break;
    case ManuP_Max_Num:
        break;
    default:
        break;
    }

    return lReturn;
}

//=============================================================================
// Method		: OnManualTestItem
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaID
// Parameter	: __in UINT nStepIdx
// Qualifier	:
// Last Update	: 2017/10/27 - 18:00
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnManualTestItem(__in UINT nParaID, __in UINT nStepIdx)
{
    LRESULT lReturn = RC_OK;

// 	ST_StepInfo* const pStepInfo = &m_stInspInfo.RecipeInfo.StepInfo;
// 	INT_PTR iStepCnt = m_stInspInfo.RecipeInfo.StepInfo.GetCount();
// 	DWORD dwElapTime = 0;
// 	UINT nCornerStep = 0;
// 
// 	if (0 < pStepInfo->StepList[nStepIdx].bUseChart_Rot || 0 < pStepInfo->StepList[nStepIdx].nMoveY)
// 	{
// 		lReturn = OnMotion_2DCAL_Test(pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot, nParaID);
// 		if (lReturn != RC_OK)
// 		{
// 			// ERROR
// 			TRACE("[Motion Err] OnMotion_2DCal_Test(x:%d, y:%d, charR:%d)\r\n", pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
// 			OnLog_Err(_T("[Motion Err] OnMotion_2DCal_Test(x:%d, y:%d, charR:%d)"), pStepInfo->StepList[nStepIdx].iMoveX, pStepInfo->StepList[nStepIdx].nMoveY, pStepInfo->StepList[nStepIdx].dChart_Rot);
// 		}
// 	}
//          
// 	if (lReturn == RC_OK)
// 	{
// 		// * 검사 시작
// 		if (TRUE == pStepInfo->StepList[nStepIdx].bTest)
// 		{
// 			if (FALSE == m_stInspInfo.bDryRunMode)
// 			{
// 				if (pStepInfo->StepList[nStepIdx].nTestItem == TI_2D_Detect_CornerExt)
// 				{
// 					StartTest_2D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, nCornerStep++, nParaID);
// 				}
// 				else
// 				{
// 					StartTest_2D_CAL_TestItem(pStepInfo->StepList[nStepIdx].nTestItem, nStepIdx, 0, nParaID);
// 				}
// 			}
// 		}
// 	}
    return lReturn;
}

//=============================================================================
// Method		: OnCheck_Barcode
// Access		: virtual protected  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/26 - 20:56
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCheck_Barcode(__in UINT nParaIdx /*= 0*/)
{
	LRESULT lReturn = RC_OK;
	
	if (Sys_Focusing != m_InspectionType)
	{
		// CAN 통신으로 Barcode를 읽었는가?
		if (m_stInspInfo.CamInfo[nParaIdx].szBarcode.IsEmpty())
		{
			lReturn = RC_NoBarcode;
		};
	}
	else // Sys_Focusing 설비
	{
		// WIP_ID 15자리
		if (Barcode_Leng == m_stInspInfo.szBarcodeBuf.GetLength())
		{
			OnSet_Barcode(m_stInspInfo.szBarcodeBuf.GetBuffer(0));
		}
		else //if (m_stInspInfo.szBarcodeBuf.IsEmpty())
		{
			// 에러 상황
			lReturn = RC_NoBarcode;
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnCheck_DeviceComm
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCheck_DeviceComm()
{
	LRESULT lReturn	= RC_OK;

	return lReturn;
}

//=============================================================================
// Method		: OnCheck_SaftyJIG
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCheck_SaftyJIG()
{
	LRESULT lReturn = RC_OK;

	if (m_stInspInfo.bForcedStop)
	{
		lReturn = RC_Forced_Stoped;

		if (m_stInspInfo.bEMO)
		{
			AfxMessageBox(_T("EMO Occurred!!, Please Restart Program"), MB_SYSTEMMODAL);
		}
		else
		{
			AfxMessageBox(_T("Force Stopped!!, Need Equipment Initialize"), MB_SYSTEMMODAL);
		}
	}

	return lReturn;
}

//=============================================================================
// Method		: OnCheck_RecipeInfo
// Access		: virtual protected  
// Returns		: LRESULT
// Qualifier	:
// Last Update	: 2017/9/23 - 14:44
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::OnCheck_RecipeInfo()
{
	LRESULT lReturn = RC_OK;

	if (m_stInspInfo.RecipeInfo.szRecipeFile.IsEmpty())
	{
		//OnLog_Err(_T("모델 설정이 되지 않았습니다."));
		//AfxMessageBox(_T("모델 설정이 되지 않았습니다."), MB_SYSTEMMODAL);

		return RC_Recipe_Err;
	}
	
	return lReturn;
}
//=============================================================================
// Method		: OnMakeReport
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 14:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnJugdement_And_Report(__in UINT nParaIdx)
{
	CFile_Report fileReport;

	switch (m_InspectionType)
	{
	case Sys_Focusing:
		break;

	case Sys_2D_Cal:
		fileReport.SaveFinalizeResult2DCal(m_stInspInfo.Path.szReport, nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, &m_stInspInfo.CamInfo[nParaIdx].st2D_Result);
		break;

	case Sys_Image_Test:
		break;

	case Sys_3D_Cal:
		//fileReport.SaveFinalizeResult3DCalTotal(m_stInspInfo.Path.szReport, nParaIdx, m_stInspInfo.CamInfo[nParaIdx].szBarcode, m_stInspInfo.CamInfo[nParaIdx].st3D_Depth, &m_stInspInfo.CamInfo[nParaIdx].st3D_Eval);
		break;

	default:
		break;
	}
}

void CTestManager_EQP::OnJugdement_And_Report_All()
{
	OnUpdateYield();
	OnSaveWorklist();
}

//=============================================================================
// Method		: OnSet_TestTime
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/3 - 12:04
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSet_TestTime(__in UINT nParaIdx /*= 0*/)
{
	if (0 < m_stInspInfo.dwTactTime)
	{
		m_stInspInfo.CycleTime.AddTestTime(m_stInspInfo.dwTactTime, nParaIdx);
	}

	m_stInspInfo.CycleTime.AddTestTime(m_stInspInfo.CamInfo[nParaIdx].dwCycleTime, nParaIdx);

	// 검사 항목 별 수율 정보 추가
	ST_TestItemMeas* pTest = NULL;
	for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.StepInfo.GetCount(); nIdx++)
	{
		if (m_stInspInfo.GetTestStep(nIdx)->bTest)
		{
			pTest = m_stInspInfo.GetTestItemMeas(nIdx, nParaIdx);

			m_stInspInfo.CycleTime.AddItemCycleTime(m_stInspInfo.GetTestItemElapTime(pTest->nItemID, nParaIdx), pTest->nItemID);
		}
	}
}

//=============================================================================
// Method		: OnSet_CycleTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/10/11 - 21:01
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSet_CycleTime()
{
	if (0 < m_stInspInfo.dwTactTime)
	{
		m_stInspInfo.CycleTime.AddTactTime(m_stInspInfo.dwTactTime);
	}

	m_stInspInfo.CycleTime.AddCycleTime(m_stInspInfo.CamInfo[0].dwCycleTime);

	// 검사 항목 별 수율 정보 추가
	ST_TestItemMeas* pTest = NULL;

	for (UINT nParaIdx = 0; nParaIdx < m_stInspInfo.GetTestChannelCount(); nParaIdx++)
	{
		for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.StepInfo.GetCount(); nIdx++)
		{
			if (m_stInspInfo.GetTestStep(nIdx)->bTest)
			{
				pTest = m_stInspInfo.GetTestItemMeas(nIdx, nParaIdx);

				m_stInspInfo.CycleTime.AddItemCycleTime(m_stInspInfo.GetTestItemElapTime(pTest->nItemID, nParaIdx), pTest->nItemID);
			}
		}

		if (TR_Pass != m_stInspInfo.CamInfo[nParaIdx].nJudgment)
		{
			break;
		}
	}
}

//=============================================================================
// Method		: OnReset_CamInfo
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/9/19 - 22:49
// Desc.		: 바코드 정보 제외하고 데이터 초기화
//=============================================================================
void CTestManager_EQP::OnReset_CamInfo(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.ResetCamInfo(nParaIdx);
}

//=============================================================================
// Method		: OnReset_CamInfo_All
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/27 - 16:41
// Desc.		: 바코드 정보 포함 데이터 초기화
//=============================================================================
void CTestManager_EQP::OnReset_CamInfo_All()
{
	m_stInspInfo.ResetCamInfo_All();
}

//=============================================================================
// Method		: OnResetInfo_Loading
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/23 - 22:52
// Desc.		: 제품 투입시 데이터 초기화 (바코드 초기화 안함)
//=============================================================================
void CTestManager_EQP::OnResetInfo_Loading()
{
	m_stInspInfo.ResetCamInfo_All(TRUE);
}

//=============================================================================
// Method		: OnResetInfo_StartTest
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/2/21 - 20:34
// Desc.		: 검사 시작시 데이터 초기화
//=============================================================================
void CTestManager_EQP::OnResetInfo_StartTest(__in UINT nParaIdx /*= 0*/)
{
	// 바코드 버퍼 초기화?
}

//=============================================================================
// Method		: OnResetInfo_Unloading
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/23 - 16:43
// Desc.		: 제품 배출시 데이터 초기화
//=============================================================================
void CTestManager_EQP::OnResetInfo_Unloading()
{
	// * 바코드 버퍼 초기화
	m_stInspInfo.ResetBarcodeBuffer();
	
	// * 제품 검사 데이터 초기화
	m_stInspInfo.ResetCamInfo_All();

	for (UINT nParaIdx = 0; nParaIdx < USE_CHANNEL_CNT; nParaIdx++)
	{
		m_stInspInfo.CamInfo[nParaIdx].nProgressStatus = TP_Ready;
	}
}

//=============================================================================
// Method		: OnResetInfo_Measurment
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/12/16 - 11:58
// Desc.		: 검사 진행된 데이터 초기화
//=============================================================================
void CTestManager_EQP::OnResetInfo_Measurment(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.Reset_Measurment(nParaIdx);
}

//=============================================================================
// Method		: OnAddAlarmInfo
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enResultCode nResultCode
// Qualifier	:
// Last Update	: 2017/9/20 - 20:43
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnAddAlarmInfo(__in enResultCode nResultCode)
{
	// 	CDlg_ErrView Dlg_ErrView;
	// 	Dlg_ErrView.ErrMessage(lErrorCode);
	// 	Dlg_ErrView.DoModal();
}

//=============================================================================
// Method		: OnAddAlarm
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szAlarm
// Qualifier	:
// Last Update	: 2017/12/4 - 16:09
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnAddAlarm(__in LPCTSTR szAlarm)
{
}

//=============================================================================
// Method		: OnSet_BoardPower
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in BOOL bOnOff
// Qualifier	:
// Last Update	: 2017/11/12 - 21:55
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSet_BoardPower(__in BOOL bOnOff)
{
}

//=============================================================================
// Method		: CreateTimer_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in DWORD DueTime
// Parameter	: __in DWORD Period
// Qualifier	:
// Last Update	: 2017/11/8 - 14:17
// Desc.		: 파워 서플라이 모니터링
//=============================================================================
void CTestManager_EQP::CreateTimer_UpdateUI(__in DWORD DueTime /*= 5000*/, __in DWORD Period /*= 250*/)
{
// 	if ((Sys_2D_Cal == m_InspectionType) || (Sys_Image_Test == m_InspectionType) || (Sys_3D_Cal == m_InspectionType))
// 	{
// 		__super::CreateTimer_UpdateUI(5000, 2500);
// 	}
}

void CTestManager_EQP::DeleteTimer_UpdateUI()
{
// 	if ((Sys_2D_Cal == m_InspectionType) || (Sys_Image_Test == m_InspectionType) || (Sys_3D_Cal == m_InspectionType))
// 	{
// 		__super::DeleteTimer_UpdateUI();
// 	}
}

//=============================================================================
// Method		: OnMonitor_TimeCheck
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 13:28
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnMonitor_TimeCheck()
{
#ifndef NO_CHECK_ELAP_TIME
	// 현재 시간 체크
 	m_dwTimeCheck = timeGetTime();
 	
 	// 개별 채널 검사 시간 체크
 	for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
 	{
 		// 검사 중이면
 		if (TP_Testing == m_stInspInfo.GetTestProgress(nUnitIdx))
 		{
 			m_stInspInfo.CamInfo[nUnitIdx].TestTime.Get_Duration_Test(m_dwTimeCheck);
 
 			OnUpdate_ElapTime_TestUnit(nUnitIdx);
 		}
 	}

	if ((TP_Loading <= m_stInspInfo.GetTestStatus()) && (m_stInspInfo.GetTestStatus() <= TP_Unloading))
	{
		for (UINT nUnitIdx = 0; nUnitIdx < USE_CHANNEL_CNT; nUnitIdx++)
		{
			m_stInspInfo.CamInfo[nUnitIdx].TestTime.Get_Duration_Cycle(m_dwTimeCheck);
		}

		OnUpdate_ElapTime_Cycle();
	}
 
#endif
}

//=============================================================================
// Method		: OnMonitor_UpdateUI
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/30 - 21:19
// Desc.		: Power Supply 모니터링
//=============================================================================
void CTestManager_EQP::OnMonitor_UpdateUI()
{
// 	if (m_Device.DigitalIOCtrl.AXTState() == TRUE)
// 	{
// 		for (BYTE nBitOffset = 0; nBitOffset < DI_2D_MaxEnum; nBitOffset++)
// 		{
// 			BOOL bStatus = m_Device.DigitalIOCtrl.Get_DI_Status(nBitOffset);
// 
// 			if (bStatus != m_stInspInfo.byDIO_DI[nBitOffset])
// 			{
// 				m_stInspInfo.byDIO_DI[nBitOffset] = bStatus;
// 
// 				OnDIO_UpdateDInSignal(nBitOffset, bStatus);
// 				OnDIn_DetectSignal(nBitOffset, bStatus);
// 			}
// 		}
// 	}
}

//=============================================================================
// Method		: OnSet_TestProgress
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in enTestProcess nProcess
// Qualifier	:
// Last Update	: 2016/5/30 - 10:25
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSet_TestProgress(__in enTestProcess nProcess)
{
	m_stInspInfo.SetTestStatus(nProcess);
}

void CTestManager_EQP::OnSet_TestProgress_Unit(__in enTestProcess nProcess, __in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.CamInfo[nParaIdx].SetTestProgress(nProcess);
}

void CTestManager_EQP::OnSet_TestResult(__in enTestResult nResult)
{
	m_stInspInfo.Set_Judgment_All(nResult);
}

void CTestManager_EQP::OnSet_TestResult_Unit(__in enTestResult nResult, __in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.CamInfo[nParaIdx].nJudgment = nResult;
}

void CTestManager_EQP::OnSet_ResultCode_Unit(__in LRESULT nResultCode, __in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.CamInfo[nParaIdx].ResultCode = nResultCode;
}

void CTestManager_EQP::OnSet_TestItemResult(__in UINT nItemIdx, __in enTestResult nResult, __in LPCTSTR szMeasureValue, __in DWORD dwDuration)
{

}

void CTestManager_EQP::OnSet_TestStepSelect(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{
	
}

void CTestManager_EQP::OnSet_TestStepResult(__in UINT nStepIdx, __in UINT nParaIdx /*= 0*/)
{

}

//=============================================================================
// Method		: OnSet_InputTime
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/9/27 - 17:26
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSet_InputTime()
{
	m_stInspInfo.SetInputTime();
}

//=============================================================================
// Method		: OnSet_InputTime__Unit
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/3/18 - 13:24
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSet_InputTime__Unit(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.SetInputTime_Uint(nParaIdx);
}

void CTestManager_EQP::OnSet_BeginTestTime(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.SetBeginTestTime(nParaIdx);
}

void CTestManager_EQP::OnSet_EndTestTime(__in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.SetEndTestTime(nParaIdx);
}

void CTestManager_EQP::OnSet_OutputTime()
{
	m_stInspInfo.SetOutputTime();
}

//=============================================================================
// Method		: OnSet_Barcode
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in LPCTSTR szBarcode
// Parameter	: __in UINT nRetryCnt
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2018/6/3 - 19:59
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSet_Barcode(__in LPCTSTR szBarcode, __in UINT nRetryCnt /*= 0*/, __in UINT nParaIdx /*= 0*/)
{
	m_stInspInfo.Set_Barcode(szBarcode, nRetryCnt, nParaIdx);

	AddLog_F(_T("Barcode : %s"), szBarcode);
}

void CTestManager_EQP::OnSet_VCSEL_Status(__in BOOL bOn, __in UINT nParaIdx /*= 0*/)
{
	//m_stInspInfo.CamInfo[nParaIdx].bCAN_VCSEL_On = bOn;
}

//=============================================================================
// Method		: OnSaveWorklist
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/5/10 - 20:24
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSaveWorklist()
{
	m_stInspInfo.WorklistInfo.Reset();

	ST_CamInfo* pstCam = NULL;

	CString szText;
	CString szTime;
	SYSTEMTIME lcTime;

	GetLocalTime(&lcTime);
	szTime.Format(_T("'%04d-%02d-%02d %02d:%02d:%02d.%03d"), lcTime.wYear, lcTime.wMonth, lcTime.wDay,
		lcTime.wHour, lcTime.wMinute, lcTime.wSecond, lcTime.wMilliseconds);

	for (UINT nChIdx = 0; nChIdx < USE_CHANNEL_CNT; nChIdx++)
	{
		if (FALSE == m_stInspInfo.bTestEnable[nChIdx])
			continue;

		pstCam = &m_stInspInfo.CamInfo[nChIdx];

		m_stInspInfo.WorklistInfo.Time = szTime;
		m_stInspInfo.WorklistInfo.EqpID = m_stInspInfo.szEquipmentID;	// g_szInsptrSysType[m_InspectionType];
		m_stInspInfo.WorklistInfo.SWVersion;
		m_stInspInfo.WorklistInfo.Model = m_stInspInfo.RecipeInfo.szModelCode;
		m_stInspInfo.WorklistInfo.Barcode = pstCam->szBarcode;
		m_stInspInfo.WorklistInfo.Result = g_TestResult[pstCam->nJudgment].szText;

		szText.Format(_T("%d"), nChIdx + 1);
		m_stInspInfo.WorklistInfo.Socket = szText;


		m_stInspInfo.WorklistInfo.MakeItemz();

		// 파일 저장
		m_Worklist.Save_FinalResult_List(m_stInspInfo.Path.szReport, &pstCam->TestTime.tmStart_Loading, &m_stInspInfo.WorklistInfo);

		// UI 표시
		OnInsertWorklist();

	}// End of for
}

//=============================================================================
// Method		: OnUpdateYield
// Access		: virtual protected  
// Returns		: void
// Parameter	: __in UINT nParaIdx
// Qualifier	:
// Last Update	: 2017/10/12 - 15:02
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnUpdateYield(__in UINT nParaIdx /*= 0*/)
{
	// 수율 정보 추가
	if (TR_Pass == m_stInspInfo.CamInfo[nParaIdx].nJudgment)
	{
		m_stInspInfo.YieldInfo.IncreasePass(nParaIdx);
	}
	else
	{
		m_stInspInfo.YieldInfo.IncreaseFail(nParaIdx);
	}

	// 검사 항목 별 수율 정보 추가
	ST_TestItemMeas* pTest = NULL;
	for (UINT nIdx = 0; nIdx < m_stInspInfo.RecipeInfo.StepInfo.GetCount(); nIdx++)
	{
		if (m_stInspInfo.GetTestStep(nIdx)->bTest)
		{
			pTest = m_stInspInfo.GetTestItemMeas(nIdx, nParaIdx);

			if (TR_Pass == pTest->nJudgmentAll)
			{
				m_stInspInfo.YieldInfo.IncreasePass_TestItem(pTest->nItemID);
			}
			else
			{
				m_stInspInfo.YieldInfo.IncreaseFail_TestItem(pTest->nItemID);
			}
		}
	}

	// 수율 파일로 저장
	OnSaveYield();
}

//=============================================================================
// Method		: OnSaveYield
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 14:15
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnSaveYield()
{
// 	CFile_Report fileYield;
// 	CString szPath;
// 	SYSTEMTIME* ptmLocal = &m_stInspInfo.Time.tmStart_Test;
// 
// 	szPath.Format(_T("%s%04d_%02d_%02d\\%s\\%s\\"), m_stInspInfo.Path.szReport, 
// 													ptmLocal->wYear,ptmLocal->wMonth, ptmLocal->wDay, 
// 													m_stInspInfo.RecipeInfo.szModelCode, m_stInspInfo.LotInfo.szLotName);
// 
// 	MakeDirectory(szPath);
// 
// 	szPath = szPath + _T("Yield.ini");
// 
// 	fileYield.SaveYield_LOT(szPath, &m_stInspInfo.YieldInfo);
}

//=============================================================================
// Method		: OnLoadYield
// Access		: virtual protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/2 - 14:26
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnLoadYield()
{
// 	CFile_Report fileYield;
// 	CString szPath;
// 	SYSTEMTIME tmLocal;
// 	GetLocalTime(&tmLocal);
// 
// 	szPath.Format(_T("%s%04d_%02d_%02d\\%s\\%s\\Yield.ini"), m_stInspInfo.Path.szReport, tmLocal.wYear, tmLocal.wMonth, tmLocal.wDay,
// 													m_stInspInfo.RecipeInfo.szModelCode, m_stInspInfo.szLotName);
// 
// 	fileYield.LoadYield_LOT(szPath, m_stInspInfo.YieldInfo);
}

//=============================================================================
// Method		: Reset_ConsumCount
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __in UINT nItemIdx
// Qualifier	:
// Last Update	: 2018/3/5 - 9:01
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Reset_ConsumCount(__in UINT nItemIdx)
{
	m_stInspInfo.ConsumablesInfo.ResetCount(nItemIdx);

	OnSetStatus_ConsumInfo(nItemIdx);

	return Save_ConsumInfo(nItemIdx);
}

//=============================================================================
// Method		: Increase_ConsumCount
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/7/21 - 11:27
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Increase_ConsumCount(__in UINT nItemIdx)
{
	m_stInspInfo.ConsumablesInfo.Increase_Count(nItemIdx);
	
	OnSetStatus_ConsumInfo(nItemIdx);
	
	return Save_ConsumInfo(nItemIdx);
}

//=============================================================================
// Method		: Increase_ConsumCount
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/3/5 - 8:53
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Increase_ConsumCount()
{
	BOOL bReturn = TRUE;

	bReturn = Increase_ConsumCount(Para_Left);

	if ((1 < g_IR_ModelTable[m_stInspInfo.Get_ModelType()].Camera_Cnt) && (1 < g_InspectorTable[m_InspectionType].Grabber_Cnt))
	{
		bReturn &= Increase_ConsumCount(Para_Right);
	}
	
	return bReturn;
}

//=============================================================================
// Method		: Save_ConsumInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in enPogoCntIndex nPogoIndex
// Qualifier	:
// Last Update	: 2016/6/22 - 14:52
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Save_ConsumInfo()
{
 	CFile_Recipe	m_fileRecipe;
	m_fileRecipe.SetSystemType(m_InspectionType);

 	CString		strFullPath;
 	CString		strLog;
 	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szConsumables, m_stInspInfo.RecipeInfo.szConsumablesFile, CONSUM_FILE_EXT);
 
 	if (FALSE == m_fileRecipe.Save_ConsumInfoFile(strFullPath, &m_stInspInfo.ConsumablesInfo))
 	{
 		strLog.Format(_T("Error saving Pogo configuration file."));
 		OnLog_Err(strLog);
 		AfxMessageBox(strLog, MB_SYSTEMMODAL);
 
 		return FALSE;
 	}

	return TRUE;
}

//=============================================================================
// Method		: Save_ConsumInfo
// Access		: protected  
// Returns		: BOOL
// Parameter	: __in UINT nItemIdx
// Qualifier	:
// Last Update	: 2017/2/14 - 10:56
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Save_ConsumInfo(__in UINT nItemIdx)
{
	CFile_Recipe	fileRecipe;
	fileRecipe.SetSystemType(m_InspectionType);

	CString			strFullPath;
	CString			strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szConsumables, m_stInspInfo.RecipeInfo.szConsumablesFile, CONSUM_FILE_EXT);

	if (FALSE == fileRecipe.Save_ConsumInfo(strFullPath, nItemIdx, m_stInspInfo.ConsumablesInfo.Item[nItemIdx].dwCount))
	{
		strLog.Format(_T("Error saving <Consumable Info> configuration file."));
		OnLog_Err(strLog);
		AfxMessageBox(strLog, MB_SYSTEMMODAL);

		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_ConsumInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/14 - 10:51
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Load_ConsumInfo()
{
	CFile_Recipe	fileRecipe;
	fileRecipe.SetSystemType(m_InspectionType);

	CString			strFullPath;
	CString			strLog;
	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szConsumables, m_stInspInfo.RecipeInfo.szConsumablesFile, CONSUM_FILE_EXT);

	if (FALSE == fileRecipe.Load_ConsumInfoFile(strFullPath, m_stInspInfo.ConsumablesInfo))
	{
		strLog.Format(_T("Can not load <Consumable Info> configuration file."));
		OnLog_Err(strLog);
		AfxMessageBox(strLog, MB_SYSTEMMODAL);

		return FALSE;
	}

	OnSetStatus_ConsumInfo();

	return TRUE;
}

//=============================================================================
// Method		: Check_ConsumInfo
// Access		: protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2017/2/14 - 10:51
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Check_ConsumInfo()
{
	// POGO 카운트 값이 최대치에 도달했으면 경고 창 팝업
	if (m_stInspInfo.ConsumablesInfo.IsMaxCount())
	{	
		AfxMessageBox(_T("The pogo pin count is the maximum.\r\nCall an engineer."), MB_SYSTEMMODAL);
		return FALSE;
	}

	return TRUE;
}

//=============================================================================
// Method		: Load_PresetStepInfo
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/4/3 - 14:17
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::Load_PresetStepInfo(__in LPCTSTR szRecipe)
{
#ifdef USE_PRESET_MODE
	// Preset_01.xml ~ Preset_10.xml

	CFile_Recipe	fileRecipe;
	CString			strFullPath;

	strFullPath.Format(_T("%s%s.%s"), m_stInspInfo.Path.szRecipe, szRecipe, RECIPE_FILE_EXT);

	fileRecipe.Load_PresetInfo(strFullPath, m_stInspInfo.RecipeInfo.stPresetStepInfo);
	fileRecipe.LoadXML_PresetStepInfo(m_stInspInfo.Path.szRecipe, m_stInspInfo.RecipeInfo.stPresetStepInfo);

#endif
	return TRUE;
}

//=============================================================================
// Method		: OnSave_SelectedRecipe
// Access		: virtual protected  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/8/21 - 11:45
// Desc.		: 선택한 모델 레지스트리에 저장
//=============================================================================
BOOL CTestManager_EQP::OnSave_SelectedRecipe()
{
	if (m_bUseForcedModel)	// .exe 커맨드 라인 인자로 특정 모델을 지정했을 경우
	{
		return m_regInspInfo.SaveSelectedModel(m_nModelType, m_stInspInfo.RecipeInfo.szRecipeFile, m_stInspInfo.RecipeInfo.szModelCode);
	}
	else
	{
		return m_regInspInfo.SaveSelectedModel(m_stInspInfo.RecipeInfo.szRecipeFile, m_stInspInfo.RecipeInfo.szModelCode);
	}
}

//=============================================================================
// Method		: OnLoad_SelectedRecipe
// Access		: virtual protected  
// Returns		: BOOL
// Parameter	: __out CString & szRecipeFile
// Parameter	: __out CString & szRecipe
// Qualifier	:
// Last Update	: 2018/8/21 - 11:49
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::OnLoad_SelectedRecipe(__out CString& szRecipeFile, __out CString& szRecipe)
{
	if (m_bUseForcedModel)	// .exe 커맨드 라인 인자로 특정 모델을 지정했을 경우
	{
		return m_regInspInfo.LoadSelectedModel(m_nModelType, szRecipeFile, szRecipe);
	}
	else
	{
		return m_regInspInfo.LoadSelectedModel(szRecipeFile, szRecipe);
	}
}

void CTestManager_EQP::OnCheck_ForcedModel()
{
	if (m_bUseForcedModel)
	{
		// 레시피 폴더를 모델 설정에 따라 변경
		m_stOption.Inspector.szPath_Recipe;

		g_szModelFolder[m_nModelType];
	}
}

//=============================================================================
// Method		: OnInitialize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnInitialize()
{
	//__super::OnInitialize();
	
	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		m_hEvent_EjectModule[nIdx] = NULL;
		m_hEvent_EjectModule[nIdx]	= CreateEvent(NULL, TRUE, FALSE, NULL/*_T("Eject Module")*/);
	}

	m_hEvent_ReqCapture	= CreateEvent(NULL, TRUE, FALSE, NULL/*_T("ReqCapture")*/);

	for (UINT nIdx = 0; nIdx < Event_Bn_MaxEnum; nIdx++)
	{
		m_hEvent_Butoon[nIdx] = CreateEvent(NULL, TRUE, FALSE, NULL/*_T("BnStart")*/);
	}

	CreateTimer_TimeCheck();
	CreateTimer_UpdateUI();	
}

//=============================================================================
// Method		: OnFinalize
// Access		: virtual public  
// Returns		: void
// Qualifier	:
// Last Update	: 2016/5/26 - 6:03
// Desc.		:
//=============================================================================
void CTestManager_EQP::OnFinalize()
{
	//__super::OnFinalize();

	DeleteTimer_TimeCheck();
	DeleteTimer_UpdateUI();

	for (UINT nIdx = 0; nIdx < USE_CHANNEL_CNT; nIdx++)
	{
		if (NULL != m_hEvent_EjectModule[nIdx])
		{
			CloseHandle(m_hEvent_EjectModule[nIdx]);

			m_hEvent_EjectModule[nIdx] = NULL;
		}
	}

	if (NULL != m_hEvent_ReqCapture)
	{
		CloseHandle(m_hEvent_ReqCapture);

		m_hEvent_ReqCapture = NULL;
	}
}

//=============================================================================
// Method		: SetSystemType
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enInsptrSysType nSysType
// Qualifier	:
// Last Update	: 2017/9/26 - 13:52
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetSystemType(__in enInsptrSysType nSysType)
{
	__super::SetSystemType(nSysType);

	m_stInspInfo.SetSystemType(nSysType);
}

//=============================================================================
// Method		: IsTesting
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/5/16 - 10:57
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting()
{
	if (NULL != m_hThrTest_All)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_All, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	if (IsTesting_LoadUnload())
		return TRUE;

	for (UINT nUnitIdx = 0; nUnitIdx < MAX_OPERATION_THREAD; nUnitIdx++)
	{
		if (IsTesting_Unit(nUnitIdx))
			return TRUE;
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_All
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/6/9 - 11:52
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_All()
{
	if (NULL != m_hThrTest_All)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_All, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_LoadUnload
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/5/16 - 10:57
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_LoadUnload()
{
	if (NULL != m_hThr_LoadUnload)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThr_LoadUnload, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_Manual
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2018/5/12 - 19:46
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_Manual()
{
	if (NULL != m_hThrTest_Manual)
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Manual, &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_Unit
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nUnitIdx
// Qualifier	:
// Last Update	: 2018/2/14 - 13:37
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_Unit(__in UINT nUnitIdx)
{
	if (NULL != m_hThrTest_Unit[nUnitIdx])
	{
		DWORD dwExitCode = NULL;
		GetExitCodeThread(m_hThrTest_Unit[nUnitIdx], &dwExitCode);

		if (STILL_ACTIVE == dwExitCode)
		{
			return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsTesting_Exc
// Access		: public  
// Returns		: BOOL
// Parameter	: __in UINT nExcUnitIdx
// Qualifier	:
// Last Update	: 2016/6/30 - 16:59
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsTesting_Exc(__in UINT nExcUnitIdx)
{
	DWORD dwExitCode = NULL;
	GetExitCodeThread(m_hThrTest_All, &dwExitCode);
	if (STILL_ACTIVE == dwExitCode)
	{
		return TRUE;
	}

	if (IsTesting_LoadUnload())
		return TRUE;

	for (UINT nUnitIdx = 0; nUnitIdx < MAX_OPERATION_THREAD; nUnitIdx++)
	{
		if (nUnitIdx != nExcUnitIdx)
		{
			if (IsTesting_Unit(nUnitIdx))
				return TRUE;
		}
	}

	return FALSE;
}

//=============================================================================
// Method		: IsManagerMode
// Access		: public  
// Returns		: BOOL
// Qualifier	:
// Last Update	: 2016/1/13 - 10:32
// Desc.		:
//=============================================================================
BOOL CTestManager_EQP::IsManagerMode()
{
	if (Permission_Manager == m_stInspInfo.PermissionMode)
		return TRUE;
	else
		return FALSE;
}

//=============================================================================
// Method		: GetPermissionMode
// Access		: public  
// Returns		: enPermissionMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
enPermissionMode CTestManager_EQP::GetPermissionMode()
{
	return m_stInspInfo.PermissionMode;
}

//=============================================================================
// Method		: SetPermissionMode
// Access		: public  
// Returns		: void
// Parameter	: __in enPermissionMode nAcessMode
// Qualifier	:
// Last Update	: 2016/11/9 - 18:52
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetPermissionMode(__in enPermissionMode nAcessMode)
{
	m_stInspInfo.PermissionMode = nAcessMode;
}

//=============================================================================
// Method		: SetMESOnlineMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enMES_Online nOnlineMode
// Qualifier	:
// Last Update	: 2018/3/1 - 10:33
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetMESOnlineMode(__in enMES_Online nOnlineMode)
{
	// MES로 Online 상태 변경
	m_stInspInfo.MESOnlineMode = nOnlineMode;
}

//=============================================================================
// Method		: SetOperateMode
// Access		: virtual public  
// Returns		: void
// Parameter	: __in enOperateMode nOperMode
// Qualifier	:
// Last Update	: 2018/3/12 - 19:50
// Desc.		:
//=============================================================================
void CTestManager_EQP::SetOperateMode(__in enOperateMode nOperMode)
{
	if (FALSE == IsTesting())
	{
		m_stInspInfo.OperateMode = nOperMode;
	}
}

//=============================================================================
// Method		: EquipmentInit
// Access		: virtual public  
// Returns		: void
// Parameter	: __in UINT nCondition
// Qualifier	:
// Last Update	: 2018/3/11 - 22:08
// Desc.		:
//=============================================================================
void CTestManager_EQP::EquipmentInit(__in UINT nCondition /*= 0*/)
{
	// 검사 중?

	// 강제 중지 Flag 원복

	if (IsTesting())
	{
		TRACE(_T("검사가 진행 가능한 상태가 아닙니다.\n"));
		OnLog_Err(_T("Equipment Init Error : Inspection is in progress."));
		AfxMessageBox(_T("Inspection is in progress. \r\n\r\nPlease wait until the Inspection is finished."), MB_SYSTEMMODAL);
		return; //RC_AlreadyTesting;
	}

	// 확인
	if (IDYES == AfxMessageBox(_T("Are you sure you want to Initialize?"), MB_YESNO))
	{
		// 패스워드 체크
// 		CDlg_ChkPassword	dlgPassword(this);
// 		if (IDCANCEL == dlgPassword.DoModal())
// 			return FALSE;
		
	}

	

	m_stInspInfo.bForcedStop = FALSE;
}

//=============================================================================
// Method		: _TI_Cm_RegisterChangeMode
// Access		: virtual public  
// Returns		: LRESULT
// Parameter	: __in UINT nParaIdx
// Parameter	: UINT nRegisterMode
// Qualifier	:
// Last Update	: 2018/5/12 - 19:46
// Desc.		:
//=============================================================================
LRESULT CTestManager_EQP::_TI_Cm_RegisterChangeMode(__in UINT nParaIdx , UINT nRegisterMode)
{
	LRESULT lReturn = RC_OK;

	TRACE(_T("_TI_Cm_RegisterChangeMode [CH_%02d]\n"), nParaIdx);

	OnDAQ_CaptureStop(nParaIdx); 

	OnCameraBrd_PowerOnOff(enPowerOnOff::Power_Off, nParaIdx);

	Sleep(500);

	// * Camera Power On
	lReturn = OnCameraBrd_PowerOnOff(enPowerOnOff::Power_On, nParaIdx, 3);

	if (RC_OK == lReturn)
	{
		Sleep(1000);
		// * 전류 측정으로 제품 감지?
		//OnCameraBrd_Read_Current(nParaIdx);

		// * Register Write (Alpha ??)
		lReturn = OnDAQ_SensorInit_Unit(nParaIdx, nRegisterMode);

		if (RC_OK == lReturn)
		{
			// * Capture On
			lReturn = OnDAQ_CaptureStart(nParaIdx);

			if (RC_OK == lReturn)
			{
				// * 영상 신호 대기
				lReturn = OnDAQ_CheckVideoSignal(10000, nParaIdx);

				if (RC_OK != lReturn)
				{
					TRACE(_T("Error : OnDAQ_CheckVideoSignal [CH_%02d]\n"), nParaIdx);
					//OnAddAlarm_F(_T("Grabber : No Video Signal [CH_%02d]"), nParaIdx);
				}
			}
			else
			{
				TRACE(_T("Error : OnDAQ_CaptureStart [CH_%02d]\n"), nParaIdx);
				//OnAddAlarm_F(_T("Grabber : Capture Start Failed [CH_%02d]"), nParaIdx);
			}
		}
		else
		{
			TRACE(_T("Error : OnDAQ_SensorInit_Unit [CH_%02d]\n"), nParaIdx);
			//OnAddAlarm_F(_T("Grabber : Sensor Register Init Failed [CH_%02d]"), nParaIdx);
		}
	}
	else
	{
	}

	// 스텝 결과
	if (RC_OK == lReturn)
	{
		TRACE(_T("_TI_Cm_Initialize Suceed [CH_%02d]\n"), nParaIdx);


		// * 검사 항목별 결과
	}
	else
	{
		TRACE(_T("_TI_Cm_Initialize Failed [CH_%02d]\n"), nParaIdx);
		AddLog_F(_T("Camera : Initialize Failed [CH_%02d]"), nParaIdx);


		// * 에러 상황
	}


	return lReturn;
}