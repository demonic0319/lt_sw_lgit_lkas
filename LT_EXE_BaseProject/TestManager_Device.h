﻿//*****************************************************************************
// Filename	: 	TestManager_Device.h
// Created	:	2016/9/28 - 19:54
// Modified	:	2016/9/28 - 19:54
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef TestManager_Device_h__
#define TestManager_Device_h__

#pragma once

#include "TestManager_Base.h"

#include "Def_TestDevice.h"
#include "Def_DataStruct.h"
#include "LT_Option.h"
#include "Def_ResultCode_Cm.h"

//-----------------------------------------------------------------------------
// CTestManager_Device
//-----------------------------------------------------------------------------
class CTestManager_Device : public CTestManager_Base
{
public:
	CTestManager_Device();
	virtual ~CTestManager_Device();

protected:

	//-------------------------------------------------------------------------
	// 옵션
	//-------------------------------------------------------------------------
	stLT_Option		m_stOption;
	// 환경설정 데이터 불러오기
	virtual BOOL	OnLoadOption					();
	//virtual void	OnInitUISetting					(__in HWND hWndOwner = NULL);

	//-------------------------------------------------------------------------
	// 주변장치 제어
	//-------------------------------------------------------------------------

	// 전체 주변장치와 통신 연결 전의 초기 작업
	virtual void	InitDevicez						(__in HWND hWndOwner = NULL);
	// 전체 주변장치와 통신 연결 시도
	virtual void	ConnectDevicez					();
	// 전체 주변장치의 연결 해제
	virtual void	DisconnectDevicez				();

	// MES TCP/IP 통신 연결 및 해제
	virtual BOOL	ConnectMES						(__in BOOL bConnect);
	
	// 바코드 리더기 연결 및 해제
	virtual BOOL	ConnectHandyBCR					(__in BOOL bConnect);
	virtual BOOL	ConnectFixedBCR					(__in BOOL bConnect);
	
	// 제어 보드 연결 및 해제
	virtual BOOL	ConnectPCB_Camera_Brd			(__in BOOL bConnect);
	
	// 변위 센서 연결 및 해제
	virtual BOOL	ConnectPCB_Displace				(__in BOOL bConnect);

	// 광원 보드 연결 및 해제
	virtual BOOL	ConnectPCB_LightBrd				(__in BOOL bConnect);

	// 광원 파워 서플라이 연결 및 해제
	virtual BOOL	ConnectLightPSU					(__in BOOL bConnect);
	
	// PLC 연결 및 해제
	virtual BOOL	ConnectPLC						(__in BOOL bConnect);

	// Digital I/O, Motion 연결 및 해제
	virtual BOOL	ConnectMotion					(__in BOOL bConnect);

	// Digital Indicator 제어 연결 및 해제
	virtual BOOL	ConnectIndicator				(__in BOOL bConnect);

	// 프레임 그래버 보드 연결 및 해제
	virtual BOOL	ConnectGrabberBrd				(__in BOOL bConnect, __in UINT nIdxBrd = 0);	
	
	// 토크 드라이버 연결 및 해제
	virtual BOOL	ConnectTorque					(__in BOOL bConnect);

	// 토크 드라이버 연결 및 해제
	virtual BOOL	ConnectVisionCam				(__in BOOL bConnect, __in UINT nCamIdx = 0);	

	virtual BOOL	ConnectFCM30					(__in BOOL bConnect);
	//-------------------------------------------------------------------------
	// 통신 연결 상태 UI에 표시 
	//-------------------------------------------------------------------------
	
	// MES 통신 상태
	virtual void	OnSetStatus_MES					(__in UINT nCommStatus){};
	virtual void	OnSetStatus_MES_Online			(__in UINT nOnlineMode){};
	
	// 바코드 리더기 통신 연결 상태
	virtual void	OnSetStatus_HandyBCR			(__in UINT nConnect){};
	virtual void	OnSetStatus_FixedBCR			(__in UINT nConnect){};
	
	// 테스터에 접속된 핸들러 통신 상태
	virtual void	OnSetStatus_FCM30				(__in UINT nConnect){};

	// 제어 보드 연결 상태
	virtual void	OnSetStatus_Camera_Brd			(__in UINT nConnect, __in UINT nIdxBrd = 0){};
	
	// 변위 센서 연결 상태
	virtual void	OnSetStatus_DisplaceSensor		(__in UINT nConnect){};

	// 비전 카메라 연결 상태
	virtual void	OnSetStatus_VisionCam			(__in BOOL bConnect, __in UINT nCamIdx = 0){};

	// 광원 보드 통신 연결상태
	virtual void	OnSetStatus_LightBrd			(__in UINT nConnect, __in UINT nIdxBrd = 0){};
	
	// 파워 서플라이 연결상태
	virtual void	OnSetStatus_LightPSU			(__in UINT nConnect, __in UINT nIdxBrd = 0){};

	// PLC 통신 상태
	virtual void	OnSetStatus_PLC					(__in UINT nCommStatus){};

	// Digital I/O, Motion 통신 상태
	virtual void	OnSetStatus_Motion				(__in UINT nCommStatus){};

	// Digital Indicator 연결 상태
	virtual void	OnSetStatus_Indicator			(__in UINT nConnect, __in UINT nIdxBrd = 0){};
	
	virtual	void	OnMachineLamp					(__in UINT nLampStatus){};

	// 프레임 그래버 보드 통신 연결 상태
	virtual void	OnSetStatus_GrabBoard			(__in UINT nConnect, __in UINT nIdxBrd = 0){};
	virtual void	OnSetStatus_VideoSignal			(__in UINT bSignalStatus, __in UINT nIdxBrd = 0){};
	
	// 토크 드라이버 연결 상태
	virtual void	OnSetStatus_Torque				(__in UINT nConnect){};

	//-------------------------------------------------------------------------
	// 바코드
	//-------------------------------------------------------------------------
	// 바코드 입력
	virtual void	OnSet_BarcodeWithDialog			(__in LPCTSTR szBarcode){};

	//-----------------------------------------------------
	// Digital I/O
	//-----------------------------------------------------
	virtual void	OnFunc_IO_Stop					(){};
	virtual void	OnAddAlarmInfo					(__in enResultCode nResultCode){};
	virtual void	OnAddAlarm						(__in LPCTSTR szAlarm){};
	virtual void	OnAddAlarm_F					(__in LPCTSTR szAlarm, ...){};
	virtual void	OnResetAlarm					(){};

	virtual void	OnSet_BoardPower				(__in BOOL bOnOff){};

	//-------------------------------------------------------------------------
	// Handler <-> Tester 통신 
	//-------------------------------------------------------------------------
	// 핸들러 : 서버에 접속되면 Lot Name, Model Name, Operator Name 전송
	virtual void	OnConnectedToServer				(__in BYTE byServerIdx){};
	// 핸들러 : 서버 접속이 끊어짐
	virtual void	OnDisonnectedFromServer			(__in BYTE byServerIdx){};
	// 핸들러 : Ping Ack Error (Tester Error)
	virtual void	OnRecv_PingAckError				(__in BYTE byServerIdx){};
	// 핸들러 : Ping Timeout 신호 수신
	virtual void	OnRecv_PingTimeout				(__in BYTE byServerIdx){};

	//-----------------------------------------------------
	// 주변 장치 제어용 클래스 모음
	//-----------------------------------------------------
	ST_Device			m_Device;
	
	// 실행파일 커맨드 라인을 이용한 강제 모델 설정
	BOOL				m_bUseForcedModel = FALSE;
	// 커맨드 라인에 입력된 모델
	enModelType			m_nModelType = Model_OMS_Entry;

public:

	// 생성자 처리용 코드
	virtual void	OnInitialize					();
	// 소멸자 처리용 코드
	virtual	void	OnFinalize						();

};

#endif // TestManager_Device_h__

