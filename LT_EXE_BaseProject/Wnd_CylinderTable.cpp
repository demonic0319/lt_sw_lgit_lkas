﻿//*****************************************************************************
// Filename	: 	Wnd_CylinderTable.cpp
// Created	:	2018/1/5 - 17:26
// Modified	:	2018/1/5 - 17:26
//
// Author	:	PiRing
//	
// Purpose	:	
//****************************************************************************
// Wnd_CylinderTable.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_CylinderTable.h"

// CWnd_CylinderTable

#define IDC_BN_CONTROL		1000
#define IDC_ST_CYL_ITEM		2000

IMPLEMENT_DYNAMIC(CWnd_CylinderTable, CWnd)


CWnd_CylinderTable::CWnd_CylinderTable()
{
	VERIFY(m_font_Data.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_SEMIBOLD,			// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_CylinderTable::~CWnd_CylinderTable()
{
	m_font_Data.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_CylinderTable, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BN_CONTROL, IDC_BN_CONTROL + 999, OnRangeCmds)
END_MESSAGE_MAP()

//=============================================================================
// Method		: OnCreate
// Access		: protected  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
int CWnd_CylinderTable::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_CYL.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_CYL.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_CYL.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_CYL.SetFont_Gdip(L"Arial", 12.0F);
	m_st_CYL.Create(_T("CYLINDER CONTROL"), WS_VISIBLE | WS_CHILD | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);
	
	for (UINT nIdx = 0; nIdx < CYL_Fo_MaxEnum; nIdx++)
	{
		m_st_CYL_Name[nIdx].SetFont_Gdip(L"Arial", 9.0f, Gdiplus::FontStyleRegular);
		m_st_CYL_Name[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_CYL_Name[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_st_CYL_Name[nIdx].SetTextAlignment(StringAlignmentNear);
		m_st_CYL_Name[nIdx].Create(g_szCYL_Focusing[nIdx], WS_VISIBLE | WS_CHILD | SS_LEFT | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_ST_CYL_ITEM + nIdx);

		m_bn_CYL_Status[nIdx].Create(_T(""), WS_VISIBLE | WS_CHILD | BS_PUSHLIKE , rectDummy, this, IDC_BN_CONTROL + nIdx);
	}

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: protected  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2016/5/29 - 10:54
// Desc.		:
//=============================================================================
void CWnd_CylinderTable::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iMargin		= 10;
	int iSpacing	= 5;
	int iLeft		= 0;
	int iTop		= 0;
	int iWidth		= cx;
	int iHeight		= cy;
	int iLeftSub	= 0;
	int iCtrlH		= (iHeight - iSpacing - iSpacing * CYL_Fo_MaxEnum) / (CYL_Fo_MaxEnum + 1);

	int iStatusW	= 80; // 윈도우 크기에 따라서 가변
	int iCtrlW		= iWidth - iSpacing -iStatusW;

	m_st_CYL.MoveWindow(0, 9, iWidth, iCtrlH - 12);

	iLeft		= 0;
	iLeftSub	= iLeft + iStatusW + iSpacing;
	iTop		= iCtrlH + iSpacing;

	for (UINT nIdx = 0; nIdx < CYL_Fo_MaxEnum; nIdx++)
	{
		m_bn_CYL_Status[nIdx].MoveWindow(iLeft, iTop, iStatusW, iCtrlH + 1);
		m_st_CYL_Name[nIdx].MoveWindow(iLeftSub, iTop, iCtrlW, iCtrlH + 1);
	
		iTop += iCtrlH + iSpacing;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/2/5 - 18:30
// Desc.		:
//=============================================================================
BOOL CWnd_CylinderTable::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2016/5/29 - 10:55
// Desc.		:
//=============================================================================
void CWnd_CylinderTable::OnRangeCmds(UINT nID)
{
	UINT nIndex = nID - IDC_BN_CONTROL;

	m_st_CYL_Name[nIndex].SetColorStyle(CVGStatic::ColorStyle_Default);

	switch (nIndex)
	{
	case CYL_Fo_FixOn:
		m_st_CYL_Name[CYL_Fo_FixOff].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_FixOff:
		m_st_CYL_Name[CYL_Fo_FixOn].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_StageUp:
		m_st_CYL_Name[CYL_Fo_StageDown].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_StageDown:
		m_st_CYL_Name[CYL_Fo_StageUp].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_GripperOn:
		m_st_CYL_Name[CYL_Fo_GripperOff].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_GripperOff:
		m_st_CYL_Name[CYL_Fo_GripperOn].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_ParticleIn:
		m_st_CYL_Name[CYL_Fo_ParticleOut].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_ParticleOut:
		m_st_CYL_Name[CYL_Fo_ParticleIn].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_DriveUp:
		m_st_CYL_Name[CYL_Fo_DriveDown].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	case CYL_Fo_DriveDown:
		m_st_CYL_Name[CYL_Fo_DriveUp].SetColorStyle(CVGStatic::ColorStyle_Default);
		break;
	default:
		break;
	}

	if (TRUE == GetOwner()->SendMessageW(WM_CYLINGER_MANUL, nIndex, m_nParaID))
	{
		Set_CylinderResult(nIndex, TRUE);
	}
	else
	{
		Set_CylinderResult(nIndex, FALSE);
	}

}

//=============================================================================
// Method		: Set_CylenderResult
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nItem
// Parameter	: __in BOOL bResult
// Qualifier	:
// Last Update	: 2018/3/8 - 13:55
// Desc.		:
//=============================================================================
void CWnd_CylinderTable::Set_CylinderResult(__in UINT nItem, __in BOOL bResult)
{
	if (bResult)
	{
		m_st_CYL_Name[nItem].SetColorStyle(CVGStatic::ColorStyle_Blue);
	}
	else
	{
		m_st_CYL_Name[nItem].SetColorStyle(CVGStatic::ColorStyle_Red);
	}
}

//=============================================================================
// Method		: Set_CylinderReset
// Access		: protected  
// Returns		: void
// Parameter	: __in UINT nItem
// Qualifier	:
// Last Update	: 2018/3/12 - 9:11
// Desc.		:
//=============================================================================
void CWnd_CylinderTable::Set_CylinderReset(__in UINT nItem)
{
	m_st_CYL_Name[nItem].SetColorStyle(CVGStatic::ColorStyle_Default);
}
