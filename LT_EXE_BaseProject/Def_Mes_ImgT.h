﻿#ifndef Def_Mes_ImgT_h__
#define Def_Mes_ImgT_h__

#include <afxwin.h>


#pragma pack(push,1)

#ifdef SET_GWANGJU
static LPCTSTR g_szMESHeader[] =
{
	_T("Current_1.8V"),
	_T("Current_3.3V"),
	_T("Current_9.0V"),
	_T("Current_14.7V"),
	_T("Current_-5.7V"),
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("Rotation"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Distortion"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Fov_수평"),
	_T("Fov_수직"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Stain_#1 농도"),
	_T("Stain_#1 개수"),
	_T("Stain_#2 농도"),
	_T("Stain_#2 개수"),
	_T("Stain_#3 농도"),
	_T("Stain_#3 개수"),
	_T("Stain_#4 농도"),
	_T("Stain_#4 개수"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("DefectPixel_Very Hot Pixel"),
	_T("DefectPixel_Hot Pixel"),
	_T("DefectPixel_Very Bright Pixel"),
	_T("DefectPixel_Bright Pixel"),
	_T("DefectPixel_Very Dark Pixel"),
	_T("DefectPixel_Dark Pixel"),
	_T("DefectPixel_Cluster"),
	_T("DefectPixel_Dark Row"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("D/R_1 영역 - 밝기(W)"),
	_T("D/R_1 영역 - 밝기(G)"),
	_T("D/R_1 영역 - 밝기(B)"),
	_T("D/R_1 영역 - STD(W)"),
	_T("D/R_1 영역 - STD(G"),
	_T("D/R_1 영역 - STD(B)"),
	_T("D/R_1 영역 - SNR_BW"),
	_T("D/R_1 영역 - DynamicRange"),
	_T("D/R_2 영역 - 밝기(W)"),
	_T("D/R_2 영역 - 밝기(G)"),
	_T("D/R_2 영역 - 밝기(B)"),
	_T("D/R_2 영역 - STD(W)"),
	_T("D/R_2 영역 - STD(G)"),
	_T("D/R_2 영역 - STD(B)"),
	_T("D/R_2 영역 - SNR_BW"),
	_T("D/R_2 영역 - DynamicRange"),
	_T("D/R_3 영역 - 밝기(W)"),
	_T("D/R_3 영역 - 밝기(G)"),
	_T("D/R_3 영역 - 밝기(B)"),
	_T("D/R_3 영역 - STD(W)"),
	_T("D/R_3 영역 - STD(G)"),
	_T("D/R_3 영역 - STD(B)"),
	_T("D/R_3 영역 - SNR_BW"),
	_T("D/R_3 영역 - DynamicRange"),
	_T("D/R_4 영역 - 밝기(W)"),
	_T("D/R_4 영역 - 밝기(G)"),
	_T("D/R_4 영역 - 밝기(B)"),
	_T("D/R_4 영역 - STD(W)"),
	_T("D/R_4 영역 - STD(G)"),
	_T("D/R_4 영역 - STD(B"),
	_T("D/R_4 영역 - SNR_BW"),
	_T("D/R_4 영역 - DynamicRange"),
	_T("D/R_5 영역 - 밝기(W)"),
	_T("D/R_5 영역 - 밝기(G)"),
	_T("D/R_5 영역 - 밝기(B)"),
	_T("D/R_5 영역 - STD(W)"),
	_T("D/R_5 영역 - STD(G)"),
	_T("D/R_5 영역 - STD(B)"),
	_T("D/R_5 영역 - SNR_BW"),
	_T("D/R_5 영역 - DynamicRange"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Shading_Center - 밝기"),
	_T("Shading_Center - ratio"),
	_T("Shading_우상 - 밝기"),
	_T("Shading_우상 - ratio"),
	_T("Shading_우하 - 밝기"),
	_T("Shading_우하 - ratio"),
	_T("Shading_좌상 - 밝기"),
	_T("Shading_좌상 - ratio"),
	_T("Shading_좌하 - 밝기"),
	_T("Shading_좌하 - ratio"),
	_T("SNRLIGHT_1"),
	_T("SNRLIGHT_2"),
	_T("SNRLIGHT_3"),
	_T("SNRLIGHT_4"),
	_T("SNRLIGHT_5"),
	_T("SNRLIGHT_6"),
	_T("SNRLIGHT_7"),
	_T("SNRLIGHT_8"),
	_T("SNRLIGHT_9"),
	_T("SNRLIGHT_10"),
	_T("SNRLIGHT_11"),
	_T("SNRLIGHT_12"),
	_T("SNRLIGHT_13"),
	_T("SNRLIGHT_14"),
	_T("SNRLIGHT_15"),
	_T("SNRLIGHT_16"),
	_T("SNRLIGHT_17"),
	_T("SNRLIGHT_18"),
	_T("SNRLIGHT_19"),
	_T("SNRLIGHT_20"),
	_T("SNRLIGHT_21"),
	_T("SNRLIGHT_22"),
	_T("SNRLIGHT_23"),
	_T("SNRLIGHT_24"),
	_T("SNRLIGHT_25"),
	_T("SNRLIGHT_26"),
	_T("SNRLIGHT_27"),
	_T("SNRLIGHT_28"),
	_T("SNRLIGHT_29"),
	_T("SNRLIGHT_30"),
	_T("SNRLIGHT_31"),
	_T("SNRLIGHT_32"),
	_T("SNRLIGHT_33"),
	_T("SNRLIGHT_34"),
	_T("SNRLIGHT_35"),
	_T("SNRLIGHT_36"),
	_T("SNRLIGHT_37"),
	_T("SNRLIGHT_38"),
	_T("SNRLIGHT_39"),
	_T("SNRLIGHT_40"),
	_T("SNRLIGHT_41"),
	_T("SNRLIGHT_42"),
	_T("SNRLIGHT_43"),
	_T("SNRLIGHT_44"),
	_T("SNRLIGHT_45"),
	_T("SNRLIGHT_46"),
	_T("SNRLIGHT_47"),
	_T("SNRLIGHT_48"),
	_T("SNRLIGHT_49"),
	_T("SNRLIGHT_50"),
	_T("SNRLIGHT_51"),
	_T("SNRLIGHT_52"),
	_T("SNRLIGHT_53"),
	_T("SNRLIGHT_54"),
	_T("SNRLIGHT_55"),
	_T("SNRLIGHT_56"),
	_T("Shading_1"),
	_T("Shading_2"),
	_T("Shading_3"),
	_T("Shading_4"),
	_T("Shading_5"),
	_T("Shading_6"),
	_T("Shading_7"),
	_T("Shading_8"),
	_T("Shading_9"),
	_T("Shading_10"),
	_T("Shading_11"),
	_T("Shading_12"),
	_T("Shading_13"),
	_T("Shading_14"),
	_T("Shading_15"),
	_T("Shading_16"),
	_T("Shading_17"),
	_T("Shading_18"),
	_T("Shading_19"),
	_T("Shading_20"),
	_T("Shading_21"),
	_T("Shading_22"),
	_T("Shading_23"),
	_T("Shading_24"),
	_T("Shading_25"),
	_T("Shading_26"),
	_T("Shading_27"),
	_T("Shading_28"),
	_T("Shading_29"),
	_T("Shading_30"),
	_T("Shading_31"),
	_T("Shading_32"),
	_T("Shading_33"),
	_T("Shading_34"),
	_T("Shading_35"),
	_T("Shading_36"),
	_T("Shading_37"),
	_T("Shading_38"),
	_T("Shading_39"),
	_T("Shading_40"),
	_T("Shading_41"),
	_T("Shading_42"),
	_T("Shading_43"),
	_T("Shading_44"),
	_T("Shading_45"),
	_T("Shading_46"),
	_T("Shading_47"),
	_T("Shading_48"),
	_T("Shading_49"),
	_T("Shading_50"),
	_T("Shading_51"),
	_T("Shading_52"),
	_T("Shading_53"),
	_T("Shading_54"),
	_T("Shading_55"),
	_T("Shading_56"),
	_T("Shading_57"),
	_T("Shading_58"),
	_T("Shading_59"),
	_T("Shading_60"),
	_T("Shading_61"),
	_T("Shading_62"),
	_T("Shading_63"),
	_T("Shading_64"),
	_T("Shading_65"),
	_T("Shading_66"),
	_T("Shading_67"),
	_T("Shading_68"),
	_T("Shading_69"),
	_T("Shading_70"),
	_T("Shading_71"),
	_T("Shading_72"),
	_T("Shading_73"),
	_T("HotPixel"),
	_T("Reserved"),
	_T("Reserved"),
	_T("FPN_X"),
	_T("FPN_Y"),
	_T("Reserved"),
	_T("Reserved"),
	_T("3D_Depth_Brightness"),
	_T("3D_Depth_Deviation"),
	_T("Reserved"),
	_T("Reserved"),
	_T("EEPROM_OC_X"),
	_T("EEPROM_OC_Y"),
	_T("EEPROM_CHECK_1"),
	_T("EEPROM_CHECK_2"),
	_T("Reserved"),
	_T("Reserved"),
};

#else	
static LPCTSTR g_szMESHeader[] =
{
	_T("Current_1.8V"),
	_T("Current_2.8V"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("SFR_MIN_0"),
	_T("SFR_MIN_1"),
	_T("SFR_MIN_2"),
	_T("SFR_MIN_3"),
	_T("SFR_MIN_4"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("Rotation"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Distortion"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Fov_수평"),
	_T("Fov_수직"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Stain_#1 농도"),
	_T("Stain_#1 개수"),
	_T("Stain_#2 농도"),
	_T("Stain_#2 개수"),
	_T("Stain_#3 농도"),
	_T("Stain_#3 개수"),
	_T("Stain_#4 농도"),
	_T("Stain_#4 개수"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("DefectPixel_Very Hot Pixel"),
	_T("DefectPixel_Hot Pixel"),
	_T("DefectPixel_Very Bright Pixel"),
	_T("DefectPixel_Bright Pixel"),
	_T("DefectPixel_Very Dark Pixel"),
	_T("DefectPixel_Dark Pixel"),
	_T("DefectPixel_Cluster"),
	_T("DefectPixel_Dark Row"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("D/R_1 영역 - 밝기(W)"),
	_T("D/R_1 영역 - 밝기(G)"),
	_T("D/R_1 영역 - 밝기(B)"),
	_T("D/R_1 영역 - STD(W)"),
	_T("D/R_1 영역 - STD(G"),
	_T("D/R_1 영역 - STD(B)"),
	_T("D/R_1 영역 - SNR_BW"),
	_T("D/R_1 영역 - DynamicRange"),
	_T("D/R_2 영역 - 밝기(W)"),
	_T("D/R_2 영역 - 밝기(G)"),
	_T("D/R_2 영역 - 밝기(B)"),
	_T("D/R_2 영역 - STD(W)"),
	_T("D/R_2 영역 - STD(G)"),
	_T("D/R_2 영역 - STD(B)"),
	_T("D/R_2 영역 - SNR_BW"),
	_T("D/R_2 영역 - DynamicRange"),
	_T("D/R_3 영역 - 밝기(W)"),
	_T("D/R_3 영역 - 밝기(G)"),
	_T("D/R_3 영역 - 밝기(B)"),
	_T("D/R_3 영역 - STD(W)"),
	_T("D/R_3 영역 - STD(G)"),
	_T("D/R_3 영역 - STD(B)"),
	_T("D/R_3 영역 - SNR_BW"),
	_T("D/R_3 영역 - DynamicRange"),
	_T("D/R_4 영역 - 밝기(W)"),
	_T("D/R_4 영역 - 밝기(G)"),
	_T("D/R_4 영역 - 밝기(B)"),
	_T("D/R_4 영역 - STD(W)"),
	_T("D/R_4 영역 - STD(G)"),
	_T("D/R_4 영역 - STD(B"),
	_T("D/R_4 영역 - SNR_BW"),
	_T("D/R_4 영역 - DynamicRange"),
	_T("D/R_5 영역 - 밝기(W)"),
	_T("D/R_5 영역 - 밝기(G)"),
	_T("D/R_5 영역 - 밝기(B)"),
	_T("D/R_5 영역 - STD(W)"),
	_T("D/R_5 영역 - STD(G)"),
	_T("D/R_5 영역 - STD(B)"),
	_T("D/R_5 영역 - SNR_BW"),
	_T("D/R_5 영역 - DynamicRange"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Shading_Center - 밝기"),
	_T("Shading_Center - ratio"),
	_T("Shading_우상 - 밝기"),
	_T("Shading_우상 - ratio"),
	_T("Shading_우하 - 밝기"),
	_T("Shading_우하 - ratio"),
	_T("Shading_좌상 - 밝기"),
	_T("Shading_좌상 - ratio"),
	_T("Shading_좌하 - 밝기"),
	_T("Shading_좌하 - ratio"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),  //-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),  //-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-10
};

#endif

static LPCTSTR g_szMESHeader_IR[] =
{
	_T("Current_1.8V"),
	_T("Current_2.8V"),
	_T("Current_LED"),
	//_T("Reserved"),
	//_T("Reserved"),
	//_T("OpticalCenter_X"),
	//_T("OpticalCenter_Y"),
	//_T("Reserved"),
	//_T("Reserved"),
	//_T("Reserved"),
	//_T("SFR_MIN_0"),
	//_T("SFR_MIN_1"),
	//_T("SFR_MIN_2"),
	//_T("SFR_MIN_3"),
	//_T("SFR_MIN_4"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("Rotation"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Distortion"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Fov_수평"),
	_T("Fov_수직"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Stain_#1 농도"),
	_T("Stain_#1 개수"),
	_T("Stain_#2 농도"),
	_T("Stain_#2 개수"),
	_T("Stain_#3 농도"),
	_T("Stain_#3 개수"),
	_T("Stain_#4 농도"),
	_T("Stain_#4 개수"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("DefectPixel_Very Hot Pixel"),
	_T("DefectPixel_Hot Pixel"),
	_T("DefectPixel_Very Bright Pixel"),
	_T("DefectPixel_Bright Pixel"),
	_T("DefectPixel_Very Dark Pixel"),
	_T("DefectPixel_Dark Pixel"),
	_T("DefectPixel_Cluster"),
	_T("DefectPixel_Dark Row"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("D/R_1 영역 - 밝기(W)"),
	_T("D/R_1 영역 - 밝기(G)"),
	_T("D/R_1 영역 - 밝기(B)"),
	_T("D/R_1 영역 - STD(W)"),
	_T("D/R_1 영역 - STD(G"),
	_T("D/R_1 영역 - STD(B)"),
	_T("D/R_1 영역 - SNR_BW"),
	_T("D/R_1 영역 - DynamicRange"),
	_T("D/R_2 영역 - 밝기(W)"),
	_T("D/R_2 영역 - 밝기(G)"),
	_T("D/R_2 영역 - 밝기(B)"),
	_T("D/R_2 영역 - STD(W)"),
	_T("D/R_2 영역 - STD(G)"),
	_T("D/R_2 영역 - STD(B)"),
	_T("D/R_2 영역 - SNR_BW"),
	_T("D/R_2 영역 - DynamicRange"),
	_T("D/R_3 영역 - 밝기(W)"),
	_T("D/R_3 영역 - 밝기(G)"),
	_T("D/R_3 영역 - 밝기(B)"),
	_T("D/R_3 영역 - STD(W)"),
	_T("D/R_3 영역 - STD(G)"),
	_T("D/R_3 영역 - STD(B)"),
	_T("D/R_3 영역 - SNR_BW"),
	_T("D/R_3 영역 - DynamicRange"),
	_T("D/R_4 영역 - 밝기(W)"),
	_T("D/R_4 영역 - 밝기(G)"),
	_T("D/R_4 영역 - 밝기(B)"),
	_T("D/R_4 영역 - STD(W)"),
	_T("D/R_4 영역 - STD(G)"),
	_T("D/R_4 영역 - STD(B"),
	_T("D/R_4 영역 - SNR_BW"),
	_T("D/R_4 영역 - DynamicRange"),
	_T("D/R_5 영역 - 밝기(W)"),
	_T("D/R_5 영역 - 밝기(G)"),
	_T("D/R_5 영역 - 밝기(B)"),
	_T("D/R_5 영역 - STD(W)"),
	_T("D/R_5 영역 - STD(G)"),
	_T("D/R_5 영역 - STD(B)"),
	_T("D/R_5 영역 - SNR_BW"),
	_T("D/R_5 영역 - DynamicRange"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Shading_Center - 밝기"),
	_T("Shading_Center - ratio"),
	_T("Shading_우상 - 밝기"),
	_T("Shading_우상 - ratio"),
	_T("Shading_우하 - 밝기"),
	_T("Shading_우하 - ratio"),
	_T("Shading_좌상 - 밝기"),
	_T("Shading_좌상 - ratio"),
	_T("Shading_좌하 - 밝기"),
	_T("Shading_좌하 - ratio"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),  //-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),  //-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-1
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),//-10
	_T("Reserved"),//-10
};



//항목 순서
typedef enum enMesDataIndex_IR
{
	MesDataIdx_ECurrent_IR = 0,
	MesDataIdx_SFR_IR,
	MesDataIdx_DyanamicRange_IR,		//dynamic Range
	MesDataIdx_SNR_BW_IR,
	MesDataIdx_Shading_IR,
	MesDataIdx_Ymean_IR,
	MesDataIdx_Distortion_IR,
	MesDataIdx_DefectBlack_IR,
	MesDataIdx_DefectWhite_IR,
	MesDataIdx_MAX_IR,
};


//항목 순서
typedef enum enMesDataIndex
{
	MesDataIdx_ECurrent = 0,
	MesDataIdx_OpticalCenter,
	MesDataIdx_SFR,
	MesDataIdx_Rotation,
	MesDataIdx_Distortion,
	MesDataIdx_FOV,
	MesDataIdx_Stain,
	MesDataIdx_DefectPixel,
	MesDataIdx_Particle_SNR,		//dynamic Range
	MesDataIdx_Intensity,
	MesDataIdx_SNR_Light,
	MesDataIdx_Shading,
#ifdef SET_GWANGJU
	MesDataIdx_HotPixel,
	MesDataIdx_FPN,
	MesDataIdx_3DDepth,
	MesDataIdx_EEPROM,
#endif
	MesDataIdx_MAX,
};


//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM 
{
	MESDataNum_ECurrent			= 3,
	MESDataNum_OpticalCenter	= 5,
	MESDataNum_SFR				= 30,
	MesDataNUM_DynamicBW		= 2,
	MESDataNum_Rotation			= 3,
	MESDataNum_Distortion		= 3,
	MESDataNum_FOV				= 4,
	MESDataNum_Stain			= 11 ,
	MESDataNum_DefectPixel		= 11,
	MESDataNum_Particle_SNR		= 48,
	MESDataNum_Intensity		= 15,
	MESDataNum_SNR_Light		= 56,
	//MESDataNum_Shading			= 73,//광량비 Shading
	MESDataNum_Shading			= 19,
	MESDataNum_HotPixel			= 3,
	MESDataNum_FPN				= 4,
	MESDataNum_3DDepth			= 4,
	MESDataNum_EEPROM			= 6,
	MESDataNum_RI				= 2,
} MesData_DataNUM;


typedef struct _tag_MesData
{
	CString szMesTestData[2][MesDataIdx_MAX];
	CString szMesTestLogData[2][MesDataIdx_MAX];

	int		nMesDataNum[MesDataIdx_MAX];
	CString szDataName[MesDataIdx_MAX];

	_tag_MesData()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_ECurrent]		= MESDataNum_ECurrent;
		nMesDataNum[MesDataIdx_OpticalCenter]	= MESDataNum_OpticalCenter;
		nMesDataNum[MesDataIdx_Rotation]		= MESDataNum_Rotation;
		nMesDataNum[MesDataIdx_Distortion]		= MESDataNum_Distortion;
		nMesDataNum[MesDataIdx_FOV]				= MESDataNum_FOV;
		nMesDataNum[MesDataIdx_SFR]				= MESDataNum_SFR;
		nMesDataNum[MesDataIdx_Stain]			= MESDataNum_Stain;
		nMesDataNum[MesDataIdx_DefectPixel]		= MESDataNum_DefectPixel;
		nMesDataNum[MesDataIdx_Particle_SNR]	= MESDataNum_Particle_SNR;
		nMesDataNum[MesDataIdx_Intensity]		= MESDataNum_Intensity;
		nMesDataNum[MesDataIdx_SNR_Light]		= MESDataNum_SNR_Light;
		nMesDataNum[MesDataIdx_Shading]			= MESDataNum_Shading;

		szDataName[MesDataIdx_ECurrent]			= _T("Current");
		szDataName[MesDataIdx_OpticalCenter]	= _T("Optical Center");
		szDataName[MesDataIdx_Rotation]			= _T("Rotate");
		szDataName[MesDataIdx_Distortion]		= _T("Distortion");
		szDataName[MesDataIdx_FOV]				= _T("Fov");
		szDataName[MesDataIdx_SFR]				= _T("SFR");
		szDataName[MesDataIdx_Stain]			= _T("Statin");
		szDataName[MesDataIdx_DefectPixel]		= _T("DefectPixel");
		szDataName[MesDataIdx_Particle_SNR]		= _T("DynamicRange");
		szDataName[MesDataIdx_Intensity]		= _T("Shading[RI]");
		szDataName[MesDataIdx_SNR_Light]		= _T("SNR Light");
		szDataName[MesDataIdx_Shading]			= _T("Shading[SNR]");

#ifdef SET_GWANGJU
		nMesDataNum[MesDataIdx_HotPixel]	= MESDataNum_HotPixel;
		nMesDataNum[MesDataIdx_FPN]			= MESDataNum_FPN;
		nMesDataNum[MesDataIdx_3DDepth]		= MESDataNum_3DDepth;
		nMesDataNum[MesDataIdx_EEPROM]		= MESDataNum_EEPROM;

		szDataName[MesDataIdx_HotPixel]		= _T("HotPixel");
		szDataName[MesDataIdx_FPN]			= _T("FPN");
		szDataName[MesDataIdx_3DDepth]		= _T("3D_Depth");
		szDataName[MesDataIdx_EEPROM]		= _T("EEPROM");
#endif

	};

	void Reset(int Num)
	{
		for (int t = 0; t < MesDataIdx_MAX; t++)
		{
			szMesTestData[Num][t].Empty();
		}
	};

	_tag_MesData& operator= (_tag_MesData& ref)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int t = 0; t < MesDataIdx_MAX; t++)
			{
				szMesTestData[k][t] = ref.szMesTestData[k][t];
			}
		}
		
		return *this;
	};

}ST_MES_Data, *PST_MES_Data;


typedef struct _tag_MesData_IR
{
	CString szMesTestData[2][MesDataIdx_MAX_IR];
	CString szMesTestLogData[2][MesDataIdx_MAX_IR];

	int		nMesDataNum[MesDataIdx_MAX_IR];
	CString szDataName[MesDataIdx_MAX_IR];

	_tag_MesData_IR()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_ECurrent_IR] = MESDataNum_ECurrent;
		//nMesDataNum[MesDataIdx_OpticalCenter] = MESDataNum_OpticalCenter;
		//nMesDataNum[MesDataIdx_Rotation] = MESDataNum_Rotation;
		//nMesDataNum[MesDataIdx_Distortion] = MESDataNum_Distortion;
		//nMesDataNum[MesDataIdx_FOV] = MESDataNum_FOV;
		nMesDataNum[MesDataIdx_SFR_IR] = MESDataNum_SFR;
		nMesDataNum[MesDataIdx_DyanamicRange_IR] = MesDataNUM_DynamicBW;
		nMesDataNum[MesDataIdx_SNR_BW_IR] = MESDataNum_SNR_Light;
		nMesDataNum[MesDataIdx_Shading_IR] = MESDataNum_Shading;
		nMesDataNum[MesDataIdx_Ymean_IR] = MESDataNum_Stain;

		szDataName[MesDataIdx_ECurrent_IR] = _T("Current");
		szDataName[MesDataIdx_SFR_IR] = _T("SFR");
		szDataName[MesDataIdx_DyanamicRange_IR] = _T("DyanamicRange");
		szDataName[MesDataIdx_SNR_BW_IR] = _T("SNR_BW");
		szDataName[MesDataIdx_Shading_IR] = _T("Shading");
		szDataName[MesDataIdx_Ymean_IR] = _T("Stain");
		szDataName[MesDataIdx_Distortion_IR] = _T("Distortion");
		szDataName[MesDataIdx_DefectBlack_IR] = _T("DefectBlack");
		szDataName[MesDataIdx_DefectWhite_IR] = _T("DefecWhite");
		
#ifdef SET_GWANGJU
		nMesDataNum[MesDataIdx_HotPixel] = MESDataNum_HotPixel;
		nMesDataNum[MesDataIdx_FPN] = MESDataNum_FPN;
		nMesDataNum[MesDataIdx_3DDepth] = MESDataNum_3DDepth;
		nMesDataNum[MesDataIdx_EEPROM] = MESDataNum_EEPROM;

		szDataName[MesDataIdx_HotPixel] = _T("HotPixel");
		szDataName[MesDataIdx_FPN] = _T("FPN");
		szDataName[MesDataIdx_3DDepth] = _T("3D_Depth");
		szDataName[MesDataIdx_EEPROM] = _T("EEPROM");
#endif

	};

	void Reset(int Num)
	{
		for (int t = 0; t < MesDataIdx_MAX_IR; t++)
		{
			szMesTestData[Num][t].Empty();
		}
	};

	_tag_MesData_IR& operator= (_tag_MesData_IR& ref)
	{
		for (int k = 0; k < 2; k++)
		{
			for (int t = 0; t < MesDataIdx_MAX_IR; t++)
			{
				szMesTestData[k][t] = ref.szMesTestData[k][t];
			}
		}

		return *this;
	};

}ST_MES_Data_IR, *PST_MES_Data_IR;


#pragma pack(pop)

#endif // Def_FOV_h__