﻿#ifndef TestManager_TestMES_IR_h__
#define TestManager_TestMES_IR_h__

#pragma once

#include <windows.h>

#include "Def_DataStruct.h"
#include "File_Mes.h"
#include "Def_Mes_IR_ImgT.h"
#include "Def_Mes_Foc.h"
#include "Def_Mes_EachTest.h"


class CTestManager_TestMES_IR 
{
public:
	CTestManager_TestMES_IR();
	virtual ~CTestManager_TestMES_IR();

	//-모든 정보
	ST_InspectionInfo *m_pInspInfo;

	void	SetPtr_InspInfo(ST_InspectionInfo* pstInfo)
	{
		if (pstInfo == NULL)
			return;

		m_pInspInfo = pstInfo;
	};

	//전류
	void Current_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData);
	//SFR
	void SFR_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData, int nDistance);
	//Dynamic Range
//	void DynamicRange_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData);
	//Stain
	void Ymean_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData);
	//Shading
	void RI_DataSave(int nParaIdx, ST_MES_Data_IR_IKC *pMesData);


	//전류
	void Current_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData);
	//SFR
	void SFR_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData, int nDistance);
	//Dynamic Range
	void DynamicBW_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData);
	//Stain
	void Ymean_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData);
	//Shading
	void RI_DataSave(int nParaIdx, ST_MES_Data_IR_MRA2 *pMesData);



	//-개별 저장 
	void Current_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void SFR_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DynamicRange_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void Ymean_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz,  CStringArray* pAddData, UINT &nResult);
	void RI_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Distortion_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DefectBlack_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void DefectWhite_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Shading_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	
	void OpicalCenter_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Vision_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void Displace_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
	void IIC_Each_DataSave(enInsptrSysType nSysType, int nParaIdx, CStringArray* pAddHeaderz, CStringArray* pAddData, UINT &nResult);
};

#endif // TestManager_TestMES_h__
