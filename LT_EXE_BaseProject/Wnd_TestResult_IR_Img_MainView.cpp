﻿// Wnd_TestResult_IR_Img_MainView.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "resource.h"
#include "Wnd_TestResult_IR_Img_MainView.h"


// Wnd_TestResult_IR_Img_MainView
typedef enum TestResult_ImgID
{
	IDC_BTN_ITEM  = 1001,
	IDC_CMB_ITEM  = 2001,
	IDC_EDT_ITEM  = 3001,
	IDC_LIST_ITEM = 4001,
};

IMPLEMENT_DYNAMIC(CWnd_TestResult_IR_Img_MainView, CWnd)

CWnd_TestResult_IR_Img_MainView::CWnd_TestResult_IR_Img_MainView()
{
	m_InspectionType = Sys_Focusing;

	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_HEAVY,				// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_TestResult_IR_Img_MainView::~CWnd_TestResult_IR_Img_MainView()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_TestResult_IR_Img_MainView, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_ITEM, IDC_BTN_ITEM + 999, OnRangeBtnCtrl)
	ON_CBN_SELENDOK(IDC_CMB_ITEM, OnLbnSelChangeTest)
END_MESSAGE_MAP()

// CWnd_TestResult_IR_Img_MainView 메시지 처리기입니다.
//=============================================================================
// Method		: OnCreate
// Access		: public  
// Returns		: int
// Parameter	: LPCREATESTRUCT lpCreateStruct
// Qualifier	:
// Last Update	: 2017/1/12 - 17:14
// Desc.		:
//=============================================================================
int CWnd_TestResult_IR_Img_MainView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	for (UINT nIdex = 0; nIdex < STI_TR_IR_Img_Main_MAX; nIdex++)
	{
		m_st_Item[nIdex].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_st_Item[nIdex].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_st_Item[nIdex].SetFont_Gdip(L"Arial", 9.0F);
		m_st_Item[nIdex].Create(g_szTestResult_IR_Img_Main_Static[nIdex], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
	}

	m_st_default.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_default.SetColorStyle(CVGStatic::ColorStyle_White);
	m_st_default.SetFont_Gdip(L"Arial", 9.0F);
	m_st_default.Create(_T("TEST RESULT"), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);


	m_cb_TestItem.Create(dwStyle | CBS_DROPDOWNLIST | WS_VSCROLL, rectDummy, this, IDC_CMB_ITEM);
	m_cb_TestItem.SetFont(&m_font);

	UINT	nWndID = IDC_LIST_ITEM;

	m_Wnd_Rst_SFR.SetOwner(GetOwner());
	m_Wnd_Rst_SFR.Create(NULL, _T("SFR"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Dynamic.SetOwner(GetOwner());
	m_Wnd_Rst_Dynamic.Create(NULL, _T("Dynamic & BW"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Shading.SetOwner(GetOwner());
	m_Wnd_Rst_Shading.Create(NULL, _T("Shading"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Ymean.SetOwner(GetOwner());
	m_Wnd_Rst_Ymean.Create(NULL, _T("Ymean"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_LCB.SetOwner(GetOwner());
	m_Wnd_Rst_LCB.Create(NULL, _T("LCB"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_BlackSpot.SetOwner(GetOwner());
	m_Wnd_Rst_BlackSpot.Create(NULL, _T("BlackSpot"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_RI.SetOwner(GetOwner());
	m_Wnd_Rst_RI.Create(NULL, _T("Rllumination"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Current.SetOwner(GetOwner());
	m_Wnd_Rst_Current.Create(NULL, _T("Current"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_IIC.SetOwner(GetOwner());
	m_Wnd_Rst_IIC.Create(NULL, _T("IIC"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Displace.SetOwner(GetOwner());
	m_Wnd_Rst_Displace.Create(NULL, _T("Displace"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_Vision.SetOwner(GetOwner());
	m_Wnd_Rst_Vision.Create(NULL, _T("Vision"), dwStyle, rectDummy, this, nWndID++);
	m_Wnd_Rst_Distortion.SetOwner(GetOwner());
	m_Wnd_Rst_Distortion.Create(NULL, _T("Distortion"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_DefectBlack.SetOwner(GetOwner());
	m_Wnd_Rst_DefectBlack.Create(NULL, _T("DefectBlack"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_DefectWhite.SetOwner(GetOwner());
	m_Wnd_Rst_DefectWhite.Create(NULL, _T("DefectWhite"), dwStyle, rectDummy, this, nWndID++);

	m_Wnd_Rst_OpticalCenter.SetOwner(GetOwner());
	m_Wnd_Rst_OpticalCenter.Create(NULL, _T("OpticalCenter"), dwStyle, rectDummy, this, nWndID++);
	
	m_cb_TestItem.AddString(_T("Current"));

	m_Wnd_Rst_Current.ShowWindow(SW_SHOW);
	m_Wnd_Rst_IIC.ShowWindow(SW_HIDE);
	m_Wnd_Rst_SFR.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Shading.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Ymean.ShowWindow(SW_HIDE); 
	m_Wnd_Rst_LCB.ShowWindow(SW_HIDE);
	m_Wnd_Rst_BlackSpot.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Displace.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Distortion.ShowWindow(SW_HIDE);
	m_Wnd_Rst_DefectBlack.ShowWindow(SW_HIDE);
	m_Wnd_Rst_DefectWhite.ShowWindow(SW_HIDE);
	m_Wnd_Rst_OpticalCenter.ShowWindow(SW_HIDE);


	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	int iMargin  = 10;
	int iSpacing = 5;

	int iLeft	 = 0;
	int iTop	 = 0;
	int iWidth   = cx;
	int iHeight  = cy;
	
	int iStHeight = 20;
	int iCbHeight = 25;

	m_st_Item[STI_TR_IR_Img_Main_Title].MoveWindow(iLeft, iTop, iWidth, iStHeight);
	
	iTop += iStHeight + 2;

	m_cb_TestItem.MoveWindow(iLeft, iTop, iWidth, 100);
	iTop += iCbHeight + 2;
	
	m_Wnd_Rst_SFR.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
//	m_Wnd_Rst_Dynamic.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Shading.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Ymean.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_LCB.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_BlackSpot.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
//	m_Wnd_Rst_RI.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_IIC.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Current.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Displace.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Vision.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_Distortion.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_DefectBlack.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_DefectWhite.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
	m_Wnd_Rst_OpticalCenter.MoveWindow(iLeft, iTop, iWidth, iHeight - iTop);
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/1/12 - 17:12
// Desc.		:
//=============================================================================
BOOL CWnd_TestResult_IR_Img_MainView::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnShowWindow
// Access		: public  
// Returns		: void
// Parameter	: BOOL bShow
// Parameter	: UINT nStatus
// Qualifier	:
// Last Update	: 2017/2/13 - 17:02
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWnd::OnShowWindow(bShow, nStatus);
}

//=============================================================================
// Method		: OnRangeBtnCtrl
// Access		: protected  
// Returns		: void
// Parameter	: UINT nID
// Qualifier	:
// Last Update	: 2017/10/12 - 17:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::OnRangeBtnCtrl(UINT nID)
{
	UINT nIdex = nID - IDC_BTN_ITEM;
}

//=============================================================================
// Method		: AllDataReset
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 19:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::AllDataReset()
{
//	m_Wnd_Rst_Dynamic.Result_Reset();
	m_Wnd_Rst_SFR.Result_Reset();
	m_Wnd_Rst_Shading.Result_Reset();
	m_Wnd_Rst_Ymean.Result_Reset();
	m_Wnd_Rst_LCB.Result_Reset();
	m_Wnd_Rst_BlackSpot.Result_Reset();
//	m_Wnd_Rst_RI.Result_Reset();
	m_Wnd_Rst_Current.Result_Reset();
	m_Wnd_Rst_IIC.Result_Reset();
	m_Wnd_Rst_Displace.Result_Reset();
	m_Wnd_Rst_Vision.Result_Reset();
	m_Wnd_Rst_Distortion.Result_Reset();
	m_Wnd_Rst_DefectBlack.Result_Reset();
	m_Wnd_Rst_DefectWhite.Result_Reset();
	m_Wnd_Rst_OpticalCenter.Result_Reset();
}

//=============================================================================
// Method		: SetUI_Reset
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Qualifier	:
// Last Update	: 2018/5/12 - 19:07
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::SetUI_Reset(int nIdx)
{
	switch (nIdx)
	{
	case TI_IR_ImgT_Fn_SFR:
	{
		m_Wnd_Rst_SFR.Result_Reset();
	}
		break;

	case TI_IR_ImgT_Fn_Ymean:
	{
		m_Wnd_Rst_Ymean.Result_Reset();
	}
		break;

	case TI_IR_ImgT_Fn_LCB:
	{
		m_Wnd_Rst_LCB.Result_Reset();
	}
		break;

	case TI_IR_ImgT_Fn_BlackSpot:
	{
		m_Wnd_Rst_BlackSpot.Result_Reset();
	}
		break;


	case TI_IR_ImgT_Fn_Shading:
	{
		m_Wnd_Rst_Shading.Result_Reset();
	}
	break;

	case TI_IR_ImgT_Fn_Current:
	{
		m_Wnd_Rst_Current.Result_Reset();
	}
		break;

	case TI_IR_ImgT_Fn_IIC:
	{
		m_Wnd_Rst_IIC.Result_Reset();
	}
		break;

	case TI_IR_ImgT_Fn_Distortion:
	{
		 m_Wnd_Rst_Distortion.Result_Reset();
	}
		break;

	case TI_IR_ImgT_Fn_DefectBlack:
	{
		m_Wnd_Rst_DefectBlack.Result_Reset();
	}
		break;
		
	case TI_IR_ImgT_Fn_DefectWhite:
	{
		m_Wnd_Rst_DefectWhite.Result_Reset();
	}
		break;
	case TI_IR_ImgT_Fn_Vision:
	{
		m_Wnd_Rst_Vision.Result_Reset();
	}
	break;

	case TI_IR_ImgT_Fn_Displace:
	{
		m_Wnd_Rst_Displace.Result_Reset();
	}
	break;

	case TI_IR_ImgT_Fn_OpticalCenter:
	{
		m_Wnd_Rst_OpticalCenter.Result_Reset();
	}
	break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetUIData
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: LPVOID pParam
// Qualifier	:
// Last Update	: 2018/7/31 - 16:20
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::SetUIData(int nIdx, LPVOID pParam)
{
	if (pParam == NULL)
		return;

	for (int iIndx = 0; iIndx < m_cb_TestItem.GetCount(); iIndx++)
	{
		if (nIdx == m_iSelectTest[iIndx])
		{
			SelectNum(iIndx);
			break;
		}
	}
	
	switch (nIdx)
	{
	case TI_IR_ImgT_Fn_SFR:
	{
		for (int nNum = 0; nNum < enSFR_Result_Max; nNum++)
		{
			//if (((ST_IR_Result *)pParam)->stSFR.bResult[nNum])
			//{
				m_Wnd_Rst_SFR.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stSFR.bResult[nNum], ((ST_IR_Result *)pParam)->stSFR.dbValue[nNum]);
			//}
		}
	}
		break;


	case TI_IR_ImgT_Fn_Ymean:
	{
		for (int nNum = 0; nNum < enYmean_Result_Max; nNum++)
		{
			//if (((ST_IR_Result *)pParam)->stSFR.bResult[nNum])
			//{
			m_Wnd_Rst_Ymean.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stYmean.bYmeanResult, ((ST_IR_Result *)pParam)->stYmean.nDefectCount);
			//}
		}
	}
		break;

	case TI_IR_ImgT_Fn_LCB:
		for (int nNum = 0; nNum < enLCB_Result_Max; nNum++)
		{
			m_Wnd_Rst_LCB.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stLCB.bLCBResult, ((ST_IR_Result *)pParam)->stLCB.nDefectCount);
		}
		break;

	case TI_IR_ImgT_Fn_BlackSpot:
		for (int nNum = 0; nNum < enBlackSpot_Result_Max; nNum++)
		{
			m_Wnd_Rst_BlackSpot.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stBlackSpot.bBlackSpotResult, ((ST_IR_Result *)pParam)->stBlackSpot.nDefectCount);
		}
		break;

	case TI_IR_ImgT_Fn_Distortion:
	{
		for (int nNum = 0; nNum < enRotate_Result_Max; nNum++)
		{
			//if (((ST_IR_Result *)pParam)->stSFR.bResult[nNum])
			//{
			m_Wnd_Rst_Distortion.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stDistortion.bRotation, ((ST_IR_Result *)pParam)->stDistortion.dbRotation);
			//}
		}
	}
		break;

	case TI_IR_ImgT_Fn_DefectBlack:
	{
		for (int nNum = 0; nNum < enDefect_Black_Result_Max; nNum++)
		{
			//if (((ST_IR_Result *)pParam)->stSFR.bResult[nNum])
			//{
			m_Wnd_Rst_DefectBlack.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stDefect_Black.bDefect_BlackResult, ((ST_IR_Result *)pParam)->stDefect_Black.nDefect_BlackCount);
			//}
		}
	}
		break;

	case TI_IR_ImgT_Fn_DefectWhite:
	{
		 for (int nNum = 0; nNum < enDefect_White_Result_Max; nNum++)
		{
			//if (((ST_IR_Result *)pParam)->stSFR.bResult[nNum])
			//{
			m_Wnd_Rst_DefectWhite.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stDefect_White.bDefect_WhiteResult, ((ST_IR_Result *)pParam)->stDefect_White.nDefect_WhiteCount);
			//}
		}
	}
		break;

	case TI_IR_ImgT_Fn_Shading:
		for (int nNum = 0; nNum < enShading_Result_Max; nNum++)
		{
			m_Wnd_Rst_Shading.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stShading.bResult[nNum], ((ST_IR_Result *)pParam)->stShading.dValue[nNum]);
		}
		break;

	case TI_IR_ImgT_Fn_Current:
	{
								  for (int nNum = 0; nNum < enCurrent_Result_Max; nNum++)
								  {
									  m_Wnd_Rst_Current.Result_Display(0, nNum, TRUE, ((ST_IR_Result *)pParam)->stCurrent.dbValue[nNum]);
								  }
	}
		break;

	case TI_IR_ImgT_Fn_IIC:
	{
		for (int nNum = 0; nNum < enIIC_Result_Max; nNum++)
		{
			m_Wnd_Rst_IIC.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stIIC.bResult[nNum], ((ST_IR_Result *)pParam)->stIIC.dbValue[nNum]);
		}
	}

	case TI_IR_ImgT_Fn_Vision:
	{
		for (int nNum = 0; nNum < enVision_Result_Max; nNum++)
		{
			m_Wnd_Rst_Vision.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stVision.bResult[nNum], ((ST_IR_Result *)pParam)->stVision.dbValue[nNum]);
		}
	}
	break;

	case TI_IR_ImgT_Fn_Displace:
	{
		for (int nNum = 0; nNum < enDisplace_Result_Max; nNum++)
		{
			m_Wnd_Rst_Displace.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stDisplace.bResult[nNum], ((ST_IR_Result *)pParam)->stDisplace.dbValue[nNum + 4]);
		}
	}
	break;

	case TI_IR_ImgT_Fn_OpticalCenter:
	{
		for (int nNum = 0; nNum < enOpticalCenter_Result_Max; nNum++)
		{
			m_Wnd_Rst_OpticalCenter.Result_Display(0, nNum, ((ST_IR_Result *)pParam)->stOpticalCenter.bResult[nNum], ((ST_IR_Result *)pParam)->stOpticalCenter.dValue[nNum]);
		}
	}
	break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SetClearTab
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 19:08
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::SetClearTab()
{
	for (int iIndx = 0; iIndx < TI_IR_ImgT_MaxEnum; iIndx++)
	{
		m_iSelectTest[iIndx] = -1;
	}

	if (m_cb_TestItem.GetCount() > 0)
	{
		m_cb_TestItem.ResetContent();
	}
}

//=============================================================================
// Method		: SetAddTab
// Access		: public  
// Returns		: void
// Parameter	: int nIdx
// Parameter	: int & iItemCnt
// Qualifier	:
// Last Update	: 2018/5/12 - 19:09
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::SetAddTab(int nIdx, int &iItemCnt)
{
	switch (nIdx)
	{
	case TI_IR_ImgT_Fn_SFR:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("SFR"));
	}
		break;

	case TI_IR_ImgT_Fn_Ymean:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Ymean"));
	}
		break;

	case TI_IR_ImgT_Fn_LCB:
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("LCB"));
		break;

	case TI_IR_ImgT_Fn_BlackSpot:
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("BlackSpot"));
		break;

	case TI_IR_ImgT_Fn_Distortion:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Distortion"));
	}
		break;

	case TI_IR_ImgT_Fn_Current:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Current"));
	}
		break;

	case TI_IR_ImgT_Fn_Vision:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Vision"));
	}
	break;

	case TI_IR_ImgT_Fn_Displace:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Displace"));
	}
	break;

	case TI_IR_ImgT_Fn_Shading:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Shading"));
	}
	break;

	case TI_IR_ImgT_Fn_DefectBlack:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Defect_Black"));
	}
		break;
	case TI_IR_ImgT_Fn_DefectWhite:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Defect_White"));
	}
		break;

	case TI_IR_ImgT_Fn_OpticalCenter:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("Optical_Center"));
	}
	break;

	case TI_IR_ImgT_Fn_IIC:
	{
		m_iSelectTest[iItemCnt++] = nIdx; //들어온 순서대로 ID Input;
		m_cb_TestItem.AddString(_T("IIC"));
	}
	break;

	default:
		break;
	}
}

//=============================================================================
// Method		: OnLbnSelChangeTest
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2018/5/12 - 19:10
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::OnLbnSelChangeTest()
{
	m_Wnd_Rst_SFR.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Shading.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Ymean.ShowWindow(SW_HIDE);
	m_Wnd_Rst_LCB.ShowWindow(SW_HIDE);
	m_Wnd_Rst_BlackSpot.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Vision.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Displace.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Current.ShowWindow(SW_HIDE);
	m_Wnd_Rst_IIC.ShowWindow(SW_HIDE);
	m_Wnd_Rst_Distortion.ShowWindow(SW_HIDE);
	m_Wnd_Rst_DefectBlack.ShowWindow(SW_HIDE);
	m_Wnd_Rst_DefectWhite.ShowWindow(SW_HIDE);

	int iIdex = m_cb_TestItem.GetCurSel();

	switch (m_iSelectTest[iIdex])
	{
	case TI_IR_ImgT_Fn_SFR:
	{
		m_Wnd_Rst_SFR.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_Ymean:
	{
		m_Wnd_Rst_Ymean.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_LCB:
		m_Wnd_Rst_LCB.ShowWindow(SW_SHOW);
		break;

	case TI_IR_ImgT_Fn_BlackSpot:
		m_Wnd_Rst_BlackSpot.ShowWindow(SW_SHOW);
		break;

	case TI_IR_ImgT_Fn_Distortion:
	{
		m_Wnd_Rst_Distortion.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_Shading:
	{
		m_Wnd_Rst_Shading.ShowWindow(SW_SHOW);
	}
	break;

	case TI_IR_ImgT_Fn_Current:
	{
		m_Wnd_Rst_Current.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_IIC:
	{
		m_Wnd_Rst_IIC.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_Vision:
	{
		m_Wnd_Rst_Vision.ShowWindow(SW_SHOW);
	}
	break;

	case TI_IR_ImgT_Fn_Displace:
	{
		m_Wnd_Rst_Displace.ShowWindow(SW_SHOW);
	}
	break;

	case TI_IR_ImgT_Fn_DefectBlack:
	{
		m_Wnd_Rst_DefectBlack.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_DefectWhite:
	{
		m_Wnd_Rst_DefectWhite.ShowWindow(SW_SHOW);
	}
		break;

	case TI_IR_ImgT_Fn_OpticalCenter:
	{
		m_Wnd_Rst_OpticalCenter.ShowWindow(SW_SHOW);
	}
	break;

	default:
		break;
	}
}

//=============================================================================
// Method		: SelectNum
// Access		: public  
// Returns		: void
// Parameter	: UINT nSelect
// Qualifier	:
// Last Update	: 2018/5/12 - 19:10 
// Desc.		:
//=============================================================================
void CWnd_TestResult_IR_Img_MainView::SelectNum(UINT nSelect)
{
	m_cb_TestItem.SetCurSel(nSelect);
	OnLbnSelChangeTest();
}