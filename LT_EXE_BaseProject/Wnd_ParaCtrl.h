#pragma once

// CWnd_ParaCtrl
#ifndef Wnd_ParaCtrl_h__
#define Wnd_ParaCtrl_h__

#include "Def_TestFunc.h"
#include "VGGroupWnd.h"
#include "VGStatic.h"

class CWnd_ParaCtrl : public CVGGroupWnd
{
	DECLARE_DYNAMIC(CWnd_ParaCtrl)

public:
	CWnd_ParaCtrl();
	virtual ~CWnd_ParaCtrl();

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);
			void	OnRangeCmds(__in UINT nID);

			void	SetParaIdx(__in UINT nParaIdx)
			{
				m_nParaIdx = nParaIdx;
			}

	CFont			m_font;

	// Manual Button
	CMFCButton		m_btnFuncCtrl[ManuP_Max_Num];

	// Status â
	CVGStatic		m_stStatusName[ManuP_Max_Num];
	
protected:
	DECLARE_MESSAGE_MAP()

	UINT		m_nParaIdx;
};

#endif // Wnd_ParaCtrl_h__