//*****************************************************************************
// Filename	: Wnd_MotorView.h
// Created	: 2017/04/03
// Modified	: 2017/04/03
//
// Author	: KHO
//	
// Purpose	: 기본 화면용 윈도우
//*****************************************************************************
#ifndef Wnd_MotorView_h__
#define Wnd_MotorView_h__

#pragma once

#include "Wnd_BaseView.h"
#include "MotionManager.h"
#include "VGStatic.h"
#include "Wnd_OriginOp.h"
#include "Wnd_MotionOp.h"
#include "Wnd_TeachOp.h"
#include "Wnd_MotionTable.h"
#include "Wnd_MotionCtr.h"
#include "File_WatchList.h"

//=============================================================================
// CWnd_MotorView
//=============================================================================
class CWnd_MotorView : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_MotorView)

public:
	CWnd_MotorView();
	virtual ~CWnd_MotorView();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize(UINT nType, int cx, int cy);
	afx_msg void	OnBnClickedBnNew();
	afx_msg void	OnCbnSelendokFile();

	afx_msg LRESULT	OnMotorUpdata(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnMotorSelect(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT	OnMotorOrigin(WPARAM wParam, LPARAM lParam);

	virtual BOOL	PreCreateWindow(CREATESTRUCT& cs);

	CMFCTabCtrl			m_tc_Option;
	CMFCTabCtrl			m_tc_Item;

	CMotionManager*		m_pstDevice;

	CWnd_MotionTable	m_Wnd_MotionTable;
	CWnd_OriginOp	 	m_Wnd_OriginOp;
	CWnd_MotionOp		m_Wnd_MotionOp;
	CWnd_TeachOp		m_Wnd_TeachOp;
	CWnd_MotionCtr		m_Wnd_MotionCtr;

	CString				m_szMotorFile;
	CString				m_szMotorPath;
	CFont				m_font;

	CVGStatic			m_st_File;
	CComboBox			m_cb_File;
	CButton				m_bn_NewFile;

	CFile_WatchList		m_IniWatch;

	// 검사기 설정
	enInsptrSysType		m_InspectionType = enInsptrSysType::Sys_Focusing;
	CVGStatic			m_st_Filename;

public:

	void	SetPtr_MaintenanceInfo(__in ST_MaintenanceInfo*	pstMaintInfo)
	{
		if (pstMaintInfo == NULL)
			return;

		m_Wnd_TeachOp.SetPtr_MaintenanceInfo(pstMaintInfo);
	}

	void	GetPtr_MaintenanceInfo(__out ST_MaintenanceInfo&	stMaintInfo)
	{
		m_Wnd_TeachOp.GetPtr_MaintenanceInfo(stMaintInfo);
	}

	void	SetPtr_Device		(__in CMotionManager* pstDevice)
	{
		if (pstDevice == NULL)
			return;

		m_pstDevice = pstDevice;

		m_Wnd_TeachOp.SetPtr_Device(pstDevice);
		m_Wnd_OriginOp.SetPtr_Device(pstDevice);
		m_Wnd_MotionOp.SetPtr_Device(pstDevice);
		m_Wnd_MotionCtr.SetPtr_Device(pstDevice);
		m_Wnd_MotionTable.SetPtr_Device(pstDevice);
	};

	// 검사기 종류 설정
	void	SetSystemType		(__in enInsptrSysType nSysType);

	void	SetPath				(__in LPCTSTR szMotorPath)
	{
		if (NULL != szMotorPath)
			m_szMotorPath = szMotorPath;
	};

	void	SetDeleteTimer		();

	void	SetOriginEnable		();

	void	SetPermissionMode	(enPermissionMode InspMode);

	void	RefreshFileList		(__in const CStringList* pFileList);

	void	UpdateMotorInfo		(__in CString szMotorFile);

};

#endif // Wnd_MotorView
