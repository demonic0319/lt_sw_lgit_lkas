//*****************************************************************************
// Filename	: 	Wnd_Cfg_TestItem_EachTest.h
// Created	:	2017/9/24 - 16:11
// Modified	:	2017/9/24 - 16:11
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_Cfg_TestItem_EachTest_h__
#define Wnd_Cfg_TestItem_EachTest_h__

#pragma once


#include "Wnd_BaseView.h"
#include "VGStatic.h"
#include "Def_TestItem_Cm.h"
#include "Def_DataStruct.h"

#include "List_EachTestStep.h"




//-----------------------------------------------------------------------------
// CWnd_Cfg_TestItem_EachTest
//-----------------------------------------------------------------------------
class CWnd_Cfg_TestItem_EachTest : public CWnd_BaseView
{
	DECLARE_DYNAMIC(CWnd_Cfg_TestItem_EachTest)

public:
	CWnd_Cfg_TestItem_EachTest();
	virtual ~CWnd_Cfg_TestItem_EachTest();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg int		OnCreate			(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize				(UINT nType, int cx, int cy);
	afx_msg void	OnShowWindow		(BOOL bShow, UINT nStatus);
	afx_msg void	OnNMClickTestItem	(NMHDR *pNMHDR, LRESULT *pResult);

	// 검사기 설정
	enInsptrSysType			m_InspectionType = enInsptrSysType::Sys_Focusing;

	CMFCTabCtrl				m_tc_Option;
	
	CList_EachTestStep		m_List_EachTestStep;

	CVGStatic				m_st_ManualTest;
	CVGStatic				m_st_Result;

	CMFCButton				m_bn_Start;

	void		SetSysAddTabCreate		();

public:
	// 검사기 종류 설정
	void		SetSystemType			(__in enInsptrSysType nSysType);

	// 저장된 Test Item Info 데이터 불러오기
	void		Set_RecipeInfo			(__in  ST_RecipeInfo* pstRecipeInfo);


	afx_msg void	OnBnClickedStart();

	UINT *m_pnCamParaIdx;

	void	SetCameraParaIdx	(__in UINT *pstCamParaIdx)
	{
		if (NULL == pstCamParaIdx)
			return;

		m_pnCamParaIdx = pstCamParaIdx;
	};


};
#endif // Wnd_Cfg_TestItem_h__


