﻿#ifndef Def_Mes_Foc_h__
#define Def_Mes_Foc_h__

#include <afxwin.h>


#pragma pack(push,1)



static LPCTSTR g_szMESHeader_Foc[] =
{
	_T("Current_1.8V"),
	_T("Current_2.8V"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("OpticalCenter_X"),
	_T("OpticalCenter_Y"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("SFR_MIN_0"),
	_T("SFR_MIN_1"),
	_T("SFR_MIN_2"),
	_T("SFR_MIN_3"),
	_T("SFR_MIN_4"),
	_T("SFR_1"),
	_T("SFR_2"),
	_T("SFR_3"),
	_T("SFR_4"),
	_T("SFR_5"),
	_T("SFR_6"),
	_T("SFR_7"),
	_T("SFR_8"),
	_T("SFR_9"),
	_T("SFR_10"),
	_T("SFR_11"),
	_T("SFR_12"),
	_T("SFR_13"),
	_T("SFR_14"),
	_T("SFR_15"),
	_T("SFR_16"),
	_T("SFR_17"),
	_T("SFR_18"),
	_T("SFR_19"),
	_T("SFR_20"),
	_T("SFR_21"),
	_T("SFR_22"),
	_T("SFR_23"),
	_T("SFR_24"),
	_T("SFR_25"),
	_T("SFR_26"),
	_T("SFR_27"),
	_T("SFR_28"),
	_T("SFR_29"),
	_T("SFR_30"),
	_T("Rotation"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Stain_#1 농도"),
	_T("Stain_#1 개수"),
	_T("Stain_#2 농도"),
	_T("Stain_#2 개수"),
	_T("Stain_#3 농도"),
	_T("Stain_#3 개수"),
	_T("Stain_#4 농도"),
	_T("Stain_#4 개수"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("DefectPixel_Very Hot Pixel"),
	_T("DefectPixel_Hot Pixel"),
	_T("DefectPixel_Very Bright Pixel"),
	_T("DefectPixel_Bright Pixel"),
	_T("DefectPixel_Very Dark Pixel"),
	_T("DefectPixel_Dark Pixel"),
	_T("DefectPixel_Cluster"),
	_T("DefectPixel_Dark Row"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Reserved"),
	_T("Torque_Value_A"),
	_T("Torque_Value_B"),
	_T("Torque_Value_C"),
	_T("Torque_Value_D"),
	_T("Torque_Count_A"),
	_T("Torque_Count_B"),
	_T("Torque_Count_C"),
	_T("Torque_Count_D"),
	NULL,
};

//항목 순서
typedef enum enMesDataIndex_Foc
{
	MesDataIdx_Foc_ECurrent = 0,
	MesDataIdx_Foc_OpticalCenter,
	MesDataIdx_Foc_SFR,
	MesDataIdx_Foc_Rotation,
	MesDataIdx_Foc_Stain,
	MesDataIdx_Foc_DefectPixel,
	MesDataIdx_Foc_Torque,
	MesDataIdx_Foc_MAX,
};

//각 항목에 대한 Data 갯수
typedef enum enMesData_DataNUM_Foc 
{
	MESDataNum_Foc_ECurrent			= 5,
	MESDataNum_Foc_OpticalCenter	= 5,
	MESDataNum_Foc_SFR				= 35,
	MESDataNum_Foc_Rotation			= 3,
	MESDataNum_Foc_Stain			= 11,
	MESDataNum_Foc_DefectPixel		= 11,
	MESDataNum_Foc_Torque			= 8,

} MesData_DataNUM_Foc;


typedef struct _tag_MesData_Foc
{
	CString szMesTestData[2][MesDataIdx_Foc_MAX];
	CString szDataName[MesDataIdx_Foc_MAX];

	UINT nMesDataNum[MesDataIdx_Foc_MAX];

	_tag_MesData_Foc()
	{
		Reset(0);
		Reset(1);

		nMesDataNum[MesDataIdx_Foc_ECurrent		] = MESDataNum_Foc_ECurrent;
		nMesDataNum[MesDataIdx_Foc_OpticalCenter] = MESDataNum_Foc_OpticalCenter;
		nMesDataNum[MesDataIdx_Foc_Rotation		] = MESDataNum_Foc_Rotation;
		nMesDataNum[MesDataIdx_Foc_SFR			] = MESDataNum_Foc_SFR;
		nMesDataNum[MesDataIdx_Foc_Stain		] = MESDataNum_Foc_Stain;
		nMesDataNum[MesDataIdx_Foc_DefectPixel	] = MESDataNum_Foc_DefectPixel;
		nMesDataNum[MesDataIdx_Foc_Torque		] = MESDataNum_Foc_Torque;

		szDataName[MesDataIdx_Foc_ECurrent		] = _T("Current");
		szDataName[MesDataIdx_Foc_OpticalCenter	] = _T("Optical Center");
		szDataName[MesDataIdx_Foc_Rotation		] = _T("Rotate");
		szDataName[MesDataIdx_Foc_SFR			] = _T("SFR");
		szDataName[MesDataIdx_Foc_Stain			] = _T("Statin");
		szDataName[MesDataIdx_Foc_DefectPixel	] = _T("DefectPixel");
		szDataName[MesDataIdx_Foc_Torque		] = _T("Torque");
	};

	void Reset(UINT nCam)
	{
		for (UINT nItem = 0; nItem < MesDataIdx_Foc_MAX; nItem++)
		{
			szMesTestData[nCam][nItem].Empty();
		}
	};

	_tag_MesData_Foc& operator= (_tag_MesData_Foc& ref)
	{
		for (UINT nCam = 0; nCam < 2; nCam++)
		{
			for (UINT nItem = 0; nItem < MesDataIdx_Foc_MAX; nItem++)
			{
				szMesTestData[nCam][nItem] = ref.szMesTestData[nCam][nItem];
			}
		}
		
		return *this;
	};

}ST_MES_Data_Foc, *PST_MES_Data_Foc;


#pragma pack(pop)

#endif // Def_FOV_h__