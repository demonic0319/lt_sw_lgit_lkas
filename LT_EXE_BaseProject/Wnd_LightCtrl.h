﻿//*****************************************************************************
// Filename	: 	Wnd_LightCtrl.h
// Created	:	2016/10/31 - 20:17
// Modified	:	2016/10/31 - 20:17
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#ifndef Wnd_LightCtrl_h__
#define Wnd_LightCtrl_h__

#pragma once

#include "VGStatic.h"
#include "Wnd_BaseView.h"
#include "Def_DataStruct.h"
#include "Def_TestDevice.h"
#include "File_WatchList.h"

// 광원 제어 종류
typedef enum enLightBrdType
{
	//LBT_ODA_PT,			// ODA PT 시리즈
	LBT_Luritech_CH01,	// 루리텍 광원 보드
	LBT_Luritech_CH02,
	LBT_Luritech_CH03,
	LBT_Luritech_ALL,
};

typedef enum enLightItem
{
	LightItm_Volt,
	LightItm_Step,
	LightItm_MaxEnum,
};

//=============================================================================
// CWnd_LightCtrl
//=============================================================================
class CWnd_LightCtrl : public CWnd
{
	DECLARE_DYNAMIC(CWnd_LightCtrl)

public:
	CWnd_LightCtrl();
	virtual ~CWnd_LightCtrl();

protected:
	DECLARE_MESSAGE_MAP()

	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnHScroll				(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void	OnBnClickedVoltApply	();
	afx_msg void	OnBnClickedStepApply	();
	afx_msg void	OnBnClickedBnLightOn	();
	afx_msg void	OnBnClickedBnLightOff	();
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);
	virtual BOOL	PreTranslateMessage		(MSG* pMsg);

	
	CFont			m_font;

	CVGStatic		m_st_Title;
	CVGStatic		m_st_Frame;

	CVGStatic		m_st_Volt;
	CMFCMaskedEdit	m_ed_Volt;
	CMFCButton		m_bn_Volt_Apply;
	CSliderCtrl		m_sc_Volt;

	CVGStatic		m_st_Step;
	CMFCMaskedEdit	m_ed_Step;
	CMFCButton		m_bn_Step_Apply;
	CSliderCtrl		m_sc_Step;

	CMFCButton		m_bn_LightOn;
	CMFCButton		m_bn_LightOff;

	enLightBrdType	m_nLightBoardType		= LBT_Luritech_CH01;
	PVOID			m_pLightDevice			= NULL;

	ST_LightCfg		m_stLightConfig;
	CString			m_szTitle;

public:

	virtual void	SetPtr_Device			(__in enLightBrdType nCtrlType, __in PVOID pLightDevice);

	void			SetLightInfo			(__in const ST_LightCfg* pstLightInfo);
	void			GetLightInfo			(__out ST_LightCfg& stLightInfo);	

	virtual void	Set_VoltageMinMax		(__in float fVoltMin, __in float fVoltMax);
	virtual void	Set_CurrentMinMax		(__in float fCurrMin, __in float fCurrMax);
	virtual void	Set_StepMinMax			(__in WORD wStepMin, __in WORD wStepMax);
	
	void			SetTitle				(__in LPCTSTR szText);
	
};

#endif // Wnd_LightCtrl_h__
