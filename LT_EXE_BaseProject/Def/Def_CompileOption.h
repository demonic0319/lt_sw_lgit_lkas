﻿//*****************************************************************************
// Filename	: Def_CompileOption.h
// Created	: 2012/10/30
// Modified	: 2016/08/11
//
// Author	: PiRing
//	
// Purpose	: LGE : 5종 검사 장비 빌드 옵션
//*****************************************************************************
#ifndef Def_CompileOption_h__
#define Def_CompileOption_h__

#include "Def_Enum_Cm.h"
//=============================================================================
// 라인 구분 
//=============================================================================

//=============================================================================
// 언어 설정
//=============================================================================
//#define	LANG_KR	//한국어
//#define	LANG_EN	//영어
//#define	LANG_CN	//중국어

//=============================================================================
// 검사기 종류
//=============================================================================
// #define SYS_2D_CAL			0	// 2D Calibration
// #define SYS_3D_CAL			1	// 3D Calibration
// #define SYS_FOCUSING			2	// Focusing
// #define SYS_IMAGE_TEST		3	// Front Image Test (Front Set)
// #define SYS_IR_IMAGE_TEST	4	// 반사판 Image Test (광원 제어 없음)

//=============================================================================
// 검사기 선택
//=============================================================================

//=============================================================================
// 검사기 선택
//=============================================================================
#ifdef SET_INSPECTOR
#undef SET_INSPECTOR
#endif

// "/DLT_EQP#0" ~ "/DLT_EQP#4" : Project Properties (Configuration Peroperties (속성) -> C/C++ -> Command Line (명령줄) -> Additional Options (추가옵션))
#ifdef LT_EQP	// 프로젝트 설정에서 /D 커맨드라인 사용 할 경우
	#define		SET_INSPECTOR		LT_EQP
#else			// 수동 설정
#define		SET_INSPECTOR		SYS_IR_IMAGE_TEST
#endif

// Ex) 위 LT_EQP 설정을 풀어서 사용한 예
// #if (LT_EQP == 0)
// 	#define		SET_INSPECTOR		SYS_2D_CAL
// #elif (LT_EQP == 1)
// 	#define		SET_INSPECTOR		SYS_3D_CAL
// #elif (LT_EQP == 2)
// 	#define		SET_INSPECTOR		SYS_FOCUSING
// #elif (LT_EQP == 3)
// 	#define		SET_INSPECTOR		SYS_IMAGE_TEST
// #elif (LT_EQP == 3)
// 	#define		SET_INSPECTOR		SYS_IR_IMAGE_TEST
// #else // 수동 설정
// 	#define		SET_INSPECTOR		SYS_2D_CAL
// #endif

//=============================================================================
// 프로그램 환경설정 레지스트리 주소
//=============================================================================
#define REG_PATH_BASE			_T("Software\\Luritech")
//#define REG_PATH_OPTION_BASE	_T("Software\\Luritech\\Option")

//=============================================================================
// 기본 사용 장치 선택
//=============================================================================
//#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\Environment")
//#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\Environment\\Option")

#if (SET_INSPECTOR == SYS_2D_CAL)
	#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\LGIT\\EQP_2DCal\\Env")
	#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\LGIT\\EQP_2DCal\\Env\\Option")
#elif (SET_INSPECTOR == SYS_3D_CAL)
	#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\LGIT\\EQP_3DCal\\Env")
	#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\LGIT\\EQP_3DCal\\Env\\Option")
#elif (SET_INSPECTOR == SYS_FOCUSING)
	#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\LGIT\\EQP_Focusing\\Env")
	#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\LGIT\\EQP_Focusing\\Env\\Option")
#elif (SET_INSPECTOR == SYS_IMAGE_TEST)
	#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\LGIT\\EQP_ImageTest\\Env")
	#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\LGIT\\EQP_ImageTest\\Env\\Option")
#elif (SET_INSPECTOR == SYS_IR_IMAGE_TEST)
	#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\Environment")
	#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\Environment\\Option")
#else
	#define		REG_PATH_APP_BASE		_T("Software\\Luritech\\LGIT\\Env")
	#define		REG_PATH_OPTION_BASE	_T("Software\\Luritech\\LGIT\\Env\\Option")
#endif


//=============================================================================
// 프로그램 테스트 모드로 빌드시 장치 사용 여부 
//=============================================================================
//#define		USE_HW_LOCK_KEY					// 하드웨어 락키 사용여부 (기본 : 사용)
#define			USE_BARCODE_SCANNER

//#define		NO_CHECK_ELAP_TIME			// 검사 진행 시간 체크 여부 (기본 : 미사용)
//#define		NO_CHECK_MOTION_SENSOR		// 모션 센서 모니터링 사용 여부 (기본 : 미사용)

//#define		USE_TEST_MODE				// 테스트 모드
//#define		DEVELOPMENT_MODE			// 개발자 모드

//#define		MOTION_NOT_USE				// 모터 제어 미사용 (기본 : 미사용)

//#define		USE_TEST_BUTTON				// 테스트 버튼 사용 (기본 : 미사용)
//#define		NOT_USE_STEREO_ALGORITHM	// Stereo CAL 루리텍 알고리즘 사용여부 (기본 : 미사용)

#endif // Def_CompileOption_h__

 