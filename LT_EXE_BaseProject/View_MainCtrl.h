﻿//*****************************************************************************
// Filename	: View_MainCtrl.h
// Created	: 2010/11/26
// Modified	: 2016/06/07
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef View_MainCtrl_h__
#define View_MainCtrl_h__

#pragma once

// UI
#include "Wnd_MainView.h"
//#include "Wnd_ManualView.h"
#include "Wnd_IO_View.h"
#include "Wnd_Maintenance_View.h"
#include "Wnd_RecipeView.h"
//#include "Wnd_AlarmView.h"
#include "Wnd_LogView.h"
#include "Wnd_Origin.h"

#include "Overlay_Proc.h"
#include "SplashScreenEx.h"
#include "Dlg_Barcode.h"

#include "DisplayPage_CanComm.h"

typedef enum _enumSubView
{
	SUBVIEW_AUTO	= 0,	
	//SUBVIEW_MANUAL,
	SUBVIEW_IO,
	SUBVIEW_MAINTENANCE,
	SUBVIEW_RECIPE,
	//SUBVIEW_ALARM,
	SUBVIEW_LOG,
	SUBVIEW_MAX,
}enumSubView;


//=============================================================================
// CView_MainCtrl 창
//=============================================================================
class CView_MainCtrl : public CWnd
{
// 생성입니다.
public:
	CView_MainCtrl();
	virtual ~CView_MainCtrl();

protected:

	afx_msg int		OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void	OnSize					(UINT nType, int cx, int cy);
	afx_msg void	OnPaint					();
	afx_msg BOOL	OnEraseBkgnd			(CDC* pDC);
	afx_msg HBRUSH	OnCtlColor				(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void	OnTimer					(UINT_PTR nIDEvent);

	DECLARE_MESSAGE_MAP()
	
	virtual BOOL	PreCreateWindow			(CREATESTRUCT& cs);

	// 배경색 처리용
	CBrush				m_brBkgr;

	//-----------------------------------------------------
	// 차일드 윈도우 구분용
	//-----------------------------------------------------	
	UINT					m_nWndIndex;
	CWnd*					m_pWndPtr[SUBVIEW_MAX];

	CWnd_MainView			m_wnd_MainView;
	//CWnd_ManualView		m_wnd_ManualView;
	CWnd_IO_View			m_wnd_IOView;
	CWnd_Maintenance_View	m_wnd_MaintenanceView;
	CWnd_RecipeView			m_wnd_RecipeView;
	//CWnd_AlarmView		m_wnd_AlarmView;
	CWnd_LogView			m_wnd_LogView;

	// 통신 상태 표시 Pane의 포인터
	CWnd*					m_pwndCommPane;
	// 바코드 입력 다이얼로그
	CDlg_Barcode*			m_pdlgBarcode;

	COverlay_Proc			m_OverlayProc;

	HANDLE					m_hThr_MotorMonitoring	= NULL;
	//-----------------------------------------------------
	// 로그/파일 처리
	//-----------------------------------------------------
	virtual void	OnInitLogFolder					();
		
	//-----------------------------------------------------
	// 초기 설정 관련
	//-----------------------------------------------------
	// 생성자에서 초기화 할 세팅
	virtual void	InitConstructionSetting			();
	// Window 생성 후 세팅
	virtual void	InitUISetting					();
	// 주변장치들 기본 설정
	virtual void	InitDeviceSetting				();
	
	//-----------------------------------------------------
	// 영상 작업 처리
	//-----------------------------------------------------
	virtual void	DisplayVideo					(__in UINT nChIdx, __in LPBYTE lpbyRGB, __in DWORD dwRGBSize, __in UINT nWidth, __in UINT nHeight);
	virtual void	DisplayVideo_LastImage			(__in UINT nChIdx);
	virtual void	DisplayVideo_NoSignal			(__in UINT nChIdx);
	virtual void	DisplayVideo_Overlay			(__in UINT nChIdx, __in enOverlayItem enItem, __inout IplImage *TestImage);

	//-------------------------------------------------------------------------
	// 모델 파일 불러오기 및 세팅
	virtual inline BOOL		LoadRecipeInfo			(__in LPCTSTR szRecipe, __in BOOL bNotifyModelWnd = TRUE);
	virtual void			InitLoadRecipeInfo		();

	// 모터 파일 불러오기 및 세팅
	virtual inline BOOL		LoadMotorInfo			(__in LPCTSTR szMotir, __in BOOL bNotifyModelWnd = TRUE);
	virtual void			InitLoadMotorInfo		();

	// 유지 관리 파일 불러오기 및 세팅
	virtual inline BOOL		LoadMaintenanceInfo		(__in LPCTSTR szMaintenance, __in BOOL bNotifyModelWnd = TRUE);
	virtual void			InitLoadMaintenanceInfo	();
	
	// 수동으로 보드 제어
	virtual void			Manual_DeviceControl	(__in UINT nChIdx, __in UINT nBnIdx);	

	// 메인 UI Para 변경
	virtual void			OnSetCamerParaSelect	(__in UINT nParaIdx = 0);

//=============================================================================
public: 
//=============================================================================
	
	// 윈도우 배경색 설정용 함수
	void			SetBackgroundColor			(__in COLORREF color, __in BOOL bRepaint = TRUE);

	// 차일드 윈도우 전환 시 사용
	virtual UINT	SwitchWindow				(__in UINT nIndex);
	// 장치 통신 상태 표시 윈도우 포인터 설정
	virtual void	SetCommPanePtr				(__in CWnd* pwndCommPane);
	
	// 옵션이 변경 되었을 경우 다시 UI나 데이터를 세팅하기 위한 함수
	virtual void	ReloadOption				();
	// 프로그램 로딩 끝난 후 자동 처리를 위한 함수
	virtual void	InitStartProgress			();	
	virtual BOOL	InitStartDeviceProgress		();	

	// 프로그램 종료시 처리해야 할 기능들을 처리하는 함수
	virtual void	FinalExitProgress			();
	
	// 수동 바코드 입력
	virtual void	ManualBarcode				();

	// 모터 원점
	virtual BOOL	MotorOrigin					();

	// MES Online Mode
	virtual void	ChangeMESOnlineMode			(__in enMES_Online nOnlineMode);

	virtual void	OnManual_OneItemTest		(__in UINT nStepIdx, __in UINT nParaIdx);

	// EEPROM 제어 (임시)
	virtual void	OnEEPROM_Ctrl				(__in UINT nCondition){};

	//--------------------- TEST --------------------------
	virtual void	Test_Process				(__in UINT nTestNo);
	//--------------------- TEST --------------------------


	// *** 임시 테스트용 코드
	void			Test_SNR();


	
};

#endif // View_MainCtrl_h__


