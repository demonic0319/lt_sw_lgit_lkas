// Wnd_MT_Particle.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "Wnd_MT_Particle.h"
#include "resource.h"

// CWnd_MT_Particle

#define		IDC_BTN_SENDFUNC			3000
#define		IDC_EDIT_SEND				4000
#define		IDC_EDIT_RECV				5000
#define		IDC_EDIT_VALUE				6000

#define		IDC_BTN_PARA				7000

// CWnd_MT_PCBComm

static LPCTSTR g_szParaPort[] =
{
	_T("LEFT"),
	_T("RIGHT"),
	NULL
};

static LPCTSTR g_szTiltName[] =
{
	_T("ITEM"),
	_T("TIME"),
	_T("RESULT"),
	_T("TEST"),
	NULL
};

IMPLEMENT_DYNAMIC(CWnd_MT_Particle, CWnd)

CWnd_MT_Particle::CWnd_MT_Particle()
{
	m_nPort = Para_Left;
	VERIFY(m_font.CreateFont(
		15,						// nHeight
		0,						// nWidth
		0,						// nEscapement
		0,						// nOrientation
		FW_SEMIBOLD,			// nWeight
		FALSE,					// bItalic
		FALSE,					// bUnderline
		0,						// cStrikeOut
		ANSI_CHARSET,			// nCharSet
		OUT_DEFAULT_PRECIS,		// nOutPrecision
		CLIP_DEFAULT_PRECIS,	// nClipPrecision
		ANTIALIASED_QUALITY,	// nQuality
		DEFAULT_PITCH,			// nPitchAndFamily
		_T("Arial")));			// lpszFacename
}

CWnd_MT_Particle::~CWnd_MT_Particle()
{
	m_font.DeleteObject();
}

BEGIN_MESSAGE_MAP(CWnd_MT_Particle, CWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_COMMAND_RANGE(IDC_BTN_SENDFUNC, IDC_BTN_SENDFUNC + MTCtrl_Particle_MaxNum, OnRangeCmds)
	ON_COMMAND_RANGE(IDC_BTN_PARA, IDC_BTN_PARA + Para_MaxEnum, OnRangePort)
END_MESSAGE_MAP()

// CWnd_MT_Particle 메시지 처리기입니다.
int CWnd_MT_Particle::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	DWORD dwStyle = WS_VISIBLE | WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	CRect rectDummy;
	rectDummy.SetRectEmpty();

	m_st_Name.SetStaticStyle(CVGStatic::StaticStyle_Default);
	m_st_Name.SetBackColor_COLORREF(RGB(33, 73, 125));
	m_st_Name.SetTextColor(Gdiplus::Color::White, Gdiplus::Color::White);
	m_st_Name.SetFont_Gdip(L"Arial", 12.0F);
	m_st_Name.Create(_T("COMMUNICATION MANUL CONTROL"), dwStyle | SS_CENTER | SS_LEFTNOWORDWRAP, rectDummy, this, IDC_STATIC);

	for (UINT nIdx = 0; nIdx < Title_CANCommPg4_MaxNum; nIdx++)
	{
		m_stTitle[nIdx].Create(g_szTiltName[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_stTitle[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stTitle[nIdx].SetColorStyle(CVGStatic::ColorStyle_DarkGray);
		m_stTitle[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
		m_stTitle[nIdx].SetFont(&m_font);
	}

	for (UINT nIdx = 0; nIdx < MTCtrl_Particle_MaxNum; nIdx++)
	{
		m_stTestName[nIdx].Create(g_szMTCtrl_Particle[nIdx], dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_stTestName[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Default);
		m_stTestName[nIdx].SetColorStyle(CVGStatic::ColorStyle_Default);
		m_stTestName[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
		m_stTestName[nIdx].SetFont(&m_font);

		m_stSendProtol[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_stSendProtol[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Data);
		m_stSendProtol[nIdx].SetColorStyle(CVGStatic::ColorStyle_White);
		m_stSendProtol[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
		m_stSendProtol[nIdx].SetFont(&m_font);

		m_stRecvProtol[nIdx].Create(_T(""), dwStyle | SS_CENTER | SS_CENTERIMAGE, rectDummy, this, IDC_STATIC);
		m_stRecvProtol[nIdx].SetStaticStyle(CVGStatic::StaticStyle_Data);
		m_stRecvProtol[nIdx].SetColorStyle(CVGStatic::ColorStyle_White);
		m_stRecvProtol[nIdx].SetFont_Gdip(L"Arial", 9.0F, Gdiplus::FontStyleRegular);
		m_stRecvProtol[nIdx].SetFont(&m_font);

		m_btTestFunc[nIdx].Create(_T("SEND"), dwStyle, rectDummy, this, IDC_BTN_SENDFUNC + nIdx);

		m_ed_Index[nIdx].Create(dwStyle | WS_BORDER | ES_CENTER, rectDummy, this, IDC_EDIT_VALUE + nIdx);
		m_ed_Index[nIdx].SetFont(&m_font);

		m_ed_Index[nIdx].SetWindowTextW(_T("0"));
		m_ed_Index[nIdx].SetValidChars(_T("012"));
	}

	for (UINT nIdex = 0; nIdex < Para_MaxEnum; nIdex++)
	{
		m_bn_Item[nIdex].Create(g_szParaPort[nIdex], dwStyle | BS_AUTOCHECKBOX, rectDummy, this, IDC_BTN_PARA + nIdex);
		m_bn_Item[nIdex].SetMouseCursorHand();
		m_bn_Item[nIdex].SetImage(IDB_UNCHECKED_16);
		m_bn_Item[nIdex].SetCheckedImage(IDB_CHECKED_16);
		m_bn_Item[nIdex].SizeToContent();
		m_bn_Item[nIdex].SetFont(&m_font);
	}

	m_bn_Item[Para_Left].SetCheck(1);

	

	return 0;
}

//=============================================================================
// Method		: OnSize
// Access		: public  
// Returns		: void
// Parameter	: UINT nType
// Parameter	: int cx
// Parameter	: int cy
// Qualifier	:
// Last Update	: 2017/11/12 - 23:18
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::OnSize(UINT nType, int cx, int cy)
{
	CWnd::OnSize(nType, cx, cy);

	if ((0 == cx) || (0 == cy))
		return;

	int iTitleH = 35;
	int iLeftMargin = 0;
	int iRightMargin = 0;
	int iTopMargin = 0;
	int iBottomMargin = 7;

	int iSpacing = 5;

	int iLeft = iLeftMargin;
	int iTop = iTopMargin;

	int iBottom = cy - iBottomMargin;
	int iWidth = cx - iLeftMargin - iRightMargin;
	int iHeight = cy - iTopMargin - iBottomMargin;

	int iNameWidth = iWidth / (Title_CANCommPg4_MaxNum + 2);

	int iBtnHeight = 26;
	int iBtnWidth = (iNameWidth - 1) / 2;

	int iCtrlHeight = (iHeight - iTitleH - iBtnHeight - MTCtrl_Particle_MaxNum) / (MTCtrl_Particle_MaxNum + 1);

	int iLeftTemp = iLeft;

	m_st_Name.MoveWindow(0, iTop, cx, iTitleH);

	iTop += iTitleH + 3;

	iLeft = cx - iBtnWidth - 1;
	m_bn_Item[Para_Right].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iLeft -= iBtnWidth;
	m_bn_Item[Para_Left].MoveWindow(iLeft, iTop, iBtnWidth, iBtnHeight);

	iTop += iBtnHeight + 3;
	iLeft = iLeftMargin;
	for (UINT nIdx = 0; nIdx < Title_CANCommPg4_MaxNum; nIdx++)
	{
		if (Title_CANCommPg4_Recv == nIdx)
		{
			m_stTitle[nIdx].MoveWindow(iLeft, iTop, iNameWidth * 3, iCtrlHeight);
			iLeft += iNameWidth * 3 + 1;
		}
		else
		{
			m_stTitle[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);
			iLeft += iNameWidth + 1;
		}

	}

	iLeft = iLeftTemp;
	iTop += iCtrlHeight + 1;

	for (UINT nIdx = 0; nIdx < MTCtrl_Particle_MaxNum; nIdx++)
	{
		m_stTestName[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);

		iLeft += iNameWidth + 1;
		m_stSendProtol[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);

		iLeft += iNameWidth + 1;
		m_stRecvProtol[nIdx].MoveWindow(iLeft, iTop, iNameWidth * 3, iCtrlHeight);

		iLeft += iNameWidth * 3 + 1;
		m_ed_Index[nIdx].MoveWindow(iLeft, iTop, iNameWidth, iCtrlHeight);

		iLeft += iNameWidth + 1;
		m_btTestFunc[nIdx].MoveWindow(iLeft, iTop - 1, iNameWidth, iCtrlHeight + 1);

		iTop += iCtrlHeight + 1;
		iLeft = iLeftTemp;
	}
}

//=============================================================================
// Method		: PreCreateWindow
// Access		: virtual public  
// Returns		: BOOL
// Parameter	: CREATESTRUCT & cs
// Qualifier	:
// Last Update	: 2017/11/12 - 23:18
// Desc.		:
//=============================================================================
BOOL CWnd_MT_Particle::PreCreateWindow(CREATESTRUCT& cs)
{
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS,
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW + 1), NULL);

	return CWnd::PreCreateWindow(cs);
}

//=============================================================================
// Method		: OnRangeCmds
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nID
// Qualifier	:
// Last Update	: 2017/11/12 - 23:18
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::OnRangeCmds(__in UINT nID)
{
	UINT nFunc	 = nID - IDC_BTN_SENDFUNC;
	INT nParaIdx = m_nPort;

	SetButtonEnable(FALSE);

	LRESULT lReturn = GetParent()->GetParent()->GetParent()->SendMessage(WM_MANAUL_CANCOMMPG4, (WPARAM)nParaIdx, (LPARAM)nFunc);

	SetButtonEnable(TRUE);
}

//=============================================================================
// Method		: OnRangePort
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nID
// Qualifier	:
// Last Update	: 2017/11/18 - 16:52
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::OnRangePort(__in UINT nID)
{
	UINT nFunc = nID - IDC_BTN_PARA;

	m_nPort = nFunc;

	if (Para_Left == nFunc)
	{
		m_bn_Item[Para_Right].SetCheck(0);
	}
	else
	{
		m_bn_Item[Para_Left].SetCheck(0);
	}
}

//=============================================================================
// Method		: GetTextValue
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestItem
// Parameter	: __out CString & szValue
// Qualifier	:
// Last Update	: 2017/12/10 - 10:29
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::GetTextValue(__in UINT nTestItem, __out CString &szValue)
{
	m_ed_Index[nTestItem].GetWindowTextW(szValue);
}

//=============================================================================
// Method		: SetTextSendProtocol
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/12 - 23:18
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::SetTextSendProtocol(__in UINT nTestItem, __in CString szValue)
{
	m_stSendProtol[nTestItem].SetWindowTextW(szValue);
}

//=============================================================================
// Method		: SetTextRecvProtocol
// Access		: public  
// Returns		: void
// Parameter	: __in UINT nTestItem
// Parameter	: __in CString szValue
// Qualifier	:
// Last Update	: 2017/11/12 - 23:18
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::SetTextRecvProtocol(__in UINT nTestItem, __in CString szValue)
{
	m_stRecvProtol[nTestItem].SetWindowTextW(szValue);
}

//=============================================================================
// Method		: ResetInfo
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/11/12 - 23:18
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::ResetInfo()
{
	for (UINT nIdx = 0; nIdx < MTCtrl_Particle_MaxNum; nIdx++)
	{
		m_stRecvProtol[nIdx].SetWindowTextW(_T(""));
	}
}

//=============================================================================
// Method		: SetButtonEnable
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bEnable
// Qualifier	:
// Last Update	: 2017/11/18 - 16:24
// Desc.		:
//=============================================================================
void CWnd_MT_Particle::SetButtonEnable(__in BOOL bEnable)
{
	for (UINT nIdx = 0; nIdx < MTCtrl_Particle_MaxNum; nIdx++)
	{
		m_btTestFunc[nIdx].EnableWindow(bEnable);
	}
}







