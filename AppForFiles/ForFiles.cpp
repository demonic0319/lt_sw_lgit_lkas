﻿//*****************************************************************************
// Filename	: 	ForFiles.cpp
// Created	:	2017/3/6 - 19:52
// Modified	:	2017/3/6 - 19:52
//
// Author	:	PiRing
//	
// Purpose	:	
//*****************************************************************************
#include "stdafx.h"
#include "ForFiles.h"


CForFiles::CForFiles()
{
// 	m_wScheduleHour		= 0;
// 	m_wCycleDay			= 1;
// 	m_wCycleHour		= 0;
// 	m_dwTimerCycle		= 86400000;// 1 Day
// 
// 	// 쓰레드
// 	m_hTimer_ForFiles	= nullptr;
//	m_hTimerQueue		= nullptr;
}


CForFiles::~CForFiles()
{
	DeleteTimer();
}

//=============================================================================
// Method		: CreateTimer
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/6 - 19:51
// Desc.		:
//=============================================================================
void CForFiles::CreateTimer()
{
	__try
	{
	 	if (nullptr == m_hTimerQueue)
	 	{
	 		m_hTimerQueue = CreateTimerQueue();
			if (nullptr != m_hTimerQueue)
			{
				__try
				{
					if (!CreateTimerQueueTimer(&m_hTimer_ForFiles, m_hTimerQueue, (WAITORTIMERCALLBACK)TimerRoutine, (PVOID)this, 0, m_dwTimerCycle, WT_EXECUTEDEFAULT))
					{
						TRACE(_T("CreateTimerQueueTimer failed (%d)\n"), GetLastError());
					}
				}
				__except (EXCEPTION_EXECUTE_HANDLER)
				{
					TRACE(_T("*** Exception Error : CForFiles::CreateTimerQueueTimer()\n"));
				}
			}
			else
	 		{
	 			TRACE(_T("CreateTimerQueue failed (%d)\n"), GetLastError());
	 			return;
	 		}
	 	}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	 	TRACE(_T("*** Exception Error : CreateTimerQueue ()"));
	}
}

//=============================================================================
// Method		: DeleteTimer
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/7 - 9:53
// Desc.		:
//=============================================================================
void CForFiles::DeleteTimer()
{
	__try
	{
	 	if (nullptr != m_hTimerQueue)
	 	{
	 		if (DeleteTimerQueue(m_hTimerQueue))
	 		{
	 			m_hTimerQueue = nullptr;
	 
	 			TRACE(_T("타이머 종료 : CForFiles::DeleteTimer()\n"));
	 		}
	 		else
	 		{
	 			TRACE(_T("DeleteTimerQueue failed (%d)\n"), GetLastError());
	 		}
	 	}
	}
	__except (EXCEPTION_EXECUTE_HANDLER)
	{
	 	TRACE(_T("*** Exception Error : CForFiles::DeleteTimer()\n"));
	}
}

//=============================================================================
// Method		: TimerRoutine
// Access		: protected static  
// Returns		: VOID CALLBACK
// Parameter	: __in PVOID lpParam
// Parameter	: __in BOOLEAN TimerOrWaitFired
// Qualifier	:
// Last Update	: 2017/3/7 - 9:53
// Desc.		:
//=============================================================================
VOID CALLBACK CForFiles::TimerRoutine(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired)
{
	CForFiles* pThis = (CForFiles*)lpParam;

	pThis->CheckOldFiles();
}

//=============================================================================
// Method		: CheckOldFiles
// Access		: protected  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/6 - 19:51
// Desc.		:
//=============================================================================
void CForFiles::CheckOldFiles()
{
	auto nCount = m_listPathz.GetCount();

	CString szParameters;
	for (INT_PTR nIdx = 0; nIdx < nCount; nIdx++)
	{
		if (m_listPathz[nIdx].bIncludeSubFolder)
			szParameters.Format(_T("/p %s /s /m *.%s /d -%d /c \"cmd /c del @file\""), m_listPathz[nIdx].szPath, m_listPathz[nIdx].szFilter, m_listPathz[nIdx].wDays);
		else
			szParameters.Format(_T("/p %s /m *.%s /d -%d /c \"cmd /c del @file\""), m_listPathz[nIdx].szPath, m_listPathz[nIdx].szFilter, m_listPathz[nIdx].wDays);

		ShellExecute(NULL, TEXT("open"), _T("forfiles.exe"), szParameters, NULL, SW_HIDE);

		//forfiles /p 경로 /s 하위포함 /m 필터 /d 기준날짜 /c 명령어 형식이며
		//forfiles /p C:\Temp /S /M *.txt /d -2015-05-06 /c "cmd /c del @file"
	}
}

//=============================================================================
// Method		: SaveSettings
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Qualifier	:
// Last Update	: 2017/3/7 - 15:35
// Desc.		:
//=============================================================================
BOOL CForFiles::SaveSettings(__in LPCTSTR szPath)
{
	if (NULL == szPath)
		return FALSE;

	::DeleteFile(szPath);

	BOOL bReturn = TRUE;

	CString strAppName;
	CString strValue;

	strAppName = _T("Schedule");

	strValue.Format(_T("%d"), m_wScheduleHour);
	WritePrivateProfileString(strAppName, _T("ScheduleHour"), strValue, szPath);

	strValue.Format(_T("%d"), m_wCycleDay);
	WritePrivateProfileString(strAppName, _T("CycleDay"), strValue, szPath);

	strValue.Format(_T("%d"), m_wCycleHour);
	WritePrivateProfileString(strAppName, _T("CycleHour"), strValue, szPath);

	auto nCount = m_listPathz.GetCount();

	for (INT_PTR nIdx = 0; nIdx < nCount; nIdx++)
	{
		strAppName.Format(_T("Itemz_%03d"), nIdx + 1);;

		strValue.Format(_T("%d"), m_listPathz[nIdx].bIncludeSubFolder);
		WritePrivateProfileString(strAppName, _T("IncludeSubFolder"), strValue, szPath);

		strValue = m_listPathz[nIdx].szPath;
		WritePrivateProfileString(strAppName, _T("Path"), strValue, szPath);

		strValue = m_listPathz[nIdx].szFilter;
		WritePrivateProfileString(strAppName, _T("Filter"), strValue, szPath);

		strValue.Format(_T("%d"), m_listPathz[nIdx].wDays);
		WritePrivateProfileString(strAppName, _T("Days"), strValue, szPath);
	}

	return bReturn;
}

//=============================================================================
// Method		: LoadSettings
// Access		: public  
// Returns		: BOOL
// Parameter	: __in LPCTSTR szPath
// Qualifier	:
// Last Update	: 2017/3/7 - 16:09
// Desc.		:
//=============================================================================
BOOL CForFiles::LoadSettings(__in LPCTSTR szPath)
{
	if (NULL == szPath)
		return FALSE;

	// 파일이 존재하는가?
	if (!PathFileExists(szPath))
	{
		return FALSE;
	}

	BOOL bReturn = TRUE;

	TCHAR   inBuff[1024] = { 0, };
	CString strAppName;

	strAppName = _T("Schedule");

	GetPrivateProfileString(strAppName, _T("ScheduleHour"), _T(""), inBuff, 255, szPath);
	m_wScheduleHour = (WORD)_ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("CycleDay"), _T(""), inBuff, 255, szPath);
	m_wCycleDay = (WORD)_ttoi(inBuff);

	GetPrivateProfileString(strAppName, _T("CycleHour"), _T(""), inBuff, 255, szPath);
	m_wCycleHour = (WORD)_ttoi(inBuff);


	m_listPathz.RemoveAll();

	for (INT_PTR nIdx = 0; nIdx < 100; nIdx++)
	{
		ST_ForFile	stForFile;

		strAppName.Format(_T("Itemz_%03d"), nIdx + 1);

		if (NULL != GetPrivateProfileString(strAppName, _T("IncludeSubFolder"), _T(""), inBuff, 255, szPath))
		{
			stForFile.bIncludeSubFolder = (BOOL)_ttoi(inBuff);

			GetPrivateProfileString(strAppName, _T("Path"), _T(""), inBuff, 255, szPath);
			stForFile.szPath = inBuff;

			GetPrivateProfileString(strAppName, _T("Filter"), _T(""), inBuff, 255, szPath);
			stForFile.szFilter = inBuff;

			GetPrivateProfileString(strAppName, _T("Days"), _T(""), inBuff, 255, szPath);
			stForFile.wDays = (WORD)_ttoi(inBuff);

			m_listPathz.Add(stForFile);
		}
		else
		{
			break;
		}
	}


	return bReturn;
}

//=============================================================================
// Method		: RemoveAll_PathFilters
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/6 - 19:51
// Desc.		:
//=============================================================================
void CForFiles::RemoveAll_PathFilters()
{
	m_listPathz.RemoveAll();

	SaveSettings(m_szPath);
}

//=============================================================================
// Method		: Add_PathFilter
// Access		: public  
// Returns		: void
// Parameter	: __in LPCTSTR szPath
// Parameter	: __in LPCTSTR szFilter
// Parameter	: __in WORD wDays
// Parameter	: __in BOOL bIncludeSubFolder
// Qualifier	:
// Last Update	: 2017/3/6 - 19:51
// Desc.		:
//=============================================================================
void CForFiles::Add_PathFilter(__in LPCTSTR szPath, __in LPCTSTR szFilter /*= _T("*")*/,  __in WORD wDays /*= 30*/, __in BOOL bIncludeSubFolder /*= TRUE*/)
{
	ST_ForFile	stForFile;

	stForFile.szPath			= szPath;
	stForFile.szFilter			= szFilter;
	stForFile.bIncludeSubFolder	= bIncludeSubFolder;
	stForFile.wDays				= wDays;

	m_listPathz.Add(stForFile);

	SaveSettings(m_szPath);
}

void CForFiles::Add_PathFilter(__in ST_ForFile* pstForFile)
{
	if (NULL != pstForFile)
	{
		m_listPathz.Add(*pstForFile);

		SaveSettings(m_szPath);
	}
}

//=============================================================================
// Method		: Delete_PathFilter
// Access		: public  
// Returns		: void
// Parameter	: __in INT_PTR iIndex
// Qualifier	:
// Last Update	: 2017/3/8 - 16:50
// Desc.		:
//=============================================================================
void CForFiles::Delete_PathFilter(__in INT_PTR iIndex)
{
	INT_PTR iCount = m_listPathz.GetCount();

	if (iIndex < iCount)
	{
		m_listPathz.RemoveAt(iIndex);

		SaveSettings(m_szPath);
	}
}

//=============================================================================
// Method		: GetAt_PathFilter
// Access		: public  
// Returns		: ST_ForFile&
// Parameter	: __in INT_PTR iIndex
// Qualifier	:
// Last Update	: 2017/3/8 - 16:54
// Desc.		:
//=============================================================================
ST_ForFile& CForFiles::GetAt_PathFilter(__in INT_PTR iIndex)
{
	return m_listPathz.GetAt(iIndex);
}

//=============================================================================
// Method		: GetCount_PathFilter
// Access		: public  
// Returns		: INT_PTR
// Qualifier	:
// Last Update	: 2017/3/8 - 16:54
// Desc.		:
//=============================================================================
INT_PTR CForFiles::GetCount_PathFilter()
{
	return m_listPathz.GetCount();
}

//=============================================================================
// Method		: SetSchedule
// Access		: public  
// Returns		: void
// Parameter	: __in WORD wScheduleHour
// Parameter	: __in WORD wCycleDay
// Parameter	: __in WORD wCycleHour
// Qualifier	:
// Last Update	: 2017/3/6 - 19:51
// Desc.		:
//=============================================================================
void CForFiles::SetSchedule(__in WORD wScheduleHour /*= 0*/, __in WORD wCycleDay /*= 1*/, __in WORD wCycleHour /*= 0*/)
{
	m_wScheduleHour	= wScheduleHour;
	m_wCycleDay		= wCycleDay;
	m_wCycleHour	= wCycleHour;

	m_dwTimerCycle	= (wCycleDay * 86400000) + (wCycleHour * 3600000);
	// 86400000 = (24 * 60 * 60 * 1000)
	//  3600000 = (60 * 60 * 1000)
}

//=============================================================================
// Method		: StartTimer
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 16:16
// Desc.		:
//=============================================================================
void CForFiles::StartTimer()
{
	CreateTimer();
}

void CForFiles::StopTimer()
{
	DeleteTimer();
}

//=============================================================================
// Method		: RunForFiles
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 16:45
// Desc.		:
//=============================================================================
void CForFiles::RunForFiles()
{
	CheckOldFiles();
}

//=============================================================================
// Method		: SetUseSettingFiles
// Access		: public  
// Returns		: void
// Parameter	: __in BOOL bUse
// Parameter	: __in LPCTSTR szPath
// Qualifier	:
// Last Update	: 2017/3/8 - 10:01
// Desc.		:
//=============================================================================
void CForFiles::SetUseSettingFiles(__in BOOL bUse, __in LPCTSTR szPath /*= NULL*/)
{
	m_bUseSettingFile = bUse;
	
	if (NULL != szPath)
	{
		//m_szPath = szPath;
		m_szPath.Format(_T("%s\\ForFiles.ini"), szPath);
	}
}

//=============================================================================
// Method		: LoadPreviousSettings
// Access		: public  
// Returns		: void
// Qualifier	:
// Last Update	: 2017/3/8 - 17:02
// Desc.		:
//=============================================================================
void CForFiles::LoadPreviousSettings()
{
	LoadSettings(m_szPath);
}
