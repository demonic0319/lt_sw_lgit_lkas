//*****************************************************************************
// Filename	: AppForFilesDlg.h
// Created	: 2010/12/28
// Modified	: 2017/03/08
//
// Author	: PiRing
//	
// Purpose	: 
//*****************************************************************************
#ifndef AppForFilesDlg_h__
#define AppForFilesDlg_h__

#pragma once

#include "afxwin.h"
#include "ForFiles.h"

//=============================================================================
// CAppForFilesDlg 대화 상자
//=============================================================================
class CAppForFilesDlg : public CDialogEx
{
// 생성입니다.
public:
	CAppForFilesDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.
	virtual ~CAppForFilesDlg ();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_APPFORFILES };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	afx_msg int			OnCreate				(LPCREATESTRUCT lpCreateStruct);
	afx_msg void		OnDestroy				();
	afx_msg void		OnClose					();
	afx_msg void		OnSize					(UINT nType, int cx, int cy);
	afx_msg void		OnGetMinMaxInfo			(MINMAXINFO* lpMMI);
	afx_msg void		OnPaint					();
	afx_msg HBRUSH		OnCtlColor				(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg HCURSOR		OnQueryDragIcon			();

	virtual BOOL		OnInitDialog			();
	virtual BOOL		PreTranslateMessage		(MSG* pMsg);
	virtual BOOL		PreCreateWindow			(CREATESTRUCT& cs);

	afx_msg void		OnAppExit				();
	afx_msg void		OnAppOpen				();
	afx_msg LRESULT		OnTrayNotify			(WPARAM wp, LPARAM lp);
	afx_msg LRESULT		OnCopyData				(WPARAM wParam, LPARAM lParam);

	afx_msg void		OnBnClickedOk			();

	afx_msg void		OnBnClickedBnPath		();
	afx_msg void		OnBnClickedBnAdd		();
	afx_msg void		OnBnClickedBnDelete		();
	afx_msg void		OnBnClickedBnClear		();
	afx_msg void		OnBnClickedBnDoForFiles	();
	
	DECLARE_MESSAGE_MAP()
	
	NOTIFYICONDATA		m_nid;			// struct for Shell_NotifyIcon args
	void				OnTrayContextMenu	();

	CStatic				m_st_Title;

	CStatic				m_st_Path;
	CEdit				m_ed_Path;
	CButton				m_bn_Path;

	CStatic				m_st_Filter;
	CEdit				m_ed_Filter;

	CStatic				m_st_IncSubFolder;
	CButton				m_chk_IncSubFolder;

	CStatic				m_st_Days;
	CEdit				m_ed_Days;

	CButton				m_bn_Add;
	CButton				m_bn_Delete;
	CButton				m_bn_ClearList;
	CListCtrl			m_lc_PathList;

	CButton				m_bn_DoForFiles;

	DWORD				m_dwCycleWatch;

	// 오래된 파일 자동삭제 기능의 클래스
	CForFiles			m_ForFiles;

	// 프로그램 버전 정보 구하기
	CString			GetVersionInfo		(LPCTSTR lpszEntry);

	// 프로그램 강제 종료시 트레이 아이콘 잔상이 남는 문제 해결용 함수
	static HWND		FindTrayWindow		();
	BOOL			EraseTrayIcon		();

	// 리스트 컨트롤 초기 설정
	void			InitListCtrl		();
	// 리스트 컨트롤에 항목 추가
	void			Insert_PathFilter	(__in ST_ForFile* pstForFile);
	// CForFiles에 설정된 항목을 리스트 컨트롤에 표시하는 기능
	void			RefreshList			();

public:
	
	// 타이머 구동/정지
	void			StartWatchTimer		();
	void			StopWatchTimer		();

};

#endif // AppForFilesDlg_h__
