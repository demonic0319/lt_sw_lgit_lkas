//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by AppForFiles.rc
//
#define IDD_DLG_APPFORFILES             102
#define IDR_MAINFRAME                   128
#define IDR_MENU_TRAY                   129
#define IDC_ST_TITLE                    1007
#define ID_TRAY_OPEN                    32771
#define ID_TRAY_EXIT                    32772
#define ID_APP_OPEN                     32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
