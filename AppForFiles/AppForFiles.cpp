
// AppForFiles.cpp : 응용 프로그램에 대한 클래스 동작을 정의합니다.
//

#include "stdafx.h"
#include "AppForFiles.h"
#include "LimitSingleInstance.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAppForFilesApp

BEGIN_MESSAGE_MAP(CAppForFilesApp, CWinAppEx)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CAppForFilesApp 생성

CAppForFilesApp::CAppForFilesApp()
{
	// TODO: 여기에 생성 코드를 추가합니다.
	// InitInstance에 모든 중요한 초기화 작업을 배치합니다.
}


// 유일한 CAppForFilesApp 개체입니다.

CAppForFilesApp theApp;
CLimitSingleInstance g_SingleInstanceObj(TEXT("Global\\{1B7CC55C-EB7A-4E2B-AFD2-EE591DA3D369}"));

// CAppForFilesApp 초기화

BOOL CAppForFilesApp::InitInstance()
{
	if (g_SingleInstanceObj.IsAnotherInstanceRunning())
		return FALSE; 

	// 응용 프로그램 매니페스트가 ComCtl32.dll 버전 6 이상을 사용하여 비주얼 스타일을
	// 사용하도록 지정하는 경우, Windows XP 상에서 반드시 InitCommonControlsEx()가 필요합니다.
	// InitCommonControlsEx()를 사용하지 않으면 창을 만들 수 없습니다.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 응용 프로그램에서 사용할 모든 공용 컨트롤 클래스를 포함하도록
	// 이 항목을 설정하십시오.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	// 표준 초기화
	// 이들 기능을 사용하지 않고 최종 실행 파일의 크기를 줄이려면
	// 아래에서 필요 없는 특정 초기화
	// 루틴을 제거해야 합니다.
	// 해당 설정이 저장된 레지스트리 키를 변경하십시오.
	// TODO: 이 문자열을 회사 또는 조직의 이름과 같은
	// 적절한 내용으로 수정해야 합니다.
	SetRegistryKey(_T("BiwonTech"));

	//CCommandLineInfo cmdInfo;
	//ParseCommandLine(cmdInfo);


	m_pDlg = new CAppForFilesDlg;	

	if (NULL != theApp.m_lpCmdLine)
	{		
		//m_pDlg->SetFindWindowName(theApp.m_lpCmdLine);
		//m_pDlg->SetFindProcessName(theApp.m_lpCmdLine);
	}

	m_pDlg->Create( CAppForFilesDlg::IDD );
	m_pDlg->StartWatchTimer();
	m_pDlg->ShowWindow( SW_HIDE );

	m_pMainWnd = m_pDlg;

	return TRUE;
	
	//CAppForFilesDlg dlg;
	//m_pMainWnd = &dlg;
	//INT_PTR nResponse = dlg.DoModal();
	//if (nResponse == IDOK)
	//{
	//	// TODO: 여기에 [확인]을 클릭하여 대화 상자가 없어질 때 처리할
	//	//  코드를 배치합니다.
	//}
	//else if (nResponse == IDCANCEL)
	//{
	//	// TODO: 여기에 [취소]를 클릭하여 대화 상자가 없어질 때 처리할
	//	//  코드를 배치합니다.
	//}

	//// 대화 상자가 닫혔으므로 응용 프로그램의 메시지 펌프를 시작하지 않고  응용 프로그램을 끝낼 수 있도록 FALSE를
	//// 반환합니다.
	//return FALSE;
}

int CAppForFilesApp::ExitInstance()
{
	delete m_pDlg;	

	return CWinAppEx::ExitInstance();
}
