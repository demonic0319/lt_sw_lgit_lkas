﻿//*****************************************************************************
// Filename	: 	ForFiles.h
// Created	:	2017/3/6 - 19:03
// Modified	:	2017/3/6 - 19:03
//
// Author	:	PiRing
//	
// Purpose	:	오래된 파일들 자동으로 삭제하는 클래스
//*****************************************************************************
#ifndef ForFiles_h__
#define ForFiles_h__

#pragma once

#include <afxwin.h>

typedef struct _tag_ForFile
{
	CString		szPath;				// 검색 할 폴더
	CString		szFilter;			// ex) *.*, *.txt, *.log, *.csv, *.bmp, *.jpg, *.raw, *.png
	BOOL		bIncludeSubFolder;	// 하위 폴더 검색 여부
	WORD		wDays;				// 설정된 날짜 이전의 파일만 검색한다.
}ST_ForFile, *PST_ForFile;

//-----------------------------------------------------------------------------
// CForFiles
//-----------------------------------------------------------------------------
class CForFiles
{
public:
	CForFiles();
	~CForFiles();

protected:
	
	// 항목 저장 공간
	CArray<ST_ForFile, ST_ForFile&>	m_listPathz;

	WORD		m_wScheduleHour		= 0;
	// 타이머 주기 날짜, 시간
	WORD		m_wCycleDay			= 1;
	WORD		m_wCycleHour		= 0;
	// 타이머 주기 파라미터 변수 (날짜 + 시간을 ms로 변환한 값)
	DWORD		m_dwTimerCycle		= 86400000;// 1 Day

	// ini 설정 파일 사용여부
	BOOL		m_bUseSettingFile	= FALSE;
	// ini 설정 파일의 전체 경로와 파일명
	CString		m_szPath;

	// 타이머 처리 변수 및 함수들
	HANDLE		m_hTimer_ForFiles	= nullptr;
	HANDLE		m_hTimerQueue		= nullptr;
	void		CreateTimer				();
	void		DeleteTimer				();
	static VOID CALLBACK TimerRoutine	(__in PVOID lpParam, __in BOOLEAN TimerOrWaitFired);
	
	// 오래된 파일 검색하여 삭제
	void		CheckOldFiles			();	
	
	// ini 파일 저장/불러오기
	BOOL		SaveSettings(__in LPCTSTR szPath);
	BOOL		LoadSettings(__in LPCTSTR szPath);

public:

	// 모든 항목 목록에서 제거
	void		RemoveAll_PathFilters	();
	// 목록에 항목 추가
	void		Add_PathFilter			(__in LPCTSTR szPath, __in LPCTSTR szFilter = _T("*"), __in WORD wDays = 30, __in BOOL bIncludeSubFolder = TRUE);
	void		Add_PathFilter			(__in ST_ForFile* pstForFile);
	// 목록에서 항목 제거
	void		Delete_PathFilter		(__in INT_PTR iIndex);
	// 특정 위치에 있는 항목 가져오기
	ST_ForFile&	GetAt_PathFilter		(__in INT_PTR iIndex);
	// 목록에 있는 전체 항목 갯수 가져우기
	INT_PTR		GetCount_PathFilter		();
	
	// 타이머 주기 설정
	void		SetSchedule				(__in WORD wScheduleHour = 0, __in WORD wCycleDay = 1, __in WORD wCycleHour = 0);
	// 타이머 시작
	void		StartTimer				();
	// 타이머 중지
	void		StopTimer				();
	// 오래된 파일 바로 지우기
	void		RunForFiles				();

	// ini 파일 사용여부
	void		SetUseSettingFiles		(__in BOOL bUse, __in LPCTSTR szPath = NULL);
	// ini 파일에서 기존 설정 불러오기
	void		LoadPreviousSettings	();


	// 윈도우 스케쥴러에 등록하기 (등록, 정보, 삭제)
	

};

#endif // ForFiles_h__

